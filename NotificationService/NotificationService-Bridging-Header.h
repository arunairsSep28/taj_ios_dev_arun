//
//  Use this file to import your target's public headers that you would like to expose to Swift.
//

// ServiceExtension-Bridging-Header.h (Optional)
// Required if ServiceExtension Target is in Swift

#import <UserNotifications/UserNotifications.h>
#import <WebEngageBannerPush/WEXPushNotificationService.h>
