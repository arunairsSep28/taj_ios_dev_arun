//
//  GamesFilter.h
//  Taj Rummy
//
//  Created by Grid Logic on 18/08/20.
//  Copyright © 2020 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GamesFilter : NSObject

+ (GamesFilter *)sharedInstance;


+(NSMutableArray *)filterTablesWithTablesList:(NSMutableArray*)tablesList tableCost:(NSString *)tableCost tableType:(NSString *)tableType playersType:(int)playerType ;

+(NSArray*)sortWithList:(NSArray *)listArray;

@end

NS_ASSUME_NONNULL_END
