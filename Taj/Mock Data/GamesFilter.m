//
//  GamesFilter.m
//  Taj Rummy
//
//  Created by Grid Logic on 18/08/20.
//  Copyright © 2020 Robosoft Technologies. All rights reserved.
//


#import "GamesFilter.h"

@implementation GamesFilter

+ (GamesFilter *)sharedInstance
{
    static GamesFilter *_sharedInstance = nil;
    _sharedInstance = [[GamesFilter alloc] init];
    return _sharedInstance;
}


+(NSMutableArray *)filterTablesWithTablesList:(NSMutableArray*)tablesList tableCost:(NSString *)tableCost tableType:(NSString *)tableType playersType:(int)playerType {
    
    NSMutableArray * filteredTablesList = [NSMutableArray new];
    
    for (NSDictionary *dict in tablesList) {
        
        if ([[dict objectForKey:@"TAJ_table_cost"] isEqualToString:tableCost] && ([[dict objectForKey:@"TAJ_table_type"] isEqualToString:tableType]) && ([[dict objectForKey:@"TAJ_maxplayer"] intValue] == playerType) ) {
            [filteredTablesList addObject:dict];
        }
    }
    
    NSSortDescriptor *sortByName = [NSSortDescriptor sortDescriptorWithKey:@"TAJ_bet.intValue"
        ascending:YES];
    NSArray *sortDescriptors = [NSArray arrayWithObject:sortByName];
    NSArray *sortedFilteredTablesList= [filteredTablesList sortedArrayUsingDescriptors:sortDescriptors];
    return [sortedFilteredTablesList mutableCopy];
    
}

+(NSArray*)sortWithList:(NSArray *)listArray
{
    NSMutableArray * betsList = [NSMutableArray new];
    NSArray * sortedTablesList = [NSArray new];
    
    NSDictionary * betDict = [NSDictionary new];
    
    for (NSDictionary *dict in listArray) {
        [betsList addObject:[dict objectForKey:@"TAJ_bet"]];
    }
    
//    NSLog(@"BET Array : %@",betsList);
//
//    NSSet *betsSet = [NSSet setWithArray:betsList];
//    //NSLog(@"TEl : %@",poolsSet);
//    NSArray *uniqueBetsArray = [betsSet allObjects];
//    //NSLog(@"setArray : %@",poolsSetArray);
//
//    sortedTablesList = [uniqueBetsArray sortedArrayUsingDescriptors:
//                        @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
//                                                        ascending:YES]]];
//
//    NSLog(@"sorted_Array : %@",sortedTablesList);
//
    return betsList;
}

@end
