//
//  LocationHandler.m
//  GoolePlacesDemo
//
//  Created by Ratheesh on 09/04/18.
//  Copyright © 2018 Ratheesh. All rights reserved.
//

#import "LocationHandler.h"

#define AlertTitle_authorizationLocationDenied  "Location Authorisation Denied"
#define AlertTitle_authorizationLocationRestricted "Location Restricted"
#define AlertTitle_authorizationLocationDisabled   "Location Service disabled"

#define AlertMessage_authorizationLocationDisabled  @"Needs access to your current location.Please turn on location service in your device settings."
#define AlertMessage_authorizationLocationRestricted @"is restricted from using location services.Please go to Settings and enable the Location for this app to use this feature to restric the user don't play in judiciary areas."
#define AlertMessage_authorizationLocationDenied "level location permission settings has been denied. Please go to Settings and enable the Location for this app to use this feature to restric the user don't play in judiciary areas."

#define location_settings @"Settings"
#define location_cancel @"Cancel"

typedef void(^OtherButtonsAction)(UIAlertAction *otherButtonsAction, long clickedAtIndex);

@implementation LocationHandler
@synthesize delegate, locationStatus;

#pragma mark - Life Cycle

+(LocationHandler *)SharedLocationHandler {
    static LocationHandler *sharedHandler;
    if (sharedHandler == nil){
        sharedHandler = [[LocationHandler alloc] init];
        [sharedHandler initiate];
    }
    return sharedHandler;
}

- (void)initiate {
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.delegate = self;

    if ([_locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
        [_locationManager requestWhenInUseAuthorization];
    }
}



#pragma mark - Start/Stop/Reset Update

- (void)updateCurrentLocation:(currentLocation)completionBlock {
    _locationBlock = completionBlock;
}

- (void)startUpdating {
    [self.locationManager startUpdatingLocation];
}

- (void)stopUpdating {
    [self.locationManager stopUpdatingLocation];
}

- (void)resetLatLon {
}

#pragma mark - CLLocation Delegate

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
#if DEBUG
    NSLog(@"LOCATION FETCHING FAILED: %@", error.description);
#endif
    if (_locationBlock != nil) {
        
        CLLocation *location = [[CLLocation alloc] initWithLatitude:0.0 longitude:0.0];
        _locationBlock(location);
    }
    
    if ([delegate respondsToSelector:@selector(locationManagerReceivedError:)]) {
        [delegate locationManagerReceivedError:error.localizedDescription];
    }
}

- (void)locationManager:(CLLocationManager *)manager didUpdateLocations:(nonnull NSArray<CLLocation *> *)locations
{
    self.currentLocation = [locations lastObject];
    
   // NSLog(@"LATITUDE: %f", _currentLocation.coordinate.latitude);
   // NSLog(@"LONGITUDE: %f", _currentLocation.coordinate.longitude);

    if (_locationBlock != nil) {
        _locationBlock(self.currentLocation);
    }
    
    if (delegate != nil) {

        if ([delegate respondsToSelector:@selector(locationFound:and:)]) {
            [delegate locationFound:self.currentLocation.coordinate.latitude and:self.currentLocation.coordinate.longitude];
        }
    }
}


- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    
    BOOL hasAuthorised = FALSE;
        switch (status)
        {
            case kCLAuthorizationStatusRestricted: {
                locationStatus = @"Restricted Access";
            }
                break;
            case kCLAuthorizationStatusDenied: {
                locationStatus = @"Denied access";
            }
                break;
            case kCLAuthorizationStatusNotDetermined:{
                locationStatus = @"Not determined";
            }
                break;
            default:
                locationStatus = @"Allowed access";
                hasAuthorised = TRUE;
                break;
        }
    
    if (hasAuthorised == TRUE) {
        [self updateCurrentLocation:^(CLLocation *currentLocation) {
        }];
        [self startUpdating];
    }
    else {
        
        [self resetLatLon];
        if (![locationStatus isEqualToString:@"Denied access"]){
            
            if (_locationBlock != nil) {
                CLLocation *location = [[CLLocation alloc] initWithLatitude:0.0 longitude:0.0];
                _locationBlock(location);
            }
        }
        if ((delegate != nil) && [delegate respondsToSelector:@selector(locationManagerStatus:)]){
            [delegate locationManagerStatus:locationStatus];
        }
    }
}

#pragma mark - Helpers

- (BOOL)shouldFetchUserLocationInViewController:(UIViewController *)viewController {
    
    BOOL shouldFetchLocation= NO;
    
    if ([CLLocationManager locationServicesEnabled]) {
        
        switch ([CLLocationManager authorizationStatus])
        {
            case kCLAuthorizationStatusAuthorizedWhenInUse: {
                
                shouldFetchLocation = YES;
                [self updateCurrentLocation:^(CLLocation *currentLocation) {}];
                [self startUpdating];
            }
            break;
                
            case kCLAuthorizationStatusDenied: {
                
                [self showAlertWithTitle:@AlertTitle_authorizationLocationDenied
                                 message:[NSString stringWithFormat:@"App %s", AlertMessage_authorizationLocationDenied]
                      otherButtonsTitles:@[location_settings]
                        showCancelButton:YES
                       cancelButtonTitle:location_cancel
                              showInview:viewController
                    alertControllerStyle:UIAlertControllerStyleAlert
                      otherButtonsAction:^(UIAlertAction *otherButtonsAction, long clickedAtIndex) {
                          
                          [self openURLWithString:UIApplicationOpenSettingsURLString];
                      }];
            }
            break;
            
            case kCLAuthorizationStatusRestricted:{
                
                [self showAlertWithTitle:@AlertTitle_authorizationLocationRestricted
                                 message:[NSString stringWithFormat:@"App %@", AlertMessage_authorizationLocationRestricted]
                      otherButtonsTitles:@[location_settings]
                        showCancelButton:YES
                       cancelButtonTitle:location_cancel
                              showInview:viewController
                    alertControllerStyle:UIAlertControllerStyleAlert
                      otherButtonsAction:^(UIAlertAction *otherButtonsAction, long clickedAtIndex) {
                          
                          [self openURLWithString:UIApplicationOpenSettingsURLString];
                      }];
            }
            break;
                
            case kCLAuthorizationStatusNotDetermined:{
                
                [self updateCurrentLocation:^(CLLocation *currentLocation) {}];
                [self startUpdating];
            }
            break;
                
            default:
                break;
        }
    }
    else {
        
        [self showAlertWithTitle:@AlertTitle_authorizationLocationDisabled
                         message:[NSString stringWithFormat:@"App %@", AlertMessage_authorizationLocationDisabled]
              otherButtonsTitles:@[location_settings]
                showCancelButton:YES
               cancelButtonTitle:location_cancel
                      showInview:viewController
            alertControllerStyle:UIAlertControllerStyleAlert
              otherButtonsAction:^(UIAlertAction *otherButtonsAction, long clickedAtIndex)
         {
             [self openURLWithString:UIApplicationOpenSettingsURLString];
         }];
    }
    return shouldFetchLocation;
}

#pragma mark - show Alert Controller

- (void)showAlertWithTitle:(NSString *)title
                    message:(NSString *)message
         otherButtonsTitles:(NSArray *)otherButtonsTitles
           showCancelButton:(BOOL)showCancelButton
          cancelButtonTitle:(NSString *)cancelButtonTitle
                 showInview:(UIViewController*)viewController
       alertControllerStyle:(UIAlertControllerStyle)alertControllerStyle
        otherButtonsAction:(OtherButtonsAction)otherButtonsAction {
    
    UIAlertController *alertController = [UIAlertController  alertControllerWithTitle:title  message:message  preferredStyle:alertControllerStyle];
    
    for (int index =0 ; index<otherButtonsTitles.count; index++){
        [alertController addAction:[UIAlertAction actionWithTitle:otherButtonsTitles[index] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action){
            otherButtonsAction(action,index);
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
    }
    
    if (showCancelButton) {
        
        [alertController addAction:[UIAlertAction actionWithTitle:cancelButtonTitle style:UIAlertActionStyleCancel handler:^(UIAlertAction *action){
            [alertController dismissViewControllerAnimated:YES completion:nil];
        }]];
    }
    [viewController presentViewController:alertController animated:YES completion:nil];
    
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}


@end
