//
//  LocationHandler.h
//  GoolePlacesDemo
//
//  Created by Ratheesh on 09/04/18.
//  Copyright © 2018 Ratheesh. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <UIKit/UIKit.h>

@protocol LocationManagerDelegate <NSObject>

@optional
- (void)locationFound:(double)latitiude and:(double)longitude;
- (void)locationManagerStatus:(NSString *)status;
- (void)locationManagerReceivedError:(NSString *)error;

@end

@interface LocationHandler : NSObject<CLLocationManagerDelegate>

@property (strong, nonatomic) CLLocationManager * locationManager;
@property (strong, nonatomic) CLLocation *currentLocation;
@property (strong, nonatomic) UIViewController *viewController;

typedef void (^currentLocation)(CLLocation *currentLocation);
@property (copy, nonatomic) currentLocation locationBlock;
@property (strong, nonatomic) id<LocationManagerDelegate> delegate;
@property (strong, nonatomic) NSString *locationStatus;

+ (LocationHandler*)SharedLocationHandler;
- (void)initiate;
- (void)startUpdating;
- (void)stopUpdating;
- (BOOL)shouldFetchUserLocationInViewController:(UIViewController *)viewController;
- (void)updateCurrentLocation:(currentLocation)completionBlock;


@end
