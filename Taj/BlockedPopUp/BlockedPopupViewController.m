/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRegistrationSuccessPopupViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by  Yogisha Poojary on 20/08/14.
 
 **/

#import "BlockedPopupViewController.h"

@interface BlockedPopupViewController ()
- (IBAction)closeThePopUp:(UIButton *)sender;

@end

@implementation BlockedPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mainView.layer.cornerRadius = 10;
    self.mainView.layer.masksToBounds = true;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeThePopUp:(UIButton *)sender
{
    [self.BlockedPopupDelegate closeButtonPressedForBlockedPopup];
}

@end
