//
//  SupportTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 19/03/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SupportTableViewCell : UITableViewCell

- (void)updateCellWithTitle:(NSString *)title managerStatus:(NSString *)status;


@end

NS_ASSUME_NONNULL_END
