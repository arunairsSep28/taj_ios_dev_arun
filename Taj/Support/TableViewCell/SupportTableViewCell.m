//
//  SupportTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 19/03/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "SupportTableViewCell.h"

@interface SupportTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgTitle;
@property (weak, nonatomic) IBOutlet UIView *viewStatus;

@end

@implementation SupportTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void) configureView {
    self.viewStatus.layer.borderColor = [UIColor whiteColor].CGColor;
}

- (void)updateCellWithTitle:(NSDictionary *)dataDic managerStatus:(NSString *)status
{
    [self configureView];
    self.lblTitle.text = [dataDic objectForKey:@"title"];
    self.imgTitle.image = [UIImage imageNamed:[dataDic objectForKey:@"support_logo"]];
    self.viewStatus.hidden = YES;
    if ([[dataDic valueForKey:@"status"] boolValue]) {
        self.viewStatus.hidden = NO;
    }
    
    if ([status isEqualToString:@"A"]) {
        //AWAY
        self.viewStatus.backgroundColor = [UIColor colorWithRed:251.0f/255.0f green:179.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
    }
    else if ([status isEqualToString:@"O"]) {
        //ONLINE
        self.viewStatus.backgroundColor = [UIColor colorWithRed:57.0f/255.0f green:181.0f/255.0f blue:74.0f/255.0f alpha:1.0f];
    }
    else {
        //INVISIBLE
        self.viewStatus.backgroundColor = [UIColor grayColor];
    }
    
}

@end
