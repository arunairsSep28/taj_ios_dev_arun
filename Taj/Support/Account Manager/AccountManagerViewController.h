//
//  AccountManagerViewController.h
//  Taj Rummy
//
//  Created by svc on 08/04/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AccountManagerViewController : UIViewController

- (instancetype)initWithInfoDict:(NSDictionary *)infoDict isAvailable:(BOOL)isAvailable;


@end

NS_ASSUME_NONNULL_END
