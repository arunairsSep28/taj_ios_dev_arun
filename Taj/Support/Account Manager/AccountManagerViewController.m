//
//  AccountManagerViewController.m
//  Taj Rummy
//
//  Created by svc on 08/04/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "AccountManagerViewController.h"

@interface AccountManagerViewController ()
@property (weak, nonatomic) IBOutlet UILabel *lblContactMessage;

@property (weak, nonatomic) IBOutlet UILabel *lblManagerName;
@property (weak, nonatomic) IBOutlet UILabel *lblCall;
@property (weak, nonatomic) IBOutlet UILabel *lblWhatsapp;
@property (weak, nonatomic) IBOutlet UIView *viewInfo;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (strong, nonatomic) NSDictionary *infoDict;
@property (assign ,nonatomic) BOOL isAvailable;
@property (weak, nonatomic) IBOutlet UIView *viewStatus;

@end

@implementation AccountManagerViewController

- (instancetype)initWithInfoDict:(NSDictionary *)infoDict isAvailable:(BOOL)isAvailable
{
    self = [super init];
    if (self) {
        _infoDict = infoDict;
        _isAvailable = isAvailable;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if (self.isAvailable) {
        [self loadData];
        self.viewInfo.hidden = NO;
        self.lblInfo.hidden = YES;
    }
    else {
        self.viewInfo.hidden = YES;
        self.lblInfo.hidden = NO;
    }
    
    [self configureView];
}

- (void) configureView {
    self.viewStatus.layer.borderColor = [UIColor whiteColor].CGColor;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

//9908293991 - 2950

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

#pragma mark - HELPERS

- (void)loadData {
    NSString * status = [self.infoDict objectForKey:@"manager_status"];
    if ([status isEqualToString:@"A"]) {
        //AWAY
        self.viewStatus.backgroundColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
    }
    else if ([status isEqualToString:@"O"]) {
        //ONLINE
        self.viewStatus.backgroundColor = [UIColor colorWithRed:57.0f/255.0f green:181.0f/255.0f blue:74.0f/255.0f alpha:1.0f];
    }
    else {
        //INVISIBLE
        self.viewStatus.backgroundColor = [UIColor grayColor];
    }
    
    self.lblManagerName.text = [self.infoDict valueForKey:@"manager_name"];
    self.lblContactMessage.text = [self.infoDict valueForKey:@"manager_message"];
    self.lblCall.text = [self.infoDict valueForKey:@"manager_number"];
    self.lblWhatsapp.text = [self.infoDict valueForKey:@"manager_number"];
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)callClicked:(UIButton *)sender {
    NSString *phoneStr = [NSString stringWithFormat:@"tel:%@",self.lblCall.text];
    NSURL *phoneURL = [NSURL URLWithString:phoneStr];
    [[UIApplication sharedApplication] openURL:phoneURL];
}

- (IBAction)whatsappClicked:(UIButton *)sender {
    NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
    NSString * messageStr = [NSString stringWithFormat:@"Hi %@, %@ here",self.lblManagerName.text,name];
    NSURL *whatsappURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://api.whatsapp.com/send?phone=%@&text=Hi!!!",self.lblWhatsapp.text]];
    if ([[UIApplication sharedApplication] canOpenURL: whatsappURL]) {
        [[UIApplication sharedApplication] openURL: whatsappURL];
    }
}

@end
