/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLobbyViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Shrinidhi Rao on 24/04/14.
 **/

#import <UIKit/UIKit.h>
#import "NIDropDown.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJTableViewController.h"
#import "TournamentLobbyView.h"
#import "TournamentDetailView.h"

@protocol TAJLobbyViewControllerDelegate;

@interface TAJLobbyViewController : UIViewController <UITableViewDelegate, UITableViewDataSource,NIDropDownDelegate,TAJTournamentDelegate,InfoPopupViewDelegate>
{
    TournamentDetailView* tourneyDetailView;
    BOOL isTournamentChipChanged;
}
@property (nonatomic, weak) id<TAJLobbyViewControllerDelegate>  lobbyDelegate;
@property (nonatomic,retain)UIView* topOptionsView;
@property(nonatomic,strong) NSMutableArray *arrayFilter;
@property(nonatomic,strong) NSMutableArray *arrayTablesList;//ratheesh

@property (weak, nonatomic) IBOutlet UIView *gameListView;
@property (weak, nonatomic) IBOutlet UIView *tournamentDetailView;

@property (strong, nonatomic) NSTimer *emptyTableNSTimer;
@property (assign) BOOL    isTournament;

@property (nonatomic,retain)NIDropDown* gameSelectList;
@property (nonatomic,retain)NIDropDown* variantSelectList;
@property (nonatomic,retain)NIDropDown* chipsSelectList;
@property (nonatomic,retain)NIDropDown* betSelectList;
@property (nonatomic,retain)NIDropDown* playerSelectList;

@property (nonatomic,retain)NSMutableArray* gameSelectListIndex;
@property (nonatomic,retain)NSMutableArray* variantSelectListIndex;
@property (nonatomic,retain)NSMutableArray* chipsSelectListIndex;
@property (nonatomic,retain)NSMutableArray* betSelectListIndex;
@property (nonatomic,retain)NSMutableArray* playerSelectListIndex;

@property (weak, nonatomic) IBOutlet UILabel *chipsButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *variantssButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *gametypeButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *betButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *playersButtonLabel;

@property (weak, nonatomic) IBOutlet UIButton *goButton;
@property (weak, nonatomic) IBOutlet UIButton *backButton;
@property (nonatomic, strong) NSMutableArray *filterdTableArray;
@property (nonatomic)         BOOL                  isFilteredData;

@property (nonatomic)         TableTypeStruct       tableTypeSearch;
@property (nonatomic)         GameTypeStruct        gameTypeSearch;
@property (nonatomic)         PlayerTypeStruct      playerTypeSearch;
@property (nonatomic)         BetTypeStruct         betTypeSearch;
@property (nonatomic)         TableCostEnum         tableCostSearch;
@property (nonatomic)         ViewTypeEnum          viewTypeSearch;

@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (nonatomic,retain)UIImageView* topImage;
@property (weak, nonatomic) IBOutlet UIButton *variantButton;
@property (weak, nonatomic) IBOutlet UIButton *gameTypeButton;
@property (weak, nonatomic) IBOutlet UIButton *chipsButton;
@property (weak, nonatomic) IBOutlet UIButton *betButton;
@property (weak, nonatomic) IBOutlet UIButton *betDummyButton;


@property (weak, nonatomic) IBOutlet UIButton *playersButton;
@property (nonatomic, strong) TAJTableViewController *tableViewController;
@property (nonatomic, strong) TournamentLobbyView *tournamentView;
@property (nonatomic, strong) TAJInfoPopupViewController *alertView;

//single click
@property(strong, nonatomic) NSMutableArray *listArray;
@property (assign, nonatomic) int selectedIndexPoints;
@property (assign, nonatomic) int selectedIndexPools;
@property (assign, nonatomic) int selectedIndexDeals;
@property(strong, nonatomic) NSString *gameVariantTypeStr;
@property(strong, nonatomic) NSString *gameCostTypeStr;

- (void)reloadLobbyScreen;

- (void)gameVariantTypeSelection:(UIButton *)sender withSelection:(NSMutableArray*)gameTypeVariant;
- (void)gameVariantTypeSelection:(UIButton *)sender withSelection:(NSMutableArray*)gameTypeVariant chipsButton:(UIButton *)sender2 withChips:(NSMutableArray*)chipsType gameButton:(UIButton *)sender3 withGameType:(NSMutableArray*)gameType;
-(void)gameVariantTypeSelection:(NSString *)gameTypeVariant withGameType:(NSString *)gameType;

- (IBAction)showGamesList:(id)sender;
- (IBAction)showChipsList:(id)sender;
- (IBAction)showBetList:(id)sender;
- (IBAction)showPlayersList:(id)sender;
- (IBAction)showVariantList:(id)sender;

- (void)hideAnyListwithRefresh:(BOOL)isRefresh;

- (IBAction)backClicked:(id)sender;
- (IBAction)refreshClicked:(id)sender;
- (IBAction)goClicked:(id)sender;
- (void)alreadyFilter:(BOOL)boolValue withGameType:(GameTypeStruct)gametype withPlayerType:(PlayerTypeStruct)playerType withBetType:(BetTypeStruct)betType withTableCost:(TableCostEnum)tableCost withViewTye:(ViewTypeEnum)viewType;

- (void)showNoOfTableLive:(NSString *)noOfTable;
- (void)showNoOfPlayerLive:(NSString *)noOfPlayer;

@end

@protocol TAJLobbyViewControllerDelegate <NSObject>

//ask can join table
- (BOOL)canJoinView;

// if already 2 table is joined
- (void)loadGameTable:(NSDictionary *)tableDictionary;

//if already one table is joined
- (BOOL)ifSingleTableJoined:(NSDictionary *)tableDictionary;
- (void)canJoinSingleTable;

//back button clicks
- (void)lobbyTableBackButtonClicked;


@end
