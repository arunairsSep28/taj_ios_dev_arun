/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLobbyViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Shrinidhi Rao on 24/04/14.
 **/

#import "TAJLobbyViewController.h"
#import "TAJConstants.h"
#import "TAJGameEngine.h"
#import "TAJAppDelegate.h"
#import "TAJUtilities.h"
#include "TAJLobby.h"
#import "TAJTableViewController.h"
#import "BannersCollectionViewCell.h"

#define ANIM_DURATION 0.3f
#define LOBBY_FONT_NAME    @"AvantGardeITCbyBT-Demi"

@interface TAJLobbyViewController ()<TAJTableViewControllerDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIImageView *topImageView;
@property (weak, nonatomic) IBOutlet UIView *lobbyOptionBar;


// live feed
@property (weak, nonatomic) IBOutlet UILabel *playerCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableCountLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (nonatomic)  BOOL animating;
@property (assign)  BOOL isListTableRequested;

//favourite list
@property (strong, nonatomic) NSMutableArray *arrayFavouriteTables;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;


//new
@property (weak, nonatomic) IBOutlet UIView *viewCashType;
@property (weak, nonatomic) IBOutlet UIView *viewFreeType;
@property (weak, nonatomic) IBOutlet UIView *viewFavouritesType;
@property (weak, nonatomic) IBOutlet UIView *viewTounamentsType;

@property (weak, nonatomic) IBOutlet UILabel *lblCashType;
@property (weak, nonatomic) IBOutlet UILabel *lblFreeType;
@property (weak, nonatomic) IBOutlet UILabel *lblFavouritesType;
@property (weak, nonatomic) IBOutlet UILabel *lblTournamentsType;


@property (weak, nonatomic) IBOutlet UIView *viewPonits;
@property (weak, nonatomic) IBOutlet UIView *viewPools;
@property (weak, nonatomic) IBOutlet UIView *viewDeals;

@property (weak, nonatomic) IBOutlet UILabel *lblPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblPools;
@property (weak, nonatomic) IBOutlet UILabel *lblDeals;

@property BOOL isFreeGames;
@property BOOL isCahsGames;
@property BOOL isTournaments;
@property BOOL isFavouriteGames;

@property BOOL isPoints;
@property BOOL isPools;
@property BOOL isDeals;

@property (weak, nonatomic) IBOutlet UIView *viewPointsRummy;
@property (weak, nonatomic) IBOutlet UIButton *btnTwoPlayers;
@property (weak, nonatomic) IBOutlet UIButton *btnSixPlayers;

@property BOOL isTwoPlayers;
@property BOOL isSixPlayers;

@property (weak, nonatomic) IBOutlet UIView *viewPoolsRummy;
@property (weak, nonatomic) IBOutlet UIButton *btnPoolsTwoPlayers;
@property (weak, nonatomic) IBOutlet UIButton *btnPoolsSixPlayers;
@property (weak, nonatomic) IBOutlet UIButton *btnPools101Players;
@property (weak, nonatomic) IBOutlet UIButton *btnPools201Players;

@property BOOL isPoolsTwoPlayers;
@property BOOL isPoolsSixPlayers;
@property BOOL isPools101Players;
@property BOOL isPools201Players;

@property (weak, nonatomic) IBOutlet UIView *viewDealsRummy;
@property (weak, nonatomic) IBOutlet UIButton *btnDealsTwoPlayers;
@property (weak, nonatomic) IBOutlet UIButton *btnDealsSixPlayers;
@property (weak, nonatomic) IBOutlet UIButton *btnDealsBestOfTwo;
@property (weak, nonatomic) IBOutlet UIButton *btnDealsBestOfThree;

@property BOOL isDealsTwoPlayers;
@property BOOL isDealsSixPlayers;

@property BOOL isDealsBestOfTwoGames;
@property BOOL isDealsBestOfThreeGames;

//Single Click
@property (strong, nonatomic) NSMutableArray *arrayPoolsBetList;
@property (strong, nonatomic) NSArray *sortedArrayPools;
@property (strong, nonatomic) NSArray *sortedArrayPoints;
@property (strong, nonatomic) NSArray *sortedArrayDeals;


@property (weak, nonatomic) IBOutlet UISlider *sliderPoints;
@property (weak, nonatomic) IBOutlet UISlider *sliderPools;
@property (weak, nonatomic) IBOutlet UISlider *sliderDeals;



//values slider chnaged

@property (weak, nonatomic) IBOutlet UILabel *lblPoolsEntryFee;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsValue;
@property (weak, nonatomic) IBOutlet UILabel *lblPointsEntryFee;
@property (weak, nonatomic) IBOutlet UILabel *lblDealsEntryFee;

//FILTER
@property (strong, nonatomic) NSString *tableCost, *tableType;
@property (assign, nonatomic) int playerType;

@end

@implementation TAJLobbyViewController

static NSString *const kCellIdentifierProducts = @"cellIdentifierProducts";

@synthesize arrayFilter = _arrayFilter;
@synthesize tournamentDetailView,alertView,isTournament,listArray,selectedIndexDeals,selectedIndexPoints,selectedIndexPools,gameVariantTypeStr,gameCostTypeStr;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.isFilteredData = NO;
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Lobby Screen"];
    
    [self.gameTypeButton addTarget:self
                            action:@selector(chooseGameFromList:)
                  forControlEvents:UIControlEventTouchUpInside];
    
    self.tableViewController = [[TAJTableViewController alloc] initWithNibName:@"TAJTableViewController" bundle:nil];
    // Objective-C
    // self.tableView.tableFooterView = [[UIView alloc] initWithFrame:CGRectZero];
    
    tourneyDetailView = [[[UINib nibWithNibName:@"TournamentDetailView" bundle:nil] instantiateWithOwner: self options:nil] objectAtIndex:0];
    [TAJAppDelegate setScreenSize : tourneyDetailView fromView : tournamentDetailView];
    self.tournamentView = [[[UINib nibWithNibName:@"TournamentLobbyView" bundle:nil] instantiateWithOwner: self options:nil] objectAtIndex:0];
    
    // [TAJAppDelegate setScreenSize : tournamentView fromView : tournamentDetailView];
    
    //    }
    
    self.gameSelectListIndex=[[NSMutableArray alloc]init];
    self.variantSelectListIndex=[[NSMutableArray alloc]init];
    self.chipsSelectListIndex=[[NSMutableArray alloc]init];
    self.betSelectListIndex=[[NSMutableArray alloc]init];
    self.playerSelectListIndex=[[NSMutableArray alloc]init];
    self.versionLabel.text = VERSION_NUMBER;
    self.tableViewController.tableDelegate = self;
    
    [TAJAppDelegate setScreenSize : self.tableViewController.view fromView : self.gameListView];//ReDIM Changes
    [self.gameListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    [self.gameListView addSubview:self.tableViewController.view];
    
    // Do any additional setup after loading the view from its nib.
    
    [self.variantButton.titleLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.variantButton.titleLabel.font.pointSize]];
    [self.gameTypeButton.titleLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.gameTypeButton.titleLabel.font.pointSize]];
    [self.chipsButton.titleLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.chipsButton.titleLabel.font.pointSize]];
    [self.betButton.titleLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.betButton.titleLabel.font.pointSize]];
    [self.playersButton.titleLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.playersButton.titleLabel.font.pointSize]];
    
    [self.variantssButtonLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.variantssButtonLabel.font.pointSize]];
    [self.gametypeButtonLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.gametypeButtonLabel.font.pointSize]];
    [self.chipsButtonLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.chipsButtonLabel.font.pointSize]];
    [self.betButtonLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.betButtonLabel.font.pointSize]];
    [self.playersButtonLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:self.playersButtonLabel.font.pointSize]];
    
    [self goClicked:self.goButton];
    // [self addScreenObserver];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closePopUp:)
                                                 name:@"closePopUp"
                                               object:nil];
    
    [self configureView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self removeScreenObserver];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.tableViewController.isClicked = YES;
    tournamentDetailView.hidden = YES;
    [self addScreenObserver];
    
    if ([self.tableViewController.listGameTable numberOfRowsInSection:0 ] == 0)
    {
        if (self.emptyTableNSTimer != nil)
        {
            [self.emptyTableNSTimer invalidate];
            self.emptyTableNSTimer = nil;
        }
        self.emptyTableNSTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                                selector:@selector(updateRefreshTimer:)
                                                                userInfo:nil repeats:YES];
    }
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self hideAnyListwithRefresh:NO];
    [self removeScreenObserver];
    if(tourneyDetailView){
        [tourneyDetailView clearData];
        [tourneyDetailView hideAlert];
    }
}


- (void)configureView {
    
    // Register Collection view cells
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BannersCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kCellIdentifierProducts];
    
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = 4;
    
    [self.collectionView reloadData];
    
    [self.sliderPools setValue:0 animated:YES];
    [self.sliderPools addTarget:self action:@selector(poolSliderAction:) forControlEvents:UIControlEventValueChanged];

    [self.sliderPoints setValue:0 animated:YES];
    [self.sliderPoints addTarget:self action:@selector(pointsSliderAction:) forControlEvents:UIControlEventValueChanged];
    
    [self.sliderDeals setValue:0 animated:YES];
    [self.sliderDeals addTarget:self action:@selector(dealsSliderAction:) forControlEvents:UIControlEventValueChanged];
    
//    [self setCashGames];
//    //[self setPoints];
//    [self setPools];
//    [self setPoolsTwoPlayers];
//    [self setPools101Players];
    self.selectedIndexPoints = 0;
    self.selectedIndexPools = 0;
    self.selectedIndexDeals = 0;
    
   // gameCostTypeStr = @"";
    //gameVariantTypeStr = @"";
    NSLog(@"CHECK GAME COST : %@",gameCostTypeStr);
    NSLog(@"CHECK GAME TYPE : %@",gameVariantTypeStr);
}

//Success login
- (void)closePopUp:(NSNotification *) notification
{
    NSLog(@"DEPOSIT CHECK");
    NSString *uniqueID = [[NSUserDefaults standardUserDefaults] objectForKey:UNIQUE_SESSION_KEY];
    NSString * url = [NSString stringWithFormat:@"%@%@?client_type=ios&device_type=%@&unique_id=%@",BASE_URL,SENDPAYMENTREQUEST,[[TAJUtilities sharedUtilities] getDeviceType],uniqueID];
    
    [self openURLWithString:url];
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}

- (void)addScreenObserver{
    [self removeScreenObserver];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorEvent:)
                                                 name:ERROR_NOTIFICATION
                                               object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(requestJoinTable:)
    //                                                 name:REQUEST_JOIN_TABLE
    //                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopRegistration:)
                                                 name:STOP_REGISTRATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stopCancelRegistration:)
                                                 name:STOP_CANCEL_REGISTRATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleGameQuit:)
                                                 name:STATE_BLOCKED_REPLY object:nil];
}

- (void)removeScreenObserver{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REQUEST_JOIN_TABLE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STOP_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STOP_CANCEL_REGISTRATION object:nil];
}

- (void)showPlayersStatusOnTable:(NSNotification *)notification
{
    //    dispatch_async(dispatch_get_main_queue(), ^{
    //
    //        if (notification) {
    //            NSLog(@"DATA!!! %@",[notification object]);
    //        }
    //        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_PLAYERS object:[notification object]];
    //        [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_PLAYERS object:nil];
    //    });
    
}

- (void)errorEvent:(NSNotification *)notification
{
    NSLog(@"CHECKING ERROR");
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self processErrorEvent:[dictionary[kTaj_Error_code] intValue]];
}

- (void)requestJoinTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [[TAJGameEngine sharedGameEngine] requestJoinTableReply:dictionary[@"TAJ_msg_uuid"]];
    [[TAJGameEngine sharedGameEngine] getTableDetailsAndJoinTableAsPlayTournament:dictionary];
    
    // [self performSelector:@selector(loadTournamentGame:) withObject:dictionary afterDelay:2];
}

- (void)loadTournamentGame:(NSDictionary*)dictionary{
    //[self loadGameTable:dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:MOVETOGAMESCREEN object:dictionary];
}

- (void)stopRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    /*{
     "TAJ_event_name" = "stop_registration";
     "TAJ_msg_uuid" = "ded9a882-0bc7-11e8-85a9-1cc1de039106";
     "TAJ_name" = event;
     "TAJ_timestamp" = "1517981370.0";
     "TAJ_tournament_id" = 3037;
     }*/
    //Registrations closed
}
- (void)stopCancelRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    /*{
     "TAJ_event_name" = "stop_cancel_registration";
     "TAJ_msg_uuid" = "ccf805e6-0bc7-11e8-85a9-1cc1de039106";
     "TAJ_name" = event;
     "TAJ_timestamp" = "1517981340.0";
     "TAJ_tournament_id" = 3037;
     }*/
    //time to cancel registrations has ended
}

- (void)processErrorEvent:(int)errorCode{
    switch (errorCode){
        case eAlreadyRegistered:
        case eAlreadyWaiting:
        case eNotEligible:
        case eNotRegistered:
        case eRegistrationFull:
        case eRegistrationNotOpen:
        case eRegistrationClosed:
        case eYouCantCancel:
        case eCurrentlyNotAvailable:
        case eDontHaveCashOrLoyalty:
        case eNeedDeposit1:
        case eNeedDeposit2:
        case eNeedDeposit3:
        case eNeedDeposit4:
        case eSufficientLoyaltyPoints:
        case eVerifyEmail:
        case eVerifyMobile:
        case eVerifyMobileAndEmail:
        case eFirstTimeFund:
        case eOnlyFreePlayer:
        case eYouNeedMaximumDeposit:
        case eVIPRegiNotThere:
        case eVIPRegiFull:
            //        case eENGINE_UNDER_MAINTENANCE:
            //            [self showErrorAlert:locate(eENGINE_UNDER_MAINTENANCE)];
            //            break;
        case eSTATE_BLOCKED:
            [self showErrorAlert:locate(errorCode)];
            break;
        case eVIPCodeInvalid:
            [self showErrorAlert:locate(errorCode)];
            break;
        case eNoTournament:
            [self showErrorAlert:locate(errorCode)];
            [self.tournamentView tournamnetNotYetCreated];
            break;
        default:
            break;
    }
}

- (void)showErrorAlert:(NSString *)message
{
    if (!alertView){
        alertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_SERVER_ERROR_TAG popupButtonType:eOK_Type message:message];
        [alertView show];
    }
}

- (void)showStateBlockedErrorAlert:(NSString *)message
{
    if (!alertView){
        NSLog(@"NOT ALERT");
        alertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_STATE_BLOCKED_ERROR_TAG popupButtonType:eOK_Type message:message];
        [alertView show];
    }
}

-(void) updateRefreshTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateRefreshTimer ");
#endif
    
    if ([self.tableViewController.listGameTable numberOfRowsInSection:0 ] == 0)
    {
        [self reloadTableViewScreen];
    }
    else
    {
        [self.emptyTableNSTimer invalidate];
        self.emptyTableNSTimer = nil;
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)showNoOfTableLive:(NSString *)noOfTable
{
    self.tableCountLabel.text = [NSString stringWithFormat:LIVE_TAJ_TABLE_FEED,noOfTable];
}

- (void)showNoOfPlayerLive:(NSString *)noOfPlayer
{
    self.playerCountLabel.text = [NSString stringWithFormat:LIVE_TAJ_PLAYERS_FEED,noOfPlayer];
}

- (void)reloadLobbyScreen
{
    if(isTournament) {
        [self.tableViewController removeAllTableViewData];
        //[self.tableViewController.view removeFromSuperview];
        [self.gameListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        // self.tableViewController.tableDelegate = nil;
        
        // self.tournamentView = (TournamentLobbyView*)[TournamentLobbyView getView];
        [TAJAppDelegate setScreenSize : self.tournamentView fromView : self.gameListView];
        self.tournamentView.chipArray = [NSArray arrayWithObjects:@"FREE",@"CASH",nil];
        // self.tournamentView.chipArray = [NSArray arrayWithArray:self.chipsSelectListIndex];
        self.tournamentView.tournamentDelegate = self;
        [self.tournamentView initView];
        [self.gameListView addSubview:self.tournamentView];
        self.tableViewController.tableDelegate = nil;
        
    } else {
        [self.tableViewController removeAllTableViewData];
        // if(!self.tableViewController.tableDelegate) { //ReDIM Changes
        [self.gameListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
        self.tableViewController.tableDelegate = self;
        [TAJAppDelegate setScreenSize : self.tableViewController.view fromView : self.gameListView];
        [self.gameListView addSubview:self.tableViewController.view];
        self.tournamentView.tournamentDelegate = nil;
        // }
    }
    
    self.tableViewController.isClicked = YES;
    [[TAJGameEngine sharedGameEngine] performSelector:@selector(requestListTable) withObject:nil afterDelay:0.5];
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    engine.isListTableRequested = YES;
    [self.tableViewController startAnimatingActivityIndicator];
    [self.tableViewController reloadTableView];
    
    self.gameSelectList = nil;
    self.variantSelectList = nil;
    self.chipsSelectList = nil;
    self.playerSelectList = nil;
    self.betSelectList = nil;
}

- (void)reloadTableViewScreen
{
    self.tableViewController.isClicked = YES;
    [self.tableViewController reloadTableView];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
    // Configure the cell...
    return cell;
}

- (void)chooseGameFromList:(id)sender
{
    
}

- (void)loadTheView
{
    _topOptionsView=[[UIView alloc] initWithFrame:CGRectMake(15 ,2,self.view.frame.size.height-82,self.view.frame.size.width-80)];
    [_topOptionsView setBackgroundColor:[UIColor redColor]];
    [self.view addSubview:self.topOptionsView];
    // Do any additional setup after loading the view from its nib.
}

- (void)loadTableview
{
    CGFloat x = 100;
    CGFloat y = 50;
    CGFloat width = self.view.frame.size.width;
    CGFloat height = self.view.frame.size.height*0.7f;
    CGRect tableFrame = CGRectMake(x, y, width, height);
    
    UITableView *tableView = [[UITableView alloc]initWithFrame:tableFrame style:UITableViewStylePlain];
    
    tableView.rowHeight = 45;
    tableView.sectionFooterHeight = 22;
    tableView.sectionHeaderHeight = 22;
    tableView.scrollEnabled = YES;
    tableView.showsVerticalScrollIndicator = YES;
    tableView.userInteractionEnabled = YES;
    tableView.bounces = YES;
    
    tableView.delegate = self;
    tableView.dataSource = self;
    [self.view addSubview:tableView];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self removeScreenObserver];
    // Dispose of any resources that can be recreated.
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    [self hideAnyListwithRefresh:YES];
}

- (IBAction)showGamesList:(id)sender
{
    if(self.gameSelectList == nil)
    {
        [self hideAnyListwithRefresh:YES];
    }
    
    NSMutableArray * arr = [NSMutableArray array];
    NSArray * tempArray=[NSArray array] ;
    CGFloat f = 110;
    
    for (NSString*  string in self.variantSelectListIndex)
    {
        if ([string compare:POOLS_LABEL_LOBBY]==0)
        {
            tempArray = [NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, TWO_NOT_ONE_LABEL_LOBBY,nil];//BEST_OF_THREE_LABEL_LOBBY
            f = 110;
        }
        else if([string compare:DEALS_LABEL_LOBBY]==0)
        {
            tempArray = [NSArray arrayWithObjects:BEST_OF_TWO_LABEL_LOBBY,BEST_OF_THREE_LABEL_LOBBY,BEST_OF_SIX_LABEL_LOBBY,nil];
            f = 80;
        }
        else if([string compare:STRIKES_LABEL_LOBBY]==0)
        {
            tempArray = [NSArray arrayWithObjects:GAME_JOKER_LOBBY, GAME_NO_JOKER_LOBBY,nil];
            f = 80;
        }
//        else if([string compare:FAVOURITES_LABEL_LOBBY]==0)
//        {
//            tempArray = [NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, TWO_NOT_ONE_LABEL_LOBBY,BEST_OF_TWO_LABEL_LOBBY,BEST_OF_THREE_LABEL_LOBBY,BEST_OF_SIX_LABEL_LOBBY,GAME_JOKER_LOBBY, GAME_NO_JOKER_LOBBY,nil];
//            f = 80;
//        }

        [arr addObjectsFromArray:tempArray];
    }
    
    NSArray * arrImage = [[NSArray alloc] init];
    
    if(self.gameSelectList == nil)
    {
        
        self.gameSelectList = [[NIDropDown alloc] showDropDown:self.gameTypeButton :&f :arr :arrImage :@"down" selectedIndices:self.gameSelectListIndex withTitle:GAME_TYPE_LOBBY];
        self.gameSelectList.delegate = self;
        
    }
    else
    {
        [self.gameSelectList hideDropDown:self.gameTypeButton duration:ANIM_DURATION refreshView:YES];
        self.gameSelectList = nil;
    }
    
}

- (IBAction)showChipsList:(id)sender
{
    
    if(self.chipsSelectList == nil)
    {
        [self hideAnyListwithRefresh:YES];
    }
    
    NSArray * arr = [[NSArray alloc] init];
#if LIVE_RELEASE
    arr = [NSArray arrayWithObjects:FREE_GAME_LOBBY, nil];
#else
    arr = [NSArray arrayWithObjects:FREE_GAME_LOBBY, CASH_GAME_LOBBY, nil];
    //arr = [NSArray arrayWithObjects:FREE_GAME_LOBBY, nil];
    
#endif
    NSArray * arrImage = [[NSArray alloc] init];
    
    if(_chipsSelectList == nil)
    {
        CGFloat f = 80;
        
        _chipsSelectList = [[NIDropDown alloc] showDropDown:self.chipsButton :&f :arr :arrImage :@"down" selectedIndices:self.chipsSelectListIndex withTitle:@"Chips"];
        _chipsSelectList.delegate = self;
    }
    else
    {
        [_chipsSelectList hideDropDown:self.chipsButton duration:ANIM_DURATION refreshView:YES];
        _chipsSelectList = nil;
    }
    
}

- (IBAction)showBetList:(id)sender
{
    if(self.betSelectList == nil)
    {
        [self hideAnyListwithRefresh:YES];
    }
    
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:LOW_BET_LABEL_LOBBY, MEDIUM_BET_LABEL_LOBBY, HIGH_BET_LABEL_LOBBY,nil];
    NSArray * arrImage = [[NSArray alloc] init];
    
    if(_betSelectList == nil)
    {
        
        CGFloat f = 110;
        _betSelectList = [[NIDropDown alloc]showDropDown:self.betButton :&f :arr :arrImage :@"down" selectedIndices:self.betSelectListIndex withTitle:@"Bet"];
        _betSelectList.delegate = self;
        
    }
    else
    {
        [_betSelectList hideDropDown:self.betButton duration:ANIM_DURATION refreshView:YES];
        _betSelectList = nil;
    }
}

- (IBAction)showPlayersList:(id)sender
{
    if(self.playerSelectList == nil)
    {
        [self hideAnyListwithRefresh:YES];
    }
    
    NSArray * arr = [[NSArray alloc] init];
    arr = [NSArray arrayWithObjects:TWO_PLAYER_LABEL, SIX_PLAYER_LABEL,nil];
    NSArray * arrImage = [[NSArray alloc] init];
    if(_playerSelectList == nil)
    {
        CGFloat f = 70;
        
        _playerSelectList = [[NIDropDown alloc]showDropDown:self.playersButton :&f :arr :arrImage :@"down" selectedIndices:self.playerSelectListIndex withTitle:@"Players"];
        _playerSelectList.delegate = self;
    }
    else
    {
        [_playerSelectList hideDropDown:self.playersButton  duration:ANIM_DURATION refreshView:YES];
        _playerSelectList = nil;
    }
}

- (IBAction)showVariantList:(id)sender
{
    if(self.variantSelectList == nil)
    {
        [self hideAnyListwithRefresh:YES];
    }
    
    NSArray * arr = [NSArray array];
    //arr = [NSArray arrayWithObjects:POOLS_LABEL_LOBBY, DEALS_LABEL_LOBBY, STRIKES_LABEL_LOBBY,FAVOURITES_LABEL_LOBBY, TOURNAMENTS_LABEL_LOBBY,nil];
        arr = [NSArray arrayWithObjects:POOLS_LABEL_LOBBY, DEALS_LABEL_LOBBY, STRIKES_LABEL_LOBBY, nil];
    NSArray * arrImage = [[NSArray alloc] init];
    if(_variantSelectList == nil)
    {
        CGFloat f = 110;
        _variantSelectList = [[NIDropDown alloc]showDropDown:self.variantButton :&f :arr :arrImage :@"down" selectedIndices:self.variantSelectListIndex withTitle:@"Game Variants"];
        _variantSelectList.delegate = self;
    }
    else
    {
        [_variantSelectList hideDropDown:self.variantButton duration:ANIM_DURATION refreshView:YES];
        _variantSelectList = nil;
    }
}

-(void)gameVariantTypeSelection:(NSString *)gameTypeVariant withGameType:(NSString *)gameType{
    
    gameVariantTypeStr = gameTypeVariant;
    gameCostTypeStr = gameType;
    
    NSLog(@"gameTypeSelectionStr : %@",gameCostTypeStr);
    NSLog(@"gameVariantTypeStr : %@",gameVariantTypeStr);//pools-deals-points
}

- (void)gameVariantTypeSelection:(UIButton *)sender withSelection:(NSMutableArray*)gameTypeVariant
{
    NSArray * arr = [NSArray array];
    arr = [NSArray arrayWithObjects:POOLS_LABEL_LOBBY, DEALS_LABEL_LOBBY, STRIKES_LABEL_LOBBY,TOURNAMENTS_LABEL_LOBBY,nil];

    //arr = [NSArray arrayWithObjects:POOLS_LABEL_LOBBY, DEALS_LABEL_LOBBY, STRIKES_LABEL_LOBBY,FAVOURITES_LABEL_LOBBY,TOURNAMENTS_LABEL_LOBBY,nil];
    NSArray * arrImage = [[NSArray alloc] init];
    if(self.variantSelectList == nil)
    {
        CGFloat f = 110;
        self.variantSelectList = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down" selectedIndices:self.variantSelectListIndex withTitle:@"Game Variants"];
        self.variantSelectList.delegate = self;
    }
    else
    {
        [self.variantSelectList hideDropDown:sender duration:ANIM_DURATION refreshView:YES];
    }
    
    [self.variantSelectList hideDropDown:sender duration:ANIM_DURATION refreshView:YES];
    
    self.variantSelectList.selectedIndex = gameTypeVariant;
    self.variantSelectListIndex= gameTypeVariant;
    
    if ([gameTypeVariant count]==1)
    {
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:POOLS_LABEL_LOBBY];
        }
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:DEALS_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:DEALS_LABEL_LOBBY];
        }
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:STRIKES_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:STRIKES_LABEL_LOBBY];
        }
//        if ([gameTypeVariant containsObject:[NSString stringWithFormat:FAVOURITES_LABEL_LOBBY]]) {
//            [self.variantSelectList selectSelection:FAVOURITES_LABEL_LOBBY];
//        }
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:TOURNAMENTS_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:TOURNAMENTS_LABEL_LOBBY];
        }
    }
    
    [self goClicked:self.goButton];
}

- (void)hideAnyListwithRefresh:(BOOL)isRefresh
{
    if (self.variantSelectList!=nil)
    {
        [self.variantSelectList hideDropDown:self.variantButton  duration:ANIM_DURATION refreshView:isRefresh];
        self.variantSelectList=nil;
        DLog(@"Variant");
        
    }
    
    if (self.playerSelectList!=nil)
    {
        [self.playerSelectList hideDropDown:self.playersButton  duration:ANIM_DURATION refreshView:isRefresh];
        self.playerSelectList=nil;
        DLog(@"Player");
    }
    
    if (self.betSelectList!=nil)
    {
        [self.betSelectList hideDropDown:self.betButton duration:ANIM_DURATION refreshView:isRefresh];
        self.betSelectList=nil;
        DLog(@"Bet");
    }
    
    if (self.chipsSelectList!=nil)
    {
        [self.chipsSelectList hideDropDown:self.chipsButton duration:ANIM_DURATION refreshView:isRefresh];
        self.chipsSelectList=nil;
        DLog(@"Chips");
    }
    
    
    if (self.gameSelectList!=nil)
    {
        [self.gameSelectList hideDropDown:self.gameTypeButton duration:ANIM_DURATION refreshView:isRefresh];
        self.gameSelectList=nil;
        DLog(@"Gametype");
    }
}

- (void)gameVariantTypeSelection:(UIButton *)sender withSelection:(NSMutableArray*)gameTypeVariant chipsButton:(UIButton *)sender2 withChips:(NSMutableArray*)chipsType gameButton:(UIButton *)sender3 withGameType:(NSMutableArray*)gameType
{
    NSLog(@"chips :%@",chipsType);
    NSLog(@"gameTypeVariant :%@",gameTypeVariant);
    NSLog(@"gameType :%@",gameType);
    
    NSArray * arr = [NSArray array];
    
    arr = [NSArray arrayWithObjects:POOLS_LABEL_LOBBY, DEALS_LABEL_LOBBY, STRIKES_LABEL_LOBBY,TOURNAMENTS_LABEL_LOBBY,nil];

    //arr = [NSArray arrayWithObjects:POOLS_LABEL_LOBBY, DEALS_LABEL_LOBBY, STRIKES_LABEL_LOBBY,FAVOURITES_LABEL_LOBBY,TOURNAMENTS_LABEL_LOBBY,nil];
    NSArray * arrImage = [[NSArray alloc] init];
    
    if(self.variantSelectList == nil)
    {
        CGFloat f = 110;
        self.variantSelectList = [[NIDropDown alloc]showDropDown:sender :&f :arr :arrImage :@"down" selectedIndices:self.variantSelectListIndex withTitle:@"Game Variants"];
        self.variantSelectList.delegate = self;
    }
    else
    {
        [self.variantSelectList hideDropDown:sender duration:0.0f refreshView:NO];
    }
    
    [self.variantSelectList hideDropDown:sender duration:0.0f refreshView:NO];
    
    self.variantSelectList.selectedIndex = gameTypeVariant;
    self.variantSelectListIndex= gameTypeVariant;
    
    if ([gameTypeVariant count]==1)
    {
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:POOLS_LABEL_LOBBY];
        }
        
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:DEALS_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:DEALS_LABEL_LOBBY];
        }
        
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:STRIKES_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:STRIKES_LABEL_LOBBY];
        }
        
//        if ([gameTypeVariant containsObject:[NSString stringWithFormat:FAVOURITES_LABEL_LOBBY]])
//        {
//            [self.variantSelectList selectSelection:FAVOURITES_LABEL_LOBBY];
//        }
        
        if ([gameTypeVariant containsObject:[NSString stringWithFormat:TOURNAMENTS_LABEL_LOBBY]])
        {
            [self.variantSelectList selectSelection:TOURNAMENTS_LABEL_LOBBY];
        }
    }
    
    NSArray * arr2 = [[NSArray alloc] init];
#if LIVE_RELEASE
    arr2 = [NSArray arrayWithObjects:FREE_GAME_LOBBY, nil];
#else
    arr2 = [NSArray arrayWithObjects:FREE_GAME_LOBBY, CASH_GAME_LOBBY, nil];
    //arr2 = [NSArray arrayWithObjects:FREE_GAME_LOBBY, nil];
    
#endif
    NSArray * arrImage2 = [[NSArray alloc] init];
    if(_chipsSelectList == nil)
    {
        CGFloat f = 80;
        
        _chipsSelectList = [[NIDropDown alloc]showDropDown:sender2 :&f :arr2 :arrImage2 :@"down" selectedIndices:self.chipsSelectListIndex withTitle:@"Chips"] ;
        _chipsSelectList.delegate = self;
    }
    else
    {
        [_chipsSelectList hideDropDown:sender2 duration:0.0f refreshView:NO];
        _chipsSelectList = nil;
    }
    [_chipsSelectList hideDropDown:sender2 duration:0.0f refreshView:NO];
    
    self.chipsSelectList.selectedIndex = chipsType;
    self.chipsSelectListIndex=chipsType;
    
    if ([chipsType count]==1)
    {
#if LIVE_RELEASE
        if ([chipsType containsObject:[NSString stringWithFormat:FREE_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:FREE_GAME_LOBBY];
        }
        
#else
        if ([chipsType containsObject:[NSString stringWithFormat:FREE_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:FREE_GAME_LOBBY];
        }
        
        
        if ([chipsType containsObject:[NSString stringWithFormat:CASH_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:CASH_GAME_LOBBY];
        }
        
        if ([chipsType containsObject:[NSString stringWithFormat:VIP_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:VIP_GAME_LOBBY];
        }
        
        if ([chipsType containsObject:[NSString stringWithFormat:BLAZE_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:BLAZE_GAME_LOBBY];
        }
        
        if ([chipsType containsObject:[NSString stringWithFormat:HAPPY_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:HAPPY_GAME_LOBBY];
        }
        
#endif
    } else {
        if ([chipsType containsObject:[NSString stringWithFormat:FREE_GAME_LOBBY]] && [chipsType containsObject:[NSString stringWithFormat:CASH_GAME_LOBBY]])
        {
            [self.chipsSelectList selectSelection:@"Any"];
        }
    }
    
    NSArray * arrImage3 = [[NSArray alloc] init];
    NSMutableArray * arr3 = [NSMutableArray array];
    NSArray * tempArray3=[NSArray array] ;
    CGFloat f = 110;
    
    for (NSString*  string in self.variantSelectListIndex)
    {
        if ([string compare:POOLS_LABEL_LOBBY]==0)
        {
            tempArray3 = [NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, TWO_NOT_ONE_LABEL_LOBBY,nil];//BEST_OF_THREE_LABEL_LOBBY
            f = 110;
        }
        else if([string compare:DEALS_LABEL_LOBBY]==0)
        {
            tempArray3 = [NSArray arrayWithObjects:BEST_OF_TWO_LABEL_LOBBY,BEST_OF_THREE_LABEL_LOBBY,BEST_OF_SIX_LABEL_LOBBY,nil];
            f = 80;
        }
        else if([string compare:STRIKES_LABEL_LOBBY]==0)
        {
            tempArray3 = [NSArray arrayWithObjects:GAME_JOKER_LOBBY, GAME_NO_JOKER_LOBBY,nil];
            f = 80;
        }
//        else if([string compare:FAVOURITES_LABEL_LOBBY]==0)
//        {
//            /*[NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, TWO_NOT_ONE_LABEL_LOBBY,BEST_OF_TWO_LABEL_LOBBY,BEST_OF_THREE_LABEL_LOBBY,BEST_OF_SIX_LABEL_LOBBY,GAME_JOKER_LOBBY, GAME_NO_JOKER_LOBBY,nil]*/
//            tempArray3 = [NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, nil];
//            f = 80;
//        }
        
        [arr3 addObjectsFromArray:tempArray3];
    }
    self.gameSelectList = nil;
    if(self.gameSelectList == nil)
    {
        
        self.gameSelectList = [[NIDropDown alloc]showDropDown:sender3 :&f :arr3 :arrImage3 :@"down" selectedIndices:self.gameSelectListIndex withTitle:GAME_TYPE_LABEL];
        self.gameSelectList.delegate = self;
    }
    else
    {
        [self.gameSelectList hideDropDown:sender3 duration:0.0f refreshView:NO];
        self.gameSelectList = nil;
    }
    
    self.gameSelectList.selectedIndex = gameType;
    self.gameSelectListIndex= gameType;
    [self.gameSelectList hideDropDown:sender3 duration:0.0f refreshView:NO];
    if ([gameType count]==1)
    {
        [self.gameSelectList selectSelection:[gameType objectAtIndex:0]];
    }
    [self goClicked:self.goButton];
}

- (IBAction)backClicked:(id)sender
{
    //lingam
    if(!tournamentDetailView.hidden){
        if(tourneyDetailView){
            [tourneyDetailView clearData];
        }
        tournamentDetailView.hidden = YES;
        [tournamentDetailView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    }else{
        [self hideAnyListwithRefresh:YES ];
        [self.view endEditing:YES];
        [self lobbyTableBackButtonClicked];
    }
    
}

- (IBAction)refreshClicked:(id)sender
{
    [self goClicked:self.goButton];
    // an ivar for your class:
}


- (IBAction)goClicked:(id)sender
{
    NSArray *array = NULL;
#if LIVE_RELEASE
    _tableTypeSearch.isFree = NO;
#else
    _tableTypeSearch.isFree = NO;
    _tableCostSearch=eCashGame;
    _tableTypeSearch.isCash = NO;
    _tableTypeSearch.isVIP = NO;
    _tableTypeSearch.isBlaze = NO;
    _tableTypeSearch.isHappy = NO;
#endif
    for (NSString*  string in self.chipsSelectListIndex)
    {
#if LIVE_RELEASE
        if([self.chipsSelectListIndex containsObject:[NSString stringWithFormat:FREE_GAME_LOBBY]])
        {
            _tableTypeSearch.isFree = YES;
            
        }
#else
        if([self.chipsSelectListIndex containsObject:[NSString stringWithFormat:FREE_GAME_LOBBY]])
        {
            _tableTypeSearch.isFree = YES;
            
        }
        if ([self.chipsSelectListIndex containsObject:[NSString stringWithFormat:CASH_GAME_LOBBY]])
        {
            _tableTypeSearch.isCash = YES;
            
        }
        if ([self.chipsSelectListIndex containsObject:[NSString stringWithFormat:VIP_GAME_LOBBY]])
        {
            _tableTypeSearch.isVIP = YES;
            
        }
        if([self.chipsSelectListIndex containsObject:[NSString stringWithFormat:BLAZE_GAME_LOBBY]])
        {
            _tableTypeSearch.isBlaze = YES;
            
        }
        if([self.chipsSelectListIndex containsObject:[NSString stringWithFormat:HAPPY_GAME_LOBBY]])
        {
            _tableTypeSearch.isHappy = YES;
            
        }
        
        
#endif
    }
    
    
    if ([self.chipsSelectListIndex count]<=0)
    {
#if LIVE_RELEASE
        
        _tableTypeSearch.isFree = YES;
        _tableTypeSearch.isCash = NO;
        _tableTypeSearch.isVIP = NO;
        _tableTypeSearch.isBlaze = NO;
        _tableTypeSearch.isHappy = NO;
        
        
        
#else
        _tableTypeSearch.isFree = YES;
        _tableTypeSearch.isCash = YES;
        _tableTypeSearch.isVIP = NO;
        _tableTypeSearch.isBlaze = NO;
        _tableTypeSearch.isHappy = NO;
#endif
    }
    _gameTypeSearch.isOneNotOne = NO;
    _gameTypeSearch.isTwoNotOne = NO;
    _gameTypeSearch.isBestOfThree = NO;
    _gameTypeSearch.isBestOfTwo = NO;
    _gameTypeSearch.isBestOfSix = NO;
    _gameTypeSearch.isJoker = NO;
    _gameTypeSearch.isNoJoker = NO;
    //[self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]]
    if (([self.gameSelectListIndex containsObject:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY]] || [self.gameSelectListIndex containsObject:[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY]]) && ![self.variantSelectListIndex containsObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]])
    {
        if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY]];
            
        }
        
        if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY]];
            
        }
        
        //        if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]])
        //        {
        //            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]];
        //
        //        }
    }
    
    if (([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY]] || [self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY]] || [self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]]) && ![self.variantSelectListIndex containsObject:[NSString stringWithFormat:DEALS_LABEL_LOBBY]])
    {
        if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY]];
            
        }
        if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]];
            
        }
        if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY]];
        }
        
    }
    
    if (([self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_JOKER_LOBBY]] || [self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_NO_JOKER_LOBBY]]) && ![self.variantSelectListIndex containsObject:[NSString stringWithFormat:STRIKES_LABEL_LOBBY]])
    {
        if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_JOKER_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:GAME_JOKER_LOBBY]];
            
        }
        
        if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_NO_JOKER_LOBBY]])
        {
            [self.gameSelectListIndex removeObject:[NSString stringWithFormat:GAME_NO_JOKER_LOBBY]];
            
        }
        
    }
    
    
    if ([self.variantSelectListIndex count]>0)
    {
        for (NSString*  string in self.variantSelectListIndex)
        {
            
            DLog(@"Game Select List Index%@",self.gameSelectListIndex);
            
            if([self.variantSelectListIndex containsObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]])
            {
                if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY]])
                {
                    _gameTypeSearch.isOneNotOne = YES;
                    
                }
                
                if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY]])
                {
                    _gameTypeSearch.isTwoNotOne = YES;
                    
                }
                
                //                if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]])
                //                {
                //                    _gameTypeSearch.isBestOfThree = YES;
                //
                //                }
                
                
                
                if([self.gameSelectListIndex count]==0)
                {
                    _gameTypeSearch.isOneNotOne = YES;
                    _gameTypeSearch.isTwoNotOne = YES;
                    // _gameTypeSearch.isBestOfThree = YES;
                }
                
            }
            
            
            if([self.variantSelectListIndex containsObject:[NSString stringWithFormat:DEALS_LABEL_LOBBY]])
            {
                if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY]])
                {
                    _gameTypeSearch.isBestOfTwo = YES;
                    
                }
                if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]])
                {
                    _gameTypeSearch.isBestOfThree = YES;
                    
                }
                if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY]])
                {
                    _gameTypeSearch.isBestOfSix = YES;
                    
                }
                
                if (![self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY]] && ![self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]] && ![self.gameSelectListIndex containsObject:[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY]] && [self.gameSelectListIndex count]==0)
                {
                    _gameTypeSearch.isBestOfTwo = YES;
                    _gameTypeSearch.isBestOfThree = YES;
                    _gameTypeSearch.isBestOfSix = YES;
                }
                
                if([self.gameSelectListIndex count]==0)
                {
                    _gameTypeSearch.isBestOfTwo = YES;
                    _gameTypeSearch.isBestOfThree = YES;
                    _gameTypeSearch.isBestOfSix = YES;
                }
            }
            
            
            if([self.variantSelectListIndex containsObject:[NSString stringWithFormat:STRIKES_LABEL_LOBBY]])
            {
                if ([self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_JOKER_LOBBY]])
                {
                    _gameTypeSearch.isJoker = YES;
                }
                if([self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_NO_JOKER_LOBBY]])
                {
                    _gameTypeSearch.isNoJoker = YES;
                }
                
                if (![self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_JOKER_LOBBY]] && ![self.gameSelectListIndex containsObject:[NSString stringWithFormat:GAME_NO_JOKER_LOBBY]] && [self.gameSelectListIndex count]==0)
                {
                    _gameTypeSearch.isJoker = YES;
                    _gameTypeSearch.isNoJoker = YES;
                }
                
                if([self.gameSelectListIndex count]==0)
                {
                    _gameTypeSearch.isJoker = YES;
                    _gameTypeSearch.isNoJoker = YES;
                }
                
            }
            
            
        }
    }
    else
    {
        _gameTypeSearch.isOneNotOne = YES;
        _gameTypeSearch.isTwoNotOne = YES;
        _gameTypeSearch.isBestOfThree = YES;
        _gameTypeSearch.isBestOfTwo = YES;
        _gameTypeSearch.isBestOfSix = YES;
        _gameTypeSearch.isJoker = YES;
        _gameTypeSearch.isNoJoker = YES;
    }
    _betTypeSearch.isLowBet = NO;
    _betTypeSearch.isMediumBet = NO;
    _betTypeSearch.isHighBet = NO;
    
    if ([self.betSelectListIndex count]>0)
    {
        for (NSString*  string in self.betSelectListIndex)
        {
            
            if ([self.betSelectListIndex containsObject:[NSString stringWithFormat:LOW_BET_LABEL_LOBBY]])
            {
                _betTypeSearch.isLowBet = YES;
            }
            if([self.betSelectListIndex containsObject:[NSString stringWithFormat:MEDIUM_BET_LABEL_LOBBY]])
            {
                _betTypeSearch.isMediumBet = YES;
                
            }
            if([self.betSelectListIndex containsObject:[NSString stringWithFormat:HIGH_BET_LABEL_LOBBY]])
            {
                _betTypeSearch.isHighBet = YES;
            }
        }
    }
    else
    {
        _betTypeSearch.isLowBet = YES;
        _betTypeSearch.isMediumBet = YES;
        _betTypeSearch.isHighBet = YES;
    }
    _playerTypeSearch.isTwoPlayer = NO;
    _playerTypeSearch.isSixPlayer = NO;
    
    
    if ([self.playerSelectListIndex count]>0)
    {
        for (NSString*  string in self.playerSelectListIndex)
        {
            if ([self.playerSelectListIndex containsObject:[NSString stringWithFormat:TWO_PLAYER_LABEL]])
            {
                _playerTypeSearch.isTwoPlayer = YES;
            }
            
            if([self.playerSelectListIndex containsObject:[NSString stringWithFormat:SIX_PLAYER_LABEL]])
            {
                _playerTypeSearch.isSixPlayer = YES;
                
            }
        }
        
    }
    else
    {
        _playerTypeSearch.isTwoPlayer = YES;
        _playerTypeSearch.isSixPlayer = YES;
    }
    _viewTypeSearch=ePlayGame;
    
    DLog(@"");
    array = [TAJUtilities getFilteredArrayForDictionary:[[[TAJGameEngine sharedGameEngine] lobby] tableDetails]
                                           withGameType:_gameTypeSearch
                                            withPlayers:_playerTypeSearch
                                            withBetType:_betTypeSearch
                                          withTableCost:_tableTypeSearch
                                           withViewType:_viewTypeSearch];
    
    self.arrayFilter = [array mutableCopy];
    
    NSLog(@"GAMES LIST : %@",self.tableViewController.tableArray);
    NSLog(@"GAMES LIST COUNT : %lu",(unsigned long)self.tableViewController.tableArray.count);
    
    if ([gameCostTypeStr isEqualToString:@"Cash"] && [gameVariantTypeStr isEqualToString:@"Pools"]) {
        //pools
        [self setCashGames];
        [self setPools];
        [self setPoolsTwoPlayers];
        [self setPools101Players];
        NSLog(@"NO VALUES IN STRING : CASH POOLS");
    }
    else if ([gameCostTypeStr isEqualToString:@"Free"] && [gameVariantTypeStr isEqualToString:@"Pools"]) {
        //pools
        [self setFreeGames];
        [self setPools];
        [self setPoolsTwoPlayers];
        [self setPools101Players];
        NSLog(@"NO VALUES IN STRING : FREE POOLS");
    }
    else if ([gameCostTypeStr isEqualToString:@"Cash"] && [gameVariantTypeStr isEqualToString:@"Deals"]) {
        //Deals
        [self setCashGames];
        [self setDeals];
        [self setDealsTwoPlayers];
        [self setDealsBestOfTwoGames];
        NSLog(@"NO VALUES IN STRING : CASH DEALS");
    }
    else if ([gameCostTypeStr isEqualToString:@"Free"] && [gameVariantTypeStr isEqualToString:@"Deals"]) {
        //Deals
        [self setFreeGames];
        [self setDeals];
        [self setDealsTwoPlayers];
        [self setDealsBestOfTwoGames];
        NSLog(@"NO VALUES IN STRING : FREE DEALS");
    }
    else if ([gameCostTypeStr isEqualToString:@"Cash"] && [gameVariantTypeStr isEqualToString:@"Points"]) {
        //Points
        [self setCashGames];
        [self setPoints];
        [self setPointsTwoPlayers];
        NSLog(@"NO VALUES IN STRING : CASH POINTS");
    }
    else if ([gameCostTypeStr isEqualToString:@"Free"] && [gameVariantTypeStr isEqualToString:@"Points"])  {
        //Points
        [self setFreeGames];
        [self setPoints];
        [self setPointsTwoPlayers];
        NSLog(@"NO VALUES IN STRING : FREE POINTS");
    }
    
    //NSLog(@"CHECK STRING : %@",[TAJUtilities checkIfStringIsEmpty:gameCostTypeStr]);

    else if (([gameCostTypeStr length] == 0) && ([gameVariantTypeStr length] == 0)) {
        [self setCashGames];
        [self setPoints];
        [self setPointsTwoPlayers];
        NSLog(@"NO VALUES IN STRING");
    }
    // NSLog(@"GAMES FOUND : %@",self.arrayFilter);
    
    //filter server data
    if ([self.arrayFilter count] > 0)
    {
        DLog(@"Games found");
        // NSLog(@"GAMES FOUND : %@",self.arrayFilter);
        
        [self.tableViewController alreadyFilter:YES withGameType:_gameTypeSearch
                                 withPlayerType:_playerTypeSearch
                                    withBetType:_betTypeSearch
                                  withTableCost:_tableTypeSearch withViewTye:_viewTypeSearch];
        
        self.tableViewController.filterdTableArray = [self.arrayFilter mutableCopy];
        NSLog(@"GAMES LIST : %@",self.arrayFilter);
        //NSLog(@"GAMES LIST COUNT ARRAY : %lu",(unsigned long)self.arrayFilter.count);
        //NSLog(@"GAMES LIST : %lu",(unsigned long)self.tableViewController.filterdTableArray.count);
        [self.tableViewController reloadTableView];
        
        /*
         self.arrayPoolsBetList = [NSMutableArray array];
         
         NSMutableArray * arrayPointsBetList = [NSMutableArray array];
         NSMutableArray * arrayDealsBetList = [NSMutableArray array];
         
         for (int i = 0; i < self.arrayFilter.count; i++)
         {
         NSMutableDictionary *tableDict = [NSMutableDictionary new];
         
         NSString * gameType = [self.arrayFilter valueForKey:@"TAJ_table_type"][i];
         
         if ([gameType isEqualToString:@"101_POOL"] || [gameType isEqualToString:@"201_POOL"]) {
         tableDict = self.arrayFilter[i];
         int bet = (int)[[tableDict objectForKey:@"TAJ_bet"] integerValue];
         NSLog(@"POOLS BET :%d",bet);
         
         [self.arrayPoolsBetList addObject:[NSNumber numberWithInteger:bet]];
         //[self.arrayPoolsBetList insertObject:[NSNumber numberWithInteger:bet] atIndex:i];
         //[self.arrayPoolsBetList insertObject:[NSNumber numberWithInteger:bet] atIndex:i];
         }
         
         
         if ([gameType isEqualToString:@"PR_JOKER"]) {
         tableDict = self.arrayFilter[i];
         int bet = (int)[[tableDict objectForKey:@"TAJ_bet"] integerValue];
         NSLog(@"POINTS BET :%d",bet);
         
         [arrayPointsBetList addObject:[NSNumber numberWithInteger:bet]];
         }
         
         if ([gameType isEqualToString:@"BEST_OF_2"] || [gameType isEqualToString:@"BEST_OF_3"]) {
         tableDict = self.arrayFilter[i];
         int bet = (int)[[tableDict objectForKey:@"TAJ_bet"] integerValue];
         NSLog(@"DEALS BET :%d",bet);
         
         [arrayDealsBetList addObject:[NSNumber numberWithInteger:bet]];
         }
         
         }
         NSLog(@"POOLS BET ARRAY : %@",self.arrayPoolsBetList);
         
         NSSet *poolsSet = [NSSet setWithArray:self.arrayPoolsBetList];
         //NSLog(@"TEl : %@",poolsSet);
         NSArray *poolsSetArray = [poolsSet allObjects];
         //NSLog(@"setArray : %@",poolsSetArray);
         
         self.sortedArrayPools = [poolsSetArray sortedArrayUsingDescriptors:
         @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
         ascending:YES]]];
         
         NSLog(@"sortedArrayPools : %@",self.sortedArrayPools);
         
         self.sliderPools.minimumValue = 0;
         self.sliderPools.maximumValue = self.sortedArrayPools.count;
         
         //POINTS
         NSLog(@"POOLS BET ARRAY : %@",arrayPointsBetList);
         NSSet *pointsSet = [NSSet setWithArray:arrayPointsBetList];
         //NSLog(@"TEl : %@",poolsSet);
         NSArray *pointsSetArray = [pointsSet allObjects];
         //NSLog(@"setArray : %@",poolsSetArray);
         
         self.sortedArrayPoints = [pointsSetArray sortedArrayUsingDescriptors:
         @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
         ascending:YES]]];
         
         NSLog(@"sorted Array POINTS : %@",self.sortedArrayPoints);
         
         self.sliderPoints.minimumValue = 0;
         self.sliderPoints.maximumValue = self.sortedArrayPoints.count;
         
         //DEALS
         NSLog(@"DEALS BET ARRAY : %@",arrayDealsBetList);
         NSSet *dealsSet = [NSSet setWithArray:arrayDealsBetList];
         //NSLog(@"TEl : %@",poolsSet);
         NSArray *dealsSetArray = [dealsSet allObjects];
         //NSLog(@"setArray : %@",poolsSetArray);
         
         self.sortedArrayDeals = [dealsSetArray sortedArrayUsingDescriptors:
         @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
         ascending:YES]]];
         
         NSLog(@"sorted Array DEALS : %@",self.sortedArrayDeals);
         
         self.sliderDeals.minimumValue = 0;
         self.sliderDeals.maximumValue = self.sortedArrayDeals.count;
         */
    }
    else
    {
        // if no game found while searching
        
        [self.tableViewController alreadyFilter:YES withGameType:_gameTypeSearch
                                 withPlayerType:_playerTypeSearch
                                    withBetType:_betTypeSearch
                                  withTableCost:_tableTypeSearch withViewTye:_viewTypeSearch];
        
        self.tableViewController.filterdTableArray = [self.arrayFilter mutableCopy];
        
        [self.tableViewController removeAllTableViewData];
        [self.tableViewController reloadTableView];
    }
    
}

- (void)niDropDownDelegateMethod: (NIDropDown *) sender ifPools:(BOOL)ifpool ifDeals:(BOOL)ifdeals ifPoints:(BOOL)ifpoints;
{
    isTournament = NO;//ReDIM Changes
    if ([sender isEqual:self.variantSelectList])
    {
        self.variantSelectListIndex=self.variantSelectList.selectedIndex;
        if(self.variantSelectList.selectedIndex.count == 0)
        {
            NSString *tempString=[NSString stringWithFormat:@"Select"];
            [self.gameTypeButton     setTitle:tempString forState:UIControlStateNormal];
        }
        
        NSArray * arrImage3 = [[NSArray alloc] init];
        CGFloat f = 110;
        
        NSMutableArray * arr3 = [NSMutableArray array];
        NSArray * tempArray3=[NSArray array] ;
        
        for (NSString*  string in self.variantSelectListIndex)
        {
            if ([string compare:POOLS_LABEL_LOBBY]==0)
            {
                tempArray3 = [NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, TWO_NOT_ONE_LABEL_LOBBY,nil];//BEST_OF_THREE_LABEL_LOBBY
                f = 110;
            }
            else if([string compare:DEALS_LABEL_LOBBY]==0)
            {
                tempArray3 = [NSArray arrayWithObjects:BEST_OF_TWO_LABEL_LOBBY,BEST_OF_THREE_LABEL_LOBBY, BEST_OF_SIX_LABEL_LOBBY,nil];
                f = 80;
            }
            else if([string compare:STRIKES_LABEL_LOBBY]==0)
            {
                tempArray3 = [NSArray arrayWithObjects:GAME_JOKER_LOBBY, GAME_NO_JOKER_LOBBY,nil];
                f = 80;
            }
            else if([string compare:TOURNAMENTS_LABEL_LOBBY]==0)//ReDIM Changes
            {
                isTournament = YES;
            }
            
            
            [arr3 addObjectsFromArray:tempArray3];
        }
        
        if(isTournament) {
            
        } else {
            
            if (ifpool)
            {
                [self.gameSelectListIndex addObjectsFromArray:[NSArray arrayWithObjects:ONE_NOT_ONE_LABEL_LOBBY, TWO_NOT_ONE_LABEL_LOBBY,nil]];//BEST_OF_THREE_LABEL_LOBBY
            }
            
            if (ifdeals)
            {
                [self.gameSelectListIndex addObjectsFromArray:[NSArray arrayWithObjects:BEST_OF_TWO_LABEL_LOBBY,BEST_OF_THREE_LABEL_LOBBY,BEST_OF_SIX_LABEL_LOBBY,nil]];
            }
            
            if (ifpoints)
            {
                [self.gameSelectListIndex addObjectsFromArray:[NSArray arrayWithObjects:GAME_JOKER_LOBBY, GAME_NO_JOKER_LOBBY,nil]];
            }
        }
        /*
         ReDIM Changes unwanted code
         self.gameSelectList = nil;
         
         if(self.gameSelectList == nil) {
         
         self.gameSelectList = [[NIDropDown alloc]showDropDown:self.gameTypeButton :&f :arr3 :arrImage3 :@"down" selectedIndices:self.gameSelectListIndex withTitle:GAME_TYPE_LABEL];
         self.gameSelectList.delegate = self;
         }
         else
         {
         [self.gameSelectList hideDropDown:self.gameTypeButton duration:0.0f refreshView:NO];
         self.gameSelectList = nil;
         }
         */
        
        [self.gameSelectList hideDropDown:self.gameTypeButton duration:0.0f refreshView:NO];
        
    }
    else if ([sender isEqual:self.playerSelectList])
    {
        // self.variantSelectListIndex=self.variantSelectList.selectedIndex;
        
        for (NSString*  string in self.variantSelectListIndex)
        {
            if([string compare:TOURNAMENTS_LABEL_LOBBY]==0)//ReDIM Changes
            {
                isTournament = YES;
            }
        }
        
        self.playerSelectListIndex=self.playerSelectList.selectedIndex;
        
        DLog(@"Player");
    }
    else if ([sender isEqual:self.betSelectList])
    {
        //self.variantSelectListIndex=self.variantSelectList.selectedIndex;
        
        for (NSString*  string in self.variantSelectListIndex)
        {
            if([string compare:TOURNAMENTS_LABEL_LOBBY]==0)//ReDIM Changes
            {
                isTournament = YES;
            }
        }
        
        self.betSelectListIndex=self.betSelectList.selectedIndex;
        
        DLog(@"Bet");
    }
    else if ([sender isEqual:self.chipsSelectList])
    {
        // self.variantSelectListIndex=self.variantSelectList.selectedIndex;
        
        for (NSString*  string in self.variantSelectListIndex)
        {
            if([string compare:TOURNAMENTS_LABEL_LOBBY]==0)//ReDIM Changes
            {
                isTournament = YES;
            }
        }
        
        self.chipsSelectListIndex=self.chipsSelectList.selectedIndex;
        
        DLog(@"Chips");
    }
    else if ([sender isEqual:self.gameSelectList])
    {
        DLog(@"Gametype %@ %@",self.gameSelectListIndex,self.gameSelectList.selectedIndex);
        //self.variantSelectListIndex=self.variantSelectList.selectedIndex;
        
        for (NSString*  string in self.variantSelectListIndex)
        {
            if([string compare:TOURNAMENTS_LABEL_LOBBY]==0)//ReDIM Changes
            {
                isTournament = YES;
            }
        }
        
        self.gameSelectListIndex = self.gameSelectList.selectedIndex;
        NSLog(@"SELECTED GAME TYPE : %@",self.gameSelectListIndex);
    }
    
    if(isTournament) {//ReDIM Changes
        
        if(!self.tournamentView.tournamentDelegate) {
            [self.tableViewController removeAllTableViewData];
            //[self.tableViewController.view removeFromSuperview];
            //self.tableViewController.tableDelegate = self;
            [self.gameListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            // self.tournamentView = (TournamentLobbyView*)[TournamentLobbyView getView];
            //self.tournamentView.chipArray = self.chipsSelectListIndex;
            [TAJAppDelegate setScreenSize : self.tournamentView fromView : self.gameListView];
            self.tournamentView.tournamentDelegate = self;
            //if(!self.tournamentView.listData){
            [self.tournamentView initView];
            //}
            [self.gameListView addSubview:self.tournamentView];
            
            self.tableViewController.tableDelegate = nil;
        }
        if(![self.tournamentView.chipArray isEqualToArray:self.chipsSelectListIndex]){
            self.tournamentView.chipArray = [NSArray arrayWithArray:self.chipsSelectListIndex];
            [self.tournamentView reloadTableView];
        }
        
    } else {
        
        if(!self.tableViewController.tableDelegate) {
            //[self.gameListView.subviews[0] removeFromSuperview];
            //  [self.gameListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            [self.gameListView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
            self.tableViewController.tableDelegate = self;
            [TAJAppDelegate setScreenSize : self.tableViewController.view fromView : self.gameListView];
            [self.gameListView addSubview:self.tableViewController.view];
            self.tournamentView.tournamentDelegate = nil;
        }
        
        [self goClicked:self.goButton];
    }
    //[self goClicked:self.goButton];
    
    [self hideAnyListwithRefresh:NO];
    sender=nil;
}

//lingam
-(void)addTournamentDetails:(NSDictionary *)dict{
    tournamentDetailView.hidden = NO;
    // tourneyDetailView = (TournamentDetailView*)[TournamentDetailView getView];
    //    tournamentView.tournamentDelegate = self;
    tourneyDetailView.lobbyView = self;
    tourneyDetailView.tourneyID = dict[@"TAJ_tourney_id"];
    [tourneyDetailView initView];
    [TAJAppDelegate setScreenSize : tourneyDetailView fromView : tournamentDetailView];
    [tournamentDetailView addSubview:tourneyDetailView];
}

- (void)yesButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_REGISTER_TAG:{
            [tourneyDetailView registerTournament];
        }
            break;
        case ALERT_DEREGISTER_TAG:{
            [tourneyDetailView deregisterTournament];
        }
            break;
            
        default:
            break;
    }
}

- (void)noButtonPressedForInfoPopup:(id)object
{
    //TAJInfoPopupViewController *conroller = object;
    //    switch (conroller.tag)
    //    {
    //        case 0:{
    //
    //        }
    //            break;
    //        case 1:{
    //
    //        }
    //            break;
    //        default:
    //            break;
    //    }
    [tourneyDetailView hideAlert];
}

- (void)okButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_SERVER_ERROR_TAG:{
            [alertView hide];
            alertView = nil;
        }
            break;
        case ALERT_STATE_BLOCKED_ERROR_TAG:{
            [alertView hide];
            alertView = nil;
            [self lobbyTableBackButtonClicked];
        }
            break;
            
        case REGISTER_SUCCESS_TAG:
        case DEREGISTER_SUCCESS_TAG:
        case STOP_REGISTRATION_TAG:
        case STOP_CANCEL_REGISTRATION_TAG:
        case TOURNAMENT_TO_START_TAG:
            [tourneyDetailView hideAlert];
            break;
        default:
            [alertView hide];
            alertView = nil;
            break;
    }
    // [tourneyDetailView hideAlert];
}

- (void)closeButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case 0:{
            
        }
            break;
            
        default:
            break;
    }
}

- (void)alreadyFilter:(BOOL)boolValue withGameType:(GameTypeStruct)gametype withPlayerType:(PlayerTypeStruct)playerType withBetType:(BetTypeStruct)betType withTableCost:(TableCostEnum)tableCost withViewTye:(ViewTypeEnum)viewType
{
    if (boolValue)
    {
        self.isFilteredData = boolValue;
        _gameTypeSearch = gametype;
        _playerTypeSearch = playerType;
        _betTypeSearch = betType;
        _tableCostSearch = tableCost;
        _viewTypeSearch = viewType;
    }
    else if (!boolValue)
    {
        self.isFilteredData = boolValue;
    }
}

#pragma mark - Table view delegate

- (BOOL)canJoinView {
    BOOL canJoion = YES;
    if (self.lobbyDelegate && [self.lobbyDelegate respondsToSelector:@selector(canJoinView)])
    {
        canJoion = [self.lobbyDelegate canJoinView];
    }
    return canJoion;
}

- (void)loadGameTable:(NSDictionary *)tableDictionary {
    //NSLog(@"LOBBY DATA :%@",tableDictionary);
    if (self.lobbyDelegate && [self.lobbyDelegate respondsToSelector:@selector(loadGameTable:)])
    {
        [self.lobbyDelegate loadGameTable:tableDictionary];
    }
}

- (BOOL)ifSingleTableJoined:(NSDictionary *)tableDictionary {
    BOOL canJoionSingleTable = NO;
    if (self.lobbyDelegate && [self.lobbyDelegate respondsToSelector:@selector(ifSingleTableJoined:)])
    {
        canJoionSingleTable = [self.lobbyDelegate ifSingleTableJoined:tableDictionary];
    }
    return canJoionSingleTable;
}

- (void)canJoinSingleTable {
    if (self.lobbyDelegate && [self.lobbyDelegate respondsToSelector:@selector(canJoinSingleTable)])
    {
        [self.lobbyDelegate canJoinSingleTable];
    }
}

- (void)lobbyTableBackButtonClicked  {
    if (self.lobbyDelegate && [self.lobbyDelegate respondsToSelector:@selector(lobbyTableBackButtonClicked)])
    {
        [self.lobbyDelegate lobbyTableBackButtonClicked];
    }
}

- (void)boldFontForLabel:(UILabel *)label{
    UIFont *currentFont = label.font;
    UIFont *newFont = [UIFont fontWithName:[NSString stringWithFormat:@"%@-Bold",currentFont.fontName] size:currentFont.pointSize];
    label.font = newFont;
}

- (void)dismisDropDownList {
    if(self.variantSelectList!=nil||self.variantSelectList!=nil||self.betSelectList!=nil||self.playerSelectList!=nil||self.chipsSelectList!=nil||self.gameSelectList!=nil)
    {
        [self hideAnyListwithRefresh:YES ];
    }
}


#pragma mark - CollectionView Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  4;//[self.arrayBaners count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = ([TAJUtilities screenWidth]);
#if DEBUG
    //NSLog(@"CellWidth = %f",width);
#endif
    
    if ([TAJUtilities isIPhone])
    {
        return CGSizeMake(width, 128);
    }
    else {
        return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    }
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    // T,L,B,R
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BannersCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifierProducts forIndexPath:indexPath];
    //[cell loadDataForCollectionView:[self.arrayCompanies objectAtIndex:indexPath.row]];
    self.pageControl.currentPage=indexPath.row;
    
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - ScrollView Delegate Methods

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = 0;
    
    if ([TAJUtilities isIPhone])
    {
        page = (int)(scrollView.contentOffset.x/(([TAJUtilities screenWidth])));
    }
    else {
        page = (int)(scrollView.contentOffset.x/((self.collectionView.frame.size.width)));
    }
    
    if (self.pageControl.currentPage != page) {
        self.pageControl.currentPage = page;
    }
}

#pragma mark - SingleClick Methods

-(void)setDefaultGameType{
    self.isCahsGames = NO;
    self.isFreeGames = NO;
    self.isTournaments = NO;
    self.isFavouriteGames = NO;
    
    self.viewCashType.backgroundColor = [UIColor whiteColor];
    self.viewFreeType.backgroundColor = [UIColor whiteColor];
    self.viewTounamentsType.backgroundColor = [UIColor whiteColor];
    self.viewFavouritesType.backgroundColor = [UIColor whiteColor];
    
    self.lblCashType.textColor = [UIColor blackColor];
    self.lblFreeType.textColor = [UIColor blackColor];
    self.lblTournamentsType.textColor = [UIColor blackColor];
    self.lblFavouritesType.textColor = [UIColor blackColor];
}

-(void)setCashGames {
    [self setDefaultGameType];
    self.lblCashType.textColor = [UIColor whiteColor];
    self.viewCashType.backgroundColor = [UIColor selectedGameTypeColor];
    self.tableCost = @"CASH_CASH";
    
    [self setPoints];
}

-(void)setFreeGames {
    [self setDefaultGameType];
    self.lblFreeType.textColor = [UIColor whiteColor];
    self.viewFreeType.backgroundColor = [UIColor selectedGameTypeColor];
    self.tableCost = @"FUNCHIPS_FUNCHIPS";
    
    [self setPoints];
}

-(void)setTournamentsGames {
    [self setDefaultGameType];
    self.lblTournamentsType.textColor = [UIColor whiteColor];
    self.viewTounamentsType.backgroundColor = [UIColor selectedGameTypeColor];
}

-(void)setFavouriteGames {
    [self setDefaultGameType];
    self.lblFavouritesType.textColor = [UIColor whiteColor];
    self.viewFavouritesType.backgroundColor = [UIColor selectedGameTypeColor];
}


- (IBAction)cashGamesClicked:(UIButton *)sender {
    [self setCashGames];
}
- (IBAction)freeGamesClicked:(UIButton *)sender {
    [self setFreeGames];
}

- (IBAction)tournamentGamesClicked:(UIButton *)sender {
    [self setTournamentsGames];
}

- (IBAction)favouritesGamesClikced:(UIButton *)sender {
    [self setFavouriteGames];
}

-(void)setDefaultGames{
    self.isPoints = NO;
    self.isPools = NO;
    self.isDeals = NO;
    
    self.viewPonits.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    self.viewPools.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    self.viewDeals.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.lblPoints.textColor = [UIColor whiteColor];
    self.lblPools.textColor = [UIColor whiteColor];
    self.lblDeals.textColor = [UIColor whiteColor];
    
    self.viewPointsRummy.hidden = YES;
    self.viewPoolsRummy.hidden = YES;
    self.viewDealsRummy.hidden = YES;
}

-(void)setPoints {
    [self setDefaultGames];
    self.isPoints = YES;
    self.lblPoints.textColor = [UIColor blackColor];
    self.viewPonits.backgroundColor = [UIColor whiteColor];
    self.viewPointsRummy.hidden = NO;
    
    [self setPointsTwoPlayers];
}

-(void)setPools {
    [self setDefaultGames];
    self.isPools = YES;
    self.lblPools.textColor = [UIColor blackColor];
    self.viewPools.backgroundColor = [UIColor whiteColor];
    self.viewPoolsRummy.hidden = NO;
    
    [self setPoolsTwoPlayers];
}

-(void)setDeals {
    [self setDefaultGames];
    self.isDeals = YES;
    self.lblDeals.textColor = [UIColor blackColor];
    self.viewDeals.backgroundColor = [UIColor whiteColor];
    self.viewDealsRummy.hidden = NO;

    [self setDealsTwoPlayers];
}

- (IBAction)pointsClikced:(UIButton *)sender {
    [self setPoints];
}

- (IBAction)poolsClikced:(UIButton *)sender {
    [self setPools];
}

- (IBAction)dealsClikced:(UIButton *)sender {
    [self setDeals];
}

-(void)setDefaultPointsPlayers {
    self.isTwoPlayers = NO;
    self.isSixPlayers = NO;
    
    self.btnTwoPlayers.backgroundColor = [UIColor clearColor];
    self.btnSixPlayers.backgroundColor = [UIColor clearColor];
    
    [self.btnTwoPlayers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnSixPlayers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)setPointsTwoPlayers {
    [self setDefaultPointsPlayers];
    [self.btnTwoPlayers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnTwoPlayers.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.playerType = 2;
    
    
//    NSMutableArray * listArray = [NSMutableArray new];
    
    listArray = [GamesFilter filterTablesWithTablesList:self.tableViewController.tableArray tableCost:self.tableCost tableType:self.tableType playersType:self.playerType];
    
    NSLog(@"FILTERED POINTS 6 ARRAY : %@",listArray);
    NSLog(@"FILTERED POINTS 6 ARRAY COUNT: %lu",(unsigned long)listArray.count);
    
    
    NSArray * sortedBetsList = [NSArray new];
    
    sortedBetsList = [GamesFilter sortWithList:listArray];
    
    self.sortedArrayPoints = [NSArray new];
    self.sortedArrayPoints = sortedBetsList;
    
    NSLog(@"SORTED POINTS 6 ARRAY : %@",self.sortedArrayPoints);
    
    self.sliderPoints.minimumValue = 0;
    self.sliderPoints.maximumValue = self.sortedArrayPoints.count;
    if (self.sortedArrayPoints.count > 0) {
        self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayPoints[0]];
    }
    
    [self.sliderPoints setValue:0 animated:YES];
}

-(void)setPointsSixPlayers {
    [self setDefaultPointsPlayers];
    [self.btnSixPlayers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnSixPlayers.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.playerType = 6;
    
//    NSMutableArray * listArray = [NSMutableArray new];
    
    listArray = [GamesFilter filterTablesWithTablesList:self.tableViewController.tableArray tableCost:self.tableCost tableType:self.tableType playersType:self.playerType];
    
    NSLog(@"FILTERED POINTS 6 ARRAY : %@",listArray);
    NSLog(@"FILTERED POINTS 6 ARRAY COUNT: %lu",(unsigned long)listArray.count);
    
    
    NSArray * sortedBetsList = [NSArray new];
    
    sortedBetsList = [GamesFilter sortWithList:listArray];
    
    self.sortedArrayPoints = [NSArray new];
    self.sortedArrayPoints = sortedBetsList;
    
    NSLog(@"SORTED POINTS 6 ARRAY : %@",self.sortedArrayPoints);
    
    self.sliderPoints.minimumValue = 0;
    self.sliderPoints.maximumValue = self.sortedArrayPoints.count;
    if (self.sortedArrayPoints.count > 0) {
        self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayPoints[0]];
    }
    
    [self.sliderPoints setValue:0 animated:YES];
}

- (IBAction)pointsTwoPlayersClikced:(UIButton *)sender {
    if (self.isPoints) {
    [self setPointsTwoPlayers];
    }
    else if (self.isPools) {
        [self setPoolsTwoPlayers];
    }
    else {
        [self setDealsTwoPlayers];
    }
}

- (IBAction)pointsSixPlayersClikced:(UIButton *)sender {
    if (self.isPoints) {
    [self setPointsSixPlayers];
    }
    else if (self.isPools) {
        [self setPoolsSixPlayers];
    }
    else {
        [self setDealsSixPlayers];
    }
}

-(void)setDefaultPoolsPlayers {
    self.isPoolsTwoPlayers = NO;
    self.isPoolsSixPlayers = NO;
    
    self.btnPoolsTwoPlayers.backgroundColor = [UIColor clearColor];
    self.btnPoolsSixPlayers.backgroundColor = [UIColor clearColor];
    
    [self.btnPoolsTwoPlayers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnPoolsSixPlayers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)setPoolsTwoPlayers {
    [self setDefaultPoolsPlayers];
    [self.btnPoolsTwoPlayers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnPoolsTwoPlayers.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.playerType = 2;
    
    [self setPools101Players];
}

-(void)setPoolsSixPlayers {
    [self setDefaultPoolsPlayers];
    [self.btnPoolsSixPlayers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnPoolsSixPlayers.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.playerType = 6;
    
    [self setPools101Players];
}

-(void)setDefaultPoolsPlayersType {
    self.isPools101Players = NO;
    self.isPools201Players = NO;
    
    self.btnPools101Players.backgroundColor = [UIColor clearColor];
    self.btnPools201Players.backgroundColor = [UIColor clearColor];
    
    [self.btnPools101Players setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnPools201Players setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
}

-(void)setPools101Players {
    [self setDefaultPoolsPlayersType];
    [self.btnPools101Players setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnPools101Players.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.tableType = @"101_POOL";
    
//    NSMutableArray * listArray = [NSMutableArray new];
    
    listArray = [GamesFilter filterTablesWithTablesList:self.tableViewController.tableArray tableCost:self.tableCost tableType:self.tableType playersType:self.playerType];
    
    NSLog(@"FILTERED 101 ARRAY : %@",listArray);
    NSLog(@"FILTERED 101 ARRAY COUNT: %lu",(unsigned long)listArray.count);
    
    NSArray * sortedBetsList = [NSArray new];
    
    sortedBetsList = [GamesFilter sortWithList:listArray];
    
    self.sortedArrayPools = [NSArray new];
    self.sortedArrayPools = sortedBetsList;
    
    NSLog(@"SORTED 101 ARRAY : %@",self.sortedArrayPools);
    
    self.sliderPools.minimumValue = 0;
    self.sliderPools.maximumValue = self.sortedArrayPools.count;
    if (self.sortedArrayPools.count > 0) {
        self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayPools[0]];
    }
    
    [self.sliderPools setValue:0 animated:YES];
}

-(void)setPools201Players {

    [self setDefaultPoolsPlayersType];
    [self.btnPools201Players setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnPools201Players.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.tableType = @"201_POOL";
    
//    NSMutableArray * listArray = [NSMutableArray new];
    
    listArray = [GamesFilter filterTablesWithTablesList:self.tableViewController.tableArray tableCost:self.tableCost tableType:self.tableType playersType:self.playerType];
    
    NSLog(@"FILTERED 201 ARRAY : %@",listArray);
    NSLog(@"FILTERED 201 ARRAY COUNT: %lu",(unsigned long)listArray.count);
    
    NSArray * sortedBetsList = [NSArray new];
    
    sortedBetsList = [GamesFilter sortWithList:listArray];
    
    self.sortedArrayPools = [NSArray new];
    self.sortedArrayPools = sortedBetsList;
    
    NSLog(@"SORTED 201 ARRAY : %@",self.sortedArrayPools);
    
    self.sliderPools.minimumValue = 0;
    self.sliderPools.maximumValue = self.sortedArrayPools.count;
    if (self.sortedArrayPools.count > 0) {
        self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayPools[0]];
    }
    
    [self.sliderPools setValue:0 animated:YES];
}

- (IBAction)pools101Clikced:(UIButton *)sender {
    [self setPools101Players];
}

- (IBAction)pools201Clikced:(UIButton *)sender {
    [self setPools201Players];
}

-(void)setDefaultDealsPlayers {
    self.isDealsTwoPlayers = NO;
    self.isDealsSixPlayers = NO;
    
    self.btnDealsTwoPlayers.backgroundColor = [UIColor clearColor];
    self.btnDealsSixPlayers.backgroundColor = [UIColor clearColor];
    
    [self.btnDealsTwoPlayers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnDealsSixPlayers setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}

-(void)setDealsTwoPlayers {
    [self setDefaultDealsPlayers];
    [self.btnDealsTwoPlayers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnDealsTwoPlayers.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.playerType = 2;
    
    [self setDealsBestOfTwoGames];
}

-(void)setDealsSixPlayers {
    [self setDefaultDealsPlayers];
    [self.btnDealsSixPlayers setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnDealsSixPlayers.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.playerType = 2;
    
    [self setDealsBestOfTwoGames];
}

-(void)setDefaultDealsGameTypes {
    self.isDealsBestOfTwoGames = NO;
    self.isDealsBestOfThreeGames = NO;
    
    self.btnDealsBestOfTwo.backgroundColor = [UIColor clearColor];
    self.btnDealsBestOfThree.backgroundColor = [UIColor clearColor];
    
    [self.btnDealsBestOfTwo setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    [self.btnDealsBestOfThree setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];

}

-(void)setDealsBestOfTwoGames {
    [self setDefaultDealsGameTypes];
    [self.btnDealsBestOfTwo setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnDealsBestOfTwo.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.tableType = @"BEST_OF_2";
    
//    NSMutableArray * listArray = [NSMutableArray new];
    
    listArray = [GamesFilter filterTablesWithTablesList:self.tableViewController.tableArray tableCost:self.tableCost tableType:self.tableType playersType:self.playerType];
    
    NSLog(@"FILTERED BESTOF2 ARRAY : %@",listArray);
    NSLog(@"FILTERED BESTOF2 ARRAY COUNT: %lu",(unsigned long)listArray.count);
    
    
    NSArray * sortedBetsList = [NSArray new];
    
    sortedBetsList = [GamesFilter sortWithList:listArray];
    
    self.sortedArrayDeals = [NSArray new];
    self.sortedArrayDeals = sortedBetsList;
    
    NSLog(@"SORTED BESTOF2 ARRAY : %@",self.sortedArrayDeals);
    
    self.sliderDeals.minimumValue = 0;
    self.sliderDeals.maximumValue = self.sortedArrayDeals.count;
    if (self.sortedArrayDeals.count > 0) {
        self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayDeals[0]];
    }
    
    [self.sliderDeals setValue:0 animated:YES];
}

-(void)setDealsBestOfThreeGames {
    [self setDefaultDealsGameTypes];
    [self.btnDealsBestOfThree setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    self.btnDealsBestOfThree.backgroundColor = [UIColor lobbySelectedGameTypeColor];
    
    self.tableType = @"BEST_OF_3";
    
//    NSMutableArray * listArray = [NSMutableArray new];
    
    listArray = [GamesFilter filterTablesWithTablesList:self.tableViewController.tableArray tableCost:self.tableCost tableType:self.tableType playersType:self.playerType];
    
    NSLog(@"FILTERED BESTOF3 ARRAY : %@",listArray);
    NSLog(@"FILTERED BESTOF3 ARRAY COUNT: %lu",(unsigned long)listArray.count);
    
    
    NSArray * sortedBetsList = [NSArray new];
    
    sortedBetsList = [GamesFilter sortWithList:listArray];
    
    self.sortedArrayDeals = [NSArray new];
    self.sortedArrayDeals = sortedBetsList;
    
    NSLog(@"SORTED BESTOF3 ARRAY : %@",self.sortedArrayDeals);
    
    self.sliderDeals.minimumValue = 0;
    self.sliderDeals.maximumValue = self.sortedArrayDeals.count;
    if (self.sortedArrayDeals.count > 0) {
        self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayDeals[0]];
    }
    
    [self.sliderDeals setValue:0 animated:YES];
    
}

- (IBAction)dealsBestOf2Clikced:(UIButton *)sender {
    [self setDealsBestOfTwoGames];
}

- (IBAction)dealsBestOf3Clikced:(UIButton *)sender {
    [self setDealsBestOfThreeGames];
}

- (IBAction)playNowClikced:(UIButton *)sender {
    
    
//    for (NSDictionary *dict in listArray) {
//        if ([[dict objectForKey:@""] isEqualToString:self.lblPoolsEntryFee.text]) {
//            // do service
//            dict
//            break;
//        }
//    }
    
    if (sender.tag == 10) {
        //Points
        NSIndexPath *path = [NSIndexPath indexPathForRow:selectedIndexPoints inSection:0];
        
        [self.tableViewController joinTableWithIndexPath:path selectedArray:listArray];
    }
    else if (sender.tag == 11) {
        //Pools
        NSIndexPath *path = [NSIndexPath indexPathForRow:selectedIndexPools inSection:0];
        
        [self.tableViewController joinTableWithIndexPath:path selectedArray:listArray];
    }
    else {
        //Deals
        NSIndexPath *path = [NSIndexPath indexPathForRow:selectedIndexDeals inSection:0];
        
        [self.tableViewController joinTableWithIndexPath:path selectedArray:listArray];
    }
    
}

- (void)poolSliderAction:(UISlider *)sender {
     selectedIndexPools = sender.value;
    NSLog(@"SLIDER VALUE : %f",sender.value);
    
    NSLog(@"POOLS SLIDER VALUE %d",(int)selectedIndexPools);
    if (selectedIndexPools >= self.sortedArrayPools.count) {
        selectedIndexPools = (int)self.sortedArrayPools.count - 1;
    }
    NSLog(@"SELECTED BET : %@",self.sortedArrayPools[selectedIndexPools]);
    self.lblPoolsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayPools[selectedIndexPools]];
}


- (void)pointsSliderAction:(UISlider *)sender {
    NSLog(@"POINTS SLIDER VALUE : %f",sender.value);
    selectedIndexPoints = sender.value;
    NSLog(@"POINTS SLIDER VALUE %d",(int)selectedIndexPoints);
    if (selectedIndexPoints >= self.sortedArrayPoints.count) {
        selectedIndexPoints = (int)self.sortedArrayPoints.count - 1;
    }
    NSLog(@"SELECTED POINTS BET : %@",self.sortedArrayPoints[selectedIndexPoints]);
    int entryFee = 80;
    int selectedPointsvalue = [self.sortedArrayPoints[selectedIndexPoints] intValue];
    NSLog(@"SELECTED POINTS Value : %d",selectedPointsvalue);
    
    NSLog(@"POINTS ENTRY FEE : %d",selectedPointsvalue * entryFee);
    
    self.lblPointsValue.text = [NSString stringWithFormat:@"%@",self.sortedArrayPoints[selectedIndexPoints]];
    self.lblPointsEntryFee.text = [NSString stringWithFormat:@"%d",selectedPointsvalue * entryFee];
}

- (void)dealsSliderAction:(UISlider *)sender {
    selectedIndexDeals = sender.value;
    NSLog(@"DEALS SLIDER VALUE : %f",sender.value);
    
    NSLog(@"DEALS SLIDER VALUE %d",(int)selectedIndexDeals);
    if (selectedIndexDeals >= self.sortedArrayDeals.count) {
        selectedIndexDeals = (int)self.sortedArrayDeals.count - 1;
    }
    NSLog(@"SELECTED DEAL : %@",self.sortedArrayDeals[selectedIndexDeals]);
    self.lblDealsEntryFee.text = [NSString stringWithFormat:@"%@",self.sortedArrayDeals[selectedIndexDeals]];
}

@end


