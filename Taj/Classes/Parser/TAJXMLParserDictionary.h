//
// XMLDictionary.h
//
// Version 1.3
//
// Created by Nick Lockwood on 15/11/2010.
// Copyright 2010 Charcoal Design. All rights reserved.
//
// Get the latest version of XMLDictionary from here:
//
// https://github.com/nicklockwood/XMLDictionary
//
// This software is provided 'as-is', without any express or implied
// warranty. In no event will the authors be held liable for any damages
// arising from the use of this software.
//
// Permission is granted to anyone to use this software for any purpose,
// including commercial applications, and to alter it and redistribute it
// freely, subject to the following restrictions:
//
// 1. The origin of this software must not be misrepresented; you must not
// claim that you wrote the original software. If you use this software
// in a product, an acknowledgment in the product documentation would be
// appreciated but is not required.
//
// 2. Altered source versions must be plainly marked as such, and must not be
// misrepresented as being the original software.
//
// 3. This notice may not be removed or altered from any source distribution.
//

#import <Foundation/Foundation.h>
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wobjc-missing-property-synthesis"


typedef NS_ENUM(NSInteger,  TAJXMLDictionaryAttributesMode)
{
     TAJXMLDictionaryAttributesModePrefixed = 0, //default
     TAJXMLDictionaryAttributesModeDictionary,
     TAJXMLDictionaryAttributesModeUnprefixed,
     TAJXMLDictionaryAttributesModeDiscard
};


typedef NS_ENUM(NSInteger,  TAJXMLDictionaryNodeNameMode)
{
     TAJXMLDictionaryNodeNameModeRootOnly = 0, //default
     TAJXMLDictionaryNodeNameModeAlways,
     TAJXMLDictionaryNodeNameModeNever
};


static NSString *const  TAJXMLDictionaryAttributesKey   = @"TAJ_attributes";
static NSString *const  TAJXMLDictionaryCommentsKey     = @"TAJ_comments";
static NSString *const  TAJXMLDictionaryTextKey         = @"TAJ_text";
static NSString *const  TAJXMLDictionaryNodeNameKey     = @"TAJ_name";
static NSString *const  TAJXMLDictionaryAttributePrefix = @"TAJ_";
 


@interface TAJXMLParserDictionary : NSObject <NSCopying>

+ (TAJXMLParserDictionary *)sharedInstance;

@property (nonatomic, assign) BOOL collapseTextNodes; // defaults to YES
@property (nonatomic, assign) BOOL stripEmptyNodes;   // defaults to YES
@property (nonatomic, assign) BOOL trimWhiteSpace;    // defaults to YES
@property (nonatomic, assign) BOOL alwaysUseArrays;   // defaults to NO
@property (nonatomic, assign) BOOL preserveComments;  // defaults to NO
@property (nonatomic, assign) BOOL wrapRootNode;      // defaults to NO

@property (nonatomic, assign)  TAJXMLDictionaryAttributesMode attributesMode;
@property (nonatomic, assign)  TAJXMLDictionaryNodeNameMode nodeNameMode;

- (NSDictionary *)dictionaryWithParser:(NSXMLParser *)parser;
- (NSDictionary *)dictionaryWithData:(NSData *)data;
- (NSDictionary *)dictionaryWithString:(NSString *)string;
- (NSDictionary *)dictionaryWithFile:(NSString *)path;

@end


@interface NSDictionary ( TAJXMLDictionary)

+ (NSDictionary *)dictionaryWithXMLParser:(NSXMLParser *)parser;
+ (NSDictionary *)dictionaryWithXMLData:(NSData *)data;
+ (NSDictionary *)dictionaryWithXMLString:(NSString *)string;
+ (NSDictionary *)dictionaryWithXMLFile:(NSString *)path;

- (NSDictionary *)attributes;
- (NSDictionary *)childNodes;
- (NSArray *)comments;
- (NSString *)nodeName;
- (NSString *)innerText;
- (NSString *)innerXML;
- (NSString *)XMLString;

- (NSArray *)arrayValueForKeyPath:(NSString *)keyPath;
- (NSString *)stringValueForKeyPath:(NSString *)keyPath;
- (NSDictionary *)dictionaryValueForKeyPath:(NSString *)keyPath;

@end


@interface NSString (XMLDictionary)

- (NSString *)XMLEncodedString;

@end

