/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJInfoPopupViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 21/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJConstants.h"

@protocol InfoPopupViewDelegate <NSObject>

//PopupType will be passed
@optional
-(void) yesButtonPressedForInfoPopup:(id)object;
-(void) noButtonPressedForInfoPopup:(id)object;
-(void) okButtonPressedForInfoPopup:(id)object;
-(void) closeButtonPressedForInfoPopup:(id)object;

@end



@interface TAJInfoPopupViewController : UIViewController
{
    
}

@property (nonatomic, weak) id<InfoPopupViewDelegate> delegate;// Delegate should always be a UIViewController
@property (nonatomic) EPopupButtonType buttonType;
@property (nonatomic) EPopupType popupType;
@property (weak, nonatomic) IBOutlet UIView *popupContainerView;


@property (weak, nonatomic) IBOutlet UIButton *yesButton;
@property (weak, nonatomic) IBOutlet UIButton *okButton;
@property (weak, nonatomic) IBOutlet UIButton *noButton;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UILabel *message;
@property (weak, nonatomic) IBOutlet UIView *mainView;
@property (weak, nonatomic) IBOutlet UIView *yesNoView;
@property (weak, nonatomic) IBOutlet UIView *okView;
@property (weak, nonatomic) IBOutlet UIView *okCancelView;
@property (strong, nonatomic) IBOutlet UIImageView *avatar;
@property (weak, nonatomic) IBOutlet UIView *depositView;

@property (strong, nonatomic) NSString *messageString;
@property (nonatomic)                  int tag;// default is 0

- (void) updateInfoPopupBasedOnTypes;

- (IBAction)yesPressed:(id)sender;
- (IBAction)okPressed:(id)sender;
- (IBAction)noPressed:(id)sender;
- (IBAction)closePressed:(id)sender;
-(void)frameSettingForContainerView;
//Strictly use this initializer method
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil delegate:(id<InfoPopupViewDelegate>) delegate tag:(NSInteger)tag popupButtonType:(EPopupButtonType)buttonType message:(NSString*)message;
- (void) initPopUp:(id<InfoPopupViewDelegate>) delegate tag:(NSInteger)tag popupButtonType:(EPopupButtonType)buttonType message:(NSString*)message;
- (void) show;
- (void) hide;

@end
