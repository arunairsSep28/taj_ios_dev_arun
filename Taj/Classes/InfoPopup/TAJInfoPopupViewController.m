/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJInfoPopupViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 21/04/14.
 **/

#import "TAJInfoPopupViewController.h"
#import "TAJUtilities.h"
#import "TAJAppDelegate.h"

@interface TAJInfoPopupViewController ()

@end

@implementation TAJInfoPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil delegate:(id<InfoPopupViewDelegate>) delegate tag:(NSInteger)tag popupButtonType:(EPopupButtonType)buttonType message:(NSString*)message
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        self.delegate = delegate;
        self.tag = tag;
        self.buttonType = buttonType;
        self.messageString = message;
    }
    return self;
}
-(void) initPopUp:(id<InfoPopupViewDelegate>) delegate tag:(NSInteger)tag popupButtonType:(EPopupButtonType)buttonType message:(NSString*)message
{
    if (self)
    {
        // Custom initialization
        self.delegate = delegate;
        self.tag = tag;
        self.buttonType = buttonType;
        self.messageString = message;
    }
}

-(id) initWithCoder:(NSCoder *)aDecoder
{
    self=[super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [TAJAppDelegate setScreenSizeForRes : self.view];
    //Update the popup
    [self updateInfoPopupBasedOnTypes];
    
    self.mainView.layer.cornerRadius = 10;
    self.mainView.layer.masksToBounds = true;
    
    self.message.text = self.messageString;
}

-(void)viewDidAppear:(BOOL)animated
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateInfoPopupBasedOnTypes
{
    switch (self.buttonType)
    {
        case eYes_No_Type:
            
//            [self.yesButton setTitle:YES_STRING_FOR_POPUP forState:UIControlStateNormal];
//            [self.noButton setTitle:NO_STRING_FOR_POPUP forState:UIControlStateNormal];
//            [self.yesButton setTitle:YES_STRING_FOR_POPUP forState:UIControlStateHighlighted];
//            [self.noButton setTitle:NO_STRING_FOR_POPUP forState:UIControlStateHighlighted];
//            self.okButton.hidden = YES;
            self.yesNoView.hidden = NO;
            self.okView.hidden = YES;
            self.okCancelView.hidden = YES;
            self.depositView.hidden = YES;
            break;
            
        case eOK_Cancel_Type:
//            [self.yesButton setTitle:OK_STRING_FOR_POPUP forState:UIControlStateNormal];
//            [self.yesButton setTitle:OK_STRING_FOR_POPUP forState:UIControlStateHighlighted];
//            [self.noButton setTitle:CANCEL_STRING forState:UIControlStateNormal];
//            [self.noButton setTitle:CANCEL_STRING forState:UIControlStateHighlighted];
//            self.okButton.hidden = YES;
            self.yesNoView.hidden = YES;
            self.okView.hidden = YES;
            self.okCancelView.hidden = NO;
            self.depositView.hidden = YES;
            break;
            
        case eOK_Type:
//            [self.okButton setTitle:OK_STRING_FOR_POPUP forState:UIControlStateNormal];
//            [self.okButton setTitle:OK_STRING_FOR_POPUP forState:UIControlStateHighlighted];
//            self.yesButton.hidden = YES;
//            self.noButton.hidden = YES;
            self.yesNoView.hidden = YES;
            self.okView.hidden = NO;
            self.okCancelView.hidden = YES;
            self.depositView.hidden = YES;
            break;
            
        case eDEPOSIT_Type:
    
            self.depositView.hidden = NO;
            self.yesNoView.hidden = YES;
            self.okView.hidden = YES;
            self.okCancelView.hidden = YES;
            break;
            
        case eNone_Type:
//            self.yesButton.hidden = YES;
//            self.noButton.hidden = YES;
//            self.okButton.hidden = YES;
//            self.closeButton.hidden = YES;
            self.yesNoView.hidden = YES;
            self.okView.hidden = YES;
            self.okCancelView.hidden = YES;
            self.depositView.hidden = YES;
            break;
            
        default:
            break;
    }
}

- (IBAction)yesPressed:(id)sender
{
    self.yesButton.userInteractionEnabled = NO;
    if (self.delegate)
    {
        [self.delegate yesButtonPressedForInfoPopup:self];
    }
}

- (IBAction)okPressed:(id)sender
{
    DLog(@"okPressed :okButtonPressedForInfoPopup");
    self.okButton.userInteractionEnabled = NO;
    
    if (self.delegate)
    {
        DLog(@"okPressed delegate :okButtonPressedForInfoPopup");
        
        [self.delegate okButtonPressedForInfoPopup:self];
    }
}

- (IBAction)noPressed:(id)sender
{
    self.noButton.userInteractionEnabled = NO;
    if (self.delegate)
    {
        [self.delegate noButtonPressedForInfoPopup:self];
    }
}

- (IBAction)closePressed:(id)sender
{
    self.closeButton.userInteractionEnabled = NO;
    if (self.delegate)
    {
        [self.delegate closeButtonPressedForInfoPopup:self];
    }
}

- (void)show
{
    UIViewController *controller = Nil;
    controller = (UIViewController*)self.delegate;
    [controller.view addSubview:self.view];
    
    [TAJAppDelegate setScreenSize: self.view];
    
    self.okButton.userInteractionEnabled = YES;
    self.yesButton.userInteractionEnabled = YES;
    self.noButton.userInteractionEnabled = YES;
    self.closeButton.userInteractionEnabled = YES;
}

- (void)frameSettingForContainerView
{
    CGRect alertRect = self.popupContainerView.frame;
    
    if([TAJUtilities isIPhone])
    {
        alertRect.origin.y +=100;
    }
    else
    {
        alertRect.origin.y += 250;
    }
    self.popupContainerView.frame = alertRect;
}

- (void)hide
{
    if (self.view)
    {
        [self.view removeFromSuperview];
    }
}

@end
