/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJHomePageController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Raghavendra Kamat on 09-04-2014.
 Created by Pradeep BM on 03/02/14.
 **/

#import "TAJHomePageViewController.h"
#import "TAJAppDelegate.h"
#import "TAJUtilities.h"
#import "TAJFilterViewController.h"
#import "TAJConstants.h"
#import "TAJGameEngine.h"
#import "TAJLoginHomeViewController.h"
#import "TAJTableViewController.h"
#import "TAJSwitchLobbyViewController.h"
#import "TAJGameEngine.h"
#import "TAJClientServerCommunicator.h"

@interface TAJHomePageViewController ()

@property(retain,nonatomic) TAJFilterViewController *nextFilterController;
@property (strong, nonatomic) TAJLoginHomeViewController *loginHomeViewController;
@property (strong, nonatomic) TAJSwitchLobbyViewController *switchLobbyController;

@property (strong, nonatomic) TAJAppDelegate *appDelegate;

@property (weak, nonatomic) IBOutlet UIButton *lobbyButton;
@property (weak, nonatomic) IBOutlet UIButton *seachButton;
@property (weak, nonatomic) IBOutlet UIButton *profileButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;

- (IBAction)gotoLobby:(UIButton *)sender;
- (IBAction)searchGame:(UIButton *)sender;
- (IBAction)profileManagement:(UIButton *)sender;
- (IBAction)userGameSetting:(UIButton *)sender;

@end

@implementation TAJHomePageViewController
@synthesize nextFilterController = _nextFilterController;
@synthesize prevController = _prevController;
@synthesize lobbyButton = _lobbyButton;
@synthesize seachButton = _seachButton;
@synthesize profileButton = _profileButton;
@synthesize settingsButton = _settingsButton;
@synthesize switchLobbyController = _switchLobbyController;
@synthesize profileController = _profileController;
@synthesize settingsViewController = _settingsViewController;

#pragma mark UIViewController Life Cycle

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.versionNumber.text = VERSION_NUMBER;
    
    self.title = NAV_HOME_TITLE;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(processGameListData:) name:REMOVE_STORE_CREDENTIAL_LOGIN_ACTIVITY object:nil];
    
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        // if iOS 7
        self.edgesForExtendedLayout = UIRectEdgeNone; //layout adjustements
    }
    
//#if USERNAME_PASSWORD_STORE_CHECK
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *username = Nil;
//    NSString *password = Nil;
//    username = [defaults objectForKey:USENAME_KEY];
//    password = [defaults objectForKey:PASSWORD_KEY];
//    
//    
//    if (username && password)
//    {
//        if ([[TAJUtilities sharedUtilities] isInternetConnected])
//        {
//            TAJUtilities *utility = [TAJUtilities sharedUtilities];
//            UIView *view = [utility activity];
//            CGRect windowSize = [[UIScreen mainScreen] bounds];
//            view.center = CGPointMake(windowSize.size.width * 0.75f, windowSize.size.height * 0.30f);
//            [self.view addSubview:view];
//            [utility showLoginActivity];
//            
//            //start game engine [login action]
//            [[TAJGameEngine sharedGameEngine] createUserModelWithUserName: username
//                                                                 password: password];
//            [[TAJClientServerCommunicator sharedClientServerCommunicator] createSocketAndConnect];
//            //[[TAJGameEngine sharedGameEngine] scheduleHeartBeat];
//            [[TAJGameEngine sharedGameEngine] setCurrentController:self];
//
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NO_INTERNET message:PLEASE_CHECK_YOUR_INTERNET_CONNECTION delegate:self cancelButtonTitle:OK_STRING otherButtonTitles:nil];
//            [alert show];
//        }
//        
//    }
//    else
//    {
//        [self performSelector:@selector(checkAlreadyLogedIn) withObject:nil afterDelay:1.0];
//    }
//    
//#else
//    
//      [self performSelector:@selector(checkAlreadyLogedIn) withObject:nil afterDelay:1.0];
//
//#endif
    UIBarButtonItem * logOutHome = [[UIBarButtonItem alloc]initWithTitle:@"Logout" style:UIBarButtonItemStylePlain target:self action:@selector(logOutFromHome)];
    self.navigationItem.rightBarButtonItem = logOutHome;
}

//Author : RK
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    
    if(NSOrderedSame == [title compare:OK_STRING])
    {
#if USERNAME_PASSWORD_STORE_CHECK
        // TODO: show the login screen with username and password
        [self performSelector:@selector(checkAlreadyLogedIn) withObject:nil afterDelay:1.0];
#endif
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

-(UIInterfaceOrientation)preferredInterfaceOrientationForPresentation
{
    return UIInterfaceOrientationLandscapeRight;
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationMaskLandscape;// | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)processGameListData:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self handleProcessGameListData];
}

- (void)handleProcessGameListData
{
    
}

#pragma mark Navigation bar event

-(void)logOutFromHome
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        [[TAJGameEngine sharedGameEngine] destroyUserNameAndPassword];
        [[TAJGameEngine sharedGameEngine] logout];
        [self checkAlreadyLogedIn];
    }
    else
    {
//        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:NO_INTERNET message:PLEASE_CHECK_YOUR_INTERNET_CONNECTION delegate:self cancelButtonTitle:OK_STRING otherButtonTitles:nil];
//        [alert show];
    }

}

#pragma mark Login Check

-(void)checkAlreadyLogedIn
{
    self.appDelegate = [[UIApplication sharedApplication] delegate];
    
    [self.appDelegate loadMainWindow];
}

#pragma mark Goto Lobby Action

- (IBAction)gotoLobby:(UIButton *)sender
{
    if ([TAJUtilities isIPhone])
    {
        self.switchLobbyController = [[TAJSwitchLobbyViewController alloc] init];
    }
    else
    {
        self.switchLobbyController = [[TAJSwitchLobbyViewController alloc] init];
    }
    
    [self.navigationController pushViewController:self.switchLobbyController animated:YES];
}

#pragma mark Search game Action

- (IBAction)searchGame:(UIButton *)sender
{
    if ([TAJUtilities isIPhone])
    {
        self.nextFilterController = [[TAJFilterViewController alloc] initWithNibName:@"TAJFilterViewController_iPhone" bundle:nil];
    }
    else
    {
        self.nextFilterController = [[TAJFilterViewController alloc] initWithNibName:@"TAJFilterViewController_iPad" bundle:nil];
    }
    [self.navigationController pushViewController:self.nextFilterController animated:YES];

}

#pragma mark Profile management Actions

- (IBAction)profileManagement:(UIButton *)sender
{
    self.profileController = [[TAJProfileViewController alloc] init];
    [self.navigationController pushViewController:self.profileController animated:YES];
}

#pragma mark User game settings

- (IBAction)userGameSetting:(UIButton *)sender
{
    self.settingsViewController = [[TAJSettingsViewController alloc] init];
    [self.navigationController pushViewController:self.settingsViewController animated:YES];
}

@end
