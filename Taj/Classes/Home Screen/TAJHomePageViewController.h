/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJHomePageController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Raghavendra Kamat on 09-04-2014.
 Created by Pradeep BM on 03/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJLoginViewController.h"
#import "TAJProfileViewController.h"
#import "TAJSettingsViewController.h"
#import "TAJBaseViewController.h"

@interface TAJHomePageViewController : TAJBaseViewController

@property(retain,nonatomic) TAJLoginViewController *prevController;
@property(retain,nonatomic) TAJProfileViewController *profileController;
@property(retain,nonatomic) TAJSettingsViewController *settingsViewController;
@property (retain, nonatomic) IBOutlet UILabel *versionNumber;

@end
