/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSupportButtonViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */



#import <UIKit/UIKit.h>
#import "TAJConstants.h"

@class TAJHomePageViewController;

@protocol TAJSupportButtonViewControllerDelegate;

@interface TAJSupportButtonViewController : UIViewController

@property (nonatomic, weak) id<TAJSupportButtonViewControllerDelegate> supportButtonDelegate;
- (BOOL)isSupportButtonViewVisible;

@end

@protocol TAJSupportButtonViewControllerDelegate <NSObject>

- (void)removeSupportButtonControllerView;
- (void)didPressedLogOutPressed;

@end
