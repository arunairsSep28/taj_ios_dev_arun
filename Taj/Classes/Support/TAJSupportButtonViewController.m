/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSupportButtonViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Pradeep BM on 12/05/14.
 Created by  Yogisha Poojary on 12/05/14.
 **/

#import "TAJSupportButtonViewController.h"
#import "TAJGameEngine.h"
#import "MoreTableViewCell.h"
#import "WebViewController.h"

@interface TAJSupportButtonViewController ()
@property (weak, nonatomic) IBOutlet UIView *supportButtonViewHolder;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayMenu;
@property (strong, nonatomic) NSString *managerStatus;
@property (assign, nonatomic) BOOL managerAvailability;
@property (strong, nonatomic) NSString *managerStatusDesp;
@property (strong, nonatomic) NSDictionary *managerInfoDict;


@end

@implementation TAJSupportButtonViewController

static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Support Screen"];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMoreButtonView)];
    tapGesture.numberOfTapsRequired = 1;
    [self.supportButtonViewHolder addGestureRecognizer:tapGesture];
    [self addObserver];
    
    
    // Register tableViewCell #import "SupportTableViewCell.h"
    [self.tableView registerNib:[UINib nibWithNibName:@"SupportTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
}


#pragma mark - HELPERS

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self getAccountManagerDetails];
}

- (void)addObserver
{
    [self removeObserver];
}

- (void)removeObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REQUEST_JOIN_TABLE object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self removeObserver];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self removeObserver];
}

- (BOOL)isSupportButtonViewVisible
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

-(void) getAccountManagerDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,ACCOUNT_MANAGER];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    //NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
           // NSLog(@"ACCOUNT_MANAGER Response :%@",responseData);
            NSDictionary * jsonDictionay = [responseData valueForKey:@"data"];
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                self.managerAvailability = YES;
                NSDictionary *dataDic = [jsonDictionay dictionaryRemovingNSNullValues];
                //NSLog(@"%@", dataDic);
                self.managerStatus = [dataDic valueForKey:@"manager_status"];
                self.managerInfoDict = dataDic;
                
                self.arrayMenu = @[
                                   @{
                                       @"title" : @"Help and Support",
                                       @"support_logo" : @"TR_FAQ",
                                       @"status" : @NO
                                       },
                                   @{
                                       @"title" : @"Email Us",
                                       @"support_logo" : @"TR_Mailus",
                                       @"status" : @NO
                                       },
                                   @{
                                       @"title" : @"Live Chat",
                                       @"support_logo" : @"TR_Voicsupport",
                                       @"status" : @NO
                                       },
                                   @{
                                       @"title" : @"Account Manager",
                                       @"support_logo" : @"customer_service",
                                       @"status" : @YES
                                       }
                                   ];
                
                [self.tableView reloadData];
            }
            else {
                self.managerAvailability = NO;
                self.managerStatusDesp = [jsonDictionay objectForKey:@"message"];
                self.arrayMenu = @[
                                   @{
                                       @"title" : @"Help and Support",
                                       @"support_logo" : @"TR_FAQ",
                                       @"status" : @NO
                                       },
                                   @{
                                       @"title" : @"Email Us",
                                       @"support_logo" : @"TR_Mailus",
                                       @"status" : @NO
                                       },
                                   @{
                                       @"title" : @"Live Chat",
                                       @"support_logo" : @"TR_Voicsupport",
                                       @"status" : @NO
                                       }
                                   ];
                [self.tableView reloadData];
                
                self.managerInfoDict = responseData;
            }
            
        });
        
    } errorBlock:^(NSString *errorString) {
        //NSLog(@"ACCOUNT_MANAGER ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

- (void)closeMoreButtonView
{
    if (self.supportButtonDelegate && [self.supportButtonDelegate respondsToSelector:@selector(removeMoreButtonControllerView)])
    {
        [self.supportButtonDelegate removeSupportButtonControllerView];
    }
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone])
    {
    return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    SupportTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithTitle:self.arrayMenu[indexPath.row] managerStatus:self.managerStatus];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        HelpViewController * viewController = [[HelpViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 1) {
        EmailUsViewController * viewController = [[EmailUsViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 2) {
        NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
        NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:USEREMAIL_KEY];
        NSString *mobile = [[NSUserDefaults standardUserDefaults] objectForKey:USERMOBILE_KEY];
        [ZDCChat updateVisitor:^(ZDCVisitorInfo *user) {
            user.phone = mobile;
            user.name = name;
            user.email = email;
        }];
        [ZDCChat startChatIn:self.navigationController withConfig:^(ZDCConfig *config) {
            config.preChatDataRequirements.name = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.email = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.phone = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.department = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.message = ZDCPreChatDataNotRequired;
            config.emailTranscriptAction = ZDCEmailTranscriptActionNeverSend;
        }];
    }
    else if (indexPath.row == 3) {
        AccountManagerViewController * viewController = [[AccountManagerViewController alloc] initWithInfoDict:self.managerInfoDict isAvailable:self.managerAvailability];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
}
    
@end
