/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJReportaBugView.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 12/06/14.
 Created by Yogisha Poojary on 06/05/14.
 **/

#import "GameInformationView.h"
#import "TAJUtilities.h"

@implementation GameInformationModel
@synthesize gameInformationDictionary = _gameInformationDictionary;

@end

#pragma mark - TAJReportaBugView

@interface GameInformationView()

@property (weak, nonatomic) IBOutlet UILabel *lblGameVariant;
@property (weak, nonatomic) IBOutlet UILabel *lblTableId;
@property (weak, nonatomic) IBOutlet UILabel *lblGameType;
@property (weak, nonatomic) IBOutlet UILabel *lblGameId;
@property (weak, nonatomic) IBOutlet UILabel *lblEntryFee;
@property (weak, nonatomic) IBOutlet UILabel *lblDrops;
@property (weak, nonatomic) IBOutlet UILabel *lblFullCount;
@property (weak, nonatomic) IBOutlet UILabel *lblTurnTimer;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoExtraTimer;


@property (strong, nonatomic) GameInformationModel *gameInformatonModel;


//@property CGRect selectDropDownViewFrame;
- (IBAction)gameInformationCloseButton_Pressed:(id)sender;

@end

@implementation GameInformationView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        //_isSelectButtonPressed = YES;
    }
    return self;
}

- (void)initializeTheGameInformation:(NSDictionary *)bugDictionary withGameId:(NSString *)gameId
{
    NSLog(@"GameInformation Dictionary= %@",bugDictionary);
    DLog(@"Bug Dictionary= %@",bugDictionary);
    self.gameInformatonModel = [[GameInformationModel alloc] init];
    self.gameInformatonModel.gameInformationDictionary = bugDictionary;
    
    self.lblTableId.text =[NSString stringWithFormat:@": %@",[self.gameInformatonModel.gameInformationDictionary valueForKeyPath:GAME_TABLE_ID_VALUE]];
    self.lblGameType.text = [NSString stringWithFormat:@": %@",[self.gameInformatonModel.gameInformationDictionary valueForKeyPath:@"table_details.TAJ_table_type"]];
    self.lblGameId.text = [NSString stringWithFormat:@": %@",gameId];
    
    self.lblGameVariant.text = [NSString stringWithFormat:@": %@",[self.gameInformatonModel.gameInformationDictionary valueForKeyPath:@"table_details.TAJ_table_type"]];

    self.lblEntryFee.text = [NSString stringWithFormat:@": %@",[self.gameInformatonModel.gameInformationDictionary valueForKeyPath:@"table_details.TAJ_table_type"]];
    
}

- (IBAction)gameInformationCloseButton_Pressed:(id)sender
{
    if (self.gameInformationDelegate && [self.gameInformationDelegate respondsToSelector:@selector(gameInfoCloseButton_p)])
    {
        [self.gameInformationDelegate gameInfoCloseButton_p];
    }
}




@end
