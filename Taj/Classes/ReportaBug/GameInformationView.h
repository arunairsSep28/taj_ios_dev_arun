/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJReportaBugView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 12/06/14.
 Created by Yogisha Poojary on 06/05/14.
 **/

#import <UIKit/UIKit.h>

@interface GameInformationModel : NSObject

@property(strong, nonatomic) NSDictionary *gameInformationDictionary;

@end

@protocol GameInformationViewDelegate;

@interface GameInformationView : UIView

@property(weak,nonatomic) id<GameInformationViewDelegate> gameInformationDelegate;
@property (weak, nonatomic) IBOutlet UIView *reportBugContainerView;

@property (weak, nonatomic) IBOutlet UIView *reportBugMainContainerView;

- (void)initializeTheGameInformation:(NSDictionary *)bugDictionary withGameId:(NSString *)gameId;

@end

@protocol GameInformationViewDelegate<NSObject>

- (void)gameInfoCloseButton_p;


@end
