/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJReportaBugView.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 12/06/14.
 Created by Yogisha Poojary on 06/05/14.
 **/

#import "TAJReportaBugView.h"
#import "TAJUtilities.h"

@implementation TAJReportaBugModel
@synthesize bugReportDictionary = _bugReportDictionary;

@end

#pragma mark - TAJReportaBugView

@interface TAJReportaBugView()<UITextFieldDelegate,UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *bugReportGameVariant;
@property (weak, nonatomic) IBOutlet UILabel *bugReportTableId;
@property (weak, nonatomic) IBOutlet UILabel *bugReportGameType;
@property (weak, nonatomic) IBOutlet UILabel *bugReportGameId;
@property (weak, nonatomic) IBOutlet UIButton *submitButton;
@property (strong, nonatomic) TAJReportaBugModel *bugReportModel;
@property (weak, nonatomic) IBOutlet UIButton *selectDropDownButton;
@property (weak, nonatomic) IBOutlet UIView *selectDropDownView;
@property (weak, nonatomic) IBOutlet UITableView *dropDownTableView;
@property (weak, nonatomic) IBOutlet UIButton *gameRulesButton;
@property (strong, nonatomic) NSMutableArray *bugListArray;
@property (nonatomic, strong)   NSString    *userBugType;

@property BOOL isSelectButtonPressed;
@property CGRect selectDropDownViewFrame;
- (IBAction)submitButton_pressed:(id)sender;
- (IBAction)bugReportCloseButton_Pressed:(id)sender;
- (IBAction)selectDropDownButtonPressed:(id)sender;
- (IBAction)gameRulesButtonTapped:(UIButton *)sender;

@property BOOL isDisconnections;
@property BOOL isGameIssues;
@property BOOL isScoreIssues;
@property BOOL isRequestCall;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewDiscoonections;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewGameIssues;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewScoreIssues;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewRequestCall;
@property (weak, nonatomic) IBOutlet UILabel *lblErrorMsg;

@end

@implementation TAJReportaBugView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        _isSelectButtonPressed = YES;
        self.lblErrorMsg.text = @"";
        [self setDefaults];
    }
    return self;
}

-(void)setDefaults {
    self.isDisconnections = NO;
    self.isGameIssues = NO;
    self.isScoreIssues = NO;
    self.isRequestCall = NO;
    self.userBugType = @"";
    self.imgViewDiscoonections.image = [UIImage imageNamed:@"menu_check"];
    self.imgViewGameIssues.image = [UIImage imageNamed:@"menu_check"];
    self.imgViewScoreIssues.image = [UIImage imageNamed:@"menu_check"];
    self.imgViewRequestCall.image = [UIImage imageNamed:@"menu_check"];

}

-(void)setGameIssues {
    [self setDefaults];
    self.isGameIssues = YES;
    self.imgViewGameIssues.image = [UIImage imageNamed:@"check_tick"];
    self.userBugType = @"Game Issues";
}

-(void)setDisconnects {
    [self setDefaults];
    self.isDisconnections = YES;
    self.imgViewDiscoonections.image = [UIImage imageNamed:@"check_tick"];
    self.userBugType = @"Disconnects";
}

-(void)setScoreIssues {
    [self setDefaults];
    self.isScoreIssues = YES;
    self.imgViewScoreIssues.image = [UIImage imageNamed:@"check_tick"];
    self.userBugType = @"Score Issues";
}

-(void)setRequestCall {
    [self setDefaults];
    self.isRequestCall = YES;
    self.imgViewRequestCall.image = [UIImage imageNamed:@"check_tick"];
    self.userBugType = @"Request Call Back";
}

- (void)initializeTheBugReport:(NSDictionary *)bugDictionary withGameId:(NSString *)gameId
{
    NSLog(@"Bug Dictionary= %@",bugDictionary);
    DLog(@"Bug Dictionary= %@",bugDictionary);
    _isSelectButtonPressed = YES;
    self.bugReportModel = [[TAJReportaBugModel alloc] init];
    //[_bugReportTextField becomeFirstResponder];
    self.bugReportModel.bugReportDictionary = bugDictionary;
    self.bugReportTableId.text =[NSString stringWithFormat:@"%@",[self.bugReportModel.bugReportDictionary valueForKeyPath:GAME_TABLE_ID_VALUE]];
    self.bugReportGameType.text = [NSString stringWithFormat:@"%@",[self.bugReportModel.bugReportDictionary valueForKeyPath:@"table_details.TAJ_table_type"]];
    self.bugReportGameId.text = gameId;
    
    self.bugReportGameVariant.text = [NSString stringWithFormat:@"%@",[self.bugReportModel.bugReportDictionary valueForKeyPath:@"table_details.TAJ_table_type"]];
    //Disconnections, Game Issues, Score Issues, Request Call Back
    
    self.bugListArray = [NSMutableArray arrayWithObjects:@"Internet",@"Game",@"Score",@"Others", nil];
    
    self.selectDropDownButton.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.bugReportTextField.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    UIView *paddingView = [[UIView alloc] initWithFrame:CGRectMake(0, 8, 8, 20)];
    self.bugReportTextField.leftView = paddingView;
    self.bugReportTextField.leftViewMode = UITextFieldViewModeAlways;
    
    if ([TAJUtilities isItIPhonex] || [TAJUtilities isItIPhone6Plus] || [TAJUtilities isItIPhoneXSMax]) {
        CGRect Frame = self.reportBugMainContainerView.frame;
        Frame.origin.x = self.reportBugMainContainerView.frame.origin.x - 80;
        self.reportBugMainContainerView.frame = Frame;
    }
}

- (IBAction)submitButton_pressed:(id)sender
{

    if ((self.isDisconnections == YES) || (self.isGameIssues == YES) || (self.isScoreIssues == YES) || (self.isRequestCall == YES)) {
        self.lblErrorMsg.text = @"";
        NSLog(@"SELECTED TYPE");
        
        if (self.bugDelecate && [self.bugDelecate respondsToSelector:@selector(sumbitButtonClickOnReportaBug:withGameID:withUserData:onGameType:haveBugType:)])
        {
            [self.bugDelecate sumbitButtonClickOnReportaBug:self.bugReportTableId.text withGameID:self.bugReportGameId.text withUserData:@"" onGameType:self.bugReportGameType.text haveBugType:self.userBugType];
        }
    }
    
    else {
        self.lblErrorMsg. text = @"Please select any bug type";
        NSLog(@"Please select any bug type");
    }
    
    /*
    NSString *bug = self.selectDropDownButton.titleLabel.text;
    if (![bug isEqualToString:@"Select"] && [self.bugReportTextField.text length] != 0)
    {
        if ([self.bugReportTextField.text isEqualToString:@"Thanks for reporting a problem"] || [self.bugReportTextField.text isEqualToString:@"Please enter your report"] || [self.bugReportTextField.text isEqualToString:@"Please select any bug type"])
        {
            [self.bugReportTextField setText:@"Please enter your report"];
        }
        else
        {
            if (self.bugDelecate && [self.bugDelecate respondsToSelector:@selector(sumbitButtonClickOnReportaBug:withGameID:withUserData:onGameType:haveBugType:)])
            {
                [self.bugDelecate sumbitButtonClickOnReportaBug:self.bugReportTableId.text withGameID:self.bugReportGameId.text withUserData:self.bugReportTextField.text onGameType:self.bugReportGameType.text haveBugType:self.userBugType];
            }
            [self.bugReportTextField setText:@"Thanks for reporting a problem"];
        }
    }
    else  if([self.bugReportTextField.text length] == 0 && [bug isEqualToString:@"Select"] )
    {
        [self.bugReportTextField setText:@"Please enter your report"];
    }

    else if([bug isEqualToString:@"Select"]|| [self.bugReportTextField.text length] == 0)
    {
        if([bug isEqualToString:@"Select"])
        {
            [self.bugReportTextField setText:@"Please select any bug type"];
        }

        if([self.bugReportTextField.text length] == 0)
        {
            [self.bugReportTextField setText:@"Please enter your report"];

        }

    }
    [self.bugReportTextField resignFirstResponder];
     */
}

- (IBAction)bugReportCloseButton_Pressed:(id)sender
{
    if (self.bugDelecate && [self.bugDelecate respondsToSelector:@selector(bugReportCloseButton_p)])
    {
        [self.bugDelecate bugReportCloseButton_p];
    }
}

- (IBAction)selectDropDownButtonPressed:(id)sender
{
    self.selectDropDownButton.titleLabel.textColor = [UIColor blackColor];
    if (self.isSelectButtonPressed)
    {
        self.selectDropDownView.hidden = NO;
        self.isSelectButtonPressed = NO;
    }
    else
    {
        self.selectDropDownView.hidden = YES;
        self.isSelectButtonPressed = YES;
    }
    
    if ([TAJUtilities isIPhone])
    {
       // [self.selectDropDownButton setBackgroundImage:[UIImage imageNamed:@"reportabug_unselected-568h~iphone.png"] forState:UIControlStateNormal];
    }
    else
    {
       // [self.selectDropDownButton setBackgroundImage:[UIImage imageNamed:@"reportabug_unselected.png"] forState:UIControlStateNormal];
    }
    
    [self.selectDropDownButton setTitle:@"Select" forState:UIControlStateNormal];
    [self.selectDropDownButton setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
//    if ([TAJUtilities isIPhone])
//    {
//        self.selectDropDownButton.titleLabel.font = [UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 8.0];
//    }
//    else
//    {
//        self.selectDropDownButton.titleLabel.font = [UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 19.0];
//    }
}

- (IBAction)gameRulesButtonTapped:(UIButton *)sender
{
    if (self.bugDelecate && [self.bugDelecate respondsToSelector:@selector(loadGameRulesWebPageFromReportBug)])
    {
        [self.bugDelecate loadGameRulesWebPageFromReportBug];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.bugListArray.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
//    if ([TAJUtilities isIPhone])
//    {
//        return 13.61;
//    }
//    else
    {
        return 30;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"SettingsCell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:cellIdentifier];
    }
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor whiteColor];
    cell.contentView.backgroundColor = [UIColor whiteColor];
    
    [cell.textLabel setText:self.bugListArray[indexPath.row]];
    [cell.textLabel setTextColor:[UIColor blackColor]];
    UIFont *myFont;
    if ([TAJUtilities isIPhone])
    {
        myFont = [ UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 14.0 ];
        
    }
    else
    {
        myFont = [ UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 15.0 ];
        
    }
    cell.textLabel.font  = myFont;
    [cell.textLabel adjustsFontSizeToFitWidth];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    NSString *title = cell.textLabel.text;
    self.userBugType = [NSString stringWithFormat:@"%@",title];
    [self.selectDropDownButton setTitle:title forState:UIControlStateNormal];
    [self.selectDropDownButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
//    if ([TAJUtilities isIPhone])
//    {
//        self.selectDropDownButton.titleLabel.font = [UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 16.0];
//    }
//    else
//    {
//        self.selectDropDownButton.titleLabel.font = [UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 20.0];
//    }
    
    if ([TAJUtilities isIPhone])
    {
       // [self.selectDropDownButton setBackgroundImage:[UIImage imageNamed:@"reportabug_selected-568h~iphone.png"] forState:UIControlStateNormal];
        
    }
    else
    {
       // [self.selectDropDownButton setBackgroundImage:[UIImage imageNamed:@"reportabug_selected.png"] forState:UIControlStateNormal];
        
    }
    self.selectDropDownView.hidden = YES;
    self.isSelectButtonPressed = YES;
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if(textField.text.length > 0)
    {
        textField.text =@"";
    }
    return YES;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    
    if (self.bugDelecate && [self.bugDelecate respondsToSelector:@selector(didReportaBugTextFieldStartEditing)])
    {
        [self.bugDelecate didReportaBugTextFieldStartEditing];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if (self.selectDropDownView.hidden == NO)
    {
        
        self.selectDropDownView.hidden = YES;
        self.isSelectButtonPressed = NO;
    }
    else
    {
        self.isSelectButtonPressed = YES;
    }
    
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    
    if (self.bugDelecate && [self.bugDelecate respondsToSelector:@selector(didReportaBugTextFieldEndEditing)])
    {
        [self.bugDelecate didReportaBugTextFieldEndEditing];
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    return YES;
}

- (IBAction)dissconnectionsClicked:(UIButton *)sender
{
    [self setDisconnects];
}

- (IBAction)gameIssuesClicked:(UIButton *)sender
{
    [self setGameIssues];
}

- (IBAction)scoreIssuesClicked:(UIButton *)sender
{
    [self setScoreIssues];
}

- (IBAction)requestCallClicked:(UIButton *)sender
{
    [self setRequestCall];
}

@end
