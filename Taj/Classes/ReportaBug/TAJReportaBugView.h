/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJReportaBugView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 12/06/14.
 Created by Yogisha Poojary on 06/05/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJReportaBugModel : NSObject

@property(strong, nonatomic) NSDictionary *bugReportDictionary;

@end

@protocol TAJReportaBugViewDelegate;

@interface TAJReportaBugView : UIView

@property(weak,nonatomic) id<TAJReportaBugViewDelegate> bugDelecate;
@property (weak, nonatomic) IBOutlet UITextField *bugReportTextField;
@property (weak, nonatomic) IBOutlet UIView *reportBugContainerView;

@property (weak, nonatomic) IBOutlet UIView *reportBugMainContainerView;
- (void)initializeTheBugReport:(NSDictionary *)bugDictionary withGameId:(NSString *)gameId;

@end

@protocol TAJReportaBugViewDelegate<NSObject>

- (void)sumbitButtonClickOnReportaBug:(NSString *)tableID withGameID:(NSString *)gameID withUserData:(NSString *)explanation onGameType:(NSString *)gameType haveBugType:(NSString *)bugType;
- (void)loadGameRulesWebPageFromReportBug;
- (void)didReportaBugTextFieldStartEditing;
- (void)didReportaBugTextFieldEndEditing;
- (void)bugReportCloseButton_p;


@end
