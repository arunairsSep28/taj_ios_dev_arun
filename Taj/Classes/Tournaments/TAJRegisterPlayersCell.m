
#import "TAJRegisterPlayersCell.h"

@interface TAJRegisterPlayersCell ()

@end

@implementation TAJRegisterPlayersCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.slNoLbl =[[UILabel alloc]init];
        self.playerNameLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJRegisterPlayersCell*)init
{
    self.slNoLbl =[[UILabel alloc]init];
    self.playerNameLbl = [[UILabel alloc]init];

    return self;
}

@end
