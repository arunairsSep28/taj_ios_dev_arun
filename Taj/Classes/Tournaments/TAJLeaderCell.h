
#import <UIKit/UIKit.h>

@interface TAJLeaderCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *slNoLbl;
@property (strong, nonatomic) IBOutlet UILabel   *playerNameLbl;
@property (strong, nonatomic) IBOutlet UILabel   *chipsLbl;

-(TAJLeaderCell*)init;

@end
