
#import <UIKit/UIKit.h>

@interface TAJWaitListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *slNoLbl;
@property (strong, nonatomic) IBOutlet UILabel   *playerNameLbl;

-(TAJWaitListCell*)init;

@end
