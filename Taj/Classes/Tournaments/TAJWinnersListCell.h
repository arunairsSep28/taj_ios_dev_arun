
#import <UIKit/UIKit.h>

@interface TAJWinnersListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *rankLbl;
@property (strong, nonatomic) IBOutlet UILabel   *cashLbl;
@property (strong, nonatomic) IBOutlet UILabel   *playersLbl;


-(TAJWinnersListCell*)init;

@end
