//
//  TournamentLobbyView.m
//  Taj
//
//  Created by Priya Saravanan on 10/30/17.
//  Copyright © 2017 Robosoft Technologies. All rights reserved.
//

#import "TournamentLobbyView.h"
#import "TAJConstants.h"
#import "TAJGameEngine.h"

@implementation TournamentLobbyView
@synthesize tournamentDelegate,chipArray,listData,filterListData;

+ (UIView*) getView {
    TournamentLobbyView* tournamentLobbyView = [[[UINib nibWithNibName:@"TournamentLobbyView" bundle:nil] instantiateWithOwner: self options:nil] objectAtIndex:0];
    //[tournamentLobbyView requestData];
    //test svn
    return tournamentLobbyView;
}

- (void)initView{
    [self requestData];
    //loadingView.hidden = NO;
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Tournament Lobby Screen"];
}

- (void) requestData {
    listData = [[NSMutableArray alloc] init];
    filterListData = [[NSMutableArray alloc]init];
    [self reloadTableView];
    [self addTableObserver];
    [self getTournamentList];
}

- (void)getTournamentList{
    [[TAJGameEngine sharedGameEngine] performSelector:@selector(requestTournamentListTable) withObject:nil afterDelay:0.5];
}

- (void)removeAllObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LIST_TABLE_UPDATE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_TOURNAMENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:END_TOURNAMENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_TO_START object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:START_TOURNAMENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:START_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:START_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STOP_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STOP_CANCEL_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_REGISTERED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_DEREGISTERED object:nil];
}

- (void)addTableObserver {
    [self removeAllObserver];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentListTableUpdate:)
                                                 name:TOURNAMENT_LIST_TABLE_UPDATE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showTournament:)
                                                 name:SHOW_TOURNAMENT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(endTournament:)
                                                 name:END_TOURNAMENT
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(tournamentToStart:)
//                                                 name:TOURNAMENT_TO_START
//                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startTournament:)
                                                 name:START_TOURNAMENT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startRegistration:)
                                                 name:START_REGISTRATION
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(stopRegistration:)
//                                                 name:STOP_REGISTRATION
//                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(stopCancelRegistration:)
//                                                 name:STOP_CANCEL_REGISTRATION
//                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerRegistered:)
                                                 name:PLAYER_REGISTERED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDeregistered:)
                                                 name:PLAYER_DEREGISTERED
                                               object:nil];
}

- (void)showTournament:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)endTournament:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}
- (void)tournamentToStart:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)startTournament:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)startRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)stopRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)stopCancelRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)playerRegistered:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)playerDeregistered:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self getTournamentList];
}

- (void)tournamentListTableUpdate:(NSNotification *) notification
{

    //"TAJ_tourney_cost" = "CASH_CASH";
    
    NSMutableDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    self.tournamentDict = dictionary;
    
    listData = [[NSMutableArray alloc] init];
    id obj = dictionary[@"tournaments"][@"tournament"];
    listData = [self convertToArray:obj];
    
    [self reloadTableView];
}

- (void)reloadTableView
{
    
    filterListData = [[NSMutableArray alloc]init];
    if(chipArray != nil && chipArray.count != 0 && chipArray.count != 2){
       // NSMutableArray *arrayTable = NULL;
        NSString *filter;
        if([[chipArray objectAtIndex:0] isEqualToString:@"FREE"]){
            //filter = @"FUNCHIPS_FUNCHIPS";
            filter = @"FUNCHIPS_CASH";//ratheesh
        }else{
            filter = @"CASH_CASH";
        }
        NSPredicate *predicateForGameCost = [NSPredicate predicateWithFormat:@"TAJ_tourney_cost == %@",filter];
        filterListData = [NSMutableArray arrayWithArray:[listData filteredArrayUsingPredicate:predicateForGameCost]];
        
    }else{
        [filterListData addObjectsFromArray:listData];
    }
    NSLog(@"FILTERD DATA : %@",filterListData);
    
    for(NSDictionary *dict in [filterListData copy]){
        NSDictionary *sortDict = [dict mutableCopy];
        NSString* status = [sortDict  objectForKey:@"TAJ_status"];
        if([[status lowercaseString] isEqualToString:@"registration open"]){
            [sortDict setValue:@"A" forKey:@"sortBy"];
        }else if([[status lowercaseString] isEqualToString:@"running"]){
            [sortDict setValue:@"B" forKey:@"sortBy"];
        }else if([[status lowercaseString] isEqualToString:@"completed"]){
            [sortDict setValue:@"C" forKey:@"sortBy"];
        }else if([[status lowercaseString] isEqualToString:@"announced"]){
            [sortDict setValue:@"D" forKey:@"sortBy"];
        }else if([[status lowercaseString] isEqualToString:@"canceled"]){
            [sortDict setValue:@"E" forKey:@"sortBy"];
            [sortDict setValue:@"Cancelled" forKey:@"TAJ_status"];
        }else{
            [sortDict setValue:@"F" forKey:@"sortBy"];
        }
        [filterListData replaceObjectAtIndex:[filterListData indexOfObject:dict] withObject:sortDict];
    }
    
    [filterListData sortUsingDescriptors:@[
                                     [NSSortDescriptor sortDescriptorWithKey:@"sortBy" ascending:YES],
                                     [NSSortDescriptor sortDescriptorWithKey:@"TAJ_start_date" ascending:YES],
                                     ]];
    
    loadingView.hidden = YES;
    [lobbyTableView reloadData];
    //    [self.activityViewShow stopAnimating];
    //    self.activityViewShow.hidden = YES;
    
}

-(NSMutableArray*)convertToArray:(id)obj{
    NSMutableArray *array = [NSMutableArray array];
    if([obj isKindOfClass:[NSArray class]]){
        array = obj;
    }else if([obj isKindOfClass:[NSDictionary class]]){
        NSMutableDictionary *dict = [NSMutableDictionary
                                     dictionaryWithDictionary:obj];
        array = [NSMutableArray arrayWithObjects:dict, nil];
    }
    return array;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return filterListData.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = (UITableViewCell*)[tableView  dequeueReusableCellWithIdentifier: CellIdentifier];
    if(cell == nil) {
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TournamentLobbyCell" owner:self   options:nil];
        cell = [topLevelObjects objectAtIndex:0];
        cell.accessoryType = UITableViewCellAccessoryNone;
        
        cell.backgroundColor = [UIColor clearColor];
    }
    
    NSDictionary* dataDictionary = (NSDictionary*)[filterListData objectAtIndex: indexPath.row];
    
    NSString* cashPrize = [dataDictionary  objectForKey:@"TAJ_cash_prize"];
    NSString* entry = [dataDictionary  objectForKey:@"TAJ_entry"];
    if([entry integerValue] == 0){
        entry = @"Free";
    }
    if([cashPrize integerValue] == 0){
        cashPrize = @"TBA";
    }
//    NSString* finalPrize = [dataDictionary  objectForKey:@"TAJ_finalprize"];
    NSString* players = [dataDictionary  objectForKey:@"TAJ_players"];
//    NSString* regStatus = [dataDictionary  objectForKey:@"TAJ_reg_status"];
    NSString* startDate = [dataDictionary  objectForKey:@"TAJ_start_date"];
    NSString* status = [dataDictionary  objectForKey:@"TAJ_status"];
//    NSString* name = [dataDictionary  objectForKey:@"TAJ_tournament_name"];
//    NSString* tournament_sd = [dataDictionary  objectForKey:@"TAJ_tournament_sd"];
//    NSString* tournamentCost = [dataDictionary  objectForKey:@"TAJ_tourney_cost"];
    NSString* tournamentId = [dataDictionary  objectForKey:@"TAJ_tourney_id"];
//    NSString* type = [dataDictionary  objectForKey:@"TAJ_type"];
    
    UILabel *tournamentIdLabel = (UILabel *)[cell viewWithTag:100];
    tournamentIdLabel.text = [NSString stringWithFormat: @"TID : %@", tournamentId];
    
    UILabel *entryFeeLabel = (UILabel *)[cell viewWithTag:101];
    entryFeeLabel.text = [NSString stringWithFormat: @"Entry Fee : %@", entry];
    
    UILabel *cashPrizeLabel = (UILabel *)[cell viewWithTag:102];
    cashPrizeLabel.text = [NSString stringWithFormat: @"Cash Prize : %@", cashPrize];
    
    UILabel *statusLabel = (UILabel *)[cell viewWithTag:103];
    statusLabel.text = status.capitalizedString;
    
    UIButton *joinButton = (UIButton*)[cell viewWithTag:107];
    joinButton.tag = indexPath.row;
    if(![statusLabel.text isEqualToString: @"Registration Open"]) {
        [joinButton setTitle: @"VIEW" forState: UIControlStateNormal];
    }
    
    [joinButton addTarget:self action:@selector(buttonTapped:) forControlEvents:UIControlEventTouchUpInside];
    
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[startDate longLongValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:@"dd-MMM hh:mm a"];
    NSString *stringFromDate = [formatter stringFromDate:date];
    
    UILabel *startTimeLabel = (UILabel *)[cell viewWithTag:104];
    startTimeLabel.text = [NSString stringWithFormat: @"Start Time : %@", stringFromDate];
    
    UILabel *playersLabel = (UILabel *)[cell viewWithTag:105];
    playersLabel.text = [NSString stringWithFormat: @"%@ Players", players];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    [self openDetailView:indexPath.row];
}

- (void)buttonTapped:(UIButton*)evt {
    [self openDetailView:evt.tag];
}

-(void)openDetailView:(NSInteger)index {
    NSDictionary* dataDictionary = (NSDictionary*)[filterListData objectAtIndex:index];
    [tournamentDelegate addTournamentDetails:dataDictionary];
}

- (void)tournamnetNotYetCreated {
    loadingView.hidden = YES;
}


@end
