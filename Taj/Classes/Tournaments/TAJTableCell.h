
#import <UIKit/UIKit.h>

@interface TAJTableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *tableIDLbl;
@property (strong, nonatomic) IBOutlet UILabel   *playersLbl;
@property (strong, nonatomic) IBOutlet UILabel   *highLbl;
@property (strong, nonatomic) IBOutlet UILabel   *lowLbl;

-(TAJTableCell*)init;

@end
