//
//  TournamentLobbyView.h
//  Taj
//
//  Created by Priya Saravanan on 10/30/17.
//  Copyright © 2017 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol TAJTournamentDelegate;

@interface TournamentLobbyView : UIView {
    
    IBOutlet UITableView* lobbyTableView;
    
    IBOutlet UIActivityIndicatorView* loadingView;
}
@property (weak, nonatomic) id<TAJTournamentDelegate> tournamentDelegate;
@property (nonatomic, strong) NSArray *chipArray;
@property (nonatomic, strong) NSMutableArray* listData;
@property (nonatomic, strong) NSMutableArray* filterListData;
@property (nonatomic, weak) NSMutableDictionary  *tournamentDict;

+ (UIView*) getView;
- (void)initView;
- (void)requestData;
- (void)removeAllObserver;
- (void)addTableObserver;
- (void)reloadTableView;
- (void)tournamnetNotYetCreated;
@end

@protocol TAJTournamentDelegate <NSObject>

- (void)addTournamentDetails:(NSDictionary *)dict;

@end
