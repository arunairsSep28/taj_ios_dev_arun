
#import <UIKit/UIKit.h>

@interface TAJLevelCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *levelLbl;
@property (strong, nonatomic) IBOutlet UILabel   *scheduleTimeLbl;
@property (strong, nonatomic) IBOutlet UILabel   *betLbl;
@property (strong, nonatomic) IBOutlet UILabel   *qualifyLbl;
@property (strong, nonatomic) IBOutlet UILabel   *rebuyLbl;


-(TAJLevelCell*)init;

@end
