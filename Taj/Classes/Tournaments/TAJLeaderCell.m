
#import "TAJLeaderCell.h"

@interface TAJLeaderCell ()

@end

@implementation TAJLeaderCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.slNoLbl =[[UILabel alloc]init];
        self.playerNameLbl = [[UILabel alloc]init];
        self.chipsLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJLeaderCell*)init
{
    self.slNoLbl =[[UILabel alloc]init];
    self.playerNameLbl = [[UILabel alloc]init];
    self.chipsLbl = [[UILabel alloc]init];

    return self;
}

@end
