
#import <UIKit/UIKit.h>

@interface TAJPrizeListCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *rankLbl;
@property (strong, nonatomic) IBOutlet UILabel   *cashLbl;

-(TAJPrizeListCell*)init;

@end
