//
//  TournamentDetailView.m
//  Taj
//
//  Created by Padagalingam A on 24/01/18.
//  Copyright © 2018 Robosoft Technologies. All rights reserved.
//

#import "TournamentDetailView.h"

@implementation TournamentDetailView
@synthesize alertView,lobbyView,tourneyID,cashPrize;

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+ (UIView*) getView {
    TournamentDetailView* tournamentDetailView = [[[UINib nibWithNibName:@"TournamentDetailView" bundle:nil] instantiateWithOwner: self options:nil] objectAtIndex:0];
    [tournamentDetailView addTableObserver];
    [tournamentDetailView getTournamentDetail];
    return tournamentDetailView;
}

- (void)initView{
    [self TourneyAndPrizeInfoAction:nil];
    [self getTournamentDetail];
    [self addTableObserver];
    loadingView.hidden = NO;
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Tournament Details Screen"];
}

- (void)getTournamentDetail{
     [[TAJGameEngine sharedGameEngine] requestTournamentDetails:tourneyID];
}

- (IBAction)TourneyAndPrizeInfoAction:(id)sender{
    TourneyAndPrizeInfoView.hidden = NO;
    SchedulesAndTablesView.hidden = YES;
    PlayersView.hidden = YES;
    
    if([TAJUtilities isIPhone]) {
        TourneyAndPrizeInfoHeaderView.hidden = NO;
        SchedulesAndTablesHeaderView.hidden = YES;
        PlayersHeaderView.hidden = YES;
    }else{
        TourneyAndPrizeInfoBtn.selected = YES;
        SchedulesAndTablesBtn.selected  = NO;
        PlayersBtn.selected  = NO;
    }
    
}
- (IBAction)SchedulesAndTablesViewAction:(id)sender{
    TourneyAndPrizeInfoView.hidden = YES;
    SchedulesAndTablesView.hidden = NO;
    PlayersView.hidden = YES;
    
    if([TAJUtilities isIPhone]) {
        TourneyAndPrizeInfoHeaderView.hidden = YES;
        SchedulesAndTablesHeaderView.hidden = NO;
        PlayersHeaderView.hidden = YES;
    }else{
        TourneyAndPrizeInfoBtn.selected = NO;
        SchedulesAndTablesBtn.selected  = YES;
        PlayersBtn.selected  = NO;
    }
    
}
- (IBAction)PlayersAction:(id)sender{
    TourneyAndPrizeInfoView.hidden = YES;
    SchedulesAndTablesView.hidden = YES;
    PlayersView.hidden = NO;
    
    if([TAJUtilities isIPhone]) {
        TourneyAndPrizeInfoHeaderView.hidden = YES;
        SchedulesAndTablesHeaderView.hidden = YES;
        PlayersHeaderView.hidden = NO;
    }else{
        TourneyAndPrizeInfoBtn.selected = NO;
        SchedulesAndTablesBtn.selected  = NO;
        PlayersBtn.selected  = YES;
    }
}

- (void)removeAllObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_DETAIL_UPDATE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_PRIZE_LIST object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LEVEL_START object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LEVEL_SCHEDULE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LEVEL_END object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_GET_LEVEL_TIMER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_TABLES object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_RESULT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_REGISTER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_DEREGISTER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_TOURNAMENT_WAIT_LIST object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_TOURNAMENT_REGISTERED_PLAYER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_TOURNAMENT_LEADER_BOARD object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_TOURNAMENT_PRIZE_DISTRIBUTION object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_REGISTERED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_DEREGISTERED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STOP_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STOP_CANCEL_REGISTRATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_TO_START object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNEY_BALANCE object:nil];
}

- (void)addTableObserver {
    
    [self removeAllObserver];
    
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentDetailUpdate:)
                                                 name:TOURNAMENT_DETAIL_UPDATE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentPrizeListUpdate:)
                                                 name:TOURNAMENT_PRIZE_LIST
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLevelStart:)
                                                 name:TOURNAMENT_LEVEL_START
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLevelSchedule:)
                                                 name:TOURNAMENT_LEVEL_SCHEDULE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLevelEnd:)
                                                 name:TOURNAMENT_LEVEL_END
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentGetLeveltimer:)
                                                 name:TOURNAMENT_GET_LEVEL_TIMER
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentGetTables:)
                                                 name:TOURNAMENT_TABLES
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentResult:)
                                                 name:TOURNAMENT_RESULT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentRegister:)
                                                 name:TOURNAMENT_REGISTER
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentDeregister:)
                                                 name:TOURNAMENT_DEREGISTER
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentWaitList:)
                                                 name:GET_TOURNAMENT_WAIT_LIST
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentRegisteredPlayer:)
                                                 name:GET_TOURNAMENT_REGISTERED_PLAYER
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLeaderBoard:)
                                                 name:GET_TOURNAMENT_LEADER_BOARD
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentPrizeDistribution:)
                                                 name:GET_TOURNAMENT_PRIZE_DISTRIBUTION
                                               object:nil];
    
  
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerRegistered:)
                                                 name:PLAYER_REGISTERED
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDeregistered:)
                                                 name:PLAYER_DEREGISTERED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(stopRegistration:)
                                                     name:STOP_REGISTRATION
                                                   object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(stopCancelRegistration:)
                                                     name:STOP_CANCEL_REGISTRATION
                                                   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                                 selector:@selector(tournamentToStart:)
                                                     name:TOURNAMENT_TO_START
                                                   object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tourneyBalance:)
                                                 name:TOURNEY_BALANCE
                                               object:nil];
}


- (void)tourneyBalance:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    NSDictionary *objDict = dictionary[@"tourneys"][@"tourney"];
    NSString *inPlay = objDict[@"TAJ_tourney_inplay"];
    if(inPlay != nil){
        YourBalLbl.text = inPlay;
    }
    
    /*{
     "TAJ_event_name" = "TOURNEY_BALANCE";
     "TAJ_msg_uuid" = "a2fee3e6-1611-11e8-85a9-1cc1de039106";
     "TAJ_name" = event;
     "TAJ_nick_name" = redimuser2;
     "TAJ_timestamp" = "1519112563.0";
     "TAJ_user_id" = 7321;
     tourneys =     {
     tourney =         {
     "TAJ_tournament_id" = 3976;
     "TAJ_tourney_chips" = "0.0";
     "TAJ_tourney_inplay" = "1500.0";
     };
     };
     }*/
}



- (void)tournamentDetailUpdate:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    entry = [dictionary[@"TAJ_entry"] intValue];
    //NSString* entry = [dataDictionary  objectForKey:@"TAJ_entry"];
    if(entry == 0){
        EntryFeeLbl.text = @"Free";
    }else{
        EntryFeeLbl.text = dictionary[@"TAJ_entry"];
    }
    TidLbl.text = dictionary[@"TAJ_tournament_id"];
    NSString *tourneyType = dictionary[@"TAJ_tournament_type"];
    
    if([[tourneyType lowercaseString] isEqualToString:@"regurlar"] || [[tourneyType lowercaseString] isEqualToString:@"regular"]){
        tourneyType = @"Regular";
    }
    TourneyTypeLbl.text = tourneyType.capitalizedString;
    
    cashPrize = [dictionary[@"TAJ_cash_prize"] intValue];
    
    JoinedLbl.text = [NSString stringWithFormat:@"%@/%@",dictionary[@"TAJ_players"],dictionary[@"TAJ_max_registration"]];
    RegisCloseLbl.text = [self convertToDate:dictionary[@"TAJ_time_to_close_registrations"]];
    TourneyStartsLbl.text = [self convertToDate:dictionary[@"TAJ_tournament_start_date"]];
    
    NSString *status = dictionary[@"TAJ_status"];
    
    BOOL isRegistered = [dictionary[@"TAJ_registered"] boolValue];
    if(!isRegistered){
        YourRankLbl.text = @"NA";
        YourBalLbl.text = @"NA";
        [registerBtn setTitle:@"Register" forState:UIControlStateNormal];
        registerBtn.tag = ALERT_REGISTER_TAG;
    }else{
        NSString *rank = dictionary[@"TAJ_rank"];
        if(rank){
            YourRankLbl.text = rank;
        }else{
            YourRankLbl.text = @"0";
        }
        
        if([[status lowercaseString] isEqualToString:@"running"]){
             if([[dictionary[@"TAJ_status_in_tourney"] lowercaseString] isEqualToString:@"eleminated"]){
                 YourBalLbl.text = dictionary[@"TAJ_tourney_chips"];
             }else{
                 YourBalLbl.text = dictionary[@"TAJ_tourney_inplay"];
             }
        }else{
              YourBalLbl.text = dictionary[@"TAJ_tourney_chips"];
        }
       
        [registerBtn setTitle:@"Deregister" forState:UIControlStateNormal];
        registerBtn.tag = ALERT_DEREGISTER_TAG;
    }
    NSString *currentLevel = dictionary[@"TAJ_current_level"];
    LevelLbl.text = currentLevel;
   
    if([status isEqualToString:@"canceled"]){
        status = @"Cancelled";
    }
    
    StatusLbl.text = status.capitalizedString;
    levelsArray = [NSMutableArray array];
    levelsArray = dictionary[@"levels"][@"level"];
    TimeBetweenLevelsLbl.text = [NSString stringWithFormat:@"%d Sec",[[[levelsArray objectAtIndex:0] objectForKey:@"TAJ_delay_between_level"] intValue]];
    [levelTableView reloadData];
    
    waitListView.hidden = NO;
    leaderView.hidden = YES;
    if([dictionary[@"TAJ_wait_list"] intValue] == 1){
        [[TAJGameEngine sharedGameEngine] requestGetTournamentWaitList:tourneyID];
    }
    
    if(![[status lowercaseString] isEqualToString:@"announced"]){
        [[TAJGameEngine sharedGameEngine] requestGetRegisteredPlayer:tourneyID];
    }
    
    prizeListView.hidden = NO;
    winnersView.hidden = YES;
    if([[status lowercaseString] isEqualToString:@"completed"]){
        [[TAJGameEngine sharedGameEngine] requestGetPrizeDistribution:tourneyID];
        registerBtn.hidden = YES;
        CompletedStatusView.hidden = NO;
        runningStatusView.hidden = YES;
        winnersView.hidden = NO;
        prizeListView.hidden = YES;
        waitListView.hidden = YES;
        leaderView.hidden = NO;
        [[TAJGameEngine sharedGameEngine] requestGetLeaderBoard:tourneyID];
        CompletedTimeLbl.text = [self convertToDate:[[levelsArray objectAtIndex:levelsArray.count-1] objectForKey:@"TAJ_end_time"]];
    }else if([[status lowercaseString] isEqualToString:@"running"]){
        [[TAJGameEngine sharedGameEngine] requestGetPrizeList:tourneyID];
        [[TAJGameEngine sharedGameEngine] requestGetLevelTimer:tourneyID msgUUID:[[TAJUtilities sharedUtilities] getTheUUID]];
        [[TAJGameEngine sharedGameEngine] requestGetTournamentTables:tourneyID];
        waitListView.hidden = YES;
        leaderView.hidden = NO;
        [[TAJGameEngine sharedGameEngine] requestGetLeaderBoard:tourneyID];
        registerBtn.hidden = YES;
        runningStatusView.hidden = NO;
        CompletedStatusView.hidden = YES;
        runningLevelLbl.text = [NSString stringWithFormat:@"%@/%lu",currentLevel,(unsigned long)[levelsArray count]];
    }else if([[status lowercaseString] isEqualToString:@"canceled"] || [[status lowercaseString] isEqualToString:@"announced"]){
        [[TAJGameEngine sharedGameEngine] requestGetPrizeList:tourneyID];
        registerBtn.hidden = YES;
        CompletedStatusView.hidden = YES;
        runningStatusView.hidden = YES;
    }else{
        [[TAJGameEngine sharedGameEngine] requestGetPrizeList:tourneyID];
        registerBtn.hidden = NO;
        CompletedStatusView.hidden = YES;
        runningStatusView.hidden = YES;
    }
    scheduleDateLbl.text =[NSString stringWithFormat:@"(%@) :",TourneyStartsLbl.text];
}


- (NSString*)convertToDate:(NSString*)dateString{
    return [self convertToFormat:dateString format:@"EEE-dd MMM hh:mm a"];
}

- (NSString*)convertToTime:(NSString*)dateString{
    return [self convertToFormat:dateString format:@"hh:mm"];
}

- (NSString*)convertToTimeWithMeridiem:(NSString*)dateString{
    return [self convertToFormat:dateString format:@"hh:mm a"];
}

- (NSString*)convertToFormat:(NSString*)dateString format:(NSString*)formatString{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[dateString longLongValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:formatString];
    NSString *convertedString = [formatter stringFromDate:date];
    return convertedString;
}

- (void)tournamentLevelSchedule:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
}
- (void)tournamentLevelStart:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    LevelLbl.text = dictionary[@"TAJ_level_id"];
    [[TAJGameEngine sharedGameEngine] requestGetLevelTimer:dictionary[@"TAJ_tournament_id"] msgUUID:dictionary[@"TAJ_msg_uuid"]];

}
- (void)tournamentLevelEnd:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
}
- (void)tournamentGetLeveltimer:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
  
    levelTimer = [dictionary[@"TAJ_leveltimer"] doubleValue];
    [self stopTimer];
    [self updateTimer];
    [self startTimer];
    runningLevelLbl.text = dictionary[@"TAJ_leveldetails"];
}

- (void)stopTimer{
    [stopWatchTimer invalidate];
    stopWatchTimer = nil;
}

- (void)startTimer{
    stopWatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTimer)
                                                    userInfo:nil
                                                     repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:stopWatchTimer
                                 forMode:NSRunLoopCommonModes];
}

- (void)updateTimer{
   
    NSString *timestamp = [NSString stringWithFormat:@"%f", [[NSDate new] timeIntervalSince1970]];
    double current = [timestamp doubleValue];
    NSTimeInterval difference = [[NSDate dateWithTimeIntervalSince1970:levelTimer] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:current]];
    runningTimerLbl.text = [self stringFromTimeInterval:difference];
    
    //NSLog(@"difference: %f", difference);
    if ([runningTimerLbl.text isEqualToString:@"00:00"]) {
        [self stopTimer];
    }
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
   // NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

- (void)tournamentRegister:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    /*{
     "TAJ_code" = 200;
     "TAJ_data" = "register_tournament";
     "TAJ_msg_uuid" = "54860AFD-17F4-4B55-92B7-85AA005ADA03";
     "TAJ_name" = reply;
     "TAJ_nick_name" = redimuser2;
     "TAJ_timestamp" = "1519113893.0";
     "TAJ_tournament_chips" = 1000;
     "TAJ_tournament_id" = 3998;
     "TAJ_type" = "+OK";
     "TAJ_user_id" = 7321;
     "TAJ_vipcode" = 0;
     "TAJ_waiting" = False;
     }*/
    
    YourRankLbl.text = @"0";
    YourBalLbl.text = dictionary[@"TAJ_tournament_chips"];
    
    [registerBtn setTitle:@"Deregister" forState:UIControlStateNormal];
    registerBtn.tag = ALERT_DEREGISTER_TAG;
    
    NSString *msg = [NSString stringWithFormat:@"You have been registered for the tourney and you have %@ T.Chips for the tournament.",dictionary[@"TAJ_tournament_chips"]];
    [self showAlert_OKType:msg tag:REGISTER_SUCCESS_TAG];
}

- (void)tournamentDeregister:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    YourRankLbl.text = @"NA";
    YourBalLbl.text = @"NA";
    
    [registerBtn setTitle:@"Register" forState:UIControlStateNormal];
    registerBtn.tag = ALERT_REGISTER_TAG;
    NSString *msg = [NSString stringWithFormat:@"You have been unregistered for the tourney."];
    [self showAlert_OKType:msg tag:DEREGISTER_SUCCESS_TAG];
}

- (void)showAlert_OKType:(NSString *)message tag:(NSInteger)tag
{
    if(alertView){
        [self hideAlert];
    }
    if (!alertView){
        alertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:lobbyView tag:tag popupButtonType:eOK_Type message:message];
        [alertView show];
    }
}

- (void)tournamentResult:(NSNotification *) notification{
//    NSDictionary *dictionary = nil;
//    if (notification)
//    {
//        dictionary = [notification object];
//    }
//    winnersArray = [NSMutableArray array];
//    id obj = dictionary[@"players"];
//    winnersArray = [self convertToArray:obj];
//    [winnersTableView reloadData];
}

- (void)tournamentPrizeDistribution:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    loadingView.hidden = YES;
    
    winnersArray = [NSMutableArray array];
    id obj = dictionary[@"players"][@"player"];
    winnersArray = [self convertToArray:obj];
    [winnersTableView reloadData];
}

- (void)tournamentPrizeListUpdate:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    loadingView.hidden = YES;
    prizeListArray = [NSMutableArray array];
    id obj = dictionary[@"prize_list"][@"prize"];
    prizeListArray = [self convertToArray:obj];
    [prizeTableView reloadData];
}

- (void)tournamentGetTables:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    tablesArray = [NSMutableArray array];
    id obj = dictionary[@"tables"][@"table"];
    tablesArray = [self convertToArray:obj];
    [tablesTableView reloadData];
}

- (void)tournamentWaitList:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    waitlistArray = [NSMutableArray array];
    id obj = dictionary[@"tournament_wait_list"][@"player"];
    waitlistArray = [self convertToArray:obj];
    [waitlistTableView reloadData];
}

- (void)tournamentRegisteredPlayer:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    registerPlayersArray = [NSMutableArray array];
   
    id obj = dictionary[@"players"][@"player"];
    registerPlayersArray = [self convertToArray:obj];
    
    [registerPlayersTableView reloadData];
}

- (void)tournamentLeaderBoard:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }

    leaderArray = [NSMutableArray array];
    id obj = dictionary[@"leader_board"][@"player"];
    leaderArray = [self convertToArray:obj];
    [leaderArray sortUsingDescriptors:@[
                                     [NSSortDescriptor sortDescriptorWithKey:@"TAJ_rank" ascending:YES],
                                     ]];
    [leaderTableView reloadData];
}

- (void)playerRegistered:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    JoinedLbl.text = dictionary[@"TAJ_total_players"];
    [[TAJGameEngine sharedGameEngine] requestGetRegisteredPlayer:tourneyID];
}

- (void)playerDeregistered:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [[TAJGameEngine sharedGameEngine] requestGetRegisteredPlayer:tourneyID];
}

- (void)stopRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    /*{
     "TAJ_event_name" = "stop_registration";
     "TAJ_msg_uuid" = "ded9a882-0bc7-11e8-85a9-1cc1de039106";
     "TAJ_name" = event;
     "TAJ_timestamp" = "1517981370.0";
     "TAJ_tournament_id" = 3037;
     }*/
    
    if([tourneyID isEqualToString:dictionary[@"TAJ_tournament_id"]]){
        NSString *msg = @"Registrations closed";
        [self showAlert_OKType:msg tag:STOP_REGISTRATION_TAG];
    }
    //Registrations closed
}
- (void)stopCancelRegistration:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    /*{
     "TAJ_event_name" = "stop_cancel_registration";
     "TAJ_msg_uuid" = "ccf805e6-0bc7-11e8-85a9-1cc1de039106";
     "TAJ_name" = event;
     "TAJ_timestamp" = "1517981340.0";
     "TAJ_tournament_id" = 3037;
     }*/
    //time to cancel registrations has ended
    if([tourneyID isEqualToString:dictionary[@"TAJ_tournament_id"]]){
         NSString *msg = @"Time to cancel registrations has ended";
        [self showAlert_OKType:msg tag:STOP_CANCEL_REGISTRATION_TAG];
    }
}

- (void)tournamentToStart:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }

    if([tourneyID isEqualToString:dictionary[@"TAJ_tournament_id"]]){
        NSString *msg = @"Tournament is going to start";
        [self showAlert_OKType:msg tag:TOURNAMENT_TO_START_TAG];
    }
}

-(NSMutableArray*)convertToArray:(id)obj{
    NSMutableArray *array = [NSMutableArray array];
    if([obj isKindOfClass:[NSArray class]]){
        array = obj;
    }else if([obj isKindOfClass:[NSDictionary class]]){
        NSMutableDictionary *dict = [NSMutableDictionary
                                     dictionaryWithDictionary:obj];
        array = [NSMutableArray arrayWithObjects:dict, nil];
    }
    return array;
}

#pragma mark -UITableView Delegate Datasource-

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
     if(tableView == prizeTableView){
         return [prizeListArray count];
     }else if(tableView == levelTableView){
         return [levelsArray count];
     }else if(tableView == tablesTableView){
         return [tablesArray count];
     }if(tableView == winnersTableView){
         return [winnersArray count];
     }if(tableView == registerPlayersTableView){
         return [registerPlayersArray count];
     }if(tableView == waitlistTableView){
         return [waitlistArray count];
     }if(tableView == leaderTableView){
         return [leaderArray count];
     }else{
         return 0;
     }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if(tableView == prizeTableView){
        static NSString *identifier = @"PrizeListID";
        
        TAJPrizeListCell *cell = (TAJPrizeListCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJPrizeListCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        
        NSString *cash;
       // if ([[StatusLbl.text lowercaseString] isEqualToString:@"registration open"] || [[StatusLbl.text lowercaseString] isEqualToString:@"running"] || [[StatusLbl.text lowercaseString] isEqualToString:@"cancelled"] ) {
        if (cashPrize == 0){
            cash = [NSString stringWithFormat:@"%@%@",[[prizeListArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_percentage"],@"%"];
        }else{
            cash = [NSString stringWithFormat:@"%2.0f", [[[prizeListArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_prize_amount"] floatValue]];
        }
        
        cell.cashLbl.text = cash;
        cell.rankLbl.text = [[prizeListArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_rank"];
        return cell;
    }else if(tableView == levelTableView){
        static NSString *identifier = @"LevelID";
        TAJLevelCell *cell = (TAJLevelCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJLevelCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        
        cell.levelLbl.text = [[levelsArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_level_id"];
        cell.scheduleTimeLbl.text =[NSString stringWithFormat:@"%@ to %@",[self convertToTime:[[levelsArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_start_time"]],[self convertToTime:[[levelsArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_end_time"]]];
        cell.betLbl.text = [[levelsArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_bet"];
        cell.qualifyLbl.text = [[levelsArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_qualifying_points"];
        NSString *levelBuying =  [[levelsArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_level_buying"];
        if([levelBuying intValue] == 0){
            cell.rebuyLbl.text = @"NA";
        }else{
            cell.rebuyLbl.text = levelBuying;
        }
        return cell;
    }else if(tableView == tablesTableView){
        static NSString *identifier = @"TableID";
        TAJTableCell *cell = (TAJTableCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJTableCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        
        cell.tableIDLbl.text = [[tablesArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_table_id"];
        cell.playersLbl.text =[[tablesArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_current_players"];
        cell.highLbl.text = [NSString stringWithFormat:@"%d",[[[tablesArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_high"] integerValue]];
        cell.lowLbl.text = [NSString stringWithFormat:@"%d",[[[tablesArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_low"] integerValue]];
        return cell;
    }else if(tableView == winnersTableView){
        static NSString *identifier = @"WinnersListID";
        
        TAJWinnersListCell *cell = (TAJWinnersListCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJWinnersListCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        cell.cashLbl.text =[NSString stringWithFormat:@"%2.0f", [[[winnersArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_prize_amount"] floatValue]];
        cell.rankLbl.text = [[winnersArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_rank"];
         cell.playersLbl.text = [[winnersArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_nick_name"];
        return cell;
    }else if(tableView == registerPlayersTableView){
        static NSString *identifier = @"RegisterPlayersID";
        
        TAJRegisterPlayersCell *cell = (TAJRegisterPlayersCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJRegisterPlayersCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        cell.slNoLbl.text =[NSString stringWithFormat:@"%d",indexPath.row+ 1];
        cell.playerNameLbl.text = [[registerPlayersArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_nick_name"];
        
        return cell;
    }else if(tableView == waitlistTableView){
        static NSString *identifier = @"WaitListID";
        
        TAJWaitListCell *cell = (TAJWaitListCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJWaitListCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        cell.slNoLbl.text =[NSString stringWithFormat:@"%ld",indexPath.row+ 1];
        cell.playerNameLbl.text = [[waitlistArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_nick_name"];
        return cell;
    }else if(tableView == leaderTableView){
        static NSString *identifier = @"LeaderID";
        
        TAJLeaderCell *cell = (TAJLeaderCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        if (cell == nil){
            NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"TAJLeaderCell" owner:self options:nil];
            cell = [topLevelObjects objectAtIndex:0];
            cell.accessoryType = UITableViewCellAccessoryNone;
            cell.backgroundColor = [UIColor clearColor];
        }
        cell.slNoLbl.text =[NSString stringWithFormat:@"%ld",indexPath.row+ 1];
        cell.playerNameLbl.text = [[leaderArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_nick_name"];
        cell.chipsLbl.text = [[leaderArray objectAtIndex:indexPath.row] objectForKey:@"TAJ_tourney_chips"];
        return cell;
    }
   
}

- (IBAction)registerAction:(UIButton*)sender{
    if(sender.tag == ALERT_REGISTER_TAG){
        NSString *msg = [NSString stringWithFormat:@"Do you want to register for this Tournament? %d Chips will be deducted from your account.",entry];
        alertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:lobbyView tag:ALERT_REGISTER_TAG popupButtonType:eYes_No_Type message:msg];
        
    }else{
        NSString *msg = [NSString stringWithFormat:@"Are you sure want to unregister from TID:  %@",tourneyID];
        alertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:lobbyView tag:ALERT_DEREGISTER_TAG popupButtonType:eYes_No_Type message:msg];
        
    }
    [alertView show];
}

- (void)registerTournament{
    [self hideAlert];
    [[TAJGameEngine sharedGameEngine] requestRegisterTournament:tourneyID level:@"1" amt:@"0"];
}

- (void)deregisterTournament{
    [self hideAlert];
    [[TAJGameEngine sharedGameEngine] requestDeregisterTournament:tourneyID level:@"1"];
}

- (void)hideAlert{
    [alertView hide];
    alertView = nil;
}

- (void)clearData{
    [self stopTimer];
    [self removeAllObserver];
}

@end
