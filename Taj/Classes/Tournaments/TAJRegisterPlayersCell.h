
#import <UIKit/UIKit.h>

@interface TAJRegisterPlayersCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *slNoLbl;
@property (strong, nonatomic) IBOutlet UILabel   *playerNameLbl;


-(TAJRegisterPlayersCell*)init;

@end
