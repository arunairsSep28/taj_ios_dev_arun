
#import "TAJPrizeListCell.h"

@interface TAJPrizeListCell ()

@end

@implementation TAJPrizeListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.rankLbl =[[UILabel alloc]init];
        self.cashLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJPrizeListCell*)init
{
    self.rankLbl =[[UILabel alloc]init];
    self.cashLbl = [[UILabel alloc]init];

    return self;
}

@end
