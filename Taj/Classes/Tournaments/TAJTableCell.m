
#import "TAJTableCell.h"

@interface TAJTableCell ()

@end

@implementation TAJTableCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.tableIDLbl =[[UILabel alloc]init];
        self.playersLbl = [[UILabel alloc]init];
        self.highLbl =[[UILabel alloc]init];
        self.lowLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJTableCell*)init
{
    self.tableIDLbl =[[UILabel alloc]init];
    self.playersLbl = [[UILabel alloc]init];
    self.highLbl =[[UILabel alloc]init];
    self.lowLbl = [[UILabel alloc]init];

    return self;
}

@end
