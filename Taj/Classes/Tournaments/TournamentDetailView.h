//
//  TournamentDetailView.h
//  Taj
//
//  Created by Padagalingam A on 24/01/18.
//  Copyright © 2018 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAJUtilities.h"
#import "TAJPrizeListCell.h"
#import "TAJWinnersListCell.h"
#import "TAJLevelCell.h"
#import "TAJTableCell.h"
#import "TAJRegisterPlayersCell.h"
#import "TAJWaitListCell.h"
#import "TAJLeaderCell.h"
#import "TAJGameEngine.h"
#import "TAJInfoPopupViewController.h"

@interface TournamentDetailView : UIView<InfoPopupViewDelegate>{
    
    IBOutlet UIView* TourneyAndPrizeInfoView;
    IBOutlet UIView* SchedulesAndTablesView;
    IBOutlet UIView* PlayersView;
    
    IBOutlet UIButton* TourneyAndPrizeInfoBtn;
    IBOutlet UIButton* SchedulesAndTablesBtn;
    IBOutlet UIButton* PlayersBtn;
    
    IBOutlet UIView* TourneyAndPrizeInfoHeaderView;
    IBOutlet UIView* SchedulesAndTablesHeaderView;
    IBOutlet UIView* PlayersHeaderView;
    
    //TourneyAndPrizeInfoView
    IBOutlet UIView* prizeListView;
    IBOutlet UIView* winnersView;
    IBOutlet UITableView* prizeTableView;
    IBOutlet UITableView* winnersTableView;
    NSMutableArray *prizeListArray;
    NSMutableArray *winnersArray;
    IBOutlet UILabel* TidLbl;
    IBOutlet UILabel* TourneyTypeLbl;
    IBOutlet UILabel* EntryFeeLbl;
    IBOutlet UILabel* JoinedLbl;
    IBOutlet UILabel* RegisCloseLbl;
    IBOutlet UILabel* TourneyStartsLbl;
    IBOutlet UILabel* YourBalLbl;
    IBOutlet UILabel* YourRankLbl;
    IBOutlet UILabel* LevelLbl;
    IBOutlet UILabel* TimeBetweenLevelsLbl;
    IBOutlet UILabel* StatusLbl;
    
    //scedules and table
    IBOutlet UILabel* scheduleDateLbl;
    IBOutlet UITableView* levelTableView;
    NSMutableArray *levelsArray;
    IBOutlet UITableView* tablesTableView;
    NSMutableArray *tablesArray;
    NSTimer* stopWatchTimer;
    double levelTimer;
    int entry;
    
    //RegisterPlayer,Waitlist and LeaderBoard
    IBOutlet UIView* waitListView;
    IBOutlet UIView* leaderView;
    IBOutlet UITableView* registerPlayersTableView;
    IBOutlet UITableView* waitlistTableView;
    IBOutlet UITableView* leaderTableView;
    NSMutableArray *registerPlayersArray;
    NSMutableArray *waitlistArray;
    NSMutableArray *leaderArray;
    
    //footer
    IBOutlet UIButton* registerBtn;
    IBOutlet UIView* runningStatusView;
    IBOutlet UIView* CompletedStatusView;
    IBOutlet UILabel* runningLevelLbl;
    IBOutlet UILabel* runningTimerLbl;
    IBOutlet UILabel* CompletedTimeLbl;
    
    IBOutlet UIView* loadingView;
}
@property (nonatomic, strong) TAJInfoPopupViewController *alertView;
@property (nonatomic, weak) id lobbyView;
@property (nonatomic, strong) NSString *tourneyID;
@property (nonatomic) int cashPrize;

- (IBAction)TourneyAndPrizeInfoAction:(id)sender;
- (IBAction)SchedulesAndTablesViewAction:(id)sender;
- (IBAction)PlayersAction:(id)sender;
- (IBAction)registerAction:(UIButton*)sender;
- (void)registerTournament;
- (void)deregisterTournament;
- (void)hideAlert;

+ (UIView*) getView;
- (void)initView;
- (void) clearData;

@end
