
#import "TAJWaitListCell.h"

@interface TAJWaitListCell ()

@end

@implementation TAJWaitListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.slNoLbl =[[UILabel alloc]init];
        self.playerNameLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJWaitListCell*)init
{
    self.slNoLbl =[[UILabel alloc]init];
    self.playerNameLbl = [[UILabel alloc]init];

    return self;
}

@end
