
#import "TAJLevelCell.h"

@interface TAJLevelCell ()

@end

@implementation TAJLevelCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.levelLbl =[[UILabel alloc]init];
        self.scheduleTimeLbl = [[UILabel alloc]init];
        self.betLbl =[[UILabel alloc]init];
        self.qualifyLbl = [[UILabel alloc]init];
        self.rebuyLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJLevelCell*)init
{
    self.levelLbl =[[UILabel alloc]init];
    self.scheduleTimeLbl = [[UILabel alloc]init];
    self.betLbl =[[UILabel alloc]init];
    self.qualifyLbl = [[UILabel alloc]init];
    self.rebuyLbl = [[UILabel alloc]init];

    return self;
}

@end
