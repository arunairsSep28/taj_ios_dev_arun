
#import "TAJWinnersListCell.h"

@interface TAJWinnersListCell ()

@end

@implementation TAJWinnersListCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.rankLbl =[[UILabel alloc]init];
        self.cashLbl = [[UILabel alloc]init];
        self.playersLbl = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJWinnersListCell*)init
{
    self.rankLbl =[[UILabel alloc]init];
    self.cashLbl = [[UILabel alloc]init];
    self.playersLbl = [[UILabel alloc]init];

    return self;
}

@end
