/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSettingsViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 07/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJBaseViewController.h"

@interface TAJSettingsViewController : TAJBaseViewController
{
    
}

@property (strong, nonatomic) IBOutlet UISwitch *soundSwitch;

- (void)changeSwitch:(id)sender;

@end
