/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWebViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 09/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJBaseViewController.h"

@interface TAJWebViewController : TAJBaseViewController<UIWebViewDelegate>
{
    
}
@property (strong, nonatomic) IBOutlet UIWebView *webViewForPurchase;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadinActivity;

- (void) loadPurchaseScreen;

@end
