/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWebViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 09/04/14.
 **/

#import "TAJWebViewController.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"

@interface TAJWebViewController ()

@end

@implementation TAJWebViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loadinActivity.hidden = YES;
    
    // Do any additional setup after loading the view from its nib.
    [self loadPurchaseScreen];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) loadPurchaseScreen
{
    self.webViewForPurchase.delegate = self;
    [self.loadinActivity startAnimating];

    NSString *urlString = PURCHASE_URL;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *request = [NSURLRequest requestWithURL:url];
    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
    
    {
         if ([data length] > 0 && error == nil)
         {
             [self.webViewForPurchase loadRequest:request];
         }
         else if (error != nil)
         {
             [self.loadinActivity stopAnimating];
             [[TAJUtilities sharedUtilities] showAlert:[error description]];
         }
     }];
}


- (void)webViewDidStartLoad:(UIWebView *)webView
{
    
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    [self.loadinActivity stopAnimating];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.loadinActivity stopAnimating];
    [[TAJUtilities sharedUtilities] showAlert:[error description]];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationMaskPortrait;//| UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    
}

@end
