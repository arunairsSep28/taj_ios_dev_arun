/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJBaseViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 02/04/14.
 **/

#import "TAJBaseViewController.h"
#import "TAJConstants.h"

@interface TAJBaseViewController ()

-(void)reachabilityChanged:(NSNotification*)note;

@property (nonatomic, strong) TAJAppDelegate *appDelegate;
//@property(strong) Reachability * googleReach;
//@property(strong) Reachability * localWiFiReach;
//@property(strong) Reachability * internetConnectionReach;

@end

@implementation TAJBaseViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(reachabilityChanged:)
//                                                 name:kReachabilityChangedNotification
//                                               object:nil];
//
//    [self checkInternet];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
	// Do any additional setup after loading the view.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePlayerDisconnected:) name:PLAYER_DISCONNECTED object:Nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(handlePlayerReconnected:) name:PLAYER_RECONNECTED object:Nil];
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_DISCONNECTED object:Nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_RECONNECTED object:Nil];
    
    [super viewWillDisappear:animated];
}

-(void) handlePlayerDisconnected:(NSNotification *)notification
{
    
}

-(void) handlePlayerReconnected:(NSNotification *)notification
{
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}

#pragma mark - Fetch Data From Saved Core Data

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    
    //dispatch_async(dispatch_get_main_queue(), ^{

    self.appDelegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
    //});

    context = [[self.appDelegate persistentContainer] viewContext];
    
    return context;
}

-(void)saveToCoreDataWithEvent:(NSString *)event {
    
    NSString * userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USERID]];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *events = [NSEntityDescription insertNewObjectForEntityForName:@"FirstTableEvents" inManagedObjectContext:context];
    
    [events setValue:event forKey:@"event"];
    [events setValue:userId forKey:@"playerid"];
    [events setValue:self.firstGameTableId forKey:@"tableid"];
    [events setValue:self.firstGameId forKey:@"gameid"];
    [events setValue:self.firstGameSubId forKey:@"subgameid"];
    [events setValue:[self currentTime] forKey:@"timestamp"];
    [events setValue:@"level" forKey:@"log_level"];
    [events setValue:self.firstGameChipsType forKey:@"chipstype"];
    [events setValue:self.firstGameBet forKey:@"bet"];
    [events setValue:self.firstGameType forKey:@"gametype"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"Success");
        //[self fetchDataFromCoreData];
    }
}

- (NSString *)currentTime {
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *timeString = [formatter stringFromDate:date];
    NSLog(@"CURRENT TIME : %@",timeString);
    return timeString;
}

-(void)saveToCoreDataWithSecondTableEvent:(NSString *)event {
    
    NSString * userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USERID]];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *events = [NSEntityDescription insertNewObjectForEntityForName:@"SecondTableEvents" inManagedObjectContext:context];
    
    [events setValue:event forKey:@"event"];
    [events setValue:userId forKey:@"playerid"];
    [events setValue:self.firstGameTableId forKey:@"tableid"];
    [events setValue:self.firstGameId forKey:@"gameid"];
    [events setValue:self.firstGameSubId forKey:@"subgameid"];
    [events setValue:[self currentTime] forKey:@"timestamp"];
    [events setValue:@"level" forKey:@"log_level"];
    [events setValue:self.firstGameChipsType forKey:@"chipstype"];
    [events setValue:self.firstGameBet forKey:@"bet"];
    [events setValue:self.firstGameType forKey:@"gametype"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"Success");
        
        //[self fetchDataFromCoreData];
    }
}


-(void)saveToCoreDataWithTableId:(NSString *)tableId isFirstTabel:(BOOL)isFirstTable {
    
    NSString * userId = [NSString stringWithFormat:@"%@",[[NSUserDefaults standardUserDefaults] objectForKey:USERID]];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *events = [NSEntityDescription insertNewObjectForEntityForName:@"Tables" inManagedObjectContext:context];
    if (isFirstTable) {
        [events setValue:tableId forKey:@"firstTableId"];
    }
    else {
        [events setValue:userId forKey:@"secondTableId"];
    }
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"Tabeles Data Saved.Success...");
        //[self fetchTablesDataFromCoreData];
    }
}

#pragma mark - Fetch Data From Saved Core Data

-(void)fetchTablesDataFromCoreData {
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"Tables"];
    fetchRequest.resultType = NSDictionaryResultType;
    NSMutableArray * tablesArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"SAVED TABLES DATA : %@",tablesArray);
    if (tablesArray.count > 0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
                       {
                           self.firstTableID = tablesArray[0][@"firstTableId"];
                           self.secondTableId = tablesArray[0][@"secondTableId"];
                           NSLog(@"CORE DATA FIRST TABLE ID : %@",self.firstTableID);
                           NSLog(@"CORE DATA SECOND TABLE ID : %@",self.secondTableId);
                       });
    }
}

-(void)fetchDataFromCoreData {
    NSMutableArray* eventsArray = [NSMutableArray new];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"FirstTableEvents"];
    fetchRequest.resultType = NSDictionaryResultType;
    eventsArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"SAVED DATA : %@",eventsArray);
    if (eventsArray.count > 0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
                       {
                           //[self postEventsToEGCS];
                           //[self deleteAllEntities:@"FirstTableEvents"];
                       });
    }
}
/*
-(void)checkInternet {
    // create a Reachability object for www.google.com
    
    self.googleReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    self.googleReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this uses NSOperationQueue mainQueue
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        }];
    };
    
    self.googleReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Unreachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this one uses dispatch_async they do the same thing (as above)
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [self.googleReach startNotifier];
    
    
    // create a reachability for the local WiFi
    
    self.localWiFiReach = [Reachability reachabilityForLocalWiFi];
    
    // we ONLY want to be reachable on WIFI - cellular is NOT an acceptable connectivity
    self.localWiFiReach.reachableOnWWAN = NO;
    
    self.localWiFiReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"LocalWIFI Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    self.localWiFiReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"LocalWIFI Block Says Unreachable(%@)", reachability.currentReachabilityString];
        
        NSLog(@"%@", temp);
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [self.localWiFiReach startNotifier];
    
    // create a Reachability object for the internet
    
    self.internetConnectionReach = [Reachability reachabilityForInternetConnection];
    
    self.internetConnectionReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@" InternetConnection Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    self.internetConnectionReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"InternetConnection Block Says Unreachable(%@)", reachability.currentReachabilityString];
        
        NSLog(@"%@", temp);
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [self.internetConnectionReach startNotifier];
}

-(void)reachabilityChanged:(NSNotification *)note
{
    Reachability * reach = [note object];
    if (reach == self.internetConnectionReach)
    {
        if([reach isReachable])
        {
            NSString * temp = [NSString stringWithFormat:@"InternetConnection Notification Says Reachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            
            NSDictionary *aDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Reachable", @"check", nil];

            [[NSNotificationCenter defaultCenter] postNotificationName: @"Internet_Connection" object: aDictionary];
        }
        else
        {
            NSString * temp = [NSString stringWithFormat:@"InternetConnection Notification Says Unreachable(%@)", reach.currentReachabilityString];
            NSLog(@"%@", temp);
            
            NSDictionary *aDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:@"Unreachable", @"check", nil];
            
            [[NSNotificationCenter defaultCenter] postNotificationName: @"Internet_Connection" object: aDictionary];
        }
    }
}
*/

@end
