/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJBaseViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 02/04/14.
 **/

#import <UIKit/UIKit.h>
#import "Reachability.h"

@interface TAJBaseViewController : UIViewController
{
    
}

@property(strong) Reachability * googleReach;
@property(strong) Reachability * localWiFiReach;
@property(strong) Reachability * internetConnectionReach;

@property (strong, nonatomic) NSString *firstGameTableId;
@property (strong, nonatomic) NSString *firstGameId;
@property (strong, nonatomic) NSString *firstGameSubId;
@property (strong, nonatomic) NSString *firstGameChipsType;
@property (strong, nonatomic) NSString *firstGameBet;
@property (strong, nonatomic) NSString *firstGameType;

@property (strong, nonatomic) NSString *secondGameTableId;
@property (strong, nonatomic) NSString *secondGameId;
@property (strong, nonatomic) NSString *secondGameSubId;
@property (strong, nonatomic) NSString *secondGameChipsType;
@property (strong, nonatomic) NSString *secondGameBet;
@property (strong, nonatomic) NSString *secondGameType;


@property (strong, nonatomic) NSString *firstTableID;
@property (strong, nonatomic) NSString *secondTableId;

-(void) handlePlayerDisconnected;
-(void) handlePlayerReconnected;
- (void)openURLWithString:(NSString *)string;

//COREDATA METHODS
-(NSManagedObjectContext *)managedObjectContext;
-(void)saveToCoreDataWithEvent:(NSString *)event;
-(void)saveToCoreDataWithSecondTableEvent:(NSString *)event;
    
-(void)saveToCoreDataWithTableId:(NSString *)tableId isFirstTabel:(BOOL)isFirstTable;

-(void)checkInternet;

@end
