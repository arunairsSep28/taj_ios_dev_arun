/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRammyCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/01/14.
 **/

#import "TAJRammyCardView.h"
#import "TAJRummyCard.h"

@implementation TAJRammyCardView
@synthesize card = _card;

- (id)initWithCard:(TAJRummyCard *)card
{
    self = [super init];
    if(self)
    {
        self.card = card;
        self.cardImageView = [[UIImageView alloc]initWithFrame:self.bounds];
        self.cardImageView.image = card.image;
        [self addSubview:self.cardImageView];
    }
    return self;
}

- (void)setFrame:(CGRect)frame
{
    [super setFrame:frame];
    self.cardImageView.frame = self.bounds;
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    self.cardImageView.frame = self.bounds;
}

@end
