/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRummyCard.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/01/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJRummyCard : NSObject

@property (nonatomic, assign) NSString      *suit;
@property (nonatomic, assign) NSUInteger    value;
@property (nonatomic, assign) NSString      *suitCode;
@property (nonatomic, strong) UIImage       *image;

- (TAJRummyCard*)initWithSuit:(NSString *)suit andValue:(NSUInteger)value;

@end
