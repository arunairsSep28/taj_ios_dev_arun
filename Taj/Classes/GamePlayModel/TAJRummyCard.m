/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRummyCard.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/01/14.
 **/

#import "TAJRummyCard.h"

@implementation TAJRummyCard
@synthesize suit=_suit;
@synthesize value = _value;
@synthesize image = _image;
@synthesize suitCode=_suitCode;

- (id)initWithSuit:(NSString *)suit andValue:(NSUInteger)value
{
    self = [super init];
    if(self) {
        self.suit = suit;
        self.value = value;
        NSString *cardImageName = [NSString stringWithFormat:@"%d%@.png",value,[suit uppercaseString]];
        self.image = [UIImage imageNamed:cardImageName];
    }
    return self;
}

@end
