/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRammyCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/01/14.
 **/

#import <UIKit/UIKit.h>

@class TAJRummyCard;
@class TAJRammyCardView;

@interface TAJRammyCardView : UIView

@property (nonatomic, strong) UIImageView * cardImageView;
@property (nonatomic, strong) TAJRummyCard *card;

- (id)initWithCard:(TAJRummyCard *)card;

@end
