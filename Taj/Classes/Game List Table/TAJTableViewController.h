/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/01/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJConstants.h"

@protocol TAJTableViewControllerDelegate;

@interface TAJTableViewController : UIViewController

//IBOutlet
@property (weak, nonatomic) IBOutlet UITableView               *listGameTable;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView   *activityViewShow;

@property(nonatomic) BOOL                                      isClicked;
@property(nonatomic) BOOL                                      awaitingReload;
@property(nonatomic) BOOL                                      isReloadPending;

@property (nonatomic, strong) NSMutableArray                   *filterdTableArray;
@property (nonatomic, strong) NSMutableArray                   *tableArray;
@property (atomic, strong)      NSString                   *mPreviousJoinmessageUUID;
@property (atomic, strong)      NSString                   *mPreviousQuitmessageUUID;

////favourites
//@property (nonatomic, strong) NSString *variantType;
//@property (nonatomic, strong) NSString *selectedGameType;


// the dictionary as lobby table dictionary
@property (nonatomic, weak) NSMutableDictionary              *tableDictionary;

@property (weak, nonatomic) id<TAJTableViewControllerDelegate> tableDelegate;



- (void)reloadTableView;
- (void)reloadTableViewWithOffset;
- (void)revertWaitingReloadTimer;
- (void)startAnimatingActivityIndicator;


- (void)removeAllTableViewData;
//- (void)joinTableWithIndexPath:(NSIndexPath *)indexPath;
- (void)joinTableWithIndexPath:(NSIndexPath *)indexPath selectedArray:(NSArray *)selectedArray;
- (void)removeAllObserver;

- (void)alreadyFilter:(BOOL)boolValue withGameType:(GameTypeStruct)gametype withPlayerType:(PlayerTypeStruct)playerType withBetType:(BetTypeStruct)betType withTableCost:(TableTypeStruct)tableCost withViewTye:(ViewTypeEnum)viewType;

@end

@protocol TAJTableViewControllerDelegate <NSObject>

//ask can join table
- (BOOL)canJoinView;

// if already 2 table is joined
- (void)loadGameTable:(NSDictionary *)tableDictionary;

//if already one table is joined
- (BOOL)ifSingleTableJoined:(NSDictionary *)tableDictionary;
- (void)canJoinSingleTable;
@optional
//to dismis drop down list
- (void)dismisDropDownList;
- (void)tableJoined;

- (void)addToFavouriteWithTableID:(NSString*)tableID;
- (void)removeFromFavouriteWithTableID:(NSString*)tableID;

@end


