/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/01/14.
 **/

#import "TAJTableViewController.h"
#import "TAJTableViewCell.h"
#import "TAJGameEngine.h"
#import "TAJTableDataModel.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJLobby.h"
#import "TAJGameTableController.h"

#define TABLE_GAME_STATUS_OPEN      @"Open:"
#define TABLE_GAME_STATUS_RUNNING   @"Running:"
#define TABLE_GAME_STATUS_SITTING   @"Seating:"

#define TABLE_JOIN_AS_PLAY          @"TAJ_table_join_as"
#define TABLE_JOIN_PLAY             @"play"
#define TABLE_JOIN_MIDDLE             @"middle"

@interface TAJTableViewController ()<UITableViewDataSource,UITableViewDelegate,InfoPopupViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewLowBalanceError;

@property (strong, nonatomic) TAJTableDataModel *dataModel;
@property (nonatomic) BOOL                      isFiltered;
@property(nonatomic,retain) UIViewController *gameScreen;

@property (nonatomic)         GameTypeStruct        gameTypeSearch;
@property (nonatomic)         PlayerTypeStruct      playerTypeSearch;
@property (nonatomic)         BetTypeStruct         betTypeSearch;
@property (nonatomic)         TableTypeStruct         tableCostSearch;
@property (nonatomic)         ViewTypeEnum          viewTypeSearch;

@property (nonatomic, strong) UIImage* playerIndicator;
@property (nonatomic, strong) UIImage* fadedPlayerIndicator;
@property (nonatomic, strong) UIColor* greenColor;
@property (nonatomic, strong) UIColor* orangeColor;
@property (nonatomic, strong) UIColor* grayColor;

@property (strong, nonatomic) TAJInfoPopupViewController  *errorPopUpController;

//favourite list
@property (strong, nonatomic) NSMutableArray *arrayFavouriteTables;

@end

@implementation TAJTableViewController
@synthesize filterdTableArray = _filterdTableArray;
@synthesize gameScreen = _gameScreen;
@synthesize isClicked = _isClicked;
@synthesize dataModel = _dataModel;
@synthesize tableDelegate = _tableDelegate;

#pragma mark UIViewController Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.playerIndicator = [UIImage imageNamed:@"bright_dot.png"];
        self.fadedPlayerIndicator = [UIImage imageNamed:@"fade_dot.png"];
        self.greenColor = [UIColor greenColor];
        self.orangeColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
        self.grayColor = [UIColor colorWithRed:128.0f/255.0f green:130.0f/255.0f blue:132.0f/255.0f alpha:1.0f];
        self.isFiltered = NO;
        self.tableDictionary = [[[TAJGameEngine sharedGameEngine] lobby] tableDetails];
        NSMutableArray *array = Nil;
        NSLog(@"GAME TYPE : %@",self.gameTypeSearch);
        
#if  LIVE_RELEASE
        _tableCostSearch.isFree = YES;
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
        self.filterdTableArray = [NSMutableArray arrayWithArray:array];
#else
        self.filterdTableArray = [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
#endif
        
        self.mPreviousQuitmessageUUID = nil;
        self.mPreviousJoinmessageUUID = nil;
        
        //        [self removeAllObserver];
        //        [self addTableObserver];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.title = NAV_TABLE_TITLE;
    
    self.activityViewShow.hidden = YES;
    //    [self removeAllObserver];
    //    [self addTableObserver];
    self.listGameTable.tableFooterView = [[UIView alloc]
                                          initWithFrame:CGRectZero];
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.viewLowBalanceError.hidden = YES;
    
    [super viewWillAppear:animated];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        // if iOS 7
        self.edgesForExtendedLayout = UIRectEdgeNone; //layout adjustements
    }
    
    self.activityViewShow.hidden = YES;
    self.isClicked = YES;
    self.awaitingReload = NO;
    self.isReloadPending = NO;
    
    self.listGameTable.delegate = self;
    self.listGameTable.dataSource = self;
    
    [self.listGameTable reloadData];
    [self removeAllObserver];
    [self addTableObserver];
}

//added
- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    
    //    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];
    //    [UINavigationController attemptRotationToDeviceOrientation];
    //
    
    //    [[UIDevice currentDevice] setValue:
    //     [NSNumber numberWithInteger: UIInterfaceOrientationLandscapeLeft]
    //                                forKey:@"orientation"];
    
    [UIViewController attemptRotationToDeviceOrientation];
}

//added

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self removeAllObserver];
    //    [self addTableObserver];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self removeAllObserver];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)removeAllObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_JOIN object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_QUIT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TABLE_START object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TABLE_CLOSED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:START_GAME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_SCHEDULE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_END object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ON_SOCKET_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LIST_TABLE_UPDATE_AFTER_RECONNECT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:LIST_TABLE_UPDATE_AFTER_INVALID object:nil];
    
}

- (void)addTableObserver
{
    // Error code
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableViewErrorCode:)
                                                 name:ERROR_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receivePlayerJoinUpdate:)
                                                 name:PLAYER_JOIN
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(receiveTableUpdate:)
                                                 name:PLAYER_QUIT
                                               object:nil];
    
    //Author: RK
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableStarted:)
                                                 name:TABLE_START
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableClosed:)
                                                 name:TABLE_CLOSED
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startGame:)
                                                 name:START_GAME
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gameSchedule:)
                                                 name:GAME_SCHEDULE
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gameEnd:)
                                                 name:GAME_END
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableSocketError:)
                                                 name:ON_SOCKET_ERROR
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateLobbyTableReconnect:)
                                                 name:LIST_TABLE_UPDATE_AFTER_RECONNECT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(updateLobbyTableInvalid:)
                                                 name:LIST_TABLE_UPDATE_AFTER_INVALID
                                               object:nil];
}

- (void)alreadyFilter:(BOOL)boolValue withGameType:(GameTypeStruct)gametype withPlayerType:(PlayerTypeStruct)playerType withBetType:(BetTypeStruct)betType withTableCost:(TableTypeStruct)tableCost withViewTye:(ViewTypeEnum)viewType
{
    if (boolValue)
    {
        self.isFiltered = boolValue;
        _gameTypeSearch = gametype;
        _playerTypeSearch = playerType;
        _betTypeSearch = betType;
        _tableCostSearch = tableCost;
        _viewTypeSearch = viewType;
    }
    else if (!boolValue)
    {
        self.isFiltered = boolValue;
    }
}

#pragma mark Notification observer

- (void)tableSocketError:(NSNotification *) notification
{
    //    self.activityViewShow.hidden = YES;
    self.isClicked= YES;
}

- (void)tableViewErrorCode:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary)
    {
        [self handleTableViewErrorCode:dictionary];
    }
}

- (void)receivePlayerJoinUpdate:(NSNotification *) notification
{
    //update existing player in game if join
    if(NSOrderedSame == [[notification name] compare: PLAYER_JOIN])
    {
        // DLog(@"notification name %@",[notification name]);
        [self updatePlayerJoinNotificationData:[notification object]];
    }
}

- (void)receiveTableUpdate:(NSNotification *) notification
{
    //update existing player in game if quit
    if(NSOrderedSame == [[notification name] compare: PLAYER_QUIT])
    {
        [self updatePlayerQuitNotificationData:[notification object]];
    }
}

- (void)updateLobbyTableReconnect:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self reloadTableView];
    [self.activityViewShow stopAnimating];
    self.activityViewShow.hidden = YES;
}

- (void)updateLobbyTableInvalid:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self reloadTableView];
    [self.activityViewShow stopAnimating];
    self.activityViewShow.hidden = YES;
}

- (void)reloadTableView
{
    NSMutableArray *arraytable = NULL;
    
    self.tableDictionary = [[[TAJGameEngine sharedGameEngine] lobby] tableDetails];
#if  LIVE_RELEASE
    
    _tableCostSearch.isFree = YES;
    arraytable = [TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                withGameType:_gameTypeSearch
                                                 withPlayers:_playerTypeSearch
                                                 withBetType:_betTypeSearch
                                               withTableCost:_tableCostSearch
                                                withViewType:_viewTypeSearch];
    
    
#else
    if (self.isFiltered)
    {
        arraytable = [TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                    withGameType:_gameTypeSearch
                                                     withPlayers:_playerTypeSearch
                                                     withBetType:_betTypeSearch
                                                   withTableCost:_tableCostSearch
                                                    withViewType:_viewTypeSearch];
    }
    else
    {
        arraytable = [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
    
#endif
    
    arraytable = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:arraytable];
    self.filterdTableArray = arraytable;
    
    NSMutableArray *arraytables  = [NSMutableArray new];
   arraytables = [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    self.tableArray = [arraytables mutableCopy];
    
    NSLog(@"TABLE LIST : %@",arraytables);
    NSLog(@"TABLE LIST COUNT: %lu",(unsigned long)arraytables.count);
    //NSLog(@"TABLE LIST COUNT : %lu",(unsigned long)arraytables.count);
    
    //NSLog(@"TABLE LIST : %@",self.filterdTableArray);
    //NSLog(@"TABLE LIST COUNT: %lu",(unsigned long)self.filterdTableArray.count);
    //NSLog(@"TABLE arraytable COUNT: %lu",(unsigned long)self.filterdTableArray.count);
    
    [self.listGameTable reloadData];
    /* - commented fav
    arraytable = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:arraytable];
    self.filterdTableArray = arraytable;
    
    [self filterTablesListArray:self.filterdTableArray];
    
    [self.listGameTable reloadData];
      - commented fav
    */
    
//    NSLog(@"FINAL ARRAY : %@",arraytable);
//
//    for (int j = 0; j < arraytable.count ; j++) {
//        NSMutableDictionary *tableDict = [NSMutableDictionary new];
//        tableDict = arraytable[j];
//
//        if (tableDict[@"TAJ_favorite"] != nil) {
//        }
//        else {
//            [tableDict setObject:[NSNumber numberWithBool:0] forKey:@"TAJ_favorite"];
//            [arraytable replaceObjectAtIndex:j withObject:tableDict];
//            NSLog(@"RELOAD AFTER INSERTION LOBBY LIST : %@",arraytable);
//        }
//    }
//
//    self.arrayFavouriteTables = [NSMutableArray new];
//    self.filterdTableArray = [NSMutableArray new];
//
//    for (int i = 0; i < arraytable.count; i++)
//    {
//        NSMutableDictionary *tableDict = [NSMutableDictionary new];
//
//        BOOL isFavourite = [[arraytable valueForKey:@"TAJ_favorite"][i] boolValue] ;
//
//        //NSLog(@"userId :%d",isFavourite);
//        //NSLog(@"kTaj_scoresheet_userid :%@",userId);
//        if (isFavourite) {
//            tableDict = arraytable[i];
//            NSString * gameType = [tableDict objectForKey:@"TAJ_table_type"];
//            NSLog(@"FAV GAME TYPE :%@",gameType);
//            NSString *stringWithoutSpaces = [gameType
//                                             stringByReplacingOccurrencesOfString:@"_" withString:@" "];
//            NSLog(@"GAME TYPE : %@",[stringWithoutSpaces uppercaseString]);
//            NSLog(@"SELECTED GAME TYPE : %@",[self.selectedGameType uppercaseString]);
//
//            //[arraytable valueForKey:@"TAJ_table_type"][i];
//            //[self.arrayFavouriteTables addObject:tableDict];
//            if ([[stringWithoutSpaces uppercaseString] isEqualToString:[self.selectedGameType uppercaseString]]) {
//                [self.arrayFavouriteTables addObject:tableDict];
//            }
//
//        }
//
//    }
//
//    NSLog(@"FAV ARRAY : %@",self.arrayFavouriteTables);
//    self.filterdTableArray = self.arrayFavouriteTables;
//    if ([self.variantType isEqualToString:FAVOURITES_LABEL_LOBBY]) {
//        self.filterdTableArray = self.arrayFavouriteTables;
//    }
//    else {
//        self.filterdTableArray = arraytable;
//    }
//    NSLog(@"FINAL FILTERED ARRAY : %@",self.filterdTableArray);
//
//    [self.listGameTable reloadData];
    
}

- (void)removeAllTableViewData
{
    [self.filterdTableArray removeAllObjects];
    [self.listGameTable reloadData];
}

- (void)startAnimatingActivityIndicator
{
    self.activityViewShow.hidden = NO;
    [self.activityViewShow startAnimating];
}

- (void)stopAnimatingActivityIndicator
{
    [self.activityViewShow stopAnimating];
    self.activityViewShow.hidden = YES;
}

//Author : RK
#pragma mark Update notification data

- (void)tableStarted:(NSNotification *) notification
{
    NSDictionary *data = NULL;
    if (notification)
    {
        data = [notification object];
    }
    
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    NSMutableArray *array = nil;
    array = [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    //Adding a new table in the table view of lobby
    
    NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
    
    [dictionary setObject:[data objectForKey:TAJ_BET] forKey:TAJ_BET];
    [dictionary setObject:[data objectForKey:TAJ_CURRENT_PLAYER] forKey:TAJ_CURRENT_PLAYER];
    [dictionary setObject:[data objectForKey:TAJ_GAME_SCHEDULE] forKey:TAJ_GAME_SCHEDULE];
    [dictionary setObject:[data objectForKey:TAJ_GAME_START] forKey:TAJ_GAME_START];
    [dictionary setObject:[data objectForKey:TAJ_MAX_BUYIN] forKey:TAJ_MAX_BUYIN];
    [dictionary setObject:[data objectForKey:TAJ_MAX_PLAYER] forKey:TAJ_MAX_PLAYER];
    [dictionary setObject:[data objectForKey:TAJ_MIN_BUYIN] forKey:TAJ_MIN_BUYIN];
    [dictionary setObject:[data objectForKey:TAJ_MIN_PLAYER] forKey:TAJ_MIN_PLAYER];
    [dictionary setObject:[data objectForKey:TAJ_SCHEDULE_NAME] forKey:TAJ_SCHEDULE_NAME];
    [dictionary setObject:[data objectForKey:TAJ_TABLE_COST] forKey:TAJ_TABLE_COST];
    [dictionary setObject:[data objectForKey:TAJ_TABLE_ID] forKey:TAJ_TABLE_ID];
    [dictionary setObject:[data objectForKey:TAJ_TABLE_TYPE] forKey:TAJ_TABLE_TYPE];
    [dictionary setObject:[data objectForKey:TAJ_TOTAL_PLAYER] forKey:TAJ_TOTAL_PLAYER];
    //DLog(@"new table %@",dictionary);
    [array addObject:dictionary]; //Adding a new table to the array
    
    //Checking whether filtered array is to be displayed
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
#else
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
    else
    {
        array = [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
    
    
#endif
    
    //Reloading the filtered array
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    [self.listGameTable reloadData];
    NSLog(@"Filter DATA LIST : %@",self.filterdTableArray);
    //NSLog(@"Filter DATA LIST COUNT: %lu",(unsigned long)self.filterdTableArray.count);
}

#pragma mark - FILTER GAMES LIST
/*
-(NSMutableArray*)filterTablesListArray:(NSMutableArray *)lisTArray {
    
    for (int j = 0; j < lisTArray.count ; j++) {
        NSMutableDictionary *tableDict = [NSMutableDictionary new];
        tableDict = lisTArray[j];
        
        if (tableDict[@"TAJ_favorite"] != nil) {
        }
        else {
            [tableDict setObject:[NSNumber numberWithBool:0] forKey:@"TAJ_favorite"];
            [lisTArray replaceObjectAtIndex:j withObject:tableDict];
           // NSLog(@"AFTER INSERTION LOBBY LIST : %@",lisTArray);
        }
    }
    
    //NSLog(@"AFTER LOBBY LIST : %@",self.filterdTableArray);
    
    self.arrayFavouriteTables = [NSMutableArray new];
    self.filterdTableArray = [NSMutableArray new];
    
    for (int i = 0; i < lisTArray.count; i++)
    {
        NSMutableDictionary *tableDict = [NSMutableDictionary new];
        
        BOOL isFavourite = [[lisTArray valueForKey:@"TAJ_favorite"][i] boolValue] ;
        
        //NSLog(@"userId :%d",isFavourite);
        //NSLog(@"kTaj_scoresheet_userid :%@",userId);
        if (isFavourite) {
            tableDict = lisTArray[i];
            NSString * gameType = [tableDict objectForKey:@"TAJ_table_type"];
            NSLog(@"FAV GAME TYPE :%@",gameType);
            NSString *stringWithoutSpaces = [gameType
                                             stringByReplacingOccurrencesOfString:@"_" withString:@" "];
            NSLog(@"GAME TYPE : %@",[stringWithoutSpaces uppercaseString]);
            NSLog(@"SELECTED GAME TYPE : %@",[self.selectedGameType uppercaseString]);
            
            //[arraytable valueForKey:@"TAJ_table_type"][i];
            //[self.arrayFavouriteTables addObject:tableDict];
            
            if ([self.selectedGameType isEqualToString:@"Select"]) {
                [self.arrayFavouriteTables addObject:tableDict];
            }
            
            if ([[stringWithoutSpaces uppercaseString] isEqualToString:[self.selectedGameType uppercaseString]]) {
                [self.arrayFavouriteTables addObject:tableDict];
            }
            
        }
        
    }
    
    self.filterdTableArray = self.arrayFavouriteTables;
    if ([self.variantType isEqualToString:FAVOURITES_LABEL_LOBBY]) {
        self.filterdTableArray = self.arrayFavouriteTables;
       // NSLog(@"FILTERED FAV");
    }
    else {
        self.filterdTableArray = lisTArray;
       // NSLog(@"FILTERED ONLY");
    }
    
    //NSLog(@"LIST ARRAY : %@",self.filterdTableArray);
    
    return self.filterdTableArray;
}
*/
- (void)tableClosed:(NSNotification *) notification
{
    NSDictionary *data = NULL;
    if (notification)
    {
        data = [notification object];
    }
    
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    
    NSMutableArray *array = Nil;
    
    array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    //Closed table will be removed from the table list
    for (NSMutableDictionary *dict in array)
    {
        NSString *tableID = NULL;
        tableID = [dict objectForKey: TAJ_TABLE_ID];
        if (NSOrderedSame == [tableID compare: tableIDFromNotif])
        {
            [array removeObject:dict]; //Deleting a new table from the array
            break;
        }
    }
    
    //Checking whether filtered array is to be displayed
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
#else
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
#endif
    //Reloading the filtered array
//    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
//    self.filterdTableArray = array;
//
//    [self filterTablesListArray:self.filterdTableArray];
//
//    [self.listGameTable reloadData];
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    [self.listGameTable reloadData];

    
}

- (void)startGame:(NSNotification *) notification
{
    NSDictionary *data = NULL;
    data = [notification object];
    
    //RK: Code to change game state to Running
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    NSMutableArray *array = Nil;
    array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    for (NSMutableDictionary *dict in array)
    {
        NSString *tableID = NULL;
        tableID = [dict objectForKey: TAJ_TABLE_ID];
        
        if (NSOrderedSame == [tableID compare: tableIDFromNotif])
        {
            [dict setObject:TRUE_VALUE forKey:TAJ_GAME_START];
            break;
        }
    }
    
    //Checking whether filtered array is to be displayed
    
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
#else
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
    
    else
    {
        array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
#endif
    //Reloading the filtered array
//    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
//    self.filterdTableArray = array;
//
//    [self filterTablesListArray:self.filterdTableArray];
//
//    [self.listGameTable reloadData];
    
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    [self.listGameTable reloadData];
}

- (void)gameSchedule:(NSNotification *) notification
{
    NSDictionary *data = NULL;
    data = [notification object];
    
    //RK: Code to change game state to Running
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    NSMutableArray *array = Nil;
    array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    for (NSMutableDictionary *dict in array)
    {
        NSString *tableID = NULL;
        tableID = [dict objectForKey: TAJ_TABLE_ID];
        
        if (NSOrderedSame == [tableID compare: tableIDFromNotif])
        {
            [dict setObject:TRUE_VALUE forKey:TAJ_GAME_SCHEDULE];
            break;
        }
    }
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    //Checking whether filtered array is to be displayed
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
#else
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
    
    else
    {
        array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
    
#endif
    
//    //Reloading the filtered array
//    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
//    self.filterdTableArray = array;
//
//    [self filterTablesListArray:self.filterdTableArray];
//
//    [self.listGameTable reloadData];
    
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    [self.listGameTable reloadData];
}

- (void)gameEnd:(NSNotification *) notification
{
    NSDictionary *data = NULL;
    data = [notification object];
    
    //RK: Code to change game state to Running
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    NSMutableArray *array = Nil;
    array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    for (NSMutableDictionary *dict in array)
    {
        NSString *tableID = NULL;
        tableID = [dict objectForKey: TAJ_TABLE_ID];
        
        if (NSOrderedSame == [tableID compare: tableIDFromNotif])
        {
            [dict setObject:FALSE_VALUE_UPPER forKey:TAJ_GAME_SCHEDULE];
            //            [dict setObject:FALSE_VALUE_UPPER forKey:TAJ_GAME_START];
            break;
        }
    }
    
    //Checking whether filtered array is to be displayed
    
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
    
#else
    
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
    else
    {
        array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
#endif
    //Reloading the filtered array
//    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
//    self.filterdTableArray = array;
//
//    [self filterTablesListArray:self.filterdTableArray];
//
//    [self.listGameTable reloadData];
    
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    [self.listGameTable reloadData];
    
}

- (void)updatePlayerJoinNotificationData:(NSDictionary *)notifyData
{
    NSDictionary *data = NULL;
    data = notifyData;
    
    BOOL shouldReload= NO;
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    NSMutableArray *array = Nil;
    array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    // Code to increment the number of players in a table
    if ([data[TABLE_JOIN_AS_PLAY] isEqualToString:TABLE_JOIN_PLAY] || [data[TABLE_JOIN_AS_PLAY] isEqualToString:TABLE_JOIN_MIDDLE])
    {
        for (NSMutableDictionary *dict in array)
        {
            NSString *tableID = NULL;
            tableID = [dict objectForKey: TAJ_TABLE_ID];
            if (NSOrderedSame == [tableID compare: tableIDFromNotif])
            {
                if (self.mPreviousJoinmessageUUID)
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:TAJ_JOIN_MUUID] isEqualToString:[data objectForKey: TAJ_MSG_UUID]])
                    {
                        DLog(@"1Skipped message %@ table: %@",self.mPreviousQuitmessageUUID,tableID);
                        break;
                    }
                    
                    DLog(@"Pre Skipped message %@ table: %@",self.mPreviousJoinmessageUUID,tableID);
                    if ([[data objectForKey: TAJ_MSG_UUID] isEqualToString:self.mPreviousJoinmessageUUID])
                    {
                        DLog(@"Skipped message %@ table: %@",self.mPreviousJoinmessageUUID,tableID);
                        
                        break;
                    }
                }
                else
                    
                {
                    //                    DLog(@"Not Skipped message added table: %@ Current num:%d",tableID,[[dict objectForKey: TAJ_CURRENT_PLAYER] integerValue]+1);
                    
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[data objectForKey: TAJ_MSG_UUID]] forKey:TAJ_JOIN_MUUID];
                self.mPreviousJoinmessageUUID =[NSString stringWithFormat:@"%@",[data objectForKey: TAJ_MSG_UUID]]  ;
                
                NSNumber *number = [dict objectForKey: TAJ_CURRENT_PLAYER];
                int integer = [number integerValue];
                integer = integer+1; //Increment the players value
                
                shouldReload = YES;
                
                if ( [[dict valueForKey: TAJ_MAX_PLAYER] intValue]<integer)
                {
                    integer = [[dict valueForKey: TAJ_MAX_PLAYER] intValue];
                }
                
                [dict setValue:[NSNumber numberWithInt:integer] forKey: TAJ_CURRENT_PLAYER];
                break;
            }
        }
    }
    
    //Checking whether filtered array is to be displayed
    
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
    
#else
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
    else
    {
        array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
#endif
    //Reloading the filtered array
//    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
//    self.filterdTableArray = array;
//
//    [self filterTablesListArray:self.filterdTableArray];
    
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    
    if (shouldReload && !self.awaitingReload)
    {
        self.awaitingReload = YES;
        [self reloadTableViewWithOffset];
        [self performSelector:@selector(revertWaitingReloadTimer) withObject:self afterDelay:3.0f];
    }
    else if (shouldReload)
    {
        self.isReloadPending = YES;
    }
    
}

- (void)revertWaitingReloadTimer
{
    if (self.isReloadPending)
    {
        [self reloadTableViewWithOffset];
        self.isReloadPending = NO;
    }
    
    self.awaitingReload = NO;
    
}

- (void)reloadTableViewWithOffset
{
    [self.listGameTable reloadData];
    [self.listGameTable setContentOffset:CGPointZero animated:NO];
}

- (void)updatePlayerQuitNotificationData:(NSDictionary *)notifyData
{
    NSDictionary *data = NULL;
    data = notifyData;
    BOOL shouldReload= NO;
    
    NSString *tableIDFromNotif = NULL;
    tableIDFromNotif = [data objectForKey: TAJ_TABLE_ID];
    NSMutableArray *array = Nil;
    
    array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    
    // Code to decrement the number of players in a table
    if ([data[TABLE_JOIN_AS_PLAY] isEqualToString:TABLE_JOIN_PLAY] || [data[TABLE_JOIN_AS_PLAY] isEqualToString:TABLE_JOIN_MIDDLE])
    {
        for (NSMutableDictionary *dict in array)
        {
            NSString *tableID = NULL;
            tableID = [dict objectForKey: TAJ_TABLE_ID];
            if (NSOrderedSame == [tableID compare: tableIDFromNotif])
            {
                NSNumber *number = [dict objectForKey: TAJ_CURRENT_PLAYER];
                
                
                if (self.mPreviousQuitmessageUUID)
                {
                    if ([[[NSUserDefaults standardUserDefaults] objectForKey:TAJ_QUIT_MUUID] isEqualToString:[data objectForKey: TAJ_MSG_UUID]])
                    {
                        DLog(@"1Skipped message %@ table: %@",self.mPreviousQuitmessageUUID,tableID);
                        break;
                    }
                    if (NSOrderedSame == [self.mPreviousQuitmessageUUID compare:[data objectForKey: TAJ_MSG_UUID]])
                    {
                        DLog(@"Skipped message %@ table: %@",self.mPreviousQuitmessageUUID,tableID);
                        break;
                    }
                }
                else
                    
                {
                    //                    DLog(@"Not Skipped message subtracted table: %@ Current num:%d",tableID,[number integerValue]-1);
                    
                }
                
                [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",[data objectForKey: TAJ_MSG_UUID]] forKey:TAJ_QUIT_MUUID];
                
                self.mPreviousQuitmessageUUID = [NSString stringWithFormat:@"%@",[data objectForKey: TAJ_MSG_UUID]]  ;
                
                int integer = [number integerValue];
                if (integer > 0)
                {
                    integer = integer - 1; //decrement the players value
                    [dict setValue:[NSNumber numberWithInt:integer] forKey: TAJ_CURRENT_PLAYER];
                    
                    shouldReload = YES;
                    
                    
                    NSString * gameStartVal=[data objectForKey:TAJ_GAME_START];
                    
                    if (integer>0 && integer == 1 && (NSOrderedSame == [gameStartVal compare:TRUE_VALUE]))
                    {
                        [dict setObject:FALSE_VALUE forKey:TAJ_GAME_START];
                    }
                    // DLog(@"Player Quit %@",dict);
                    
                }
                
                
                break;
            }
        }
    }
    
    //Checking whether filtered array is to be displayed
#if  LIVE_RELEASE
    _tableCostSearch.isFree = YES;
    array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                          withGameType:_gameTypeSearch
                                                                           withPlayers:_playerTypeSearch
                                                                           withBetType:_betTypeSearch
                                                                         withTableCost:_tableCostSearch
                                                                          withViewType:_viewTypeSearch]];
    
#else
    if (self.isFiltered)
    {
        array = [NSMutableArray arrayWithArray:[TAJUtilities getFilteredArrayForDictionary:self.tableDictionary
                                                                              withGameType:_gameTypeSearch
                                                                               withPlayers:_playerTypeSearch
                                                                               withBetType:_betTypeSearch
                                                                             withTableCost:_tableCostSearch
                                                                              withViewType:_viewTypeSearch]];
    }
    else
    {
        array = (NSMutableArray *) [self.tableDictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    }
#endif
    //Reloading the filtered array
//    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
//    self.filterdTableArray = array;
//
//    [self filterTablesListArray:self.filterdTableArray];
    
    array = [[TAJUtilities sharedUtilities] sortTablesArraybyCurrentPlayerAndGameStart:array];
    self.filterdTableArray = array;
    
    if (shouldReload && !self.awaitingReload)
    {
        self.awaitingReload = YES;
        [self reloadTableViewWithOffset];
        [self performSelector:@selector(revertWaitingReloadTimer) withObject:self afterDelay:3.0f];
    }
    else if (shouldReload)
    {
        self.isReloadPending = YES;
    }
    
}

- (void)handleTableViewErrorCode:(NSDictionary *)dictionary
{
   // NSLog(@"ERROR DIC : %@",dictionary);
    
    if (!dictionary)
    {
        return;
    }
    
    [self processError:[dictionary[kTaj_Error_code] integerValue]];
}

#pragma mark -UITableView Delegate Datasource-

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.filterdTableArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"CustomCellReuseID";
    
    TAJTableViewCell *cell = (TAJTableViewCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    if (cell == nil)
    {
        NSArray *topLevelObjects = nil;
        topLevelObjects = [[NSBundle mainBundle] loadNibNamed:TAJTableViewCellNibName owner:self options:nil];
        
        for (id currentObject in topLevelObjects)
        {
            if ([currentObject isKindOfClass:[UITableViewCell class]])
            {
                cell = (TAJTableViewCell *)currentObject;
                break;
            }
        }
        
        cell.backgroundColor = [UIColor clearColor];
    }
    
    //cell.selectionStyle = UITableViewCellSelectionStyleBlue;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    self.dataModel = [[TAJTableDataModel alloc]initWithTableDictionary:[self.filterdTableArray objectAtIndex:indexPath.row]];
    //NSLog(@"TABLE PLAYERS DATA : %@",[self.dataModel currentPlayers]);
    
    [cell.tableIndicator1 setImage:self.fadedPlayerIndicator];
    [cell.tableIndicator2 setImage:self.fadedPlayerIndicator];
    [cell.tableIndicator3 setImage:self.fadedPlayerIndicator];
    [cell.tableIndicator4 setImage:self.fadedPlayerIndicator];
    [cell.tableIndicator5 setImage:self.fadedPlayerIndicator];
    [cell.tableIndicator6 setImage:self.fadedPlayerIndicator];
    
    cell.gameType.text = [self.dataModel tableType];
    //cell.chipsType.text = [NSString stringWithFormat:@"Chips: %@",[self.dataModel tableCost]];
    //NSLog(@"CHIPS : %@",[self.dataModel tableCost]);
    if ([[self.dataModel tableCost] isEqualToString:@"CASH_CASH"]) {
        cell.chipsType.text = @"Chips: Cash";
    }
    else {
        cell.chipsType.text = @"Chips: Free";
    }
    cell.betTitle.text =[NSString stringWithFormat:@"Bet: %@",[self.dataModel bet]];
    cell.minBuyIn.text = [self.dataModel minBuyin];
    
    cell.noOfPlayerTitle.text = [self.dataModel currentPlayers];
    //NSLog(@"Current Players : %@",self.dataModel.currentPlayer);
    if ([self.dataModel.currentPlayer isEqualToString:@"0"]) {
        cell.imgViewStatus.image = [UIImage imageNamed:@"group_icon_grey.png"];
        cell.noOfPlayerTitle.textColor = [UIColor grayColor];
        cell.statusGameLabel.textColor = [UIColor grayColor];
    }
    else {
        cell.imgViewStatus.image = [UIImage imageNamed:@"group_icon_orange.png"];
        cell.noOfPlayerTitle.textColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
        cell.statusGameLabel.textColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
    }
    
   // NSLog(@"FAVOUTITE STATUS : %d",self.dataModel.favorite);
//    if ([[dataArray objectAtIndex:indexPathSet.row] valueForKey:@"SetEntries"] != nil)
    if (self.dataModel.favorite) {
        cell.imgViewFavourite.image = [UIImage imageNamed:@"favourite_red"];
    }
    else {
        cell.imgViewFavourite.image = [UIImage imageNamed:@"favourite_white"];
    }
    
    cell.tableIdTitle.text = [NSString stringWithFormat:@"TID: %@",[self.dataModel tableId]];
    cell.maxPlayerTitle.text = [self.dataModel maxPlayer];
    cell.totalPlayers.text = [self.dataModel totalPlayer];
    
    BOOL isTwoPlayer = ([cell.maxPlayerTitle.text intValue] == 2);
    [cell.tableIndicator3 setHidden:isTwoPlayer];
    [cell.tableIndicator4 setHidden:isTwoPlayer];
    [cell.tableIndicator5 setHidden:isTwoPlayer];
    [cell.tableIndicator6 setHidden:isTwoPlayer];
    
    switch ([cell.noOfPlayerTitle.text intValue])
    {
        case 1:
            [cell.tableIndicator1 setImage:self.playerIndicator];
            break;
        case 2:
            [cell.tableIndicator1 setImage:self.playerIndicator];
            [cell.tableIndicator2 setImage:self.playerIndicator];
            break;
        case 3:
            [cell.tableIndicator1 setImage:self.playerIndicator];
            [cell.tableIndicator2 setImage:self.playerIndicator];
            [cell.tableIndicator3 setImage:self.playerIndicator];
            break;
        case 4:
            [cell.tableIndicator1 setImage:self.playerIndicator];
            [cell.tableIndicator2 setImage:self.playerIndicator];
            [cell.tableIndicator3 setImage:self.playerIndicator];
            [cell.tableIndicator4 setImage:self.playerIndicator];
            break;
        case 5:
            [cell.tableIndicator1 setImage:self.playerIndicator];
            [cell.tableIndicator2 setImage:self.playerIndicator];
            [cell.tableIndicator3 setImage:self.playerIndicator];
            [cell.tableIndicator4 setImage:self.playerIndicator];
            [cell.tableIndicator5 setImage:self.playerIndicator];
            break;
        case 6:
            [cell.tableIndicator1 setImage:self.playerIndicator];
            [cell.tableIndicator2 setImage:self.playerIndicator];
            [cell.tableIndicator3 setImage:self.playerIndicator];
            [cell.tableIndicator4 setImage:self.playerIndicator];
            [cell.tableIndicator5 setImage:self.playerIndicator];
            [cell.tableIndicator6 setImage:self.playerIndicator];
            break;
        default:
            break;
    }
    if ([[self.dataModel currentPlayers] integerValue] == 0) // When no players present in table
    {
        cell.statusGameLabel.text = TABLE_GAME_STATUS_OPEN;
    }
    else if([[self.dataModel maxPlayer] integerValue] == 2) // considering 2 players
    {
        if ((NSOrderedSame ==  [[self.dataModel gameStart] compare:TRUE_VALUE]))
        {
            cell.statusGameLabel.text = TABLE_GAME_STATUS_RUNNING;
        }
        else if([[self.dataModel currentPlayers] integerValue] >= 1) // considering players more then 1 as sitting state
        {
            cell.statusGameLabel.text = TABLE_GAME_STATUS_SITTING;
            
        }
    }
    else if([[self.dataModel maxPlayer] integerValue] == 6)// considering for 6 players
    {
        if([[self.dataModel gameStart] isEqualToString:TRUE_VALUE])
        {
            cell.statusGameLabel.text = TABLE_GAME_STATUS_RUNNING;
        }
        else if ([[self.dataModel currentPlayers] integerValue] >= 1) // considering players more then 1 as sitting state
        {
            cell.statusGameLabel.text = TABLE_GAME_STATUS_SITTING;
        }
    }
    
    [cell.joinButton addTarget:self action:@selector(checkButtonTapped:event:) forControlEvents:UIControlEventTouchUpInside];
    
    cell.btnFavourite.tag = indexPath.row;
    [cell.btnFavourite addTarget:self action:@selector(favouriteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return NO;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([TAJUtilities isIPhone]) {
        return 40.0f;
    } else {
        return 62.0f;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 0.0f;
}


- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath
{
   // [self joinTableWithIndexPath:indexPath selectedArray:[self.filterdTableArray objectAtIndex:indexPath.row]];
    [self joinTableWithIndexPath:indexPath];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(self.tableDelegate && [self.tableDelegate respondsToSelector:@selector(dismisDropDownList)])
    {
        [self.tableDelegate dismisDropDownList];
    }
        
   // [self joinTableWithIndexPath:indexPath selectedArray:[self.filterdTableArray objectAtIndex:indexPath.row]];
    [self joinTableWithIndexPath:indexPath];
}

//- (void)joinTableWithIndexPath:(NSIndexPath *)indexPath selectedArray:(NSArray *)selectedArray
- (void)joinTableWithIndexPath:(NSIndexPath *)indexPath
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
            if (self.isClicked)
            {
                BOOL canJoin = YES;
                
                //check from delegate whether to join table
                if ([self.tableDelegate respondsToSelector:@selector(canJoinView)])
                {
                    canJoin  = [self.tableDelegate canJoinView];
                }
                
                if(self.filterdTableArray.count >0)
                {
                    self.dataModel = [[TAJTableDataModel alloc] initWithTableDictionary:[self.filterdTableArray objectAtIndex:indexPath.row]];
                   // self.dataModel = [[TAJTableDataModel alloc] initWithTableDictionary:[selectedArray objectAtIndex:indexPath.row]];
                }
                
                //NSLog(@"CHECKING DATA : %@",[self.dataModel tableDictionary]);
                //NSLog(@"Fav TableiD : %@",[[self.dataModel tableDictionary] objectForKey:@"TAJ_tableid"]);

                NSString * gameType = self.dataModel.tableId;
                NSLog(@"JOINING TABLE ID %@",gameType);
                
                if ([gameType isEqualToString:@"CASH_CASH"]) {
                    APP_DELEGATE.gameType = CASH_GAME_LOBBY;
                }
                else {
                    APP_DELEGATE.gameType = FREE_GAME_LOBBY;
                }
                
                //NSLog(@"CHECKING TYPE : %@",[[self.dataModel tableDictionary] objectForKey:@"TAJ_table_cost"]);
                
                int minumumBuyIN = [[[self.dataModel tableDictionary] objectForKey:@"TAJ_minimumbuyin"] intValue];
                int realChips = [[[[TAJGameEngine sharedGameEngine] usermodel]realChips] intValue];
                NSString * tableType = [[self.dataModel tableDictionary] objectForKey:@"TAJ_table_type"];
                //NSLog(@"minumumBuyIN %d",minumumBuyIN);
                //NSLog(@"tableType %@",tableType);
                //NSLog(@"gameType %@",gameType);
                //NSLog(@"realChips %d",realChips);
                
                if (([tableType isEqualToString:@"PR_JOKER"]) && [gameType isEqualToString:@"CASH_CASH"] && (realChips < minumumBuyIN)) {
                    self.viewLowBalanceError.hidden = NO;
                }
                else {
                    //NSLog(@"NOT PRJOCKER");
                //ask delegate can join for play if either of 2 game table is empty
                if (canJoin)
                {
                    BOOL singleTableJoined = NO;
                    if ([self.tableDelegate respondsToSelector:@selector(ifSingleTableJoined:)])
                    {
#if DEBUG
                        NSLog(@"check 1");
#endif
                        singleTableJoined  = [self.tableDelegate ifSingleTableJoined:[self.dataModel tableDictionary]];
                    }
                    
                    if (singleTableJoined)
                    {
#if DEBUG
                        NSLog(@"check 2");
#endif
                        if ([self.tableDelegate respondsToSelector:@selector(canJoinSingleTable)])
                        {
                            [self.tableDelegate canJoinSingleTable];
                        }
                    }
                    else if(!singleTableJoined)
                    {
#if DEBUG
                        NSLog(@"check 3");
#endif
                        [[TAJGameEngine sharedGameEngine] getTableDetailsAndJoinTableAsView:[self.dataModel tableDictionary]];
                        self.activityViewShow.hidden = NO;
                        [self.activityViewShow startAnimating];
                        self.isClicked = NO;
                    }
                }
                else
                {
#if DEBUG
                    NSLog(@"check 4");
#endif
                    if ([self.tableDelegate respondsToSelector:@selector(loadGameTable:)])
                    {
                        [self.tableDelegate loadGameTable:[self.dataModel tableDictionary]];
                    }
                }
            }
        }
    }
}

- (void)checkButtonTapped:(id)sender event:(id)event
{
    NSSet *touches = [event allTouches];
    UITouch *touch = [touches anyObject];
    CGPoint currentTouchPosition = [touch locationInView:self.listGameTable];
    
    NSIndexPath *indexPath = [self.listGameTable indexPathForRowAtPoint: currentTouchPosition];
    if (indexPath != nil)
    {
        [self tableView:self.listGameTable accessoryButtonTappedForRowWithIndexPath: indexPath];
    }
}

- (void)favouriteClicked:(UIButton *)sender {
    
    TAJTableDataModel * favDataModel = [[TAJTableDataModel alloc]initWithTableDictionary:[self.filterdTableArray objectAtIndex:sender.tag]];
    NSString * favouriteTableID = [NSString stringWithFormat:@"%@",favDataModel.tableId];
    NSString * favouriteTableCost = [NSString stringWithFormat:@"%@",favDataModel.tableCost];

    NSLog(@"Fav TableiD : %@",favouriteTableID);
    
    if (favDataModel.favorite) {
        [self removeFromFavouriteWithTableID:favouriteTableID tableCost:favouriteTableCost];
        favDataModel.favorite = NO;
    }
    else {
        [self addToFavouriteWithTableID:favouriteTableID tableCost:favouriteTableCost];
        favDataModel.favorite = YES;
    }
    
    //for update list after favourite changes
    
    for (int i = 0; i < self.filterdTableArray.count; i++) {
        NSMutableDictionary *tableInfo = self.filterdTableArray[i];

        NSString * tableID = tableInfo[@"TAJ_table_id"];
        
        if ([favouriteTableID isEqualToString:tableID]) {
            BOOL favourtite = [tableInfo[@"TAJ_favorite"] boolValue];
            if (favourtite) {
                favourtite = false;
            }
            else {
                favourtite = true;
            }
            [tableInfo setValue:[NSNumber numberWithBool:favourtite] forKey:@"TAJ_favorite"];
            [self.filterdTableArray replaceObjectAtIndex:sender.tag withObject:tableInfo];
        }
    }
    
    [self reloadTableView];
    [self.listGameTable reloadData];
    
}

- (void)addToFavouriteWithTableID:(NSString*)tableID tableCost:(NSString *)tableCost {
    NSString *userId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] stringValue];
    NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
    [[TAJGameEngine sharedGameEngine] addToFavouriteWithTableID:tableID nickName:name userID:userId tableCost:tableCost];
}

- (void)removeFromFavouriteWithTableID:(NSString*)tableID tableCost:(NSString *)tableCost{
    NSString *userId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] stringValue];
    NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
    [[TAJGameEngine sharedGameEngine] removeFromFavouriteWithTableID:tableID nickName:name userID:userId tableCost:tableCost];
}

#pragma mark - IBActions

//action to load lobby when player is playing games
- (IBAction)okAction:(UIButton *)sender
{
    if (sender.tag == 1000) {
        //deposit
        self.viewLowBalanceError.hidden = YES;
//        NSString * url = [NSString stringWithFormat:@"%@%@",BASE_URL,SENDPAYMENTREQUEST];
//        UIApplication *application = [UIApplication sharedApplication];
//        NSURL *URL = [NSURL URLWithString:url];
//        [application openURL:URL options:@{} completionHandler:^(BOOL success) {
//            if (success) {
//                //NSLog(@"Opened url : %@",url);
//            }
//        }];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"closePopUp" object:nil];
    }
    else {
        self.viewLowBalanceError.hidden = YES;
    }
}

#pragma mark - Error handling -

- (void)processError:(int)errorCode
{
    NSLog(@"ERROR CODE CHECK :%d ",errorCode);
    
    switch (errorCode)
    {
        case ePLAYER_ALREADY_INPLAY:
        {
            //504
            self.activityViewShow.hidden = YES;
            self.isClicked = YES;
        }
            break;
            
        case eENGINE_UNDER_MAINTENANCE:
        {
            //100
            self.activityViewShow.hidden = YES;
            self.isClicked = NO;
            
            //[self showAlertOnServerError:@"Enginer is Under Maintanance"];
        }
            break;
            
        case ePLAYER_ALREADY_INVIEW:
        {
            //505
            self.activityViewShow.hidden = YES;
            self.isClicked = YES;
            
        }
            break;
        default:
            self.activityViewShow.hidden = YES;
            self.isClicked = YES;
            break;
    }
}

- (void)showAlertOnServerError:(NSString *)message
{
    if (!self.errorPopUpController)
    {
        self.errorPopUpController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:message];
        
        [self.errorPopUpController show];
    }
}

#pragma Info_PopUp

- (void)okButtonPressedForInfoPopup:(UIButton *)sender
{
    [self.errorPopUpController hide];
    self.errorPopUpController = nil;
}

@end
