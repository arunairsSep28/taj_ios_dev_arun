
#import "TAJTableViewCell.h"

@interface TAJTableViewCell ()

@end

@implementation TAJTableViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.betTitle =[[UILabel alloc]init];
        self.chipsType =[[UILabel alloc]init];
        self.noOfPlayerTitle = [[UILabel alloc]init];
        self.maxPlayerTitle = [[UILabel alloc]init];
        self.tableIdTitle = [[UILabel alloc]init];
        self.statusGameLabel = [[UILabel alloc] init];
        self.joinButton = [[UIButton alloc]init];
        self.totalPlayers = [[UILabel alloc]init];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJTableViewCell*)init
{
    self.gameType = [[UILabel alloc]init];
    self.chipsType = [[UILabel alloc] init];
    self.betTitle = [[UILabel alloc]init];
    self.minBuyIn = [[UILabel alloc]init];
    self.noOfPlayerTitle = [[UILabel alloc]init];
    self.maxPlayerTitle = [[UILabel alloc]init];
    self.tableIdTitle = [[UILabel alloc]init];
    self.statusGameLabel = [[UILabel alloc] init];
    self.joinButton = [[UIButton alloc]init];
    self.totalPlayers =[[UILabel alloc]init];
    
    return self;
}

@end
