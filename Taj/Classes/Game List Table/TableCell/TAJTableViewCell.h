/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableViewCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 22/01/14.
 **/


#import <UIKit/UIKit.h>

@interface TAJTableViewCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel   *gameType;
@property (strong, nonatomic) IBOutlet UILabel   *chipsType;
@property (strong, nonatomic) IBOutlet UILabel   *betTitle;
@property (strong, nonatomic) IBOutlet UILabel   *minBuyIn;
@property (strong, nonatomic) IBOutlet UILabel   *maxPlayerTitle;
@property (strong, nonatomic) IBOutlet UILabel   *tableIdTitle;
@property (strong, nonatomic) IBOutlet UILabel   *noOfPlayerTitle;
@property (strong, nonatomic) IBOutlet UILabel   *statusGameLabel;
@property (strong, nonatomic) IBOutlet UIButton  *joinButton;
@property (weak, nonatomic) IBOutlet UIImageView *tableIndicator1;
@property (weak, nonatomic) IBOutlet UIImageView *tableIndicator2;
@property (weak, nonatomic) IBOutlet UIImageView *tableIndicator3;
@property (weak, nonatomic) IBOutlet UIImageView *tableIndicator4;
@property (weak, nonatomic) IBOutlet UIImageView *tableIndicator5;
@property (weak, nonatomic) IBOutlet UIImageView *tableIndicator6;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewTableIndicator;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewFavourite;
@property (strong, nonatomic) IBOutlet UILabel *totalPlayers;
@property (weak, nonatomic) IBOutlet UIButton *btnFavourite;

-(TAJTableViewCell*)init;

@end
