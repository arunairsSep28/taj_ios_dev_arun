//
//  TAJViewController.h
//  Taj
//
//  Created by Raghavendra Kamat on 07/01/14.
//  Copyright (c) 2014 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TAJHomePageViewController.h"
#import "TAJBaseViewController.h"

@interface TAJFilterViewController : TAJBaseViewController

@property(nonatomic,strong) NSMutableArray *arrayFilter;

@property(retain,nonatomic) TAJHomePageViewController *prevController;



@end
