//
//  TAJViewController.m
//  Taj
//
//  Created by Raghavendra Kamat on 07/01/14.
//  Copyright (c) 2014 Robosoft Technologies. All rights reserved.
//

#import "TAJFilterViewController.h"
#import "TAJConstants.h"
#import "TAJRadioButton.h"
#import "TAJGameEngine.h"
#import "TAJLobby.h"
#import "TAJTableViewController.h"
#import "TAJUtilities.h"
#import "TAJSwitchLobbyViewController.h"


@interface TAJFilterViewController ()<TAJRadioButtonDelegate>

//search view properties
@property (weak, nonatomic) IBOutlet UISegmentedControl         *gameTypeSegment;
@property (weak, nonatomic) IBOutlet UIView                     *gameTypeView;
@property (weak, nonatomic) IBOutlet UISegmentedControl         *tableTypeSegement;
@property (weak, nonatomic) IBOutlet UISegmentedControl         *viewTypeSegement;

@property (weak, nonatomic) IBOutlet UIView                     *playerTypeView;

@property (weak, nonatomic) IBOutlet UIView                     *betTypeView;

@property (weak, nonatomic) IBOutlet UIButton                   *searchButton;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView    *activityIndicator;
@property (weak, nonatomic) IBOutlet UIButton *buyinButton;

- (IBAction)filterData:(id)sender;
- (IBAction)selectGameType:(id)sender;
- (IBAction)selectTableType:(UISegmentedControl *)sender;
- (IBAction)selectViewType:(UISegmentedControl *)sender;
- (IBAction)buyinAction:(UIButton *)sender;

//layout radio button
@property (nonatomic, strong) TAJRadioButton        *pool101;
@property (nonatomic, strong) TAJRadioButton        *pool201;
@property (nonatomic, strong) TAJRadioButton        *poolBestof3;
@property (nonatomic, strong) TAJRadioButton        *twoPlayerRadio;
@property (nonatomic, strong) TAJRadioButton        *sixPlayerRadio;
@property (nonatomic, strong) TAJRadioButton        *lowBetRadio;
@property (nonatomic, strong) TAJRadioButton        *mediumBetRadio;
@property (nonatomic, strong) TAJRadioButton        *highBetRadio;

@property (nonatomic)         GameTypeStruct        gameTypeSearch;
@property (nonatomic)         PlayerTypeStruct      playerTypeSearch;
@property (nonatomic)         BetTypeStruct         betTypeSearch;
@property (nonatomic)         TableTypeStruct         tableCostSearch;
@property (nonatomic)         ViewTypeEnum          viewTypeSearch;
@property (nonatomic)         int                   selectedIndex;

@property (nonatomic, strong) UILabel               *label101;
@property (nonatomic, strong) UILabel               *label201;
@property (nonatomic, strong) UILabel               *labelBest3;

//pass filter data to TableView
@property (nonatomic, strong) TAJTableViewController *gameListTable;

//switch lobby
@property (nonatomic, strong) TAJSwitchLobbyViewController *switchLobbyView;

@end

@implementation TAJFilterViewController
@synthesize playerTypeView = _playerTypeView;
@synthesize gameTypeView = _gameTypeView;
@synthesize arrayFilter = _arrayFilter;
@synthesize gameListTable = _gameListTable;
@synthesize prevController = _prevController;
@synthesize activityIndicator = _activityIndicator;
@synthesize switchLobbyView = _switchLobbyView;

#pragma mark
#pragma mark UIViewController life cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = NAV_SEARCH_TITLE;
    
    [self initializeDefaultValues];
    
    [self selectGameType:0];
    
    [self updateRadioButtonUI:NO_OF_PLAYER withSegmentIndex:0];
    [self updateRadioButtonUI:BET_TYPE withSegmentIndex:0];
    
    [self.twoPlayerRadio setButtonState];
    [self.lowBetRadio setButtonState];
    
    self.tableTypeSegement.transform = CGAffineTransformRotate(self.tableTypeSegement.transform,270.0/180*M_PI);
    self.viewTypeSegement.transform = CGAffineTransformRotate(self.viewTypeSegement.transform,270.0/180*M_PI);
    
    self.tableTypeSegement.selectedSegmentIndex = 0;
    [self selectTableType:self.tableTypeSegement];
    
    self.viewTypeSegement.selectedSegmentIndex = 0;
    [self selectViewType:self.viewTypeSegement];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        // if iOS 7
        self.edgesForExtendedLayout = UIRectEdgeNone; //layout adjustements
    }

    [self.activityIndicator stopAnimating];
    self.activityIndicator.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Initialize view -

-(void)initializeDefaultValues
{
    self.label101 = [[UILabel alloc]init];
    self.label201 = [[UILabel alloc]init];
    self.labelBest3 = [[UILabel alloc]init];
}

#pragma mark - Search and Filter Server data -

- (IBAction)filterData:(id)sender
{
    NSArray *array = NULL;
    
    if (self.selectedIndex == 1)
    {
        // selected Index = 1 means 101, 201 and best_of_3
        if ((_gameTypeSearch.isOneNotOne == NO) && (_gameTypeSearch.isTwoNotOne == NO) && (_gameTypeSearch.isBestOfThree == NO))
        {
            _gameTypeSearch.isOneNotOne = YES;
            _gameTypeSearch.isTwoNotOne = YES;
            _gameTypeSearch.isBestOfThree = YES;
        }
    }
    else if (self.selectedIndex == 2)
    {
        // selected Index = 2 means best_of_2 and best_of_6
        if ((_gameTypeSearch.isBestOfTwo == NO) && (_gameTypeSearch.isBestOfSix == NO))
        {
            _gameTypeSearch.isBestOfTwo = YES;
            _gameTypeSearch.isBestOfSix = YES;
        }
    }
    else if (self.selectedIndex == 3)
    {
        // selected Index = 3 means joker and no-joker
        if ((_gameTypeSearch.isJoker == NO) && (_gameTypeSearch.isNoJoker == NO))
        {
            _gameTypeSearch.isJoker = YES;
            _gameTypeSearch.isNoJoker = YES;
        }
    }
    
    if ((_playerTypeSearch.isTwoPlayer == NO) && (_playerTypeSearch.isSixPlayer == NO))
    {
        _playerTypeSearch.isTwoPlayer = YES;
        _playerTypeSearch.isSixPlayer = YES;
    }
    
    if ((_betTypeSearch.isLowBet == NO) && (_betTypeSearch.isMediumBet == NO) && (_betTypeSearch.isHighBet == NO))
    {
        _betTypeSearch.isLowBet = YES;
        _betTypeSearch.isMediumBet = YES;
        _betTypeSearch.isHighBet = YES;
    }
    
    array = [TAJUtilities getFilteredArrayForDictionary:[[[TAJGameEngine sharedGameEngine] lobby] tableDetails]
                                           withGameType:_gameTypeSearch
                                            withPlayers:_playerTypeSearch
                                            withBetType:_betTypeSearch
                                          withTableCost:_tableCostSearch
                                           withViewType:_viewTypeSearch];
    
    self.arrayFilter = [array mutableCopy];
    
    //filter server data
    if ([self.arrayFilter count] > 0)
    {
        self.activityIndicator.hidden = NO;
        [self.activityIndicator startAnimating];
    
        if ([TAJUtilities isIPhone])
        {
            self.switchLobbyView = [[TAJSwitchLobbyViewController alloc] init];
        }
        else
        {
            self.switchLobbyView = [[TAJSwitchLobbyViewController alloc] init];
        }
        
        [self.switchLobbyView alreadyFilter:YES withGameType:_gameTypeSearch
                             withPlayerType:_playerTypeSearch
                                withBetType:_betTypeSearch
                              withTableCost:_tableCostSearch withViewTye:_viewTypeSearch];
        
        self.switchLobbyView.filterdTableArray = [self.arrayFilter mutableCopy];
        
        [self.navigationController pushViewController:self.switchLobbyView animated:YES];
    }
    else
    {
        [[[UIAlertView alloc] initWithTitle:@"Alert" message:@"No games available" delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil] show];
    }
    
}

#pragma mark - Segment Control action -

- (IBAction)selectGameType:(id)sender
{
    if (self.gameTypeSegment.selectedSegmentIndex == 0 || sender == 0)
    {
        _gameTypeSearch.isBestOfTwo = NO;
        _gameTypeSearch.isBestOfSix = NO;
        _gameTypeSearch.isJoker = NO;
        _gameTypeSearch.isNoJoker = NO;
        
        [self updateRadioButtonUI:GAME_TYPE withSegmentIndex:0];
        
        [self.pool101 setButtonState];
        self.selectedIndex = 1;
    }
    if(self.gameTypeSegment.selectedSegmentIndex == 1)
    {
        _gameTypeSearch.isOneNotOne = NO;
        _gameTypeSearch.isTwoNotOne = NO;
        _gameTypeSearch.isBestOfThree = NO;
        _gameTypeSearch.isJoker = NO;
        _gameTypeSearch.isNoJoker = NO;
        
        [self updateRadioButtonUI:GAME_TYPE  withSegmentIndex:1];
        
        [self.pool101 setButtonState];
        self.selectedIndex = 2;
        
    }
    if(self.gameTypeSegment.selectedSegmentIndex == 2)
    {
        _gameTypeSearch.isOneNotOne = NO;
        _gameTypeSearch.isTwoNotOne = NO;
        _gameTypeSearch.isBestOfThree = NO;
        _gameTypeSearch.isBestOfTwo = NO;
        _gameTypeSearch.isBestOfSix = NO;
        
        [self updateRadioButtonUI:GAME_TYPE  withSegmentIndex:2];
        
        [self.pool101 setButtonState];
        self.selectedIndex = 3;
    }
    
}

- (IBAction)selectTableType:(UISegmentedControl *)sender
{
    
    _tableCostSearch.isFree = NO;
    _tableCostSearch.isCash = NO;
    _tableCostSearch.isVIP = NO;
    _tableCostSearch.isBlaze = NO;
    _tableCostSearch.isHappy = NO;
    
    if (self.tableTypeSegement.selectedSegmentIndex == 0)
    {
        _tableCostSearch.isCash =YES;
    }
    if (self.tableTypeSegement.selectedSegmentIndex == 1)
    {
        _tableCostSearch.isFree =YES;
    }
    
    if (self.tableTypeSegement.selectedSegmentIndex == 2)
    {
        _tableCostSearch.isVIP =YES;
    }
    
    if (self.tableTypeSegement.selectedSegmentIndex == 3)
    {
        _tableCostSearch.isBlaze =YES;
    }
    
    if (self.tableTypeSegement.selectedSegmentIndex == 4)
    {
        _tableCostSearch.isHappy =YES;
    }
}

- (IBAction)selectViewType:(UISegmentedControl *)sender
{
    if (self.viewTypeSegement.selectedSegmentIndex == 0)
    {
        _viewTypeSearch = ePlayGame;
    }
    if (self.viewTypeSegement.selectedSegmentIndex == 1)
    {
        _viewTypeSearch = eWatchGame;
    }
}

- (IBAction)buyinAction:(UIButton *)sender
{
    
}

#pragma mark - Update UI depends on game type -

-(void)updateRadioButtonUI:(NSString *)groupID withSegmentIndex:(int)segmentIndex
{
    if ([groupID isEqualToString:GAME_TYPE])
    {
        if (segmentIndex == 0)
        {
            // for pool rummy 101, 201 and best_of_3
            self.pool101 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:ONE_NOT_ONE];
            self.pool201 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:TWO_NOT_ONE];
            self.poolBestof3 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:BEST_OF_THREE];
            
            self.pool101.frame = CGRectMake( self.gameTypeView.bounds.origin.x, self.gameTypeView.bounds.origin.y,
                                            RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            self.pool201.frame = CGRectMake( self.gameTypeView.bounds.origin.x+90, self.gameTypeView.bounds.origin.y,
                                            RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            self.poolBestof3.frame = CGRectMake( self.gameTypeView.bounds.origin.x+180, self.gameTypeView.bounds.origin.y,
                                                RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            
            [self.gameTypeView addSubview: self.pool101];
            [self.gameTypeView addSubview: self.pool201];
            [self.gameTypeView addSubview: self.poolBestof3];
            
            self.label101.frame = CGRectMake(self.gameTypeView.bounds.origin.x+25,
                                            self.gameTypeView.bounds.origin.y, 60, 20);
            self.label101.backgroundColor = [UIColor clearColor];
            self.label101.text = @"";
            self.label101.text = ONE_NOT_ONE_LABEL;
            [self.gameTypeView addSubview:self.label101];
            
            
            self.label201.frame =CGRectMake(self.gameTypeView.bounds.origin.x+115,
                                            self.gameTypeView.bounds.origin.y, 60, 20);
            self.label201.backgroundColor = [UIColor clearColor];
            self.label201.text = @" ";
            self.label201.text = TWO_NOT_ONE_LABEL;
            [self.gameTypeView addSubview:self.label201];
            
            
            self.labelBest3.frame =CGRectMake(self.gameTypeView.bounds.origin.x+205,
                                              self.gameTypeView.bounds.origin.y, 90, 20);
            self.labelBest3.backgroundColor = [UIColor clearColor];
            self.labelBest3.text = @" ";
            self.labelBest3.text = BEST_OF_THREE_LABEL;
            self.labelBest3.hidden = NO;
            [self.gameTypeView addSubview: self.labelBest3];
            
            [TAJRadioButton addObserverForGroupId:GAME_TYPE observer:self];
            
        }
        else if(segmentIndex == 1)
        {
            // for deals rummy best_of_2 and best_of_6
            self.pool101 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:BEST_OF_TWO];
            self.pool201 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:BEST_OF_SIX];
            
            self.pool101.frame = CGRectMake( self.gameTypeView.bounds.origin.x, self.gameTypeView.bounds.origin.y,
                                            RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            self.pool201.frame = CGRectMake( self.gameTypeView.bounds.origin.x+90, self.gameTypeView.bounds.origin.y,
                                            RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            
            [self.gameTypeView addSubview: self.pool101];
            [self.gameTypeView addSubview: self.pool201];
            self.poolBestof3.hidden = YES;
            self.labelBest3.hidden = YES;
            
            self.label101.frame = CGRectMake(self.gameTypeView.bounds.origin.x+25,
                                            self.gameTypeView.bounds.origin.y, 90, 20);
            self.label101.backgroundColor = [UIColor clearColor];
            self.label101.text = @" ";
            self.label101.text = BEST_OF_TWO_LABEL;
            [self.gameTypeView addSubview: self.label101];
            
            
            self.label201.frame = CGRectMake(self.gameTypeView.bounds.origin.x+115,
                                            self.gameTypeView.bounds.origin.y, 90, 20);
            self.label201.backgroundColor = [UIColor clearColor];
            self.label201.text = @" ";
            self.label201.text = BEST_OF_SIX_LABEL;
            [self.gameTypeView addSubview:self.label201];
            
            
            [TAJRadioButton addObserverForGroupId:GAME_TYPE observer:self];
           
        }
        else
        {
            // for points rummy joker and non joker
            self.pool101 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:GAME_JOKER];
            self.pool201 = [[TAJRadioButton alloc] initWithGroupId:GAME_TYPE index:GAME_NO_JOKER];
            
            self.pool101.frame = CGRectMake(self.gameTypeView.bounds.origin.x, self.gameTypeView.bounds.origin.y,
                                            RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            self.pool201.frame = CGRectMake(self.gameTypeView.bounds.origin.x+90, self.gameTypeView.bounds.origin.y,
                                            RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
            
            [self.gameTypeView addSubview: self.pool101];
            [self.gameTypeView addSubview: self.pool201];
            self.poolBestof3.hidden = YES;
            self.labelBest3.hidden = YES;
            
            self.label101.frame =CGRectMake(self.gameTypeView.bounds.origin.x+25,
                                            self.gameTypeView.bounds.origin.y, 70, 20);
            self.label101.backgroundColor = [UIColor clearColor];
            self.label101.text = @" ";
            self.label101.text = GAME_JOKER_LABEL;
            [self.gameTypeView addSubview:self.label101];
            
            
            self.label201.frame = CGRectMake(self.gameTypeView.bounds.origin.x+115,
                                            self.gameTypeView.bounds.origin.y, 70, 20);
            self.label201.backgroundColor = [UIColor clearColor];
            self.label201.text=@" ";
            self.label201.text = GAME_NO_JOKER_LABEL;
            [self.gameTypeView addSubview:self.label201];
            
            [TAJRadioButton addObserverForGroupId:GAME_TYPE observer:self];
            
        }
    }
    else if([groupID isEqualToString:NO_OF_PLAYER])
    {
        // for no.of players
        self.twoPlayerRadio = [[TAJRadioButton alloc] initWithGroupId:NO_OF_PLAYER index:TWO_PLAYER];
        self.sixPlayerRadio = [[TAJRadioButton alloc] initWithGroupId:NO_OF_PLAYER index:SIX_PLAYER];
        
        self.twoPlayerRadio.frame = CGRectMake(self.playerTypeView.bounds.origin.x, self.playerTypeView.bounds.origin.y,
                                     RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
        self.sixPlayerRadio.frame = CGRectMake(self.playerTypeView.bounds.origin.x+90, self.playerTypeView.bounds.origin.y,
                                     RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
        
        [self.playerTypeView addSubview: self.twoPlayerRadio];
        [self.playerTypeView addSubview: self.sixPlayerRadio];
        
        UILabel *label101 = [[UILabel alloc] initWithFrame:CGRectMake(self.playerTypeView.bounds.origin.x+25,
                                                                      self.playerTypeView.bounds.origin.y, 60, 20)];
        label101.backgroundColor = [UIColor clearColor];
        label101.text = TWO_PLAYER_LABEL;
        [self.playerTypeView addSubview: label101];
        
        
        UILabel *label201 = [[UILabel alloc] initWithFrame:CGRectMake(self.playerTypeView.bounds.origin.x+115,
                                                                      self.playerTypeView.bounds.origin.y, 60, 20)];
        label201.backgroundColor = [UIColor clearColor];
        label201.text = SIX_PLAYER_LABEL;
        [self.playerTypeView addSubview:label201];
        
        [TAJRadioButton addObserverForGroupId:NO_OF_PLAYER observer:self];
        
    }
    else
    {
        // for bet
        self.lowBetRadio = [[TAJRadioButton alloc] initWithGroupId:BET_TYPE index:LOW_BET];
        self.mediumBetRadio = [[TAJRadioButton alloc] initWithGroupId:BET_TYPE index:MEDIUM_BET];
        self.highBetRadio = [[TAJRadioButton alloc] initWithGroupId:BET_TYPE index:HIGH_BET];
        
        self.lowBetRadio.frame = CGRectMake(self.betTypeView.bounds.origin.x, self.betTypeView.bounds.origin.y,
                                   RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
        self.mediumBetRadio.frame = CGRectMake(self.betTypeView.bounds.origin.x+90, self.betTypeView.bounds.origin.y,
                                   RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
        self.highBetRadio.frame = CGRectMake(self.betTypeView.bounds.origin.x+180, self.betTypeView.bounds.origin.y,
                                       RADIO_BUTTON_WIDTH, RADIO_BUTTON_HEIGHT);
        
        [self.betTypeView addSubview: self.lowBetRadio];
        [self.betTypeView addSubview: self.mediumBetRadio];
        [self.betTypeView addSubview: self.highBetRadio];
        
        UILabel *label101 = [[UILabel alloc] initWithFrame:CGRectMake(self.betTypeView.bounds.origin.x+25,
                                                                     self.betTypeView.bounds.origin.y, 60, 20)];
        label101.backgroundColor = [UIColor clearColor];
        label101.text = LOW_BET_LABEL;
        [self.betTypeView addSubview: label101];
        
        
        UILabel *label201 = [[UILabel alloc] initWithFrame:CGRectMake(self.betTypeView.bounds.origin.x+115,
                                                                     self.betTypeView.bounds.origin.y, 75, 20)];
        label201.backgroundColor = [UIColor clearColor];
        label201.text = MEDIUM_BET_LABEL;
        [self.betTypeView addSubview: label201];
        
        
        UILabel *labelBest3 =[[UILabel alloc] initWithFrame:CGRectMake(self.betTypeView.bounds.origin.x+220,
                                                                       self.betTypeView.bounds.origin.y, 80, 20)];
        labelBest3.backgroundColor = [UIColor clearColor];
        labelBest3.text = HIGH_BET_LABEL;
        [self.betTypeView addSubview: labelBest3];
        
        [TAJRadioButton addObserverForGroupId:BET_TYPE observer:self];

    }
}

#pragma mark - Radio Button Delegate -

-(void)radioButtonSelectedAtIndex:(NSUInteger)index inGroup:(NSString *)groupId
{
    if ([groupId isEqualToString:GAME_TYPE])
    {
        if (index == ONE_NOT_ONE)
        {
            _gameTypeSearch.isOneNotOne = YES;
        }
        else if(index == TWO_NOT_ONE)
        {
            
            _gameTypeSearch.isTwoNotOne = YES;
        }
        else if (index == BEST_OF_THREE)
        {
            
            _gameTypeSearch.isBestOfThree = YES;
        }
        else if(index == BEST_OF_TWO)
        {
            _gameTypeSearch.isBestOfTwo = YES;
            
        }
        else if(index == BEST_OF_SIX)
        {
            _gameTypeSearch.isBestOfSix = YES;
            
        }
        else if(index == GAME_JOKER)
        {
            _gameTypeSearch.isJoker = YES;
        }
        else
        {
            _gameTypeSearch.isNoJoker = YES;
        }
    }
    else if([groupId isEqualToString:NO_OF_PLAYER])
    {
        if(index == TWO_PLAYER)
        {
            _playerTypeSearch.isTwoPlayer = YES;
        }
        else
        {
            _playerTypeSearch.isSixPlayer = YES;
        }
    }
    else
    {
        if(index == LOW_BET)
        {
            _betTypeSearch.isLowBet = YES;
        }
        else if(index == MEDIUM_BET)
        {
            _betTypeSearch.isMediumBet = YES;
        }
        else
        {
            _betTypeSearch.isHighBet = YES;
        }
        
    }
    DLog(@"Selected state called");
    
}

-(void)radioButtonUnSelectedAtIndex:(NSUInteger)index inGroup:(NSString *)groupId
{
    if ([groupId isEqualToString:GAME_TYPE])
    {
        if (index == ONE_NOT_ONE)
        {
            
            _gameTypeSearch.isOneNotOne = NO;
        }
        else if(index == TWO_NOT_ONE)
        {
            _gameTypeSearch.isTwoNotOne = NO;
        }
        else if (index == BEST_OF_THREE)
        {
            _gameTypeSearch.isBestOfThree = NO;
        }
        else if(index == BEST_OF_TWO)
        {
            _gameTypeSearch.isBestOfTwo = NO;
            
        }
        else if(index == BEST_OF_SIX)
        {
            _gameTypeSearch.isBestOfSix = NO;
            
        }
        else if(index == GAME_JOKER)
        {
            _gameTypeSearch.isJoker = NO;
        }
        else
        {
            _gameTypeSearch.isNoJoker = NO;
        }

    }
    else if([groupId isEqualToString:NO_OF_PLAYER])
    {
        if(index == TWO_PLAYER)
        {
            _playerTypeSearch.isTwoPlayer = NO;
        }
        else
        {
            _playerTypeSearch.isSixPlayer = NO;
        }
    }
    else
    {
        if(index == LOW_BET)
        {
            _betTypeSearch.isLowBet = NO;
        }
        else if(index == MEDIUM_BET)
        {
            _betTypeSearch.isMediumBet = NO;
        }
        else
        {
            _betTypeSearch.isHighBet = NO;
        }
        
    }
    DLog(@"Unselected state called");
}

@end
