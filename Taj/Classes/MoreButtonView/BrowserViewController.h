//
//  BrowserViewController.h
//  TajRummy
//
//  Created by Grid Logic on 11/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BrowserViewController : UIViewController
@property (strong, nonatomic) NSString *url;

@end

NS_ASSUME_NONNULL_END
