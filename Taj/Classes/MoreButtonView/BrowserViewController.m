//
//  BrowserViewController.m
//  TajRummy
//
//  Created by Grid Logic on 11/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "BrowserViewController.h"

@interface BrowserViewController ()<UIWebViewDelegate>
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end

@implementation BrowserViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    NSString *urlAddress = @"http://www.google.com";//self.url;
    NSURL *url = [[NSURL alloc] initWithString:urlAddress];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    
    [self.webView loadRequest:requestObj];
    
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}


@end
