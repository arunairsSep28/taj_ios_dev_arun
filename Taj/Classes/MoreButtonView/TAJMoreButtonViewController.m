/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMoreButtonViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Pradeep BM on 12/05/14.
 Created by  Yogisha Poojary on 12/05/14.
 **/

#import "TAJMoreButtonViewController.h"
#import "TAJGameEngine.h"
#import "MoreTableViewCell.h"
#import "WebViewController.h"
#import "BrowserViewController.h"

#define LOYALTY_CLUB_PLAYER_LEVEL_ONE   @"CORAL"
#define LOYALTY_CLUB_PLAYER_LEVEL_TWO   @"SAPPHIRE"
#define LOYALTY_CLUB_PLAYER_LEVEL_THREE  @"RUBY"
#define LOYALTY_CLUB_PLAYER_LEVEL_FOUR   @"EMERALD"
#define LOYALTY_CLUB_PLAYER_LEVEL_FIVE   @"DIAMONDS"

@interface TAJMoreButtonViewController ()
@property (weak, nonatomic) IBOutlet UIView *moreButtonViewHolder;
@property (weak, nonatomic) IBOutlet UILabel *moreButtonUserId;
@property (weak, nonatomic) IBOutlet UILabel *moreButtonPlayMoney;
@property (weak, nonatomic) IBOutlet UIButton *logoutButton;
- (IBAction)logoutButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UILabel *loyaltyPointsValue;
@property (weak, nonatomic) IBOutlet UILabel *loyaltyClubValue;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayMenu;

@end

@implementation TAJMoreButtonViewController

static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"More Screen"];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeMoreButtonView)];
    tapGesture.numberOfTapsRequired = 1;
    [self.moreButtonViewHolder addGestureRecognizer:tapGesture];
    [self addObserver];
    
//    self.arrayMenu = @[@"My Account",@"Banking",@"Account History",@"Unique Features",@"Promotions",@"Help and Support",@"About Us",@"Logout"];
//    self.arrayMenu = @[@"My Account",@"Help and Support",@"About Us",@"Logout"];
    //self.arrayMenu = @[@"My Account",@"Deposit",@"About Us",@"Logout"];
    
    self.arrayMenu = @[@"My Account",@"Banking",@"Account History",@"About Us",@"Logout"];
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];

}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    TAJUserModel *model = engine.usermodel;
    
    self.moreButtonUserId.text = [model nickName];
    self.moreButtonPlayMoney.text = [model totalGamesPlayed];
    self.loyaltyPointsValue.text = [model loyaltyChips];
    if ([[model playerLevel] isEqualToString:@"1"])
    {
        self.loyaltyClubValue.text = [NSString stringWithFormat:LOYALTY_CLUB_PLAYER_LEVEL_ONE];
    }
    else if ([[model playerLevel] isEqualToString:@"2"])
    {
        self.loyaltyClubValue.text = [NSString stringWithFormat:LOYALTY_CLUB_PLAYER_LEVEL_TWO];
    }
    else if ([[model playerLevel] isEqualToString:@"3"])
    {
        self.loyaltyClubValue.text = [NSString stringWithFormat:LOYALTY_CLUB_PLAYER_LEVEL_THREE];
    }
    else if ([[model playerLevel] isEqualToString:@"4"])
    {
        self.loyaltyClubValue.text = [NSString stringWithFormat:LOYALTY_CLUB_PLAYER_LEVEL_FOUR];
    }
    else if ([[model playerLevel] isEqualToString:@"5"])
    {
        self.loyaltyClubValue.text = [NSString stringWithFormat:LOYALTY_CLUB_PLAYER_LEVEL_FIVE];
    }
}

- (void)addObserver
{
    [self removeObserver];

//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(requestJoinTable:)
//                                                 name:REQUEST_JOIN_TABLE
//                                               object:nil];
//
}

- (void)removeObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REQUEST_JOIN_TABLE object:nil];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self removeObserver];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self removeObserver];
}

- (BOOL)isMoreButtonViewVisible
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}


- (void)closeMoreButtonView
{
    if (self.moreButtonDelegate && [self.moreButtonDelegate respondsToSelector:@selector(removeMoreButtonControllerView)])
    {
        [self.moreButtonDelegate removeMoreButtonControllerView];
    }
}

- (IBAction)logoutButtonPressed:(id)sender
{
    if (self.moreButtonDelegate && [self.moreButtonDelegate respondsToSelector:@selector(didPressedLogOutPressed)])
    {
        [self.moreButtonDelegate didPressedLogOutPressed];
    }
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone])
    {
    return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithTitle:self.arrayMenu[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        MyAccountViewController * viewController = [[MyAccountViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 1) {
        BankingViewController * viewController = [[BankingViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 2) {
        AccountHistoryViewController * viewController = [[AccountHistoryViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
//    else if (indexPath.row == 3) {
//        PromotionsViewController * viewController = [[PromotionsViewController alloc] init];
//        [self presentViewController:viewController animated:NO completion:nil];
//    }
    else if (indexPath.row == 3) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"About US" url:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,ABOUTUS]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 4) {
        if (self.moreButtonDelegate && [self.moreButtonDelegate respondsToSelector:@selector(didPressedLogOutPressed)])
        {
            [self.moreButtonDelegate didPressedLogOutPressed];
        }
    }
    
    //    else if (indexPath.row == 1) {
    //        BankingViewController * viewController = [[BankingViewController alloc] init];
    //        [self presentViewController:viewController animated:NO completion:nil];
    //    }
    
    //    else if (indexPath.row == 3) {
    //        UniqueFeaturesViewController * viewController = [[UniqueFeaturesViewController alloc] init];
    //        [self presentViewController:viewController animated:NO completion:nil];
    //    }
    //    else if (indexPath.row == 4) {
    //        PromotionsViewController * viewController = [[PromotionsViewController alloc] init];
    //        [self presentViewController:viewController animated:NO completion:nil];
    //    }
    //    else if (indexPath.row == 1) {
    //        HelpViewController * viewController = [[HelpViewController alloc] init];
    //        [self presentViewController:viewController animated:NO completion:nil];
    //    }
}
    

@end
