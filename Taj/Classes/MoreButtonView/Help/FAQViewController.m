//
//  FAQViewController.m
//  Taj Rummy
//
//  Created by svc on 13/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "FAQViewController.h"
#import "FAQTableViewCell.h"

@interface FAQViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayFAQ;

@end

@implementation FAQViewController
static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayFAQ = @[@{
                           @"question" : @"Is it legal to play online rummy in India?",
                           @"answer" : @"Yes, it is absolutely legal to play online rummy in India. The Indian Supreme Court has deemed Rummy as a game of skill and not a game of chance, and is protected under the Constitution of India under Article 19[1](g)."
                           },
                       @{
                           @"question" : @"Are my personal and payment card details secure at Taj Rummy?",
                           @"answer" : @"With responsible gaming, fraud protection and 24/7 support, we aim at delivering a user friendly and secure online gaming experience at Taj Rummy."
                           
                           },
                       @{ @"question" : @"What do I do if I don't have a PAN Card?",
                          @"answer" : @"Your profile at Taj Rummy is only complete when you enter your PAN Card details into your account. You will need to log in to your account, go to My Account (on the right hand side bar) and click on Profile Overview to get to your profile, where you can update your account with other essential details that were not recorded during the Quick Sign Up process.Your profile consists of four sections: Validation Information, Personal Information, Address Information and Other Information. PAN Card number is a mandatory field in the last section (Other Information). Click on the Edit button below all the sections to simply enter your PAN Card number into your profile. If for any reason you do not have a PAN Card, you may enter UNIRV1234N into this field. You will then be able to carry out transactions like depositing money into your Taj Rummy account, placing bets in cash games and Withdrawals of winnings successfully. "
                          
                          },
                          @{
                              @"question" : @"How do I open an account at Taj Rummy?",
                              @"answer" : @"Opening a player account at Taj Rummy is very simple. All you have to do is fill up the Quick Sign Up form on our homepage. You will need to provide us with a unique username and password which you'll easily remember, then fill in your email address, birth date, state and gender. We suggest you enter your Mobile number in order to get notifications on your phone. It only takes a minute to create an account here, and then you're ready to play Rummy."
                           },
                       @{
                           @"question" : @"How do I access my account?",
                           @"answer" : @"Enter your username and password in the corresponding boxes provided on the top right corner of the homepage (and every other page on the site). Then click on the Login button to access your Taj Rummy account and begin playing."
                           },
                       @{
                           @"question" : @"Am I old enough to play at Taj Rummy?",
                           @"answer" : @"Taj Rummy does not allow players less than 18 years of age to sign up and play games at the site."
                           },
                       @{
                           @"question" : @"What if I forget my account password?",
                           @"answer" : @"If you have forgotten your Taj Rummy login details, click on Forgot Password? under the login panel. You will be asked to enter your email address recorded during the time of registration. An email will be sent to your inbox with instructions to set up a new password. For more details, please read:http://blog.tajrummy.com/how-to-retrieve-your-password/"
                           },
                       @{
                           @"question" : @"How do I see my account details?",
                           @"answer" : @"You may see and even edit your Taj Rummy account details by logging into your account and clicking on the 'My Account' tab to see your account details.For more details, please read:http://blog.tajrummy.com/how-to-fill-your-profile-page/"
                           },
                       @{
                           @"question" : @"How can I make a deposit?",
                           @"answer" : @"Login to your account and click on the Buy Cash Chips tab followed by Make a Deposit. Please select your payment method on the left and enter the correct amount that you wish to deposit. If you have a bonus code, enter it in the box provided and then click on 'Next' to proceed further.For more details, please read:http://blog.tajrummy.com/how-to-make-a-deposit/"
                           },
                       @{
                           @"question" : @"What is the minimum deposit amount?",
                           @"answer" : @"The minimum deposit amount at a given time at Taj Rummy is Rs. 100."
                           },
                       @{
                           @"question" : @"What is the maximum deposit amount?",
                           @"answer" : @"The maximum deposit limit at agiven time at Taj Rummy is Rs. 20,000."
                           },
                       @{
                           @"question" : @"What is the minimum withdrawal amount?",
                           @"answer" : @"The minimum withdrawal amount at a given time at Taj Rummy is Rs. 100."
                           },
                       @{
                           @"question" : @"What is the maximum withdrawal amount?",
                           @"answer" : @"The maximum withdrawal amount at a given time at Taj Rummy is Rs. 10,000."
                           },
                       @{
                           @"question" : @"How long will it take to receive my withdrawal?",
                           @"answer" : @"Once your submitted withdrawal is processed at our end, it may take between 2 to 5 working days for you to receive it, depending on your bank."
                           },
                       @{
                           @"question" : @"What are the variants of Rummy available to play on Taj Rummy?",
                           @"answer" : @"At Taj Rummy, you can play the following different Rummy variants:\n\nPools Rummy: 101, 201, Best of 3.\nPoints/Strikes Rummy: With Joker, Without Joker.\nDeals Rummy: Best of 2, Best of 6.\nTournaments."
                           },
                       @{
                           @"question" : @"What if my internet connection is lost while playing games at Taj Rummy?",
                           @"answer" : @"If you lose your internet connection in the middle of a game at Taj Rummy, your game will be set to Auto Play for the duration of disconnection. Once the connection is resumed, you can continue playing."
                           },
                       @{
                           @"question" : @"Can I play Rummy without downloading any software?",
                           @"answer" : @"Taj Rummy is a no-download Rummy game site. Once you register an account here, all you need to do is log in to your account and click on the Play Now button to get th the games lobby where you can choose a rummy varient, join a table and start playing."
                           }];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"FAQTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];

}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
   
        return YES;
    
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayFAQ.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
    return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FAQTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithData:self.arrayFAQ[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    FAQDetailViewController * viewController = [[FAQDetailViewController alloc] initWithData:self.arrayFAQ[indexPath.row]];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

@end
