//
//  EmailUsViewController.m
//  Taj Rummy
//
//  Created by svc on 13/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "EmailUsViewController.h"

@interface EmailUsViewController ()
@property (weak, nonatomic) IBOutlet UITextField *txtFieldUsername;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldQuery;
@property (weak, nonatomic) IBOutlet UITextView *txtViewQuery;

@property (weak, nonatomic) IBOutlet UIView *viewThankyou;
@property (strong, nonatomic) NSPredicate *emailPredicate;

@end

@implementation EmailUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"EmailUs Screen"];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    self.emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexEmail];

    NSString * username = [[NSUserDefaults standardUserDefaults]objectForKey:USENAME_KEY];
    NSString * email = [[NSUserDefaults standardUserDefaults]objectForKey:USEREMAIL_KEY];
    self.txtFieldUsername.text = username;
    self.txtFieldEmail.text = email;
    
    [self configureView];
    [self checkEmail];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
        return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.viewThankyou.hidden = YES;
    self.txtViewQuery.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldUsername.text length] == 0) {
        [self.view makeToast:@"Username can not be empty"];
        return YES;
    }
    
    else if ([self.txtFieldEmail.text length] == 0) {
        [self.view makeToast:@"Email can not be empty"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail.text]) && ([self.txtFieldEmail.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    else if ([self.txtFieldQuery.text length] == 0) {
        [self.view makeToast:@"Query can not be empty"];
        return YES;
    }
    
    else if ([self.txtViewQuery.text length] == 0) {
        [self.view makeToast:@"Detailed query can not be empty"];
        return YES;
    }
    
    return NO;
}

-(void)checkEmail {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_EMAIL];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    //NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            //NSLog(@"CHECK MOBILE Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                // enable user to edit mail
                [self.txtFieldEmail setUserInteractionEnabled:YES];
            }
            
            else {
                [self.txtFieldEmail setUserInteractionEnabled:NO];
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"CHECKEMAIL ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

#pragma mark - ACTION

- (IBAction)backClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)submitClicked:(UIButton *)sender {
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{
                                 @"name" : self.txtFieldUsername.text,
                                 @"subject" : self.txtFieldQuery.text,
                                 @"message" : self.txtViewQuery.text,
                                 @"email": self.txtFieldEmail.text
                                 
                                 };
#if DEBUG
        NSLog(@"EMAILUS PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,SUPPORT];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"EMAILUS RESPONSE: %@", jsondata);
#endif
                            id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                            
                            NSDictionary *userData=@{
                                                     @"userId":userId
                                                     };
                            [weAnalytics trackEventWithName:@"SupportQueryRaised" andValue:userData];
                            
                            self.viewThankyou.hidden = NO;
                            
                        }
                        else {
                            NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
        
    }
}


@end
