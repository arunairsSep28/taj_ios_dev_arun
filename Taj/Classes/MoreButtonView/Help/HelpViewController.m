//
//  BankingViewController.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "HelpViewController.h"

@interface HelpViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayMenu;

@end

@implementation HelpViewController
static NSString *const kCellIdentifier = @"cellIdentifier";
static const CGFloat kCellHeightiPad = 84;
static const CGFloat kCellHeightTitle = 54;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Help Screen"];
    
//    self.arrayMenu = @[@"How To Play",@"Help/FAQ's",@"Email us",@"Terms / Conditions",@"Privacy Policy",@"Legality"];
    
    self.arrayMenu = @[@"How To Play",@"Help/FAQ's",@"Terms & Conditions",@"Privacy Policy",@"Legality"];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    //[[Crashlytics sharedInstance] crash];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
        return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithTitle:self.arrayMenu[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"How To Play" url:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,HOWTOPLAY]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 1) {
        FAQViewController * viewController = [[FAQViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
//    else if (indexPath.row == 2) {
//        EmailUsViewController * viewController = [[EmailUsViewController alloc] init];
//        [self presentViewController:viewController animated:NO completion:nil];
//    }
    else if (indexPath.row == 2) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"Terms & Conditions" url:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,TERMS]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 3) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"Privacy Policy" url:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,PRIVACY]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 4) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"Legality" url:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,LEGALITY]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
}

@end
