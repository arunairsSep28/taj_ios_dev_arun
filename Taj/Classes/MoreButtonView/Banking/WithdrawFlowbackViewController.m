//
//  WithdrawFlowbackViewController.m
//  Taj Rummy
//
//  Created by svc on 15/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "WithdrawFlowbackViewController.h"

@interface WithdrawFlowbackViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *arrayFlowback;

@end

@implementation WithdrawFlowbackViewController
static NSString *const kCellIdentifier = @"cellIdentifier";
static NSString *const kHeaderCellIdentifier = @"HeaderCellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"WithdrawFlowbackTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"WithdrawFlowbackHeaderTableViewCell" bundle:nil] forCellReuseIdentifier:kHeaderCellIdentifier];
    
    [self getFlowbackData];
}

#pragma mark -  HELPERS

-(void)getFlowbackData {

    [self.view makeToastActivity:CSToastPositionCenter];

    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,FLOWBACK];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            NSLog(@"FLOWBACK Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSLog(@"FLOWBACK DATA : %@",[responseData valueForKey:@"data"]);
                self.arrayFlowback = [[NSMutableArray alloc] init];

                for (NSMutableDictionary *areaDic in [responseData valueForKey:@"data"]) {
                    Flowback *areaObject = [Flowback sharedInstance];
                    [areaObject assignData:areaDic];
                    [self.arrayFlowback addObject:areaObject];
                }
                NSLog(@"FLOWBACK Count : %lu",(unsigned long)self.arrayFlowback.count);
                [self.tableView reloadData];

            }
        });

    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        NSLog(@"FLOWBACK ERROR : %@",errorString);
        [self.view makeToast:errorString];
    }];

}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (void)flowBackClicked:(UIButton *)sender
{
    Flowback *flowback = self.arrayFlowback[sender.tag];
    [self.view makeToastActivity:CSToastPositionCenter];

    NSDictionary *params = @{@"flowbackamount":flowback.withdraw_amount,
                             @"orderid":[NSString stringWithFormat:@"%ld",(long)flowback.order_id],
                             @"orderamount":flowback.available_amount
                             };
#if DEBUG
    NSLog(@"LOGIN PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];

    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,FLOWBACK];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                     NSLog(@"RESPONSE: %@", jsondata);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {

#if DEBUG
                        NSLog(@"RESPONSE: %@", jsondata);
#endif
                        [self.arrayFlowback removeObjectAtIndex:sender.tag];
                        [self.tableView reloadData];
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                    else {
                        NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];

}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayFlowback.count;
    
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
    return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WithdrawFlowbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnFlowback.tag = indexPath.row;
    [cell.btnFlowback addTarget:self action:@selector(flowBackClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell updateCellWithyData:self.arrayFlowback[indexPath.row]];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    WithdrawFlowbackTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHeaderCellIdentifier];
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([TAJUtilities isIPhone]) {
        return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

@end
