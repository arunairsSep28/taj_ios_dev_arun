//
//  BankingViewController.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "BankingViewController.h"

@interface BankingViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayMenu;

@end

@implementation BankingViewController
static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;


- (void)viewDidLoad {
    [super viewDidLoad];

    [self configureView];
}

-(void)configureView {
    self.arrayMenu = @[@"Deposit",@"Withdraw Cash",@"Withdraw Flowback"];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
    return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithTitle:self.arrayMenu[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *uniqueID = [[NSUserDefaults standardUserDefaults] objectForKey:UNIQUE_SESSION_KEY];
    if (indexPath.row == 0) {
        NSString * url = [NSString stringWithFormat:@"%@%@?client_type=ios&device_type=%@&unique_id=%@",BASE_URL,SENDPAYMENTREQUEST,[[TAJUtilities sharedUtilities] getDeviceType],uniqueID];
       
        NSLog(@"Opened url : %@",url);
        
        [self openURLWithString:url];
    }
    else if (indexPath.row == 1) {
        WithdrawCashViewController * viewController = [[WithdrawCashViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 2) {
        WithdrawFlowbackViewController * viewController = [[WithdrawFlowbackViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
}

@end
