//
//  WithdrawCashViewController.m
//  Taj Rummy
//
//  Created by svc on 20/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "WithdrawCashViewController.h"

#define Message @"You need to verify your mobile number to request for a withdrawal. Please go to PROFILE to verify."

@interface WithdrawCashViewController ()
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblWithdrawbleBalance;

@property (weak, nonatomic) IBOutlet UILabel *lblWithdrawbleFee;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAmount;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSMutableArray *arrayBanks;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (assign, nonatomic) NSInteger cellCount;

//OTP View
@property (weak, nonatomic) IBOutlet UIView *viewOTPContainer;
@property (weak, nonatomic) IBOutlet UIView *viewOTP;
@property (weak, nonatomic) IBOutlet UILabel *lblOTP;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnGenerateOTP;
@property (weak, nonatomic) IBOutlet UIButton *btnVerify;


@end

static NSString *const kHeaderCellIdentifier = @"HeadercellIdentifier";
static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeight = 54;
static const CGFloat kCellHeightiPad = 84;


static const CGFloat kHeaderCellHeight = 324;
static const CGFloat kHeaderCellHeightiPad = 264;

@implementation WithdrawCashViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.scrollView.hidden = YES;
    self.viewOTPContainer.hidden = YES;
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"WithdrawBankDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:kHeaderCellIdentifier];
    [self.tableView registerNib:[UINib nibWithNibName:@"WithdrawTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [self getWithdrawCashData];
    [super viewWillAppear:animated];
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldAmount.text length] == 0) {
        [self.view makeToast:@"Amount can not be empty"];
        return YES;
    }
    
    return NO;
}

- (BOOL)validateOTPInput
{
    if ([self.txtFieldOTP.text length] == 0) {
        [self.view makeToast:@"Enter OTP"];
        return YES;
    }
    
    return NO;
}

-(void)getWithdrawCashData {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,WITHDRAWCASH];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    NSLog(@"URL : %@",URL);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"WITHDRAWCASH DETAILS Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                if ([[responseData objectForKey:@"redirect"] isEqualToString:@"otp_page"]) {
                    self.scrollView.hidden = YES;
                    self.viewOTPContainer.hidden = NO;
                    self.txtFieldOTP.userInteractionEnabled = NO;
                    self.btnVerify.userInteractionEnabled = NO;
                    self.btnGenerateOTP.userInteractionEnabled = YES;
                    
                    self.txtFieldOTP.userInteractionEnabled = NO;
                    self.lblOTP.text = @"Generate OTP on your registered mobile number";
                    self.txtFieldOTP.text = @"";
                    self.txtFieldOTP.placeholder = @"";
                    self.btnVerify.userInteractionEnabled = NO;
                    self.btnGenerateOTP.userInteractionEnabled = YES;
                    
                    //[self.view makeToast:[responseData valueForKey:@"message"]];
                    //                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                    //                                                                                   message:Message
                    //                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    //
                    //                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                    //                                                                          handler:^(UIAlertAction * action) {
                    //                                                                              [self dismissViewControllerAnimated:NO completion:nil];
                    //                                                                          }];
                    //
                    //                    [alert addAction:defaultAction];
                    //                    [self presentViewController:alert animated:YES completion:nil];
                }
                else {
                    self.scrollView.hidden = NO;
                    self.viewOTPContainer.hidden = YES;
                    self.arrayBanks = [NSMutableArray new];
                    NSDictionary * dataDic = [responseData valueForKey:@"data"];
                    
                    NSLog(@"WITHDRAWCASH DETAILS DATA : %@",dataDic);
                    self.lblWithdrawbleBalance.text = [dataDic valueForKey:@"withdrawable_amount"];
                    self.lblWithdrawbleFee.text = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"withdrawal_fee_amount"]];
                    
                    for(NSMutableDictionary *productsDic in [dataDic valueForKey:@"account_list"]) {
                        BankDetails *bankObject = [BankDetails sharedInstance];
                        [bankObject assignData:productsDic];
                        [self.arrayBanks addObject:bankObject];
                    }
                    
                    self.tableViewHeightConstraint.constant = [self calculateHeightOfTableViewWithIsSelected:NO];
                    [self.tableView reloadData];
                }
            }
            else {
                self.scrollView.hidden = YES;
                //[self.view makeToast:[responseData valueForKey:@"message"]];
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                               message:[responseData valueForKey:@"message"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          [self dismissViewControllerAnimated:NO completion:nil];
                                                                      }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"WITHDRAWCASH DETAILS ERROR : %@",errorString);
        self.scrollView.hidden = YES;
        [self.view makeToast:@"Error while fetching data"];
        [self.view hideToastActivity];
    }];
}

- (float)calculateHeightOfTableViewWithIsSelected:(BOOL )isSelected
{
    float totalHeight = 0.0;
    if ([TAJUtilities isIPhone]) {
        if (isSelected) {
            totalHeight = kHeaderCellHeight * self.arrayBanks.count + 44;
        }
        else {
            totalHeight = kHeaderCellHeight * self.arrayBanks.count;
        }
        
    }
    else {
        if (isSelected) {
            totalHeight = kHeaderCellHeightiPad * self.arrayBanks.count + 84;
        }
        else {
            totalHeight = kHeaderCellHeightiPad * self.arrayBanks.count;
        }
    }
    return totalHeight;
}

-(void)generateOTP
{
    [self hideKeyboard];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,WITHDRAW_OTP_VERIFY];
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"WITHDRAWCASH DETAILS Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                self.txtFieldOTP.userInteractionEnabled = YES;
                self.lblOTP.text = @"OTP has been sent to your registered mobile number.";
                self.txtFieldOTP.text = @"";
                self.txtFieldOTP.placeholder = @"Enter OTP";
                self.btnVerify.userInteractionEnabled = YES;
                self.btnGenerateOTP.userInteractionEnabled = NO;
            }
            
            else {
                [self.view makeToast:[responseData valueForKey:@"message"]];
            }
            
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"WITHDRAWCASH DETAILS ERROR : %@",errorString);
        self.scrollView.hidden = YES;
        [self.view makeToast:@"Error while fetching data"];
        [self.view hideToastActivity];
    }];
}

-(void)verifyOTP
{
    [self hideKeyboard];
    BOOL error = [self validateOTPInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"token": self.txtFieldOTP.text};
#if DEBUG
        NSLog(@"WITHDRAW_OTP_VERIFY PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,WITHDRAW_OTP_VERIFY];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"WITHDRAW_OTP_VERIFY RESPONSE: %@", jsondata);
#endif
                            
                            self.viewOTPContainer.hidden = YES;
                            self.scrollView.hidden = NO;
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                            self.arrayBanks = [NSMutableArray new];
                            NSDictionary * dataDic = [jsondata valueForKey:@"data"];
                            
                            self.lblWithdrawbleBalance.text = [dataDic valueForKey:@"withdrawable_amount"];
                            self.lblWithdrawbleFee.text = [NSString stringWithFormat:@"%@",[dataDic valueForKey:@"withdrawal_fee_amount"]];
                            
                            for(NSMutableDictionary *productsDic in [dataDic valueForKey:@"account_list"]) {
                                BankDetails *bankObject = [BankDetails sharedInstance];
                                [bankObject assignData:productsDic];
                                [self.arrayBanks addObject:bankObject];
                            }
                            
                            self.tableViewHeightConstraint.constant = [self calculateHeightOfTableViewWithIsSelected:NO];
                            [self.tableView reloadData];
                            
                        }
                        else {
                            NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
    }
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)addNewBankClicked:(UIButton *)sender {
    AddBankAccountViewController * viewController = [[AddBankAccountViewController alloc] init];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (void)deleteClicked:(UIButton *)sender
{
    BankDetails *bank = self.arrayBanks[sender.tag];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{
                             @"account_id":[NSString stringWithFormat:@"%ld",(long)bank.bankId]                             };
#if DEBUG
    NSLog(@"LOGIN PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,DELETE_ACCOUNT];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    NSLog(@"DELETE_ACCOUNT RESPONSE: %@", jsondata);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
                        [self.arrayBanks removeObjectAtIndex:sender.tag];
                        
                        self.tableViewHeightConstraint.constant = [self calculateHeightOfTableViewWithIsSelected:NO];
                        
                        [self.tableView reloadData];
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                    else {
                        NSLog(@"DELETE_ACCOUNT Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];
}

- (void)withDrawClicked:(UIButton *)sender
{
    BankDetails *bank = self.arrayBanks[sender.tag];
    
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{
                                 @"amount" : self.txtFieldAmount.text,
                                 @"account_id" :[NSString stringWithFormat:@"%ld",(long)bank.bankId]                             };
#if DEBUG
        NSLog(@"WITHDRAWCASH PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,WITHDRAWCASH];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        NSLog(@"WITHDRAWCASH RESPONSE: %@", jsondata);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            self.txtFieldAmount.text = @"";
                            [self makeEveryBankUnselect];
                            UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                                           message:[jsondata valueForKey:@"message"]
                                                                                    preferredStyle:UIAlertControllerStyleAlert];
                            
                            UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                                  handler:^(UIAlertAction * action) {
                                                                                      [self dismissViewControllerAnimated:NO completion:nil];
                                                                                  }];
                            
                            [alert addAction:defaultAction];
                            [self presentViewController:alert animated:YES completion:nil];
                            
                        }
                        else {
                            NSLog(@"WITHDRAWCASH Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view hideToastActivity];
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
    }
}

- (void)selectClicked:(UIButton *)sender
{
    [self hideKeyboard];
    BankDetails * bank = self.arrayBanks[sender.tag];
    
    if (bank.isSelected) {
        self.tableViewHeightConstraint.constant = [self calculateHeightOfTableViewWithIsSelected:NO];
        [self makeEveryBankUnselect];
        [self.tableView reloadData];
        return;
    }
    
    [self makeEveryBankUnselect];
    bank.isSelected = TRUE;
    self.tableViewHeightConstraint.constant = [self calculateHeightOfTableViewWithIsSelected:YES];
    [self.tableView reloadData];
    
}

- (IBAction)generateClicked:(UIButton *)sender {
    [self generateOTP];
}

- (IBAction)verifyClicked:(UIButton *)sender {
    [self verifyOTP];
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.arrayBanks.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    BankDetails *bank = self.arrayBanks[section];
    if (bank.isSelected) {
        return 1;
    }
    else {
        return 0;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone])
    {
        return kCellHeight;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    WithdrawTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnWithdraw.tag = indexPath.section;
    [cell.btnWithdraw addTarget:self action:@selector(withDrawClicked:) forControlEvents:UIControlEventTouchUpInside];
    return cell;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    WithdrawBankDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kHeaderCellIdentifier];
    cell.btnDelete.tag = section;
    cell.btnSelect.tag = section;
    [cell.btnDelete addTarget:self action:@selector(deleteClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnSelect addTarget:self action:@selector(selectClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell updateCellWithData:self.arrayBanks[section]];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([TAJUtilities isIPhone]) {
        return kHeaderCellHeight;
    }
    else {
        return kHeaderCellHeightiPad;
    }
    
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

- (void)makeEveryBankUnselect
{
    for (BankDetails *bank in self.arrayBanks) {
        bank.isSelected = FALSE;
    }
    [self.tableView reloadData];
}

@end
