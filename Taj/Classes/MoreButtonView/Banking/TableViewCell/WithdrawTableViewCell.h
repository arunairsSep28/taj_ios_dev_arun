//
//  WithdrawTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 22/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnWithdraw;

@end

NS_ASSUME_NONNULL_END
