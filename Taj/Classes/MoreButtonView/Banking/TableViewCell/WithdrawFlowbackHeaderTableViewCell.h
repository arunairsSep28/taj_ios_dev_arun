//
//  WithdrawFlowbackHeaderTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 18/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawFlowbackHeaderTableViewCell : UITableViewCell

@end

NS_ASSUME_NONNULL_END
