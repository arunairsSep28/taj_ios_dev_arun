//
//  WithdrawFlowbackTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 15/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "WithdrawFlowbackTableViewCell.h"

@interface WithdrawFlowbackTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *lblOrderID;
@property (weak, nonatomic) IBOutlet UILabel *lblTimeStamp;
@property (weak, nonatomic) IBOutlet UILabel *lblWithdrawAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblAvailableAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblFlowback;

@end
@implementation WithdrawFlowbackTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithyData:(Flowback *)data
{
    self.lblOrderID.text = [NSString stringWithFormat:@"%ld",(long)data.order_id];
    self.lblTimeStamp.text = data.time;
    self.lblWithdrawAmount.text = data.withdraw_amount;
    self.lblAvailableAmount.text = data.available_amount;
    self.lblFlowback.hidden = YES;
    [self.btnFlowback setUserInteractionEnabled:NO];
    
    if (data.show_button) {
        self.lblFlowback.hidden = NO;
        [self.btnFlowback setUserInteractionEnabled:YES];
    }
}

@end
