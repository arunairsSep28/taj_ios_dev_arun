//
//  WithdrawBankDetailsTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 21/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "WithdrawBankDetailsTableViewCell.h"

@interface WithdrawBankDetailsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *viewBankDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblBankName;
@property (weak, nonatomic) IBOutlet UILabel *lblBankTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblAcHolderName;
@property (weak, nonatomic) IBOutlet UILabel *lblAcNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBankAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UILabel *lblIFSC;
@property (weak, nonatomic) IBOutlet UIImageView *imgSelct;

@end

@implementation WithdrawBankDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)configureView {
    self.viewBankDetails.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)updateCellWithData:(BankDetails *)data
{
    [self configureView];
    self.lblBankTitle.text = [data.bank_name uppercaseString];
    self.lblBankName.text = data.bank_name;
    self.lblBankAddress.text = data.address;
    self.lblAcHolderName.text = data.account_name;
    self.lblAcNumber.text = data.account_number;
    self.lblBankAddress.text = data.address;
    self.lblState.text = data.state;
    self.lblIFSC.text = data.ifsc_code;
    
    if(data.isSelected == TRUE) {
        self.imgSelct.image = [UIImage imageNamed:@"check"];
    }
    else {
        self.imgSelct.image = [UIImage imageNamed:@"uncheck"];
    }
    
}

@end
