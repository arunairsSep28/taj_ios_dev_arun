//
//  WithdrawFlowbackTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 15/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Flowback.h"

NS_ASSUME_NONNULL_BEGIN

@interface WithdrawFlowbackTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnFlowback;

- (void)updateCellWithyData:(Flowback *)data;

@end

NS_ASSUME_NONNULL_END
