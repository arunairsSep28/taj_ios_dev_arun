//
//  AddBankAccountViewController.m
//  Taj Rummy
//
//  Created by svc on 21/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "AddBankAccountViewController.h"

@interface AddBankAccountViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITextField *textFieldAccountName;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldBankName;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldBankAddress;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAccountNO;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldIFSC;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldState;

//picker
@property (weak, nonatomic) IBOutlet UIView *viewPickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *arrayStates;
@property (strong, nonatomic) State *selectedStateObj;

@end

@implementation AddBankAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    self.viewPickerContainer.hidden = YES;
    [self getStatesList];
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.textFieldAccountName.text length] == 0) {
        [self.view makeToast:@"Account name can not be empty"];
        return YES;
    }
    else if ([self.textFieldAccountName.text length] < UserNameMinLength) {
        [self.view makeToast:@"Please enter 5 to 15 characters of Account name"];
        return YES;
    }
    if ([self.txtFieldBankName.text length] == 0) {
        [self.view makeToast:@"Bank name can not be empty"];
        return YES;
    }
    else if ([self.txtFieldBankName.text length] < UserNameMinLength) {
        [self.view makeToast:@"Please enter 5 to 15 characters of Bank name"];
        return YES;
    }
    else if ([self.txtFieldBankAddress.text length] == 0) {
        [self.view makeToast:@"Bank address can not be empty"];
        return YES;
    }
    else if ([self.txtFieldBankAddress.text length] < UserNameMinLength) {
        [self.view makeToast:@"Please enter 5 to 15 characters of Bank address"];
        return YES;
    }
    else if ([self.txtFieldAccountNO.text length] == 0) {
        [self.view makeToast:@"Account number can not be empty"];
        return YES;
    }
    else if ([self.txtFieldAccountNO.text length] < 8) {
        [self.view makeToast:@"Please enter minimum 8 digits of Account number"];
        return YES;
    }
    else if ([self.txtFieldIFSC.text length] == 0) {
        [self.view makeToast:@"IFSC can not be empty"];
        return YES;
    }
    else if (![self validateIFSC:self.txtFieldIFSC.text]) {
        [self.view makeToast:@"Enter valid IFSC"];
        return YES;
    }
    else if ([self.txtFieldState.text length] == 0) {
        [self.view makeToast:@"State not be empty"];
        return YES;
    }
    return NO;
}

- (BOOL)validateIFSC: (NSString *) cardNumber {
    //[A-Za-z]{5}\d{4}[A-Za-z]{1}">
    NSString *ifscRegex = @"^[A-Za-z]{4}[0-9]{6,7}$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", ifscRegex];
    return [cardTest evaluateWithObject:cardNumber];
}

-(void)getStatesList {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,STATES_LIST];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            //NSLog(@"STATES Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSLog(@"STATES DATA : %@",[responseData valueForKey:@"data"]);
                NSDictionary * dataDic = [responseData valueForKey:@"data"];
                self.arrayStates = [[NSMutableArray alloc] init];
                
                for (NSMutableDictionary *areaDic in [dataDic valueForKey:@"states_list"]) {
                    State *areaObject = [State sharedInstance];
                    [areaObject assignData:areaDic];
                    [self.arrayStates addObject:areaObject];
                }
                NSLog(@"STATES Count : %lu",(unsigned long)self.arrayStates.count);
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        NSLog(@"STATES ERROR : %@",errorString);
        [self.view makeToast:errorString];
    }];
    
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)statesClicked:(UIButton *)sender {
    if ([self.arrayStates count] > 0) {
        self.viewPickerContainer.hidden = NO;
        [self.pickerView reloadAllComponents];
    }
}

- (IBAction)submitClicked:(UIButton *)sender {
    
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"bank_name": self.txtFieldBankName.text,
                                 @"account_name": self.textFieldAccountName.text,
                                 @"account_number" : self.txtFieldAccountNO.text,
                                 @"ifsc_code" : self.txtFieldIFSC.text,
                                 @"state" : self.txtFieldState.text,
                                 @"address" : self.txtFieldBankAddress.text                                 };
#if DEBUG
        NSLog(@"ADD_ACCOUNT PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,ADD_ACCOUNT];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        NSLog(@"ADD_ACCOUNT RESPONSE: %@", jsondata);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                            
                            [self dismissViewControllerAnimated:NO completion:nil];
                        }
                        else {
                            [self.view hideToastActivity];
                            NSLog(@"ADD_ACCOUNT Error :%@",[jsondata valueForKey:@"error"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                            
                        }
                    });
                }
                else
                {
                    [self.view makeToast:@"Error while fetching data."];
                    [self.view hideToastActivity];
                }
            });
        }];
        
    }
}

#pragma mark - PickerView Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
   
    return [self.arrayStates count];
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    State * stateObject = self.arrayStates[row];
    return [[NSString stringWithFormat:@"%@",stateObject.name] capitalizedString];
}

#pragma mark - STATE PICKER

- (IBAction)hidePickerView:(id)sender
{
    self.viewPickerContainer.hidden = YES;
}

- (IBAction)donePickerView:(id)sender
{
    NSUInteger num = [[self.pickerView dataSource] numberOfComponentsInPickerView:self.pickerView];
    
        for (int i = 0; i < num; i++) {
            NSUInteger selectRow = [self.pickerView selectedRowInComponent:i];
            self.selectedStateObj = self.arrayStates[selectRow];
            self.txtFieldState.text = [self.selectedStateObj.name capitalizedString];
            
        }
    
    self.viewPickerContainer.hidden = YES;
}

#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Name
    if (textField == self.textFieldAccountName) {
        return !([textField.text length] > 36 && [string length] > range.length);
    }
    else if (textField == self.txtFieldBankName) {
        return !([textField.text length] > 36 && [string length] > range.length);
    }
    
    else if (textField == self.txtFieldBankAddress) {
        return !([textField.text length] > 120 && [string length] > range.length);
    }
    
    else if (textField == self.txtFieldIFSC) {
        return !([textField.text length] > 10 && [string length] > range.length);
    }
    
    // pincode Number
    else if (textField == self.txtFieldAccountNO) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@" "]) {
            return NO;
        }
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return ([string isEqualToString:filteredString] ? newLength <= 18 : NO);
    }
    
    return YES;
}

@end
