//
//  WebViewController.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "WebViewController.h"

@interface WebViewController ()<UIWebViewDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (strong, nonatomic) NSString *labelTitle;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *viewMore;
@property (assign, nonatomic) BOOL isFromPromotions;;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIWebView *webView;
@property (weak, nonatomic) IBOutlet UILabel *lblContent;

@property (assign, nonatomic)  BOOL allowLoad;
@property (assign, nonatomic) NSInteger loadCount;
@property (weak, nonatomic) IBOutlet WKWebView *wkWebView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *textViewHeightConstarint;
@end

@implementation WebViewController

- (instancetype)initWithTitle:(NSString *)title url:(NSString *)url;
{
    self = [super init];
    if (self)
    {
        _labelTitle = title;
        _url = url;
    }
    return self;
}

- (instancetype)initWithViewMore:(NSString *)viewMore isFromPromotions:(BOOL)isFromPromotions
{
    self = [super init];
    if (self)
    {
        _viewMore = viewMore;
        _isFromPromotions = isFromPromotions;
    }
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.textViewHeightConstarint.constant = self.view.frame.size.height - 76;
    
    self.lblTitle.text = self.labelTitle;
//    self.allowLoad = YES;
    
    if (self.isFromPromotions) {
        [self getPromotions];
        self.webView.hidden = YES;
        self.scrollView.hidden = NO;
    }
    else if ([self.lblTitle.text isEqualToString:@"Terms & Conditions"]){
        self.webView.hidden = NO;
        self.scrollView.hidden = YES;
        
        NSString *urlAddress = [NSString stringWithFormat:@"%@promotions/terms-content/",BASE_URL] ;
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlAddress]];
                NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
                //self.webView.delegate = self;
        [self.webView loadRequest:requestObj];
    }
    //privacy-policy-content
    else if ([self.lblTitle.text isEqualToString:@"Privacy Policy"]){
        self.webView.hidden = NO;
        self.scrollView.hidden = YES;
        
        NSString *urlAddress = [NSString stringWithFormat:@"%@promotions/privacy-policy-content/",BASE_URL] ;
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlAddress]];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        //self.webView.delegate = self;
        [self.webView loadRequest:requestObj];
    }
    //legality-content/
    else if ([self.lblTitle.text isEqualToString:@"Legality"]){
        self.webView.hidden = NO;
        self.scrollView.hidden = YES;
        
        NSString *urlAddress = [NSString stringWithFormat:@"%@promotions/legality-content/",BASE_URL] ;
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlAddress]];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        //self.webView.delegate = self;
        [self.webView loadRequest:requestObj];
    }
    else if ([self.lblTitle.text isEqualToString:@"Withdrawble balance working"]){
        self.webView.hidden = NO;
        self.scrollView.hidden = YES;
        
        NSString *urlAddress = [NSString stringWithFormat:@"%@%@withdrawable-balance-content/",BASE_URL,API_BASE_URL];
        
        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlAddress]];
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        //self.webView.delegate = self;
        [self.webView loadRequest:requestObj];
    }
    else {
        [self getData];
        self.webView.hidden = YES;
        self.scrollView.hidden = NO;
//        self.webView.hidden = NO;
//        self.scrollView.hidden = YES;
//        NSLog(@"TITLE %@ : URL%@",self.labelTitle,self.url);
        //self.wkWebView.delegate = self;
//        self.webView.delegate = self;
//        NSString *urlAddress = self.url;
//
//        NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@", urlAddress]];
//        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//        //self.webView.delegate = self;
//        [self.webView loadRequest:requestObj];
//        //[self.view addSubview:self.webView];
//        [self.view makeToastActivity:CSToastPositionCenter];
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

#pragma mark - HELPERS

-(void)getPromotions {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = self.viewMore;
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    NSLog(@"URL : %@",URL);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"PROMOTION DETAILS Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                
                NSDictionary * dataDic = [responseData valueForKey:@"data"];
                NSString * description = [[[dataDic valueForKey:@"promotions_list"]objectAtIndex:0] valueForKey:@"content"];
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[description dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
                
                self.lblContent.attributedText = attrStr;
            }
            
            NSLog(@"PROMOTION DETAILS DATA : %@",[responseData valueForKey:@"data"]);
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"PROMOTIONS DETAILS ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}


-(void)getData {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = self.url;
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    NSLog(@"URL : %@",URL);
    [[Service sharedInstance] getRequestWithHeader:nil URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"DATA Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                
                NSString * description = [responseData valueForKey:@"message"];
            
                [self.webView loadHTMLString:[description stringByReplacingOccurrencesOfString:@"\n" withString:@"<br/>"] baseURL:nil];
                UIColor *color = [UIColor blackColor];
                UIFont *textFont = [UIFont fontWithName:@"HelveticaNeue-Medium" size:16];
                NSAttributedString * attrStr = [[NSAttributedString alloc] initWithData:[description dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,NSForegroundColorAttributeName : color, NSFontAttributeName :textFont } documentAttributes:nil error:nil];
                
                self.lblContent.attributedText = attrStr;
        
            }
            
            NSLog(@"DATA DETAILS DATA : %@",[responseData valueForKey:@"message"]);
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"DATA ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark UIWebView delegate methods

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    return YES;
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    [self.view hideToastActivity];
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.view hideToastActivity];
    NSLog(@"webView Error : %@",error);
}

@end
