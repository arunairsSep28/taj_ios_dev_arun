//
//  Bonus.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BonusHistory : NSObject

@property(strong, nonatomic) NSString *bonusamount;
@property(strong, nonatomic) NSString *chunks;
@property(strong, nonatomic) NSString *type;
@property(strong, nonatomic) NSString *status;
@property(strong, nonatomic) NSString *timestamp;

@property(assign, nonatomic) BOOL isSelected;

+ (BonusHistory *)sharedInstance;
- (BonusHistory *)assignData:(NSDictionary*)getData;

@end
