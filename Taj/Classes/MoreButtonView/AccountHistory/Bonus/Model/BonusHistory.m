//
//  Bonus.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "BonusHistory.h"

@implementation BonusHistory
static BonusHistory *instance;

@synthesize bonusamount,chunks,type,status, timestamp;

+ (BonusHistory *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    BonusHistory *copy = [[BonusHistory allocWithZone: zone] init];
    
    [copy setBonusamount:bonusamount];
    [copy setChunks:chunks];
    [copy setType:type];
    [copy setStatus:status];
    [copy setTimestamp:timestamp];
    
    return copy;
}

- (BonusHistory *)assignData:(NSDictionary*)getData;
{
    bonusamount = [getData objectForKey:@"bonusamount"];
    chunks = [getData objectForKey:@"chunks"];
    type = [getData objectForKey:@"type"];
    status = [getData objectForKey:@"status"];
    timestamp = [getData objectForKey:@"timestamp"];
    
    return self;
}

- (BOOL)isContainsKey:(NSDictionary *)player key:(NSString *)key
{
    BOOL retVal = 0;
    NSArray *allKeys = [player allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

@end
