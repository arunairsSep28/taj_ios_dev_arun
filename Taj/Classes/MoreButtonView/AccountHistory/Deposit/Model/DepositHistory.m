//
//  Bonus.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "DepositHistory.h"

@implementation DepositHistory
static DepositHistory *instance;

@synthesize amount, bonus_code,order_id,modeofdeposit,ordermoney,orderstate, timestamp;

+ (DepositHistory *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    DepositHistory *copy = [[DepositHistory allocWithZone: zone] init];
    
    [copy setBonus_code:bonus_code];
    [copy setAmount:amount];
    [copy setOrder_id:order_id];
    [copy setModeofdeposit:modeofdeposit];
    [copy setOrdermoney:ordermoney];
    [copy setOrderstate:orderstate];
    [copy setTimestamp:timestamp];
    
    return copy;
}

- (DepositHistory *)assignData:(NSDictionary*)getData;
{
    bonus_code = [getData objectForKey:@"bonuscode"];
    amount = [getData objectForKey:@"amount"];
    order_id = [getData objectForKey:@"id"];
    modeofdeposit = [getData objectForKey:@"modeofdeposit"];
    ordermoney = [getData objectForKey:@"ordermoney"];
    orderstate = [getData objectForKey:@"orderstate"];
    timestamp = [getData objectForKey:@"timestamp"];
    
    return self;
}

- (BOOL)isContainsKey:(NSDictionary *)player key:(NSString *)key
{
    BOOL retVal = 0;
    NSArray *allKeys = [player allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

@end
