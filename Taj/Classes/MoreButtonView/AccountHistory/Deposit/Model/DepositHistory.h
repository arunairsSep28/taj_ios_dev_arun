//
//  Bonus.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DepositHistory : NSObject

@property(strong, nonatomic) NSString *bonus_code;
@property(strong, nonatomic) NSString *amount;
@property(strong, nonatomic) NSString *order_id;
@property(strong, nonatomic) NSString *modeofdeposit;
@property(strong, nonatomic) NSString *ordermoney;
@property(strong, nonatomic) NSString *orderstate;
@property(strong, nonatomic) NSString *timestamp;

@property(assign, nonatomic) BOOL isSelected;

+ (DepositHistory *)sharedInstance;
- (DepositHistory *)assignData:(NSDictionary*)getData;
@end
