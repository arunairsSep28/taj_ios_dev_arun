//
//  DepositHistoryViewController.m
//  Taj Rummy
//
//  Created by svc on 13/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "DepositHistoryViewController.h"

@interface DepositHistoryViewController ()

@property (weak, nonatomic) IBOutlet UIButton *btnToday;
@property (weak, nonatomic) IBOutlet UIView *viewToday;
@property (weak, nonatomic) IBOutlet UIButton *btnPast;
@property (weak, nonatomic) IBOutlet UIView *viewPast;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UIView *viewDate;
@property (weak, nonatomic) IBOutlet UILabel *lblPastDateSelection;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewDateHeightConstraint;

@property (strong, nonatomic) NSMutableArray *arrayList;
@property (strong, nonatomic) NSArray *arrayCustomSelection;

//picker
@property (weak, nonatomic) IBOutlet UIView *viewPickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSString *selectedDateType;
@property (weak, nonatomic) IBOutlet UIView *viewCustomDate;

//DatePicker
@property (weak, nonatomic) IBOutlet UIView *viewDatePickerContainer;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (assign, nonatomic) NSInteger optionClickedIndex;
@property (strong, nonatomic) NSString *selectedFromDate;
@property (strong, nonatomic) NSString *selectedToDate;


@property (weak, nonatomic) IBOutlet UIView *viewFromDate;
@property (weak, nonatomic) IBOutlet UIView *viewToDate;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewCustomDateHeightConstraint;
@property(assign, nonatomic) BOOL isCustomDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldFromDate;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldTodate;

@property(strong, nonatomic) NSDictionary *params;

@end

@implementation DepositHistoryViewController

static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}


- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)configureView
{
    // initialization
    self.viewPickerContainer.hidden = YES;
    self.viewDatePickerContainer.hidden = YES;
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"DepositHistoryTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    self.arrayCustomSelection = @[
                                  @{
                                      @"name" : @"Yesterday",
                                      @"id" : @"Yesterday"
                                      },
                                  @{
                                      @"name" : @"Last 7 days",
                                      @"id" : @"WeekTildate"
                                      },
                                  @{
                                      @"name" : @"Last 30 days",
                                      @"id" : @"Last 30 Days"
                                      },
                                  @{
                                      @"name" : @"This Month",
                                      @"id" : @"MonthTilldate"
                                      },
                                  @{
                                      @"name" : @"Last Month",
                                      @"id" : @"LastMonth"
                                      },
                                  @{
                                      @"name" : @"Custom",
                                      @"id" : @"Custom"
                                      }
                                  ];
    
    self.viewFromDate.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewToDate.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    [self setToday];
    
}

#pragma mark - HELPERS

-(void)setDefaults {
    self.viewToday.backgroundColor = [UIColor clearColor];
    self.viewPast.backgroundColor = [UIColor clearColor];
    //[UIColor themeColor].CGColor
}

-(void)setToday {
    [self setDefaults];
    self.viewToday.backgroundColor = [UIColor greenishColor];
    self.viewDateHeightConstraint.constant = 0;
    self.viewDate.hidden = YES;
    self.selectedDateType = @"Today";
    self.params = @{@"customid": @"Today"};
    [self getData];
    self.viewCustomDateHeightConstraint.constant = 0;
    self.viewCustomDate.hidden = YES;

}

-(void)setPast {
    [self setDefaults];
    self.viewPast.backgroundColor = [UIColor greenishColor];
    if ([TAJUtilities isIPhone]) {
        self.viewDateHeightConstraint.constant = 54;
    }
    else {
        self.viewDateHeightConstraint.constant = 74;
    }
    self.viewDate.hidden = NO;
    self.lblPastDateSelection.text = [self.arrayCustomSelection[0] objectForKey:@"name"];
    self.selectedDateType = [self.arrayCustomSelection[0] objectForKey:@"id"];
    
    if ([self.selectedDateType isEqualToString:@"Custom"]) {
        self.isCustomDate = YES;
        self.viewCustomDate.hidden = NO;
        if ([TAJUtilities isIPhone]) {
            self.viewCustomDateHeightConstraint.constant = 88;
        }
        else {
            self.viewCustomDateHeightConstraint.constant = 116;
        }
        
    }
    else {
        self.isCustomDate = NO;
        self.viewCustomDate.hidden = YES;
        self.viewCustomDateHeightConstraint.constant = 0;
    }
    self.params = @{@"customid": self.selectedDateType};
    [self getData];
}

- (BOOL)validateInput
{
    
    if ([self.txtFieldFromDate.text length] == 0) {
        [self.view makeToast:@"Select from date"];
        return YES;
    }
    else if ([self.txtFieldTodate.text length] == 0) {
        [self.view makeToast:@"Select to date"];
        return YES;
    }
    
    
    return NO;
}

-(void)getData
{
    [self.view makeToastActivity:CSToastPositionCenter];
    

#if DEBUG
    NSLog(@"DEPOSIT HISTORY PARAMS: %@", self.params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"header: %@", header);
#endif
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,DEPOSIT_HISTORY];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:self.params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"DEPOSIT HISTORY RESPONSE: %@", jsondata);
#endif
                        self.arrayList = [[NSMutableArray alloc] init];
                        
                        for (NSMutableDictionary *ordersDic in [jsondata valueForKey:@"data"][@"data_list"]){
                            
                            DepositHistory *bonus = [DepositHistory sharedInstance];
                            [bonus assignData:ordersDic];
                            [self.arrayList addObject:bonus];
                        }
                        
                        #if DEBUG
                        NSLog(@"DEPOSIT HISTORY COUNT : %lu",(unsigned long)self.arrayList.count);
#endif
                        if (self.arrayList.count == 0) {
                            [self.view makeToast:@"No data available"];
                        }
        
                        [self.tableView reloadData];
                        self.tableView.hidden = NO;
                    }
                    else {
                        [self.view hideToastActivity];
                        NSLog(@"DEPOSIT HISTORY Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        [self.tableView reloadData];
                        self.tableView.hidden = YES;
                    }
                });
                
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
                [self.view hideToastActivity];
                [self.tableView reloadData];
                self.tableView.hidden = YES;
            }
        });
    }];
    
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)todayClicked:(UIButton *)sender {
    [self setToday];
}

- (IBAction)pastClicked:(UIButton *)sender {
    [self setPast];
}
- (IBAction)dateSelectionClicked:(UIButton *)sender {
    self.viewPickerContainer.hidden = NO;
    [self.pickerView reloadAllComponents];
}
- (IBAction)submitClicked:(UIButton *)sender {
    
    BOOL error = [self validateInput];
    if (!error) {
        self.params = @{@"customid": self.selectedDateType,
                        @"fromdate" : self.selectedFromDate,
                        @"todate" : self.selectedToDate
                        };
        [self getData];
    }
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayList.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
        return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    DepositHistoryTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithData:self.arrayList[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - PickerView Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return self.arrayCustomSelection.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",[self.arrayCustomSelection[row] objectForKey:@"name"]];
}

#pragma mark - PICKER

- (IBAction)hidePickerView:(id)sender
{
    self.viewPickerContainer.hidden = YES;
}

- (IBAction)donePickerView:(id)sender
{
    NSUInteger num = [[self.pickerView dataSource] numberOfComponentsInPickerView:self.pickerView];
    
    for (int i = 0; i < num; i++) {
        NSUInteger selectRow = [self.pickerView selectedRowInComponent:i];
        self.lblPastDateSelection.text = [ self.arrayCustomSelection[selectRow] objectForKey:@"name"];
        self.selectedDateType = [self.arrayCustomSelection[selectRow] objectForKey:@"id"];
    }
    
    self.viewPickerContainer.hidden = YES;
    
    self.params = @{@"customid": self.selectedDateType};
    
    if ([self.selectedDateType isEqualToString:@"Custom"]) {
        self.isCustomDate = YES;
        self.viewCustomDate.hidden = NO;
        if ([TAJUtilities isIPhone]) {
            self.viewCustomDateHeightConstraint.constant = 88;
        }
        else {
            self.viewCustomDateHeightConstraint.constant = 116;
        }
        self.tableView.hidden = YES;
    }
    else {
        self.isCustomDate = NO;
        self.viewCustomDate.hidden = YES;
        self.viewCustomDateHeightConstraint.constant = 0;
        [self getData];
    }
    
}

#pragma mark - DateOfBirth

- (IBAction)optionsClicked:(UIButton *)sender
{
    self.optionClickedIndex = (int)[sender tag];
    
    self.datePicker.maximumDate = [NSDate date];
    self.viewDatePickerContainer.hidden = NO;
    // [self.scrollView bringSubviewToFront:self.viewDatePickerContainer];
    self.datePicker.backgroundColor = [UIColor whiteColor];
    [self.datePicker setValue:[UIColor blackColor] forKey:@"textColor"];
}


- (IBAction)hideDatePickerView:(id)sender
{
    self.viewDatePickerContainer.hidden = YES;
}

- (IBAction)doneDatePickerView:(id)sender
{
    
    NSDateFormatter *selectedFromDateFormatter = [[NSDateFormatter alloc] init];
    [selectedFromDateFormatter setDateFormat:@"MM/dd/yy"];
    
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM/dd/yyyy"];
    NSString *displayDate = [formatter stringFromDate:self.datePicker.date];
    
    if (self.optionClickedIndex == 20) {
        self.txtFieldFromDate.text = displayDate;
        self.selectedFromDate = [selectedFromDateFormatter stringFromDate:self.datePicker.date];
    }
    else {
        self.txtFieldTodate.text = displayDate;
        self.selectedToDate = [selectedFromDateFormatter stringFromDate:self.datePicker.date];
    }
    self.viewDatePickerContainer.hidden = YES;
    
}



@end
