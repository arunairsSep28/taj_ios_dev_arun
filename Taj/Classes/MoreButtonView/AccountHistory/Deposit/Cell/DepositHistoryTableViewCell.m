//
//  DepositHistoryTableViewCell.m
//  TajRummy
//
//  Created by Grid Logic on 27/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "DepositHistoryTableViewCell.h"

@interface DepositHistoryTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderId;
@property (weak, nonatomic) IBOutlet UILabel *lblModeOfDeposit;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblCoupon;
@property (weak, nonatomic) IBOutlet UILabel *lblDepositAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalChips;

@end

@implementation DepositHistoryTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithData:(DepositHistory *)historyData
{
    UIColor * textColor;
    if ([[historyData.orderstate lowercaseString] isEqualToString:@"successfull"]) {
        textColor = [UIColor greenishColor];
        self.lblStatus.text = @"SUCCESS";
    }
    else {
        textColor = [UIColor blackColor];
        self.lblStatus.text = [historyData.orderstate uppercaseString];
    }
    self.lblOrderId.text = [NSString stringWithFormat:@"#%@",historyData.order_id];
    self.lblModeOfDeposit.text = historyData.modeofdeposit;
    self.lblDepositAmount.text = [NSString stringWithFormat:@"₹%@",historyData.ordermoney];
    self.lblTotalChips.text = [NSString stringWithFormat:@"₹%@",historyData.amount];
    self.lblCoupon.text = historyData.bonus_code;
    
    self.lblOrderId.textColor = textColor;
    self.lblStatus.textColor = textColor;
    self.lblDepositAmount.textColor = textColor;
    self.lblDate.textColor = textColor;
    
    self.lblDate.text = [self getDate:historyData.timestamp];
    //NSLog(@"DATE TEXT : %@",_lblDate.text);
    
    self.lblTime.text = [self getTime:historyData.timestamp];
    
    //NSLog(@"CONVERTED TIME : %@",[self getTime:historyData.timestamp]);
}

-(NSString*)getDate:(NSString *)dateString {
    NSString *myDayString, *myMonthString, *myYearString;
    
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateToConvert=[dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"dd"];
    myDayString = [dateFormatter stringFromDate:dateToConvert];
    
    [dateFormatter setDateFormat:@"MMM"];
    myMonthString = [dateFormatter stringFromDate:dateToConvert];
    
    [dateFormatter setDateFormat:@"yyyy"];
    myYearString = [dateFormatter stringFromDate:dateToConvert];
    
    return [NSString stringWithFormat:@"%@ %@",myDayString,myMonthString];
}

-(NSString *)getTime:(NSString *)dateString {
    //2019-06-27 10:20:06
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[dateFormatter dateFromString:dateString];
    
    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setTimeZone:[NSTimeZone systemTimeZone]];
    [dfTime setDateFormat:@"hh:mm a"];
    NSString *time=[dfTime stringFromDate:date];
    NSLog(@"%@",time);
    
    return time;
}

- (void)updateCellWithWithdrawalData:(DepositHistory *)historyData;
{
    UIColor * textColor;
    if ([[historyData.orderstate lowercaseString] isEqualToString:@"finished"]) {
        textColor = [UIColor greenishColor];
        //self.lblStatus.text = @"SUCCESS";
    }
    else {
        textColor = [UIColor blackColor];
        //self.lblStatus.text = [historyData.orderstate uppercaseString];
    }
    self.lblStatus.text = historyData.orderstate;
    self.lblOrderId.text = [NSString stringWithFormat:@"#%@",historyData.order_id];
    self.lblModeOfDeposit.text = historyData.modeofdeposit;
    self.lblDepositAmount.text = [NSString stringWithFormat:@"₹%@",historyData.ordermoney];
    self.lblTotalChips.text = [NSString stringWithFormat:@"₹%@",historyData.amount];
    self.lblCoupon.text = @"Status";
    
    self.lblOrderId.textColor = textColor;
    self.lblStatus.textColor = textColor;
    self.lblDepositAmount.textColor = textColor;
    self.lblDate.textColor = textColor;
    
    self.lblDate.text = [self getDate:historyData.timestamp];
    //NSLog(@"DATE TEXT : %@",_lblDate.text);
    
    self.lblTime.text = [self getTime:historyData.timestamp];
    
    //NSLog(@"CONVERTED TIME : %@",[self getTime:historyData.timestamp]);
}

- (void)updateCellWithWithBonusData:(BonusHistory *)historyData
{
    UIColor * textColor;
   
    textColor = [UIColor blackColor];
    
    self.lblStatus.text = historyData.status;
    
    self.lblOrderId.text = historyData.chunks;
    self.lblModeOfDeposit.text = @"Chunks";
    self.lblDepositAmount.text = [NSString stringWithFormat:@"₹%@",historyData.bonusamount];
    self.lblTotalChips.text = historyData.type;
    self.lblCoupon.text = @"Status";
    
    self.lblOrderId.textColor = textColor;
    self.lblStatus.textColor = textColor;
    self.lblDepositAmount.textColor = textColor;
    self.lblDate.textColor = textColor;
    
    self.lblDate.text = [self getDate:historyData.timestamp];
    
    self.lblTime.text = [self getTime:historyData.timestamp];
    
}

- (void)updateCellWithWithReconReportsData:(ReconcileReports *)historyData
{
    UIColor * textColor;
    //WINNING - [[historyData.transactiontype lowercaseString] isEqualToString:@"winning"]
    if ([[historyData.transactiontype lowercaseString] isEqualToString:@"winner"]) {
        textColor = [UIColor greenishColor];
        //self.lblStatus.text = @"SUCCESS";
    }
    else {
        textColor = [UIColor blackColor];
        //self.lblStatus.text = [historyData.orderstate uppercaseString];
    }
    
    self.lblStatus.text = historyData.gametype;
    
    self.lblOrderId.text = [NSString stringWithFormat:@"#%@",historyData.gameid];
    self.lblModeOfDeposit.text = historyData.tableid;
    self.lblDepositAmount.text = [NSString stringWithFormat:@"₹%@",historyData.amount];
    self.lblTotalChips.text = historyData.totalplayerbalance;
    self.lblCoupon.text = historyData.transactiontype;
    
    self.lblOrderId.textColor = textColor;
    self.lblStatus.textColor = textColor;
    self.lblDepositAmount.textColor = textColor;
    self.lblDate.textColor = textColor;
    
    self.lblDate.text = [self getDate:historyData.timestamp];
    
    self.lblTime.text = [self getTime:historyData.timestamp];
    
}

- (void)updateCellWithGameLogsData:(ReconcileReports *)historyData{
    UIColor * textColor;
    if ([[historyData.transactiontype lowercaseString] isEqualToString:@"winner"] || [[historyData.transactiontype lowercaseString] isEqualToString:@"winning"]) {
        textColor = [UIColor greenishColor];
        //self.lblStatus.text = @"SUCCESS";
    }
    else {
        textColor = [UIColor blackColor];
        //self.lblStatus.text = [historyData.orderstate uppercaseString];
    }
    
    self.lblStatus.text = historyData.gametype;
    
    self.lblOrderId.text = [NSString stringWithFormat:@"#%@",historyData.gameid];
    self.lblModeOfDeposit.text = [NSString stringWithFormat:@"TID: %@",historyData.tableid];
    self.lblDepositAmount.text = [NSString stringWithFormat:@"₹%@",historyData.amount];
    self.lblTotalChips.text = historyData.streamtype;
    self.lblCoupon.text = historyData.transactiontype;
    
    self.lblOrderId.textColor = textColor;
    self.lblStatus.textColor = textColor;
    self.lblDepositAmount.textColor = textColor;
    self.lblDate.textColor = textColor;
    
    self.lblDate.text = [self getDate:historyData.timestamp];
    
    self.lblTime.text = [self getTime:historyData.timestamp];
    
}
@end
