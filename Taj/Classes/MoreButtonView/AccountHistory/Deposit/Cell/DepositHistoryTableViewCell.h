//
//  DepositHistoryTableViewCell.h
//  TajRummy
//
//  Created by Grid Logic on 27/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "DepositHistory.h"
#import "BonusHistory.h"
#import "ReconcileReports.h"

NS_ASSUME_NONNULL_BEGIN

@interface DepositHistoryTableViewCell : UITableViewCell

- (void)updateCellWithData:(DepositHistory *)historyData;
- (void)updateCellWithWithdrawalData:(DepositHistory *)historyData;
- (void)updateCellWithWithBonusData:(BonusHistory *)historyData;
- (void)updateCellWithWithReconReportsData:(ReconcileReports *)historyData;
- (void)updateCellWithGameLogsData:(ReconcileReports *)historyData;
@end

NS_ASSUME_NONNULL_END
