//
//  Bonus.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ReconcileReports : NSObject

@property(strong, nonatomic) NSString *amount;
@property(strong, nonatomic) NSString *gameid;
@property(strong, nonatomic) NSString *gametype;
@property(strong, nonatomic) NSString *tableid;
@property(strong, nonatomic) NSString *timestamp, *totalplayerbalance, *transactiontype, *streamtype;

@property(assign, nonatomic) BOOL isSelected;

+ (ReconcileReports *)sharedInstance;
- (ReconcileReports *)assignData:(NSDictionary*)getData;

@end
