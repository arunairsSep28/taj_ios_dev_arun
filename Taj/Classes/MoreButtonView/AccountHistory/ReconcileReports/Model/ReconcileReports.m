//
//  Bonus.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "ReconcileReports.h"

@implementation ReconcileReports
static ReconcileReports *instance;

@synthesize amount,gameid,gametype,tableid, timestamp, totalplayerbalance,transactiontype, streamtype;

+ (ReconcileReports *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    ReconcileReports *copy = [[ReconcileReports allocWithZone: zone] init];
    
    [copy setAmount:amount];
    [copy setGameid:gameid];
    [copy setGametype:gametype];
    [copy setTableid:tableid];
    [copy setTimestamp:timestamp];
    [copy setTotalplayerbalance:totalplayerbalance];
    [copy setTransactiontype:transactiontype];
    [copy setStreamtype:streamtype];
    
    return copy;
}

- (ReconcileReports *)assignData:(NSDictionary*)getData;
{
    amount = [getData objectForKey:@"amount"];
    gameid = [getData objectForKey:@"gameid"];
    gametype = [getData objectForKey:@"gametype"];
    tableid = [getData objectForKey:@"tableid"];
    timestamp = [getData objectForKey:@"timestamp"];
    totalplayerbalance = [getData objectForKey:@"toplayerbalance"];
    transactiontype = [getData objectForKey:@"transactiontype"];
    streamtype = [getData objectForKey:@"streamtype"];
    
    return self;
}

- (BOOL)isContainsKey:(NSDictionary *)player key:(NSString *)key
{
    BOOL retVal = 0;
    NSArray *allKeys = [player allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

@end
