//
//  BankingViewController.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "AccountHistoryViewController.h"

@interface AccountHistoryViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayMenu;

@end

@implementation AccountHistoryViewController
static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;

- (void)viewDidLoad {
    [super viewDidLoad];
    self.arrayMenu = @[@"Deposit History",@"Withdraw History",@"Bonus History",@"Reconcile Reports",@"Game Logs"];//@"Refer a Friend Logs"
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];

}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}


- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}
#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
    return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithTitle:self.arrayMenu[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        DepositHistoryViewController * viewController = [[DepositHistoryViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 1) {
        WithdrawalHistoryViewController * viewController = [[WithdrawalHistoryViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 2) {
        BonusHistoryViewController * viewController = [[BonusHistoryViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 3) {
        ReconcileReportsViewController * viewController = [[ReconcileReportsViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 4) {
        GameLogsViewController * viewController = [[GameLogsViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 5) {
        RFRLogsViewController * viewController = [[RFRLogsViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    
}

@end
