//
//  FRFTableViewCell.m
//  TajRummy
//
//  Created by Grid Logic on 01/07/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "RFRTableViewCell.h"

@interface RFRTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblEventName;
@property (weak, nonatomic) IBOutlet UILabel *lblEvent;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblFullDate;

@end

@implementation RFRTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithRFRLogsData:(RFRLogs *)rfrData
{
    UIColor * textColor;
    if ([[rfrData.event lowercaseString] isEqualToString:@"joined"]) {
        textColor = [UIColor greenishColor];
        self.lblFullDate.hidden = NO;
        self.lblFullDate.text = [NSString stringWithFormat:@"%@,%@",[self getDate:rfrData.accept_date],[self getTime:rfrData.accept_date]];
    }
    else {
        textColor = [UIColor blackColor];
        self.lblFullDate.hidden = YES;
        self.lblFullDate.text = [NSString stringWithFormat:@"%@,%@",[self getDate:rfrData.invited_time],[self getTime:rfrData.invited_time]];
    }
    
    self.lblEventName.text = rfrData.email;
    self.lblEvent.text = rfrData.event;
    self.lblName.text = rfrData.name;
    self.lblDate.text = [self getDate:rfrData.invited_time];
    self.lblTime.text = [self getTime:rfrData.invited_time];
    
    self.lblDate.textColor = textColor;
    self.lblEventName.textColor = textColor;
    self.lblEvent.textColor = textColor;
}

-(NSString*)getDate:(NSString *)dateString {
    NSString *myDayString, *myMonthString, *myYearString;
    
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateToConvert=[dateFormatter dateFromString:dateString];
    
    [dateFormatter setDateFormat:@"dd"];
    myDayString = [dateFormatter stringFromDate:dateToConvert];
    
    [dateFormatter setDateFormat:@"MMM"];
    myMonthString = [dateFormatter stringFromDate:dateToConvert];
    
    [dateFormatter setDateFormat:@"yyyy"];
    myYearString = [dateFormatter stringFromDate:dateToConvert];
    
    return [NSString stringWithFormat:@"%@ %@",myDayString,myMonthString];
}

-(NSString *)getTime:(NSString *)dateString {
    //2019-06-27 10:20:06
    NSDateFormatter *dateFormatter=[NSDateFormatter new];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *date=[dateFormatter dateFromString:dateString];
    
    NSDateFormatter *dfTime = [NSDateFormatter new];
    [dfTime setTimeZone:[NSTimeZone systemTimeZone]];
    [dfTime setDateFormat:@"hh:mm a"];
    NSString *time=[dfTime stringFromDate:date];
    NSLog(@"%@",time);
    
    return time;
}

@end
