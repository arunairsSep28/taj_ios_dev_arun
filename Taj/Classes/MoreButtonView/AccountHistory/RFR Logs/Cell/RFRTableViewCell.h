//
//  FRFTableViewCell.h
//  TajRummy
//
//  Created by Grid Logic on 01/07/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RFRLogs.h"

NS_ASSUME_NONNULL_BEGIN

@interface RFRTableViewCell : UITableViewCell

- (void)updateCellWithRFRLogsData:(RFRLogs *)rfrData;

@end

NS_ASSUME_NONNULL_END
