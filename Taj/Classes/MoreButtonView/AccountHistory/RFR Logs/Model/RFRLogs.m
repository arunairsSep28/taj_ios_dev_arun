//
//  Bonus.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "RFRLogs.h"

@implementation RFRLogs
static RFRLogs *instance;

@synthesize email,event,invited_time,name,accept_date;

+ (RFRLogs *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    RFRLogs *copy = [[RFRLogs allocWithZone: zone] init];
    
    [copy setEmail:email];
    [copy setEvent:event];
    [copy setInvited_time:invited_time];
    [copy setName:name];
    [copy setAccept_date:accept_date];
    
    return copy;
}

- (RFRLogs *)assignData:(NSDictionary*)getData;
{
    name = [getData objectForKey:@"name"];
    email = [getData objectForKey:@"email"];
    event = [getData objectForKey:@"event"];
    invited_time = [getData objectForKey:@"invited_time"];
    accept_date = [getData objectForKey:@"accept_date"];
    
    return self;
}

- (BOOL)isContainsKey:(NSDictionary *)player key:(NSString *)key
{
    BOOL retVal = 0;
    NSArray *allKeys = [player allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

@end
