//
//  Bonus.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RFRLogs : NSObject

@property(strong, nonatomic) NSString *name;
@property(strong, nonatomic) NSString *event;
@property(strong, nonatomic) NSString *email;
@property(strong, nonatomic) NSString *invited_time;
@property(strong, nonatomic) NSString *accept_date;

+ (RFRLogs *)sharedInstance;
- (RFRLogs *)assignData:(NSDictionary*)getData;

@end
