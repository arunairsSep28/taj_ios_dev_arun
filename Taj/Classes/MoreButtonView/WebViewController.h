//
//  WebViewController.h
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "TAJBaseViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface WebViewController : UIViewController

- (instancetype)initWithTitle:(NSString *)title url:(NSString *)url;
- (instancetype)initWithViewMore:(NSString *)viewMore isFromPromotions:(BOOL)isFromPromotions;

@end

NS_ASSUME_NONNULL_END
