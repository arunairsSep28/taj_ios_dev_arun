//
//  KYC.h
//  Taj Rummy
//
//  Created by svc on 12/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "KYC.h"

@interface KYC : NSObject
@property (strong, nonatomic) NSString *emailid;
@property (strong, nonatomic) NSString *mobile_no_status;
@property (strong, nonatomic) NSString *id_proof_status;
@property (strong, nonatomic) NSString *pan_card_status;
@property (strong, nonatomic) NSString *aadhar_status;
@property (strong, nonatomic) NSString *pan_card_no;
@property (strong, nonatomic) NSString *bank_status;
@property (strong, nonatomic) NSString *id_proof_no;
@property (strong, nonatomic) NSString *aadhar_no;
@property (strong, nonatomic) NSString *mobile_no;
@property (strong, nonatomic) NSString *kyc_verified_status;
@property (strong, nonatomic) NSString *email_status;
@property (strong, nonatomic) NSString *id_proof_type;

+ (KYC *)sharedInstance;
- (KYC *)assignData:(NSDictionary *)data;

@end
