//
//  Preferences.h
//  Taj Rummy
//
//  Created by svc on 12/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Preferences : NSObject

@property (assign, nonatomic) BOOL phone_preference;
@property (assign, nonatomic) BOOL sms_preference;
@property (assign, nonatomic) BOOL email_preference;
@property (assign, nonatomic) BOOL newsletter_preference;

+ (Preferences *)sharedInstance;
- (Preferences *)assignData:(NSDictionary *)getData;

@end
