//
//  Flowback.h
//  Taj Rummy
//
//  Created by svc on 18/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Flowback : NSObject
@property (strong, nonatomic) NSString *available_amount;
@property (strong, nonatomic) NSString *time;
@property (strong, nonatomic) NSString *withdraw_amount;
@property (assign, nonatomic) NSInteger order_id;
@property (assign, nonatomic) BOOL show_button;


+ (Flowback *)sharedInstance;
- (Flowback *)assignData:(NSDictionary *)data;

@end
