//
//  AccountOverview.h
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BankDetails : NSObject
@property (strong, nonatomic) NSString *bank_name;
@property (strong, nonatomic) NSString *country;
@property (strong, nonatomic) NSString *ifsc_code;
@property (strong, nonatomic) NSString *state;
@property (strong, nonatomic) NSString *account_number;
@property (strong, nonatomic) NSString *address;
@property (strong, nonatomic) NSString *account_name;
@property (assign, nonatomic) NSInteger bankId;

@property(assign, nonatomic) BOOL isSelected;

+ (BankDetails *)sharedInstance;
- (BankDetails *)assignData:(NSDictionary *)data;

@end
