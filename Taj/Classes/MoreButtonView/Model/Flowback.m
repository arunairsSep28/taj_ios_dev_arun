//
//  Flowback.m
//  Taj Rummy
//
//  Created by svc on 18/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "Flowback.h"

@implementation Flowback

static Flowback *instance;

@synthesize
withdraw_amount,
available_amount,
time,
order_id,
show_button;

+ (Flowback *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    Flowback *copy = [[Flowback allocWithZone: zone] init];
    
    [copy setWithdraw_amount:withdraw_amount];
    [copy setAvailable_amount:available_amount];
    [copy setTime:time];
    [copy setOrder_id:order_id];
    [copy setShow_button:show_button];
    
    return copy;
}

- (Flowback *)assignData:(NSDictionary *)data
{
    withdraw_amount = [data objectForKey:@"withdraw_amount"];
    available_amount = [data objectForKey:@"available_amount"];
    time = [data objectForKey:@"time"];
    order_id = [[data objectForKey:@"id"] integerValue];
    show_button = [[data objectForKey:@"show_button"] boolValue];
    
    return self;
}

@end


