//
//  Preferences.h
//  Taj Rummy
//
//  Created by svc on 12/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "Preferences.h"

@implementation Preferences

static Preferences *instance;

@synthesize
phone_preference,
sms_preference,
email_preference,
newsletter_preference;

+ (Preferences *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    Preferences *copy = [[Preferences allocWithZone: zone] init];
    [copy setPhone_preference:phone_preference];
    [copy setSms_preference:sms_preference];
    [copy setEmail_preference:email_preference];
    [copy setNewsletter_preference:newsletter_preference];
    
    return copy;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
    [encoder encodeBool:phone_preference forKey:@"phone_preference"];
    [encoder encodeBool:sms_preference forKey:@"sms_preference"];
    [encoder encodeBool:email_preference forKey:@"email_preference"];
    [encoder encodeBool:newsletter_preference forKey:@"newsletter_preference"];
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init])) {
        //decode properties, other class vars
        phone_preference = [decoder decodeBoolForKey:@"phone_preference"];
        sms_preference = [decoder decodeBoolForKey:@"sms_preference"];
        email_preference = [decoder decodeBoolForKey:@"email_preference"];
        newsletter_preference = [decoder decodeBoolForKey:@"newsletter_preference"];
    }
     return self;
}

- (Preferences *)assignData:(NSDictionary *)getData
{
    phone_preference = [[getData objectForKey:@"phone_preference"] boolValue];
    sms_preference = [[getData objectForKey:@"sms_preference"] boolValue];
    email_preference = [[getData objectForKey:@"email_preference"] boolValue];
    newsletter_preference = [[getData objectForKey:@"newsletter_preference"] boolValue];
    
    return self;
}

@end
