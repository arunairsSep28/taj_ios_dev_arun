//
//  AccountOverview.m
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "KYC.h"

@implementation KYC

static KYC *instance;

@synthesize
id_proof_status,
emailid,
mobile_no_status,
pan_card_status,
aadhar_status,
pan_card_no,
bank_status,
id_proof_no,
aadhar_no,
mobile_no,
kyc_verified_status,
email_status,
id_proof_type;

+ (KYC *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    KYC *copy = [[KYC allocWithZone: zone] init];
    
    [copy setEmailid:emailid];
    [copy setId_proof_status:id_proof_status];
    [copy setMobile_no_status:mobile_no_status];
    [copy setPan_card_status:pan_card_status];
    [copy setAadhar_status:aadhar_status];
    [copy setPan_card_status:pan_card_no];
    [copy setBank_status:bank_status];
    [copy setId_proof_no:id_proof_no];
    [copy setAadhar_no:aadhar_no];
    [copy setMobile_no:mobile_no];
    [copy setKyc_verified_status:kyc_verified_status];
    [copy setEmail_status:email_status];
    [copy setId_proof_type:id_proof_type];
    
    return copy;
}

- (KYC *)assignData:(NSDictionary *)data
{
    emailid = [data objectForKey:@"emailid"];
    id_proof_status = [data objectForKey:@"id_proof_status"];
    mobile_no_status = [data objectForKey:@"mobile_no_status"];
    pan_card_status = [data objectForKey:@"pan_card_status"];
    aadhar_status = [data objectForKey:@"aadhar_status"];
    pan_card_no = [data objectForKey:@"pan_card_no"];
    bank_status = [data objectForKey:@"bank_status"];
    id_proof_no = [data objectForKey:@"id_proof_no"];
    aadhar_no = [data objectForKey:@"aadhar_no"];
    mobile_no = [data objectForKey:@"mobile_no"];
    kyc_verified_status = [data objectForKey:@"kyc_verified_status"];
    email_status = [data objectForKey:@"email_status"];
    id_proof_type = [data objectForKey:@"id_proof_type"];
    
    return self;
}

@end
