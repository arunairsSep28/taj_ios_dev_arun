//
//  State.h
//  Taj Rummy
//
//  Created by svc on 11/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface State : NSObject
@property (assign, nonatomic) NSInteger state_id;
@property (strong, nonatomic) NSString *name;


+ (State *)sharedInstance;
- (State *)assignData:(NSDictionary *)getData;

@end
