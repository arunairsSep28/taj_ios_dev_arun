//
//  AccountOverview.m
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "BankDetails.h"

@implementation BankDetails

static BankDetails *instance;

@synthesize
ifsc_code,
bank_name,
country,
state,
account_number,
address,
account_name,
bankId,
isSelected;

+ (BankDetails *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    BankDetails *copy = [[BankDetails allocWithZone: zone] init];
    
    [copy setIfsc_code:ifsc_code];
    [copy setBank_name:bank_name];
    [copy setCountry:country];
    [copy setState:state];
    [copy setAccount_number:account_number];
    [copy setAddress:address];
    [copy setAccount_name:account_name];
    [copy setBankId:bankId];
    [copy setIsSelected:isSelected];
    
    return copy;
}

- (BankDetails *)assignData:(NSDictionary *)data
{
    ifsc_code = [data objectForKey:@"ifsc_code"];
    bank_name = [data objectForKey:@"bank_name"];
    country = [data objectForKey:@"country"];
    state = [data objectForKey:@"state"];
    account_number = [data objectForKey:@"account_number"];
    address = [data objectForKey:@"address"];
    account_name = [data objectForKey:@"account_name"];
    bankId = [[data objectForKey:@"id"] integerValue];
    isSelected = FALSE;

    
    return self;
}

@end


