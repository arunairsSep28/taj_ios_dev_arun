//
//  AccountOverview.m
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "AccountOverview.h"

@implementation AccountOverview

static AccountOverview *instance;

@synthesize
pending_bonus,
club,
funchips,
withdrawal_balance,
deposit_balance,
released_bonus,
total_balance,
loyaltypoints,
array_bank_details;

+ (AccountOverview *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    AccountOverview *copy = [[AccountOverview allocWithZone: zone] init];
    
    [copy setPending_bonus:pending_bonus];
    [copy setClub:club];
    [copy setFunchips:funchips];
    [copy setWithdrawal_balance:withdrawal_balance];
    [copy setDeposit_balance:deposit_balance];
    [copy setReleased_bonus:released_bonus];
    [copy setTotal_balance:total_balance];
    [copy setLoyaltypoints:loyaltypoints];
    [copy setArray_bank_details:array_bank_details];
    
    return copy;
}

- (AccountOverview *)assignData:(NSDictionary *)data
{
    pending_bonus = [[data objectForKey:@"pending_bonus"] integerValue];
    club = [data objectForKey:@"club"];
    funchips = [data objectForKey:@"funchips"];
    withdrawal_balance = [data objectForKey:@"withdrawal_balance"];
    deposit_balance = [data objectForKey:@"deposit_balance"];
    released_bonus = [[data objectForKey:@"released_bonus"] integerValue];
    total_balance = [data objectForKey:@"total_balance"];
    loyaltypoints = [data objectForKey:@"loyaltypoints"];
    
    array_bank_details = [[NSMutableArray alloc] init];
    
    for(NSMutableDictionary *productsDic in [data valueForKey:@"bank_details"]) {
        BankDetails *bankObject = [BankDetails sharedInstance];
        [bankObject assignData:productsDic];
        [array_bank_details addObject:bankObject];
    }
    
    return self;
}

@end
