//
//  AccountOverview.h
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BankDetails.h"

@interface AccountOverview : NSObject
@property (assign, nonatomic) NSInteger pending_bonus;
@property (strong, nonatomic) NSString *funchips;
@property (strong, nonatomic) NSString *withdrawal_balance;
@property (strong, nonatomic) NSString *club;
@property (strong, nonatomic) NSString *deposit_balance;
@property (assign, nonatomic) NSInteger released_bonus;
@property (strong, nonatomic) NSString *total_balance;
@property (strong, nonatomic) NSString *loyaltypoints;

@property (strong, nonatomic) NSMutableArray *array_bank_details;

+ (AccountOverview *)sharedInstance;
- (AccountOverview *)assignData:(NSDictionary *)data;

@end
