//
//  State.m
//  Taj Rummy
//
//  Created by svc on 11/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "State.h"

@implementation State

static State *instance;

@synthesize
state_id,
name;

+ (State *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    State *copy = [[State allocWithZone: zone] init];
    [copy setState_id:state_id];
    [copy setName:name];
    
    return copy;
}


- (void)encodeWithCoder:(NSCoder *)encoder
{
    //Encode properties, other class variables, etc
    [encoder encodeInteger:state_id forKey:@"id"];
    [encoder encodeObject:name forKey:@"name"];
    
}

- (id)initWithCoder:(NSCoder *)decoder
{
    if((self = [super init])) {
        //decode properties, other class vars
        state_id = [decoder decodeIntegerForKey:@"id"];
        name = [decoder decodeObjectForKey:@"name"];
    }
     return self;
}

- (State *)assignData:(NSDictionary *)getData
{
    state_id = [[getData objectForKey:@"state_id"] integerValue];
    name = [getData objectForKey:@"name"];
    
    return self;
}

@end
