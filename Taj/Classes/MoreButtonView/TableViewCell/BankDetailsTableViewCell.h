//
//  BankDetailsTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "BankDetails.h"

NS_ASSUME_NONNULL_BEGIN

@interface BankDetailsTableViewCell : UITableViewCell

-(void)updateCellWithData:(BankDetails *)data;

@end

NS_ASSUME_NONNULL_END
