//
//  BankDetailsTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 08/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "BankDetailsTableViewCell.h"

@interface BankDetailsTableViewCell ()

@property (weak, nonatomic) IBOutlet UIView *viewBankDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblBankName;
@property (weak, nonatomic) IBOutlet UILabel *lblAcHolderName;
@property (weak, nonatomic) IBOutlet UILabel *lblAcNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblBankAddress;
@property (weak, nonatomic) IBOutlet UILabel *lblState;
@property (weak, nonatomic) IBOutlet UILabel *lblIFSC;

@end

@implementation BankDetailsTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    // Configure the view for the selected state
}

- (void)configureView {
    self.viewBankDetails.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

-(void)updateCellWithData:(BankDetails *)data
{
    [self configureView];
    self.lblBankName.text = data.bank_name;
    self.lblBankAddress.text = data.address;
    self.lblAcHolderName.text = data.account_name;
    self.lblAcNumber.text = data.account_number;
    self.lblBankAddress.text = data.address;
    self.lblState.text = data.state;
    self.lblIFSC.text = data.ifsc_code;
}

@end
