//
//  FAQTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 13/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "FAQTableViewCell.h"

@interface FAQTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end


@implementation FAQTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithData:(NSDictionary *)data {
    self.lblTitle.text = [data valueForKey:@"question"];
}

@end
