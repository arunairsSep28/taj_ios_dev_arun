//
//  MoreTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "MoreTableViewCell.h"

@interface MoreTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

@implementation MoreTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)updateCellWithTitle:(NSString *)title
{
    self.lblTitle.text = title;
}

@end
