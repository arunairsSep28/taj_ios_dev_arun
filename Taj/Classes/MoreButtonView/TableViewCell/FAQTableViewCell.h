//
//  FAQTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 13/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FAQTableViewCell : UITableViewCell

- (void)updateCellWithData:(NSDictionary *)data;

@end

NS_ASSUME_NONNULL_END
