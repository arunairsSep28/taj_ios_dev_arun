//
//  MoreTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MoreTableViewCell : UITableViewCell

- (void)updateCellWithTitle:(NSString *)title;

@end

NS_ASSUME_NONNULL_END
