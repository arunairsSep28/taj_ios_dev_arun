//
//  UniqueFeaturesViewController.m
//  Taj Rummy
//
//  Created by svc on 19/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "UniqueFeaturesViewController.h"

@interface UniqueFeaturesViewController ()

@property(strong, nonatomic) NSArray * arrayFeatures;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 280;
static const CGFloat kCellHeightiPad = 400;

@implementation UniqueFeaturesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Unique Features"];
    
    self.arrayFeatures = @[@{@"image" : @"playerprotection",
                             @"title" : @"Player Protection",
                             @"info" : @"TajRummy as a service provider aims to provide every player with a fair, safe and secured platform to enjoy their gaming experience with us."
                               },
                           @{@"image" : @"welcome",
                             @"title" : @"Biggest Welcome Bonus",
                             @"info" : @"TajRummy provides a Welcome Bonus of 100% up to Rs.2,500 on your first deposit. This means that you get extra cash equivalent to the first deposit amount."
                             },
                           @{@"image" : @"sc",
                             @"title" : @"Smart Correction",
                             @"info" : @"Smart Correction secures your game by melding the cards even when a player has the correct combination of cards but places an invalid show."
                             },
                           @{@"image" : @"ai",
                             @"title" : @"Artificial Intelligence",
                             @"info" : @"Artificial Intelligence melds your cards during disconnection. It helps you get the least points when you are not able to meld yourself due to internet issues."
                             },
                           @{@"image" : @"respo-gaming",
                             @"title" : @"Responsible Gaming",
                             @"info" : @"TajRummy believes in building a gaming environment which is compliant with industry’s best practices, makes our users play responsibly and protects them."
                             },
                           @{@"image" : @"vip",
                             @"title" : @"VIP Player Service",
                             @"info" : @"Players with high club levels will be entitled to a wide range of privileges and dedicated offers to make sure that you make the most out of our website."
                             }
                           ];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"FeaturesTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayFeatures.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone])
    {
        return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeaturesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnReadmore.tag = indexPath.row;
    [cell.btnReadmore addTarget:self action:@selector(readMoreClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell updateCellWithData:self.arrayFeatures[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{/*
    if (indexPath.row == 0) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-game-auto-play/",BASE_URL]];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 1) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@signup-bonus-offer/",BASE_URL]];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 2) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@play-rummy-smart-correction/",BASE_URL]];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 3) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-game-artificial-intelligence/",BASE_URL]];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 4) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-responsible-gaming/",BASE_URL]];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 5) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-support/",BASE_URL]];
        [self presentViewController:viewController animated:NO completion:nil];
    }
  */
}

- (void)readMoreClicked:(UIButton *)sender
{
    if (sender.tag == 0) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-game-auto-play/",BASE_URL]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (sender.tag == 1) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@signup-bonus-offer/",BASE_URL]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (sender.tag == 2) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@play-rummy-smart-correction/",BASE_URL]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (sender.tag == 3) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-game-artificial-intelligence/",BASE_URL]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (sender.tag == 4) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-responsible-gaming/",BASE_URL]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (sender.tag == 5) {
        WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"" url:[NSString stringWithFormat:@"%@rummy-support/",BASE_URL]];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
}
@end
