//
//  PromotionsViewController.m
//  Taj Rummy
//
//  Created by svc on 20/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "PromotionsViewController.h"

@interface PromotionsViewController ()

@property(strong, nonatomic) NSMutableArray * arrayPromotions;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 280;
static const CGFloat kCellHeightiPad = 400;

@implementation PromotionsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Promotion Screen"];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"FeaturesTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    [self getPromotions];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)getPromotions {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,PROMOTIONS];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"PROMOTIONS Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                self.arrayPromotions = [[NSMutableArray alloc] init];
                
                NSDictionary * dataDic = [responseData valueForKey:@"data"];
                
                for (NSMutableDictionary *areaDic in [dataDic valueForKey:@"promotions_list"]) {
                    Promotions *promotionsObject = [Promotions sharedInstance];
                    [promotionsObject assignData:areaDic];
                    [self.arrayPromotions addObject:promotionsObject];
                }
                [self.tableView reloadData];
            }
            
            NSLog(@"PROMOTIONS DATA : %@",[responseData valueForKey:@"data"]);
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"PROMOTIONS ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayPromotions.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone])
    {
        return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    FeaturesTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.btnReadmore.tag = indexPath.row;
    [cell.btnReadmore addTarget:self action:@selector(readMoreClicked:) forControlEvents:UIControlEventTouchUpInside];
    [cell updateCellWithPromotionsData:self.arrayPromotions[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    Promotions *promotionsObject = self.arrayPromotions[indexPath.row];
    
    WebViewController * viewController = [[WebViewController alloc] initWithViewMore:[NSString stringWithFormat:@"%@%@",BASE_URL,promotionsObject.view_more] isFromPromotions:YES];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (void)readMoreClicked:(UIButton *)sender
{
    Promotions *promotionsObject = self.arrayPromotions[sender.tag];
    
    WebViewController * viewController = [[WebViewController alloc] initWithViewMore:[NSString stringWithFormat:@"%@%@",BASE_URL,promotionsObject.view_more] isFromPromotions:YES];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

@end
