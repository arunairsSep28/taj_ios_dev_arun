//
//  Promotions.h
//  Taj Rummy
//
//  Created by svc on 20/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Promotions : NSObject
@property (strong, nonatomic) NSString *description;
@property (strong, nonatomic) NSString *url;
@property (strong, nonatomic) NSString *menu_name;
@property (strong, nonatomic) NSString *to_time;
@property (strong, nonatomic) NSString *view_more;
@property (strong, nonatomic) NSString *title_name;
@property (strong, nonatomic) NSString *img_url;
@property (assign, nonatomic) BOOL expired;
@property (assign, nonatomic) NSInteger promotion_id;
@property (strong, nonatomic) NSString *from_time;

+ (Promotions *)sharedInstance;
- (Promotions *)assignData:(NSDictionary *)data;

@end
