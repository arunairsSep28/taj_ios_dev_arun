//
//  Promotions.m
//  Taj Rummy
//
//  Created by svc on 20/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "Promotions.h"

@implementation Promotions

static Promotions *instance;

@synthesize
menu_name,
description,
url,
to_time,
view_more,
title_name,
img_url,
expired,
promotion_id,
from_time;

+ (Promotions *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    Promotions *copy = [[Promotions allocWithZone: zone] init];
    
    [copy setMenu_name:menu_name];
    [copy setDescription:description];
    [copy setUrl:url];
    [copy setTo_time:to_time];
    [copy setView_more:view_more];
    [copy setTitle_name:title_name];
    [copy setImg_url:img_url];
    [copy setExpired:expired];
    [copy setPromotion_id:promotion_id];
    [copy setFrom_time:from_time];
    
    return copy;
}

- (Promotions *)assignData:(NSDictionary *)data
{
    menu_name = [data objectForKey:@"menu_name"];
    description = [data objectForKey:@"description"];
    url = [data objectForKey:@"url"];
    to_time = [data objectForKey:@"to_time"];
    view_more = [data objectForKey:@"view_more"];
    title_name = [data objectForKey:@"title_name"];
    img_url = [data objectForKey:@"img_url"];
    expired = [[data valueForKey:@"expired"] boolValue];
    promotion_id = [[data valueForKey:@"id"] integerValue];
    from_time = [data objectForKey:@"from_time"];
    
    return self;
}

@end


