//
//  FeaturesTableViewCell.h
//  Taj Rummy
//
//  Created by svc on 19/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Promotions.h"


NS_ASSUME_NONNULL_BEGIN

@interface FeaturesTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnReadmore;

-(void)updateCellWithData:(NSDictionary *)data;
-(void)updateCellWithPromotionsData:(Promotions *)data;


@end

NS_ASSUME_NONNULL_END
