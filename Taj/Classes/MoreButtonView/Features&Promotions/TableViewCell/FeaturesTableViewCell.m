//
//  FeaturesTableViewCell.m
//  Taj Rummy
//
//  Created by svc on 19/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "FeaturesTableViewCell.h"

@interface FeaturesTableViewCell ()

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblInfo;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogoPromotions;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *spinner;

@end

@implementation FeaturesTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)updateCellWithData:(NSDictionary *)data {
    
    self.imgLogo.image = [UIImage imageNamed:[data valueForKey:@"image"]];
    self.lblTitle.text = [data valueForKey:@"title"];
    self.lblInfo.text = [data valueForKey:@"info"];
    
    self.imgLogo.hidden = NO;
    self.imgLogoPromotions.hidden = YES;
}

-(void)updateCellWithPromotionsData:(Promotions *)data
{
    self.imgLogo.hidden = YES;
    self.imgLogoPromotions.hidden = NO;
    
    self.lblTitle.text = data.title_name;
    self.lblInfo.text = data.description;
    //[self.imgLogo setImageWithURL:[NSURL URLWithString:data.img_url] placeholderImage:nil usingActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
    NSString * url = data.img_url;
    [self.spinner startAnimating];
    dispatch_async(dispatch_get_global_queue(0,0), ^{
        NSData * data = [[NSData alloc] initWithContentsOfURL: [NSURL URLWithString: url]];
        if ( data == nil )
            return;
        dispatch_async(dispatch_get_main_queue(), ^{
            self.imgLogoPromotions.image = [UIImage imageWithData: data];
            [self.spinner stopAnimating];
            self.spinner.hidden = YES;
        });
    });
}

@end
