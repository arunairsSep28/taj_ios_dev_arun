//
//  EditProfileViewController.m
//  TajRummy
//
//  Created by Grid Logic on 19/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "EditProfileViewController.h"

@interface EditProfileViewController ()<UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextField *txtFieldFirstName;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldLastName;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldDOB;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldGender;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldState;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldCity;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldPinCode;

//DatePicker
@property (weak, nonatomic) IBOutlet UIView *viewDatePickerContainer;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;
@property (assign, nonatomic) NSInteger optionClickedIndex;
@property (strong, nonatomic) NSString *selectedDate;

//picker
@property (weak, nonatomic) IBOutlet UIView *viewPickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSMutableArray *arrayStates;
@property (strong, nonatomic) State *selectedStateObj;

@property (strong, nonatomic) NSArray *arrayGender;

@property(strong, nonatomic) NSDictionary *profileData;

@end

@implementation EditProfileViewController

- (instancetype)initWithProfileData:(NSDictionary *)profileData
{
    self = [super init];
    if (self) {
        _profileData = profileData;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    if (self.profileData) {
        [self loadData];
    }
    
    [self configureView];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)viewWillAppear:(BOOL)animated
{
    [self getStatesList];
}

-(void)configureView
{
    self.arrayGender = @[@"Male",@"Female"];
    
    self.viewDatePickerContainer.hidden = YES;
    self.viewPickerContainer.hidden = YES;
}


#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    
    if ([self.txtFieldFirstName.text length] == 0) {
        [self.view makeToast:@"First name can not be empty"];
        return YES;
    }
    else if ([self.txtFieldFirstName.text length] < 4) {
        [self.view makeToast:@"FirstName minimum should be 4 characters"];
        return YES;
    }
    
    else if ([self.txtFieldLastName.text length] == 0) {
        [self.view makeToast:@"Last name can not be empty"];
        return YES;
    }
    else if ([self.txtFieldLastName.text length] < 4) {
        [self.view makeToast:@"LastName minimum should be 4 characters"];
        return YES;
    }
    
    else if ([self.txtFieldDOB.text length] == 0) {
        [self.view makeToast:@"DOB can not be empty"];
        return YES;
    }
    else if ([self.txtFieldGender.text length] == 0) {
        [self.view makeToast:@"Gender can not be empty"];
        return YES;
    }
    
    else if ([self.txtFieldAddress1.text length] == 0) {
        [self.view makeToast:@"Address1 not be empty"];
        return YES;
    }
    else if ([self.txtFieldAddress1.text length] < 4) {
        [self.view makeToast:@"Address1  minimum should be 4 characters"];
        return YES;
    }
    
    else if ([self.txtFieldAddress2.text length] == 0) {
        [self.view makeToast:@"Address2 can not be empty"];
        return YES;
    }
    
    else if ([self.txtFieldAddress2.text length] < 4) {
        [self.view makeToast:@"Address2 minimum should be 4 characters"];
        return YES;
    }
    
    else if ([self.txtFieldState.text length] == 0) {
        [self.view makeToast:@"State can not be empty"];
        return YES;
    }
    else if ([self.txtFieldCity.text length] == 0) {
        [self.view makeToast:@"City can not be empty"];
        return YES;
    }
    
    else if ([self.txtFieldCity.text length] < 3) {
        [self.view makeToast:@"City name minimum should be 3 characters"];
        return YES;
    }
    
    else if ([self.txtFieldPinCode.text length] == 0) {
        [self.view makeToast:@"Pincode can not be empty"];
        return YES;
    }
    else if ([self.txtFieldPinCode.text length] < 6) {
        [self.view makeToast:@"Enter valid pincode"];
        return YES;
    }
    return NO;
}


-(void)getStatesList {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,STATES_LIST];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            //NSLog(@"STATES Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                // NSLog(@"STATES DATA : %@",[responseData valueForKey:@"data"]);
                NSDictionary * dataDic = [responseData valueForKey:@"data"];
                self.arrayStates = [[NSMutableArray alloc] init];
                
                for (NSMutableDictionary *areaDic in [dataDic valueForKey:@"states_list"]) {
                    State *areaObject = [State sharedInstance];
                    [areaObject assignData:areaDic];
                    [self.arrayStates addObject:areaObject];
                }
                NSLog(@"STATES Count : %lu",(unsigned long)self.arrayStates.count);
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        NSLog(@"STATES ERROR : %@",errorString);
        [self.view makeToast:errorString];
    }];
    
}

-(void)loadData {
    self.txtFieldLastName.text = [self.profileData valueForKey:@"lastname"];
    self.txtFieldFirstName.text = [self.profileData valueForKey:@"firstname"];
    self.txtFieldDOB.text = [self.profileData valueForKey:@"dob"];
    self.selectedDate = [self.profileData valueForKey:@"dob"];
    self.txtFieldGender.text = [self.profileData valueForKey:@"gender"];
    self.txtFieldAddress1.text = [self.profileData valueForKey:@"region"];
    self.txtFieldAddress2.text = [self.profileData valueForKey:@"address"];
    self.txtFieldState.text = [self.profileData valueForKey:@"state"];
    self.txtFieldCity.text = [self.profileData valueForKey:@"city"];
    NSInteger pincode = [[self.profileData valueForKey:@"zipcode"] integerValue];
    self.txtFieldPinCode.text = [NSString stringWithFormat:@"%ld",(long)pincode];
    if (pincode == 0) {
        self.txtFieldPinCode.text = @"";
    }
}

-(void)updateProfile
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"firstname": self.txtFieldFirstName.text,
                             @"lastname": self.txtFieldLastName.text,
                             @"city" : self.txtFieldCity.text,
                             @"address1" : self.txtFieldAddress1.text,
                             @"address2" : self.txtFieldAddress2.text,
                             @"state" : self.txtFieldState.text,
                             @"zip" : self.txtFieldPinCode.text,
                             @"gender" : self.txtFieldGender.text,
                             @"dob" : self.selectedDate
                             };
#if DEBUG
    NSLog(@"updateProfile PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"header: %@", header);
#endif
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,EDIT_PROFILE];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"RESPONSE: %@", jsondata);
#endif
                        NSString *date = self.txtFieldDOB.text;
                        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
                        [dateFormatter setDateFormat:@"yyyy-mm-dd"];
                        NSDate *dateFromString = [dateFormatter dateFromString:date];
                        NSLog(@"DOB : %@",dateFromString);
                        NSCalendar *calendar =[NSCalendar currentCalendar];
                        NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit |NSYearCalendarUnit) fromDate:dateFromString];
                        int day =[components day]; //day from the date
                        int month = [components month];//month from the date
                        int year = [components year];// year from the date
                        [dateFormatter setDateFormat:@"yyyy-mm-dd"] ;
                        NSDate *InDate = [dateFormatter stringFromDate:dateFromString];
                        NSLog(@"InDate : %@",InDate);

                        if (InDate) {
                            id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                            NSDictionary *userData=@{
                                                     @"userId":userId,
                                                     @"firstname": self.txtFieldFirstName.text,
                                                     @"lastname": self.txtFieldLastName.text,
                                                     @"city" : self.txtFieldCity.text,
                                                     @"address1" : self.txtFieldAddress1.text,
                                                     @"address2" : self.txtFieldAddress2.text,
                                                     @"state" : self.txtFieldState.text,
                                                     @"zip" : self.txtFieldPinCode.text,
                                                     @"gender" : self.txtFieldGender.text,
                                                     @"dob" : InDate
                                                     };
                            [weAnalytics trackEventWithName:@"ProfileUpdate" andValue:userData];
                        }
                        
                        UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                                       message:[jsondata valueForKey:@"message"]
                                                                                preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                              handler:^(UIAlertAction * action) {
                                                                                  [self dismissViewControllerAnimated:YES completion:nil];
                                                                              }];
                        
                        [alert addAction:defaultAction];
                        [self presentViewController:alert animated:YES completion:nil];
                        
                    }
                    else {
                        [self.view hideToastActivity];
                        NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
                [self.view hideToastActivity];
            }
        });
    }];
    
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
- (IBAction)updateClicked:(UIButton *)sender {
    
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self updateProfile];
    }
}

#pragma mark - DateOfBirth

- (IBAction)optionsClicked:(UIButton *)sender
{
    self.optionClickedIndex = (int)[sender tag];
    
    if (self.optionClickedIndex == 0) {
        self.datePicker.maximumDate = [NSDate date];
        self.viewDatePickerContainer.hidden = NO;
        // [self.scrollView bringSubviewToFront:self.viewDatePickerContainer];
        self.datePicker.backgroundColor = [UIColor whiteColor];
        [self.datePicker setValue:[UIColor blackColor] forKey:@"textColor"];
    }
    else if (self.optionClickedIndex == 1) {
        if ([self.arrayStates count] > 0) {
            self.viewPickerContainer.hidden = NO;
            [self.pickerView reloadAllComponents];
        }
    }
    else if (self.optionClickedIndex == 2) {
        self.viewPickerContainer.hidden = NO;
        [self.pickerView reloadAllComponents];
    }
}


- (IBAction)hideDatePickerView:(id)sender
{
    self.viewDatePickerContainer.hidden = YES;
}

- (IBAction)doneDatePickerView:(id)sender
{
    NSDateFormatter *formatter1 = [[NSDateFormatter alloc] init];
    [formatter1 setDateFormat:@"MM-dd-yyyy"];
    self.selectedDate = [formatter1 stringFromDate:self.datePicker.date];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MM-dd-yyyy"];
    NSString *displayDate = [formatter stringFromDate:self.datePicker.date];
    
    self.txtFieldDOB.text = displayDate;
    self.viewDatePickerContainer.hidden = YES;
}

#pragma mark - PickerView Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    if (self.optionClickedIndex == 2) {
        return [self.arrayStates count];
    }
    else {
        return self.arrayGender.count;
    }
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    if (self.optionClickedIndex == 2) {
        State * stateObject = self.arrayStates[row];
        return [[NSString stringWithFormat:@"%@",stateObject.name] capitalizedString];
    }
    else {
        return [NSString stringWithFormat:@"%@",self.arrayGender[row]];
    }
}

#pragma mark - STATE PICKER

- (IBAction)hidePickerView:(id)sender
{
    self.viewPickerContainer.hidden = YES;
}

- (IBAction)donePickerView:(id)sender
{
    NSUInteger num = [[self.pickerView dataSource] numberOfComponentsInPickerView:self.pickerView];
    if (self.optionClickedIndex == 2) {
        for (int i = 0; i < num; i++) {
            NSUInteger selectRow = [self.pickerView selectedRowInComponent:i];
            self.selectedStateObj = self.arrayStates[selectRow];
            self.txtFieldState.text = [self.selectedStateObj.name capitalizedString];
            
        }
    }
    else {
        for (int i = 0; i < num; i++) {
            NSUInteger selectRow = [self.pickerView selectedRowInComponent:i];
            self.txtFieldGender.text = self.arrayGender[selectRow];
            
        }
    }
    self.viewPickerContainer.hidden = YES;
}

#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Name
    if (textField == self.txtFieldFirstName) {
        return !([textField.text length] > 15 && [string length] > range.length);
    }
    else if (textField == self.txtFieldLastName) {
        return !([textField.text length] > 15 && [string length] > range.length);
    }
    
    else if (textField == self.txtFieldAddress1) {
        return !([textField.text length] > 30 && [string length] > range.length);
    }
    
    else if (textField == self.txtFieldAddress2) {
        return !([textField.text length] > 30 && [string length] > range.length);
    }
    
    else if (textField == self.txtFieldCity) {
        return !([textField.text length] > 30 && [string length] > range.length);
    }
    
    // pincode Number
    else if (textField == self.txtFieldPinCode) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        // Limit pincode number input to 6 digits
        return ([string isEqualToString:filteredString] ? newLength <= 6 : NO);
    }
    
    return YES;
}

@end
