//
//  ChangePasswordViewController.m
//  Taj Rummy
//
//  Created by svc on 07/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "ChangeUsernameViewController.h"

@interface ChangeUsernameViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldUsername;

@end

@implementation ChangeUsernameViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    [self configureView];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.viewContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
}


#pragma mark - Helpers

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldUsername.text length] == 0) {
        [self.view makeToast:@"Username can not be empty"];
        return YES;
    }
    return NO;
}
    
#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)updateClicked:(UIButton *)sender {
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"email": self.txtFieldUsername.text
                                 };
#if DEBUG
        NSLog(@"LOGIN PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = @"http://rv.glserv.info/api/v1/change-email/";
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"RESPONSE: %@", jsondata);
#endif
                            self.txtFieldUsername.text = @"";
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                        else {
                            NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view hideToastActivity];
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
        
    }
}

#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Username Validation
    if (textField == self.txtFieldUsername) {
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:USERNAME_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        return [string isEqualToString:filteredString];
    }
    
    return YES;
}



@end
