//
//  ReferFriendViewController.m
//  Taj Rummy
//
//  Created by svc on 07/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "ReferFriendViewController.h"

@interface ReferFriendViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewReferal;
@property (weak, nonatomic) IBOutlet UILabel *lblReferalCode;

@property (weak, nonatomic) IBOutlet UILabel *lblReferalLink;
@property (weak, nonatomic) IBOutlet UIView *viewEmailsContainer;
@property (weak, nonatomic) IBOutlet UIView *viewEmail2Container;
@property (weak, nonatomic) IBOutlet UIView *viewEmail3Container;
@property (weak, nonatomic) IBOutlet UIView *viewEmail4Container;
@property (weak, nonatomic) IBOutlet UIView *viewEmail5Container;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewEmail1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewEmail2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewEmail3HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewEmail4HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewEmail5HeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *viewSMSContainer;
@property (weak, nonatomic) IBOutlet UIView *viewSMS2Container;
@property (weak, nonatomic) IBOutlet UIView *viewSMS3Container;
@property (weak, nonatomic) IBOutlet UIView *viewSMS4Container;
@property (weak, nonatomic) IBOutlet UIView *viewSMS5Container;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSMS1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSMS2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSMS3HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSMS4HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewSMS5HeightConstraint;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail3;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail4;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail5;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmailName1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmailName2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmailName3;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmailName4;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmailName5;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldMobile1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMobile2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMobile3;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMobile4;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMobile5;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldName1;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldName2;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldName3;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldName4;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldName5;

@property (strong, nonatomic) NSPredicate *emailPredicate, *phonePredicate;

@property (assign, nonatomic) CGFloat height;
@property (weak, nonatomic) IBOutlet UIView *viewShare;

@end

@implementation ReferFriendViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"RAF Screen"];
    
    self.emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexEmail];
    
    self.phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexPhoneNumber];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [self getReferalDetails];
    [self configureView];
    
    if ([TAJUtilities isIPhone]){
        self.height = 44;
    }
    else {
        self.height = 64;
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.viewReferal.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewEmailsContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewSMSContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.viewEmail2Container.hidden = YES;
    self.viewEmail3Container.hidden = YES;
    self.viewEmail4Container.hidden = YES;
    self.viewEmail5Container.hidden = YES;
    
    self.viewEmail2HeightConstraint.constant = 0;
    self.viewEmail3HeightConstraint.constant = 0;
    self.viewEmail4HeightConstraint.constant = 0;
    self.viewEmail5HeightConstraint.constant = 0;
    
    self.viewSMS2Container.hidden = YES;
    self.viewSMS3Container.hidden = YES;
    self.viewSMS4Container.hidden = YES;
    self.viewSMS5Container.hidden = YES;
    
    self.viewSMS2HeightConstraint.constant = 0;
    self.viewSMS3HeightConstraint.constant = 0;
    self.viewSMS4HeightConstraint.constant = 0;
    self.viewSMS5HeightConstraint.constant = 0;
    
}


#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void)setDefaults {
    self.txtFieldName1.text = @"";
    self.txtFieldName2.text = @"";
    self.txtFieldName3.text = @"";
    self.txtFieldName4.text = @"";
    self.txtFieldName5.text = @"";
    
    self.txtFieldMobile1.text = @"";
    self.txtFieldMobile2.text = @"";
    self.txtFieldMobile3.text = @"";
    self.txtFieldMobile4.text = @"";
    self.txtFieldMobile5.text = @"";
    
    self.txtFieldEmailName1.text = @"";
    self.txtFieldEmailName2.text = @"";
    self.txtFieldEmailName3.text = @"";
    self.txtFieldEmailName4.text = @"";
    self.txtFieldEmailName5.text = @"";
    
    self.txtFieldEmail1.text = @"";
    self.txtFieldEmail2.text = @"";
    self.txtFieldEmail3.text = @"";
    self.txtFieldEmail4.text = @"";
    self.txtFieldEmail5.text = @"";
}

- (BOOL)validateEmailInput
{
    if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail1.text]) && ([self.txtFieldEmail1.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail2.text]) && ([self.txtFieldEmail2.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail3.text]) && ([self.txtFieldEmail3.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail4.text]) && ([self.txtFieldEmail4.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail5.text]) && ([self.txtFieldEmail5.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    
    return NO;
}

- (BOOL)validateMobileInput
{
    if (![self.phonePredicate evaluateWithObject:self.txtFieldMobile1.text] && ([self.txtFieldMobile1.text length] > 0)) {
        [self.view makeToast:@"Enter valid mobile number"];
        return YES;
    }
    else if (![self.phonePredicate evaluateWithObject:self.txtFieldMobile2.text] && ([self.txtFieldMobile2.text length] > 0)) {
        [self.view makeToast:@"Enter valid mobile number"];
        return YES;
    }
    else if (![self.phonePredicate evaluateWithObject:self.txtFieldMobile3.text] && ([self.txtFieldMobile3.text length] > 0)) {
        [self.view makeToast:@"Enter valid mobile number"];
        return YES;
    }
    else if (![self.phonePredicate evaluateWithObject:self.txtFieldMobile4.text] && ([self.txtFieldMobile4.text length] > 0)) {
        [self.view makeToast:@"Enter valid mobile number"];
        return YES;
    }
    else if (![self.phonePredicate evaluateWithObject:self.txtFieldMobile5.text] && ([self.txtFieldMobile5.text length] > 0)) {
        [self.view makeToast:@"Enter valid mobile number"];
        return YES;
    }
    
    return NO;
}

-(void) getReferalDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,REFER_FRIEND];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"AccountOverviewDetails Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSDictionary * responseDic = [responseData valueForKey:@"data"];
                self.lblReferalCode.text = [responseDic valueForKey:@"trackid"];
                self.lblReferalLink.text = [responseDic valueForKey:@"link"];
            }
        });
        NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        NSLog(@"AccountOverviewDetails ERROR : %@",errorString);
    }];
}

-(void)inviteViaEmail
{
    [self hideKeyboard];
    BOOL error = [self validateEmailInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"email-1": self.txtFieldEmail1.text,
                                 @"email-2": self.txtFieldEmail2.text,
                                 @"email-3": self.txtFieldEmail3.text,
                                 @"email-4": self.txtFieldEmail4.text,
                                 @"email-5": self.txtFieldEmail5.text,
                                 @"name-1": self.txtFieldEmailName1.text,
                                 @"name-2": self.txtFieldEmailName2.text,
                                 @"name-3": self.txtFieldEmailName3.text,
                                 @"name-4": self.txtFieldEmailName4.text,
                                 @"name-5": self.txtFieldEmailName5.text,
                                 @"request_type" : @"sms"
                                 };
#if DEBUG
        NSLog(@"INVITE EMAIL PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,INVITE];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"INVITE EMAIL RESPONSE: %@", jsondata);
#endif
                            [self setDefaults];
                            
                            id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                            
                            NSDictionary *userData=@{
                                                     @"userId":userId
                                                     };
                            [weAnalytics trackEventWithName:@"RAF Request" andValue:userData];
                            
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                        else {
                            NSLog(@"INVITE Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
    }
}

-(void)inviteViaSMS
{
    [self hideKeyboard];
    BOOL error = [self validateMobileInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"mobile-1": self.txtFieldMobile1.text,
                                 @"mobile-2": self.txtFieldMobile2.text,
                                 @"mobile-3": self.txtFieldMobile3.text,
                                 @"mobile-4": self.txtFieldMobile4.text,
                                 @"mobile-5": self.txtFieldMobile5.text,
                                 @"name-1": self.txtFieldName1.text,
                                 @"name-2": self.txtFieldName2.text,
                                 @"name-3": self.txtFieldName3.text,
                                 @"name-4": self.txtFieldName4.text,
                                 @"name-5": self.txtFieldName5.text
                                 };
#if DEBUG
        NSLog(@"INVITE MOBILE PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,INVITE];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"INVITE MOBILE RESPONSE: %@", jsondata);
#endif
                            [self setDefaults];
                            id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                            
                            NSDictionary *userData=@{
                                                     @"userId":userId
                                                     };
                            [weAnalytics trackEventWithName:@"RAF Request" andValue:userData];
                            
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                        else {
                            NSLog(@"INVITE MOBILE Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
    }
}


#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)referCodeClicked:(UIButton *)sender {
    
    NSMutableArray *activityItems;
    
    if (sender.tag == 1) {
        NSString * code = @"ReferalCode";
        activityItems = [NSMutableArray arrayWithObjects:self.lblReferalCode.text,  nil];
    }
    else
    {
        NSString * code = @"link";
        activityItems = [NSMutableArray arrayWithObjects:self.lblReferalLink.text,  nil];
    }
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    activityViewController.excludedActivityTypes = @[UIActivityTypePrint,                                                         UIActivityTypeAssignToContact,                                                         UIActivityTypeSaveToCameraRoll,UIActivityTypeAddToReadingList,                                                                                                                  UIActivityTypeAirDrop];
    
    [self presentViewController:activityViewController animated:YES completion:nil];
    if ( [activityViewController respondsToSelector:@selector(popoverPresentationController)] ) {
        // iOS8
        activityViewController.popoverPresentationController.sourceView = self.viewShare;
    }
    
}

- (IBAction)emailPlusClicked:(UIButton *)sender {
    if (sender.tag == 1) {
        self.viewEmail2Container.hidden = NO;
        self.viewEmail2HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 2) {
        self.viewEmail3Container.hidden = NO;
        self.viewEmail3HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 3) {
        self.viewEmail4Container.hidden = NO;
        self.viewEmail4HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 4) {
        self.viewEmail5Container.hidden = NO;
        self.viewEmail5HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 21) {
        self.viewSMS2Container.hidden = NO;
        self.viewSMS2HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 22) {
        self.viewSMS3Container.hidden = NO;
        self.viewSMS3HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 23) {
        self.viewSMS4Container.hidden = NO;
        self.viewSMS4HeightConstraint.constant = self.height;
    }
    else if (sender.tag == 24) {
        self.viewSMS5Container.hidden = NO;
        self.viewSMS5HeightConstraint.constant = self.height;
    }
}

- (IBAction)emailDeleteClicked:(UIButton *)sender {
    if (sender.tag == 7) {
        self.viewEmail2Container.hidden = YES;
        self.viewEmail2HeightConstraint.constant = 0;
    }
    else if (sender.tag == 8) {
        self.viewEmail3Container.hidden = YES;
        self.viewEmail3HeightConstraint.constant = 0;
    }
    else if (sender.tag == 9) {
        self.viewEmail4Container.hidden = YES;
        self.viewEmail4HeightConstraint.constant = 0;
    }
    else if (sender.tag == 10) {
        self.viewEmail5Container.hidden = YES;
        self.viewEmail5HeightConstraint.constant = 0;
    }
    
    else if (sender.tag == 27) {
        self.viewSMS2Container.hidden = YES;
        self.viewSMS2HeightConstraint.constant = 0;
    }
    else if (sender.tag == 28) {
        self.viewSMS3Container.hidden = YES;
        self.viewSMS3HeightConstraint.constant = 0;
    }
    else if (sender.tag == 29) {
        self.viewSMS4Container.hidden = YES;
        self.viewSMS4HeightConstraint.constant = 0;
    }
    else if (sender.tag == 30) {
        self.viewSMS5Container.hidden = YES;
        self.viewSMS5HeightConstraint.constant = 0;
    }
}

- (IBAction)inviteViaEmailClicked:(UIButton *)sender {
    [self hideKeyboard];
    if (([self.txtFieldEmailName1.text length] > 0 && [self.txtFieldEmail1.text length] > 0) || ([self.txtFieldEmailName2.text length] > 0 && [self.txtFieldEmail2.text length] > 0) || ([self.txtFieldEmailName3.text length] > 0 && [self.txtFieldEmail3.text length] > 0) || ([self.txtFieldEmailName4.text length] > 0 && [self.txtFieldEmail4.text length] > 0) || ([self.txtFieldEmailName5.text length] > 0 && [self.txtFieldEmail5.text length] > 0)) {
        
        [self inviteViaEmail];
    }
    else {
        [self.view makeToast:@"Please enter all fields"];
    }
}

- (IBAction)inviteViaSMSClicked:(UIButton *)sender {
    [self hideKeyboard];
    if (([self.txtFieldName1.text length] > 0 && [self.txtFieldMobile1.text length] > 0) || ([self.txtFieldName2.text length] > 0 && [self.txtFieldMobile2.text length] > 0) || ([self.txtFieldName3.text length] > 0 && [self.txtFieldMobile3.text length] > 0) || ([self.txtFieldName4.text length] > 0 && [self.txtFieldMobile4.text length] > 0) || ([self.txtFieldName5.text length] > 0 && [self.txtFieldMobile5.text length] > 0)) {
        
        [self inviteViaSMS];
    }
    else {
        [self.view makeToast:@"Please enter all fields"];
    }
}


#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // Name
    if (textField == self.txtFieldName1 || textField == self.txtFieldName2 || textField == self.txtFieldName3 || textField == self.txtFieldName4 || textField == self.txtFieldName5) {
        return !([textField.text length] > 10 && [string length] > range.length);
    }
    
    if (textField == self.txtFieldEmailName1 || textField == self.txtFieldEmailName2 || textField == self.txtFieldEmailName3 || textField == self.txtFieldEmailName4 || textField == self.txtFieldEmailName5) {
        return !([textField.text length] > 10 && [string length] > range.length);
    }
    
    // Email
    if (textField == self.txtFieldEmail1 || textField == self.txtFieldEmail2 || textField == self.txtFieldEmail3 || textField == self.txtFieldEmail4 || textField == self.txtFieldEmail5) {
        return !([textField.text length] > EmailAddressCharacterLimit && [string length] > range.length);
    }
    
    // mobile Number
    if (textField == self.txtFieldMobile1 || textField == self.txtFieldMobile2 || textField == self.txtFieldMobile3 || textField == self.txtFieldMobile4 || textField == self.txtFieldMobile5) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        // Limit pincode number input to 6 digits
        return ([string isEqualToString:filteredString] ? newLength <= 10 : NO);
    }
    
    return YES;
}

@end
