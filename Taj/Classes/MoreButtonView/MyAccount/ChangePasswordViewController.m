//
//  ChangePasswordViewController.m
//  Taj Rummy
//
//  Created by svc on 07/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "ChangePasswordViewController.h"

@interface ChangePasswordViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldCurrentPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFiedlNewPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldConfirmPassword;

@end

@implementation ChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [self configureView];

}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.viewContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - Helpers

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldCurrentPassword.text length] == 0) {
        [self.view makeToast:@"Current password can not be empty"];
        return YES;
    }
    else if ([self.txtFiedlNewPassword.text length] == 0) {
        [self.view makeToast:@"New password can not be empty"];
        return YES;
    }
    else if ([self.txtFieldConfirmPassword.text length] == 0) {
        [self.view makeToast:@"Confirm Password can not be empty"];
        return YES;
    }
    else if ([self.txtFiedlNewPassword.text length] < PasswordMinLength) {
        
        [self.view makeToast:@"Password minimum should be 5 characters"];
        return YES;
    }
    else if ([self.txtFieldConfirmPassword.text length] < PasswordMinLength) {
        [self.view makeToast:@"Confirm Password minimum should be 5 characters"];
        return YES;
    }
    
    else if(![self.txtFiedlNewPassword.text isEqualToString:self.txtFieldConfirmPassword.text]) {
        [self.view makeToast:@"Password not matching"];
        return YES;
    }
    
    else if([self.txtFieldCurrentPassword.text isEqualToString:self.txtFiedlNewPassword.text]) {
        [self.view makeToast:@"New password and Current password should not be same"];
        return YES;
    }
    return NO;
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)updateClicked:(UIButton *)sender {
    [self hideKeyboard];
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"cpassword":
                                     self.txtFieldCurrentPassword.text,
                                 @"password":self.txtFiedlNewPassword.text,
                                 @"password1":self.txtFieldConfirmPassword.text
                                 };
#if DEBUG
        NSLog(@"LOGIN PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_PASSWORD];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"RESPONSE: %@", jsondata);
#endif
                            self.txtFieldCurrentPassword.text = @"";
                            self.txtFiedlNewPassword.text = @"";
                            self.txtFieldConfirmPassword.text = @"";
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                        else {
                            NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view hideToastActivity];
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
        
    }
}

#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{

    // Password
    if (textField == self.txtFieldCurrentPassword) {
        return !([textField.text length] > PasswordLength && [string length] > range.length);
    }
    // Retype Password
    if (textField == self.txtFiedlNewPassword) {
        return !([textField.text length] > PasswordLength && [string length] > range.length);
    }
    if (textField == self.txtFieldConfirmPassword) {
        return !([textField.text length] > PasswordLength && [string length] > range.length);
    }
    
    return YES;
}


@end
