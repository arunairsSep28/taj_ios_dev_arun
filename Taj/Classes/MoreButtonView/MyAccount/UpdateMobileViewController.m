//
//  ChangePasswordViewController.m
//  Taj Rummy
//
//  Created by svc on 07/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "ChangeUsernameViewController.h"

@interface UpdateMobileViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldMobile;
@property (strong, nonatomic) NSString *mobileNumber;
@property (assign ,nonatomic) BOOL isFromEdt;

@property (strong, nonatomic) NSPredicate  *phonePredicate;

@end

@implementation UpdateMobileViewController

- (instancetype)initWithMobileNumber:(NSString *)mobileNumber isFromEdt:(BOOL)isFromEdt
{
    self = [super init];
    if (self) {
        _mobileNumber = mobileNumber;
        _isFromEdt = isFromEdt;
    }
    
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];

     self.phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexPhoneNumber];
    
    if (self.isFromEdt) {
        if ([self.mobileNumber isEqualToString:@""]) {
            self.txtFieldMobile.text = self.mobileNumber;
            [self.btnUpdate setTitle:@"Update Mobile" forState:UIControlStateNormal];
            self.lblTitle.text = @"Update Mobile Number";
        }
        else {
        self.txtFieldMobile.text = self.mobileNumber;
//        [self.btnUpdate setTitle:@"Generate OTP" forState:UIControlStateNormal];
//        self.lblTitle.text = @"Update Mobile Number";
            [self generateOTP];
            
        }
    }
    else {
        [self.btnUpdate setTitle:@"Update Mobile" forState:UIControlStateNormal];
        self.lblTitle.text = @"Update Mobile Number";
    }
    [self configureView];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.viewContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldMobile.text length] == 0) {
        [self.view makeToast:@"Mobile number can not be empty"];
        return YES;
    }
    else if (![self.phonePredicate evaluateWithObject:self.txtFieldMobile.text]) {
        [self.view makeToast:@"Enter valid mobile number"];
        return YES;
    }
    return NO;
}

- (BOOL)validateOTPInput
{
    if ([self.txtFieldMobile.text length] == 0) {
        [self.view makeToast:@"Enter OTP"];
        return YES;
    }
    
    return NO;
}

-(void)updateMobile
{
    [self hideKeyboard];
    BOOL error = [self validateInput];
    if (!error) {

    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"mobile": self.txtFieldMobile.text
                             };
#if DEBUG
    NSLog(@"LOGIN PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_MOBILE];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"RESPONSE: %@", jsondata);
#endif
                        
                        id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                        
                        NSDictionary *userData=@{
                                                 @"mobile":[jsondata valueForKey:@"mobile"],
                                                 @"MobileVerificationStatus" : [NSNumber numberWithBool:0]
                                                 };
                        [weAnalytics trackEventWithName:@"MobileUpdate" andValue:userData];
                        
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        // ENTER OTP FOR VERIFICATION
                        self.lblTitle.text = @"Enter Otp for Verification";
                        self.txtFieldMobile.text = @"";
                        self.txtFieldMobile.placeholder = @"Enter OTP";
                        [self.btnUpdate setTitle:@"Verify OTP" forState:UIControlStateNormal];
                    }
                    else {
                        NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];
    }
}

-(void)generateOTP
{
    [self hideKeyboard];
    BOOL error = [self validateInput];
    if (!error) {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"mobile": self.mobileNumber};
#if DEBUG
    NSLog(@"generateOTP PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,GET_OTP];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                     NSLog(@"generateOTP RESPONSE: %@", jsondata);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"RESPONSE: %@", jsondata);
#endif
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        self.lblTitle.text = @"Enter Otp for Verification";
                        self.txtFieldMobile.text = @"";
                        self.txtFieldMobile.placeholder = @"Enter OTP";
                        [self.btnUpdate setTitle:@"Verify OTP" forState:UIControlStateNormal];
                    }
                    else {
                        NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];
    }
}

-(void)verifyOTP
{
    [self hideKeyboard];
    BOOL error = [self validateOTPInput];
    if (!error) {
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"token": self.txtFieldMobile.text
                             };
#if DEBUG
    NSLog(@"VERIFY OTP PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,MOBILE_VERIFY];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"RESPONSE: %@", jsondata);
#endif
                        id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                        
                        NSDictionary *userData = @{
                                                 @"userId":userId,
                                                 @"we_phone":_mobileNumber
                                                 };
                        [weAnalytics trackEventWithName:@"MobileVerification" andValue:userData];
                        
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        [self dismissViewControllerAnimated:NO completion:nil];
                    }
                    else {
                        NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];
    }
}
#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)updateClicked:(UIButton *)sender {
    if ([self.btnUpdate.titleLabel.text isEqualToString:@"Generate OTP"]) {
        //generate OTP
        [self generateOTP];
    }
    else if ([self.btnUpdate.titleLabel.text isEqualToString:@"Verify OTP"]){
        [self verifyOTP];
    }
    else {
        // update Mobile - change-mobile
        [self updateMobile];
    }
}

// to verify - mobile-verify

#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    
    // Mobile Number
    if ([self.btnUpdate.titleLabel.text isEqualToString:@"Verify OTP"]) {
        if (textField == self.txtFieldMobile) {
            return !([textField.text length] > 6 && [string length] > range.length);
        }
    }
    else {
        
        if (textField == self.txtFieldMobile) {
            // To block from entering zero as first digit of phone number
            if (range.location == 0 && [string isEqualToString:@"0"]) {
                return NO;
            }
            NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
            NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
            NSUInteger newLength = [textField.text length] + [string length] - range.length;
            
            // Limit phone number input to 8 digits
            return ([string isEqualToString:filteredString] ? newLength <= 10 : NO);
        }
        
    }
    return YES;
}


@end
