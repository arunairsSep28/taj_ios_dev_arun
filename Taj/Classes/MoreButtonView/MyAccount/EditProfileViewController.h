//
//  EditProfileViewController.h
//  TajRummy
//
//  Created by Grid Logic on 19/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EditProfileViewController : UIViewController
- (instancetype)initWithProfileData:(NSDictionary *)profileData;

@end

NS_ASSUME_NONNULL_END
