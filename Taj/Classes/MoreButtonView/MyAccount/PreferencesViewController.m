//
//  PreferencesViewController.m
//  Taj Rummy
//
//  Created by svc on 07/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "PreferencesViewController.h"

@interface PreferencesViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewEmail;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPhone;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewSMS;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewNews;
@property (weak, nonatomic) IBOutlet UIButton *buttonEmail;
@property (weak, nonatomic) IBOutlet UIButton *buttonSMS;
@property (weak, nonatomic) IBOutlet UIButton *buttonPhone;
@property (weak, nonatomic) IBOutlet UIButton *buttonNews;

@property (assign, nonatomic) BOOL isEmail;
@property (assign, nonatomic) BOOL isPhone;
@property (assign, nonatomic) BOOL isSMS;
@property (assign, nonatomic) BOOL isNews;
@property (strong, nonatomic) Preferences *preferencesObject;

@property (strong, nonatomic) NSString *emailPreference;
@property (strong, nonatomic) NSString *phonePreference;
@property (strong, nonatomic) NSString *smsPreference;
@property (strong, nonatomic) NSString *newsPreference;

@end

@implementation PreferencesViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Preferences Screen"];
    
    [self configureView];
    [self getReferalDetails];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)configureView {
    
    self.isEmail = NO;
    self.isPhone = NO;
    self.isSMS = NO;
    self.isNews = NO;
    [self unSetEmail];
    [self unSetPhone];
    [self unSetSMS];
    [self unSetNews];
    
}

#pragma mark - HELPERS

- (void)setEmail {
    self.imgViewEmail.image = [UIImage imageNamed:@"check"];
    self.emailPreference = @"true";
}
- (void)unSetEmail {
    self.imgViewEmail.image = [UIImage imageNamed:@"uncheck"];
    self.emailPreference = @"false";
}
- (void)setPhone {
    self.imgViewPhone.image = [UIImage imageNamed:@"check"];
    self.phonePreference = @"true";
}
- (void)unSetPhone {
    self.imgViewPhone.image = [UIImage imageNamed:@"uncheck"];
    self.phonePreference = @"false";
}
- (void)setSMS {
    self.imgViewSMS.image = [UIImage imageNamed:@"check"];
    self.smsPreference = @"true";
}
- (void)unSetSMS {
    self.imgViewSMS.image = [UIImage imageNamed:@"uncheck"];
    self.smsPreference = @"false";
    
}
- (void)setNews {
    self.imgViewNews.image = [UIImage imageNamed:@"check"];
    self.newsPreference = @"true";
}
- (void)unSetNews {
    self.imgViewNews.image = [UIImage imageNamed:@"uncheck"];
    self.newsPreference = @"false";
}

#pragma mark - HELPERS

-(void)getReferalDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,PREFERENCES];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"AccountOverviewDetails Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSDictionary * responseDic = [responseData valueForKey:@"data"];
                self.preferencesObject = [Preferences sharedInstance];
                [self.preferencesObject assignData:responseDic];
                [self loadData];
            }
            
            NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
        });
    } errorBlock:^(NSString *errorString) {
        NSLog(@"AccountOverviewDetails ERROR : %@",errorString);
    }];
}

-(void)loadData {
    
    self.imgViewSMS.image = [UIImage imageNamed:@"uncheck"];
    self.imgViewEmail.image = [UIImage imageNamed:@"uncheck"];
    self.imgViewPhone.image = [UIImage imageNamed:@"uncheck"];
    self.imgViewNews.image = [UIImage imageNamed:@"uncheck"];
    
    self.emailPreference = @"false";
    self.smsPreference = @"false";
    self.newsPreference = @"false";
    self.phonePreference = @"false";

    self.isSMS = NO;
    self.isEmail = NO;
    self.isPhone = NO;
    self.isNews = NO;
    
    if (self.preferencesObject.sms_preference) {
        self.imgViewSMS.image = [UIImage imageNamed:@"check"];
        self.smsPreference = @"true";
        self.isSMS = YES;
    }
    if (self.preferencesObject.newsletter_preference) {
        self.imgViewNews.image = [UIImage imageNamed:@"check"];
        self.newsPreference = @"true";
        self.isNews = YES;
    }
    if (self.preferencesObject.phone_preference) {
        self.imgViewPhone.image = [UIImage imageNamed:@"check"];
        self.phonePreference = @"true";
        self.isPhone = YES;
    }
    if (self.preferencesObject.email_preference) {
        self.imgViewEmail.image = [UIImage imageNamed:@"check"];
        self.emailPreference = @"true";
        self.isEmail = YES;
    }
}

-(void)updatePrefrences
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"phone_preference": self.phonePreference,
                             @"sms_preference": self.smsPreference,
                             @"email_preference" : self.emailPreference,
                             @"newsletter_preference" : self.newsPreference
                             };
#if DEBUG
    NSLog(@"PREFERENCES PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,PREFERENCES];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"PREFERENCES RESPONSE: %@", jsondata);
#endif
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                    else {
                        NSLog(@"PREFERENCES Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];
    
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)emailClicked:(UIButton *)sender {
    if (self.isEmail == YES) {
        self.isEmail = NO;
        [self unSetEmail];
    }
    else {
        self.isEmail = YES;
        [self setEmail];
    }
}

- (IBAction)smsClicked:(UIButton *)sender {
    if (self.isSMS == YES) {
        self.isSMS = NO;
        [self unSetSMS];
    }
    else {
        self.isSMS = YES;
        [self setSMS];
    }
}
- (IBAction)phoneClicked:(UIButton *)sender {
    if (self.isPhone == YES) {
        self.isPhone = NO;
        [self unSetPhone];
    }
    else {
        self.isPhone = YES;
        [self setPhone];
    }
}
- (IBAction)newsClicked:(UIButton *)sender {
    if (self.isNews == YES) {
        self.isNews = NO;
        [self unSetNews];
    }
    else {
        self.isNews = YES;
        [self setNews];
    }
}
- (IBAction)updateClicked:(UIButton *)sender {
    
    [self updatePrefrences];
}

@end
