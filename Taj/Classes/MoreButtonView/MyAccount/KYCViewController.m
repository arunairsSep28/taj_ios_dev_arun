//
//  KYCViewController.m
//  Taj Rummy
//
//  Created by svc on 07/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "KYCViewController.h"

@interface KYCViewController ()<UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UILabel *lblEmail;
@property (weak, nonatomic) IBOutlet UIView *viewEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailNotVerified;
@property (weak, nonatomic) IBOutlet UILabel *lblEmailStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgEmailStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblMobilel;
@property (weak, nonatomic) IBOutlet UIView *viewMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileNotVerified;
@property (weak, nonatomic) IBOutlet UILabel *lblMobileStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgMobieStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblPancard;
@property (weak, nonatomic) IBOutlet UIView *viewPancard;
@property (weak, nonatomic) IBOutlet UILabel *lblPancardNotVerified;
@property (weak, nonatomic) IBOutlet UILabel *lblPANStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgPANStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblAadhar;
@property (weak, nonatomic) IBOutlet UIView *viewAadhar;
@property (weak, nonatomic) IBOutlet UIView *viewAadharFront;
@property (weak, nonatomic) IBOutlet UIView *viewAadharBack;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharNotVerified;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgAadharStatus;

@property (weak, nonatomic) IBOutlet UILabel *lblAddressProof;
@property (weak, nonatomic) IBOutlet UIView *viewAddressProof;
@property (weak, nonatomic) IBOutlet UIView *viewAddressFront;
@property (weak, nonatomic) IBOutlet UIView *viewAddressBack;
@property (weak, nonatomic) IBOutlet UIView *viewAddressProofNumber;

@property (weak, nonatomic) IBOutlet UILabel *lblAddressProofNotVerified;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldIDProofNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblAddressStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddressStatus;

@property (weak, nonatomic) IBOutlet UIView *viewDocsMainContainer;
@property (weak, nonatomic) IBOutlet UIView *viewPancardDetails;
@property (weak, nonatomic) IBOutlet UIView *viewAadhaarDetails;
@property (weak, nonatomic) IBOutlet UIView *viewAddressDetails;

@property (weak, nonatomic) IBOutlet UITextField *txtFieldPAN;
@property (weak, nonatomic) IBOutlet UITextField *txtFielAadhaar;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAddress;

@property (weak, nonatomic) IBOutlet UIButton *btnPan;
@property (weak, nonatomic) IBOutlet UIButton *btnAadhaar;
@property (weak, nonatomic) IBOutlet UIButton *btnAadhaarBack;
@property (weak, nonatomic) IBOutlet UIButton *btnAddress;
@property (weak, nonatomic) IBOutlet UIButton *btnAddressBack;

@property (strong, nonatomic) UIImagePickerController *pickerController;

@property (assign, nonatomic) BOOL isPancard;
@property (assign, nonatomic) BOOL isAadhaar;
@property (assign, nonatomic) BOOL isAadhaarBack;
@property (assign, nonatomic) BOOL isAddress;
@property (assign, nonatomic) BOOL isAddressBack;

@property (strong, nonatomic) NSString * selectedPANImageBaseString;
@property (strong, nonatomic) NSString * selectedAadhaarImageBaseString;
@property (strong, nonatomic) NSString * selectedAadhaarBackImageBaseString;
@property (strong, nonatomic) NSString * selectedAddressImageBaseString;
@property (strong, nonatomic) NSString * selectedAddressBackImageBaseString;

//picker
@property (weak, nonatomic) IBOutlet UIView *viewPickerContainer;
@property (weak, nonatomic) IBOutlet UIPickerView *pickerView;
@property (strong, nonatomic) NSArray * arrayAddressProof;
@property (strong, nonatomic) NSString *selectedProof;

@property (weak, nonatomic) IBOutlet UILabel *lblPanAttachment;
@property (weak, nonatomic) IBOutlet UILabel *lblAadharAttachment;
@property (weak, nonatomic) IBOutlet UILabel *lblIDProofAttachment;

@property (strong, nonatomic) KYC *kYCDetailsObject;

@property (strong, nonatomic) UIColor * yellowColor;
@property (strong, nonatomic) UIColor * greenClr;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewPanHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewAadharHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *viewAddressHeightConstraint;

@property (weak, nonatomic) IBOutlet UIView *viewPanUpload;
@property (weak, nonatomic) IBOutlet UIView *viewAadharUpload;
@property (weak, nonatomic) IBOutlet UIView *viewAddressProofUpload;

@end

@implementation KYCViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"KYC Screen"];
    
    self.arrayAddressProof = @[@{
                                   @"proof" : @"Driving Licence",
                                   @"proof_id" : @"DL"
                                   },
                               @{
                                   @"proof" : @"Voter ID",
                                   @"proof_id" : @"VIC"
                                   },
                               @{
                                   @"proof" : @"Passport",
                                   @"proof_id" : @"PP"
                                   },
                               @{
                                   @"proof" : @"Bank Statement",
                                   @"proof_id" : @"BS"
                                   },
                               @{
                                   @"proof" : @"Electricity Bill",
                                   @"proof_id" : @"BE"
                                   },
                               @{
                                   @"proof" : @"Bank Passbook",
                                   @"proof_id" : @"PB"
                                   },
                               @{
                                   @"proof" : @"Other",
                                   @"proof_id" : @"OT"
                                   }
                               ];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    self.selectedPANImageBaseString = @"";
    self.selectedAadhaarImageBaseString = @"";
    self.selectedAadhaarBackImageBaseString = @"";
    self.selectedAddressImageBaseString = @"";
    self.selectedAddressBackImageBaseString = @"";
    self.selectedProof = @"";
    
    [self configureView];
    [self getKYCDetails];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}


-(void)configureView {
    
    self.viewPickerContainer.hidden = YES;
    self.scrollView.hidden = YES;
    self.lblPanAttachment.hidden = YES;
    self.lblAadharAttachment.hidden = YES;
    self.lblIDProofAttachment.hidden = YES;
    
    self.viewPancardDetails.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAadhaarDetails.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAadharFront.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAadharBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAddressFront.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAddressBack.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAddressDetails.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewAddressProofNumber.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    //251,176,59
    self.yellowColor = [UIColor colorWithRed:251/255.0
                                          green:176/255.0
                                           blue:59/255.0
                                          alpha:1];
    self.greenClr = [UIColor colorWithRed:106/255.0
                                       green:194/255.0
                                        blue:89/255.0
                                       alpha:1];
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

-(void)setDefaults {
    self.isPancard = NO;
    self.isAadhaar = NO;
    self.isAadhaarBack = NO;
    self.isAddressBack = NO;
    self.isAddress = NO;
}

- (BOOL)validateInput
{
    if ([self.txtFieldPAN.text length] > 0 && (![self validatePanCardNumber:[self.txtFieldPAN.text uppercaseString]])) {
        
        [self.view makeToast:@"Enter valid PAN Card Number"];
        return YES;
        
    }
    if ([self.txtFielAadhaar.text length] > 0) {
        if ([self.txtFielAadhaar.text length] < 12) {
            [self.view makeToast:@"Enter valid Aadhaar"];
            return YES;
        }
    }
    if (([self.txtFielAadhaar.text length] > 0) && ([self.selectedAadhaarImageBaseString isEqualToString:@""])) {
        [self.view makeToast:@"Please select aadhar image"];
        return YES;
    }
    if (([self.txtFielAadhaar.text length] == 0) && ((![self.selectedAadhaarImageBaseString isEqualToString:@""]) || (![self.selectedAadhaarBackImageBaseString isEqualToString:@""]))) {
        [self.view makeToast:@"Please enter aadhar number"];
        return YES;
    }
    if ([self.txtFieldAddress.text length] > 0) {
        if ([self.txtFieldIDProofNumber.text length] < 5) {
            [self.view makeToast:@"Enter valid Address proof details"];
            return YES;
        }
    }
    
//    if ([self.txtFieldIDProofNumber.text length] > 0) {
//        if (([self.selectedAddressImageBaseString length] == 0) || ([self.selectedAddressBackImageBaseString length] == 0)) {
//        [self.view makeToast:@"Please select address proof imageeeeee"];
//        return YES;
//    }
//    }
//    if ([self.txtFieldIDProofNumber.text length] == 0) {
//        if (([self.selectedAddressImageBaseString length] > 0) || ([self.selectedAddressBackImageBaseString length] > 0)) {
//        [self.view makeToast:@"Please enter address proof details"];
//        return YES;
//     }
//    }
    
    return NO;
}

- (BOOL)validateAadhar
{
    if ([self.txtFielAadhaar.text length] > 0) {
        if ([self.txtFielAadhaar.text length] < 12) {
            [self.view makeToast:@"Enter valid Aadhaar"];
            return YES;
        }
    }
    return NO;
}

- (BOOL)validateAddress
{
    if ([self.txtFieldIDProofNumber.text isEqualToString:@""]) {
            [self.view makeToast:@"Enter address proof details"];
            return YES;
    }
    return NO;
}

- (BOOL)validatePanCardNumber: (NSString *) cardNumber {
    //[A-Za-z]{5}\d{4}[A-Za-z]{1}">
    NSString *emailRegex = @"^[A-Za-z]{5}[0-9]{4}[A-Za-z]$";
    NSPredicate *cardTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [cardTest evaluateWithObject:cardNumber];
}


-(void)getKYCDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,KYC_DETAILS];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            self.scrollView.hidden = NO;
            NSLog(@"KYCDetails Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSDictionary * responseDic = [responseData valueForKey:@"data"];
                NSDictionary *dataDic = [responseDic dictionaryRemovingNSNullValues];
                self.kYCDetailsObject = [KYC sharedInstance];
                [self.kYCDetailsObject assignData:dataDic];
                [self loadData];
            }
            
            NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
        });
    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        self.scrollView.hidden = YES;
        NSLog(@"AccountOverviewDetails ERROR : %@",errorString);
    }];
}

-(void)loadData {
    //[[Crashlytics sharedInstance] crash];
    self.lblEmail.text = self.kYCDetailsObject.emailid;
    self.lblMobilel.text = self.kYCDetailsObject.mobile_no;
    self.lblPancard.text = self.kYCDetailsObject.pan_card_no;
    self.lblAadhar.text = self.kYCDetailsObject.aadhar_no;
    self.lblAddressProof.text = self.kYCDetailsObject.id_proof_no;
    
    self.lblEmailStatus.text = self.kYCDetailsObject.email_status;
    self.lblMobileStatus.text = self.kYCDetailsObject.mobile_no_status;
    self.lblPANStatus.text = self.kYCDetailsObject.pan_card_status;
    self.lblAadharStatus.text = self.kYCDetailsObject.aadhar_status;
    self.lblAddressStatus.text = self.kYCDetailsObject.id_proof_status;
    
    self.imgEmailStatus.image = [UIImage imageNamed:@""];
    self.imgMobieStatus.image = [UIImage imageNamed:@""];
    self.imgPANStatus.image = [UIImage imageNamed:@""];
    self.imgAadharStatus.image = [UIImage imageNamed:@""];
    self.imgAddressStatus.image = [UIImage imageNamed:@""];
    
    if ([[self.kYCDetailsObject.email_status lowercaseString] isEqualToString:@"not verified"]) {
        self.viewEmail.hidden = YES;
        self.lblEmailStatus.textColor = [UIColor blackColor];
    }
    else {
        self.viewEmail.hidden = NO;
        if (([[self.kYCDetailsObject.email_status lowercaseString] isEqualToString:@"verified"])) {
            self.imgEmailStatus.image = [UIImage imageNamed:@"verified"];
            self.lblEmailStatus.textColor = self.greenClr;
        }
        else {
            self.imgEmailStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblEmailStatus.textColor = [UIColor redColor];
        }
    }
    
    if ([[self.kYCDetailsObject.mobile_no_status lowercaseString] isEqualToString:@"not verified"]) {
        self.viewMobile.hidden = YES;
        self.lblMobileStatus.textColor = [UIColor blackColor];
    }
    else {
        self.viewMobile.hidden = NO;
        if (([[self.kYCDetailsObject.mobile_no_status lowercaseString] isEqualToString:@"verified"])) {
            self.imgMobieStatus.image = [UIImage imageNamed:@"verified"];
            self.lblMobileStatus.textColor = self.greenClr;
        }
        else {
            self.imgMobieStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblMobileStatus.textColor = [UIColor redColor];
        }
    }
    
    if ([[self.kYCDetailsObject.pan_card_status lowercaseString] isEqualToString:@"not verified"]) {
        self.viewPancard.hidden = YES;
        self.lblPANStatus.textColor = [UIColor blackColor];
        //self.viewPanHeightConstraint.constant = 81.5;
        self.viewPanHeightConstraint.constant = ([TAJUtilities isIPhone] ? 81.5 : 109);

        self.viewPanUpload.hidden = NO;
    }
    else {
        self.viewPancard.hidden = NO;
        if (([[self.kYCDetailsObject.pan_card_status lowercaseString] isEqualToString:@"verified"])) {
            self.imgPANStatus.image = [UIImage imageNamed:@"verified"];
            self.lblPANStatus.textColor = self.greenClr;
            self.viewPanHeightConstraint.constant = 0;
            self.viewPanUpload.hidden = YES;
        }
        
        if(([[self.kYCDetailsObject.pan_card_status lowercaseString] isEqualToString:@"rejected"])) {
            self.imgPANStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblPANStatus.textColor = [UIColor redColor];
            self.viewPanHeightConstraint.constant = ([TAJUtilities isIPhone] ? 81.5 : 109);
            self.viewPanUpload.hidden = NO;
        }
        
        if (([[self.kYCDetailsObject.pan_card_status lowercaseString] isEqualToString:@"pending"])){
            self.imgPANStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblPANStatus.textColor = [UIColor blackColor];
            self.viewPanHeightConstraint.constant = ([TAJUtilities isIPhone] ? 81.5 : 109);
            self.viewPanUpload.hidden = NO;
        }
    }
    
    if ([[self.kYCDetailsObject.aadhar_status lowercaseString] isEqualToString:@"not verified"]) {
        self.viewAadhar.hidden = YES;
        self.lblAadharStatus.textColor = [UIColor blackColor];
        //self.viewAadharHeightConstraint.constant = 185.5;
        self.viewAadharHeightConstraint.constant = ([TAJUtilities isIPhone] ? 185.5 : 253);
        self.viewAadharUpload.hidden = NO;
    }
    else {
        self.viewAadhar.hidden = NO;
        if (([[self.kYCDetailsObject.aadhar_status lowercaseString] isEqualToString:@"verified"])) {
            self.imgAadharStatus.image = [UIImage imageNamed:@"verified"];
            self.lblAadharStatus.textColor = self.greenClr;
            self.viewAadharHeightConstraint.constant = 0;
            self.viewAadharUpload.hidden = YES;
        }
        if ([[self.kYCDetailsObject.aadhar_status lowercaseString] isEqualToString:@"rejected"]) {
            self.imgAadharStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblAadharStatus.textColor = [UIColor redColor];
            self.viewAadharHeightConstraint.constant = ([TAJUtilities isIPhone] ? 185.5 : 253);
            self.viewAadharUpload.hidden = NO;
        }
        if ([[self.kYCDetailsObject.aadhar_status lowercaseString] isEqualToString:@"pending"]) {
            self.imgAadharStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblAadharStatus.textColor = [UIColor blackColor];
            self.viewAadharHeightConstraint.constant = ([TAJUtilities isIPhone] ? 185.5 : 253);
            self.viewAadharUpload.hidden = NO;
        }
    }
    
    if ([[self.kYCDetailsObject.id_proof_status lowercaseString] isEqualToString:@"not verified"]) {
        self.viewAddressProof.hidden = YES;
        self.lblAddressStatus.textColor = [UIColor blackColor];
        //self.viewAddressHeightConstraint.constant = 237.5;
        self.viewAddressHeightConstraint.constant = ([TAJUtilities isIPhone] ? 237.5 : 325);
        self.viewAddressProofUpload.hidden = NO;
    }
    else {
        self.viewAddressProof.hidden = NO;
        if (([[self.kYCDetailsObject.id_proof_status lowercaseString] isEqualToString:@"verified"])) {
            self.imgAddressStatus.image = [UIImage imageNamed:@"verified"];
            self.lblAddressStatus.textColor = self.greenClr;
            self.viewAddressHeightConstraint.constant = 0;
            self.viewAddressProofUpload.hidden = YES;
        }
        if ([[self.kYCDetailsObject.id_proof_status lowercaseString] isEqualToString:@"rejected"]) {
            self.imgAddressStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblAddressStatus.textColor = [UIColor redColor];
            self.viewAddressHeightConstraint.constant = ([TAJUtilities isIPhone] ? 237.5 : 325);
            self.viewAddressProofUpload.hidden = NO;
        }
        if ([[self.kYCDetailsObject.id_proof_status lowercaseString] isEqualToString:@"pending"]) {
            self.imgAddressStatus.image = [UIImage imageNamed:@"Pending"];
            self.lblAddressStatus.textColor = [UIColor blackColor];
            self.viewAddressHeightConstraint.constant = ([TAJUtilities isIPhone] ? 237.5 : 325);
            self.viewAddressProofUpload.hidden = NO;
        }
    }
    
//    if ([[self.kYCDetailsObject.kyc_verified_status lowercaseString] isEqualToString:@"verified"]) {
//        self.viewDocsMainContainer.hidden = YES;
//        self.scrollView.userInteractionEnabled = NO;
//    }
//    else {
//        self.viewDocsMainContainer.hidden = NO;
//        self.scrollView.userInteractionEnabled = YES;
//    }
}

-(void)updateKYC
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    if (([self.txtFielAadhaar.text length] > 0) && (([self.selectedAadhaarImageBaseString length] > 0) || ([self.selectedAadhaarBackImageBaseString length] > 0))  ) {
        
    }
    else {
//        if (([self.txtFielAadhaar.text length] > 0) && (([self.selectedAadhaarImageBaseString length] == 0) || ([self.selectedAadhaarBackImageBaseString length] == 0))  ) {
//            [self.view makeToast:@"Please select aadhar image"];
//        }
//        if (([self.txtFielAadhaar.text length] == 0) && (([self.selectedAadhaarImageBaseString length] > 0) || ([self.selectedAadhaarBackImageBaseString length] > 0))  ) {
//            [self.view makeToast:@"Please enter aadhar card number"];
//        }
//        else {
            self.selectedAadhaarImageBaseString = @"";
            self.selectedAadhaarBackImageBaseString = @"";
       // }
    }
    
    if (([self.txtFieldIDProofNumber.text length] > 0) && (([self.selectedAddressImageBaseString length] > 0) || ([self.selectedAddressBackImageBaseString length] > 0))  ) {
        
    }
    else {
        self.selectedAddressImageBaseString = @"";
        self.selectedAddressBackImageBaseString = @"";
    }
    
    
    NSDictionary *params = @{
                             @"pan_card_no" : self.txtFieldPAN.text,
                             @"pan_card_image" : self.selectedPANImageBaseString,
                             @"aadhar_image1": self.selectedAadhaarImageBaseString,
                             @"aadhar_image2": self.selectedAadhaarBackImageBaseString,
                             @"aadhar_no" : self.txtFielAadhaar.text,
                             @"id_proof_no" : self.txtFieldIDProofNumber.text,
                             @"id_proof_type" : self.selectedProof,
                             @"id_proof_image1" : self.selectedAddressImageBaseString,
                             @"id_proof_image2" : self.selectedAddressBackImageBaseString
                             };
#if DEBUG
    NSLog(@"KYC PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,KYC_DETAILS];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    NSLog(@"RESPONSE: %@", jsondata);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
                        id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                        NSString * deviceType;
                        if ([TAJUtilities isIPhone])
                        {
                            deviceType = @"Mobile";//@"IPHONE";
                        }
                        else
                        {
                            deviceType = @"Tablet";//@"IPAD";
                        }
                        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                        NSDictionary*userData=@{@"userId":userId,@"device_type":deviceType,@"client_type":@"iOS"};
                        [weAnalytics trackEventWithName:@"KYCUploaded" andValue:userData];
                        [self reset];
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        [self getKYCDetails];
                    }
                    else {
                        [self.view hideToastActivity];
                        NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
                [self.view hideToastActivity];
            }
        });
    }];
    
}

- (void)reset {
    self.txtFieldPAN.text = @"";
    self.txtFielAadhaar.text = @"";
    self.txtFieldIDProofNumber.text = @"";
    self.lblPanAttachment.hidden = YES;
    self.lblAadharAttachment.hidden = YES;
    self.lblIDProofAttachment.hidden = YES;
    
    self.selectedPANImageBaseString = @"";
    self.selectedAddressBackImageBaseString = @"";
    self.selectedAddressImageBaseString = @"";
    self.selectedAadhaarImageBaseString = @"";
    self.selectedAadhaarBackImageBaseString = @"";
    
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)PANClicked:(UIButton *)sender {
    [self setDefaults];
    [self hideKeyboard];
    self.isPancard = YES;
    [self showImagePickerOptions];
}

- (IBAction)aadhaarClicked:(UIButton *)sender {
    [self setDefaults];
    [self hideKeyboard];
    BOOL error = [self validateAadhar];
    
    if (([self.txtFielAadhaar.text length] > 0)) {
        if (!error) {
            if (sender.tag == 1) {
                self.isAadhaar = YES;
            }
            else {
                self.isAadhaarBack = YES;
            }
            [self showImagePickerOptions];
        }
    }
    else {
        [self.view makeToast:@"Enter Aadhar number"];
    }
}

- (IBAction)addressClicked:(UIButton *)sender {
    [self setDefaults];
    [self hideKeyboard];
    //[self.txtFieldAddress.text isEqualToString:@""] ? [self.view makeToast:@"Select Proof first"] : [self showImagePickerOptions];
    
    if ([self.txtFieldAddress.text length] > 0) {
        BOOL error = [self validateAddress];
        if (!error) {
            if (sender.tag == 3) {
                self.isAddress = YES;
            }
            else {
                self.isAddressBack = YES;
            }
            [self showImagePickerOptions];
        }
    }
    else {
        [self.view makeToast:@"Select Proof first"];
    }
}

- (IBAction)hidePickerView:(id)sender
{
    self.viewPickerContainer.hidden = YES;
}

- (IBAction)optionsClicked:(UIButton *)sender
{
    self.viewPickerContainer.hidden = NO;
    [self.pickerView reloadAllComponents];
}

- (IBAction)donePickerView:(id)sender
{
    NSUInteger num = [[self.pickerView dataSource] numberOfComponentsInPickerView:self.pickerView];
    
    for (int i = 0; i < num; i++) {
        NSUInteger selectRow = [self.pickerView selectedRowInComponent:i];
        self.selectedProof = [self.arrayAddressProof valueForKey:@"proof_id"][selectRow];
        self.txtFieldAddress.text = [self.arrayAddressProof valueForKey:@"proof"][selectRow];
        
    }
    
    self.viewPickerContainer.hidden = YES;
}

- (IBAction)submitClicked:(UIButton *)sender {
    [self hideKeyboard];
    BOOL error = [self validateInput];
    if (!error) {
        if( ([self validatePanCardNumber:[self.txtFieldPAN.text uppercaseString]] && [self.selectedPANImageBaseString length] > 0) ||
           (([self.txtFielAadhaar.text length] > 0) && ([self.selectedAadhaarImageBaseString length] > 0) ) ||
           (([self.txtFieldIDProofNumber.text length] > 0) && ([self.selectedAddressImageBaseString length] > 0) ) ) {
            [self updateKYC];
        }
        else {
            [self.view makeToast:@"Please attach any one image and enter the related details to update"];
        }
    }
}

#pragma mark - ImagePicker Delegate

- (void)showImagePickerOptions
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Select Image" message:@"Select source to get image" preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
            self.pickerController = [[UIImagePickerController alloc] init];
            self.pickerController.delegate = self;
            //self.isFromCamera = YES;
            self.pickerController.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:self.pickerController animated:YES completion:nil];
        }
        else {
            UIAlertController *alertController = [UIAlertController alertControllerWithTitle:@"Camera not available" message:nil preferredStyle:UIAlertControllerStyleAlert];
            
            [alertController addAction:[UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
                
            }]];
            
            [self presentViewController:alertController animated:YES completion:nil];
        }
    }]];
    [alertController addAction:[UIAlertAction actionWithTitle:@"Photo Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
        if ([TAJUtilities isIPhone]) {
            self.pickerController = [[UIImagePickerController alloc] init];
            self.pickerController.delegate = self;
            //self.isFromCamera = NO;
            self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
            [self presentViewController:self.pickerController animated:YES completion:nil];
        }
        else {
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        //self.isFromCamera = NO;
        CGRect sourceRect = CGRectMake(0, 0, self.view.frame.size.width - 20, self.view.frame.size.height);

        self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        self.pickerController.modalPresentationStyle = UIModalPresentationPopover;

        [self presentViewController:self.pickerController animated:YES completion:nil];
        self.pickerController.popoverPresentationController.sourceRect = sourceRect;
        self.pickerController.popoverPresentationController.sourceView = self.view;
        }

        /*
        self.pickerController = [[UIImagePickerController alloc] init];
        self.pickerController.delegate = self;
        //self.isFromCamera = NO;
        self.pickerController.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:self.pickerController animated:YES completion:nil];
        */
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
        
    }]];
    [self presentViewController:alertController animated:YES completion:nil];
         
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info
{
    UIImage *selectedImage = info[UIImagePickerControllerOriginalImage];
    NSData *data = UIImageJPEGRepresentation(selectedImage, 0.2);
    
    if (self.isPancard) {
        self.selectedPANImageBaseString = [data base64EncodedString];
        self.lblPanAttachment.hidden = YES;
        if (self.selectedPANImageBaseString.length > 0) {
            self.lblPanAttachment.hidden = NO;
        }
    }
    else if (self.isAadhaar) {
        self.selectedAadhaarImageBaseString = [data base64EncodedString];
    }
    else if (self.isAadhaarBack) {
        self.selectedAadhaarBackImageBaseString = [data base64EncodedString];
    }
    else if (self.isAddress) {
        self.selectedAddressImageBaseString = [data base64EncodedString];
    }
    else if (self.isAddressBack) {
        self.selectedAddressBackImageBaseString = [data base64EncodedString];
    }
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
    
    self.lblAadharAttachment.hidden = YES;
    if (self.selectedAadhaarImageBaseString.length > 0) {
        self.lblAadharAttachment.hidden = NO;
        self.lblAadharAttachment.text = @"1 File attached";
    }
    if (self.selectedAadhaarBackImageBaseString.length > 0) {
        self.lblAadharAttachment.hidden = NO;
        self.lblAadharAttachment.text = @"1 File attached";
    }
    if (self.selectedAadhaarImageBaseString.length > 0 && self.selectedAadhaarBackImageBaseString.length > 0) {
        self.lblAadharAttachment.hidden = NO;
        self.lblAadharAttachment.text = @"2 Files attached";
    }
    
    self.lblIDProofAttachment.hidden = YES;
    if (self.selectedAddressImageBaseString.length > 0) {
        self.lblIDProofAttachment.hidden = NO;
        self.lblIDProofAttachment.text = @"1 File attached";
    }
    if (self.selectedAddressBackImageBaseString.length > 0) {
        self.lblIDProofAttachment.hidden = NO;
        self.lblIDProofAttachment.text = @"1 File attached";
    }
    if (self.selectedAddressImageBaseString.length > 0 && self.selectedAddressBackImageBaseString.length > 0) {
        self.lblIDProofAttachment.hidden = NO;
        self.lblIDProofAttachment.text = @"2 Files attached";
    }
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [self.pickerController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - PickerView Methods

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)thePickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)thePickerView numberOfRowsInComponent:(NSInteger)component {
    return self.arrayAddressProof.count;
}

- (NSString *)pickerView:(UIPickerView *)thePickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return [NSString stringWithFormat:@"%@",[[self.arrayAddressProof valueForKey:@"proof"] objectAtIndex:row]];
}


#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    NSInteger nextTag = textField.tag + 1;
    // Try to find next responder
    UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
    if (nextResponder) {
        // Found next responder, so set it.
        [nextResponder becomeFirstResponder];
    } else {
        // Not found, so remove keyboard.
        [self hideKeyboard];
    }
    
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Name
//    if (textField == self.txtFieldPAN)
//    {
//        if ([string isEqualToString:@" "]) {
//            return NO;
//        }
//    }
//    if (textField == self.txtFieldPAN) {
//        return !([textField.text length] > 9 && [string length] > range.length);
//    }
    
    // address
     if (textField == self.txtFieldAddress) {
        return !([textField.text length] > 20 && [string length] > range.length);
    }
    
    // Aadhar Number
    if (textField == self.txtFielAadhaar) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        // Limit pincode number input to 6 digits
        return ([string isEqualToString:filteredString] ? newLength <= 12 : NO);
    }
    
    if (textField == self.txtFieldPAN) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        if ([string isEqualToString:@" "]) {
            return NO;
        }
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PANCARD_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        // Limit pincode number input to 6 digits
        return ([string isEqualToString:filteredString] ? newLength <= 10 : NO);
    }
    
    return YES;
}
@end
