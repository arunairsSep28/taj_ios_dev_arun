//
//  UpdateEmailViewController.m
//  Taj Rummy
//
//  Created by svc on 12/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "UpdateEmailViewController.h"

@interface UpdateEmailViewController ()<UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldEmail;

@property (strong, nonatomic) NSPredicate *emailPredicate;

@end

@implementation UpdateEmailViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexEmail];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.view addGestureRecognizer:tapGesture];
    
    [self configureView];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.viewContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldEmail.text length] == 0) {
        [self.view makeToast:@"Email can not be empty"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.txtFieldEmail.text]) && ([self.txtFieldEmail.text length] > 0)) {
        [self.view makeToast:@"Enter Valid Email"];
        return YES;
    }
    return NO;
}


-(void) getAccountOverviewDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,ACCOUNT_OVERVIEW];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            NSLog(@"AccountOverviewDetails Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {

            }
            
            NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"AccountOverviewDetails ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

- (IBAction)updateClicked:(UIButton *)sender {
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        
        NSDictionary *params = @{@"email": self.txtFieldEmail.text
                                 };
#if DEBUG
        NSLog(@"LOGIN PARAMS: %@", params);
#endif
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
        
        NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_EMAIL];
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            
#if DEBUG
                            NSLog(@"RESPONSE: %@", jsondata);
#endif
                            id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                            NSDictionary *userData=@{
                                                     @"email":[jsondata valueForKey:@"email"],
                                                     @"userId":userId, @"EmailVerificationStatus" : [NSNumber numberWithBool:0]
                                                     };
                            [weAnalytics trackEventWithName:@"EmailUpdate" andValue:userData];
                            
                            self.txtFieldEmail.text = @"";
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                        else {
                            NSLog(@"REST Error :%@",[jsondata valueForKey:@"message"]);
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view makeToast:@"Error while fetching data."];
                }
            });
        }];
        
    }
}

#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Email
    if (textField == self.txtFieldEmail) {
        return !([textField.text length] > EmailAddressCharacterLimit && [string length] > range.length);
    }
    return YES;
}

@end
