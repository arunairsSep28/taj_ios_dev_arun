//
//  AccountOverviewViewController.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "AccountOverviewViewController.h"
#import "BankDetailsTableViewCell.h"

@interface AccountOverviewViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UIView *viewCashSummay;
@property (weak, nonatomic) IBOutlet UIView *viewBonusSummary;
@property (weak, nonatomic) IBOutlet UIView *viewPracticeAccount;
@property (weak, nonatomic) IBOutlet UIView *viewLoyaltyPoints;

@property (weak, nonatomic) IBOutlet UILabel *lblDepositeBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblWithdrawableBalance;
@property (weak, nonatomic) IBOutlet UILabel *lblReleasedBonus;

@property (weak, nonatomic) IBOutlet UILabel *lblTotalCash;
@property (weak, nonatomic) IBOutlet UILabel *lblPendingChunks;
@property (weak, nonatomic) IBOutlet UILabel *lblFunChips;
@property (weak, nonatomic) IBOutlet UILabel *lblLoyaltyPoints;
@property (weak, nonatomic) IBOutlet UILabel *lblClub;
@property (weak, nonatomic) IBOutlet UILabel *lblBankDetails;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstaint;
@property (strong, nonatomic) AccountOverview *accountOverviewObject;
@property (strong, nonatomic) NSMutableArray *arrayBankDetails;


@end

@implementation AccountOverviewViewController
static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeight = 276;
static const CGFloat kCellHeightiPad = 398;

- (void)viewDidLoad {
    [super viewDidLoad];

    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Account OverView Screen"];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"BankDetailsTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
    [self configureView];
    [self getAccountOverviewDetails];
    //[self get];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

-(void)configureView
{
    self.scrollView.hidden = YES;
    self.viewCashSummay.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewBonusSummary.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewPracticeAccount.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewLoyaltyPoints.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.tableViewHeightConstaint.constant = 2 *kCellHeight;
}

#pragma mark - HELPERS

-(void) getAccountOverviewDetails {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,ACCOUNT_OVERVIEW];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    //NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            self.scrollView.hidden = NO;
#if DEBUG
        NSLog(@"AccountOverviewDetails Response :%@",responseData);
#endif
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                self.arrayBankDetails = [[NSMutableArray alloc] init];
                NSDictionary * jsonDictionay = [responseData valueForKey:@"data"];
                NSDictionary *dataDic = [jsonDictionay dictionaryRemovingNSNullValues];
                NSLog(@"%@", dataDic);
                self.accountOverviewObject = [AccountOverview sharedInstance];
                [self.accountOverviewObject assignData:dataDic];
                
                self.arrayBankDetails = self.accountOverviewObject.array_bank_details;
                self.tableViewHeightConstaint.constant = 0;
                if (self.arrayBankDetails.count > 0){
                    if ([TAJUtilities isIPhone]) {
                    self.tableViewHeightConstaint.constant = self.arrayBankDetails.count * kCellHeight;
                    }
                    else {
                        self.tableViewHeightConstaint.constant = self.arrayBankDetails.count * kCellHeightiPad;
                    }
                }
                [self.tableView reloadData];
                [self loadData];
            }
            
            //NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
        });
        
    } errorBlock:^(NSString *errorString) {
        //NSLog(@"AccountOverviewDetails ERROR : %@",errorString);
        [self.view hideToastActivity];
        self.scrollView.hidden = YES;
    }];
}

-(void)loadData {
    self.lblDepositeBalance.text = self.accountOverviewObject.deposit_balance;
    self.lblWithdrawableBalance.text = self.accountOverviewObject.withdrawal_balance;
    self.lblTotalCash.text = self.accountOverviewObject.total_balance;
    self.lblPendingChunks.text = [NSString stringWithFormat:@"%ld",(long)self.accountOverviewObject.pending_bonus];
    self.lblReleasedBonus.text = [NSString stringWithFormat:@"%ld",(long)self.accountOverviewObject.released_bonus];
    
    self.lblFunChips.text = self.accountOverviewObject.funchips;
    self.lblLoyaltyPoints.text = self.accountOverviewObject.loyaltypoints;
    self.lblClub.text = self.accountOverviewObject.club;
    self.lblBankDetails.hidden = YES;
    if (self.arrayBankDetails.count > 0){
        self.lblBankDetails.hidden = NO;
    }
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
}

- (IBAction)infoClicked:(UIButton *)sender {
    WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"Withdrawble balance working" url:
                                          [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,OVERVIEW_INFO]];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayBankDetails.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
    return kCellHeight;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    BankDetailsTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithData:self.arrayBankDetails[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


@end
