//
//  MyAccountViewController.m
//  Taj Rummy
//
//  Created by svc on 06/02/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "MyAccountViewController.h"

@interface MyAccountViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *arrayMenu;

@end

@implementation MyAccountViewController

static NSString *const kCellIdentifier = @"cellIdentifier";

static const CGFloat kCellHeightTitle = 54;
static const CGFloat kCellHeightiPad = 84;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.arrayMenu = @[@"Profile Overview",@"Account Overview",@"Your Preferences",@"Change Password",@"Update E-mail ID",@"Update Mobile Number",@"Update KYC Documents"];
    
    // Register tableViewCell
    [self.tableView registerNib:[UINib nibWithNibName:@"MoreTableViewCell" bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

#pragma mark - HELPERS

-(void)checkEmail {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_EMAIL];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    //NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            
            //NSLog(@"CHECK EMAIL Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                UpdateEmailViewController * viewController = [[UpdateEmailViewController alloc] init];
                [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
                [self presentViewController:viewController animated:NO completion:nil];
            }
            
            else {
                [self.view makeToast:[responseData valueForKey:@"message"]];
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"CHECKE MAIL ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}


-(void)checkMobile {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_MOBILE];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
        
            //NSLog(@"CHECK MOBILE Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                UpdateMobileViewController * viewController = [[UpdateMobileViewController alloc] initWithMobileNumber:@"" isFromEdt:NO];
                [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
                [self presentViewController:viewController animated:NO completion:nil];
                
            }
            
            else {
                [self.view makeToast:[responseData valueForKey:@"message"]];
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"CHECK MOBILE ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    //[self.navigationController popViewControllerAnimated:YES];
    [self dismissViewControllerAnimated:NO completion:nil];
    
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.arrayMenu.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone]) {
        return kCellHeightTitle;
    }
    else {
        return kCellHeightiPad;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MoreTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell updateCellWithTitle:self.arrayMenu[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        ProfileViewController * viewController = [[ProfileViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 1) {
        AccountOverviewViewController * viewController = [[AccountOverviewViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 2) {
        PreferencesViewController * viewController = [[PreferencesViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else if (indexPath.row == 3) {
        ChangePasswordViewController * viewController = [[ChangePasswordViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
//    else if (indexPath.row == 4) {
//        ChangeUsernameViewController * viewController = [[ChangeUsernameViewController alloc] init];
//        [self presentViewController:viewController animated:NO completion:nil];
//    }
    else if (indexPath.row == 4) {
        [self checkEmail];
    }
    else if (indexPath.row == 5) {
        [self checkMobile];
    }
    else if (indexPath.row == 6) {
        KYCViewController * viewController = [[KYCViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
    else {
        ReferFriendViewController * viewController = [[ReferFriendViewController alloc] init];
        [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:viewController animated:NO completion:nil];
    }
}

@end
