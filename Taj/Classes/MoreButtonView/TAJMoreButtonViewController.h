/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMoreButtonViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Pradeep BM on 12/05/14.
 Created by  Yogisha Poojary on 12/05/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJConstants.h"
@class TAJHomePageViewController;


@protocol TAJMoreButtonViewControllerDelegate;

@interface TAJMoreButtonViewController : UIViewController

@property (nonatomic, weak) id<TAJMoreButtonViewControllerDelegate> moreButtonDelegate;
- (BOOL)isMoreButtonViewVisible;

@end

@protocol TAJMoreButtonViewControllerDelegate <NSObject>

- (void)removeMoreButtonControllerView;
- (void)didPressedLogOutPressed;

@end
