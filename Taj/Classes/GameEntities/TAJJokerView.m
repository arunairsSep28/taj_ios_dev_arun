/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJJokerView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 22/03/14.
 **/

#import "TAJJokerView.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJGameEngine.h"
#import "TAJUserModel.h"
#define MAXLENGTH 6
//added by ratheesh
#define ENTER_MAX_BUYIN_AMOUNT @"Please enter maximum amount only."
//added by ratheesh
#define ENTER_MINIMUM_AMOUNT @"Please enter minimum amount"
#define ENTER_BUYIN_AMOUNT   @"Please enter buyin amount"

@interface TAJJokerView ()<UITextFieldDelegate>

@property (nonatomic, strong)  UIImage  *minImageName;
@property (nonatomic, strong)  UIImage  *maxImageName;
@property (nonatomic, strong)  UIImage  *thumbImageName;

@property int remainingBet;

@property (nonatomic, weak) IBOutlet UILabel *betLabel;
@property (nonatomic, weak) IBOutlet UILabel *minBuyInLabel;
@property (nonatomic, weak) IBOutlet UILabel *maxBuyInLabel;
@property (nonatomic, weak) IBOutlet UILabel *balanceLabel;
@property (nonatomic, weak) IBOutlet UITextField *buyInTextField;
@property (nonatomic, weak) IBOutlet UIButton *joinButton;
@property (nonatomic, weak) IBOutlet UIButton *cancelButton;
@property (strong, nonatomic) IBOutlet UISlider *customSlider;
@property BOOL isSliderFullyDraggedAtTheEnd;
@property (nonatomic) NSInteger maxBuyInAmount;
@property (nonatomic) NSInteger minBuyInAmount;

@property (nonatomic, strong) TAJInfoPopupViewController   *noHandAlertView;


- (IBAction)joinButtonAction:(UIButton *)sender;
- (IBAction)cancelButtonAction:(UIButton *)sender;
- (IBAction)BuyInValueChangeSlider:(id)sender;

@end

@implementation TAJJokerView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.customSlider.minimumValue = 0;
        self.customSlider.maximumValue = 0.9;
        self.remainingBet = 0;
    }
    return self;
}

-(BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    NSUInteger newLength = [textField.text length] + [string length] - range.length;
    NSCharacterSet *cs = [[NSCharacterSet characterSetWithCharactersInString:@"1234567890"] invertedSet];
    NSString *filtered = [[string componentsSeparatedByCharactersInSet:cs] componentsJoinedByString:@""];
    return (([string isEqualToString:filtered])&&(newLength <=  MAXLENGTH));
    
}

- (void)updateJokerView:(NSMutableDictionary *)dictionary  withBet:(int)previousBetValue gameType:(NSString *)gameType
{
    self.minImageName = [[UIImage imageNamed:@"buying_slider_tr.png"] resizableImageWithCapInsets:UIEdgeInsetsZero];
    self.maxImageName = [UIImage imageNamed:@"buying_slider_tr.png"];
    self.thumbImageName = [UIImage imageNamed:@"buying_pointer_tr.png"];
    
    [self.customSlider setMinimumTrackImage:self.minImageName forState:UIControlStateNormal];
    [self.customSlider setMaximumTrackImage:self.maxImageName forState:UIControlStateNormal];
    
    [self.customSlider setThumbImage:self.thumbImageName forState:UIControlStateNormal];
    
    [self.customSlider setThumbImage:self.thumbImageName forState:UIControlStateHighlighted];
    
    self.betLabel.text = dictionary[TAJ_BET];
    self.minBuyInLabel.text = dictionary[TAJ_MIN_BUYIN];
    self.maxBuyInLabel.text = dictionary[TAJ_MAX_BUYIN];
    
    DLog(@"Current Bet %@",self.betLabel.text);
    
    if ([gameType isEqualToString:CASH_GAME_LOBBY]) {
        {
            if (previousBetValue!=0)
            {
                self.remainingBet= [self.maxBuyInLabel.text intValue]-previousBetValue;
            }
            else
            {
                self.remainingBet= [self.maxBuyInLabel.text intValue];
            }
            
            int balance = [[[[TAJGameEngine sharedGameEngine] usermodel]realChips] intValue];
            
            self.balanceLabel.text = [NSString stringWithFormat:@"%d",balance];
            NSLog(@"REMAINING : %d",self.remainingBet);
            NSLog(@"BALANCE : %@",self.balanceLabel.text);
            
            DLog(@"%@ %@ %d",self.balanceLabel.text, self.buyInTextField.text,self.remainingBet);
            if ([self.balanceLabel.text intValue] >= self.remainingBet)
            {
                self.maxBuyInAmount = self.remainingBet;
                self.minBuyInAmount = [self.minBuyInLabel.text integerValue];
                self.buyInTextField.text = [NSString stringWithFormat:@"%d",self.remainingBet];
                self.customSlider.value = self.maxBuyInAmount;
                
                NSLog(@"minBuyInAmount 1 : %ld",(long)self.minBuyInAmount);
                NSLog(@"maxBuyInAmount 1 : %ld",(long)self.maxBuyInAmount);
                
            }
            else if ([self.balanceLabel.text intValue] <= self.remainingBet)
            {
                self.maxBuyInAmount = [self.balanceLabel.text intValue];
                self.minBuyInAmount = 0;
                self.buyInTextField.text = [NSString stringWithFormat:@"%@",self.balanceLabel.text];
                self.customSlider.value = [self.balanceLabel.text intValue];

                NSLog(@"minBuyInAmount 1.0 : %ld",(long)self.minBuyInAmount);
                NSLog(@"maxBuyInAmount 1.0 : %ld",(long)self.maxBuyInAmount);
                NSLog(@"customSlider value 1.0 : %f",self.customSlider.value);
            }
            
            else
            {
                self.maxBuyInAmount = [self.balanceLabel.text integerValue];
                self.minBuyInAmount = [self.minBuyInLabel.text integerValue];
                self.buyInTextField.text = [NSString stringWithFormat:@"%ld",[self.balanceLabel.text integerValue]];
                self.customSlider.value = self.maxBuyInAmount;
                
                NSLog(@"minBuyInAmount 2 : %ld",(long)self.minBuyInAmount);
                NSLog(@"maxBuyInAmount 2 : %ld",(long)self.maxBuyInAmount);
                
            }
            
            DLog(@"%@ %@ %d",self.balanceLabel.text, self.buyInTextField.text,self.remainingBet);
            self.balanceLabel.text = [NSString stringWithFormat:@"%d",balance];
            
            if ([self.balanceLabel.text floatValue]< [self.maxBuyInLabel.text floatValue])
            {
                self.remainingBet = self.maxBuyInAmount;
                self.customSlider.value = [self.balanceLabel.text intValue];
                NSLog(@"remainingBet 3 : %ld",(long)self.remainingBet);
                NSLog(@"customSlider 3 : %@",self.balanceLabel.text);
                //self.minBuyInAmount = 0;
            }
            self.isSliderFullyDraggedAtTheEnd = YES;
        }
    }
    else {
        if (previousBetValue!=0)
        {
            self.remainingBet= [self.maxBuyInLabel.text intValue]-previousBetValue;
        }
        else
        {
            self.remainingBet= [self.maxBuyInLabel.text intValue];
        }
        
        self.balanceLabel.text = [[[TAJGameEngine sharedGameEngine] usermodel]funchips];
        
        DLog(@"%@ %@ %d",self.balanceLabel.text, self.buyInTextField.text,self.remainingBet);
        if ([self.balanceLabel.text intValue] >= self.remainingBet)
        {
            self.maxBuyInAmount = self.remainingBet;
            self.minBuyInAmount = [self.minBuyInLabel.text integerValue];
            self.buyInTextField.text = [NSString stringWithFormat:@"%d",self.remainingBet];
            self.customSlider.value = self.maxBuyInAmount;
        }
        else
        {
            self.maxBuyInAmount = [self.balanceLabel.text integerValue];
            self.minBuyInAmount = [self.minBuyInLabel.text integerValue];
            self.buyInTextField.text = [NSString stringWithFormat:@"%ld",[self.balanceLabel.text integerValue]];
            self.customSlider.value = self.maxBuyInAmount;
        }
        
        DLog(@"%@ %@ %d",self.balanceLabel.text, self.buyInTextField.text,self.remainingBet);
        self.balanceLabel.text = [NSString stringWithFormat:@"%0.0f",roundf([[[[TAJGameEngine sharedGameEngine] usermodel]funchips] floatValue])];
        
        if ([self.balanceLabel.text floatValue]< [self.maxBuyInLabel.text floatValue])
        {
            self.remainingBet = self.maxBuyInAmount;
            self.customSlider.value = [self.balanceLabel.text intValue];
        }
        self.isSliderFullyDraggedAtTheEnd = YES;
    }
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (textField == self.buyInTextField)
    {
        [self processJokerOperation];
    }
    return YES;
}

- (IBAction)joinButtonAction:(UIButton *)sender
{
    [self processJokerOperation];
}

- (void)processJokerOperation
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
//        if ([APP_DELEGATE.gameType isEqualToString:CASH_GAME_LOBBY]) {
//            if (!([self.buyInTextField.text length] == 0)) {
//                [self didSelectJoin:self.buyInTextField.text];
//            }
//            else if([self.buyInTextField.text length] == 0)
//            {
//                [self.jokerDelegate showErrorAlert:ENTER_BUYIN_AMOUNT];
//                [self.buyInTextField resignFirstResponder];
//            }
//        }
//        else {
            if (!([self.buyInTextField.text length] == 0))
            {
                //added by ratheesh
                NSString * maxAmount = [NSString stringWithFormat:@"Please enter (%@) maximum amount only.",self.maxBuyInLabel.text];
                if ([self.buyInTextField.text integerValue] > [self.maxBuyInLabel.text integerValue]) {
                    [self.jokerDelegate showErrorAlert:maxAmount];
                    //ENTER_MAX_BUYIN_AMOUNT
                }
                //added by ratheesh
                
                else if ([self.buyInTextField.text integerValue] >= [self.minBuyInLabel.text integerValue])
                {
                    [self didSelectJoin:self.buyInTextField.text];
                }
                
                else
                {
                    
                    NSString * minAmount = [NSString stringWithFormat:@"Please enter (%@) minimum amount",self.minBuyInLabel.text];
                    [self.jokerDelegate showErrorAlert:minAmount];
                    //ENTER_MINIMUM_AMOUNT
                }
                
                [self.buyInTextField resignFirstResponder];
            }
            else if([self.buyInTextField.text length] == 0)
            {
                [self.jokerDelegate showErrorAlert:ENTER_BUYIN_AMOUNT];
                [self.buyInTextField resignFirstResponder];
            }
       // }
    }
}

- (void)didSelectJoin:(NSString *)buyInAmount
{
    if ([self.jokerDelegate respondsToSelector:@selector(didSelectJoinJoker:)])
    {
        [self.jokerDelegate didSelectJoinJoker:buyInAmount];
    }
}

- (IBAction)cancelButtonAction:(UIButton *)sender
{
    [self didSelectCancel];
}

- (IBAction)BuyInValueChangeSlider:(UISlider *)sender
{
    //NSLog(@"CHANGE VALUE :%f",sender.value);
        
    if ([APP_DELEGATE.gameType isEqualToString:CASH_GAME_LOBBY]) {
        
        int balance = [[[[TAJGameEngine sharedGameEngine] usermodel]realChips] intValue];
        int minimumBuyIn = [self.minBuyInLabel.text intValue];
        
        if(sender.value == 0)
        {
            if (minimumBuyIn <= balance) {
                self.buyInTextField.text = [NSString stringWithFormat:@"%d",minimumBuyIn];
            }
            else {
                self.buyInTextField.text = [NSString stringWithFormat:@"%d",0];
            }
            self.isSliderFullyDraggedAtTheEnd = NO;
        }
        else if(sender.value == 1)
        {
            NSLog(@"SENDER 1");
            
            if ([self.maxBuyInLabel.text intValue] <= balance) {
                self.buyInTextField.text = [NSString stringWithFormat:@"%@",self.maxBuyInLabel.text];
            }
            else {
                self.buyInTextField.text = [NSString stringWithFormat:@"%@",self.balanceLabel.text];
            }
            self.isSliderFullyDraggedAtTheEnd = YES;
        }
        
        else if(sender.value > 0 && !self.isSliderFullyDraggedAtTheEnd)
        {
            NSLog(@"SENDER > 0");
            NSInteger finalValue = self.minBuyInAmount+ (self.maxBuyInAmount-self.minBuyInAmount) * sender.value;
            self.buyInTextField.text = [NSString stringWithFormat:@"%ld",(long)finalValue];
        }
        else
        {
            NSLog(@"SENDER ELSE");
            NSInteger finalValue =self.minBuyInAmount+ (self.maxBuyInAmount-self.minBuyInAmount) * sender.value;
            self.buyInTextField.text = [NSString stringWithFormat:@"%ld",(long)finalValue];
        }
    }
    else {
        int balance = [[[[TAJGameEngine sharedGameEngine] usermodel]realChips] intValue];
        if(sender.value == 0)
        {
            NSLog(@"VALUE 0");
            self.buyInTextField.text = self.minBuyInLabel.text;
            self.isSliderFullyDraggedAtTheEnd = NO;
        }
        else if(sender.value == 1)
        {
            NSLog(@"VALUE 1");
            
            self.buyInTextField.text = [NSString stringWithFormat:@"%d",self.remainingBet];
            
//            if ([self.maxBuyInLabel.text intValue] <= balance) {
//                self.isSliderFullyDraggedAtTheEnd = NO;
//                NSLog(@"VALUE 1 NO");
//            }
//            else {
//                self.isSliderFullyDraggedAtTheEnd = YES;
//                NSLog(@"VALUE 1 YES");
//            }
            
            
        }
        
        else if(sender.value > 0 && !self.isSliderFullyDraggedAtTheEnd)
        {
            NSLog(@"VALUE >");
            NSInteger finalValue =self.minBuyInAmount+ (self.maxBuyInAmount-self.minBuyInAmount) * sender.value;
            self.buyInTextField.text = [NSString stringWithFormat:@"%ld",(long)finalValue];
        }
        else
        {
            NSLog(@"VALUE ELSE");
            NSInteger finalValue =self.minBuyInAmount+ (self.maxBuyInAmount-self.minBuyInAmount) * sender.value;
            self.buyInTextField.text = [NSString stringWithFormat:@"%ld",(long)finalValue];
            
        }
    }
    
}

//- (void)okButtonPressedForInfoPopup:(id)object
//{
//    TAJInfoPopupViewController *conroller = object;
//
//    switch (conroller.tag)
//    {
//
//        case INSUUFICIENT_CHIPS_TAG:
//        {
//            [self.noHandAlertView hide];
//            self.noHandAlertView = nil;
//        }
//            break;
//            //added ratheesh
//
//
//            break;
//        default:
//            break;
//    }
//}


- (void)didSelectCancel
{
    if ([self.jokerDelegate respondsToSelector:@selector(didSelectCancelJoker)])
    {
        [self.jokerDelegate didSelectCancelJoker];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.buyInTextField resignFirstResponder];
   
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if ([textField.text length] > 0)
    {
        textField.layer.shadowColor = [[UIColor clearColor] CGColor];
    }
    if (self.jokerDelegate && [self.jokerDelegate respondsToSelector:@selector(didJokerViewTextFieldStartEditing)])
    {
        [self.jokerDelegate didJokerViewTextFieldStartEditing];
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    if (self.jokerDelegate && [self.jokerDelegate respondsToSelector:@selector(didJokerViewTextFieldEndEditing)])
    {
        [self.jokerDelegate didJokerViewTextFieldEndEditing];
    }
}


//PopupType will be passed
- (void)yesButtonPressedForInfoPopup:(id)object
{
    
}

- (void)noButtonPressedForInfoPopup:(id)object
{
    
}

- (void)okButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

- (void)closeButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

- (void)chooseInfopopForDifferentDevices
{
    self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:nil tag:1 popupButtonType:eOK_Type message:INVALID_EMAIL_MESSAGE];
    [self.infoPopupViewController show];
}

@end
