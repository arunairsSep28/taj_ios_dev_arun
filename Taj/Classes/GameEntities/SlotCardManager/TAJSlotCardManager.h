/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSlotCardManager.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/05/14.
 **/

#import <Foundation/Foundation.h>

@protocol TAJSlotCardManagerDelegate;

@interface TAJSlotCardManager : NSObject

@property (weak, nonatomic) id<TAJSlotCardManagerDelegate> slotDelegate;

+ (id)sharedCardSlot;
- (void)initializeSlotArray:(NSMutableArray *)array;
- (void)swapCard:(NSUInteger)cardAindex withCard:(NSUInteger)cardBindex;
- (void)exchangeSlotCardsWhileGroup:(NSMutableArray *)indexArray;
- (void)insertPickedCardToSlotArray:(NSMutableDictionary *)dict;
- (void)discardedCardAtSlotIndex:(NSUInteger)discardedCardIndex;
- (void)discardAutoPlayCardForPlayerPicked:(NSString *)inCardNo withSuit:(NSString *)inSuit;
- (void)updateSlotArrayForAutoDiscard:(NSString *)inCardNo withSuit:(NSString *)inSuit;
- (NSMutableArray *)getSlotCardsArray;
- (void)removeAllSlotCards;
- (NSUInteger)totalSlotCards;
- (NSUInteger)totalTableCards;
- (void)resetCardSlotsInSlotManager;

@end

@protocol TAJSlotCardManagerDelegate <NSObject>

- (void)slotCardArray:(NSMutableArray *)slotArray withTableCardsArray:(NSMutableArray *)tableArray;

@end