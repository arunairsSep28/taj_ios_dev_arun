/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSlotCardManager.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/05/14.
 **/

#import "TAJSlotCardManager.h"
#import "NSMutableArray+TAJNSMutableArray.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"

#define INSERT_NULL_OBJECT_IN_RANGE     17
#define INSERT_NULL_OBJECT_IN_RANGE_Iphone5     19


@interface TAJSlotCardManager ()

@property (strong, nonatomic)   NSMutableArray  *slotArray;
@property (strong, nonatomic)   NSMutableArray  *tableCardArray;

@end

@implementation TAJSlotCardManager
static TAJSlotCardManager *cardSlot = nil;

+ (id)sharedCardSlot
{
    if (cardSlot == nil)
    {
        cardSlot = [[TAJSlotCardManager alloc] init];
        
    }
    return cardSlot;
}
- (id)init
{
    self = [super init];
    if (self)
    {
        self.slotArray = [NSMutableArray array];
        self.tableCardArray = [NSMutableArray array];
    }
    return self;
}

- (void)initializeSlotArray:(NSMutableArray *)array
{
    self.slotArray = [NSMutableArray arrayWithArray:array];
    [self removeNulSlotsFromSlotArray];
    [self resetTableCardsArray:self.slotArray];
}

- (void)resetTableCardsArray:(NSMutableArray *)array
{
    self.tableCardArray = [NSMutableArray array];
    
    for (NSUInteger i = 0; i < array.count; i++)
    {
        if ([array[i] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *cardInfo = array[i];
            // assign key to know the card index-position
            [cardInfo setObject:[cardInfo[kTaj_Card_Suit_Key] lowercaseString] forKey:kTaj_Card_Suit_Key];
            [cardInfo setObject:[NSNumber numberWithUnsignedInteger:i] forKey:TAJ_SLOT_INDEX];
            [self.tableCardArray addObject:cardInfo];
        }
    }
    
    [self informControllerForSlotChange];
}

- (NSMutableDictionary *)cardAtTableIndex:(NSUInteger)index
{
    NSMutableDictionary *cardInfo = nil;
    
    if ([self.tableCardArray[index] isKindOfClass:[NSDictionary class]])
    {
        cardInfo = self.tableCardArray[index];
    }
    return cardInfo;
}

- (BOOL)isCardExistAtSlotIndex:(NSUInteger)index
{
    if ([self.slotArray[index] isKindOfClass:[NSDictionary class]])
    {
        return YES;
    }
    return NO;
}

- (NSMutableDictionary *)cardAtSlotIndex:(NSUInteger)index
{
    NSMutableDictionary *cardInfo = nil;
    if ([self isCardExistAtSlotIndex:index])
    {
        cardInfo = self.slotArray[index];
    }
    return cardInfo;
}

- (NSUInteger)totalSlotCards
{
    return [self.slotArray count];
}

- (NSUInteger)totalTableCards
{
    return [self.tableCardArray count];
}

- (void)swapCard:(NSUInteger)cardAindex withCard:(NSUInteger)cardBindex
{
    [self.slotArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];

}

- (void)resetCardSlotsInSlotManager
{
    [self resetTableCardsArray:self.slotArray];
}

- (void)exchangeSlotCardsWhileGroup:(NSMutableArray *)indexArray
{
    NSMutableArray *indexSetArray = [self getArrayToRemoveCardSlotAtIndexesArray:indexArray];
    
    [self.slotArray insertObjects:indexSetArray atIndex:0];
    
    [self placeNullObjectForGroupedSlotAtIndex:indexArray];
    
    [self resetTableCardsArray:self.slotArray];
    
}

- (void)removeNullSlotArrayAtIndex:(NSUInteger)index
{
    if (index <= (self.slotArray.count - 1))
    {
        if ([self.slotArray[index] isKindOfClass:[NSNull class]])
        {
            [self.slotArray removeObjectAtIndex:index];
        }
    }
}

- (void)removeNulSlotsFromSlotArray
{
    DLog(@"null inintia %@",self.slotArray);
    BOOL isPreviousSlotEmpty = NO;
    BOOL isCurrentSlotEmpty = NO;

           for (NSUInteger i = 5; i < self.slotArray.count; i++)
        {
            
            if (![self isCardExistAtSlotIndex:i])
            {
                if(isPreviousSlotEmpty && isCurrentSlotEmpty)
                {
                    [self removeNullSlotArrayAtIndex:i];
                }
                else
                {
                    isPreviousSlotEmpty = YES;
                    if(isPreviousSlotEmpty)
                    {
                       isCurrentSlotEmpty = YES;
                    }
                }
            }
            else
            {
                isPreviousSlotEmpty = NO;
                isCurrentSlotEmpty = NO;

            }
        }
    for (NSUInteger i = ([TAJUtilities isIPhone] ? REMOVE_EMPTY_SPACE_FROM_INDEX_IPHONE : REMOVE_EMPTY_SPACE_FROM_INDEX_IPAD); i < self.slotArray.count; i++)
    {
        [self removeNullSlotArrayAtIndex:i];
    }
    int removeSlotsFromIndex =18;
    for (int index = self.slotArray.count; index > 0; index--)
    {
        if ([self isCardExistAtSlotIndex:index-1])
        {
            removeSlotsFromIndex = index;
            break;
        }
    }
    if (removeSlotsFromIndex <18 && removeSlotsFromIndex > 16)
    {
        
    }
    for (int nullSlotIndex = removeSlotsFromIndex; nullSlotIndex <self.slotArray.count; nullSlotIndex ++)
    {
        [self removeNullSlotArrayAtIndex:nullSlotIndex];
    }
}
- (void)placeNullObjectForGroupedSlotAtIndex:(NSMutableArray *)indexArray
{
    BOOL isPreviousSlotEmpty =NO;
    for (int i=0; i<2 ; i++)
    {
        for (NSUInteger i = 0; i < self.slotArray.count; i++)
        {
            
            if (![self isCardExistAtSlotIndex:i])
            {
                if(isPreviousSlotEmpty)
                {
                    [self removeNullSlotArrayAtIndex:i];
                }
                else
                {
                    isPreviousSlotEmpty =YES;
                }
            }
            else
            {
                isPreviousSlotEmpty =NO;
            }
        }
    }
   
   for (NSUInteger i = ([TAJUtilities isIPhone] ? REMOVE_EMPTY_SPACE_FROM_INDEX_IPHONE : REMOVE_EMPTY_SPACE_FROM_INDEX_IPAD); i < self.slotArray.count; i++)
   {
       [self removeNullSlotArrayAtIndex:i];
    }
}

- (void)avoidUnneccesorySpaceAtEnd
{
    int noOfNullObjectExist = 0;
    
    NSUInteger  indexOfNullExist = 0;
    for (NSUInteger i = (self.slotArray.count - 1); i < self.slotArray.count; i--)
    {
       if(![self isCardExistAtSlotIndex:i])
       {
           indexOfNullExist = i;
           break;
       }
    }
    
    NSUInteger  indexOfAnotherCardExist = 0;
    
    for (NSUInteger i = indexOfNullExist; i < self.slotArray.count; i--)
    {
        if (![self isCardExistAtSlotIndex:i])
        {
            noOfNullObjectExist++;
        }
        else if([self isCardExistAtSlotIndex:i])
        {
            indexOfAnotherCardExist = i;
            break;
        }
    }
    
    for (NSUInteger i = (indexOfAnotherCardExist + 1); i < indexOfNullExist; i++)
    {
        [self removeNullSlotArrayAtIndex:i];
    }
}

- (void)insertPickedCardToSlotArray:(NSMutableDictionary *)dict
{
    // first find last card info
    NSUInteger indexOfLastCardInfo;
    for (int i = 0; i < self.slotArray.count; i++)
    {
        if ([self.slotArray[i] isKindOfClass:[NSMutableDictionary class]])
        {
            indexOfLastCardInfo = i;
        }
    }
    // remove unwanted null object after last card
    if (self.slotArray.count > 0)
    {
        [self.slotArray removeObjectsInRange:NSMakeRange((indexOfLastCardInfo + 1), (self.slotArray.count - 1) - indexOfLastCardInfo)];
    }
    
    // add the picked card into slot
    [self.slotArray addObject:dict];
    
    // Add 2 null object to last for moving card extremly to right side
    if([TAJUtilities isIPhone])
    {
        if ([TAJUtilities isItIPhone5])
        {
            if (indexOfLastCardInfo <= INSERT_NULL_OBJECT_IN_RANGE_Iphone5)
            {
            [self.slotArray addObject:[NSNull null]];
            }
        }
        else
        {
            if (indexOfLastCardInfo <= INSERT_NULL_OBJECT_IN_RANGE)
            {
                [self.slotArray addObject:[NSNull null]];
            }
        }
    }
    else
    {
        if (indexOfLastCardInfo <= INSERT_NULL_OBJECT_IN_RANGE)
        {
            [self.slotArray addObject:[NSNull null]];
        }
    }
    [self resetTableCardsArray:self.slotArray];
}

- (NSUInteger)getLastSlotCardInfoIndex
{
    NSUInteger indexOfLastCard = 0;
    for (NSUInteger i = 0; i < self.slotArray.count; i++)
    {
        if ([self isCardExistAtSlotIndex:i])
        {
            indexOfLastCard = i;
        }
    }
    return indexOfLastCard;
}

- (NSMutableArray *)getSlotCardsArray
{
    return self.slotArray;
}

- (void)removeUnwantedNullObjects
{
    // first find last card info
    NSUInteger indexOfLastCardInfo = 0;
    
    for (int i = 0; i < self.slotArray.count; i++)
    {
        if ([self.slotArray[i] isKindOfClass:[NSMutableDictionary class]])
        {
            indexOfLastCardInfo = i;
        }
    }
    
    // remove unwanted null object after last card
    if (self.slotArray.count > 0 && indexOfLastCardInfo != 0)
    {
        [self.slotArray removeObjectsInRange:NSMakeRange((indexOfLastCardInfo + 1), (self.slotArray.count - 1) - indexOfLastCardInfo)];
    }
    
    // Add 2 null object to last for moving card extremly to right side
    if([TAJUtilities isIPhone])
    {
        if ([TAJUtilities isItIPhone5])
        {
            if (indexOfLastCardInfo <= INSERT_NULL_OBJECT_IN_RANGE_Iphone5)
            {
                [self.slotArray addObject:[NSNull null]];
            }
        }
        else
        {
            if (indexOfLastCardInfo <= INSERT_NULL_OBJECT_IN_RANGE)
            {
                [self.slotArray addObject:[NSNull null]];
            }
        }
    }
    else
    {
        if (indexOfLastCardInfo <= INSERT_NULL_OBJECT_IN_RANGE)
        {
            [self.slotArray addObject:[NSNull null]];
        }
    }

    for (NSUInteger i = 16; i < self.slotArray.count; i++)
    {
        [self removeNullSlotArrayAtIndex:i];
    }
}

- (NSMutableArray *)getArrayToRemoveCardSlotAtIndexesArray:(NSMutableArray *)indexArray
{
    // get card info to remove the grouped index cards
    NSMutableArray *subArray = [NSMutableArray array];
    for (NSNumber *indexNumber in indexArray)
    {
        [subArray addObject:[self.slotArray objectAtIndex:[indexNumber unsignedIntegerValue]]];
        
        [self.slotArray replaceObjectAtIndex:[indexNumber unsignedIntegerValue] withObject:[NSNull null]];
    }
    [subArray addObject:[NSNull null]];
    return subArray;
}

- (void)discardedCardAtSlotIndex:(NSUInteger)discardedCardIndex
{
    [self.slotArray removeObjectAtIndex:discardedCardIndex];
    [self resetTableCardsArray:self.slotArray];
}

- (void)discardAutoPlayCardForPlayerPicked:(NSString *)inCardNo withSuit:(NSString *)inSuit
{
    int indexOfObjectToRemove = 9999;
    for (int i = 0; i < self.slotArray.count; i++)
    {
        if ([self isCardExistAtSlotIndex:i])
        {
            NSMutableDictionary *cardInfo = self.slotArray[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardSuit = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
            
            if ([cardNum isEqualToString:inCardNo] &&
                [cardSuit isEqualToString:[inSuit lowercaseString]])
            {
                indexOfObjectToRemove = i;
                break;
            }
        }
    }
    if (indexOfObjectToRemove != 9999)
    {
        [self.slotArray removeObjectAtIndex:indexOfObjectToRemove];
    }
    DLog(@"discardautoplay card %@,%@,%@",inCardNo,inSuit,self.slotArray);
    [self resetTableCardsArray:self.slotArray];
}

- (void)updateSlotArrayForAutoDiscard:(NSString *)inCardNo withSuit:(NSString *)inSuit
{
    int indexOfObjectToRemove = 9999;
    for (int i = 0; i < self.slotArray.count; i++)
    {
        if ([self isCardExistAtSlotIndex:i])
        {
            NSMutableDictionary *cardInfo = self.slotArray[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardSuit = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
            if ([cardNum isEqualToString:inCardNo] &&
                [cardSuit isEqualToString:[inSuit lowercaseString]])
            {
                indexOfObjectToRemove = i;
                break;
            }
        }
    }
    if (indexOfObjectToRemove != 9999)
    {
        [self.slotArray removeObjectAtIndex:indexOfObjectToRemove];
    }
    [self resetTableCardsArray:self.slotArray];
}

- (NSUInteger)getMaxNumberInArray:(NSMutableArray *)array
{
    NSUInteger maxValue = 0;
    for (NSNumber *num in array)
    {
        int currentValue = [num unsignedIntegerValue];
        if (currentValue > maxValue)
        {
            maxValue = currentValue;
        }
    }
    return maxValue;
}

- (void)removeAllSlotCards
{
    [self.slotArray removeAllObjects];
    [self.tableCardArray removeAllObjects];
}

#pragma mark - Slot Delegate  -

- (void)informControllerForSlotChange
{
    if (self.slotDelegate && [self.slotDelegate respondsToSelector:@selector(slotCardArray:withTableCardsArray:)])
    {
        [self.slotDelegate slotCardArray:self.slotArray withTableCardsArray:self.tableCardArray];
    }
}

@end
