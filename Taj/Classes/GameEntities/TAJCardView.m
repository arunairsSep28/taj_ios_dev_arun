/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 19/05/14.
 Created by Sujit Yadawad on 03/02/14.
 **/

#import "TAJCardView.h"

@interface TAJCardView()

@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *cardBackImageView;
@property (weak, nonatomic) IBOutlet UIImageView *selectedCardImageView;

//joker
@property (weak, nonatomic) IBOutlet UIImageView *jokerImageView;

@property (nonatomic) CGRect startRect;

@end

@implementation TAJCardView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        //[self hideAllUI];
        self.isJokerFlipped = NO;
        self.exclusiveTouch = YES;
    }
    return self;
}

- (void)cardStateChangeAction
{
    if (!self.isMomentaryCard)
    {
        self.isSelected = !self.isSelected;
        
        // Inform and Highlight accordingly
        [self buttonStateChangedTo:self.isSelected];
    }
    else
    {
        [self didSelectCard:self];
    }
}

- (void)showCard:(NSString *)cardID isMomentary:(BOOL)isMomentary
{
    //NSLog(@"JOKER cardID = %@",cardID);
    self.isMomentaryCard = isMomentary;
    self.cardId = [cardID uppercaseString];
    
    self.isSelected = NO;
    self.draggable = NO;
    [self setExclusiveTouch:YES];
    
    // Set card image according to the input string
    UIImage *cardImage = [UIImage imageNamed:cardID];
    self.cardImageView.image = cardImage;
    
    //show joker label if joker card is exist in my deck of cards
    self.jokerCardNumber = [self getJokerCardNumber];
    //ratheesh - start
    NSString * jokerNumber = [TAJUtilities checkIfStringIsEmpty:self.jokerCardNumber];
    if (jokerNumber.length == 0) {
        self.jokerCardNumber = APP_DELEGATE.jokerNumber;
    }
    //ratheesh - end
    
    //NSLog(@"JOKER NUMBER %@, CARD NUMBER %@",self.jokerCardNumber,self.cardNumber);
    
    if ([self.jokerCardNumber isEqualToString:@"0"] && [self.cardNumber isEqualToString:@"1"])
    {
        self.jokerImageView.hidden = NO;
    }
    else if ([self.jokerCardNumber isEqualToString:self.cardNumber])
    {
        self.jokerImageView.hidden = NO;
    }
}

- (NSString *)cardNumber
{
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.cardId];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    return numberString;
}

- (NSString *)cardSuit
{
    // Intermediate
    NSString *suitString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.cardId];
    NSCharacterSet *characters = [NSCharacterSet letterCharacterSet];
    
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:characters intoString:NULL];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:characters intoString:&suitString];
    
    // Result.
    return [suitString lowercaseString];
}

- (NSString *)getJokerCardNumber
{
    NSString *jokerCardNumber = nil;
    if ([self.delegate respondsToSelector:@selector(getJokerCardNumber)])
    {
        jokerCardNumber = [self.delegate getJokerCardNumber];
        
    }
    return jokerCardNumber;
}

- (void)unselectCard
{
    if (self.isSelected)
    {
        [self cardStateChangeAction];
    }
}

- (void)unselectCardAndNotify:(BOOL)notifyDelegate
{
    if (notifyDelegate)
    {
        [self unselectCard];
    }
    else
    {
        self.isSelected = NO;
    }
}

- (void)setIsSelected:(BOOL)isSelected
{
    if (_isSelected != isSelected)
    {
        _isSelected = isSelected;
    }
    self.selectedCardImageView.hidden = !_isSelected;
}

- (void)flipCard:(BOOL)inValue
{
    self.cardBackImageView.hidden = !inValue;
    
    if (inValue)
    {
        [self unselectCard];
    }
    
    if (!self.jokerImageView.hidden && !self.isJokerFlipped)
    {
        self.jokerImageView.hidden = inValue;
        self.isJokerFlipped = inValue;
    }
    else if(self.jokerImageView.hidden && self.isJokerFlipped)
    {
        self.jokerImageView.hidden = inValue;
        self.isJokerFlipped = inValue;
    }
}

#pragma mark - Protocol Methods -

- (void)buttonStateChangedTo:(BOOL)state
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(card:didChangeState:)])
    {
        [self.delegate card:self didChangeState:state];
    }
}

- (void)didSelectCard:(TAJCardView *)card
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectCard:)])
    {
        [self.delegate didSelectCard:self];
    }
}

#pragma mark - Touch Events -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    self.startRect = self.frame;
    
    if (self.draggable && self.delegate && [self.delegate respondsToSelector:@selector(touchesBegan:withEvent:withCard:)])
    {
        [self.delegate touchesBegan:touches withEvent:event withCard:self];
    }
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event
{
    NSSet *multiTouch = [event allTouches];
    if( [multiTouch count] > 1 || !self.draggable)
    {
        return;
    }
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(touchesMoved:withEvent:withCard:)])
    {
        [self.delegate touchesMoved:touches withEvent:event withCard:self];
    }
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.draggable && self.delegate && [self.delegate respondsToSelector:@selector(touchesEnded:withEvent:withCard:)])
    {
        [self.delegate touchesEnded:touches withEvent:event withCard:self];
    }
    
    if (CGRectEqualToRect(self.startRect, self.frame))
    {
        [self cardStateChangeAction];
    }
    else
    {
        [self unselectCard];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.draggable && self.delegate && [self.delegate respondsToSelector:@selector(touchesCancelled:withEvent:withCard:)])
    {
        [self.delegate touchesCancelled:touches withEvent:event withCard:self];
    }
    
    if (CGRectEqualToRect(self.startRect, self.frame))
    {
        [self cardStateChangeAction];
    }
    else
    {
        [self unselectCard];
    }
}

@end
