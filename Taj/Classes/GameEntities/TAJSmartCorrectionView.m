/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMeldCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/02/14.
 **/

#import "TAJSmartCorrectionView.h"
#import "TAJGameJoker.h"

#import "TAJConstants.h"
#import "TAJUtilities.h"

#import "TAJXMLParserDictionary.h"

#define MELD_CARD_OFFSET_X_IPHONE_4           20.0f
#define MELD_CARD_OFFSET_X_IPHONE_5           20.0f
#define MELD_CARD_OFFSET_X_IPAD               38.0f
#define MELD_CARD_OFFSET_X_IPAD_EMPTY         52.0f
#define SLOT_CARD_FACTOR_IPHONE_5        18
#define SLOT_CARD_FACTOR_IPHONE_4        16
#define SLOT_CARD_FACTOR_IPAD 33

#define WAIT_WHILE_CHECKING_CARDS      @"Please wait checking cards"


@interface TAJSmartCorrectionView() <TAJCardViewProtocol>

@property (nonatomic, strong) NSString        *jokerCardNumber;
@property (strong, nonatomic) IBOutlet UIImageView *jokerImageView;
@property (weak, nonatomic) IBOutlet UIView *jokerImageHolderView;

@property (weak, nonatomic) IBOutlet UIView *jokerViewHolder;

//send card action
- (IBAction)sendCardsAction:(UIButton *)sender;
- (IBAction)sendCardsCancelAction:(UIButton *)sender;
- (IBAction)sendCloseAction:(UIButton *)sender;

//meld timer label
@property (weak, nonatomic) IBOutlet UILabel *meldMessageLabel;

@property (weak, nonatomic) IBOutlet UILabel *meldTimerLabel;
@property (strong, nonatomic) NSTimer        *meldNSTimer;
@property (strong, nonatomic) NSTimer        *meldContinuousNSTimer;
//time during stat of Meld timer
@property (nonatomic) double timeDuringStartTimer;
@property (nonatomic) BOOL isTimerStarted;

@property (weak, nonatomic) IBOutlet UIView *viewDeclarationButtons;
@property (weak, nonatomic) IBOutlet UIView *viewMainContainer;

@end

@implementation TAJSmartCorrectionView

@synthesize meldDelegate        = _meldDelegate;
@synthesize wrongMeldArray      = _wrongMeldArray;
@synthesize correctMeldArray     = _correctMeldArray;
@synthesize meldNSTimer         = _meldNSTimer;
@synthesize meldContinuousNSTimer = _meldContinuousNSTimer;

@synthesize meldTimerLabel      = _meldTimerLabel;
@synthesize meldTimer           = _meldTimer;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.wrongMeldArray = [NSMutableArray array];
        self.correctMeldArray = [NSMutableArray array];
        self.meldTimer = 0;
        self.isTimeOut=NO;
        self.isTimerStarted = NO;
        self.shoudlForceProcessMeldiPhone4 = NO;
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    self.meldContinuousNSTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self
                                                                selector:@selector(continuousMeldTimer:)
                                                                userInfo:nil repeats:YES];
    
    if ([TAJUtilities isItIPhone6]) {
        CGRect Frame = self.viewDeclarationButtons.frame;
        Frame.origin.y = self.viewDeclarationButtons.frame.origin.y + 16;
        self.viewDeclarationButtons.frame = Frame;
    }
    
    if ([TAJUtilities isItIPhonex] || [TAJUtilities isItIPhoneXSMax] ) {
        
        CGRect Frame = self.viewDeclarationButtons.frame;
        Frame.origin.y = self.viewDeclarationButtons.frame.origin.y + 16;
        self.viewDeclarationButtons.frame = Frame;
        
        CGRect mainFrame = self.viewMainContainer.frame;
        mainFrame.size.width = self.viewMainContainer.frame.size.width - 84;
        self.viewMainContainer.frame = mainFrame;
    }
    
}

- (void)setupMyDeckCardsWithArray:(NSDictionary *)array withJokerNumber:(NSString *)jokerCardID{
    //id str = [NSString stringWithFormat:@"<request><wmeld><box><card face='2' suit='d'/><card face='2' suit='h'/><card face='2' suit='c'/></box><box><card face='1' suit='c'/><card face='5' suit='h'/><card face='6' suit='h'/></box><box><card face='2' suit='d'/><card face='2' suit='h'/><card face='2' suit='c'/></box><box><card face='1' suit='c'/><card face='5' suit='h'/><card face='6' suit='h'/></box></wmeld> <cmeld><box><card face='2' suit='d'/><card face='2' suit='h'/><card face='2' suit='c'/></box><box><card face='1' suit='c'/><card face='5' suit='h'/><card face='6' suit='h'/></box><box><card face='2' suit='d'/><card face='2' suit='h'/><card face='2' suit='c'/></box><box><card face='1' suit='c'/><card face='5' suit='h'/><card face='6' suit='h'/></box></cmeld></request>"];
    
    //        NSDictionary *dictionary =  NULL;
    //        dictionary = [NSDictionary dictionaryWithXMLString:str];
    
    NSMutableDictionary *wMeldDict = array[kTaj_wmeld];
    NSMutableDictionary *cMeldDict = array[kTaj_cmeld];
    
    NSMutableArray *wMeldArray = wMeldDict[kTaj_scoresheet_box];
    NSMutableArray *cMeldArray = cMeldDict[kTaj_scoresheet_box];
    
    
    [self fillUpUI:[self processMeldCards:wMeldArray] view:self.wrongMeldView];
    [self fillUpUI:[self processMeldCards:cMeldArray] view:self.correctMeldView];
    
    [self setJokerNumber:jokerCardID];
    [self addJokerCard:jokerCardID];
    //    }
}

- (NSMutableArray*)processMeldCards:(id)arrayValue
{
    if (![[TAJUtilities sharedUtilities] isInternetConnected]){
        self.isTimeOut=YES;
    }
    
    //    self.meldTimerLabel.text = @"";
    //    self.meldMessageLabel.text = WAIT_WHILE_CHECKING_CARDS;
    NSMutableArray *cardsRemaining = [NSMutableArray array];
    
    NSArray *array = [self convertToArray:arrayValue];
    
    if ([array count] > 0)
    {
        for (NSUInteger i = 0; i < array.count; i++){
            if(i != 0){
                [cardsRemaining addObject: [NSNull null]];
                [cardsRemaining addObject: [NSNull null]];
            }
            
            id obj = [array[i] objectForKey:kCards_Key];
            NSArray *dictArray = [self convertToArray:obj];
            for (NSUInteger j = 0; j < [dictArray count]; j++){
                NSDictionary *dict = dictArray[j];
                [cardsRemaining addObject: dict];
            }
            
        }
    }
    return cardsRemaining;
}

-(NSMutableArray*)convertToArray:(id)obj{
    NSMutableArray *array = [NSMutableArray array];
    if([obj isKindOfClass:[NSArray class]]){
        array = obj;
    }else if([obj isKindOfClass:[NSDictionary class]]){
        NSMutableDictionary *dict = [NSMutableDictionary
                                     dictionaryWithDictionary:obj];
        array = [NSMutableArray arrayWithObjects:dict, nil];
    }
    return array;
}

- (void)fillUpUI:(NSMutableArray *)array view:(UIView*)cardHolderView
{
    for (int i = 0; i < array.count; i++)
    {
        if ([array[i] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *cardInfo = array[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
            NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
            NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
            NSUInteger slotindex = [cardInfo[SLOT_KEY] unsignedIntegerValue];
            TAJCardView *card = [self loadCardView:cardId];
            card.placeID = index;
            card.slotID = slotindex;
            [self placeMeldCards:card atIndex:i inView:cardHolderView isEmpty:NO];
        }else if ([array[i] isKindOfClass:[NSNull class]])
        {
            TAJCardView *view = [[TAJCardView alloc]init];
            [self placeMeldCards:view atIndex:i inView:cardHolderView isEmpty:YES] ;
        }
        
    }
}

#pragma mark - add card to view -

- (void)placeMeldCards:(TAJCardView *)card atIndex:(NSUInteger)index inView:(UIView *)meldView isEmpty:(BOOL)isEmptyView
{
    CGRect meldFrame = meldView.frame;
    CGRect cardFrame = card.frame;
    
    if ([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            cardFrame.origin.x = (index + 1) * MELD_CARD_OFFSET_X_IPHONE_5;
        }else
        {
            cardFrame.origin.x = (index + 1) * MELD_CARD_OFFSET_X_IPHONE_4;
        }
    }else
    {
        cardFrame.origin.x = (index + 1) * MELD_CARD_OFFSET_X_IPAD;
    }
    cardFrame.origin.y = meldFrame.size.height/2 - cardFrame.size.height/2;
    
    card.frame = cardFrame;
    
    if ([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            card.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.63f, 0.73f);
        }
        else
        {
            card.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.78f, 0.85f);
        }
    }
    else
    {
        card.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.73f, 0.77f);
    }
    [meldView addSubview:card];
}

- (void)setJokerNumber:(NSString *)jokerCardID
{
    NSString *numberString;
    NSScanner *scanner = [NSScanner scannerWithString:jokerCardID];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    self.jokerCardNumber = numberString;
}

- (void)addJokerCard:(NSString *)jokerCardID
{
    TAJGameJoker *jokerView = nil;
    if (self.jokerViewHolder.subviews && self.jokerViewHolder.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [self.jokerViewHolder subviews];
        jokerView = (TAJGameJoker *)subviews[0];
    }
    else
    {
        NSArray *nibContents;
        int index = 0;
        
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJGameJokerNibName //change the nib name
                                                    owner:nil
                                                  options:nil];
        jokerView = nibContents[index];
        [self.jokerViewHolder addSubview:jokerView];
    }
    [jokerView showJokerCardForCardID:jokerCardID];
}


#pragma mark - Load TAJCardView -

- (TAJCardView *)loadCardView:(NSString *)cardID
{
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil options:nil];
    TAJCardView *card = nibContents[index];
    card.delegate = self;
    
    [card showCard:cardID isMomentary:YES];
    
    return card;
}

#pragma mark - IBOutlet Action -

- (IBAction)sendCardsCancelAction:(UIButton *)sender {
    [self sendAgreeToController:@"0"];
}

- (IBAction)sendCardsAction:(UIButton *)sender
{
    [self sendAgreeToController:@"1"];
}

- (IBAction)sendCloseAction:(UIButton *)sender{
    [self sendAgreeToController:@"0"];
}

- (void)sendAgreeToController:(NSString *)agree
{
    [self invalidateMeldTimer];
    if ([self.meldDelegate respondsToSelector:@selector(didAgreeSmartCards:)])
    {
        [self.meldDelegate didAgreeSmartCards:agree];
    }
}

- (void)didMeldTimeOut
{
    // [self sendAgreeToController:@"0"];
}

#pragma mark - Meld Timer -

- (void)updateMeldMessageLabel:(NSString *)message
{
    self.meldMessageLabel.text = message;
}

- (void)runMeldTimerwithDictionary:(NSDictionary *)meldDictionary withTimeDeduct:(float)time
{
    if (meldDictionary)
    {
        if ([self.meldNSTimer isValid])
        {
            [self.meldNSTimer invalidate];
        }
        
        self.meldTimer = [meldDictionary[kTaj_playershow_timeout] floatValue];
        self.meldTimer = self.meldTimer - round (time);
        
        if (self.meldTimer > -5 && self.meldTimer <-2 && !self.sendCardButton.hidden)
        {
            if ([TAJUtilities isIPhone] && ![TAJUtilities  isItIPhone5])
            {
                self.shoudlForceProcessMeldiPhone4 = YES;
            }
            
        }
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
        
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)] && self.meldTimer <= 5 && self.meldTimer > -2)
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
        else if(self.meldTimer > 5 || self.meldTimer <= -5)
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
            
        }
        
        
        self.isTimerStarted = NO;
        self.timeDuringStartTimer = CACurrentMediaTime();
        self.meldNSTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                          selector:@selector(updateMeldTimer:)
                                                          userInfo:meldDictionary repeats:YES];
        
    }
}

- (void)continuousMeldTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    // DLog(@"update timer for continousMedtimer ");
#endif
    // DLog(@"update timer for continousMedtimer %f",self.meldTimer);
    if (self.meldTimer <= 5 && self.meldTimer >=-5 )
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
    }
    else
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
        }
    }
    
}

- (void)updateMeldTimer:(NSTimer *)timer
{
    // DLog(@"update timer for updateMeldTimer %f",self.meldTimer);
#if ENABLE_TIMER_DBUGGING
    // DLog(@"update timer for updateMeldTimer ");
#endif
    
    NSDictionary *meldDict = nil;
    if (!self.isTimerStarted)
    {
        double newTime =(CACurrentMediaTime() - self.timeDuringStartTimer);
        
        
        if (self.meldTimer > newTime)
        {
            self.meldTimer =  self.meldTimer -round(newTime);
        }
        
        self.isTimerStarted = YES;
    }
    else
    {
        --self.meldTimer;
    }
    
    if (self.meldTimer == -2)
    {
        [self didMeldTimeOut];
    }
    
    //    if (self.shoudlForceProcessMeldiPhone4)
    //    {
    //        [self didMeldTimeOut];
    //        self.shoudlForceProcessMeldiPhone4 = NO;
    //    }
    
    if (self.meldTimer <= 0)
    {
        // Stop timer
        // self.meldTimerLabel.text = @"";
        [self invalidateMeldTimer];
    }
    else
    {
        meldDict = [timer userInfo];
        //show message
        self.meldMessageLabel.text =  [NSString stringWithFormat:@"%@ %d Seconds", MELD_PLAYER_TIMER, (int)self.meldTimer];;//MELD_PLAYER_TIMER;
        self.meldTimerLabel.text = [NSString stringWithFormat:@"%d", (int)self.meldTimer];
        self.meldTimerLabel.textColor= [ UIColor whiteColor];
    }
#if DEBUG
    NSLog(@"smartTimer : %@",[NSString stringWithFormat:@"%d", (int)self.meldTimer]);
#endif
    /*  if (self.meldTimer <= 5)
     {
     if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
     {
     [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
     }
     
     }
     else
     {
     if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
     {
     [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
     }
     
     self.sendCardButton.hidden = NO;
     }*/
    //    if (self.meldTimer <= -5.0f)
    //    {
    //        [self.meldNSTimer invalidate];
    //        self.meldNSTimer = nil;
    //
    //        [self.meldContinuousNSTimer invalidate];
    //        self.meldContinuousNSTimer = nil;
    //
    ////        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
    ////        {
    ////            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
    ////        }
    //    }
}

- (void)invalidateMeldTimer
{
    self.meldTimerLabel.text = @"";
    [self.meldNSTimer invalidate];
    self.meldNSTimer = nil;
    [self.meldContinuousNSTimer invalidate];
    self.meldContinuousNSTimer = nil;
}

#pragma mark - Card Delegate methods -

- (NSString *)getJokerCardNumber
{
    if (self.jokerCardNumber)
    {
        return self.jokerCardNumber;
    }
    return nil;
}

- (void)playerReconnectMeldHandlerForDisconnection
{
    
}

@end
