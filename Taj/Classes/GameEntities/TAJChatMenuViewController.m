/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatMenuViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogish Poojary on 31/07/14.
 **/

#import "TAJChatMenuViewController.h"
#import "TAJChatMenuViewCell.h"
#import "TAJConstants.h"
#import  "TAJUtilities.h"

#define Cell_Identifier @"chatCell"

@interface TAJChatMenuViewController ()

@property (weak, nonatomic) IBOutlet UIView *gestureContainerView;

@end

@implementation TAJChatMenuViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.chatMenuTableView.delegate = self;
    self.chatMenuTableView.dataSource = self;
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapHit:)];
    [self.gestureContainerView addGestureRecognizer:tap];
    
       self.chatMenuViewArray = [NSMutableArray arrayWithObjects:@"Hi",
                                   @"Good luck",
                                   @"Well played",
                                   @"Awesome",
                                   @"Thanks",
                                   @"Cheers",
                                   @"Sorry",
                                   @"Haha",
                                   @"You’re good!",
                                   @"Thanks",
                                   @"Oops",
                                   @"Good game",
                                   @"Sorry gotta run",
                                   @"One more game?",
                                   @"Keep it going!",
                                   @"You’re awesome!",
                                   @"How did I miss that?",
                                   @"I didn’t see that coming!",
                                    nil];
}

-(void) tapHit:(UITapGestureRecognizer *)tap
{
    if ([self.chatMenuViewDelegate respondsToSelector:@selector(removeChatMenuView)])
    {
        [self.chatMenuViewDelegate removeChatMenuView];
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatMenuViewArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TAJChatMenuViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Cell_Identifier];
    
    if (!cell)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"TAJChatMenuViewCell"
                                                       owner:nil options:nil];
        if ([TAJUtilities isIPhone])
        {
           if([TAJUtilities isItIPhone5])
           {
             cell = array[0];
           }
            else
            {
               cell = array[1];
            }
        }
        else
        {
           cell = array[0];
        }
    }
    cell.chatMenuCellDelegate = self;
    cell.chatDefaultTextButton.tag = indexPath.row;
    cell.defaultTextLabel.text = self.chatMenuViewArray[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (IBAction)closeChatMenuView:(UIButton *)sender
{
    if ([self.chatMenuViewDelegate respondsToSelector:@selector(removeChatMenuView)])
    {
        [self.chatMenuViewDelegate removeChatMenuView];
    }
}

-(void)chatMenuButton_p:(NSInteger )buttonId
{
    NSString *resultString;
           resultString = [[NSString alloc] initWithFormat:@"%@",self.chatMenuViewArray[buttonId]];
    
    
    NSDictionary *aDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:resultString, DEFAULT_TEXT, nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:CHATMENU_TABLE_CELL_SELECTED_ALERT object:aDictionary];
}
@end
