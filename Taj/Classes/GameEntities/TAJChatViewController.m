/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogish Poojary on 13/06/14.
 Created by Sujit Yadawad on 27/03/14.
 **/

#import "TAJChatViewController.h"
#import "TAJGameEngine.h"
#import "TAJChatViewCell.h"
#import "TAJLobby.h"
#import "TAJUtilities.h"
#import "TAJChatMenuView.h"

#define Cell_Identifier @"chatCell"

@interface TAJChatViewController () <UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate, UIGestureRecognizerDelegate,TAJChatMenuDelegate>

@property (weak, nonatomic) IBOutlet UIButton    *sendButton;


@property (strong,nonatomic) TAJChatMenuView     *chatMenuView;
@property (nonatomic)           BOOL             spectator;
@property (nonatomic, strong)    NSString *      chatTableID;

@end

@implementation TAJChatViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.chatHistoryDataSource = [NSMutableArray array];
    }
    return self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
       self.chatHistoryDataSource = [NSMutableArray array];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self addChatObservers];
    
    self.chatField.delegate = self;
    
    self.chatHistory.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
#if NEW_CHAT_ENABLE
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(didselectRowAtIndexPath:)
                                                 name:CHATMENU_TABLE_CELL_SELECTED_ALERT
                                               object:nil];
#else
    UILongPressGestureRecognizer *longPressRecognizer = [[UILongPressGestureRecognizer alloc] initWithTarget:self  action:@selector(longPressDetected:)];
    longPressRecognizer.minimumPressDuration = 1.5;
    longPressRecognizer.delegate = self;
    
    [self.sendButton addGestureRecognizer:longPressRecognizer];
#endif
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
     [[NSNotificationCenter defaultCenter] removeObserver:self name:CHATMENU_TABLE_CELL_SELECTED_ALERT object:nil];
}

- (void)didselectRowAtIndexPath:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    NSString *defaultText = dictionary[DEFAULT_TEXT];
    NSString *text;
    if ([defaultText length] > 0 && self.playerModel)
    {
        [[TAJGameEngine sharedGameEngine] chatMessageWithUserID:self.playerModel.playerID
                                                        tableID:self.playerModel.tableID
                                                       nickName:self.playerModel.playerName
                                                           text:defaultText];
        
        text = [NSString stringWithFormat:@"%@ : %@",[self.playerModel.playerName uppercaseString], defaultText];
        [self.chatHistoryDataSource addObject:text];
        [self.chatHistory reloadData];
        
        // Resetting data
        self.chatField.text = @"";
        
        if (self.chatHistoryDataSource.count > 0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatHistoryDataSource.count-1 inSection:0];
            [self.chatHistory scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self removeChatObserver];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)isChatViewShowing
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

- (void)reloadChatHistoryTable
{
    [self.chatHistory reloadData];
}

- (void)reloadChatHistoryTableFromChatExtent:(NSMutableArray *)messageList
{
    if(self.chatHistoryDataSource.count > 0)
    {
        [self.chatHistoryDataSource removeAllObjects];
    }
    if(messageList.count > 0)
    {
        [self.chatHistoryDataSource  addObjectsFromArray:messageList];
    }
    [self.chatHistory reloadData];
    if (self.chatHistoryDataSource.count > 0)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatHistoryDataSource.count-1 inSection:0];
        [self.chatHistory scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }

    
}

- (void)updateChatPlayerModel:(TAJPlayerModel *)playerModel
{
    if (playerModel)
    {
        self.playerModel = playerModel;
    }
    else
    {
        self.playerModel = nil;
    }
}

- (void)currentChatTableID:(NSString *)tableID
{
    if (tableID)
    {
        self.chatTableID = tableID;
    }
}

- (IBAction)sendButtonAction:(UIButton *)sender
{
    
#if NEW_CHAT_ENABLE
    if (!self.playerModel)
    {
        // for spectator
        [self.chatViewDelegate showChatErrorAlert:@"You are not eligible to chat"];
        self.chatField.text = @"";
    }
    else
    {
        
        [self loadChatMenuView];
    }
#else
    if (!self.playerModel)
    {
        // for spectator
        [self.chatViewDelegate showChatErrorAlert:@"You are not eligible to chat"];
        self.chatField.text = @"";
    }
    else
    {
        NSString *string = [self.chatField text];
        
        [self sendChatString:string];
    }
  
#endif
}

- (void)loadChatMenuView
{
    if ([self.chatViewDelegate respondsToSelector:@selector(loadChatMenuViewInGamePlay)])
    {
        [self.chatViewDelegate loadChatMenuViewInGamePlay];
    }
    
}

- (void)longPressDetected:(UILongPressGestureRecognizer *)gesture
{
    if (gesture.state == UIGestureRecognizerStateEnded)
    {
        DLog(@"Long press Ended");
    }
    else
    {
        if (![gesture isEnabled]) return;
        if (!self.chatMenuView)
        {
            NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TAJChatMenuView" owner:nil options:nil];
            self.chatMenuView = [subviewArray objectAtIndex:0];
            if([TAJUtilities isIPhone])
            {
                CGRect frame = self.chatMenuView.frame;
                frame.origin.x = self.view.frame.origin.x + 110.0;
                frame.origin.y = self.view.frame.origin.y-10; //- 20.0;
                self.chatMenuView.frame = frame;
            }
            else
            {
                CGRect frame = self.chatMenuView.frame;
                frame.origin.x = self.view.frame.origin.x + 400.0;
                frame.origin.y = self.view.frame.origin.y; //- 20.0;
                self.chatMenuView.frame = frame;

            }
            
            [self.view addSubview:self.chatMenuView];
            self.chatMenuView.chatMenuDelegate = self;
        }
        DLog(@"Long press detected.");
    }
}

- (BOOL)gestureRecognizer:(UIGestureRecognizer *)gestureRecognizer shouldRequireFailureOfGestureRecognizer:(UIGestureRecognizer *)otherGestureRecognizer
{
    if ([otherGestureRecognizer isKindOfClass:[UILongPressGestureRecognizer class]])
    {
        return YES;
    }
    return NO;
}

#pragma mark - Chat Menu Delegate -

- (void)preDefinedTextSelected:(NSString *)message
{
    [self removeChatMenuView];
    
    //self.chatField.text = message;
    
    [self sendChatString:self.chatField.text];
}

- (void)chatMenuClosed
{
    [self removeChatMenuView];
}

- (void)removeChatMenuView
{
    if (self.chatMenuView)
    {
        self.chatMenuView.chatMenuDelegate = nil;
        [self.chatMenuView removeFromSuperview];
        self.chatMenuView = nil;
    }
}

#pragma mark - Notification observers -

- (void)addChatObservers
{
    // chat message
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(chatMessageRecieved:)
                                                 name:CHAT_MSG
                                               object:nil];
}

- (void)removeChatObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CHAT_MSG object:nil];
}

- (void)chatMessageRecieved:(NSNotification *)notification
{
   
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.chatTableID])
    {
        [self displayChatFromDictionary:dictionary];
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_CHAT_MESSAGE_FOR_EXTENT_ALERT object:dictionary];
    }
}

- (void)displayChatFromDictionary:(NSDictionary *)dictionary
{
    NSString *name = dictionary[kTaj_nickname];
    NSString *message = dictionary[kTaj_Text];
    
    NSString *string = [NSString stringWithFormat:@"%@ : %@",[name uppercaseString], message];
    
    [self.chatHistoryDataSource addObject:string];
    [self.chatHistory reloadData];
    if (self.chatHistoryDataSource.count > 0)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatHistoryDataSource.count-1 inSection:0];
        [self.chatHistory scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    
    BOOL isViewShowing = [self isChatViewShowing];
    if (!isViewShowing)
    {
        if ([self.chatViewDelegate respondsToSelector:@selector(highLightChatButton)])
        {
            [self.chatViewDelegate highLightChatButton];
        }
    }
}

- (void)sendChatString:(NSString *)string
{
    if ([string length] > 0 && self.playerModel)
    {
        [[TAJGameEngine sharedGameEngine] chatMessageWithUserID:self.playerModel.playerID
                                                        tableID:self.playerModel.tableID
                                                       nickName:self.playerModel.playerName
                                                           text:string];
        
        string = [NSString stringWithFormat:@"%@ : %@",[self.playerModel.playerName uppercaseString], string];
        [self.chatHistoryDataSource addObject:string];
        [self.chatHistory reloadData];
        
        // Resetting data
        self.chatField.text = @"";
        
        if (self.chatHistoryDataSource.count > 0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatHistoryDataSource.count-1 inSection:0];
            [self.chatHistory scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
    else if([string length] == 0 && self.playerModel)
    {
        [self.chatViewDelegate showChatErrorAlert:@"Please enter text"];
        [self.chatField resignFirstResponder];

    }
    else if (!self.playerModel)
    {
        // for spectator
        self.chatField.text = @"";
        [self.chatViewDelegate showChatErrorAlert:@"You are not eligible to chat"];
    }
}

#pragma mark - Table view data source -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatHistoryDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    TAJChatViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Cell_Identifier];
    
    if (!cell)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"TAJChatViewCell"
       owner:nil options:nil];
        if([TAJUtilities isIPhone])
        {
             if([TAJUtilities isItIPhone5])
             {
                cell =  array[0];
             }
            else
            {
                cell =  array[1];
            }
        }
        else
        {
           cell =  array[0];
        }
        
    }
    cell.chatLabel.text = self.chatHistoryDataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row =  indexPath.row;
    CGFloat rowHeight = 0.0f;
    
    if(self.chatHistoryDataSource[row])
    {
        UIFont* font = [UIFont fontWithName:@"AvantGardeITCbyBT-Demi" size:([TAJUtilities isIPhone] ? 10.0f : 13.0f)];
        CGSize expectedLabelSize = [self.chatHistoryDataSource[row] sizeWithFont:font
                                                               constrainedToSize:CGSizeMake(tableView.frame.size.width, 1000.0f)
                                                                   lineBreakMode:NSLineBreakByWordWrapping];
        rowHeight = expectedLabelSize.height;
        DLog(@"height %f", rowHeight);
        if([TAJUtilities isIPhone])
        {
            if([TAJUtilities isItIPhone5])
            {
                rowHeight = rowHeight < tableView.rowHeight ? tableView.rowHeight : rowHeight + 10.0f;
            }
            else
            {
               rowHeight = rowHeight < tableView.rowHeight ? tableView.rowHeight : rowHeight;
            }
            
        }
        else
        {
            rowHeight = rowHeight < tableView.rowHeight ? tableView.rowHeight : rowHeight + 20.0f;
        }
    }
    return rowHeight;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
#if NEW_CHAT_ENABLE
  if (!self.playerModel)
    {
        // for spectator
        [self.chatViewDelegate showChatErrorAlert:@"You are not eligible to chat"];
        [textField resignFirstResponder];
        
    }
   else
   {
       self.chatField.text = @"";
       [self.chatViewDelegate loadChatExtentView:self.chatHistoryDataSource andPlayerModel:self.playerModel];
   }
#else
    if (!self.playerModel)
    {
        // for spectator
        [self.chatViewDelegate showChatErrorAlert:@"You are not eligible to chat"];
        [textField resignFirstResponder];
        
    }
#endif
   
    

}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (!self.playerModel)
    {
        // for spectator
        [self.chatViewDelegate showChatErrorAlert:@"You are not eligible to chat"];
        self.chatField.text = @"";
        
        return NO;
    }

    return YES;
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.chatField.text isEqualToString:@""])
    {
        return YES;
    }
    [self sendChatString:self.chatField.text];
    
    return YES;
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
    UITouch *touch = [[event allTouches] anyObject];
    
    if (![[touch view] isKindOfClass:[UITextField class]])
    {
        [self.chatField resignFirstResponder];
    }
    [super touchesBegan:touches withEvent:event];
}

- (void)clearChatHistory
{
    if(self.chatHistoryDataSource.count > 0)
    {
        [self.chatHistoryDataSource removeAllObjects];
        [self.chatHistory reloadData];
    }
}
@end
