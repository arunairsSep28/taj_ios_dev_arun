/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatMenuViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogish Poojary on 31/07/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJUITextField.h"
#import "TAJChatMenuViewCell.h"

@protocol TAJChatMenuViewControllerProtocol;

@interface TAJChatMenuViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,TAJChatMenuViewCellDelegate>
@property (weak, nonatomic) IBOutlet UIView *chatMenuContentView;
@property (weak, nonatomic) IBOutlet UITableView *chatMenuTableView;
//@property (weak, nonatomic) IBOutlet TAJUITextField *chatMenuTextField;
@property (strong, nonatomic) NSMutableArray *chatMenuViewArray;
@property (weak, nonatomic) id<TAJChatMenuViewControllerProtocol> chatMenuViewDelegate;
@property (weak, nonatomic) IBOutlet UIButton *chatCloseMenu;

- (IBAction)closeChatMenuView:(UIButton *)sender;



@end
@protocol TAJChatMenuViewControllerProtocol<NSObject>
-(void)removeChatMenuView;
@end
