/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerTurnView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 15/04/14.
 **/


#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface TAJPlayerTurnView : UIView
@property(nonatomic)    int               playerRemainingTime;
- (void)startPlayerTimer:(int)playerTime turn:(BOOL)isDeviceTurn placeId:(NSInteger)placeId chunksTimer:(int)playerchunksTimer ischunks:(BOOL)ischunks;
- (void)stopRunningTimer;

@end
