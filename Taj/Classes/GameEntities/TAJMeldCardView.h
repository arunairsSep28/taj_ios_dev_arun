/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMeldCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJCardView.h"
#import "TAJPlayTableView.h"

@protocol TAJMeldCardViewDelegate;

@interface TAJMeldCardView : UIView

@property (strong, nonatomic) NSMutableArray *firstMeldArray;
@property (strong, nonatomic) NSMutableArray *secondMeldArray;
@property (strong, nonatomic) NSMutableArray *thirdMeldArray;
@property (strong, nonatomic) NSMutableArray *fourthMeldArray;
@property (strong, nonatomic) NSMutableArray *selectedCards;
@property (assign, nonatomic) BOOL isTimeOut;
@property (assign, nonatomic) BOOL shoudlForceProcessMeldiPhone4;
@property (nonatomic, strong) TAJPlayTableView  *playTable;


@property (nonatomic) float                  meldTimer;

@property (weak, nonatomic) IBOutlet UIView *firstMeldView;
@property (weak, nonatomic) IBOutlet UIView *secondMeldView;
@property (weak, nonatomic) IBOutlet UIView *thirdMeldView;
@property (weak, nonatomic) IBOutlet UIView *fourthMeldView;


@property (weak, nonatomic) id<TAJMeldCardViewDelegate> meldDelegate;
@property (weak, nonatomic) IBOutlet UIButton *sendCardButton;

- (void)fillMeldView:(NSMutableArray *)meldCardsArray;
- (void)setupMyDeckCardsWithArray:(NSMutableArray *)array withJokerNumber:(NSString *)jokerCardID;
- (void)setScore:(NSDictionary *)dictionary withGameId:(NSString *)gameId;
/*!
 @abstract
    Run meld timer
 
 @param meldDictionary
    The dictionary which contains player details.
 
 @discussion
    The NSTimer is running for meld cards.
*/
- (void)runMeldTimerwithDictionary:(NSDictionary *)meldDictionary withTimeDeduct:(float) time;
- (void)invalidateMeldTimer;
- (void)updateMeldMessageLabel:(NSString *)message;
- (void)playerReconnectMeldHandler;
- (void)playerReconnectMeldHandlerForDisconnection;
- (void)unHideMeldSendCardButton;
- (void)setCurrentSelectedCards:(NSMutableArray *)meldCardsArray;
- (void)didSelectCard:(TAJCardView *)card;
- (TAJCardView *)loadCardView:(NSString *)cardID;
@end

@protocol TAJMeldCardViewDelegate <NSObject>

- (void)didQuitMeld;
- (void)didSendMeldCards:(NSMutableArray *)meldCards;
- (void)didSendCheckMeldCards:(NSMutableArray *)meldCards;
- (void)didTapMeldCard:(NSUInteger)placeId;
- (void)enableSortButtonForMeld:(BOOL)enable;
- (void)disableUserToEnterMeldDuringAutoplay:(BOOL)boolValue;
- (void)removeMeldGroupButtonOnSend;
- (void) dismissMeldScreen;
- (void) cancelMeldAction;

@end
