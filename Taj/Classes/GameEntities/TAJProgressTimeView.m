//
//  TAJProgressTimer.m
//  ProgressTimer
//
//  Created by Ananth T Pai on 05/08/14.
//  Copyright (c) 2014 Ananth T Pai. All rights reserved.
//

#import "TAJProgressTimeView.h"
#import "TAJUtilities.h"

@interface TAJProgressTimeView()
#define DTR(__ANGLE__) ((__ANGLE__) * 0.01745329252f)
#define START_ANGLE -137.0
#define TOTAL_ANGLE 255.0

@property (nonatomic) CGFloat radius;
@property (nonatomic) CGFloat time;
@property (nonatomic) CGFloat timeCount;
@property (nonatomic) CGFloat playerchunksTimer;
@property (nonatomic) UIColor* color;
@property (nonatomic) CGFloat initialAngle;
@property (nonatomic) NSTimer* timer;
@property (nonatomic) BOOL isChunks;

@property (nonatomic) UILabel* circularCounterLabel;
@property (nonatomic) UILabel* rectangularCounterLabel;
@property (nonatomic) UIView* rectangularBgView;

@property (nonatomic) CAShapeLayer* rectangularLayer;
@property (nonatomic) CABasicAnimation *animation;
@property (nonatomic) CFTimeInterval startTime;
@property (nonatomic) CFTimeInterval stopTime;
@property (nonatomic) CGFloat rectangularTimerInitialValue;
@property (nonatomic) CGFloat rectangularTimerEndValueValue;
@property (nonatomic) double timeDuringStartTimer;
@property (nonatomic) BOOL isTimerStarted;
@property (nonatomic) CGRect initialFrame;


enum eTimerType
{
    eSimpleCircularTimer,
    eComplexCircularTimer,
    eRectangularTimer,
};

@property (nonatomic) enum eTimerType timerType;

@end


@implementation TAJProgressTimeView

- (id)initProgresssTimerWithFrame:(CGRect)frame
                            time:(CGFloat)time
                          radius:(CGFloat)radius
                           color:(UIColor*)color{
    self = [self initWithFrame:frame];
    if(self)
    {
        _radius = radius;
        _time = time;
        _color = color;
        _time = time;
        _timeCount = time;
        _timerType = eSimpleCircularTimer;
        _initialAngle = TOTAL_ANGLE;
         _timer = nil;
        _circularCounterLabel= [[UILabel alloc] initWithFrame:frame];
        [_circularCounterLabel setCenter:CGPointMake(frame.size.width / 2, frame.size.height * 0.12)];
        [_circularCounterLabel setBackgroundColor:[UIColor clearColor]];
        [_circularCounterLabel setTextColor:[UIColor whiteColor]];
        if([TAJUtilities isIPhone])
        {
            //Futura-Medium
            //BebasNeue-Regular
            [_circularCounterLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:12]];
        }
        else
        {
            [_circularCounterLabel setFont:[UIFont fontWithName:@"Futura-Medium" size:18]];
        }
        [_circularCounterLabel setText:[NSString stringWithFormat:@"%ld",(long)_timeCount]];
        [_circularCounterLabel setTextAlignment:NSTextAlignmentCenter];
        [self addSubview:_circularCounterLabel];
        
    }
    return self;
}

- (id)initRectangularTimerWithFrame:(CGRect)frame
                              time:(CGFloat)time
                             color:(UIColor*)color
                        chunksTimer:(int)playerchunksTimer
                             ischunks:(BOOL)ischunks
{
    self = [self initWithFrame:frame];
    if(self)
    {
        _time = time;
        _color = color;
        _timeCount = time;
        _isChunks = ischunks;
        _playerchunksTimer = playerchunksTimer;
        _timerType = eRectangularTimer;
         _timer = nil;
        
        [self addRectangularTimer:frame startPoint:1.0];
        
    }
    return self;
}

- (void)addRectangularTimer:(CGRect)frame startPoint:(CGFloat)startPoint
{
    [self setBackgroundColor:[UIColor clearColor]];
    //[self setAlpha:0.6];
    
    _rectangularCounterLabel= [[UILabel alloc] initWithFrame:frame];
    [_rectangularCounterLabel setCenter:CGPointMake(frame.size.width / 2, frame.size.height/2)];
    [_rectangularCounterLabel setBackgroundColor:[UIColor clearColor]];
    [_rectangularCounterLabel setTextColor:[UIColor whiteColor]];
    
    _rectangularBgView = [[UIView alloc] initWithFrame:CGRectMake(-5, -5, self.frame.size.width + 10, self.frame.size.height + 10)];
    _rectangularBgView.backgroundColor = [UIColor blackColor];
    [_rectangularBgView setAlpha:0.6];
    _rectangularBgView.layer.cornerRadius = _rectangularBgView.frame.size.width / 2;
    //NSLog(@"width : %f",frame.size.width);
    
    [self addSubview:_rectangularBgView];
    
    if([TAJUtilities isIPhone])
    {
        //Impact - old // BebasNeue Bold - new
        [_rectangularCounterLabel setFont:[UIFont fontWithName: @"Impact" size:15]];
    }
    else
    {
        [_rectangularCounterLabel setFont:[UIFont fontWithName: @"Impact" size:30]];
    }
    [_rectangularCounterLabel setText:[NSString stringWithFormat:@"%ld",(long)_timeCount]];
    [_rectangularCounterLabel setTextAlignment:NSTextAlignmentCenter];
    [self addSubview:_rectangularCounterLabel];
    
    _rectangularLayer = [CAShapeLayer layer];
    [_rectangularLayer setStrokeColor: _color.CGColor];
    if([TAJUtilities isIPhone])
    {
        [_rectangularLayer setLineWidth:8.0f];
    }
    else
    {
        [_rectangularLayer setLineWidth:8.0f];
    }
    [_rectangularLayer setFillColor:[UIColor clearColor].CGColor];
    
//    UIBezierPath *bezierPath = [UIBezierPath bezierPath];
//    [bezierPath addArcWithCenter:center radius:50 startAngle:0 endAngle:2 * M_PI clockwise:YES];
//
//    [_rectangularLayer setPath:[[UIBezierPath bezierPathWithOvalInRect:CGRectMake(0, 0, self.frame.size.width, self.frame.size.height)] CGPath]];

    [_rectangularLayer setPath:[[UIBezierPath bezierPathWithArcCenter:CGPointMake(self.frame.size.width/2, self.frame.size.height/2) radius:self.frame.size.width/2 startAngle:0 endAngle: 2 * M_PI clockwise:NO] CGPath]];
    
//    path = UIBezierPath(ovalIn: CGRect(x: self.frame.size.width/2 - self.frame.size.height/2,
//    y: 0.0,
//    width: self.frame.size.height,
//    height: self.frame.size.height))

//    UIBezierPath *path = [UIBezierPath bezierPath];
//    [path moveToPoint:CGPointMake(frame.size.width/2, 0)];
//
//    [path addLineToPoint:CGPointMake(0 + frame.size.width/8, 0)];
//    [path addQuadCurveToPoint:CGPointMake(0,0 + frame.size.height/8) controlPoint:CGPointMake(0, 0)];
//
//    [path addLineToPoint:CGPointMake(0, frame.size.height * 7/8)];
//    [path addQuadCurveToPoint:CGPointMake(0 + frame.size.width/8,frame.size.height) controlPoint:CGPointMake(0, frame.size.height)];
//
//    [path addLineToPoint:CGPointMake(frame.size.width* 7/8, frame.size.height)];
//    [path addQuadCurveToPoint:CGPointMake(frame.size.width,frame.size.height* 7/8) controlPoint:CGPointMake(frame.size.width, frame.size.height)];
//
//    [path addLineToPoint:CGPointMake(frame.size.width, frame.size.height/8)];
//    [path addQuadCurveToPoint:CGPointMake(frame.size.width*7/8,0) controlPoint:CGPointMake(frame.size.width,0)];
//
//    [path addLineToPoint:CGPointMake(frame.size.width/2, 0)];

    //_rectangularLayer.path= path.CGPath;
    
    _animation = [[CABasicAnimation alloc] init];
    _animation.keyPath = @"strokeEnd";
    _animation.fromValue = [NSNumber numberWithFloat:startPoint];
    _animation.toValue = [NSNumber numberWithFloat:0.0f];
    _animation.duration = _time;
    
    [_animation setDelegate:self];
    
    [self.layer addSublayer:_rectangularLayer];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        [self setBackgroundColor:[UIColor clearColor]];
    }
    return self;
}

- (void)awakeFromNib
{
    [super awakeFromNib];
}

- (void)drawRect:(CGRect)rect
{
    switch (_timerType) {
        case eSimpleCircularTimer:
        {
            UIBezierPath* circularPath = [UIBezierPath bezierPath];
            [circularPath addArcWithCenter:CGPointMake(rect.size.width / 2, rect.size.height/2)
                                    radius:_radius
                                startAngle:DTR(START_ANGLE)
                                  endAngle:DTR(START_ANGLE - _initialAngle) clockwise:NO];
            if([TAJUtilities isIPhone])
            {
                circularPath.lineWidth = 12;
            }
            else
            {
                circularPath.lineWidth = 21;
            }
            if((_timeCount <= 2* _time/3) && (_timeCount > _time/3))
            {
                [[UIColor yellowColor] setStroke];
                
            }
            else if((_timeCount <= _time/3) && (_timeCount > 0))
            {
                [[UIColor redColor] setStroke];
            }
            else
            {
                [_color setStroke];
            }

            [circularPath stroke];
            
        }
            break;
        case eComplexCircularTimer:
            
            break;
        case eRectangularTimer:
            
            break;
        default:
            break;
    }
    
    NSString* timeValue = [NSString stringWithFormat:@"%ld",(long)_timeCount];
    [self.rectangularCounterLabel setText:timeValue];
    [self.circularCounterLabel setText:timeValue];
}

- (void)updateTimer
{
    if (!self.isTimerStarted)
    {
        double newTime =(CACurrentMediaTime() - self.timeDuringStartTimer);
        if (_timeCount > newTime)
        {
            _timeCount =  _timeCount - newTime;
        }
        self.isTimerStarted = YES;
    }
    if(_timeCount>0)
    {
        _timeCount = _timeCount - 0.1f;
        //_initialAngle = _initialAngle - ((CGFloat)TOTAL_ANGLE/(10*_time));
        
        NSString* timeValue = [NSString stringWithFormat:@"%ld",(long)_timeCount];
           [self.rectangularCounterLabel setText:timeValue];
           [self.circularCounterLabel setText:timeValue];
        [self setNeedsDisplay];
    }
    else if (_playerchunksTimer != 0) {
        
//         [[NSNotificationCenter defaultCenter] postNotificationName:@"chunks_extraTime" object:nil];
        
//        _initialAngle = TOTAL_ANGLE;
        _timeCount = _timeCount + _playerchunksTimer;
//        [self setNeedsDisplay];
//        [self.layer removeAnimationForKey:@"strokeEnd"];
//        [self.layer removeAllAnimations];
//        [self.layer removeFromSuperlayer];
       // [self addRectangularTimer:_initialFrame startPoint:_playerchunksTimer/_time];
//        _color = [UIColor greenColor];
       // [self startTimer];

    }
    else
    {
        [_timer invalidate];
        _timer = nil;
    }
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for progress %f",_timeCount);
#endif
    
    if((_timeCount <= 2* _time/3) && (_timeCount > _time/3))
    {
        //        [_rectangularLayer setStrokeColor:[UIColor colorWithRed:(250.0/255.0) green:(165.0/255.0) blue:(25.0/255.0) alpha:1.0].CGColor];
        //        [self.rectangularCounterLabel setTextColor:[UIColor colorWithRed:(250.0/255.0) green:(165.0/255.0) blue:(25.0/255.0) alpha:1.0]];
        //        [_rectangularLayer setStrokeColor:[UIColor colorWithRed:(251.0/255.0) green:(176.0/255.0) blue:(59.0/255.0) alpha:1.0].CGColor];
        //        [self.rectangularCounterLabel setTextColor:[UIColor colorWithRed:(251.0/255.0) green:(176.0/255.0) blue:(59.0/255.0) alpha:1.0]];
        [_rectangularLayer setStrokeColor:[UIColor yellowColor].CGColor];
        //[self.rectangularCounterLabel setTextColor:[UIColor yellowColor]];
        [self.rectangularCounterLabel setTextColor:[UIColor whiteColor]];
    }
    else if((_timeCount <= _time/3) && (_timeCount > 0.3))
    {
        //NSLog(@"_timeCount : %f",_timeCount);
        [_rectangularLayer setStrokeColor:[UIColor redColor].CGColor];
        //[self.rectangularCounterLabel setTextColor:[UIColor redColor]];
        [self.rectangularCounterLabel setTextColor:[UIColor whiteColor]];
        
    }
    else if(_timeCount <= 0.3)
    {
        [_rectangularLayer setStrokeColor:[UIColor clearColor].CGColor];
        //[self.rectangularCounterLabel setTextColor:[UIColor redColor]];
        [self.rectangularCounterLabel setTextColor:[UIColor whiteColor]];
    }
    
}

-(void)startTimer
{
    if(self.timer)
    {
        [_timer invalidate];
        _timer = nil;
    }
    _isTimerStarted = NO;
    _timer =[NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(updateTimer)
                                           userInfo:nil
                                            repeats:YES];
    _timeDuringStartTimer = CACurrentMediaTime();
    [[NSRunLoop mainRunLoop] addTimer:_timer forMode:NSRunLoopCommonModes];
    switch (_timerType) {
        case eSimpleCircularTimer:
        {
            
        }
            break;
        case eComplexCircularTimer:
            
            break;
        case eRectangularTimer:
        {
            self.startTime = _timeCount;
            [_rectangularLayer addAnimation:_animation forKey:@"myStroke"];
            
        }
            break;
        default:
            break;
    }
}

- (void)stopTimer
{
    _timeCount = 0;
    [_timer invalidate];
    _timer = nil;
    [_rectangularLayer removeAnimationForKey:@"myStroke"];
    self.rectangularLayer = nil;
    self.rectangularCounterLabel = nil;
    self.animation = nil;
    NSLog(@"REMOVEING TIMER");
}

- (void)resetTimer
{
    _timeCount = _playerchunksTimer;
    _initialAngle = TOTAL_ANGLE;
    [_rectangularLayer setStrokeColor:_color.CGColor];
}

- (void)resumeTimer
{
    _timer =[NSTimer scheduledTimerWithTimeInterval:0.1
                                             target:self
                                           selector:@selector(updateTimer)
                                           userInfo:nil
                                            repeats:YES];
    [_animation setTimeOffset:(_time - _timeCount)];
    [_rectangularLayer setStrokeEnd:_rectangularTimerInitialValue];
    [_rectangularLayer addAnimation:_animation forKey:@"myStroke"];
    
}

@end
