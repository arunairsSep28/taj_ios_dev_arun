/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAutoPlayView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 10/05/14.
 Created by Yogisha Poojary on 06/05/14.
 **/

#import "TAJAutoPlayView.h"
#import "TAJConstants.h"

#define MAX_AUTOPLAY_CARD_TO_SHOW   5

@interface TAJAutoPlayView ()

@property (weak, nonatomic) IBOutlet UILabel *autoPlayLabel;

@end

@implementation TAJAutoPlayView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (void)updateAutoplayCount:(NSInteger)autoPlayCount andTotalCount:(NSInteger)totalCount
{
    //self.autoPlayLabel.text = [NSString stringWithFormat:@"Autoplay %ld/%ld",(long)autoPlayCount,(long)totalCount];
    self.autoPlayLabel.text = [NSString stringWithFormat:@"%ld/%ld",(long)autoPlayCount,(long)totalCount];
    if (autoPlayCount > MAX_AUTOPLAY_CARD_TO_SHOW)
    {
        [self removeView];
    }
}

- (void)resetCountForAutoPlay
{
    self.autoPlayLabel.text = AUTO_PLAY_DEFAULT_TEXT;
}

- (void)removeView
{
    if (self.autoPlayDelegate && [self.autoPlayDelegate respondsToSelector:@selector(removeAutoPlayView:)])
    {
        [self.autoPlayDelegate removeAutoPlayView:self.tag];
    }
}

@end
