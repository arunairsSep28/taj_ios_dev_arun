/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMenuListView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogisha Poojary on 07/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJUtilities.h"

@protocol TAJMenuListViewDelegate<NSObject>
-(void)closeMenuButtonPressed;
-(void)standUpButtonPressed;
-(void)discardHistoryButtonPressed;
-(void)lastHandButtonPressed;
-(void)scoreBoardButtonPressed;
-(void)reportaBugButtonPressed;
//new
-(void)gameInformationPressed;

- (void)vibrateButtonPressedUserDefaults:(BOOL)boolValue;
- (void)soundButtonPressedUserDefaults:(BOOL)boolValue;


@end

@interface TAJMenuListView : UIView

@property(weak,nonatomic) id<TAJMenuListViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet UIImageView      *soundOnorOffImageView;
@property (weak, nonatomic) IBOutlet UIImageView      *soundOnImageView;
@property (weak, nonatomic) IBOutlet UIImageView      *soundOffImageView;

@property (weak, nonatomic) IBOutlet UIButton         *soundButton;
@property (weak, nonatomic) IBOutlet UIButton         *gameInformationButton;

@property (weak, nonatomic) IBOutlet UIButton         *vibrateButton;
@property (weak, nonatomic) IBOutlet UIImageView      *vibrateOnorOffImageView;
@property (weak, nonatomic) IBOutlet UIImageView      *vibrateOnImageView;
@property (weak, nonatomic) IBOutlet UIImageView      *vibrateOffImageView;

@property (weak, nonatomic) IBOutlet UIButton         *lastHand;
@property (weak, nonatomic) IBOutlet UIButton         *standUpButton;
@property (weak, nonatomic) IBOutlet UIButton         *reportABug;
@property (weak, nonatomic) IBOutlet UIButton         *closeMenu;

//new game settings view
@property (weak, nonatomic) IBOutlet UIView *viewGameSettings;

@property BOOL isSoundButtonPressed;
@property BOOL isVibrateButtonPressed;


- (IBAction)soundButtonPressed:(id)sender;
- (IBAction)vibrateButtonPressed:(id)sender;
- (void)refreshSoundButton;



@end

