/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMenuListView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogisha Poojary on 07/04/14.
 **/

#import "TAJMenuListView.h"
#import "TAJSoundHandler.h"

@implementation TAJMenuListView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        [self refreshSoundButton];
        
    }
   
    return self;
}

- (void)refreshSoundButton
{
    if([((NSString*)[TAJUtilities retrieveFromUserDefaultsForKey:USER_DEFAULT_SOUND]) boolValue])
    {
        if([TAJUtilities isIPhone])
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_on-568h@2x~iphone.png"];
            
            self.soundOnImageView.hidden = NO;
            self.soundOffImageView.hidden = YES;
        }
        else
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_on.png"];
            
            self.soundOnImageView.hidden = NO;
            self.soundOffImageView.hidden = YES;
        }
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_off-568h@2x~iphone.png"];
            
            self.soundOnImageView.hidden = YES;
            self.soundOffImageView.hidden = NO;
        }
        else
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_off.png"];
            
            self.soundOnImageView.hidden = YES;
            self.soundOffImageView.hidden = NO;
        }
        
    }
    
    if([((NSString*)[TAJUtilities retrieveFromUserDefaultsForKey:USER_DEFAULT_VIBRATE]) boolValue])
    {
        if([TAJUtilities isIPhone])
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_on-568h@2x~iphone.png"];
            
            self.vibrateOnImageView.hidden = NO;
            self.vibrateOffImageView.hidden = YES;
        }
        else
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_on.png"];
            
            self.vibrateOnImageView.hidden = NO;
            self.vibrateOffImageView.hidden = YES;
        }
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_off-568h@2x~iphone.png"];
            
            self.vibrateOnImageView.hidden = YES;
            self.vibrateOffImageView.hidden = NO;
        }
        else
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_off.png"];
            
            self.vibrateOnImageView.hidden = YES;
            self.vibrateOffImageView.hidden = NO;
        }
    }
}

- (BOOL)isMenuShowimg
{
    if (self.superview)
    {
        return YES;
    }
    return NO;
}

- (IBAction)closeMenuButton_p:(id)sender
{
    [self.delegate closeMenuButtonPressed];
}


- (IBAction)standUpButton_p:(id)sender
{
    [self.delegate standUpButtonPressed];
}

- (IBAction)discardedCardsButton_p:(id)sender
{
    [self.delegate discardHistoryButtonPressed];
}

- (IBAction)lastHandButton_p:(id)sender
{
    [self.delegate lastHandButtonPressed];
}
- (IBAction)sccoreBoardButton_p:(id)sender
{
    [self.delegate scoreBoardButtonPressed];
}

- (IBAction)reportaBugButton_p:(id)sender
{
    [self.delegate reportaBugButtonPressed];
}

- (IBAction)gameInformationButton_p:(id)sender
{
    [self.delegate gameInformationPressed];
}

- (IBAction)gameSettingsPressed:(id)sender
{
    self.viewGameSettings.hidden = NO;
}
- (IBAction)soundButtonPressed:(id)sender
{
    
    if(self.isSoundButtonPressed)
    {
        if([TAJUtilities isIPhone])
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_on-568h@2x~iphone.png"];
            self.isSoundButtonPressed = NO;
            
            self.soundOnImageView.hidden = NO;
            self.soundOffImageView.hidden = YES;
        }
        else
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_on.png"];
            self.isSoundButtonPressed = NO;
            
            self.soundOnImageView.hidden = NO;
            self.soundOffImageView.hidden = YES;
        }

        [[TAJSoundHandler sharedManager] EnableSounds];

        if (self.delegate && [self.delegate respondsToSelector:@selector(soundButtonPressedUserDefaults:)])
        {
            [self.delegate soundButtonPressedUserDefaults:YES];
        }
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_off-568h@2x~iphone.png"];
            self.isSoundButtonPressed = YES;
            
            self.soundOnImageView.hidden = YES;
            self.soundOffImageView.hidden = NO;
        }
        else
        {
            self.soundOnorOffImageView.image = [UIImage imageNamed:@"settings_sounds_off.png"];
            self.isSoundButtonPressed = YES;
            
            self.soundOnImageView.hidden = YES;
            self.soundOffImageView.hidden = NO;
        }

        [[TAJSoundHandler sharedManager] DisableSounds];

        if (self.delegate && [self.delegate respondsToSelector:@selector(soundButtonPressedUserDefaults:)])
        {
            [self.delegate soundButtonPressedUserDefaults:NO];
        }

    }
}

- (IBAction)vibrateButtonPressed:(id)sender
{
    if(self.isVibrateButtonPressed)
    {
        if([TAJUtilities isIPhone])
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_on-568h@2x~iphone.png"];
            self.isVibrateButtonPressed = NO;
            self.vibrateOnImageView.hidden = NO;
            self.vibrateOffImageView.hidden = YES;
        }
        else
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_on.png"];
            self.isVibrateButtonPressed = NO;
            
            self.vibrateOnImageView.hidden = NO;
            self.vibrateOffImageView.hidden = YES;
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(vibrateButtonPressedUserDefaults:)])
        {
            [self.delegate vibrateButtonPressedUserDefaults:YES];
        }
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_off-568h@2x~iphone.png"];
            self.isVibrateButtonPressed = YES;
            
            self.vibrateOnImageView.hidden = YES;
            self.vibrateOffImageView.hidden = NO;
            
        }
        else
        {
            self.vibrateOnorOffImageView.image = [UIImage imageNamed:@"settings_vibrate_off.png"];
            self.isVibrateButtonPressed = YES;
            
            self.vibrateOnImageView.hidden = YES;
            self.vibrateOffImageView.hidden = NO;
            
        }
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(vibrateButtonPressedUserDefaults:)])
        {
            [self.delegate vibrateButtonPressedUserDefaults:NO];
        }
    }
}
- (IBAction)gameSettingsCloseAction:(UIButton *)sender {
    self.viewGameSettings.hidden = YES;
}

@end
