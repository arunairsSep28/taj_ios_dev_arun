/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatViewCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogish Poojary on 15/04/14.
 Created by Sujit Yadawad on 27/03/14.
 **/

#import "TAJChatViewCell.h"

@interface TAJChatViewCell()

@end

@implementation TAJChatViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    CGRect frame = self.chatLabel.frame;
    
    frame.origin.x = 5;
    frame.origin.y = 5;
    frame.size.width = self.frame.size.width - 10.0f;
    frame.size.height = self.frame.size.height - 10.0f;
}

@end
