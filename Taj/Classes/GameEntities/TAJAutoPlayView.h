/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAutoPlayView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 10/05/14.
 Created by Yogisha Poojary on 06/05/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJAutoPlayViewDelegate;

@interface TAJAutoPlayView : UIView

@property(nonatomic, weak) id<TAJAutoPlayViewDelegate> autoPlayDelegate;
- (void)updateAutoplayCount:(NSInteger)autoPlayCount andTotalCount:(NSInteger)totalCount;
- (void)resetCountForAutoPlay;
@end

@protocol TAJAutoPlayViewDelegate <NSObject>

- (void)removeAutoPlayView:(NSInteger) index;

@end