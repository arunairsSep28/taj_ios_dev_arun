//
//  TAJProgressTimer.h
//  ProgressTimer
//
//  Created by Ananth T Pai on 11/08/14.
//  Copyright (c) 2014 Ananth T Pai. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TAJProgressTimeView : UIView

//Simple timer with a single colo
-(id)initProgresssTimerWithFrame:(CGRect)frame
                            time:(CGFloat)time
                          radius:(CGFloat)radius
                           color:(UIColor*)color;
//Rectangular Timer
-(id)initRectangularTimerWithFrame:(CGRect)frame
                              time:(CGFloat)time
                             color:(UIColor*)color
                       chunksTimer:(int)playerchunksTimer
                          ischunks:(BOOL)ischunks;

-(void)startTimer;
-(void)stopTimer;
-(void)resetTimer;
-(void)resumeTimer;

@end
