/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerTurnView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 15/04/14.
 **/

#import "TAJPlayerTurnView.h"
#import "TAJProgressTimeView.h"
#include "TAJUtilities.h"

@interface TAJPlayerTurnView ()

@property (strong, nonatomic) IBOutlet TAJProgressTimeView *progressTimerView;
@property (weak, nonatomic) IBOutlet UIView *progressTimerViewBg;

@end

@implementation TAJPlayerTurnView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        // set default time
        self.progressTimerView = nil;
    }
    return self;
}

- (void)startPlayerTimer:(int)playerTime turn:(BOOL)isDeviceTurn placeId:(NSInteger)placeId chunksTimer:(int)playerchunksTimer ischunks:(BOOL)ischunks
{
    NSLog(@"TURN TYPE : %i",isDeviceTurn);
    // NSLog(@"Check playerTime : %d",playerTime);
    NSLog(@"CHECK PLACE ID : %ld",(long)placeId);
    
    // green color
    UIColor *greenColor = [UIColor colorWithRed:142/255.0
                                          green:218/255.0
                                           blue:57/255.0
                                          alpha:1];
    
    if (self)
    {
        if(!isDeviceTurn)
        {
            NSLog(@"CHECK PLACE NOT DEVICE TURN");
            self.progressTimerViewBg.hidden = YES;
            if([TAJUtilities isIPhone])
            {
                int xPosition = 0;
                int yPosition = 0;
                

                if (placeId == 1) {
                    if (APP_DELEGATE.isMiddleJoin) {
                        NSLog(@"YES PLACE ID 1 MIDDLE");
                        APP_DELEGATE.isMiddleJoin = NO;
                        if ([TAJUtilities isItIPhone6]) {
                            xPosition = 104;
                        }
                        else {
                        xPosition = 116;
                        }
                        yPosition = 16;
//                        xPosition = [TAJUtilities screenHeight]/4 + 10;
//                        yPosition = 16;
                        
                    }
                    else {
                        NSLog(@"YES PLACE ID 1 NOT MIDDLE");
                        //xPosition = 80;
                        //yPosition = 16;
                        //xPosition = [TAJUtilities screenHeight]/4 + 10;
                        if ([TAJUtilities isItIPhone6]) {
                            xPosition = 104;
                        }
                        else {
                            xPosition = 116;
                        }
                        yPosition = 16;
                    }
                    
                }
                else if (placeId == 2) {
                    xPosition = -58;//34;
                    yPosition = 46;//84;
                }
                else if (placeId == 3) {
                    xPosition = -58;//64;
                    yPosition = -40;
                }
                else if (placeId == 5) {
                    //                    if (APP_DELEGATE.isMiddleJoin) {
                    //                        APP_DELEGATE.isMiddleJoin = NO;
                    //                        xPosition = 110;
                    //                        yPosition = -4;
                    //                    }
                    //                    else {
                    xPosition = -36;
                    yPosition = -40;
                    //}
                }
                else if (placeId == 4) {
                    xPosition = 8;//-4;
                    yPosition = -10;
                }
                else if (placeId == 6) {
                    xPosition = -34;//-4;
                    yPosition = 46;//84;
                }
                
                self.progressTimerView = [[TAJProgressTimeView alloc] initRectangularTimerWithFrame:CGRectMake(self.progressTimerView.frame.origin.x + xPosition, self.progressTimerView.frame.origin.y + yPosition, self.progressTimerView.frame.size.width * 0.50, self.progressTimerView.frame.size.height * 0.48) time:playerTime color:greenColor chunksTimer:playerchunksTimer ischunks:ischunks];
                NSLog(@"TIMER X POSITION : %f",self.progressTimerView.frame.origin.x + xPosition);
                /*
                 self.progressTimerView = [[TAJProgressTimeView alloc] initRectangularTimerWithFrame:CGRectMake(_progressTimerView.frame.origin.x, _progressTimerView.frame.origin.y, _progressTimerView.frame.size.width * 0.40, _progressTimerView.frame.size.height * 0.40)
                 time:playerTime
                 color:[UIColor greenColor]];
                 */
                
            }
            else
            {
                int xPosition = 0;
                int yPosition = 0;
                
                if (placeId == 1) {
                    xPosition = 20;
                    yPosition = -16;
                }
                else if (placeId == 2 ) {
                    xPosition = 40;
                    yPosition = 144;
                }
                else if (placeId == 3) {
                    xPosition = 96;
                    yPosition = -62;
                }
                else if (placeId == 4) {
                    xPosition = -8;
                    yPosition = 4;
                }
                else if (placeId == 5) {
                    xPosition = -60;
                    yPosition = -62;
                }
                else if (placeId == 6) {
                    xPosition = -8;
                    yPosition = 144;
                }
                
                 self.progressTimerView = [[TAJProgressTimeView alloc] initRectangularTimerWithFrame:CGRectMake(self.progressTimerView.frame.origin.x + xPosition, self.progressTimerView.frame.origin.y + yPosition, self.progressTimerView.frame.size.width * 0.50, self.progressTimerView.frame.size.height * 0.48) time:playerTime color:greenColor chunksTimer:playerchunksTimer ischunks:ischunks];
//                self.progressTimerView = [[TAJProgressTimeView alloc] initRectangularTimerWithFrame:CGRectMake(self.progressTimerView.frame.origin.x, self.progressTimerView.frame.origin.y + 2, self.progressTimerView.frame.size.width * 0.48, self.progressTimerView.frame.size.height * 0.48) time:playerTime color:greenColor];
            }
        }
        else
        {
            NSLog(@"CHECK PLACE DEVICE TURN");
            //  self.progressTimerViewBg.hidden = NO;
            //  self.progressTimerView.backgroundColor = [UIColor purpleColor];
            //  self.progressTimerView = [[TAJProgressTimeView alloc] initProgresssTimerWithFrame:self.progressTimerView.frame
            //                                                                                      time:playerTime
            //                                                                                    radius:self.progressTimerView.frame.size.width * 0.38
            //                                                                                     color:[UIColor greenColor]];
            self.progressTimerViewBg.hidden = YES;
            
            if([TAJUtilities isIPhone])
            {
                int position = 0;
                
                if (APP_DELEGATE.isMiddleJoinDeviceTurn) {
                    APP_DELEGATE.isMiddleJoinDeviceTurn = NO;
                    if ([TAJUtilities isItIPhone6]) {
                        position = 100;
                    }
                    else {
                        position = 110;
                        
                        //position = 80;
                        
                    }
                }
                else {
                    
                    if ([TAJUtilities isItIPhone6]) {
                        //position = 108;
                        position = -110;
                    }
                    else {
                        //position = 124;
                        position = -110;
                    }
                }
                self.progressTimerView = [[TAJProgressTimeView alloc] initRectangularTimerWithFrame:CGRectMake(_progressTimerView.frame.origin.x + position, _progressTimerView.frame.origin.y + 16, _progressTimerView.frame.size.width * 0.50, _progressTimerView.frame.size.height * 0.48) time:playerTime color:greenColor chunksTimer:playerchunksTimer ischunks:ischunks];
                
                NSLog(@"TIMER X POSITION : %f",self.progressTimerView.frame.origin.x + position);
            }
            else {
                self.progressTimerView = [[TAJProgressTimeView alloc] initRectangularTimerWithFrame:CGRectMake(_progressTimerView.frame.origin.x - 195, _progressTimerView.frame.origin.y + 10, _progressTimerView.frame.size.width * 0.50, _progressTimerView.frame.size.height * 0.48) time:playerTime color:greenColor chunksTimer:playerchunksTimer ischunks:ischunks];
            }
            
        }
        
        [self addSubview:self.progressTimerView];
        
        [self.progressTimerView startTimer];
        
#if DONT_USE_RUNLOOPCOMMONMODE
#else
        [[NSRunLoop mainRunLoop] addTimer:self.playerNSTimer forMode:NSRunLoopCommonModes];
#endif
    }
}

- (void)stopRunningTimer
{
    [self.progressTimerView stopTimer];
    self.progressTimerView = nil;
    //NSLog(@"CHECKING TIMER STOP");
}

@end
