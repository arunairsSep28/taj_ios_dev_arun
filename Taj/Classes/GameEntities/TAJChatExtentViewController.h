/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatExtentViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogish Poojary on 31/07/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJChairView.h"

@protocol TAJChatExtentViewControllerProtocol;

@interface TAJChatExtentViewController : UIViewController<UITableViewDataSource, UITableViewDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UITableView *chatExtentTableView;
@property (weak, nonatomic) IBOutlet UIButton *closeButton;
@property (weak, nonatomic) IBOutlet UITextField *chatExtentTextField;
@property (weak, nonatomic) IBOutlet UIButton *chatSendButton;
@property (nonatomic, strong) TAJPlayerModel      *playerModel;
@property (nonatomic, strong) NSMutableArray     *chatExtentHistoryDataSource;
@property (nonatomic, weak) id<TAJChatExtentViewControllerProtocol> chatExtentDelegate;
@property (nonatomic, strong) NSString      *restoringChatMessage;
- (void)updateChatPlayerModelOfChatExtentView:(TAJPlayerModel *)playerModel;
- (void)reloadChatHistoryTable:(NSMutableArray *)messageList;
- (void)currentChatTableID:(NSString *)tableID;
- (IBAction)closeChatExtentView:(UIButton *)sender;
- (IBAction)chatExtentSendButtonPressed:(UIButton *)sender;
- (void)clearChatHistory;
- (void)storeChatMessage;

@end

@protocol TAJChatExtentViewControllerProtocol <NSObject>
- (void)removeChatExtentView:(NSMutableArray*)chatMessageArray;
@end

