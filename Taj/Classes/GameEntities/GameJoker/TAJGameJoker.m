/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJGameJoker.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 02/06/14.
 **/

#import "TAJGameJoker.h"

@interface TAJGameJoker ()

@property (weak, nonatomic) IBOutlet UIImageView *bgImageView;

@end

@implementation TAJGameJoker

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (void)showJokerCardForCardID:(NSString *)jokerCardId
{
    self.layer.cornerRadius = 4.0f;
    if (jokerCardId)
    {
        if ([[self cardNumberForJoker:jokerCardId] isEqualToString:@"0"])
        {
            // if it is wild card joker
            self.bgImageView.image = [UIImage imageNamed:@"jocker_1D"];
        }
        else
        {
            NSString *imageNamed = [NSString stringWithFormat:@"jocker_%@%@",[self cardNumberForJoker:jokerCardId],[self cardSuitForJoker:jokerCardId]];
            self.bgImageView.image = [UIImage imageNamed:imageNamed];
        }
    }
    else if (jokerCardId == nil)
    {
        // if it is no joker game type
        self.bgImageView.image = [UIImage imageNamed:@"jocker_no0JO"];
    }
}

- (NSString *)cardNumberForJoker:(NSString *)cardID
{
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:cardID];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    return numberString;
}

- (NSString *)cardSuitForJoker:(NSString *)cardID
{
    // Intermediate
    NSString *suitString;
    
    NSScanner *scanner = [NSScanner scannerWithString:cardID];
    NSCharacterSet *characters = [NSCharacterSet letterCharacterSet];
    
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:characters intoString:NULL];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:characters intoString:&suitString];
    
    // Result.
    return [suitString uppercaseString];
}

- (NSString *)cardNumber:(int)cardNumber
{
    NSString *cardNumberString;
    switch (cardNumber)
    {
        case 1:
            cardNumberString = @"A";
            break;
        case 2:
            cardNumberString = @"2";
            break;
        case 3:
            cardNumberString = @"3";
            break;
        case 4:
            cardNumberString = @"4";
            break;
        case 5:
            cardNumberString = @"5";
            break;
        case 6:
            cardNumberString = @"6";
            break;
        case 7:
            cardNumberString = @"7";
            break;
        case 8:
            cardNumberString = @"8";
            break;
        case 9:
            cardNumberString = @"9";
            break;
        case 10:
            cardNumberString = @"10";
            break;
        case 11:
            cardNumberString = @"J";
            break;
        case 12:
            cardNumberString = @"Q";
            break;
        case 13:
            cardNumberString = @"K";
            break;
            
        default:
            cardNumberString = @""; // for joker card number is zero, so no card number
            break;
    }
    return cardNumberString;
}

@end
