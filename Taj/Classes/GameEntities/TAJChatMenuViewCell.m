/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatMenuViewCell.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogish Poojary on 31/07/14.
 **/

#import "TAJChatMenuViewCell.h"

@implementation TAJChatMenuViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}
- (IBAction)chatDefaultMessageButton_p:(UIButton *)sender
{
     DLog(@"touch began");
    [self.chatDefaultTextButton setSelected:YES];
    [self performSelector:@selector(revertButtonState) withObject:nil afterDelay:0.15];
    [self.chatMenuCellDelegate chatMenuButton_p:sender.tag];
}

- (void)revertButtonState
{
    [self.chatDefaultTextButton setSelected:NO];

}

@end
