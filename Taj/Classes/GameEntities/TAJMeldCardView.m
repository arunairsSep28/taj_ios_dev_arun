/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *        TAJMeldCardView.h
 * @Project:
 *        Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *        Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/02/14.
 **/

#import "TAJMeldCardView.h"
#import "TAJGameJoker.h"

#import "TAJConstants.h"
#import "TAJUtilities.h"

#define MELD_CARD_OFFSET_X_IPHONE_4           24.0f
#define MELD_CARD_OFFSET_X_IPHONE_5           28.0f
//chnaged value to
//#define MELD_CARD_OFFSET_X_IPHONE_4           24.0f
//#define MELD_CARD_OFFSET_X_IPHONE_5           28.0f
#define MELD_CARD_OFFSET_X_IPAD               36.0f
#define MELD_CARD_OFFSET_X_IPAD_EMPTY         44.0f
//#define SLOT_CARD_FACTOR_IPHONE_5        18
//#define SLOT_CARD_FACTOR_IPHONE_4        16
#define SLOT_CARD_FACTOR_IPHONE_5        18
#define SLOT_CARD_FACTOR_IPHONE_4        16
#define SLOT_CARD_FACTOR_IPAD 33

//#define WAIT_WHILE_CHECKING_CARDS             @"Please wait checking cards"
#define WAIT_WHILE_CHECKING_CARDS             @" "


@interface TAJMeldCardView() <TAJCardViewProtocol>

@property UIView *meldCardsAlignment;

@property (weak, nonatomic) UIView            *currentMeldCardView;
@property (nonatomic, strong) NSString        *jokerCardNumber;
@property (strong, nonatomic) IBOutlet UIImageView *jokerImageView;
@property (weak, nonatomic) IBOutlet UIView *jokerImageHolderView;

@property (weak, nonatomic) IBOutlet UIView *jokerViewHolder;

@property (strong, nonatomic) NSMutableArray  *meldCardsArray;
@property (strong, nonatomic) NSMutableArray  *myDeckCards;

//send card action
- (IBAction)sendCardsAction:(UIButton *)sender;
- (IBAction)sendCardsCancelAction:(UIButton *)sender;
- (IBAction)firstViewTapped:(id)sender;
- (IBAction)secondViewTapped:(id)sender;
- (IBAction)thirdViewTapped:(id)sender;
- (IBAction)fourthViewTapped:(id)sender;

//meld timer label
@property (weak, nonatomic) IBOutlet UILabel *meldMessageLabel;
@property (weak, nonatomic) IBOutlet UILabel *invalidShow;

@property (weak, nonatomic) IBOutlet UILabel *meldTimerLabel;
@property (strong, nonatomic) NSTimer        *meldNSTimer;
@property (strong, nonatomic) NSTimer        *meldContinuousNSTimer;
//time during stat of Meld timer
@property (nonatomic) double timeDuringStartTimer;
@property (nonatomic) BOOL isTimerStarted;

@property (weak, nonatomic) IBOutlet UIScrollView *firstMeldCardView;
@property (weak, nonatomic) IBOutlet UIScrollView *secondMeldCardView;
@property (weak, nonatomic) IBOutlet UIScrollView *thirdMeldCardView;
@property (weak, nonatomic) IBOutlet UIScrollView *fourthMeldCardView;
@property (weak, nonatomic) IBOutlet UIView *viewMainContainer;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *lblGameID;
@property (weak, nonatomic) IBOutlet UILabel *lblTableID;


@end

@implementation TAJMeldCardView

@synthesize meldDelegate        = _meldDelegate;
@synthesize firstMeldArray      = _firstMeldArray;
@synthesize secondMeldArray     = _secondMeldArray;
@synthesize thirdMeldArray      = _thirdMeldArray;
@synthesize fourthMeldArray     = _fourthMeldArray;
@synthesize currentMeldCardView = _currentMeldCardView;
@synthesize firstMeldCardView   = _firstMeldCardView;
@synthesize secondMeldCardView  = _secondMeldCardView;
@synthesize thirdMeldCardView   = _thirdMeldCardView;
@synthesize fourthMeldCardView  = _fourthMeldCardView;
@synthesize meldNSTimer         = _meldNSTimer;
@synthesize meldContinuousNSTimer = _meldContinuousNSTimer;
@synthesize meldCardsAlignment = _meldCardsAlignment;//ratheesh

@synthesize meldTimerLabel      = _meldTimerLabel;
@synthesize meldTimer           = _meldTimer;

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.firstMeldArray = [NSMutableArray array];
        self.secondMeldArray = [NSMutableArray array];
        self.thirdMeldArray = [NSMutableArray array];
        self.fourthMeldArray = [NSMutableArray array];
        
        self.meldCardsArray = [NSMutableArray array];
        self.selectedCards = [NSMutableArray array];
        self.meldTimer = 0;
        self.isTimeOut=NO;
        self.isTimerStarted = NO;
        self.shoudlForceProcessMeldiPhone4 = NO;
        
    }
    return self;
}

-(void)awakeFromNib
{
    [super awakeFromNib];
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"Version : %@",version];

    
    if ([TAJUtilities isItIPhonex] || [TAJUtilities isItIPhoneXSMax]) {
        CGRect Frame = self.viewMainContainer.frame;
        Frame.size.width = self.viewMainContainer.frame.size.width - 84;
        self.viewMainContainer.frame = Frame;
    }
    
    
    UITapGestureRecognizer* tap = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                          action:@selector(firstViewTapped:)];
    tap.numberOfTapsRequired = 1;
    [self.firstMeldView addGestureRecognizer:tap];
    UITapGestureRecognizer* tapSecond = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(secondViewTapped:)];
    tapSecond.numberOfTapsRequired = 1;
    
    [self.secondMeldView addGestureRecognizer:tapSecond];
    
    UITapGestureRecognizer* tapThird = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                               action:@selector(thirdViewTapped:)];
    tapThird.numberOfTapsRequired = 1;
    
    [self.thirdMeldView addGestureRecognizer:tapThird];
    
    UITapGestureRecognizer* tapFourth = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                action:@selector(fourthViewTapped:)];
    tapFourth.numberOfTapsRequired = 1;
    
    [self.fourthMeldView addGestureRecognizer:tapFourth];
    
    //    DLog(@"first meld array %d",self.firstMeldArray.count);
    
    if (self.firstMeldArray.count>0)
    {
        [self fillUpUI:self.firstMeldArray];
    }
    
    if (self.secondMeldArray.count>0)
    {
        [self fillUpUI:self.secondMeldArray];
    }
    
    if (self.thirdMeldArray.count>0)
    {
        [self fillUpUI:self.thirdMeldArray];
    }
    
    if (self.fourthMeldArray.count>0)
    {
        [self fillUpUI:self.fourthMeldArray];
    }
    
    [self.firstMeldCardView setCanCancelContentTouches:NO];
    [self.firstMeldCardView setDelaysContentTouches:NO];
    
    [self.secondMeldCardView setCanCancelContentTouches:NO];
    [self.secondMeldCardView setDelaysContentTouches:NO];
    
    [self.thirdMeldCardView setCanCancelContentTouches:NO];
    [self.thirdMeldCardView setDelaysContentTouches:NO];
    
    [self.fourthMeldCardView setCanCancelContentTouches:NO];
    [self.fourthMeldCardView setDelaysContentTouches:NO];
    //    [self.firstMeldCardView setCanCancelContentTouches:NO];
    
    self.meldContinuousNSTimer = [NSTimer scheduledTimerWithTimeInterval:0.01 target:self
                                                                selector:@selector(continuousMeldTimer:)
                                                                userInfo:nil repeats:YES];
    
    [self createMeldGroup];
}

#pragma mark - Fill UI with meld cards -

- (void)fillMeldView:(NSMutableArray *)meldCardsArray
{
    //First fill array to know which meld view to fill
    [self fillArrayOfCards:meldCardsArray];
}

- (void)setCurrentSelectedCards:(NSMutableArray *)meldCardsArray
{
    NSLog(@"CHECK MELD ARRAY");
    NSLog(@"MELD ARRAY : %@",meldCardsArray);
    //First fill array to know which meld view to fill
    [self.selectedCards removeAllObjects];
    self.selectedCards= [NSMutableArray arrayWithArray:meldCardsArray];
}

- (void)fillUpUI:(NSMutableArray *)array
{
    //    int groupCount;
    for (int i = 0; i < array.count; i++)
    {
        if ([array[i] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *cardInfo = array[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
            NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
            NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
            NSUInteger slotindex = [cardInfo[SLOT_KEY] unsignedIntegerValue];
            TAJCardView *card = [self loadCardView:cardId];
            card.placeID = index;
            card.slotID = slotindex;
            
            [self placeMeldCards:card atIndex:i inView:self.meldCardsAlignment isEmpty:NO];
            
//            if ([self.jokerCardId isEqualToString:cardNum])
//            {
//                [card setIsJoker:YES];
//            }
//            else
//            {
//                [card setIsJoker:NO];
//            }
            
        }else if ([array[i] isKindOfClass:[NSNull class]])
        {
            TAJCardView *view = [[TAJCardView alloc]init];
            [self placeMeldCards:view atIndex:i inView:self.meldCardsAlignment isEmpty:YES] ;
        }
        
    }
}

- (void)fillUpRemoveCardUI:(NSMutableArray *)array
{
    for (int i = 0; i < array.count; i++)
    {
        NSMutableDictionary *cardInfo = array[i];
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
        TAJCardView *card = [self loadCardView:cardId];
        
        // Place my meld card
        [self placeMeldCards:card atIndex:i inView:self.currentMeldCardView isEmpty:NO];
    }
}

- (void)setScore:(NSDictionary *)dictionary withGameId:(NSString *)gameId
{
    NSLog(@"MELD INFO : %@",dictionary);
    
    self.lblTableID.text = [dictionary valueForKey:@"TAJ_table_id"];
    self.lblGameID.text = gameId;
    
    NSArray *scoreList = [[dictionary objectForKey:@"results"] objectForKey:@"result"];
    
    BOOL isValid = YES;
    NSString *missing;
    
    if ([self.meldCardsArray count] > 1){
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.TAJ_type CONTAINS 'plf'"];
        
        NSArray *arr = [scoreList filteredArrayUsingPredicate:predicate];
        
        if([arr count] == 0){
            isValid = NO;
            missing = PURESEQUENCEMISSING;
        }else{
            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"SELF.TAJ_type CONTAINS 'None'"];
            NSArray *arr1 = [scoreList filteredArrayUsingPredicate:predicate];
            if([arr1 count] != 0){
                missing = SOMESETMISSING;
                isValid = NO;
            }
        }
    }else{
        isValid = NO;
        missing = PURESEQUENCEMISSING;
    }
    
    if(!isValid){
        //self.invalidShow.hidden = NO;
        self.invalidShow.hidden = YES;
        [self.invalidShow setText:[NSString stringWithFormat:@"%@ %@",INVALIDSHOW,missing]];
    }
    
    BOOL isFirstCard = NO;
    int cardCount = 0;
    int groupCount = 0;
    int cardCountInLastGroup = 0;
    long firstCardIndex = 0;
    
    for (NSUInteger i = 0; i < self.firstMeldArray.count; i++){
        if ([self.firstMeldArray[i] isKindOfClass:[NSDictionary class]]){
            
            if(!isFirstCard){
                firstCardIndex = i;
            }
            isFirstCard = YES;
            cardCount++;
            cardCountInLastGroup++;
            
            if(cardCount == 13){
                [self addScore:scoreList groupCount:groupCount firstCardIndex:firstCardIndex cardCountInLastGroup:cardCountInLastGroup cardCount:cardCount];
            }
            
        }
        else
        {
            if(isFirstCard){
                [self addScore:scoreList groupCount:groupCount firstCardIndex:firstCardIndex cardCountInLastGroup:cardCountInLastGroup cardCount:cardCount];
                cardCountInLastGroup = 0;
                groupCount++;
            }
            isFirstCard = NO;
        }
    }
}

- (void)addScore:(NSArray *)scoreList groupCount:(int)groupCount firstCardIndex:(long)firstCardIndex cardCountInLastGroup:(int)cardCountInLastGroup cardCount:(int)cardCount {
    
    NSDictionary *dict;
    NSLog(@"GROUP COUNT : %d",groupCount);
    NSLog(@"firstCardIndex : %ld",firstCardIndex);
    
    if ([self.meldCardsArray count] > 1){
        dict = scoreList[groupCount];
    }else{
        dict = scoreList;//[NSDictionary dictionaryWithObjectsAndKeys:scoreList, nil] ;
    }
    
    int score = [[dict objectForKey:@"TAJ_score"] intValue];
    NSLog(@"SCORE : %d",score);
    //if(score != 0){
    float xVal = 0,yVal = 0, width = 0, height = 0, lineHeight = 0;
    float borderWidth = 0, cornerRadius = 0, fontSize = 0;
    
    if ([TAJUtilities isIPhone]){
        int position = 0;
//        if ([TAJUtilities isItIPhone6Plus]) {
//            position = 64;
//        }
//        else {
            
            position = cardCount == 13 ? (groupCount > 0 ? 30 : 20) : 30;
       // }
        
        if([TAJUtilities isItIPhone5])
        {
            xVal = ((firstCardIndex + 1) * MELD_CARD_OFFSET_X_IPHONE_5) + 44;
        }else
        {
            xVal = ((firstCardIndex + 1) * MELD_CARD_OFFSET_X_IPHONE_4) + position;
        }
        yVal = 120;
        
        if(cardCountInLastGroup == 1){
            width = cardCountInLastGroup * 25;
        } else {
            width = ((cardCountInLastGroup - 1) * 28);
        }
        
        height = 15;
        borderWidth = 1;
        cornerRadius = 7;
        fontSize = 8;
        lineHeight = 1;
    }
    else
    {
        
        int position = ((firstCardIndex + 1) * MELD_CARD_OFFSET_X_IPAD) ;
        
        //position = cardCount == 13 ? (groupCount > 0 ? 30 : 20) : 30;
        
        xVal = cardCount == 13 ? (groupCount > 0 ? position : (position + 30)) : position;
        yVal = 200;
        
        if(cardCountInLastGroup == 1){
            width = cardCountInLastGroup * 70;
        }else{
            width = 70 + ((cardCountInLastGroup - 1) * 28);
        }
        height = 30;
        borderWidth = 2;
        cornerRadius = 14;
        fontSize = 14;
        lineHeight = 2;
    }
    
    UIView *scoreView = [[UIView alloc] initWithFrame:CGRectMake(xVal, yVal, width, height )];
    scoreView.backgroundColor = [UIColor clearColor];
    UIView *lineView;
    
    if ([TAJUtilities isIPhone]) {
    lineView = [[UIView alloc] initWithFrame:CGRectMake(0, (height/2), width, lineHeight)];
    }
    else {
        lineView = [[UIView alloc] initWithFrame:CGRectMake(2, (height/2), width, lineHeight)];
    }
    [scoreView addSubview:lineView];
    
    UILabel *scoreLbl = [[UILabel alloc] init];
    scoreLbl.text = [NSString stringWithFormat:@"%d",score];
    scoreLbl.backgroundColor = [UIColor clearColor];
    scoreLbl.textAlignment = NSTextAlignmentCenter;
    [scoreLbl setFont:[UIFont fontWithName:@"Helvetica" size:fontSize]];
    
    if ([TAJUtilities isIPhone]) {
        [scoreLbl setFrame:CGRectMake((width/2)-(height/2),16, height, height)];
    }
    else {
        [scoreLbl setFrame:CGRectMake(((width/2)-(height/2) - 14),26, height*2, height)];
    }
    
    
    UIImageView *arrowImg = [[UIImageView alloc] init];
    arrowImg.backgroundColor = [UIColor clearColor];
    [arrowImg setFrame:CGRectMake((width/2)-((height - 3)/2),height/2, height - 3, height - 3)];
    [scoreView addSubview:arrowImg];
    
//    CALayer * l1 = [scoreLbl layer];
//    [l1 setMasksToBounds:YES];
//    [l1 setCornerRadius:cornerRadius];
//    [l1 setBorderWidth:borderWidth];
//    [l1 setBorderColor:[[UIColor redColor] CGColor]];
    
    [scoreView addSubview:scoreLbl];
    
    
    [self.meldCardsAlignment addSubview:scoreView];
    //[self.currentMeldCardView addSubview:scoreView];
    
    if(score != 0) {
        lineView.backgroundColor = [UIColor redColor];
        arrowImg.image = [UIImage imageNamed:@"TR_Meld_arrow.png"];
        scoreLbl.textColor = [UIColor redColor];

    }
    else {
        lineView.backgroundColor = [UIColor colorWithRed:0/255.0f green:166.0f/255.0f blue:0/255.0f alpha:1.0f];
        arrowImg.image = [UIImage imageNamed:@"TR_Meld_green_arrow"];
        scoreLbl.textColor = [UIColor colorWithRed:0/255.0f green:166.0f/255.0f blue:0/255.0f alpha:1.0f];

    }
    
    // }
}

- (void)setupMyDeckCardsWithArray:(NSMutableArray *)array withJokerNumber:(NSString *)jokerCardID
{
    NSLog(@"RE CHECKING JOKER :%@",jokerCardID);
    if (array)
    {
    
        if(self.currentMeldCardView){
            [self removeFirstMeldCardInScrollView];
        }
        
        self.myDeckCards = [NSMutableArray arrayWithArray:array];
        [self processMeldCards];
        
        [self setJokerNumber:jokerCardID];

        
        //        // check any holder subviewview exist
        TAJGameJoker *jokerView = nil;
        if (self.jokerViewHolder.subviews && self.jokerViewHolder.subviews.count > 0)
        {
            // Already added
            NSArray *subviews = [self.jokerViewHolder subviews];
            jokerView = (TAJGameJoker *)subviews[0];
        }
        else
        {
            NSArray *nibContents;
            
            int index = 0;
            
            nibContents = [[NSBundle mainBundle] loadNibNamed:TAJGameJokerNibName //change the nib name
                                                        owner:nil
                                                      options:nil];
            jokerView = nibContents[index];
            [self.jokerViewHolder addSubview:jokerView];
        }
        [jokerView showJokerCardForCardID:jokerCardID];
        
        for (unsigned int i = 0; i < self.myDeckCards.count; i++)
        {
#if CARD_GROUPING_SLOTS
            if ([self.myDeckCards[i] isKindOfClass:[NSDictionary class]])
            {
                NSMutableDictionary *cardInfo = self.myDeckCards[i];
                [cardInfo setObject:[NSNumber numberWithUnsignedInteger:i] forKey:kTaj_Card_Place_Id];
            }
            else if ([self.myDeckCards[i] isKindOfClass:[NSNull class]])
            {
                
            }
#else
            NSMutableDictionary *cardInfo = self.myDeckCards[i];
            [cardInfo setObject:[NSNumber numberWithUnsignedInteger:i] forKey:kTaj_Card_Place_Id];
#endif
        }
        self.hidden = NO;
    }
}

- (void)setJokerNumber:(NSString *)jokerCardID
{
    NSLog(@"jokerCardID : %@",jokerCardID);
    
    NSString *numberString;
    NSScanner *scanner = [NSScanner scannerWithString:jokerCardID];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:numbers intoString:NULL];
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    self.jokerCardNumber = numberString;
    NSLog(@"SET jokerCardID : %@",self.jokerCardNumber);
}

#pragma mark - Fill array of cards -

- (void)fillArrayOfCards:(NSArray *)array
{
    self.meldCardsAlignment = [[UIView alloc] init];

    if ([self.firstMeldArray count] == 0)
    {
        
        self.firstMeldArray = [NSMutableArray arrayWithArray:array];
        /* if(self.firstMeldArray.count > 5)
         {
         if([TAJUtilities isIPhone])
         {
         self.firstMeldCardView.contentSize =  CGSizeMake(self.firstMeldCardView.frame.size.width * self.firstMeldArray.count/3, self.firstMeldCardView.frame.size.height);
         }
         else
         {
         self.firstMeldCardView.contentSize =  CGSizeMake(self.firstMeldCardView.frame.size.width *self.firstMeldArray.count/3, self.firstMeldCardView.frame.size.height);
         }
         }*/
        
        
        
        self.currentMeldCardView = self.firstMeldCardView;
        //        if(cardCountInLastGroup == 1){
        //            width = cardCountInLastGroup * 25;
        //        } else {
        //            width = 25 + ((cardCountInLastGroup - 1) * 28);
        //        }
        float width = 0;
        if ([TAJUtilities isIPhone])
        {
            width = 25 + ((self.firstMeldArray.count - 1) * 28);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 140);
        }else {
            NSLog(@"iPAD MELD");
            width = 70 + ((self.firstMeldArray.count - 1) * 34);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 250);
        }
        //self.meldCardsAlignment.backgroundColor = [UIColor yellowColor];
        [self fillUpUI:self.firstMeldArray];
        NSLog(@"1MeldArray COUNT : %lu",(unsigned long)self.firstMeldArray.count);
    }
    else if ([self.secondMeldArray count] == 0)
    {
        self.secondMeldArray = [NSMutableArray arrayWithArray:array];
        if([TAJUtilities isIPhone])
        {
            self.secondMeldCardView.contentSize =  CGSizeMake(self.secondMeldCardView.frame.size.width *self.secondMeldArray.count/3, self.secondMeldCardView.frame.size.height);
        }
        else
        {
            self.secondMeldCardView.contentSize =  CGSizeMake(self.secondMeldCardView.frame.size.width* self.secondMeldArray.count/3, self.secondMeldCardView.frame.size.height);
        }
        
        self.currentMeldCardView = self.secondMeldCardView;
        float width = 0;
        
        if ([TAJUtilities isIPhone])
        {
            width = 25 + ((self.firstMeldArray.count - 1) * 28);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 140);
        }else {
            NSLog(@"iPAD MELD");
            width = 70 + ((self.firstMeldArray.count - 1) * 34);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 250);
        }
        
        [self fillUpUI:self.secondMeldArray];
        NSLog(@"2MeldArray COUNT : %lu",(unsigned long)self.secondMeldArray.count);
    }
    else if ([self.thirdMeldArray count] == 0)
    {
        self.thirdMeldArray = [NSMutableArray arrayWithArray:array];
        if([TAJUtilities isIPhone])
        {
            self.thirdMeldCardView.contentSize =  CGSizeMake(self.thirdMeldCardView.frame.size.width  * self.thirdMeldArray.count/3, self.thirdMeldCardView.frame.size.height);
        }
        else
        {
            self.thirdMeldCardView.contentSize =  CGSizeMake(self.thirdMeldCardView.frame.size.width*self.thirdMeldArray.count/3, self.thirdMeldCardView.frame.size.height);
        }
        self.currentMeldCardView = self.thirdMeldCardView;
        float width = 0;
        if ([TAJUtilities isIPhone])
        {
            width = 25 + ((self.firstMeldArray.count - 1) * 28);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 140);
        }else {
            NSLog(@"iPAD MELD");
            width = 70 + ((self.firstMeldArray.count - 1) * 34);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 250);
        }
        [self fillUpUI:self.thirdMeldArray];
        NSLog(@"3MeldArray COUNT : %lu",(unsigned long)self.thirdMeldArray.count);
    }
    else if ([self.fourthMeldArray count] == 0)
    {
        if ([self.fourthMeldArray count] == 0)
        {
            self.fourthMeldArray = [NSMutableArray arrayWithArray:array];
        }
        else
        {
            for (int i = 0; i < array.count; i++)
            {
                [self.fourthMeldArray addObject:array[i]];
            }
        }
        if([TAJUtilities isIPhone])
        {
            self.fourthMeldCardView.contentSize =  CGSizeMake(self.fourthMeldCardView.frame.size.width * self.fourthMeldArray.count/3, self.fourthMeldCardView.frame.size.height);
        }
        else
        {
            self.fourthMeldCardView.contentSize =  CGSizeMake(self.fourthMeldCardView.frame.size.width  * self.fourthMeldArray.count/3, self.fourthMeldCardView.frame.size.height);
        }
        self.currentMeldCardView = self.fourthMeldCardView;
        float width = 0;
        if ([TAJUtilities isIPhone])
        {
            width = 25 + ((self.firstMeldArray.count - 1) * 28);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 140);
        }else {
            NSLog(@"iPAD MELD");
            width = 70 + ((self.firstMeldArray.count - 1) * 34);
            self.meldCardsAlignment.frame = CGRectMake(0, 0, width, 250);
        }
        [self fillUpUI:self.fourthMeldArray];
        NSLog(@"4MeldArray COUNT : %lu",(unsigned long)self.fourthMeldArray.count);
    }
   // self.currentMeldCardView.backgroundColor = [UIColor greenColor];
    [self.currentMeldCardView addSubview:self.meldCardsAlignment];
    self.meldCardsAlignment.center = self.currentMeldCardView.center;
    
    [self removeCardsWithArray:array];
    
    [self isSortButtonTobeEnable];
}

#pragma mark - Load TAJCardView -

- (TAJCardView *)loadCardView:(NSString *)cardID
{
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    NSArray *nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil options:nil];
    TAJCardView *card = nibContents[index];
    card.delegate = self;
    
    [card showCard:cardID isMomentary:YES];
    
    return card;
}

#pragma mark - add card to view -

- (void)placeMeldCards:(TAJCardView *)card atIndex:(NSUInteger)index inView:(UIView *)meldView isEmpty:(BOOL)isEmptyView
{
    CGRect meldFrame = meldView.frame;
    CGRect cardFrame = card.frame;
    
    if ([TAJUtilities isIPhone])
    {
        int position = 0;
//        if ([TAJUtilities isItIPhone6Plus]) {
//            position = 44;
//        }
//        else {
            position = 10;//changed ratheesh 21st sept
       // }
        
        if([TAJUtilities isItIPhone5])
        {
            cardFrame.origin.x = (index + 1) * MELD_CARD_OFFSET_X_IPHONE_5 + 44;
        } else
        {
            cardFrame.origin.x = (index + 1) * MELD_CARD_OFFSET_X_IPHONE_4  + position;
        }
    }
    else {
        cardFrame.origin.x = (index + 1) * MELD_CARD_OFFSET_X_IPAD - 30;
    }
    cardFrame.origin.y = meldFrame.size.height/2 - cardFrame.size.height/2;
    
    card.frame = cardFrame;
    
    if ([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            card.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.63f, 0.73f);
        }
        else
        {
            card.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
        }
    }
    else
    {
        card.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
    }
    
    //meldView.backgroundColor = [UIColor yellowColor];
    
    [meldView addSubview:card];
    
}

- (void)placeSlotEmpty:(UIView *)view atSlotNumber:(NSUInteger)index
{
    CGRect holderFrame = self.currentMeldCardView.frame;
    CGRect viewFrame = view.frame;
    
    if ([TAJUtilities isIPhone])
    {
        viewFrame.origin.x = (index +1) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 );
    }
    else
    {
        viewFrame.origin.x = (index +1) * SLOT_CARD_FACTOR_IPAD;
    }
    
    viewFrame.origin.y = holderFrame.size.height - viewFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);
    
    if ([TAJUtilities isIPhone])
    {
        if ([TAJUtilities isItIPhone5])
        {
            viewFrame.size.width = 70;
            viewFrame.size.height = 91;
        }
        else
        {
            viewFrame.size.width = 65;
            viewFrame.size.height = 85;
        }
    }
    else
    {
        viewFrame.size.width = 103;
        viewFrame.size.height = 143;
    }
    
    view.frame = viewFrame;
    [self.currentMeldCardView addSubview:view];
}


- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}

#pragma mark - Fill All Meld Cards Information -

- (void)fillAllMeldCards:(NSMutableArray *)array
{
    if ([array count] > 0)
    {
        //        NSLog(@"self.meldCardsArray %@", self.meldCardsArray);
        [self.meldCardsArray addObject:array];
        //        array = [NSMutableArray array];
    }
}

#pragma mark - IBOutlet Action -

- (IBAction)sendCardsCancelAction:(UIButton *)sender {
    //lingam
    if (self.meldDelegate){
        [self.meldDelegate cancelMeldAction];
    }
    // self.hidden = true;
}

-(void)createMeldGroup{
    if (![[TAJUtilities sharedUtilities] isInternetConnected])
    {
        return;
    }
    if (![[TAJUtilities sharedUtilities] isInternetConnected])
    {
        self.isTimeOut=YES;
        
    }
    self.sendCardButton.hidden = YES;
    
    if ([self.meldNSTimer isValid])
    {
        [self.meldNSTimer invalidate];
    }
    
    if ([self.meldContinuousNSTimer isValid])
    {
        [self.meldContinuousNSTimer invalidate];
    }
    
    if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(removeMeldGroupButtonOnSend)])
    {
        [self.meldDelegate removeMeldGroupButtonOnSend];
    }
    //lingam
    //[self processMeldCards];
}

- (IBAction)sendCardsAction:(UIButton *)sender
{
    [self sendCardsToController];
}

- (void)processMeldCards
{
    if (![[TAJUtilities sharedUtilities] isInternetConnected])
    {
        self.isTimeOut=YES;
    }
    self.sendCardButton.hidden = YES;
    
    self.meldTimerLabel.text = @"";
    //self.meldMessageLabel.text = WAIT_WHILE_CHECKING_CARDS;
    NSMutableArray *cardsRemaining = [NSMutableArray array];
    
    self.invalidShow.hidden = YES;
    [self.firstMeldArray removeAllObjects];
    [self.meldCardsArray removeAllObjects];
    NSArray *viewsToRemove = [self.currentMeldCardView subviews];
    for (UIView *v in viewsToRemove) {
        [v removeFromSuperview];
    }
    
    if ([self.myDeckCards count] > 0)
    {
        //when click on submit button take out the remaining card which are left in table
#if CARD_GROUPING_SLOTS
        
        //cardsRemaining = [NSMutableArray arrayWithArray:self.myDeckCards];
        
        BOOL isFirstCard = NO;
        int cardCount = 0;
        int groupCount = 0;
        
        NSMutableArray *groupArray = [NSMutableArray array];
        for (NSUInteger i = 0; i < self.myDeckCards.count; i++){
            if ([self.myDeckCards[i] isKindOfClass:[NSDictionary class]]){
                isFirstCard = YES;
                [cardsRemaining addObject: self.myDeckCards[i]];
                cardCount++;
                [groupArray addObject:self.myDeckCards[i]];
            }else{
                if(isFirstCard && cardCount < 14 && groupCount < 4){
                    
                    [self fillAllMeldCards:groupArray];
                    groupArray = [NSMutableArray array];
                    groupCount++;
                    [cardsRemaining addObject: self.myDeckCards[i]];
                    [cardsRemaining addObject: self.myDeckCards[i]];
                    isFirstCard = NO;
                }
            }
            
            if(cardCount == 13 && [groupArray count] > 0){
                [self fillAllMeldCards:groupArray];
                groupArray = [NSMutableArray array];
            }
        }
        
#else
        cardsRemaining = [NSMutableArray arrayWithArray:self.myDeckCards];
#endif
        //lingam commented
        // [self fillMeldView:cardsRemaining];
    }
    
    if ([self.firstMeldArray count]==0 || [self.secondMeldArray count]==0  || [self.thirdMeldArray count]==0  || [self.fourthMeldArray count]==0 )
    {
        [self fillMeldView:cardsRemaining];
        [cardsRemaining removeAllObjects];
    }
    
    
    // to send grouped cards create arraywithArrayOfDictionary
    /* [self fillAllMeldCards:self.firstMeldArray];
     [self fillAllMeldCards:self.secondMeldArray];
     [self fillAllMeldCards:self.thirdMeldArray];
     [self fillAllMeldCards:self.fourthMeldArray];
     if((self.firstMeldArray.count+self.secondMeldArray.count+self.thirdMeldArray.count+self.fourthMeldArray.count)<13)
     {
     [self fillAllMeldCards:cardsRemaining];
     }*/
    
    if ([self.meldDelegate respondsToSelector:@selector(didSendCheckMeldCards:)]){
        [self.meldDelegate didSendCheckMeldCards:self.meldCardsArray];
    }
    [self.myDeckCards removeAllObjects];
    
    //lingam
    // [self performSelector:@selector(sendCardsToController) withObject:nil afterDelay:0.0];
}

//- (void)createScoreView(UIButton *)sender{
//
//}

- (void)sendCardsToController
{
    self.meldTimerLabel.text = @"";
    
    //    DLog(@"meld cards while meld %@",self.meldCardsArray);
    if ([self.meldDelegate respondsToSelector:@selector(didSendMeldCards:)])
    {
        //        DLog(@"meld cards while meld inside");
        
        //        NSMutableArray *finalCards = [NSMutableArray array];
        //        for (unsigned int i = 0; i < self.meldCardsArray.count; i++)
        //        {
        //            if ([self.meldCardsArray[i] isKindOfClass:[NSDictionary class]])
        //            {
        //                [finalCards addObject:self.meldCardsArray[i]];
        //            }
        //        }
        [self.meldDelegate didSendMeldCards:self.meldCardsArray];
    }
}

- (void)didMeldTimeOut
{
    //lingam
    // [self processMeldCards];
    [self sendCardsToController];
    
    if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(removeMeldGroupButtonOnSend)])
    {
        [self.meldDelegate removeMeldGroupButtonOnSend];
        
    }
}

- (void)unHideMeldSendCardButton
{
    self.sendCardButton.hidden = NO;
}

#pragma mark - Helper Function -

- (void)removeCardsWithArray:(NSArray *)array
{
    
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < array.count; i++)
    {
        if ([array[i] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *cardInfo = array[i];
            NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
            
            [self removeCardWithCardAtPlace:index];
        }
    }
#else
    for (NSMutableDictionary *cardInfo in array)
    {
        NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
        [self removeCardWithCardAtPlace:index];
    }
#endif
}

- (void)removeCardWithCardAtPlace:(NSUInteger)placeId
{
    int tempIndex = 9999;
    for (int i = 0; i < self.myDeckCards.count; i++)
    {
#if CARD_GROUPING_SLOTS
        if ([self.myDeckCards[i] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *cardInfo = self.myDeckCards[i];
            NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
            
            if (index == placeId)
            {
                tempIndex = placeId;
                break;
            }
        }
#else
        NSMutableDictionary *cardInfo = self.myDeckCards[i];
        NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
        
        if (index == placeId)
        {
            tempIndex = placeId;
            break;
        }
#endif
    }
    if (tempIndex != 9999)
    {
        [self.myDeckCards removeObjectAtIndex:tempIndex];
        [self.myDeckCards insertObject:[NSNull null] atIndex:tempIndex];
    }
}

- (BOOL)removeCardWithCardAtPlace:(NSUInteger)placeId fromArray:(NSMutableArray *)array
{
    BOOL result = NO;
    int tempIndex = 9999;
    for (int i = 0; i < array.count; i++)
    {
        NSMutableDictionary *cardInfo = array[i];
        NSUInteger index = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
        
        if (index == placeId)
        {
            result = YES;
            tempIndex = i;
            break;
        }
    }
    if (tempIndex != 9999)
    {
        [array removeObjectAtIndex:tempIndex];
    }
    
    return result;
}

#pragma mark - Meld Timer -

- (void)updateMeldMessageLabel:(NSString *)message
{
    self.meldMessageLabel.text = message;
}

- (void)runMeldTimerwithDictionary:(NSDictionary *)meldDictionary withTimeDeduct:(float)time
{
    NSLog(@"runMeldTimerwithDictionary");
    
    if (meldDictionary)
    {
        if ([self.meldNSTimer isValid])
        {
            [self.meldNSTimer invalidate];
        }
        
        self.meldTimer = [meldDictionary[kTaj_playershow_timeout] floatValue];
        self.meldTimer = self.meldTimer - round (time);
        
        if (self.meldTimer > -5 && self.meldTimer <-2 && !self.sendCardButton.hidden)
        {
            if ([TAJUtilities isIPhone] && ![TAJUtilities  isItIPhone5])
            {
                self.shoudlForceProcessMeldiPhone4 = YES;
            }
            
        }
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
        
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)] && self.meldTimer <= 5 && self.meldTimer > -2)
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
        else if(self.meldTimer > 5 || self.meldTimer <= -5)
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
            
        }
        
        self.isTimerStarted = NO;
        self.timeDuringStartTimer = CACurrentMediaTime();
        self.meldNSTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                          selector:@selector(updateMeldTimer:)
                                                          userInfo:meldDictionary repeats:YES];
    
        
    }
}

- (void)continuousMeldTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    // DLog(@"update timer for continousMedtimer ");
#endif
    // DLog(@"update timer for continousMedtimer %f",self.meldTimer);
    if (self.meldTimer <= 5 && self.meldTimer >=-5 )
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
    }
    else
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
        }
    }
    
}

- (void)updateMeldTimer:(NSTimer *)timer
{
    [self.viewMainContainer bringSubviewToFront:self.meldMessageLabel];
    NSLog(@"updateMeldTimer");
    // DLog(@"update timer for updateMeldTimer %f",self.meldTimer);
#if ENABLE_TIMER_DBUGGING
    // DLog(@"update timer for updateMeldTimer ");
#endif
    
    NSDictionary *meldDict = nil;
    if (!self.isTimerStarted)
    {
        double newTime =(CACurrentMediaTime() - self.timeDuringStartTimer);
        
        
        if (self.meldTimer > newTime)
        {
            self.meldTimer =  self.meldTimer -round(newTime);
        }
        
        self.isTimerStarted = YES;
    }
    else
    {
        --self.meldTimer;
    }
    
    if (self.meldTimer == -2)
    {
        [self didMeldTimeOut];
    }
    
    if (self.shoudlForceProcessMeldiPhone4)
    {
        [self didMeldTimeOut];
        self.shoudlForceProcessMeldiPhone4 = NO;
    }
    
    if (self.meldTimer <= 0)
    {
        // Stop timer
        self.meldTimerLabel.text = @"";
        self.sendCardButton.hidden = YES;
    }
    else
    {
        meldDict = [timer userInfo];
        NSLog(@"Checking MELD TIMER");
        //show message
        self.meldMessageLabel.text =  [NSString stringWithFormat:@"%@ %d Seconds", MELD_PLAYER_TIMER, (int)self.meldTimer];;//MELD_PLAYER_TIMER;
        self.meldTimerLabel.text = [NSString stringWithFormat:@"%d", (int)self.meldTimer];
        self.meldTimerLabel.textColor= [ UIColor redColor];
        NSLog(@"MELD TIMER : %@",self.meldMessageLabel.text);
        //self.meldMessageLabel.backgroundColor = [UIColor redColor];
       // NSLog(@"BG COLOR : %@",self.meldTimerLabel.backgroundColor);
    }
    
    if (self.meldTimer <= 5)
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:NO];
        }
    }
    else
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
        }
        self.sendCardButton.hidden = NO;
    }
    
    if (self.meldTimer <= -5.0f)
    {
        [self.meldNSTimer invalidate];
        self.meldNSTimer = nil;
        
        [self.meldContinuousNSTimer invalidate];
        self.meldContinuousNSTimer = nil;
        
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(disableUserToEnterMeldDuringAutoplay:)])
        {
            [self.meldDelegate disableUserToEnterMeldDuringAutoplay:YES];
        }
    }
}

- (void)invalidateMeldTimer
{
    [self.meldNSTimer invalidate];
    self.meldNSTimer = nil;
    [self.meldContinuousNSTimer invalidate];
    self.meldContinuousNSTimer = nil;
}

- (void)removeCard:(TAJCardView *)card
{
    NSMutableDictionary *cardInfo = [NSMutableDictionary dictionary];
    cardInfo[kTaj_Card_Face_Key] = [card cardNumber];
    cardInfo[kTaj_Card_Suit_Key] = [card cardSuit];
    cardInfo[kTaj_Card_Place_Id] = [NSNumber numberWithUnsignedInteger:card.placeID];
    cardInfo[SLOT_KEY] = [NSNumber numberWithUnsignedInteger:card.slotID];
    [self.myDeckCards removeObjectAtIndex:card.placeID];
    [self.myDeckCards insertObject:cardInfo atIndex:card.placeID];
    
    // Remove from other arrays
    if ([self removeCardWithCardAtPlace:card.placeID fromArray:self.firstMeldArray])
    {
        [self removeFirstMeldCardInScrollView];
        if(self.firstMeldArray.count > 0)
        {
            if([TAJUtilities isIPhone])
            {
                self.firstMeldCardView.contentSize =  CGSizeMake(self.firstMeldCardView.frame.size.width * self.firstMeldArray.count/3, self.firstMeldCardView.frame.size.height);
            }
            else
            {
                self.firstMeldCardView.contentSize =  CGSizeMake(self.firstMeldCardView.frame.size.width  * self.firstMeldArray.count/3, self.firstMeldCardView.frame.size.height);
            }
            self.currentMeldCardView = self.firstMeldCardView;
            [self fillUpUI:self.firstMeldArray];
        }
        return;
    }
    
    else if ([self removeCardWithCardAtPlace:card.placeID fromArray:self.secondMeldArray])
    {
        [self removeSecondMeldCardInScrollView];
        if(self.secondMeldArray.count > 0)
        {
            if([TAJUtilities isIPhone])
            {
                self.secondMeldCardView.contentSize =  CGSizeMake(self.secondMeldCardView.frame.size.width * self.secondMeldArray.count/3, self.secondMeldCardView.frame.size.height);
            }
            else
            {
                self.secondMeldCardView.contentSize =  CGSizeMake(self.secondMeldCardView.frame.size.width  * self.secondMeldArray.count/3, self.secondMeldCardView.frame.size.height);
            }
            self.currentMeldCardView = self.secondMeldCardView;
            [self fillUpUI:self.secondMeldArray];
        }
        return;
    }
    
    else if ([self removeCardWithCardAtPlace:card.placeID fromArray:self.thirdMeldArray])
    {
        [self removeThirdMeldCardInScrollView];
        if(self.thirdMeldArray.count > 0)
        {
            if([TAJUtilities isIPhone])
            {
                self.thirdMeldCardView.contentSize =  CGSizeMake(self.thirdMeldCardView.frame.size.width * self.thirdMeldArray.count/3, self.thirdMeldCardView.frame.size.height);
            }
            else
            {
                self.thirdMeldCardView.contentSize =  CGSizeMake(self.thirdMeldCardView.frame.size.width  * self.thirdMeldArray.count/3, self.thirdMeldCardView.frame.size.height);
            }
            self.currentMeldCardView = self.thirdMeldCardView;
            [self fillUpUI:self.thirdMeldArray];
        }
        return;
    }
    
    else if ([self removeCardWithCardAtPlace:card.placeID fromArray:self.fourthMeldArray])
    {
        [self removeFourthMeldCardInScrollView];
        if(self.fourthMeldArray.count > 0)
        {
            if([TAJUtilities isIPhone])
            {
                self.fourthMeldCardView.contentSize =  CGSizeMake(self.fourthMeldCardView.frame.size.width * self.fourthMeldArray.count/3, self.fourthMeldCardView.frame.size.height);
            }
            else
            {
                self.fourthMeldCardView.contentSize =  CGSizeMake(self.fourthMeldCardView.frame.size.width  * self.fourthMeldArray.count/3, self.fourthMeldCardView.frame.size.height);
            }
            self.currentMeldCardView = self.fourthMeldCardView;
            [self fillUpUI:self.fourthMeldArray];
        }
        return;
    }
}

#pragma mark - Card Delegate methods -

- (void)didSelectCard:(TAJCardView *)card
{
    //Lingam
    return;
    
    // Send this card's place id
    if (self.sendCardButton.isHidden)
    {
        return;
    }
    if (self.isTimeOut)
    {
        return;
    }
    if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(didTapMeldCard:)])
    {
        [card removeFromSuperview];
        [self.meldDelegate didTapMeldCard:card.placeID];
        [self removeCard:card];
    }
    
    [self isSortButtonTobeEnable];
}

- (NSString *)getJokerCardNumber
{
    if (self.jokerCardNumber)
    {
        return self.jokerCardNumber;
    }
    return nil;
}

- (void)playerReconnectMeldHandler
{
    // remove first cards
    [self removeFirstMeldCardInScrollView];
    [self.firstMeldArray removeAllObjects];
    
    // remove second cards
    [self removeSecondMeldCardInScrollView];
    [self.secondMeldArray removeAllObjects];
    
    // remove third cards
    [self removeThirdMeldCardInScrollView];
    [self.thirdMeldArray removeAllObjects];
    
    // remove fourth cards
    [self removeFourthMeldCardInScrollView];
    [self.fourthMeldArray removeAllObjects];
    
    [self.meldCardsArray removeAllObjects];
    
    [self isSortButtonTobeEnable];
    
}

- (void)playerReconnectMeldHandlerForDisconnection
{
    
    // remove first cards
    [self removeFirstMeldCardInScrollView];
    [self.firstMeldArray removeAllObjects];
    
    // remove second cards
    [self removeSecondMeldCardInScrollView];
    [self.secondMeldArray removeAllObjects];
    
    // remove third cards
    [self removeThirdMeldCardInScrollView];
    [self.thirdMeldArray removeAllObjects];
    
    // remove fourth cards
    [self removeFourthMeldCardInScrollView];
    [self.fourthMeldArray removeAllObjects];
    
    [self.meldCardsArray removeAllObjects];
    
}

- (void)removeFirstMeldCardInScrollView
{
    for (TAJCardView *card in self.firstMeldCardView.subviews)
    {
        if (card==self.firstMeldView)
        {
            continue;
        }
        if (card)
        {
            [card removeFromSuperview];
        }
    }
}

- (void)removeSecondMeldCardInScrollView
{
    for (TAJCardView *card in self.secondMeldCardView.subviews)
    {
        if (card==self.secondMeldView)
        {
            continue;
        }
        if (card)
        {
            [card removeFromSuperview];
        }
    }
}

- (void)removeThirdMeldCardInScrollView
{
    for (TAJCardView *card in self.thirdMeldCardView.subviews)
    {
        if (card==self.thirdMeldView)
        {
            continue;
        }
        if (card)
        {
            [card removeFromSuperview];
        }
    }
}

- (void)removeFourthMeldCardInScrollView
{
    for (TAJCardView *card in self.fourthMeldCardView.subviews)
    {
        if (card==self.fourthMeldView)
        {
            continue;
        }
        if (card)
        {
            [card removeFromSuperview];
        }
    }
}

- (void)isSortButtonTobeEnable
{
    if (self.firstMeldArray.count > 0 || self.sendCardButton.hidden)
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(enableSortButtonForMeld:)])
        {
            [self.meldDelegate enableSortButtonForMeld:NO];
        }
    }
    else
    {
        if (self.meldDelegate && [self.meldDelegate respondsToSelector:@selector(enableSortButtonForMeld:)])
        {
            [self.meldDelegate enableSortButtonForMeld:YES];
        }
    }
}

- (IBAction)firstViewTapped:(id)sender
{
    if (self.selectedCards.count<=0 )
    {
        return;
    }
    
    if (self.firstMeldArray.count==0)
    {
        return;
    }
    
    if (self.firstMeldArray.count==0 && self.selectedCards.count<=2)
    {
        return;
    }
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.selectedCards];
    
    
    
    if ([self.firstMeldArray count]<5 && (([self.firstMeldArray count]+[self.selectedCards count])<=5))
    {
        
        for (int i = 0; i < tempSelectedCards.count; i++)
        {
            TAJCardView *card = tempSelectedCards[i];
            //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
            [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                       kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                       kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
            card.hidden = YES;
            [card unselectCard];
        }
        tempSelectedCards = nil;
        
        [self removeCardsWithArray:meldCardArray];
        
        [self.firstMeldArray addObjectsFromArray:meldCardArray];
    }
    else
    {
        return;
    }
    
    if(self.firstMeldArray.count > 5)
    {
        if([TAJUtilities isIPhone])
        {
            self.firstMeldCardView.contentSize =  CGSizeMake(self.firstMeldCardView.frame.size.width * self.firstMeldArray.count/3, self.firstMeldCardView.frame.size.height);
        }
        else
        {
            self.firstMeldCardView.contentSize =  CGSizeMake(self.firstMeldCardView.frame.size.width *self.firstMeldArray.count/3, self.firstMeldCardView.frame.size.height);
        }
    }
    self.currentMeldCardView = self.firstMeldCardView;
    [self fillUpUI:self.firstMeldArray];
    
    
}

- (IBAction)secondViewTapped:(id)sender
{
    if (self.selectedCards.count<=0 )
    {
        return;
    }
    
    if (self.secondMeldArray.count==0)
    {
        return;
    }
    if (self.secondMeldArray.count==0 && self.selectedCards.count<=2)
    {
        return;
    }
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.selectedCards];
    
    
    if ([self.secondMeldArray count]<5 && (([self.secondMeldArray count]+[self.selectedCards count])<=5))
    {
        
        for (int i = 0; i < tempSelectedCards.count; i++)
        {
            TAJCardView *card = tempSelectedCards[i];
            //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
            [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                       kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                       kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
            card.hidden = YES;
            [card unselectCard];
        }
        tempSelectedCards = nil;
        
        [self removeCardsWithArray:meldCardArray];
        
        [self.secondMeldArray addObjectsFromArray:meldCardArray];
    }
    else
    {
        return;
    }
    
    if([TAJUtilities isIPhone])
    {
        self.secondMeldCardView.contentSize =  CGSizeMake(self.secondMeldCardView.frame.size.width *self.secondMeldArray.count/3, self.secondMeldCardView.frame.size.height);
    }
    else
    {
        self.secondMeldCardView.contentSize =  CGSizeMake(self.secondMeldCardView.frame.size.width* self.secondMeldArray.count/3, self.secondMeldCardView.frame.size.height);
    }
    
    self.currentMeldCardView = self.secondMeldCardView;
    [self fillUpUI:self.secondMeldArray];
    
}

- (IBAction)thirdViewTapped:(id)sender
{
    if (self.selectedCards.count<=0)
    {
        return;
    }
    
    if (self.thirdMeldArray.count==0)
    {
        return;
    }
    
    if (self.thirdMeldArray.count==0 && self.selectedCards.count<=2)
    {
        return;
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.selectedCards];
    
    
    if ([self.thirdMeldArray count]<5 && (([self.thirdMeldArray count]+[self.selectedCards count])<=5))
    {
        
        for (int i = 0; i < tempSelectedCards.count; i++)
        {
            TAJCardView *card = tempSelectedCards[i];
            //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
            [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                       kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                       kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
            card.hidden = YES;
            [card unselectCard];
        }
        tempSelectedCards = nil;
        
        [self removeCardsWithArray:meldCardArray];
        
        [self.thirdMeldArray addObjectsFromArray:meldCardArray];
    }
    else
    {
        return;
    }
    
    if([TAJUtilities isIPhone])
    {
        self.thirdMeldCardView.contentSize =  CGSizeMake(self.thirdMeldCardView.frame.size.width  * self.thirdMeldArray.count/3, self.thirdMeldCardView.frame.size.height);
    }
    else
    {
        self.thirdMeldCardView.contentSize =  CGSizeMake(self.thirdMeldCardView.frame.size.width*self.thirdMeldArray.count/3, self.thirdMeldCardView.frame.size.height);
    }
    self.currentMeldCardView = self.thirdMeldCardView;
    [self fillUpUI:self.thirdMeldArray];
}

- (IBAction)fourthViewTapped:(id)sender
{
    if (self.selectedCards.count<=0)
    {
        return;
    }
    
    if (self.fourthMeldArray.count==0)
    {
        return;
    }
    
    if (self.fourthMeldArray.count==0 && self.selectedCards.count<=2)
    {
        return;
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.selectedCards];
    
    if ([self.fourthMeldArray count]<5 && (([self.fourthMeldArray count]+[self.selectedCards count])<=5))
    {
        for (int i = 0; i < tempSelectedCards.count; i++)
        {
            TAJCardView *card = tempSelectedCards[i];
            //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
            [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                       kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                       kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]
                                       }];
            card.hidden = YES;
            [card unselectCard];
        }
        tempSelectedCards = nil;
        
        [self removeCardsWithArray:meldCardArray];
        
        [self.fourthMeldArray addObjectsFromArray:meldCardArray];
    }
    else
    {
        return;
    }
    
    if([TAJUtilities isIPhone])
    {
        self.fourthMeldCardView.contentSize =  CGSizeMake(self.fourthMeldCardView.frame.size.width * self.fourthMeldArray.count/3, self.fourthMeldCardView.frame.size.height);
    }
    else
    {
        self.fourthMeldCardView.contentSize =  CGSizeMake(self.fourthMeldCardView.frame.size.width  * self.fourthMeldArray.count/3, self.fourthMeldCardView.frame.size.height);
    }
    self.currentMeldCardView = self.fourthMeldCardView;
    [self fillUpUI:self.fourthMeldArray];
    
}

@end
