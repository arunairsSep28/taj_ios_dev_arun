/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatExtentViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogish Poojary on 31/07/14.
 **/

#import "TAJChatExtentViewController.h"
#import "TAJGameEngine.h"
#import "TAJChatViewCell.h"
#import "TAJLobby.h"
#import "TAJUtilities.h"

#define Cell_Identifier @"chatCell"

@interface TAJChatExtentViewController ()
@property (nonatomic, strong)    NSString *      chatTableID;


@end

@implementation TAJChatExtentViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.chatExtentHistoryDataSource = [NSMutableArray array];
    
     // Do any additional setup after loading the view.
    //366,351
}
-(void)viewDidAppear:(BOOL)animated
{
    [self  addChatObservers];
    [self.chatExtentTextField performSelector:@selector(becomeFirstResponder) withObject:nil afterDelay:0];
    [self.chatExtentTextField addTarget:self
                  action:@selector(editingChanged:)
        forControlEvents:UIControlEventEditingChanged];
}
-(void)viewWillDisappear:(BOOL)animated
{
    [self removeChatObserver];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeChatExtentView:(UIButton *)sender
{
    //[self.chatExtentTextField resignFirstResponder];
    [self.chatExtentDelegate removeChatExtentView:self.chatExtentHistoryDataSource];
}

- (IBAction)chatExtentSendButtonPressed:(UIButton *)sender
{
    if((!self.chatExtentTextField.text.length) == 0 && (![self.chatExtentTextField.text isEqualToString:@"Enter your text here."]))
    {
        NSString *string = [self.chatExtentTextField text];
        [self sendChatString:string];
    }
    else
    {
       self.chatExtentTextField.text = @"Enter your text here.";
    }
    
    
}

- (void)reloadChatHistoryTable:(NSMutableArray *)messageList
{
    if(messageList.count > 0)
    {
        if(self.chatExtentHistoryDataSource > 0)
        {
            [self.chatExtentHistoryDataSource removeAllObjects];
            [self.chatExtentTableView reloadData];
        }
        self.chatExtentHistoryDataSource = [NSMutableArray arrayWithArray:messageList];
        [self.chatExtentTableView reloadData];
        if(self.chatExtentHistoryDataSource.count > 0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatExtentHistoryDataSource.count - 1 inSection:0];
            [self.chatExtentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
       
    }
    
}

- (void)updateChatPlayerModelOfChatExtentView:(TAJPlayerModel *)playerModel
{
    if (playerModel)
    {
        self.playerModel = playerModel;
    }
}

- (void)currentChatTableID:(NSString *)tableID
{
    if (tableID)
    {
        self.chatTableID = tableID;
    }
}

#pragma mark - Notification observers -

- (void)addChatObservers
{
    // chat message
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(chatMessageRecieved:)
                                                 name:SHOW_CHAT_MESSAGE_FOR_EXTENT_ALERT
                                               object:nil];
}


- (void)editingChanged:(id)sender
{
    if ([self.chatExtentTextField.text rangeOfString:@"Enter your text here."].location == 0)
    {
        NSString *temp = self.chatExtentTextField.text;
        NSString * tempString= [NSString stringWithFormat:@"Enter your text here."];
        
        NSRange firstRange = [temp rangeOfString:tempString];
        DLog(@"%d %d %@",firstRange.location+tempString.length-1,temp.length-1,self.chatExtentTextField.text);
        NSRange finalRange = NSMakeRange(firstRange.location + tempString.length , temp.length - tempString.length + 10);
        
        self.chatExtentTextField.text= [temp substringWithRange:finalRange];
        ;
        
    }
}

- (void)removeChatObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_CHAT_MESSAGE_FOR_EXTENT_ALERT object:nil];
}

- (void)chatMessageRecieved:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.chatTableID])
    {
        [self displayChatFromDictionary:dictionary];
    }
}

- (void)displayChatFromDictionary:(NSDictionary *)dictionary
{
    NSString *name = dictionary[kTaj_nickname];
    NSString *message = dictionary[kTaj_Text];
    
    NSString *string = [NSString stringWithFormat:@"%@ : %@",[name uppercaseString], message];
    
    [self.chatExtentHistoryDataSource addObject:string];
    [self.chatExtentTableView reloadData];
    if (self.chatExtentHistoryDataSource.count > 0)
    {
        NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatExtentHistoryDataSource.count-1 inSection:0];
        [self.chatExtentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
}

- (void)sendChatString:(NSString *)string
{
    if ([string length] > 0 && self.playerModel)
    {
        [[TAJGameEngine sharedGameEngine] chatMessageWithUserID:self.playerModel.playerID
                                                        tableID:self.playerModel.tableID
                                                       nickName:self.playerModel.playerName
                                                           text:string];
        
        string = [NSString stringWithFormat:@"%@ : %@",[self.playerModel.playerName uppercaseString], string];
        [self.chatExtentHistoryDataSource addObject:string];
        [self.chatExtentTableView reloadData];
        
        // Resetting data
        self.chatExtentTextField.text = @"";
        
        if (self.chatExtentHistoryDataSource.count > 0)
        {
            NSIndexPath *indexPath = [NSIndexPath indexPathForRow:self.chatExtentHistoryDataSource.count-1 inSection:0];
            [self.chatExtentTableView scrollToRowAtIndexPath:indexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
    }
}

#pragma mark - Table view data source -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.chatExtentHistoryDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    TAJChatViewCell *cell = [tableView dequeueReusableCellWithIdentifier:Cell_Identifier];
    
    if (!cell)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:@"TAJChatViewCell"
                                                       owner:nil options:nil];
        if([TAJUtilities isIPhone])
        {
          cell =  array[2];
        }
        else
        {
            cell =  array[1];
        }
    }
    cell.chatLabel.text = self.chatExtentHistoryDataSource[indexPath.row];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    int row =  indexPath.row;
    CGFloat rowHeight = 0.0f;
    
    if(self.chatExtentHistoryDataSource[row])
    {
        UIFont* font = [UIFont fontWithName:@"AvantGardeITCbyBT-Demi" size:([TAJUtilities isIPhone] ? 10.0f : 20.0f)];
        CGSize expectedLabelSize = [self.chatExtentHistoryDataSource[row] sizeWithFont:font
                                                               constrainedToSize:CGSizeMake(tableView.frame.size.width, 1000.0f)
                                                                   lineBreakMode:NSLineBreakByWordWrapping];
        rowHeight = expectedLabelSize.height;
        DLog(@"height %f", rowHeight);
        if([TAJUtilities isIPhone])
        {
            if([TAJUtilities isItIPhone5])
            {
                rowHeight = rowHeight < tableView.rowHeight ? tableView.rowHeight : rowHeight + 10.0f;
            }
            else
            {
                rowHeight = rowHeight < tableView.rowHeight ? tableView.rowHeight : rowHeight;
            }
        }
        else
        {
            rowHeight = rowHeight < tableView.rowHeight ? tableView.rowHeight : rowHeight + 20.0f;
        }
    }
    return rowHeight;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if ([self.chatExtentTextField.text isEqualToString:@""])
    {
        return YES;
    }
    [self sendChatString:self.chatExtentTextField.text];
    
    return YES;
}

- (void)clearChatHistory
{
    if(self.chatExtentHistoryDataSource.count > 0)
    {
        [self.chatExtentHistoryDataSource removeAllObjects];
        [self.chatExtentTableView reloadData];
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
  
    textField.text = @"";
}


- (void)textFieldDidEndEditing:(UITextField *)textField
{
    //textField.placeholder = @"Your Placeholdertext";
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
       return YES;
}

-(void)storeChatMessage
{
    if(self.chatExtentTextField.text.length > 0)
    {
        self.restoringChatMessage = self.chatExtentTextField.text;
    }
}
@end
