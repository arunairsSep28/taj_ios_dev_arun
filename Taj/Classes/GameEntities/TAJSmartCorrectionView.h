/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMeldCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJCardView.h"

@protocol TAJSmartCorrectionViewDelegate;

@interface TAJSmartCorrectionView : UIView

@property (strong, nonatomic) NSMutableArray *wrongMeldArray;
@property (strong, nonatomic) NSMutableArray *correctMeldArray;
@property (assign, nonatomic) BOOL isTimeOut;


@property (nonatomic) float meldTimer;

@property (weak, nonatomic) IBOutlet UIView *wrongMeldView;
@property (weak, nonatomic) IBOutlet UIView *correctMeldView;

@property (weak, nonatomic) id<TAJSmartCorrectionViewDelegate> meldDelegate;
@property (weak, nonatomic) IBOutlet UIButton *sendCardButton;
@property (assign, nonatomic) BOOL shoudlForceProcessMeldiPhone4;

/*!
 @abstract
    Run meld timer
 
 @param meldDictionary
    The dictionary which contains player details.
 
 @discussion
    The NSTimer is running for meld cards.
*/
- (void)setupMyDeckCardsWithArray:(NSDictionary *)array withJokerNumber:(NSString *)jokerCardID;

- (void)runMeldTimerwithDictionary:(NSDictionary *)meldDictionary withTimeDeduct:(float) time;
- (void)invalidateMeldTimer;
- (void)updateMeldMessageLabel:(NSString *)message;
- (void)playerReconnectMeldHandler;
- (void)playerReconnectMeldHandlerForDisconnection;
- (void)unHideMeldSendCardButton;
- (void)setCurrentSelectedCards:(NSMutableArray *)meldCardsArray;
- (void)didSelectCard:(TAJCardView *)card;
- (TAJCardView *)loadCardView:(NSString *)cardID;
@end

@protocol TAJSmartCorrectionViewDelegate <NSObject>

- (void)didQuitMeld;
- (void)didSendMeldCards:(NSMutableArray *)meldCards;
- (void)didSendCheckMeldCards:(NSMutableArray *)meldCards;
- (void)didTapMeldCard:(NSUInteger)placeId;
- (void)enableSortButtonForMeld:(BOOL)enable;
- (void)disableUserToEnterMeldDuringAutoplay:(BOOL)boolValue;
- (void)removeMeldGroupButtonOnSend;
- (void) dismissMeldScreen;
- (void) cancelMeldAction;
- (void)didAgreeSmartCards:(NSString *)agree;

@end
