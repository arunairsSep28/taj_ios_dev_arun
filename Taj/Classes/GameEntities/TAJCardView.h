/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 19/05/14.
 Created by Sujit Yadawad on 03/02/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJCardViewProtocol;

@interface TAJCardView : UIView

@property (nonatomic, weak) id <TAJCardViewProtocol> delegate;

@property (nonatomic) BOOL              isSelected;
@property (nonatomic) BOOL              isMomentaryCard;
@property (nonatomic) BOOL              isJoker;
@property (nonatomic) BOOL              isJokerFlipped;
@property (nonatomic, strong) NSString  *cardId;
@property (nonatomic, strong) NSString  *jokerCardNumber;
@property (nonatomic) NSUInteger        placeID;
@property (nonatomic) NSUInteger        slotID;

@property (nonatomic) BOOL              draggable;
@property (nonatomic) float             yOffset;

- (void)showCard:(NSString *)cardID isMomentary:(BOOL)isMomentary;
- (NSString *)cardNumber;
- (NSString *)cardSuit;
- (void)unselectCard;
- (void)unselectCardAndNotify:(BOOL)notifyDelegate;
- (void)flipCard:(BOOL)inValue;

@end

@protocol TAJCardViewProtocol <NSObject>

@optional
- (void)card:(TAJCardView *)card didChangeState:(BOOL)state;
- (NSString *)getJokerCardNumber;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)didSelectCard:(TAJCardView *)card;

@end
