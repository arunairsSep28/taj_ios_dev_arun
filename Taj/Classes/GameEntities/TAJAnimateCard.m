/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAnimateCard.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 31/03/14.
 **/

#import "TAJAnimateCard.h"

@implementation TAJAnimateCard

- (void)animateCardFromRect:(CGRect)fromRectOne toRect:(CGRect)toRectTwo
{
    __block CGRect temp = fromRectOne;
    __block CGRect fromRect;
    [UIView animateWithDuration:0.5 delay:1.0 options:UIViewAnimationOptionCurveEaseInOut animations:
    ^{
        
        temp.origin.x=toRectTwo.origin.x;
        temp.origin.y=toRectTwo.origin.y;
        
        temp.size.width=toRectTwo.size.width;
        temp.size.height=toRectTwo.size.height;
        fromRect=temp;
     } completion:^(BOOL finished) {
        
    }];
}

@end
