/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatMenuView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogisha Poojary on 07/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJMenuViewCell.h"
#import "TAJUtilities.h"

@protocol TAJChatMenuViewDelegate
-(void)closeChatMenuButtonPressed;
@end
@protocol TAJChatMenuDelegate;

@interface TAJChatMenuView : UIView<UITableViewDataSource,UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property(weak,nonatomic) id<TAJChatMenuViewDelegate> delegate;

//Author: Pradeep starts
@property (weak, nonatomic) id<TAJChatMenuDelegate> chatMenuDelegate;
//Author: Pradeep ends

@end

//Author: Pradeep starts
@protocol TAJChatMenuDelegate <NSObject>

- (void)chatMenuClosed;
- (void)preDefinedTextSelected:(NSString *)message;

@end