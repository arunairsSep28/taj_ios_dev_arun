/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMeldGroupManager.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 23/05/14.
 **/

#import "TAJMeldGroupManager.h"

@interface TAJMeldGroupManager ()

@end

@implementation TAJMeldGroupManager

static TAJMeldGroupManager *meldGroup = nil;

+ (instancetype)sharedMeldGroup
{
    if (meldGroup == nil)
    {
        meldGroup = [[TAJMeldGroupManager alloc] init];
        
    }
    return meldGroup;
}

- (instancetype)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

@end
