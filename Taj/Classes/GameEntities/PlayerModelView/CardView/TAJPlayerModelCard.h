/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerModelCard.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/05/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJPlayerModelCardProtocol;

@interface TAJPlayerModelCard : UIView

@property (nonatomic, weak) id <TAJPlayerModelCardProtocol> delegate;

@property (nonatomic) BOOL              isSelected;
@property (nonatomic) BOOL              isMomentaryCard;
@property (nonatomic) BOOL              isJoker;
@property (nonatomic) BOOL              isJokerFlipped;
@property (nonatomic, strong) NSString  *cardId;
@property (nonatomic, strong) NSString  *jokerCardNumber;
@property (nonatomic) NSUInteger        placeID;
@property (nonatomic) NSUInteger        slotID;

@property (nonatomic) BOOL              draggable;
@property (nonatomic) float             yOffset;

- (void)showCard:(NSString *)cardID isMomentary:(BOOL)isMomentary;
- (NSString *)cardNumber;
- (NSString *)cardSuit;
- (void)unselectCard;
- (void)unselectCardAndNotify:(BOOL)notifyDelegate;
- (void)flipCard:(BOOL)inValue;

@end

@protocol TAJPlayerModelCardProtocol <NSObject>

@optional
- (void)card:(TAJPlayerModelCard *)card didChangeState:(BOOL)state;
- (void)didSelectCard:(TAJPlayerModelCard *)card;
- (NSString *)getJokerCardNumber;
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card;

@end