/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerModelView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 10/11/2014.
 Modified by Lloyd Praveen Mascarenhas on 07/08/2014.
 Created by Pradeep BM on 27/05/14.
 **/

#import "TAJPlayerModelView.h"
#import "TAJPlayerModelCard.h"
#import "TAJUtilities.h"

#import "TAJConstants.h"

#define PLAYERMODEL_CARD_FACTOR_IPAD   39
#define PLAYERMODEL_CARD_FACTOR_IPHONE_5   20
#define PLAYERMODEL_CARD_FACTOR_IPHONE_4   19

#define PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE   8.0
#define PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD     18.0

#define PLAYERMODE_BUTTON_WIDTH_IPHONE   50.0
#define PLAYERMODE_BUTTON_HIEGHT_IPHONE   15.0
#define PLAYERMODE_BUTTON_WIDTH_IPAD      89.0
#define PLAYERMODE_BUTTON_HIEGHT_IPAD      29.0



#define PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPHONE    8
#define PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPAD      18

#define PLAYERMODEL_ALL_BUTTON_BG_IPHONE    @"playermode_discard_button-568h.png"
#define PLAYERMODEL_ALL_BUTTON_BG_IPAD      @"playermode_discard_button.png"

#define PLAYERMODEL_ALL_BUTTON_FONT_NAME    @"AvantGardeITCbyBT-Demi"

#define PLAYERMODEL_BUTTON_TITLE_COLOR      [UIColor blackColor]
#define MELD_GROUP_TEXT                     @"Meld Group"

@interface TAJPlayerModelView ()<TAJPlayerModelCardProtocol>

@property (nonatomic)         BOOL              isModelCardsFlipped;
@property (nonatomic)         BOOL              canShowMeldGroup;
@property (nonatomic)         BOOL              canModelDiscardCard;

@property (weak, nonatomic) IBOutlet UIButton *slideButton;

- (IBAction)slideDownPLayerModeAction:(UIButton *)sender;

@property (strong, nonatomic)   NSMutableArray  *selectedArray;
@property (strong, nonatomic)   NSString        *jokerCardId;
@property (nonatomic)   int                     noOfTableCards;

//Buttons
@property (nonatomic, strong) UIButton          *discardButton;
@property (nonatomic, strong)  UIButton         *meldCardButton;
@property (nonatomic, strong) UIButton          *groupCardsButton;

// Touch related
@property (nonatomic)         CGPoint           touchStartLocation;
@property (nonatomic)         CGRect            placingFrame;
@property (nonatomic, strong) TAJPlayerModelCard       *leftCard;
@property (nonatomic, strong) TAJPlayerModelCard       *rightCard;
@property (nonatomic)         CGRect            startRect;

//meld group cards
@property (nonatomic, strong)   NSMutableArray  *firstMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *secondMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *thirdMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *fourthMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *fifthMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *sixthMeldGroupSelectedCards;

@property (nonatomic, strong)  UIButton         *meldGroupFirstButton;
@property (nonatomic, strong)  UIButton         *meldGroupSecondButton;
@property (nonatomic, strong)  UIButton         *meldGroupThirdButton;
@property (nonatomic, strong)  UIButton         *meldGroupFourthButton;
@property (nonatomic, strong)  UIButton         *meldGroupFifthButton;
@property (nonatomic, strong)  UIButton         *meldGroupSixthButton;


@end

@implementation TAJPlayerModelView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        self.myDeckArray = [NSMutableArray array];
        self.selectedArray = [NSMutableArray array];
        self.noOfTableCards = 0;
        self.isModelCardsFlipped = NO;
        self.canShowMeldGroup = NO;
        self.canModelDiscardCard = YES;
        self.firstMeldGroupSelectedCards = [NSMutableArray array];
        self.secondMeldGroupSelectedCards = [NSMutableArray array];
        self.thirdMeldGroupSelectedCards = [NSMutableArray array];
        self.fourthMeldGroupSelectedCards = [NSMutableArray array];
        self.fifthMeldGroupSelectedCards = [NSMutableArray array];
        self.sixthMeldGroupSelectedCards = [NSMutableArray array];
        
    }
    return self;
}

- (void)setIsItMyTurn:(BOOL)isItMyTurn
{
    if (isItMyTurn != _isItMyTurn)
    {
        _isItMyTurn = isItMyTurn;
        
        // If not your turn remove discard button
        [self removeAllModelButtonsExceptGroup];
        
        if (isItMyTurn)
        {
            [self flipModelCards];
        }
    }
}

- (void)flipModelCards
{
    if (!self.isModelCardsFlipped && !self.isItMyTurn)
    {
        [self removeAllModelButtons];
        for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
        {
            if ([self isCardExistAtSlotIndex:i])
            {
                TAJPlayerModelCard *card = self.myDeckArray[i];
                [card flipCard:YES];
            }
        }

        self.isModelCardsFlipped = YES;
        self.myDeckHolder.userInteractionEnabled = NO;
    }
    else if (self.isModelCardsFlipped && !self.isItMyTurn)
    {
        for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
        {
            if ([self isCardExistAtSlotIndex:i])
            {
                TAJPlayerModelCard *card = self.myDeckArray[i];
                [card flipCard:NO];
            }
        }
        self.isModelCardsFlipped = NO;
        self.myDeckHolder.userInteractionEnabled = YES;
    }
    else if (self.isModelCardsFlipped)
    {
        for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
        {
            if ([self isCardExistAtSlotIndex:i])
            {
                TAJPlayerModelCard *card = self.myDeckArray[i];
                [card flipCard:NO];
            }
        }
        self.isModelCardsFlipped = NO;
        self.myDeckHolder.userInteractionEnabled = YES;
    }
}

#pragma mark - Helper Functions -

- (TAJPlayerModelCard *)getLastCard
{
    TAJPlayerModelCard *card = nil;
    for (int cardIndex=self.myDeckArray.count-1; cardIndex >= 0; cardIndex --)
    {
        if ([self.myDeckArray[cardIndex] isKindOfClass:[TAJPlayerModelCard class]])
        {
            card = self.myDeckArray[cardIndex];
            break;
        }
        
    }
    return card;
}


- (TAJPlayerModelCard *)cardFromMyDeckCardID:(NSString *)cardId
{
    NSString *cardIdentifier;
    TAJPlayerModelCard *cardInfo = nil;
    
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            cardInfo = self.myDeckArray[i];
            
            cardIdentifier = [[cardInfo cardId] uppercaseString];
            if ([cardIdentifier isEqualToString:[cardId uppercaseString]])
            {
                return cardInfo;
            }
        }
    }
    return cardInfo;
}

- (TAJPlayerModelCard *)cardAtSlotIndex:(NSInteger)index
{
    TAJPlayerModelCard *card = nil;
    if ([self.myDeckArray[index] isKindOfClass:[TAJPlayerModelCard class]])
    {
        card = self.myDeckArray[index];
    }
    return card;
}

- (BOOL)isCardExistAtSlotIndex:(NSInteger)index
{
    if ([self.myDeckArray[index] isKindOfClass:[TAJPlayerModelCard class]])
    {
        return YES;
    }
    return NO;
}

- (BOOL)isUnhideCardExistAtSlotIndex:(NSInteger)index
{
    if ([self.myDeckArray[index] isKindOfClass:[TAJPlayerModelCard class]])
    {
        TAJPlayerModelCard *card = self.myDeckArray[index];
        if (!card.hidden)
        {
            return YES;
        }
        return NO;
    }
    return NO;
}

- (void)removeCardSlotAtIndex:(NSInteger)index
{
    TAJPlayerModelCard *card = nil;
    card = self.myDeckArray[index];
    if ([self.myDeckArray[index] isKindOfClass:[TAJPlayerModelCard class]])
    {
        [self.myDeckArray removeObjectAtIndex:index];
    }
}

- (CGPoint)cardPositionAtSlotIndex:(NSUInteger)index
{
    int xPosition = 0;
    
    if ([self.myDeckArray[index] isKindOfClass:[TAJPlayerModelCard class]])
    {
        if ([TAJUtilities isIPhone])
        {
            xPosition = (index + 1) * ([TAJUtilities isItIPhone5] ? PLAYERMODEL_CARD_FACTOR_IPHONE_5 : PLAYERMODEL_CARD_FACTOR_IPHONE_4 );
        }
        else
        {
            xPosition = (index + 1) * PLAYERMODEL_CARD_FACTOR_IPAD;
            
        }
    }
    CGPoint newPoint = CGPointMake(xPosition, 0);
    return newPoint;
}

- (void)unhideCardIDFromMyDeck:(NSString *)cardId
{
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            TAJPlayerModelCard *card = self.myDeckArray[i];
            
            NSString* deckCardID = [[card cardId] uppercaseString];
            
            if ([[cardId uppercaseString] isEqualToString:deckCardID] && card.hidden)
            {
                card.hidden = NO;
                
                break;
            }
        }
    }
}

- (void)hideOrUnhideLastCard
{
    NSUInteger lastCardExistAt = 0;
    for (NSUInteger i = (self.myDeckArray.count - 1); i < self.myDeckArray.count; i--)
    {
        if ([self isCardExistAtSlotIndex:i])
        {
            lastCardExistAt = i;
            break;
        }
    }
    
    TAJPlayerModelCard *card = [self.myDeckArray objectAtIndex:lastCardExistAt];
    if (!card.hidden)
    {
        card.hidden = YES;
    }
    else
    {
        card.hidden = NO;
    }
}

- (void)hideCardForCardID:(NSString *)cardId
{
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            TAJPlayerModelCard *card = self.myDeckArray[i];
            NSString *deckCardID = [[card cardId] uppercaseString];
            
            if ([[cardId uppercaseString] isEqualToString:deckCardID] && !card.hidden)
            {
                card.hidden = YES;
                break;
            }
        }
    }
}

#pragma mark - Update slot cards -

- (void)updatePlayerModelCards:(NSMutableArray *)myCardDecks withJokerCardID:(NSString *)jokerCardID
{
    [self removeGroupButton];
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            TAJPlayerModelCard *card = self.myDeckArray[i];
//            DLog(@"remove card from playermode %@",card);
            [card removeFromSuperview];
        }
        else if ([self.myDeckArray[i] isKindOfClass:[UIView class]])
        {
            UIView *view = self.myDeckArray[i];
            [view removeFromSuperview];
        }
    }
    [self.myDeckArray removeAllObjects];
    [self.selectedArray removeAllObjects];

    self.jokerCardId = jokerCardID;

    self.noOfTableCards = 0;

    for (int i = 0; i < myCardDecks.count; i++)
    {
        if ([myCardDecks[i] isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *cardInfo = myCardDecks[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
            NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
            NSUInteger placeindex = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
            NSUInteger slotindex = [cardInfo[SLOT_KEY] unsignedIntegerValue];
//            DLog(@"------------------=============================-------------------place id in playermode %d ,%d",placeindex,slotindex);
            self.noOfTableCards++;

            NSArray *nibContents = [NSArray array];
            // Place new card
            int index = 0;

            if ([TAJUtilities isIPhone])
            {
                index = [TAJUtilities isItIPhone5]? 0 : 1;
            }
            nibContents = [[NSBundle mainBundle] loadNibNamed:TAJPlayerModelCardViewNibName
                                                        owner:nil
                                                      options:nil];

            TAJPlayerModelCard *card = nibContents[index];
            card.delegate = self;

            [card showCard:cardId isMomentary:NO];
            if (placeindex==0)
            {
                card.placeID = slotindex;

            }
            else
            {
                card.placeID = placeindex;

            }
            card.slotID =slotindex;

            if ([cardId isEqualToString:self.jokerCardId])
            {
                card.isJoker = YES;
            }
            card.draggable = YES;

            [self placeSlotCardsModel:card atSlotNumber:i];
            [self.myDeckArray addObject:card];
        }
        else if ([myCardDecks[i] isKindOfClass:[NSNull class]])
        {
            UIView *view = [[UIView alloc]init];
            [self placeSlotEmptyModel:view atSlotNumber:i];
            [self.myDeckArray addObject:view];
        }
    }
}

- (void)computeCardFrameOrigin:(CGRect *)cardFrame holderFrame:(const CGRect *)holderFrame index:(NSUInteger)index {
    cardFrame->origin.x = (index +1) * ([TAJUtilities isItIPhone5] ? PLAYERMODEL_CARD_FACTOR_IPHONE_5 : PLAYERMODEL_CARD_FACTOR_IPHONE_4 );
    cardFrame->origin.y =([TAJUtilities isItIPhone5] ? (holderFrame->size.height - cardFrame->size.height-15) : (holderFrame->size.height - cardFrame->size.height-10) );
}

- (void)placeSlotCardsModel:(TAJPlayerModelCard *)card atSlotNumber:(NSUInteger)index
{
    CGRect holderFrame = self.myDeckHolder.frame;
    CGRect cardFrame = card.frame;
    if ([TAJUtilities isIPhone])
    {
        [self computeCardFrameOrigin:&cardFrame holderFrame:&holderFrame index:index];
    }
    else
    {
        cardFrame.origin.x = (index+1 ) * PLAYERMODEL_CARD_FACTOR_IPAD;
        cardFrame.origin.y = (holderFrame.size.height - cardFrame.size.height)/2 +6;


    }

    card.frame = cardFrame;
    [self.myDeckHolder addSubview:card];
    DLog(@"card frame %@",card);
    
}

- (void)placeSlotEmptyModel:(UIView *)view atSlotNumber:(NSUInteger)index
{
    CGRect holderFrame = self.myDeckHolder.frame;
    CGRect viewFrame = view.frame;

    if ([TAJUtilities isIPhone])
    {
        viewFrame.origin.x = (index+1 ) * ([TAJUtilities isItIPhone5] ? PLAYERMODEL_CARD_FACTOR_IPHONE_5 : PLAYERMODEL_CARD_FACTOR_IPHONE_4 );

    }
    else
    {
        viewFrame.origin.x = (index +1) * PLAYERMODEL_CARD_FACTOR_IPAD;

    }

    viewFrame.origin.y = (holderFrame.size.height - viewFrame.size.height-10);

    if ([TAJUtilities isIPhone])
    {
        viewFrame.size.width = 10;
        viewFrame.size.height = 75;
    }
    else
    {
        viewFrame.size.width = 103;
        viewFrame.size.height = 143;
    }

    view.frame = viewFrame;
    //    view.userInteractionEnabled = YES;

    [self.myDeckHolder addSubview:view];
}

#pragma mark - Touch Delegate -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    
    self.touchStartLocation = point;
    self.startRect = card.frame;
    self.leftCard = [self leftCardToCard:card];
    self.rightCard = [self rightCardToCard:card];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    if ([TAJUtilities isIPhone])
    {
        if ((point.x  < self.myDeckHolder.frame.origin.x + (card.frame.size.width-15)) || (point.x   >= self.myDeckHolder.frame.size.width-25))
        {
            return;
        }
    }
    else
    {
        if ((point.x  < self.myDeckHolder.frame.origin.x + (card.frame.size.width-50)) || (point.x   >= self.myDeckHolder.frame.size.width-40))
        {
            return;
        }
        
    }
    
    float xDifference = self.touchStartLocation.x - point.x;
    CGPoint center = card.center;
    center.x -= xDifference;
    card.center = center;
    
    self.touchStartLocation = point;
    
    [self removeMeldGroupAllButton];
    
    if (xDifference > 0 && self.leftCard)
    {
        // Left direction
        if ([self.leftCard isKindOfClass:[TAJPlayerModelCard class]])
        {
            CGPoint leftCenter = self.leftCard.center;
            
            if ((leftCenter.x * 1.06f) >= center.x)
            {
                [self swapModelCard:card withAnotherCard:self.leftCard];
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.leftCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJPlayerModelCard *newLeftCard = [self leftCardToCard:card];
                
                [self animateCard:self.leftCard withNewRect:tempRect];
                
                [self.myDeckHolder insertSubview:self.leftCard aboveSubview:card];
                
                self.rightCard = self.leftCard;
                self.leftCard = newLeftCard;
            }
        }
        else if ([self.leftCard isKindOfClass:[UIView class]])
        {
            CGPoint leftCenter = self.leftCard.center;
            
            if ((leftCenter.x * 1.06f) >= center.x)
            {
                NSUInteger cardAindex = [self.myDeckArray indexOfObject:card];
                NSUInteger cardBindex = [self.myDeckArray indexOfObject:self.leftCard];
                
                [self.myDeckArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
                
                [self swapModelCardIndex:cardAindex withAnothCardIndex:cardBindex];
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.leftCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJPlayerModelCard *newLeftCard = [self leftCardToCard:card];
                
                [self animateCard:self.leftCard withNewRect:tempRect];
                
                [self.myDeckHolder insertSubview:self.leftCard aboveSubview:card];
                
                self.rightCard = self.leftCard;
                self.leftCard = newLeftCard;
            }
        }
    }
    else if (xDifference < 0 && self.rightCard)
    {
        // right direction
        if ([self.rightCard isKindOfClass:[TAJPlayerModelCard class]])
        {
            CGPoint rightCenter = self.rightCard.center;
            if ((rightCenter.x * 1.061f) <= center.x)
            {
                [self swapModelCard:card withAnotherCard:self.rightCard];
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.rightCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJPlayerModelCard *newRightCard = [self rightCardToCard:card];
                [self animateCard:self.rightCard withNewRect:tempRect];
                
                [self.myDeckHolder insertSubview:self.rightCard belowSubview:card];
                
                self.leftCard = self.rightCard;
                self.rightCard = newRightCard;
            }
        }
        else if ([self.rightCard isKindOfClass:[UIView class]])
        {
            CGPoint rightCenter = self.rightCard.center;
            
            if ((rightCenter.x * 1.061f) <= center.x)
            {
                NSUInteger cardAindex = [self.myDeckArray indexOfObject:card];
                NSUInteger cardBindex = [self.myDeckArray indexOfObject:self.rightCard];
                
                [self.myDeckArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
                
                [self swapModelCardIndex:cardAindex withAnothCardIndex:cardBindex];
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.rightCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJPlayerModelCard *newRightCard = [self rightCardToCard:card];
                [self animateCard:self.rightCard withNewRect:tempRect];
                
                [self.myDeckHolder insertSubview:self.rightCard belowSubview:card];
                
                self.leftCard = self.rightCard;
                self.rightCard = newRightCard;
            }
        }
    }
    [self updateDiscardModelButton];
    [self updateGroupCardsModelButton];
    [self updateMeldModelButton];
    [self updateMeldGroupModelButton];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card
{
    [self animateCard:card withNewRect:self.startRect];
    self.leftCard = nil;
    self.rightCard = nil;
    
    [self updateMeldGroupModelButton];
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(resetCardSlotsPlayerMode)])
    {
        [self.modelDelegate resetCardSlotsPlayerMode];
    }
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJPlayerModelCard *)card
{
    
}

- (void)animateCard:(TAJPlayerModelCard *)card withNewRect:(CGRect)newRect
{
    card.userInteractionEnabled = NO;
    
    CGRect oldRect = card.frame;
    newRect.origin.y = oldRect.origin.y;
    [UIView animateWithDuration:0.04f
                     animations:^
     {
         card.frame = newRect;
     }
                     completion:^(BOOL finished)
     {
         card.userInteractionEnabled = YES;
     }];
}

- (void)animateCard:(TAJPlayerModelCard *)card
{
    if (!card.isSelected)
    {
        card.userInteractionEnabled = NO;
        
        CGRect cardFrame = card.frame;
        
        float offset = card.yOffset;
        cardFrame.origin.y += offset;
        card.yOffset = 0;
        
        [UIView animateWithDuration:0.2f
                         animations:^
         {
             card.frame = cardFrame;
         }
                         completion:^(BOOL finished)
         {
             card.userInteractionEnabled = YES;
         }];
        
    }
    else
    {
        card.userInteractionEnabled = NO;
        
        CGRect cardFrame = card.frame;
        
        float offset = 0.0f;
        
        if (self.isItMyTurn && self.noOfTableCards == 14)
        {
            offset = ([TAJUtilities isIPhone] ? CARD_MOVE_UP_OFFSET_Y_IPHONE : CARD_MOVE_UP_OFFSET_Y_IPAD);
        }
        //offset = ([TAJUtilities isIPhone] ? CARD_MOVE_UP_OFFSET_Y_IPHONE : CARD_MOVE_UP_OFFSET_Y_IPAD);
        card.yOffset = offset;
        
        cardFrame.origin.y -= offset;
        
        [UIView animateWithDuration:0.2f
                         animations:^
         {
             card.frame = cardFrame;
         }
                         completion:^(BOOL finished)
         {
             card.userInteractionEnabled = YES;
         }];
    }
}

- (TAJPlayerModelCard *)leftCardToCard:(TAJPlayerModelCard *)centerCard
{
    TAJPlayerModelCard *leftcard = nil;
    if (self.myDeckArray)
    {
        NSUInteger index = [self.myDeckArray indexOfObject:centerCard];
        
        if (self.myDeckArray.count > index && index != 0)
        {
            if ([self isCardExistAtSlotIndex:index])
            {
                leftcard = [self.myDeckArray objectAtIndex:index - 1];
            }
            else
            {
                leftcard = [self.myDeckArray objectAtIndex:index - 1];
            }
        }
    }
    return leftcard;
}

- (TAJPlayerModelCard *)rightCardToCard:(TAJPlayerModelCard *)centerCard
{
    TAJPlayerModelCard *rightcard;
    
    if (self.myDeckArray)
    {
        NSUInteger index = [self.myDeckArray indexOfObject:centerCard];
        if (self.myDeckArray.count > (index + 1))
        {
            if ([self isCardExistAtSlotIndex:(index + 1)])
            {
                rightcard = [self.myDeckArray objectAtIndex:index + 1];
            }
            else
            {
                rightcard = [self.myDeckArray objectAtIndex:index + 1];
            }
        }
    }
    return rightcard;
}

- (void)swapModelCard:(TAJPlayerModelCard *)cardA withAnotherCard:(TAJPlayerModelCard *)cardB
{
    if (cardA && cardB)
    {
        NSUInteger cardAindex = [self.myDeckArray indexOfObject:cardA];
        NSUInteger cardBindex = [self.myDeckArray indexOfObject:cardB];
        
        [self.myDeckArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
        
        [self swapModelCardIndex:cardAindex withAnothCardIndex:cardBindex];
    }
}

- (void)uselectOtherCardsExceptCard:(TAJPlayerModelCard *)card
{
    for (int i = 0; i < self.selectedArray.count; i++)
    {
        TAJPlayerModelCard *otherCard = self.selectedArray[i];
        if (otherCard != card)
        {
            [otherCard unselectCard];
        }
    }
}

- (NSString *)jokerNumber
{
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.jokerCardId];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    return numberString;
}

#pragma mark - Card Delegate -

- (NSString *)getJokerCardNumber
{
    NSString *jokerCardNumber = nil;
    jokerCardNumber = [self jokerNumber];
    return jokerCardNumber;
}

- (void)card:(TAJPlayerModelCard *)card didChangeState:(BOOL)state
{
    if (state)
    {
        if (![self canModelMeldCard])
        {
            if (self.noOfTableCards == 13 || self.noOfTableCards == 14)
            {
                if (card.isSelected)
                {
                    [self.selectedArray addObject:card];
                }
                if (self.noOfTableCards == 14)
                {
                    if (self.selectedArray.count > 0 && self.selectedArray.count < 2)
                    {
                        TAJPlayerModelCard *tempCard = self.selectedArray[0];
                        tempCard.isSelected = YES;
                        [self animateCard:tempCard];
                        
                    }
                    else if (self.selectedArray.count > 0)
                    {
                        TAJPlayerModelCard *tempCard = self.selectedArray[0];
                        tempCard.isSelected = YES;
                        
                        if (self.selectedArray.count ==2 )
                        {
                            TAJPlayerModelCard *tempCard1 = self.selectedArray[0];
                            tempCard1.isSelected = NO;
                            [self animateCard:tempCard1];
                            tempCard1.isSelected =YES;
                        }
                        
                    }
                }
                
            }
            else if(self.noOfTableCards == 14)
            {
                if (self.selectedArray.count > 0)
                {
                    TAJPlayerModelCard *tempCard = self.selectedArray[0];
                    tempCard.isSelected = NO;
                    [self animateCard:tempCard];
                    
                }
                
                [self.selectedArray removeAllObjects];
                [self.selectedArray addObject:card];
            }
        }
        else if([self canModelMeldCard])
        {
            if (self.selectedArray.count < 5)
            {
                [self.selectedArray addObject:card];
            }
            else
            {
                card.isSelected = NO;
            }
        }
    }
    else
    {
        if (self.noOfTableCards <= 14)
        {
            TAJPlayerModelCard *previousCard = self.selectedArray[0];
            
            if (self.selectedArray &&
                self.selectedArray.count > 0 &&
                self.selectedArray.count > [self.selectedArray indexOfObject:card])
            {
                [self.selectedArray removeObjectAtIndex:[self.selectedArray indexOfObject:card]];
            }
            
            if (self.selectedArray.count > 1)
            {
                TAJPlayerModelCard *tempCard = self.selectedArray[0];
                tempCard.isSelected =YES;
            }
            else if(self.selectedArray.count == 1)
            {
                TAJPlayerModelCard *tempCard = self.selectedArray[0];
                tempCard.isSelected = YES;
                [self animateCard:tempCard];
            }
            else if(self.selectedArray.count == 0)
            {
                previousCard.isSelected = NO;
                [self animateCard:previousCard];
            }
        }
        else if (self.noOfTableCards == 14)
        {
            card.isSelected = YES;
            [self animateCard:card];
        }
    }
    [self updateDiscardModelButton];
    [self updateGroupCardsModelButton];
    [self updateMeldModelButton];
    [self didSelectCards:self.selectedArray];
}

- (void)didSelectCards:(NSMutableArray *)array
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(setCurrentSelectedCardsFromPlayerMode:)])
    {
        [self.modelDelegate setCurrentSelectedCardsFromPlayerMode:array];
    }
    
}
#pragma mark - Remove Buttons -

- (void)removeAllModelButtons
{
    [self removeCardDiscardButton];
    [self removeMeldCardButton];
    [self removeGroupButton];
}

- (void)removeAllModelButtonsExceptDiscard
{
    [self removeMeldCardButton];
    [self removeGroupButton];
}

- (void)removeCardDiscardButton
{
    if (self.discardButton)
    {
        // Remove discard button its no longer necessary
        [self.discardButton removeFromSuperview];
        self.discardButton = nil;
        [self canModelEnableShowButton:NO withDiscardCardInfo:nil];
    }
}

- (void)removeMeldCardButton
{
    if (self.meldCardButton)
    {
        [self.meldCardButton removeFromSuperview];
        self.meldCardButton = nil;
    }
}

- (void)removeGroupButton
{
    if (self.groupCardsButton)
    {
        [self.groupCardsButton removeFromSuperview];
        self.groupCardsButton = nil;
    }
}

- (void)removeAllModelButtonsExceptGroup
{
    [self removeCardDiscardButton];
    [self removeMeldCardButton];
}

- (void)removeAllMeldButtonsExceptGroup
{
    [self removeCardDiscardButton];
    [self removeMeldGroupAllButton];
}

- (void)removeMeldGroupAllButton
{
    [self removeFirstMeldGroupButton];
    [self removeSecondMeldGroupButton];
    [self removeThirdMeldGroupButton];
    [self removeFourthMeldGroupButton];
    [self removeFifthMeldGroupButton];
    [self removeSixthMeldGroupButton];
}

- (void)removeFirstMeldGroupButton
{
    if (self.meldGroupFirstButton)
    {
        [self.meldGroupFirstButton removeFromSuperview];
        self.meldGroupFirstButton = nil;
    }
}

- (void)removeSecondMeldGroupButton
{
    if (self.meldGroupSecondButton)
    {
        [self.meldGroupSecondButton removeFromSuperview];
        self.meldGroupSecondButton = nil;
    }
}

- (void)removeThirdMeldGroupButton
{
    if (self.meldGroupThirdButton)
    {
        [self.meldGroupThirdButton removeFromSuperview];
        self.meldGroupThirdButton = nil;
    }
}

- (void)removeFourthMeldGroupButton
{
    if (self.meldGroupFourthButton)
    {
        [self.meldGroupFourthButton removeFromSuperview];
        self.meldGroupFourthButton = nil;
    }
}

- (void)removeFifthMeldGroupButton
{
    if (self.meldGroupFifthButton)
    {
        [self.meldGroupFifthButton removeFromSuperview];
        self.meldGroupFifthButton = nil;
    }
}

- (void)removeSixthMeldGroupButton
{
    if (self.meldGroupSixthButton)
    {
        [self.meldGroupSixthButton removeFromSuperview];
        self.meldGroupSixthButton = nil;
    }
}

#pragma mark - Meld Related -

- (void)updateMeldModelButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.selectedArray &&
        (self.selectedArray.count == 3 ||
         self.selectedArray.count == 4 ||
         self.selectedArray.count == 5))
    {
        [self removeAllModelButtons];
        
        [self removeMeldGroupAllButton];
        
        //add meld button
        
        TAJPlayerModelCard *card = [self.selectedArray lastObject];
        
        CGRect cardFrame = card.frame;
        CGRect buttonFrame;
        if([TAJUtilities isIPhone])
        {
            if([TAJUtilities isItIPhone5])
            {
                buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x, self.myDeckHolder.frame.origin.y + cardFrame.origin.y-17,PLAYERMODE_BUTTON_WIDTH_IPHONE, PLAYERMODE_BUTTON_HIEGHT_IPHONE);
                
            }
            else
            {
                buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x, self.myDeckHolder.frame.origin.y + cardFrame.origin.y-13,PLAYERMODE_BUTTON_WIDTH_IPHONE, PLAYERMODE_BUTTON_HIEGHT_IPHONE);
                
            }
            
        }
        else
        {
            buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x+5, self.myDeckHolder.frame.origin.y + cardFrame.origin.y - 30.0,PLAYERMODE_BUTTON_WIDTH_IPAD, PLAYERMODE_BUTTON_HIEGHT_IPAD);
            
        }
        
        self.meldCardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldCardButton addTarget:self
                                action:@selector(meldButtonModelAction:)
                      forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldCardButton setTitle:@"Meld" forState:UIControlStateNormal];
        [self.meldCardButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldCardButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldCardButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldCardButton.backgroundColor = [UIColor clearColor];
        [self.meldCardButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldCardButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldCardButton.frame = buttonFrame;
        
        [self addSubview:self.meldCardButton];
    }
    else
    {
        if (self.meldCardButton)
        {
            [self removeMeldCardButton];
        }
    }
}

- (void)meldButtonModelAction:(UIButton *)sender
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.selectedArray];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.selectedArray removeAllObjects];
    
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    [self updateMeldGroupModelButton];
    
}

- (void)meldSetupForModel
{
    //once click on show button discard the card from selected array
    if (self.selectedArray && self.selectedArray.count == 1)
    {
        TAJPlayerModelCard *selectedCard = self.selectedArray[0];
        
        //remove my card from array
        [self.selectedArray removeAllObjects];
        
        //remove selected card from my deck
        [self removeDiscardedShowCardForMeldSetup:[self.myDeckArray indexOfObject:selectedCard]];
    }
}

- (void)removeRemainingCardsFromModelAfterSendCardFromMeld
{
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            TAJPlayerModelCard *card = self.myDeckArray[i];
            card.hidden = YES;
        }
    }
}

- (void)meldModelCancelled
{
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            TAJPlayerModelCard *card = self.myDeckArray[i];
            card.hidden = NO;
        }
    }
}

- (void)didTapModelMeldCard:(NSUInteger)placeId
{
    for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
    {
        if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
        {
            TAJPlayerModelCard *card = self.myDeckArray[i];
            if (card.placeID == placeId)
            {
                card.hidden = NO;
                break;
            }
        }
    }
    
    [self updateMeldGroupModelButton];
}

#pragma mark - Discard Related -

- (void)updateDiscardModelButton
{
    // First check the selected card array, if more than 1 dont show discard button
    if (self.noOfTableCards == 14 && self.selectedArray && self.selectedArray.count == 1 &&  self.isItMyTurn && self.canModelDiscardCard)
    {
        [self.discardButton removeFromSuperview];
        TAJPlayerModelCard *card = self.selectedArray[0];
        // First create button
        CGRect cardFrame = card.frame;
        CGRect buttonFrame;
        if([TAJUtilities isIPhone])
        {
            if([TAJUtilities isItIPhone5])
            {
                buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x, self.myDeckHolder.frame.origin.y + cardFrame.origin.y-15,PLAYERMODE_BUTTON_WIDTH_IPHONE, PLAYERMODE_BUTTON_HIEGHT_IPHONE);
                
            }
            else
            {
                buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x, self.myDeckHolder.frame.origin.y + cardFrame.origin.y-13,PLAYERMODE_BUTTON_WIDTH_IPHONE, PLAYERMODE_BUTTON_HIEGHT_IPHONE);
                
            }
            
        }
        else
        {
            buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x+5, self.myDeckHolder.frame.origin.y + cardFrame.origin.y - 27.0 , PLAYERMODE_BUTTON_WIDTH_IPAD , PLAYERMODE_BUTTON_HIEGHT_IPAD);
            
        }
        self.discardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [self.discardButton addTarget:self  action:@selector(discardButtonModelAction:)  forControlEvents:UIControlEventTouchUpInside];
        [self.discardButton setTitle:@"Discard" forState:UIControlStateNormal];
        [self.discardButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.discardButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.discardButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPAD];
        }
        [self.discardButton setBackgroundImage:newImage forState:UIControlStateNormal];
        // in case the parent view draws with a custom color or gradient, use a transparent color
        self.discardButton.backgroundColor = [UIColor clearColor];
        self.discardButton.frame = buttonFrame;
        
        [self addSubview:self.discardButton];
        
        //enable show button once click on card
        NSMutableDictionary * discardCardInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:[card cardNumber],kTaj_Card_Face_Key,
                                                 [[card cardSuit] uppercaseString],kTaj_Card_Suit_Key, nil];
        
        [self canModelEnableShowButton:YES withDiscardCardInfo:discardCardInfo];
    }
    else if (self.selectedArray &&
             self.selectedArray.count > 1 &&
             self.discardButton)
    {
        [self removeCardDiscardButton];
    }
    else
    {
        [self removeCardDiscardButton];
    }
}

- (void)automaticCardDiscardToOpenDeck
{
    [self discardButtonModelAction:self.discardButton];
}

- (void)discardButtonModelAction:(UIButton *)button
{
    button.hidden = YES;
    
    // Yet to implement discard action
    if (self.selectedArray && self.selectedArray.count == 1)
    {
        TAJPlayerModelCard *selectedCard = self.selectedArray[0];
        
        NSString *cardNumber = [selectedCard cardNumber];
        NSString *cardSuit = [selectedCard cardSuit];
        
        //remove my card from array
        [self.selectedArray removeLastObject];
        
        [self removeCardDiscardButton];
        
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNumber, [cardSuit uppercaseString]];
        
        [self didDiscardCardOnModelAtIndex:[self.myDeckArray indexOfObject:selectedCard ] withPlayerModeCard:selectedCard];
        [self performSelector:@selector(placeDiscardCardFromModelToOpenDeck:) withObject:cardId afterDelay:0.0 ];
    }
}

- (void)canModelDiscardCard:(BOOL)boolValue
{
    self.canModelDiscardCard = boolValue;
}

#pragma mark - Group Related -

- (void)updateGroupCardsModelButton
{
    if (![self canModelMeldCard] &&
        (self.noOfTableCards == 13 || self.noOfTableCards == 14 )&&
        self.selectedArray &&
        self.selectedArray.count > 1)
    {
        [self removeGroupButton];
        
        TAJPlayerModelCard *card = [self.selectedArray lastObject];
        
        // First create button
        CGRect cardFrame = card.frame;
        CGRect buttonFrame;
        if([TAJUtilities isIPhone])
        {
            buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x, self.myDeckHolder.frame.origin.y + cardFrame.origin.y-16,PLAYERMODE_BUTTON_WIDTH_IPHONE, PLAYERMODE_BUTTON_HIEGHT_IPHONE);
            
        }
        else
        {
            buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x+5, self.myDeckHolder.frame.origin.y + cardFrame.origin.y - 30.0,PLAYERMODE_BUTTON_WIDTH_IPAD, PLAYERMODE_BUTTON_HIEGHT_IPAD);
            
        }
        self.groupCardsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.groupCardsButton addTarget:self
                                  action:@selector(groupButtonModelAction:)
                        forControlEvents:UIControlEventTouchUpInside];
        
        [self.groupCardsButton setTitle:@"Group" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.groupCardsButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.groupCardsButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_GENERAL_BUTTON_FONT_SIZE_IPAD];
            
        }
        
        self.groupCardsButton.backgroundColor = [UIColor clearColor];
        [self.groupCardsButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [self.groupCardsButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.groupCardsButton.frame = buttonFrame;
        
        [self addSubview:self.groupCardsButton];
    }
    else if (![self canModelMeldCard] &&
             self.selectedArray &&
             self.selectedArray.count < 2 &&
             self.groupCardsButton)
    {
        [self removeGroupButton];
    }
    else
    {
        [self removeGroupButton];
    }
}

- (void)groupButtonModelAction:(UIButton *)button
{
    // Place cards in selected array at the beginning
    if (![self canModelMeldCard] &&
        self.selectedArray && self.selectedArray.count > 1)
    {
        [self removeGroupButton];
        // Save this to inform controller
        NSMutableArray *indexesArray = [NSMutableArray array];
        for (TAJPlayerModelCard *card in self.selectedArray)
        {
            [indexesArray addObject:[NSNumber numberWithUnsignedInteger:[self.myDeckArray indexOfObject:card]]];
            
            BOOL notifyToDelegate = NO;
            [card unselectCardAndNotify:notifyToDelegate];
            [card removeFromSuperview];
        }
        
        [self.selectedArray removeAllObjects];
        
        [self groupCardsOnModelAtIndex:indexesArray];
    }
}

#pragma mark - Model Delegates -

- (BOOL)canModelMeldCard
{
    BOOL result = NO;
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(canModelMeldCard)])
    {
        result = [self.modelDelegate canModelMeldCard];
        
    }
    return result;
}

- (void)swapModelCardIndex:(NSUInteger)cardAIndex  withAnothCardIndex:(NSUInteger)cardBIndex
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(swapModelCardIndex:withAnothCardIndex:)])
    {
        [self.modelDelegate swapModelCardIndex:cardAIndex withAnothCardIndex:cardBIndex];
    }
}

- (void)groupCardsOnModelAtIndex:(NSMutableArray *)indexArray
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(groupCardsOnModelAtIndex:)])
    {
        [self.modelDelegate groupCardsOnModelAtIndex:indexArray];
    }
}

- (void)canModelEnableShowButton:(BOOL)boolValue withDiscardCardInfo:(NSDictionary *)discardCardInfo
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(canModelEnableShowButton:withDiscardCardInfo:)])
    {
        [self.modelDelegate canModelEnableShowButton:boolValue withDiscardCardInfo:discardCardInfo];
    }
}

- (void)placeDiscardCardFromModelToOpenDeck:(NSString *)cardID
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(placeDiscardCardFromModelToOpenDeck:)])
    {
        [self.modelDelegate placeDiscardCardFromModelToOpenDeck:cardID];
    }
}

- (void)didDiscardCardOnModelAtIndex:(NSUInteger)index withPlayerModeCard:(TAJPlayerModelCard *)card
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(didDiscardCardOnModelAtIndex:withCard:)])
    {
        [self.modelDelegate didDiscardCardOnModelAtIndex:index withCard:card];
    }
}


- (void)removeDiscardedShowCardForMeldSetup:(NSUInteger)showCardIndex
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(removeDiscardedShowCardForMeldSetup:)])
    {
        [self.modelDelegate removeDiscardedShowCardForMeldSetup:showCardIndex];
    }
}

- (void)didMeldCardsOnModel:(NSMutableArray *)array
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(didMeldCardsOnModel:)])
    {
        [self.modelDelegate didMeldCardsOnModel:array];
    }
}

#pragma mark - Restart Setup -

- (void)resetPlayerModelView
{
    [self removeMyDeckModelCardsFromHolder];
    
    self.canShowMeldGroup = NO;
    
    [self removeAllModelButtons];
    [self removeMeldGroupAllButton];
    
    [self.firstMeldGroupSelectedCards removeAllObjects];
    [self.secondMeldGroupSelectedCards removeAllObjects];
    [self.thirdMeldGroupSelectedCards removeAllObjects];
    [self.fourthMeldGroupSelectedCards removeAllObjects];
    [self.fifthMeldGroupSelectedCards removeAllObjects];
    [self.sixthMeldGroupSelectedCards removeAllObjects];
    
    self.jokerCardId = nil;
    
    if (self.selectedArray)
    {
        [self.selectedArray removeAllObjects];
    }
}

- (void)removeMyDeckModelCardsFromHolder
{
    if (self.myDeckArray &&
        self.myDeckArray.count > 0)
    {
        for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
        {
            if ([self.myDeckArray[i] isKindOfClass:[TAJPlayerModelCard class]])
            {
                TAJPlayerModelCard *card = self.myDeckArray[i];
                [card removeFromSuperview];
            }
            else if ([self.myDeckArray[i] isKindOfClass:[UIView class]])
            {
                UIView *view = self.myDeckArray[i];
                [view removeFromSuperview];
            }
        }
        [self.myDeckArray removeAllObjects];
    }
}

#pragma mark - Update Meld 4 Group cards -

- (void)canShowMeldGroupForPlayerModel:(BOOL)boolValue
{
    self.canShowMeldGroup = boolValue;
}

- (void)updateMeldGroupModelButton
{
    if ([self canModelMeldCard] && self.canShowMeldGroup)
    {
        [self removeMeldGroupAllButton];
        [self removeMeldCardButton];
        
        NSUInteger indexOfFirstCardExist = 0;
        NSUInteger indexOfNullExist = 0;
        NSUInteger indexOfSecondNullExist = 0;
        NSUInteger indexOfSecondCardExist = 0;
        NSUInteger indexOfThirdNullExist = 0;
        NSUInteger indexOfThirdCardExist = 0;
        NSUInteger indexOfFourthNullExist = 0;
        NSUInteger indexOfFourthCardExist = 0;
        NSUInteger indexOfFifthNullExist = 0;
        NSUInteger indexOfFifthCardExist = 0;
        NSUInteger indexOfSixthNullExist = 0;
        NSUInteger indexOfSixthCardExist = 0;
        [self.firstMeldGroupSelectedCards removeAllObjects];
        [self.secondMeldGroupSelectedCards removeAllObjects];
        [self.thirdMeldGroupSelectedCards removeAllObjects];
        [self.fourthMeldGroupSelectedCards removeAllObjects];
        [self.fifthMeldGroupSelectedCards removeAllObjects];
        [self.sixthMeldGroupSelectedCards removeAllObjects];
        
        // For first meld group
        for (NSUInteger i = 0; i < self.myDeckArray.count; i++)
        {
            if ([self isUnhideCardExistAtSlotIndex:i])
            {
                indexOfFirstCardExist = i;
                break;
            }
        }
        
        for (NSUInteger j = indexOfFirstCardExist; j < self.myDeckArray.count; j++)
        {
            if ([self isUnhideCardExistAtSlotIndex:j])
            {
                if (self.firstMeldGroupSelectedCards.count < 12)
                {
                    [self fillFirstMeldGroupCards:self.myDeckArray[j] atIndex:j];
                    indexOfSecondNullExist = j;
                }
                else
                {
                    self.meldGroupFirstButton.hidden = YES;
                    indexOfSecondNullExist = j;
                    break;
                }
            }
            else if(![self isUnhideCardExistAtSlotIndex:j])
            {
                indexOfSecondNullExist = j;
                break;
            }
        }
        
        // for second meld group
        if (indexOfSecondNullExist != 0)
        {
            for (NSUInteger i = indexOfSecondNullExist; i < self.myDeckArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfSecondCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfSecondCardExist; k < self.myDeckArray.count && indexOfSecondCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.secondMeldGroupSelectedCards.count < 12)
                    {
                        [self fillSecondMeldGroupCards:self.myDeckArray[k] atIndex:k];
                        indexOfThirdNullExist = k;
                    }
                    else
                    {
                        indexOfThirdNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfThirdNullExist = k;
                    break;
                }
            }
        }
        
        // for third meld group
        if (indexOfThirdNullExist != 0)
        {
            for (NSUInteger i = indexOfThirdNullExist; i < self.myDeckArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfThirdCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfThirdCardExist; k < self.myDeckArray.count && indexOfThirdCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.thirdMeldGroupSelectedCards.count < 12)
                    {
                        [self fillThirdMeldGroupCards:self.myDeckArray[k] atIndex:k];
                        indexOfFourthNullExist = k;
                    }
                    else
                    {
                        indexOfFourthNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfFourthNullExist = k;
                    break;
                }
            }
        }
        
        // for fourth meld group
        if (indexOfFourthNullExist != 0)
        {
            for (NSUInteger i = indexOfFourthNullExist; i < self.myDeckArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfFourthCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfFourthCardExist; k < self.myDeckArray.count && indexOfFourthCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.fourthMeldGroupSelectedCards.count < 12)
                    {
                        [self fillFouthMeldGroupCards:self.myDeckArray[k] atIndex:k];
                        indexOfFifthNullExist = k;
                    }
                    else
                    {
                        indexOfNullExist = k;
                        indexOfFifthNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfNullExist = k;
                    indexOfFifthNullExist = k;
                    break;
                }
            }
        }
        
        // for fifth meld group
        if (indexOfFifthNullExist != 0 && indexOfFourthNullExist < indexOfFifthNullExist)
        {
            for (NSUInteger i = indexOfFifthNullExist; i < self.myDeckArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfFifthCardExist = i;
                    break;
                }
            }
            
            for (NSUInteger k = indexOfFifthCardExist; k < self.myDeckArray.count && indexOfFifthCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.fifthMeldGroupSelectedCards.count < 12)
                    {
                        [self fillFifthMeldGroupCards:self.myDeckArray[k] atIndex:k];
                        indexOfSixthNullExist = k;
                    }
                    else
                    {
                        indexOfNullExist = k;
                        indexOfSixthNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfNullExist = k;
                    indexOfSixthNullExist = k;
                    break;
                }
            }
        }
        
        // for sixth meld group
        if (indexOfSixthNullExist != 0 && indexOfFifthNullExist < indexOfSixthNullExist)
        {
            for (NSUInteger i = indexOfSixthNullExist; i < self.myDeckArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfSixthCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfSixthCardExist; k < self.myDeckArray.count && indexOfSixthCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.sixthMeldGroupSelectedCards.count < 12)
                    {
                        [self fillSixthMeldGroupCards:self.myDeckArray[k] atIndex:k];
                        indexOfNullExist = k;
                    }
                    else
                    {
                        indexOfNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfNullExist = k;
                    break;
                }
            }
        }
    }
}

- (void)fillFirstMeldGroupCards:(TAJPlayerModelCard *)card atIndex:(NSUInteger)index
{
    [self.firstMeldGroupSelectedCards addObject:card];
    if (self.firstMeldGroupSelectedCards.count <= 12)
    {
        [self updateFirstMeldGroupButton];
    }
}

- (void)fillSecondMeldGroupCards:(TAJPlayerModelCard *)card atIndex:(NSUInteger)index
{
    [self.secondMeldGroupSelectedCards addObject:card];
    [self updateSecondMeldGroupButton];
}

- (void)fillThirdMeldGroupCards:(TAJPlayerModelCard *)card atIndex:(NSUInteger)index
{
    [self.thirdMeldGroupSelectedCards addObject:card];
    [self updateThirdMeldGroupButton];
}

- (void)fillFouthMeldGroupCards:(TAJPlayerModelCard *)card atIndex:(NSUInteger)index
{
    [self.fourthMeldGroupSelectedCards addObject:card];
    [self updateFourthMeldGroupButton];
}

- (void)fillFifthMeldGroupCards:(TAJPlayerModelCard *)card atIndex:(NSUInteger)index
{
    [self.fifthMeldGroupSelectedCards addObject:card];
    [self updateFifthMeldGroupButton];
}

- (void)fillSixthMeldGroupCards:(TAJPlayerModelCard *)card atIndex:(NSUInteger)index
{
    [self.sixthMeldGroupSelectedCards addObject:card];
    [self updateSixthMeldGroupButton];
}

- (void)updateFirstMeldGroupButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.firstMeldGroupSelectedCards &&
        (self.firstMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllModelButtons];
        
        [self removeFirstMeldGroupButton];
        //add Meld Group button
        
        TAJPlayerModelCard *card = [self.firstMeldGroupSelectedCards objectAtIndex:(self.firstMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        float position = 33.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 16.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        ([TAJUtilities isIPhone] ? 60 : 118), [TAJUtilities isIPhone] ? 15 : 33);
        self.meldGroupFirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldGroupFirstButton addTarget:self
                                      action:@selector(meldGroupFirstButtonModelAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupFirstButton setTitle:MELD_GROUP_TEXT forState:UIControlStateNormal];
        [self.meldGroupFirstButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldGroupFirstButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldGroupFirstButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD];
        }
        self.meldGroupFirstButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupFirstButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldGroupFirstButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupFirstButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupFirstButton];
        DLog(@"first meld button %@",self.meldGroupFirstButton);
    }
    else
    {
        [self removeFirstMeldGroupButton];
    }
}

- (void)updateSecondMeldGroupButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.secondMeldGroupSelectedCards &&
        (self.secondMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllModelButtons];
        [self removeSecondMeldGroupButton];
        
        //add Meld Group button
        
        TAJPlayerModelCard *card = [self.secondMeldGroupSelectedCards objectAtIndex:(self.secondMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        float position = 33.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 16.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        ([TAJUtilities isIPhone] ? 60: 118), [TAJUtilities isIPhone] ? 15 : 33);
        self.meldGroupSecondButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldGroupSecondButton addTarget:self
                                       action:@selector(meldGroupSecondButtonModelAction:)
                             forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupSecondButton setTitle:MELD_GROUP_TEXT forState:UIControlStateNormal];
        [self.meldGroupSecondButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldGroupSecondButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldGroupSecondButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD];
        }
        self.meldGroupSecondButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupSecondButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldGroupSecondButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupSecondButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupSecondButton];
        DLog(@"second meld button %@",self.meldGroupSecondButton);
        
    }
    else
    {
        [self removeSecondMeldGroupButton];
    }
}

- (void)updateThirdMeldGroupButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.thirdMeldGroupSelectedCards &&
        (self.thirdMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllModelButtons];
        [self removeThirdMeldGroupButton];
        
        //add Meld Group button
        //        DLog(@"third meld card %@",self.thirdMeldGroupSelectedCards);
        
        TAJPlayerModelCard *card = [self.thirdMeldGroupSelectedCards objectAtIndex:(self.thirdMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        float position = 33.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 16.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        ([TAJUtilities isIPhone] ? 60 : 118), [TAJUtilities isIPhone] ? 15 : 33);
        self.meldGroupThirdButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldGroupThirdButton addTarget:self
                                      action:@selector(meldGroupThirdButtonModelAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupThirdButton setTitle:MELD_GROUP_TEXT forState:UIControlStateNormal];
        [self.meldGroupThirdButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldGroupThirdButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldGroupThirdButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD];
        }
        self.meldGroupThirdButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupThirdButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldGroupThirdButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupThirdButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupThirdButton];
        DLog(@"third meld button %@",self.meldGroupThirdButton);
        
    }
    else
    {
        [self removeThirdMeldGroupButton];
    }
}

- (void)updateFourthMeldGroupButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.fourthMeldGroupSelectedCards &&
        (self.fourthMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllModelButtons];
        [self removeFourthMeldGroupButton];
        
        //add Meld Group button
        
        TAJPlayerModelCard *card = [self.fourthMeldGroupSelectedCards objectAtIndex:(self.fourthMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        float position = 33.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 16.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        ([TAJUtilities isIPhone] ? 60 : 118), [TAJUtilities isIPhone] ? 15 : 33);
        self.meldGroupFourthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldGroupFourthButton addTarget:self
                                       action:@selector(meldGroupFourthButtonModelAction:)
                             forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupFourthButton setTitle:MELD_GROUP_TEXT forState:UIControlStateNormal];
        [self.meldGroupFourthButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldGroupFourthButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldGroupFourthButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD];
        }
        self.meldGroupFourthButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupFourthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldGroupFourthButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupFourthButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupFourthButton];
        DLog(@"fourth meld button %@",self.meldGroupFourthButton);
        
    }
    else
    {
        [self removeFourthMeldGroupButton];
    }
}

- (void)updateFifthMeldGroupButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.fifthMeldGroupSelectedCards &&
        (self.fifthMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllModelButtons];
        [self removeFifthMeldGroupButton];
        
        //add Meld Group button
        TAJPlayerModelCard *card = [self.fifthMeldGroupSelectedCards objectAtIndex:(self.fifthMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        float position = 33.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 16.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        ([TAJUtilities isIPhone] ? 60 : 118), [TAJUtilities isIPhone] ? 15 : 33);
        self.meldGroupFifthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldGroupFifthButton addTarget:self
                                      action:@selector(meldGroupFifthButtonModelAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupFifthButton setTitle:MELD_GROUP_TEXT forState:UIControlStateNormal];
        [self.meldGroupFifthButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldGroupFifthButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldGroupFifthButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD];
        }
        self.meldGroupFifthButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupFifthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldGroupFifthButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupFifthButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupFifthButton];
        DLog(@"fifth meld button %@",self.meldGroupFifthButton);
        
    }
    else
    {
        [self removeFifthMeldGroupButton];
    }
}

- (void)updateSixthMeldGroupButton
{
    if ([self canModelMeldCard] &&
        self.myDeckArray && self.noOfTableCards == 13 &&
        self.sixthMeldGroupSelectedCards &&
        (self.sixthMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllModelButtons];
        [self removeSixthMeldGroupButton];
        
        //add Meld Group button
        //        DLog(@"sixth meld card %@",self.sixthMeldGroupSelectedCards);
        
        TAJPlayerModelCard *card = [self.sixthMeldGroupSelectedCards objectAtIndex:(self.sixthMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        float position = 33.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 16.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        ([TAJUtilities isIPhone] ? 60 : 118), [TAJUtilities isIPhone] ? 15 : 33);
        self.meldGroupSixthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        
        [self.meldGroupSixthButton addTarget:self
                                      action:@selector(meldGroupSixthButtonModelAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupSixthButton setTitle:MELD_GROUP_TEXT forState:UIControlStateNormal];
        [self.meldGroupSixthButton setTitleColor:PLAYERMODEL_BUTTON_TITLE_COLOR forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPHONE];
            self.meldGroupSixthButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAYERMODEL_ALL_BUTTON_BG_IPAD];
            self.meldGroupSixthButton.titleLabel.font = [UIFont fontWithName:PLAYERMODEL_ALL_BUTTON_FONT_NAME size:PLAYERMODEL_MELD_GROUP_FONT_SIZE_IPAD];
        }
        self.meldGroupSixthButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupSixthButton setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        
        [self.meldGroupSixthButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupSixthButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupSixthButton];
    }
    else
    {
        [self removeSixthMeldGroupButton];
    }
}

- (void)meldGroupFirstButtonModelAction:(UIButton *)button
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    DLog(@"first meld group in player mode,%@",self.firstMeldGroupSelectedCards);
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.firstMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        card.hidden = YES;
        DLog(@"firstd meld group hidden %@,%d",card,card.hidden);
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.firstMeldGroupSelectedCards removeAllObjects];
    
    [self removeFirstMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    DLog(@"%@", [self.myDeckHolder subviews]);
    
}

- (void)meldGroupSecondButtonModelAction:(UIButton *)button
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    DLog(@"second meld group in player mode,%@",self.secondMeldGroupSelectedCards);
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.secondMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        
        card.hidden = YES;
        DLog(@"second meld group hidden %@,%d",card,card.hidden);
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.secondMeldGroupSelectedCards removeAllObjects];
    
    [self removeSecondMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    DLog(@"subview ----=--=-=-=-=-=-=-=-==-= %@", [self.myDeckHolder subviews]);
}

- (void)meldGroupThirdButtonModelAction:(UIButton *)button
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.thirdMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.thirdMeldGroupSelectedCards removeAllObjects];
    
    [self removeThirdMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    DLog(@"subview ----=--=-=-=-=-=-=-=-==-= %@", [self.myDeckHolder subviews]);
    
}

- (void)meldGroupFourthButtonModelAction:(UIButton *)button
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.fourthMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.fourthMeldGroupSelectedCards removeAllObjects];
    
    [self removeFourthMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    
}

- (void)meldGroupFifthButtonModelAction:(UIButton *)button
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.fifthMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.fifthMeldGroupSelectedCards removeAllObjects];
    
    [self removeFifthMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    
}

- (void)meldGroupSixthButtonModelAction:(UIButton *)button
{
    if (![self canModelMeldCard])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(getIsAnyMeldSlotEmptyInPlayerMode)])
    {
        canShowMeldButton=[self.modelDelegate getIsAnyMeldSlotEmptyInPlayerMode];
        if (!canShowMeldButton)
        {
            if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.modelDelegate showMeldErrorPopupInPlayerMode];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.sixthMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJPlayerModelCard *card = tempSelectedCards[i];
        //        NSUInteger index = [self.myDeckCardsArray indexOfObject:card];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.sixthMeldGroupSelectedCards removeAllObjects];
    
    [self removeSixthMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCardsOnModel:meldCardArray];
    
}

- (void)disablePlayerModeSlideButton
{
    self.slideButton.userInteractionEnabled = NO;
    self.slideButton.enabled = NO;
}

- (void)enablePlayerModeSlideButton
{
    self.slideButton.userInteractionEnabled = YES;
    self.slideButton.enabled = YES;
    
    
}
- (IBAction)slideDownPLayerModeAction:(UIButton *)sender
{
    if (self.modelDelegate && [self.modelDelegate respondsToSelector:@selector(didSelectArrowButton)])
    {
        [self.modelDelegate didSelectArrowButton];
        
    }
}


#pragma mark - Update info message -

- (void)showExtraMessagePlayerMode:(NSString *)message
{
#if PLAYER_MODE_INFO_DISPLAY
    self.infoLabelImageView.hidden = YES;
   self.extraMessageImageView.hidden = NO;
    self.extraInfoLabel.text = message;
    self.extraInfoLabel.hidden = NO;
    self.extraInfoLabel.alpha = 0.6f;
    [self performSelector:@selector(removeExtraMessagePlayerMode) withObject:self afterDelay:2.0];
#endif
}

- (void)removeExtraMessagePlayerMode
{
#if PLAYER_MODE_INFO_DISPLAY
    //self.infoLabelImageView.hidden = YES;
    self.extraMessageImageView.hidden = YES;
    self.extraInfoLabel.hidden = YES;
    self.extraInfoLabel.alpha = 0.0f;
#endif
}

- (void)showInstructionPlayerMode:(NSString *)instruction
{
#if PLAYER_MODE_INFO_DISPLAY
    [self removeExtraMessagePlayerMode];
    self.infoLabel.alpha = 0.6f;
    self.infoLabel.text = instruction;
    self.currentMessage = instruction;
    self.infoLabelImageView.hidden = NO;
    self.extraMessageImageView.hidden = YES;
    self.infoLabel.hidden = NO;
#endif
}

- (void)removeInfoLabelMessagePlayerMode
{
#if PLAYER_MODE_INFO_DISPLAY
    self.infoLabel.alpha = 0.0f;
    self.infoLabel.text = @"";
    self.currentMessage =  @"";
    self.infoLabel.hidden = YES;
    self.infoLabelImageView.hidden = YES;
#endif
}

- (void)hideInfoLabelMessagePlayerMode
{
#if PLAYER_MODE_INFO_DISPLAY
    self.infoLabel.alpha = 0.0f;
    self.infoLabelImageView.hidden = YES;
#endif
}
- (void)showInfoLabelMessagePlayerMode
{
#if PLAYER_MODE_INFO_DISPLAY
    
    [self removeExtraMessagePlayerMode];
    if(![self.infoLabel.text isEqualToString:@""])
    {
        self.infoLabelImageView.hidden = NO;
        self.infoLabel.alpha = 0.6f;
    }
    
#endif
}

- (void)hideInfoLabelAndExtraLabelAndImageViewPlayerMode
{
    self.infoLabel.hidden = YES;
    self.extraInfoLabel.hidden = YES;
    self.infoLabelImageView.hidden = YES;
    self.extraMessageImageView.hidden = YES;
}

@end
