/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerModelView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 10/11/2014.
 Modified by Lloyd Praveen Mascarenhas on 07/08/2014.
 Created by Pradeep BM on 27/05/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJPlayerModelCard.h"

@protocol TAJPlayerModelViewDelegate;

@interface TAJPlayerModelView : UIView

@property (nonatomic) BOOL                                  isItMyTurn;
@property (nonatomic, weak) id <TAJPlayerModelViewDelegate> modelDelegate;
@property (strong, nonatomic)   NSMutableArray              *myDeckArray;
@property (strong, nonatomic) IBOutlet UIView               *myDeckHolder;
@property (weak, nonatomic) IBOutlet UILabel                *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel                *extraInfoLabel;
@property (weak, nonatomic) NSString                        *currentMessage;
@property (weak, nonatomic) IBOutlet UIImageView            *infoLabelImageView;
@property (weak, nonatomic) IBOutlet UIImageView            *extraMessageImageView;

- (void)updatePlayerModelCards:(NSMutableArray *)myCardDecks withJokerCardID:(NSString *)jokerCardID;
- (void)removeAllModelButtons;
- (void)removeAllModelButtonsExceptDiscard;
- (void)removeAllModelButtonsExceptGroup;
- (void)removeMeldGroupAllButton;
- (void)flipModelCards;
- (void)canModelDiscardCard:(BOOL)boolValue;
- (void)disablePlayerModeSlideButton;
- (void)enablePlayerModeSlideButton;
- (TAJPlayerModelCard *)getLastCard;
- (void)automaticCardDiscardToOpenDeck;

//Restart
- (void)resetPlayerModelView;
- (void)removeMyDeckModelCardsFromHolder;

// Meld
- (void)meldSetupForModel;
- (void)meldModelCancelled;
- (void)removeRemainingCardsFromModelAfterSendCardFromMeld; // once click on send cards
- (void)didTapModelMeldCard:(NSUInteger)placeId;

// Meld group
- (void)updateMeldGroupModelButton;
- (void)canShowMeldGroupForPlayerModel:(BOOL)boolValue;
//info label
- (void)showExtraMessagePlayerMode:(NSString *)message;
- (void)removeExtraMessagePlayerMode;
- (void)showInstructionPlayerMode:(NSString *)instruction;
- (void)removeInfoLabelMessagePlayerMode;
- (void)hideInfoLabelMessagePlayerMode;
- (void)showInfoLabelMessagePlayerMode;
- (void)hideInfoLabelAndExtraLabelAndImageViewPlayerMode;

//lingam
- (void)removeAllMeldButtonsExceptGroup;
@end

@protocol TAJPlayerModelViewDelegate <NSObject>
@optional
- (BOOL)getIsAnyMeldSlotEmptyInPlayerMode;
- (void)showMeldErrorPopupInPlayerMode;
- (void)setCurrentSelectedCardsFromPlayerMode:(NSMutableArray *)array;
- (BOOL)canModelMeldCard;
- (void)groupCardsOnModelAtIndex:(NSMutableArray *)indexArray;
- (void)canModelEnableShowButton:(BOOL)boolValue withDiscardCardInfo:(NSDictionary *)discardCardInfo;
- (void)didDiscardCardOnModelAtIndex:(NSUInteger)discardCardIndex withCard:(TAJPlayerModelCard *)selectedCard;
- (void)didMeldCardsOnModel:(NSMutableArray *)array;
- (void)removeDiscardedShowCardForMeldSetup:(NSUInteger)showCardIndex;
- (void)placeDiscardCardFromModelToOpenDeck:(NSString *)cardID;
- (void)swapModelCardIndex:(NSUInteger)cardAIndex  withAnothCardIndex:(NSUInteger)cardBIndex;
- (void)didSelectArrowButton;
- (void)resetCardSlotsPlayerMode;



@end
