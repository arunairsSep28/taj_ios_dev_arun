/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChairView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 18/06/14.
 Created by Sujit Yadawad on 04/02/14.
 **/

#import "TAJChairView.h"
#import "TAJConstants.h"
#import "TAJGameEngine.h"
#import "TAJUtilities.h"
#import "TAJUserModel.h"
@interface TAJPlayerModel()

@end

@implementation TAJPlayerModel

- (id)initWithDictionary:(NSDictionary *)dictionary
{
    self = [super init];
    
    // Init necessary parameters here
    if (self)
    {
        if (!dictionary)
        {
            return self;
        }
        
        if (dictionary[GAME_PLACE_PLAYER_NAME] &&
            [dictionary[GAME_PLACE_PLAYER_NAME] isKindOfClass:[NSString class]])
        {
            //player name
            self.playerName = dictionary[GAME_PLACE_PLAYER_NAME];
        }
        
        if (dictionary[kTaj_userid_Key] &&
            [dictionary[kTaj_userid_Key] isKindOfClass:[NSString class]])
        {
            // player ID or user ID
            self.playerID = dictionary[kTaj_userid_Key];
        }
        if (dictionary[GAME_SEARCH_JOIN_TABLE_JOIN_TYPE] &&
            [dictionary[GAME_SEARCH_JOIN_TABLE_JOIN_TYPE] isKindOfClass:[NSString class]])
        {
            // player ID or user ID
            self.playerType = dictionary[GAME_SEARCH_JOIN_TABLE_JOIN_TYPE];
        }
        if (dictionary[TAJ_TABLE_ID] &&
            [dictionary[TAJ_TABLE_ID] isKindOfClass:[NSString class]])
        {
            // table ID
            self.tableID = dictionary[TAJ_TABLE_ID];
        }
        
        if (dictionary[kTaj_Place_Key] &&
            [dictionary[kTaj_Place_Key] isKindOfClass:[NSString class]])
        {
            // place ID or Seat ID
            self.placeID = dictionary[kTaj_Place_Key];
            
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(didChangePlaceID)])
            {
                [self.delegate didChangePlaceID];
            }
        }
        else if (dictionary[kTaj_Seat_key] &&
                 [dictionary[kTaj_Seat_key] isKindOfClass:[NSString class]])
        {
            // place ID or Seat ID
            self.placeID = dictionary[kTaj_Seat_key];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(didChangePlaceID)])
            {
                [self.delegate didChangePlaceID];
            }
        }
        
        if (dictionary[kTaj_buyin_amt_other] &&
            [dictionary[kTaj_buyin_amt_other] isKindOfClass:[NSString class]])
        {
            //player points in playing game
            self.playerPoints = dictionary[kTaj_buyin_amt_other];
        }
        else if (dictionary[kTaj_buyin_amt_this] &&
                 [dictionary[kTaj_buyin_amt_this] isKindOfClass:[NSString class]])
        {
            //player points in playing game
            self.playerPoints = dictionary[kTaj_buyin_amt_this];
        }
        
        if (dictionary[kTaj_player_rating] &&
            [dictionary[kTaj_player_rating] isKindOfClass:[NSString class]])
        {
            //player star to indicate how much is good in game
            self.playerGameAppraisalRate = dictionary[kTaj_player_rating];
        }
        
        if (dictionary[kTaj_player_gender] &&
            [dictionary[kTaj_player_gender] isKindOfClass:[NSString class]])
        {
            //player gender
            self.playerGender = dictionary[kTaj_player_gender];
        }
        
        if (dictionary[kTaj_player_char_no] &&
            [dictionary[kTaj_player_char_no] isKindOfClass:[NSString class]])
        {
            // player char number to display avatar image
            self.playerCharNumber = dictionary[kTaj_player_char_no];
        }
        if (dictionary[TAJ_Player_Type] &&
            [dictionary[TAJ_Player_Type] isKindOfClass:[NSString class]])
        {
            // Spectator
            if ([dictionary[TAJ_Player_Type] isEqualToString:GAME_SEARCH_JOIN_TABLE_JOIN_PLAY])
            {
                //
                self.isGameWatcher = NO;
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(isPlayerWatchingGame)])
            {
                [self.delegate isPlayerWatchingGame];
            }
            
        }
        else if (dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] &&
                 [dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isKindOfClass:[NSString class]])
        {
            //Spectator
            if ([dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_PLAY_VALUE])
            {
                self.isGameWatcher = NO;
            }
            else if ([dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_MIDDLE_VALUE])
            {
                self.isGameWatcher = NO;
            }
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(isPlayerWatchingGame)])
            {
                [self.delegate isPlayerWatchingGame];
            }
        }
        
        //device id
        if(dictionary[kTaj_Player_DeviceId] &&
           [dictionary[kTaj_Player_DeviceId] isKindOfClass:[NSString class]])
        {
            self.playerDeviceId = dictionary[kTaj_Player_DeviceId];
        }
        
    }
    return self;
}

- (void)setPlaceID:(NSString *)placeID
{
    if (_placeID != placeID)
    {
        _placeID = placeID;
        if (self.delegate && [self.delegate respondsToSelector:@selector(didChangePlaceID)])
        {
            [self.delegate didChangePlaceID];
        }
    }
}

@end

@interface TAJChairView()
@property (strong, nonatomic) IBOutlet UIImageView *backgroundContainerImage;
@property (strong, nonatomic) IBOutlet UIImageView *backgroundChairImage;
@property (weak, nonatomic) IBOutlet UIButton *sitHereButton;
@property (weak, nonatomic) IBOutlet UILabel *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *pointsLabel;
@property (weak, nonatomic) IBOutlet UIButton *arrowButton;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImageView;
@property (strong, nonatomic) IBOutlet UIImageView *starImageView;

@end

@implementation TAJChairView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        _isOccupied = NO;
        self.isSpectator = YES;
    }
    
    return self;
}

- (void)setIsOccupied:(BOOL)isOccupied
{
    if (_isOccupied != isOccupied)
    {
        _isOccupied = isOccupied;
    }
}

- (void)updateChairWithData:(TAJPlayerModel *)playerModel
{
#if DEBUG
    //NSLog(@"IMAGE ID %@",self.playerModel.playerDeviceId);
#endif
    NSMutableArray *avatarPlayerAppraisalArrayforIphone;
    NSMutableArray *avatarPlayerAppraisalArrayforIpad;
    
    [self showAllUI];
    
    self.playerModel = playerModel;
    self.playerModel.delegate = self;
    
    // Set red color because its occupied
    //self.backgroundColor = [UIColor redColor];
    self.isOccupied = YES;
    self.isSpectator = NO;
    
    self.infoLabel.textColor = [UIColor whiteColor];
    
    // Disable button
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    
    int chairPlace = [playerModel.placeID integerValue];
    self.chairId = chairPlace;
    
    // Set Player Info
    self.infoLabel.text = playerModel.playerName;
    self.pointsLabel.hidden = NO;
    self.viewChunksLevel.hidden = NO;
    if ([playerModel.playerPoints floatValue] !=0.0f && [self.playerModel.playerID isEqual:[[[TAJGameEngine sharedGameEngine] usermodel] userId] ])
    {
        
        self.pointsLabel.text = [NSString stringWithFormat:@"%g",[playerModel.playerPoints floatValue]];
        
    }
    else
    {
        self.pointsLabel.text = [NSString stringWithFormat:@"%g",[playerModel.playerPoints floatValue]];
        
    }
    
    
    //Author: Yogesh starts
    self.backgroundChairImage.hidden = NO;
    self.arrowButton.hidden=YES;
    //    self.avatarImageView.hidden = NO;
    self.avatarImageView.hidden = YES;//ratheesh
    self.deviceImageView.hidden = NO;
    self.starImageView.hidden = NO;
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
    
    self.avatarImageView.image = [UIImage imageNamed:[self getAvatarImageName:playerModel.playerGender]];
    // [self.userModel updateThisDeviceImageName:[self getAvatarImageName:playerModel.playerGender]];
    self.avatarImageView.alpha = 1.0f;
    
    self.backgroundContainerImage.alpha = 1.0f;
    self.starImageView.alpha = 1.0f;
    self.pointsLabel.alpha = 1.0f;
    self.infoLabel.alpha = 1.0f;
    self.viewChunksLevel.alpha = 1.0f;
    if ([TAJUtilities isIPhone])
    {
        //if it is iphone
        avatarPlayerAppraisalArrayforIphone =[NSMutableArray arrayWithObjects:TAJ_AVATAR_PLAYERRATE01_iphone,TAJ_AVATAR_PLAYERRATE02_iphone,TAJ_AVATAR_PLAYERRATE03_iphone,TAJ_AVATAR_PLAYERRATE04_iphone,TAJ_AVATAR_PLAYERRATE05_iphone,TAJ_AVATAR_PLAYERRATE06_iphone, nil];
        if (playerModel.playerGameAppraisalRate)
        {
            self.starImageView.image=[UIImage imageNamed:[avatarPlayerAppraisalArrayforIphone objectAtIndex:([playerModel.playerGameAppraisalRate integerValue]-TAJ_ONE)]];
            self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
            
        }
        
    }
    else
    {
        avatarPlayerAppraisalArrayforIpad =[NSMutableArray arrayWithObjects:TAJ_AVATAR_PLAYERRATE01_ipad,TAJ_AVATAR_PLAYERRATE02_ipad,TAJ_AVATAR_PLAYERRATE03_ipad,TAJ_AVATAR_PLAYERRATE04_ipad,TAJ_AVATAR_PLAYERRATE05_ipad,TAJ_AVATAR_PLAYERRATE06_ipad, nil];
        if (playerModel.playerGameAppraisalRate)
        {
            self.starImageView.image=[UIImage imageNamed:[avatarPlayerAppraisalArrayforIpad objectAtIndex:([playerModel.playerGameAppraisalRate integerValue]-TAJ_ONE)]];
            //self.backgroundContainerImage.image=[UIImage imageNamed:@"avatar_sit_bg2~ipad.png"];
            self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
        }
    }
    if ([self.playerModel.playerType isEqualToString:@"table_leave"])
    {
        DLog(@"table left  user id %@ , %@",self.playerModel.placeID,self.playerModel.playerID);
        if ([TAJUtilities isIPhone])
        {
            //self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
            self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
        }
        else
        {
            //self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
            self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
        }
        self.avatarImageView.hidden = YES;
        self.deviceImageView.hidden = YES;
        self.dropBoxImageView.hidden = NO;
        self.dropTextLabel.hidden = NO;
    }
    
    //device info
    NSString * deviceStatusImage = [self.playerModel.playerDeviceId lowercaseString];
    
    if([deviceStatusImage isEqualToString:@"iphone"] || [deviceStatusImage isEqualToString:@"mobile"])
    {
        self.deviceImageView.hidden = NO;
        self.deviceImageView.image =[UIImage imageNamed:@"device_iphone.png"];
        
    }
    else if([deviceStatusImage isEqualToString:@"ipad"] || [deviceStatusImage isEqualToString:@"ipod"] || [deviceStatusImage isEqualToString:@"tablet"])
    {
        self.deviceImageView.hidden = NO;
        if ([TAJUtilities isIPhone])
        {
            self.deviceImageView.image = [UIImage imageNamed:@"device_ipad-568h@2x~iphone"];
        }
        else
        {
            self.deviceImageView.image =[UIImage imageNamed:@"device_ipad.png"];
        }
    }
    else if([deviceStatusImage isEqualToString:@"flash"]|| [deviceStatusImage isEqualToString:@"website"])
    {
        self.deviceImageView.hidden = NO;
        if ([TAJUtilities isIPhone])
        {
            self.deviceImageView.image = [UIImage imageNamed:@"device_web-568h@2x~iphone.png"];
        }
        else
        {
            self.deviceImageView.image =[UIImage imageNamed:@"device_web.png"];
        }
    }
    else {
        self.deviceImageView.hidden = NO;
        if ([TAJUtilities isIPhone])
        {
            self.deviceImageView.image = [UIImage imageNamed:@"device_web-568h@2x~iphone.png"];
        }
        else
        {
            self.deviceImageView.image =[UIImage imageNamed:@"device_web.png"];
        }
    }
    
}

- (void)updatePlayerDeviceSymbol
{
    NSString * deviceStatusImage = [self.playerModel.playerDeviceId lowercaseString];
    
    if([deviceStatusImage isEqualToString:@"iphone"] || [deviceStatusImage isEqualToString:@"mobile"])
    {
        self.deviceImageView.hidden = NO;
        self.deviceImageView.image =[UIImage imageNamed:@"device_iphone.png"];
        
    }
    else if([deviceStatusImage isEqualToString:@"ipad"] || [deviceStatusImage isEqualToString:@"ipod"] || [deviceStatusImage isEqualToString:@"tablet"])
    {
        self.deviceImageView.hidden = NO;
        
        if ([TAJUtilities isIPhone])
        {
            self.deviceImageView.image = [UIImage imageNamed:@"device_ipad-568h@2x~iphone"];
        }
        else
        {
            self.deviceImageView.image =[UIImage imageNamed:@"device_ipad.png"];
        }
        
    }
    else if([deviceStatusImage isEqualToString:@"flash"]|| [deviceStatusImage isEqualToString:@"website"])
    {
        self.deviceImageView.hidden = NO;
        if ([TAJUtilities isIPhone])
        {
            self.deviceImageView.image = [UIImage imageNamed:@"device_web-568h@2x~iphone.png"];
        }
        else
        {
            self.deviceImageView.image =[UIImage imageNamed:@"device_web.png"];
        }
    }
    else {
        self.deviceImageView.hidden = NO;
        if ([TAJUtilities isIPhone])
        {
            self.deviceImageView.image = [UIImage imageNamed:@"device_web-568h@2x~iphone.png"];
        }
        else
        {
            self.deviceImageView.image =[UIImage imageNamed:@"device_web.png"];
        }
    }
}

- (void)updateChairForDroppedPlayerWithData:(TAJPlayerModel *)playerModel
{
    [self showAllUI];
    self.playerModel = playerModel;
    self.playerModel.delegate = self;
    int chairPlace = [playerModel.placeID integerValue];
    self.chairId = chairPlace;
    
    // Set red color because its occupied
    //self.backgroundColor = [UIColor redColor];
    self.isOccupied = YES;
    self.isSpectator = NO;
    // Disable button
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    // Set Player Info
    self.pointsLabel.hidden = YES;
    self.viewChunksLevel.hidden = YES;
    //Author: Yogesh starts
    self.backgroundChairImage.hidden = YES;
    self.arrowButton.hidden=YES;
    self.avatarImageView.hidden = YES;
    self.deviceImageView.hidden = YES;
    self.starImageView.hidden = YES;
    self.dropBoxImageView.hidden = NO;
    self.dropTextLabel.hidden = NO;
    // [self.userModel updateThisDeviceImageName:[self getAvatarImageName:playerModel.playerGender]];
    self.avatarImageView.alpha = 1.0f;
    
    self.backgroundContainerImage.alpha = 1.0f;
    if ([TAJUtilities isIPhone])
    {
        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
    }
    else
    {
        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
    }
}

- (void)updateChairImagesOnGameStart
{
    self.avatarImageView.alpha = 1.0f;
    self.backgroundContainerImage.alpha = 1.0f;
    self.starImageView.alpha = 1.0f;
    self.pointsLabel.alpha = 1.0f;
    self.infoLabel.alpha = 1.0f;
    self.viewChunksLevel.alpha = 1.0f;
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
}

- (void)hideChair
{
    self.infoLabel.hidden = YES;
    self.arrowButton.hidden = YES;
    
    // Set red color because its occupied
    //self.backgroundColor = [UIColor redColor];
    self.isOccupied = NO;
    self.isSpectator = NO;
    
    // Disable button
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    
    self.pointsLabel.hidden = YES;
    self.viewChunksLevel.hidden = YES;
    
    //Author: Yogesh starts
    self.backgroundContainerImage.hidden=NO;
    self.backgroundChairImage.hidden = YES;
    self.arrowButton.hidden=YES;
    self.avatarImageView.hidden = YES;
    self.deviceImageView.hidden = YES;
    self.starImageView.hidden = YES;
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
    
    self.avatarImageView.alpha = 0.5f;
    
    self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
    
    if ([TAJUtilities isIPhone])
    {
        //self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
    }
    else
    {
        // self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
    }
    self.starImageView.alpha = 0.5f;
    self.pointsLabel.alpha = 0.5f;
    self.infoLabel.alpha = 0.5f;
    self.viewChunksLevel.alpha = 0.5f;
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
    
}

-(NSString *)getAvatarImageName:(NSString *)gender
{
    NSMutableArray *avatarImageFemaleArray;
    NSMutableArray *avatarImageMaleArray;
    if ([TAJUtilities isIPhone])
    {
        //if it is iphone
        avatarImageFemaleArray = [NSMutableArray arrayWithObjects:TAJ_AVATAR_FEMALE_O1_iphone,TAJ_AVATAR_FEMALE_O2_iphone,TAJ_AVATAR_FEMALE_O3_iphone,TAJ_AVATAR_FEMALE_O4_iphone,TAJ_AVATAR_FEMALE_O5_iphone,TAJ_AVATAR_FEMALE_O6_iphone, nil];
        avatarImageMaleArray=[NSMutableArray arrayWithObjects:TAJ_AVATAR_MALE_O1_iphone, TAJ_AVATAR_MALE_O2_iphone,TAJ_AVATAR_MALE_O3_iphone,TAJ_AVATAR_MALE_O4_iphone,TAJ_AVATAR_MALE_O5_iphone,TAJ_AVATAR_MALE_O6_iphone, nil];
    }
    else
    {
        // if it is ipad
        avatarImageFemaleArray = [NSMutableArray arrayWithObjects:TAJ_AVATAR_FEMALE_O1_ipad,TAJ_AVATAR_FEMALE_O2_ipad,TAJ_AVATAR_FEMALE_O3_ipad,TAJ_AVATAR_FEMALE_O4_ipad,TAJ_AVATAR_FEMALE_O5_ipad,TAJ_AVATAR_FEMALE_O6_ipad, nil];
        avatarImageMaleArray=[NSMutableArray arrayWithObjects:TAJ_AVATAR_MALE_O1_ipad, TAJ_AVATAR_MALE_O2_ipad,TAJ_AVATAR_MALE_O3_ipad, TAJ_AVATAR_MALE_O4_ipad,TAJ_AVATAR_MALE_O5_ipad,TAJ_AVATAR_MALE_O6_ipad,nil];
    }
    
    if([gender isEqualToString:@"MALE"])
    {
        if ([self.playerModel.playerCharNumber integerValue] > 0 && self.playerModel.playerCharNumber)
        {
            return [avatarImageMaleArray objectAtIndex:([self.playerModel.playerCharNumber integerValue] - 1)];
        }
        return nil;
    }
    else
    {
        if ([self.playerModel.playerCharNumber integerValue] > 0 && self.playerModel.playerCharNumber)
        {
            return [avatarImageFemaleArray objectAtIndex:([self.playerModel.playerCharNumber integerValue] - 1)];
        }
        return nil;
    }
}

- (void)createNewEmptyChairWithChairID:(int)number
{
    self.chairId = number;
}

- (void)setUpPlayerPoints:(NSString *)playerPoints
{
    if (self.playerModel)
    {
        self.playerModel.playerPoints = playerPoints;
        self.pointsLabel.text = [NSString stringWithFormat:@"%g",[self.playerModel.playerPoints floatValue]];
    }
}

- (void)resetPlayerModel
{
    if (self.playerModel && self.isOccupied)
    {
        [self updateChairWithData:self.playerModel];
    }
}

- (void)hideChairs
{
    [self hideChair];
}

- (void)hideDropMessage
{
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
}

- (IBAction)sitHereAction:(UIButton *)sender
{
    [self hideAllUI];
    [self didSelectChair];
}

- (void)hideAllUI
{
    self.infoLabel.hidden = YES;
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    
    // Author: yogesh starts
    self.arrowButton.hidden=YES;
    // Author: yogesh ends
}

- (void)didSelectChair
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectChair:)])
    {
        [self.delegate didSelectChair:self];
    }
}

- (void)disableInteraction
{
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    if (self.infoLabel && !self.isOccupied)
    {
        self.infoLabel.text = @" ";
        self.infoLabel.hidden = YES;
    }
    // Author: yogesh starts
    self.arrowButton.hidden=YES;
    // Author: yogesh ends
}

- (void)removeChairAvatars
{
    [self playerLeft];
}

- (void)enableInteraction
{
    if (!self.isOccupied)
    {
        //self.backgroundColor = [UIColor greenColor];
        self.infoLabel.hidden = NO;
        //lingam
        self.sitHereButton.hidden = YES;
        self.sitLabel.hidden = YES;
        self.arrowButton.hidden = YES;
        
        // Author: yogesh starts
        self.avatarImageView.hidden = YES;
        self.deviceImageView.hidden = YES;
        if ([TAJUtilities isIPhone])
        {
            // self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
        }
        else
        {
            // self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
        }
        self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
        self.backgroundChairImage.hidden = YES;
        
        // Author: yogesh ends
    }
}

- (void)showAllUI
{
    self.infoLabel.hidden = NO;
    //lingam
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    self.arrowButton.hidden = YES;
    // self.sitHereButton.hidden=NO;
}

- (void)didChangePlaceID
{
    if (self.playerModel)
    {
        self.chairId = [self.playerModel.placeID integerValue];
    }
}

- (void)isPlayerWatchingGame
{
    if (self.playerModel)
    {
        self.isSpectator = [self.playerModel isGameWatcher];
    }
}

- (BOOL)isSeatOccupied
{
    return self.isOccupied;
}

#pragma mark - Table leave -

- (void)playerQuitTable:(BOOL)isGameScheduled
{
    if (self.playerModel && isGameScheduled)
    {
        [self playerLeft];
    }
    else if (self.playerModel && !isGameScheduled)
    {
        if (!self.isOccupied)
        {
            [self playerLeft];
        }
        else
        {
            // self.backgroundColor = [UIColor greenColor];
            self.infoLabel.text = @"";
            self.infoLabel.textColor = [UIColor blackColor];
            self.infoLabel.hidden = YES;
            self.pointsLabel.hidden = YES;
            self.viewChunksLevel.hidden = YES;
            
            //lingam
            self.sitHereButton.hidden = YES;
            self.sitLabel.hidden = YES;
            self.arrowButton.hidden = YES;
            
            self.avatarImageView.hidden = YES;
            self.deviceImageView.hidden = YES;
            self.starImageView.hidden = YES;
            self.backgroundChairImage.hidden = YES;
            self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
            //            if ([TAJUtilities isIPhone])
            //            {
            //                self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
            //            }
            //            else
            //            {
            //                self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
            //            }
            
            self.isOccupied = NO;
        }
    }
}

- (void)playerQuitTableBeforeStartGame:(BOOL)isGameScheduled
{
    if (self.playerModel && isGameScheduled)
    {
        [self playerLeftBeforeStartGame];
    }
    else if (self.playerModel && !isGameScheduled)
    {
        if (!self.isOccupied)
        {
            [self playerLeftBeforeStartGame];
        }
        else
        {
            // self.backgroundColor = [UIColor greenColor];
            self.infoLabel.text = @"";
            self.infoLabel.hidden = YES;
            self.pointsLabel.hidden = YES;
            self.viewChunksLevel.hidden = YES;
            
            //lingam
            self.sitHereButton.hidden = YES;
            self.sitLabel.hidden = YES;
            self.arrowButton.hidden = YES;
            
            self.avatarImageView.hidden = YES;
            self.deviceImageView.hidden = YES;
            self.starImageView.hidden = YES;
            self.backgroundChairImage.hidden = YES;
            //            if ([TAJUtilities isIPhone])
            //            {
            //                self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
            //            }
            //            else
            //            {
            //                self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
            //            }
            self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
            self.isOccupied = NO;
        }
    }
}

- (void)playerLeft
{
    if (!self.isSpectator)
    {
        self.playerModel = nil;
        self.infoLabel.text = @" ";
        self.infoLabel.hidden = YES;
        self.sitHereButton.hidden = YES;
        self.sitLabel.hidden = YES;
        self.arrowButton.hidden = YES;
        self.pointsLabel.hidden = YES;
        self.viewChunksLevel.hidden = YES;
        self.avatarImageView.hidden = YES;
        self.deviceImageView.hidden = YES;
        self.starImageView.hidden = YES;
        self.dropBoxImageView.hidden = NO;
        self.dropTextLabel.hidden = NO;
        self.backgroundChairImage.hidden = YES;
        self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
        //        if ([TAJUtilities isIPhone])
        //        {
        //            self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
        //        }
        //        else
        //        {
        //            self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
        //        }
        self.isOccupied = NO;
    }
}

- (void)playerLeftBeforeStartGame
{
    if (!self.isSpectator)
    {
        self.playerModel = nil;
        self.infoLabel.text = @" ";
        self.infoLabel.hidden = YES;
        self.sitHereButton.hidden = YES;
        self.sitLabel.hidden = YES;
        self.arrowButton.hidden = YES;
        self.pointsLabel.hidden = YES;
        self.viewChunksLevel.hidden = YES;
        self.avatarImageView.hidden = YES;
        self.deviceImageView.hidden = YES;
        self.starImageView.hidden = YES;
        self.dropBoxImageView.hidden = YES;
        self.dropTextLabel.hidden = YES;
        self.backgroundChairImage.hidden = YES;
        self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
        //        if ([TAJUtilities isIPhone])
        //        {
        //            self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
        //        }
        //        else
        //        {
        //            self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
        //        }
        self.isOccupied = NO;
    }
}

- (void)standUpChair
{
    self.playerModel = nil;
    self.infoLabel.hidden = YES;
    //lingam
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    self.arrowButton.hidden = YES;
    
    self.pointsLabel.hidden = YES;
    self.viewChunksLevel.hidden = YES;
    self.avatarImageView.hidden = YES;
    self.deviceImageView.hidden = YES;
    self.starImageView.hidden = YES;
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
    self.backgroundChairImage.hidden = YES;
    self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
    //    if ([TAJUtilities isIPhone])
    //    {
    //        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
    //    }
    //    else
    //    {
    //        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
    //    }
    self.isOccupied = NO;
}

#pragma mark - Player drop -

- (void)playerDropGame
{
    if (self.playerModel)
    {
        self.avatarImageView.alpha = 0.5f;
        self.backgroundContainerImage.alpha = 0.5f;
        self.starImageView.alpha = 0.5f;
        self.pointsLabel.alpha = 0.5f;
        self.infoLabel.alpha = 0.5f;
        self.viewChunksLevel.alpha = 0.5f;
        self.dropBoxImageView.hidden = NO;
        self.dropTextLabel.hidden = NO;
    }
}

- (void)otherPlayerlayerDropGame
{
    self.avatarImageView.alpha = 0.5f;
    self.backgroundContainerImage.alpha = 0.5f;
    self.starImageView.alpha = 0.5f;
    self.pointsLabel.alpha = 0.5f;
    self.infoLabel.alpha = 0.5f;
    self.viewChunksLevel.alpha = 0.5f;
    self.dropBoxImageView.hidden = NO;
    self.dropTextLabel.hidden = NO;
}

- (void)otherPlayerLeftGame
{
    // jokr game player left
    self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
    //    if ([TAJUtilities isIPhone])
    //    {
    //        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
    //    }
    //    else
    //    {
    //        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
    //    }
    self.backgroundChairImage.hidden = YES;
    self.avatarImageView.hidden = YES;
    self.deviceImageView.hidden = YES;
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    self.arrowButton.hidden = YES;
    self.dropBoxImageView.hidden = NO;
    self.dropTextLabel.hidden = NO;
    self.starImageView.hidden = YES;
    self.infoLabel.hidden = YES;
    self.pointsLabel.hidden = YES;
    self.viewChunksLevel.hidden = YES;
    self.isOccupied = NO;/*player left case*/
}

- (void)updateStrikesGamewithBet:(int)betValue
{
    if ([self.playerModel.playerPoints floatValue] !=0.0f && [self.playerModel.playerID isEqual:[[[TAJGameEngine sharedGameEngine] usermodel] userId] ])
    {
        if (betValue<0)
        {
            betValue=0;
        }
        self.pointsLabel.text = [NSString stringWithFormat:@"%d",betValue];
        
    }
    else
    {
        self.pointsLabel.text = [NSString stringWithFormat:@"%g",[self.playerModel.playerPoints floatValue]];
        
    }
}

- (void)otherPlayerLeftGameOnThisPlayerDisconnect
{
    self.infoLabel.textColor = [UIColor whiteColor];
    self.backgroundContainerImage.image=[UIImage imageNamed:@"TR_Table_Player"];
    //    if ([TAJUtilities isIPhone])
    //    {
    //        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg-568h@2x~iphone.png"];
    //    }
    //    else
    //    {
    //        self.backgroundContainerImage.image = [UIImage imageNamed:@"avatar_sit_bg.png"];
    //    }
    self.avatarImageView.hidden = YES;
    self.deviceImageView.hidden = YES;
    self.backgroundChairImage.hidden = YES;
    self.sitHereButton.hidden = YES;
    self.sitLabel.hidden = YES;
    self.arrowButton.hidden = YES;
    self.dropBoxImageView.hidden = YES;
    self.dropTextLabel.hidden = YES;
    self.starImageView.hidden = YES;
    self.infoLabel.hidden = YES;
    self.pointsLabel.hidden = YES;
    self.viewChunksLevel.hidden = YES;
    
    self.isOccupied = NO;/*player left case*/
}

- (void)updateChairWithChunksData:(NSString *)playerData WithplaceID:(NSInteger)placeID {
    NSLog(@"PLAYER PLACE : %ld",(long)placeID);
    NSLog(@"CHUNKS DATA : %@",playerData);
    
    [self setPlayer1ChunksLevelDefault];
    
    if ([playerData isEqualToString:@"3/3"]) {
        [self setPlayer1ChunksLevel3];
    }
    else if ([playerData isEqualToString:@"2/3"]) {
        [self setPlayer1ChunksLevel2];
    }
    else if ([playerData isEqualToString:@"1/3"]) {
        [self setPlayer1ChunksLevel1];
    }
    else {
        [self setPlayer1ChunksLevelDefault];
    }
}

-(void)setPlayer1ChunksLevel3 {
    self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_on"];
    self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_on"];
    self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_on"];
}

-(void)setPlayer1ChunksLevel2{
    self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_off"];
    self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_off"];
    self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_off"];
}

-(void)setPlayer1ChunksLevel1{
    self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_off"];
    self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_off"];
    self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_off"];
}

-(void)setPlayer1ChunksLevelDefault{
    self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_off"];
    self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_off"];
    self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_off"];
}

-(void)setPlayer1ChunksLevelWithChunksLevel:(NSString *)chunksLevel {
    if ([chunksLevel isEqualToString:@"3/3"]) {
        self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_on"];
        self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_on"];
        self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_on"];
    }
    else if ([chunksLevel isEqualToString:@"2/3"]) {
        self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_on"];
        self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_on"];
        self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_off"];
    }
    else if ([chunksLevel isEqualToString:@"1/3"]) {
        self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_on"];
        self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_off"];
        self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_off"];
    }
    else {
        self.imgViewChunksLevel1.image = [UIImage imageNamed:@"chunks_off"];
        self.imgViewChunksLevel2.image = [UIImage imageNamed:@"chunks_off"];
        self.imgViewChunksLevel3.image = [UIImage imageNamed:@"chunks_off"];
    }
}

@end
