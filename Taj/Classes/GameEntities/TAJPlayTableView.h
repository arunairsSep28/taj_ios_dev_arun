/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayTableView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 18/06/14.
 Created by Sujit Yadawad on 04/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJCardView.h"
//#import "TAJIAmBackCell.h"

typedef enum
{
    TAJAnimationTypeNone,
    TAJAnimationTypeOpenDeck,
    TAJAnimationTypeClosedDeck,
    TAJAnimationTypeDiscard
    
}TAJAnimationType;

@protocol TAJPlayTableProtocol;

@interface TAJPlayTableView : UIView <TAJCardViewProtocol>

//is it my turn
@property (nonatomic) BOOL                      isItMyTurn;
@property (weak, nonatomic) IBOutlet UIView     *closedDeckCardHolder;
@property (weak, nonatomic) IBOutlet UIView     *openDeckHolder;
@property (nonatomic, weak) id <TAJPlayTableProtocol> delegate;
@property (weak, nonatomic) IBOutlet UIView     *myCardDeckHolder;
@property (weak, nonatomic) NSString            *currentMessage;
@property (nonatomic, strong) NSMutableArray    *openDeckCards;
@property (nonatomic)         NSUInteger        totalPlayers;
@property (nonatomic)           BOOL            canMeldGroupCards;
@property (nonatomic)           BOOL            isThisPlayerCardDiscarded;
@property(nonatomic, weak) UIView *srcView;
- (id)initWithView:(UIView *)src;

- (void)initialiseAllUIWith:(NSDictionary *)disctionary;
- (void)playerJoined:(NSDictionary *)dictionary isThisDevicePlayerSpectator:(BOOL)value;
- (void)playerLeftTable:(NSInteger)noOfPlayers minimumPlayers:(NSInteger)count isThisDevicePlayerSpectator:(BOOL)isThisDeviceSpectator;
- (void)searchPlayerJoinTable:(NSDictionary *)dictionary isThisDevicePlayer:(BOOL)isThisDevPlayer;
- (void)showInstruction:(NSString *)instruction;
- (void)showCard:(NSString *)cardId forChair:(UIView *)chairHolder;
- (void)card:(TAJCardView *)card didChangeState:(BOOL)state;
- (void)removeCardsFromTossDeck;
- (void)removeJokerHolderCards;
- (void)setupClosedDeckWithJoker:(NSString *)jokerCardId;
- (void)placeFaceUpCard:(NSString *)faceUpCardID;
- (void)updateMyDeckWithCards:(NSMutableArray *)myCardDecks;
- (void)updateSlotCardsOnMyDeck:(NSMutableArray *)myCardDecks;
- (void)thisDivicePlayerTurn:(NSDictionary *)dictionary;
- (void)otherPlayerTurn:(NSDictionary *)dictionary;
- (void)updateCardDiscard:(NSDictionary *)disctionary;
- (void)cardPickedFromOpenStack:(TAJCardView *)cardId;
- (void)disableFirstTimePickOpenCard;
- (void)cardPickedFromClosedStack:(NSString *)cardId;
- (void)removeImagesFromMyDeckHolder;
- (void)canDiscardCard:(BOOL)boolValue;
- (void)removeAllButtonsExceptDiscard;

//yogish
- (void)fillAnimationCardWithIndex:(NSInteger)index;

- (void)refreshOpenDeckAfterReshuffle;
- (void)thisPlayerCardDiscarded:(NSDictionary *)dictionary;
- (void)otherPlayerCardDiscarded:(NSDictionary *)dictionary;

- (void)flipCards;
- (void)showExtraMessage:(NSString *)message;
- (void)removeExtraMessage;
- (void)removeInfoLabelMessage;
- (void)hideInfoLabelMessage;
- (void)showInfoLabelMessage;


- (void)removeAllButtons;
- (void)removeAllButtonsExceptGroup;
- (void)removeAllButtonsWithMeldGroup;
- (void)removeTableSelectedCards;

- (void)dropGameSetup;

// Restart game
- (void)restartGameSetupForGameEnd;
- (void)restartGameSetupForGameResult;
- (void)updateClosedDeckCardHolder:(BOOL)boolValue;
- (void)otherPlayerPickedCardOpenDeckCard;
- (void)removeOpenDeckHolderCards;
- (void)removeMyDeckCardsFromHolder;

//user interation
- (void)thisPlayerTurnUserInteration;
- (void)otherPlayerTurnUserInteration;

//meld cards
- (void)updateMeldButton;
- (void)meldSetup;
- (void)meldCancelled;
- (void)didTapMeldCard:(NSUInteger)placeId isPlayerModeEnabled:(BOOL)boolValue;
- (void)keepDiscardCardAsClosedCard;
- (void)clearCardsinClosedCard;

//remove remaining cards after send card in meld view
- (void)removeRemainingCardsAfterSendCardFromMeld;

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;
- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card;

// meld group
- (void)updateMeldGroupButton;
- (void)removeMeldGroupAllButton;
- (void)wantToShowMeldGroupButton:(BOOL)boolValue;

// Player Model
- (void)placeCardInOpenDeck:(NSString *)cardID;
- (void)enableUserTouchForPlayerModel:(BOOL)boolValue;
- (void)canAutomaticDiscardCard:(BOOL)boolValue;
- (NSString *)getJokerCardNumberForIamBackCell;

@end

@protocol TAJPlayTableProtocol <NSObject>

- (BOOL)canMeldCards;
- (void)didSelectClosedDeck;
- (void)didSelectOpenDeck:(NSString *)cardNum withCardSuit:(NSString *)cardSuit;
- (void)didDiscardedCardAtIndex:(NSUInteger)inndex;
- (void)swapCard:(NSUInteger)cardAindex withCard:(NSUInteger)cardBindex;
- (void)didMeldCards:(NSMutableArray *)array;
- (BOOL)getIsAnyMeldSlotEmpty;
- (void)showMeldErrorPopup;
- (void)discardCardFromPlayerMode;

- (void)setCurrentSelectedCards:(NSMutableArray *)array;
- (void)groupCardsAtIndexes:(NSMutableArray *)indexArray;
- (void)didEnableShowButton:(BOOL)boolValue withDiscardInfo:(NSDictionary *)discardCardInfo;

// slot spacing
- (void)removeDiscardedCardForMeldSetup:(NSUInteger)index;
- (void)removeEmptySlotFromIndex:(NSUInteger)cardAindex withIndex:(NSUInteger)cardBindex;
- (void)resetCardSlots;
- (BOOL)playerShowsCard;

@optional
- (void)updateSortButton;
- (void)showExtraMessageDelegate:(NSString *)message;
- (void)removeExtraMessageDelegate;
- (void)removeInfoLabelMessageDelegate;
- (void)showInstructionDelegate:(NSString *)instruction;

@end
