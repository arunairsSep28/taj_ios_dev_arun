/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayTableView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 18/06/14.
 Created by Sujit Yadawad on 04/02/14.
 **/

#import "TAJPlayTableView.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
//#import "TAJAnimateCard.h"

#define CGRectCenter(__CGRect__) CGPointMake(CGRectGetMidX(__CGRect__), CGRectGetMidY(__CGRect__))

//#define SLOT_CARD_FACTOR_IPAD 33
#define SLOT_CARD_FACTOR_IPAD 36
#define SLOT_CARD_FACTOR_IPHONE_5 22
//#define SLOT_CARD_FACTOR_IPHONE_5   18
//#define SLOT_CARD_FACTOR_IPHONE_4   16
#define SLOT_CARD_FACTOR_IPHONE_4   36 //22

#define PLAY_TABLE_ALL_BUTTON_FONT_NAME    @"AvantGardeITCbyBT-Demi"
#define PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPHONE    12.0
#define PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPAD      13.0

//LEFT
#define MOVE_LEFT_EMPTY_SLOT_IPAD   1.0
#define MOVE_LEFT_EMPTY_SLOT_IPHONE   0.8

//RIGHT
#define MOVE_RIGHT_EMPTY_SLOT_IPAD   1.08
#define MOVE_RIGHT_EMPTY_SLOT_IPHONE   0.7

#define PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE   8.0f
#define PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD     16.0f

#define PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE      @"meldgroup_callout-568h.png"
#define PLAY_TABLE_MELD_GROUP_IMAGE_IPAD        @"meldgroup_callout.png"

//static int count = 0;

@interface TAJPlayTableView()

@property(strong,nonatomic) NSString *previoussuit;
@property (weak, nonatomic) IBOutlet UILabel    *infoLabel;
@property (weak, nonatomic) IBOutlet UILabel *extraInfoLabel;

@property (weak, nonatomic) IBOutlet UIView     *closedDeckHolder;
@property (weak, nonatomic) IBOutlet UIView     *jokerCardHolder;
@property (weak, nonatomic) IBOutlet UIButton   *closedDeckButton;

// functional related
@property (nonatomic, strong) NSString          *jokerCardId;
@property (nonatomic, strong) TAJCardView       *jokerCard;
@property (nonatomic, strong) NSMutableArray    *tossCardsArray;
@property (nonatomic, strong) NSMutableArray    *myDeckCardsArray;
@property (nonatomic, strong) NSMutableArray    *selectedCardsArray;
@property (nonatomic, strong) UIButton          *discardButton;
@property (nonatomic)   BOOL                    canDiscardCard;
@property (nonatomic, strong) UIButton          *groupCardsButton;
@property (nonatomic)         BOOL              areCardsFlipped;

//allow player to pick any card from opendeck at first time, without any condition
@property (nonatomic)         BOOL              firstTimePickOpenCard;
@property (nonatomic)         BOOL              openDeckAutomaticDiscardCard;
@property (nonatomic)         CGRect            previousJokerPosition;

// Touch related
@property (nonatomic)         CGPoint           touchStartLocation;
@property (nonatomic)         CGRect            placingFrame;
@property (nonatomic, strong) TAJCardView       *leftCard;
@property (nonatomic, strong) TAJCardView       *rightCard;
@property (nonatomic)         CGRect            startRect;

//meld card array
@property (nonatomic, strong)  UIButton         *meldCardButton;

@property (strong, nonatomic) IBOutlet UILabel *jokerLabel;

// slot spacing
@property (nonatomic)   int         noOfTableCards;
@property (nonatomic, strong)   NSMutableArray  *hiddenCardWhenMeld;

//meld group cards
@property (nonatomic, strong)   NSMutableArray  *firstMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *secondMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *thirdMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *fourthMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *fifthMeldGroupSelectedCards;
@property (nonatomic, strong)   NSMutableArray  *sixthMeldGroupSelectedCards;

@property (nonatomic, strong)  UIButton         *meldGroupFirstButton;
@property (nonatomic, strong)  UIButton         *meldGroupSecondButton;
@property (nonatomic, strong)  UIButton         *meldGroupThirdButton;
@property (nonatomic, strong)  UIButton         *meldGroupFourthButton;
@property (nonatomic, strong)  UIButton         *meldGroupFifthButton;
@property (nonatomic, strong)  UIButton         *meldGroupSixthButton;

@property (nonatomic) BOOL isCardMoved;
@property (strong, nonatomic) UILabel *infoLabeliPhone;
@property (strong, nonatomic) UILabel *extraInfoLabeliPhone;

@end

@implementation TAJPlayTableView
@synthesize srcView;

- (id)initWithView:(UIView *)src {
    self = [super initWithFrame:src.frame];
    if (self) {
        srcView = src;
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [srcView.layer renderInContext:UIGraphicsGetCurrentContext()];
    
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        [self otherPlayerTurnUserInteration];
        self.firstTimePickOpenCard = YES;
        self.canMeldGroupCards = NO;
        self.openDeckAutomaticDiscardCard = NO;
        self.canDiscardCard = YES;
        self.isCardMoved = NO;
        
        if ([TAJUtilities isIPhone]){
            self.extraInfoLabel.hidden = YES;
            self.extraInfoLabel.alpha = 0.0f;
            self.extraInfoLabeliPhone.hidden = YES;
            self.extraInfoLabeliPhone.alpha = 0.0f;
        }
        else {
            self.extraInfoLabel.hidden = YES;
            self.extraInfoLabel.alpha = 0.0f;
        }
        
        
        //NSLog(@"CHECKING RECT");
        [self setLabelsPosition];
    }
    
    return self;
}

- (void)setLabelsPosition {
    if ([TAJUtilities isIPhone]) {
        self.extraInfoLabeliPhone = [[UILabel alloc] init];
        self.infoLabeliPhone = [[UILabel alloc] init];
        
        if ([TAJUtilities isItIPhone5]) {
            self.extraInfoLabeliPhone.frame = CGRectMake(0, 140, 667, 18);
            self.infoLabeliPhone.frame = CGRectMake(0, 140, 667, 18);
            NSLog(@"isItIPhone5");
        }
        else if ([TAJUtilities isItIPhone6]) {
            self.extraInfoLabeliPhone.frame = CGRectMake(0, 148, 600, 18);
            NSLog(@"isItIPhone6");
            self.infoLabeliPhone.frame = CGRectMake(0, 148, 600, 18);
            
            /*CGRect Frame = self.myCardDeckHolder.frame;
            Frame.origin.x = self.myCardDeckHolder.frame.origin.x + 100;
            self.myCardDeckHolder.frame = Frame;
            self.myCardDeckHolder.backgroundColor = [UIColor blueColor];*/
        }
        
        else if ([TAJUtilities isItIPhonex]) {
            NSLog(@"isItIPhonex");
            self.extraInfoLabeliPhone.frame = CGRectMake(0, 134, 647, 18);
            self.infoLabeliPhone.frame = CGRectMake(0, 134, 647, 18);
        }
        else if ([TAJUtilities isItIPhoneXSMax]) {
            NSLog(@"isItIPhoneXSMax");
            self.extraInfoLabeliPhone.frame = CGRectMake(0, 154, 667, 18);
            self.infoLabeliPhone.frame = CGRectMake(0, 154, 667, 18);
        }
        else if ([TAJUtilities isItIPhone6Plus]) {
            NSLog(@"isItIPhone6Plus");
            self.extraInfoLabeliPhone.frame = CGRectMake(0, 164, 667, 18);
            self.infoLabeliPhone.frame = CGRectMake(0, 164, 667, 18);
        }
        
        self.extraInfoLabeliPhone.textColor = [UIColor whiteColor];
        self.extraInfoLabeliPhone.textAlignment = NSTextAlignmentCenter;
        self.extraInfoLabeliPhone.font = [UIFont fontWithName:@"Helvetica" size:10];
        
        self.infoLabeliPhone.textColor = [UIColor whiteColor];
        self.infoLabeliPhone.textAlignment = NSTextAlignmentCenter;
        self.infoLabeliPhone.font = [UIFont fontWithName:@"Helvetica" size:10];
        
        [self addSubview:self.infoLabeliPhone];
        [self addSubview:self.extraInfoLabeliPhone];
    }
}

- (void)initialiseAllUIWith:(NSDictionary *)dictionary
{
    self.jokerLabel.transform = CGAffineTransformMakeRotation (-3.14/2);
    self.totalPlayers = 0;
    self.areCardsFlipped = NO;
    self.isThisPlayerCardDiscarded = NO;
    
    self.groupCardsButton.exclusiveTouch = YES;
    self.closedDeckButton.exclusiveTouch = YES;
    self.meldCardButton.exclusiveTouch = YES;
    self.discardButton.exclusiveTouch = YES;
    
    self.meldGroupFirstButton.exclusiveTouch = YES;
    self.meldGroupSecondButton.exclusiveTouch = YES;
    self.meldGroupThirdButton.exclusiveTouch = YES;
    self.meldGroupFourthButton.exclusiveTouch = YES;
    self.meldGroupFifthButton.exclusiveTouch = YES;
    self.meldGroupSixthButton.exclusiveTouch = YES;
    // Init necessary arrays
    self.tossCardsArray = [NSMutableArray array];
    self.myDeckCardsArray = [NSMutableArray array];
    self.selectedCardsArray = [NSMutableArray array];
    self.openDeckCards = [NSMutableArray array];
    
    self.hiddenCardWhenMeld = [NSMutableArray array];
    
    // for meld group button
    self.firstMeldGroupSelectedCards = [NSMutableArray array];
    self.secondMeldGroupSelectedCards = [NSMutableArray array];
    self.thirdMeldGroupSelectedCards = [NSMutableArray array];
    self.fourthMeldGroupSelectedCards = [NSMutableArray array];
    self.fifthMeldGroupSelectedCards = [NSMutableArray array];
    self.sixthMeldGroupSelectedCards = [NSMutableArray array];
    
    //    [self showInstruction:PLEASE_TAKE_SEAT_MESSAGE];
    if (self.delegate && [self.delegate respondsToSelector:@selector(showInstructionDelegate:)])
    {
        [self.delegate showInstructionDelegate:PLEASE_TAKE_SEAT_MESSAGE];
    }
    
    if (dictionary &&
        [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
    {
        // Only 1 player available
        self.totalPlayers = 1;
    }
    else if (dictionary &&
             [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
    {
        // Get the players array
        NSArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        self.totalPlayers = playersArray.count;
    }
    else
    {
        // No Player joined yet
        self.totalPlayers = 0;
    }
    
    if ([TAJUtilities isIPhone]) {
        self.extraInfoLabel.hidden = YES;
        self.extraInfoLabel.alpha = 0.0f;
        
        self.extraInfoLabeliPhone.hidden = YES;
        self.extraInfoLabeliPhone.alpha = 0.0f;
    }
    else {
        self.extraInfoLabel.hidden = YES;
        self.extraInfoLabel.alpha = 0.0f;
    }
    
}

- (void)playerJoined:(NSDictionary *)dictionary isThisDevicePlayerSpectator:(BOOL)isThisDeviceSpectator
{
    if (!dictionary)
    {
        return;
    }
    
    self.totalPlayers++;
    
    // Inform whether to wait for other players or not
    int minimumPlayers = [[dictionary valueForKeyPath:GAME_GET_MIN_PLAYER] integerValue];
    
    if (self.totalPlayers < minimumPlayers && !isThisDeviceSpectator)
    {
        NSString *descriptionString = [NSString stringWithFormat:WAIT_FOR_OTHER_PLAYERS, (minimumPlayers - self.totalPlayers)];
        if (self.delegate && [self.delegate respondsToSelector:@selector(showInstructionDelegate:)])
        {
            [self.delegate showInstructionDelegate:descriptionString];
        }
        [self showInstruction:descriptionString];
    }
    else if (self.totalPlayers < minimumPlayers && isThisDeviceSpectator)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(showInstructionDelegate:)])
        {
            [self.delegate showInstructionDelegate:PLEASE_TAKE_SEAT_MESSAGE];
        }
        //        [self showInstruction:PLEASE_TAKE_SEAT_MESSAGE];
    }
}

- (void)playerLeftTable:(NSInteger)noOfPlayers minimumPlayers:(NSInteger)count isThisDevicePlayerSpectator:(BOOL)isThisDeviceSpectator
{
    if (self.totalPlayers < count && !isThisDeviceSpectator)
    {
        NSString *descriptionString = [NSString stringWithFormat:WAIT_FOR_OTHER_PLAYERS, (count - self.totalPlayers)];
        if (self.delegate && [self.delegate respondsToSelector:@selector(showInstructionDelegate:)])
        {
            [self.delegate showInstructionDelegate:descriptionString];
        }
        [self showInstruction:descriptionString];
    }
    else if (self.totalPlayers < count && isThisDeviceSpectator)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(showInstructionDelegate:)])
        {
            [self.delegate showInstructionDelegate:PLEASE_TAKE_SEAT_MESSAGE];
        }
        //        [self showInstruction:PLEASE_TAKE_SEAT_MESSAGE];
    }
    
}

- (void)searchPlayerJoinTable:(NSDictionary *)dictionary isThisDevicePlayer:(BOOL)isThisDevPlayer
{
    // display message once he joined as play after SEARCH_JOIN_TABLE
    int minimumPlayers = [[dictionary valueForKeyPath:GAME_GET_MIN_PLAYER] integerValue];
    
    if (self.totalPlayers < minimumPlayers && isThisDevPlayer)
    {
        NSString *descriptionString = [NSString stringWithFormat:WAIT_FOR_OTHER_PLAYERS, (minimumPlayers - self.totalPlayers)];
        if (self.delegate && [self.delegate respondsToSelector:@selector(showInstructionDelegate:)])
        {
            [self.delegate showInstructionDelegate:descriptionString];
        }
        [self showInstruction:descriptionString];
    }
}

- (void)thisDivicePlayerTurn:(NSDictionary *)dictionary
{
    NSLog(@"CHECK DROPPING");
    [self thisPlayerTurnUserInteration];
}

- (void)otherPlayerTurn:(NSDictionary *)dictionary
{
    [self otherPlayerTurnUserInteration];
}

#pragma mark - CARD PLACEMENT HELPER METHODS -

- (void)showCard:(NSString *)cardId forChair:(UIView *)chairHolder
{
    NSArray *nibContents = [NSArray array];
    // Place new card
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                owner:nil
                                              options:nil];
    
    TAJCardView *card = nibContents[index];
    [card showCard:cardId isMomentary:YES];
    card.delegate = self;
    
    CGRect frame = [self convertRect:chairHolder.frame fromView:chairHolder.superview];
    CGRect newFrame = card.frame;
    
    if (frame.origin.y > 0)
    {
        // Its at the bottom
        newFrame.origin.y = frame.origin.y - ([TAJUtilities isIPhone] ? 50 : 90); //self.frame.size.height - newFrame.size.height - 30;
    }
    else if (frame.origin.y < 0)
    {
        // Its at the top
        newFrame.origin.y = -1 * (frame.origin.y + frame.size.height + ([TAJUtilities isIPhone] ? 8 : 15));//frame.size.height-80.0;//([TAJUtilities isIPhone]? 11 : 15);
    }
    newFrame.origin.x = frame.origin.x + (frame.size.width/2 - newFrame.size.width/2);
    card.frame = newFrame;
    
    [self addSubview:card];
    [self.tossCardsArray addObject:card];
}

- (void)removeCardsFromTossDeck
{
    for (UIView *card in self.tossCardsArray)
    {
        [card removeFromSuperview];
    }
    [self.tossCardsArray removeAllObjects];
}

- (void)placeJokerCard:(NSString *)jokerCardId
{
    
    NSString * jokerCard = [NSString stringWithFormat:@"J%@",jokerCardId];
    NSLog(@"JOKERID COMBINATION :%@",jokerCard);
    
    NSArray *nibContents = [[NSArray alloc]init];
    // Place new card
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                owner:nil
                                              options:nil];
    
    self.jokerCard = nibContents[index];
    [self.jokerCard showCard:jokerCard isMomentary:YES];
    self.jokerCard.delegate = self;
    
    float width, height;
    if ([TAJUtilities isIPhone])
    {
        //       width  = [TAJUtilities isItIPhone5]? 35 : 31;
        //       height = [TAJUtilities isItIPhone5]? 43 : 35;
        //        width  = [TAJUtilities isItIPhone5]? 51 : 47;
        //        height = [TAJUtilities isItIPhone5]? 59 : 51;
        width = 60;//64;
        height = 60;//64;
        CGRect frame = self.jokerCard.frame;
        frame.size.width = width;
        frame.size.height = height;
        self.jokerCard.frame = frame;
    }
    else{self.jokerCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 0.85f);
    }
    
    
    [self placeJokerCardForJockerHolder:self.jokerCard inHolder:self.jokerCardHolder];
    
    self.jokerCard.userInteractionEnabled = NO;
    
    self.jokerCardId = jokerCard;
    
    self.previousJokerPosition = self.jokerCardHolder.frame;
    
    // set transform for joker card holder
//    CGAffineTransform transform = CGAffineTransformMakeRotation(-M_PI_2);
//    self.jokerCardHolder.transform = transform;
}

- (void)placeJokerCardForJockerHolder:(TAJCardView *) card inHolder:(UIView *) holderView
{
    if (holderView.subviews && holderView.subviews.count > 0)
    {
        TAJCardView *oldCard = holderView.subviews[0];
        [oldCard removeFromSuperview];
    }
    [holderView addSubview:card];
    card.delegate = self;
    
}

- (void)placeFaceUpCard:(NSString *)faceUpCardID
{
    [self placeCardInOpenDeck:faceUpCardID];
}

- (void)placeCard:(TAJCardView *)card inHolder:(UIView *)holderView
{
    if (holderView.subviews && holderView.subviews.count > 0)
    {
        TAJCardView *oldCard = holderView.subviews[0];
        [oldCard removeFromSuperview];
    }
    CGRect holderFrame = holderView.frame;
    CGRect cardFrame = card.frame;
    cardFrame.origin.x = holderFrame.size.width/2 - cardFrame.size.width/2 ;
    cardFrame.origin.y = holderFrame.size.height/2 - cardFrame.size.height/2;
    card.frame = cardFrame;
    [holderView addSubview:card];
    card.delegate = self;
}

#pragma mark - CARD DISCARD Codes -

- (void)thisPlayerCardDiscarded:(NSDictionary *)dictionary
{
    // update open deck
    NSString *cardNum = dictionary[kTaj_Card_Face_Key];
    NSString *cardType = [dictionary[kTaj_Card_Suit_Key] uppercaseString];
    NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
    DLog(@"card discarded in playtable %@",dictionary);
    [self placeCardInOpenDeck:cardId];
    [self.selectedCardsArray removeLastObject];
}

- (void)otherPlayerCardDiscarded:(NSDictionary *)dictionary
{
    // update open deck
    NSString *cardNum = dictionary[kTaj_Card_Face_Key];
    NSString *cardType = [dictionary[kTaj_Card_Suit_Key] uppercaseString];
    NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
    DLog(@"otherplayerdicarde %@",cardId);
    [self placeCardInOpenDeck:cardId];
}

- (void)updateCardDiscard:(NSDictionary *)dictionary
{
    // update open deck
    NSString *cardNum = dictionary[kTaj_Card_Face_Key];
    NSString *cardType = [dictionary[kTaj_Card_Suit_Key] uppercaseString];
    NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
    
    [self placeCardInOpenDeck:cardId];
}

- (void)updateDiscardButton
{
    // First check the selected card array, if more than 1 dont show discard button
    if (self.noOfTableCards == 14 && self.selectedCardsArray && self.selectedCardsArray.count == 1 &&  self.isItMyTurn && self.canDiscardCard)
    {
        [self removeCardDiscardButton];
        TAJCardView *card = self.selectedCardsArray[0];
        // First create button
        CGRect cardFrame = card.frame;
        
        float position = 41.0f;
        CGRect buttonFrame;
        if ([TAJUtilities isIPhone])
        {
            position = 25.0f;
            buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x, self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position ,cardFrame.size.width, position);
        }
        else
        {
            buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x-15, self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position + 3,cardFrame.size.width, position);
        }
        
        self.discardButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.discardButton.exclusiveTouch = YES;
        [self.discardButton addTarget:self  action:@selector(discardButtonAction:)  forControlEvents:UIControlEventTouchUpInside];
        [self.discardButton setTitle:@"Discard" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            //            newImage = [UIImage imageNamed:@"discards_bg-568h.png"];
            newImage = [UIImage imageNamed:@"discards_bg-568h.png"];
            self.discardButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            //            newImage = [UIImage imageNamed:@"discards_bg.png"];
            newImage = [UIImage imageNamed:@"discards_bg~ipad.png"];
            self.discardButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPAD];
        }

        [self.discardButton setBackgroundImage:newImage forState:UIControlStateNormal];
        // in case the parent view draws with a custom color or gradient, use a transparent color
        self.discardButton.backgroundColor = [UIColor clearColor];
        //[self.discardButton setTitleColor: [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1] forState: UIControlStateNormal];
        [self.discardButton setTitleColor: [UIColor whiteColor] forState: UIControlStateNormal];
        self.discardButton.frame = buttonFrame;
        
        [self addSubview:self.discardButton];
        
        //enable show button once click on card
        NSMutableDictionary * discardCardInfo = [NSMutableDictionary dictionaryWithObjectsAndKeys:[card cardNumber],kTaj_Card_Face_Key,
                                                 [[card cardSuit] uppercaseString],kTaj_Card_Suit_Key, nil];
        
        [self didEnableShowButton:YES withDiscardInfo:discardCardInfo];
    }
    else if (self.selectedCardsArray &&
             self.selectedCardsArray.count > 1 &&
             self.discardButton)
    {
        [self removeCardDiscardButton];
    }
    else
    {
        [self removeCardDiscardButton];
    }
}

- (void)removeCardDiscardButton
{
    if (self.discardButton)
    {
        // Remove discard button its no longer necessary
        [self.discardButton removeFromSuperview];
        self.discardButton = nil;
        
        [self didEnableShowButton:NO withDiscardInfo:nil];
    }
}

- (void)canDiscardCard:(BOOL)boolValue
{
    self.canDiscardCard = boolValue;
}

- (void)canAutomaticDiscardCard:(BOOL)boolValue
{
    self.openDeckAutomaticDiscardCard = boolValue;
}

- (void)discardButtonAction:(UIButton *)button
{
    self.isThisPlayerCardDiscarded = YES;
    button.hidden = YES;
    
    // Yet to implement discard action
    if (self.selectedCardsArray && self.selectedCardsArray.count == 1)
    {
        TAJCardView *selectedCard = self.selectedCardsArray[0];
        
        NSString *cardNumber = [selectedCard cardNumber];
        NSString *cardSuit = [selectedCard cardSuit];
        
        [self didDiscardedCardAtIndex:[self.myDeckCardsArray indexOfObject:selectedCard]];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNumber, [cardSuit uppercaseString]];
        
        NSArray *nibContents;
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                    owner:nil
                                                  options:nil];
        
        TAJCardView *card = nibContents[index];
        
        card.frame=selectedCard.frame;
        [card showCard:cardId isMomentary:YES];
        
        [self.myCardDeckHolder insertSubview:card belowSubview:selectedCard];
        
        if ([TAJUtilities isIPhone])
        {
            if([TAJUtilities isItIPhone5])
            {
                card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.7f, 0.75f);
                
            }
            else
            {
                card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.9f, 0.95f);
                
            }
            
        }
        
        else
        {
            card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
        }
        
        [UIView animateWithDuration:0.05
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseIn
                         animations:
         ^{
             CGRect finalrect=[self.myCardDeckHolder convertRect:self.openDeckHolder.frame fromView:self];
             if ([TAJUtilities isIPhone])
             {
                 finalrect.origin.y -= 15;
                 finalrect.origin.x += 5;
                 finalrect.size.width = 64;
                 finalrect.size.height = 84;
             }
             
             card.frame=finalrect;
             card.alpha = 0;
             
         }
                         completion:^(BOOL finished)
         {
             [card removeFromSuperview];
         }];
        [selectedCard removeFromSuperview];
        [self performSelector:@selector(placeCardInOpenDeck:) withObject:cardId afterDelay:0.0];
        
        //remove my card from array
        [self.selectedCardsArray removeLastObject];
        
        [self removeCardDiscardButton];
        
        //enable user interaction
        if (self.isItMyTurn)
        {
            [self thisPlayerTurnUserInteration];
        }
    }
}

#pragma mark - OPEN DECK Codes -

- (void)refreshOpenDeckAfterReshuffle
{
    NSMutableArray *lastObject = [NSMutableArray array];
    int indexOfLastObject = [self.openDeckCards count] - 1;
    
    [lastObject addObject:self.openDeckCards[indexOfLastObject]];
    [self.openDeckCards removeAllObjects];
    self.openDeckCards = [NSMutableArray arrayWithArray:lastObject];
}

- (void)placeCardInOpenDeck:(NSString *)cardID
{
    if (self.openDeckHolder.subviews && self.openDeckHolder.subviews.count > 0)
    {
        TAJCardView *oldCard = self.openDeckHolder.subviews[0];
        [oldCard removeFromSuperview];
    }
    
    NSArray *nibContents;
    // Place new card
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil   options:nil];
    TAJCardView *card = nibContents[index];
    
    //float width, height;
    if ([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.7f, 0.75f);
            
        }
        else
        {
            card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.8f, 0.8f);
            // 0.9f,0.9f before oct 15th 2020
            //(CGAffineTransformIdentity, 1.0f, 1.02f)
        }
    }
    else
    {
        card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
    }
    [self placeCard:card inHolder:self.openDeckHolder];
    card.delegate = self;
    [card showCard:cardID isMomentary:YES];
    [self.openDeckCards addObject:@{kTaj_Card_Face_Key: [card cardNumber],kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString]}];
    DLog(@"placecard in opendeck %@",self.openDeckCards);
    
}

- (void)cardPickedFromOpenStack:(TAJCardView *)card
{
    if (self.openDeckCards.count == 1)
    {
        [self.openDeckCards removeLastObject];
        
        if (card)
        {
            [card removeFromSuperview];
        }
        return;
    }
    DLog(@"cardpickedfromsatck %@",self.openDeckCards);
    if (self.openDeckCards && self.openDeckCards.count > 1)
    {
        [self.openDeckCards removeLastObject];
        
        NSDictionary *cardInfo = [self.openDeckCards lastObject];
        
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
        
        if (card)
        {
            [card removeFromSuperview];
        }
        
        NSArray *nibContents;
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                    owner:nil
                                                  options:nil];
        
        TAJCardView *card = nibContents[index];
        if ([TAJUtilities isIPhone])
        {
            
            if([TAJUtilities isItIPhone5])
            {
                card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.7f, 0.74f);
                
            }
            else
            {
                card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.8f, 0.8f);
                //(CGAffineTransformIdentity, 1.0f, 1.02f)
            }
            
        }
        else
        {
            card.transform=CGAffineTransformScale(CGAffineTransformIdentity,1.0f, 1.0f);
        }
        DLog(@"cardpickedfromsatck gratere than one %@",self.openDeckCards);
        [self placeCard:card inHolder:self.openDeckHolder];
        card.delegate = self;
        [card showCard:cardId isMomentary:YES];
    }
}

- (void)pickCardFromOpenDeck:(TAJCardView *)card
{
    [self removeAllButtons];
    DLog(@"pickcardfromopendeck %@",self.openDeckCards);
    if (self.noOfTableCards == 13
        && self.isItMyTurn)
    {
        NSString *selectedCardNum = [card cardNumber];
        NSString *selectedCardSuit = [card cardSuit];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", selectedCardNum, [selectedCardSuit uppercaseString]];
        DLog(@"pickcard  fromopendeck card %@",cardId);
        // animation goes here
        NSArray *nibContents;
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil  options:nil];
        
        TAJCardView *newCard = nibContents[index];
        // float width, height;
        
        CGRect startRect=[self.myCardDeckHolder convertRect:self.openDeckHolder.frame fromView:self];
        
        newCard.frame=startRect;
        
        [newCard showCard:cardId isMomentary:YES];
        UIView *view=[self getLastCard];
        
        if ([TAJUtilities isIPhone])
        {
            newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.6f, 0.6f);
        }
        else
        {
            newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 1.0f, 1.0f);
        }
        
        CGRect finalRect=CGRectMake(view.frame.origin.x+18, view.frame.origin.y,  view.frame.size.width, view.frame.size.height);
        
        [self.myCardDeckHolder addSubview:newCard];
        [UIView animateWithDuration:0.05
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:
         ^{
             newCard.frame=finalRect;
             
         }
                         completion:^(BOOL finished)
         {
             [newCard removeFromSuperview];
         }];
        
        [self didSelectOpenDeck:selectedCardNum withCardSuit:selectedCardSuit];
        [self cardPickedFromOpenStack:card];
        
        [self removeGroupButton];
    }
    else if(self.noOfTableCards == 14
            && self.isItMyTurn)
    {
        [self removeInfoLabelMessage];
        [self showExtraMessage:INFO_PICKED_CARD];
        if (self.delegate && [self.delegate respondsToSelector:@selector(removeInfoLabelMessageDelegate)])
        {
            [self.delegate removeInfoLabelMessageDelegate];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
        {
            [self.delegate showExtraMessageDelegate:INFO_PICKED_CARD];
        }
        
        //[self.delegate removeInfoLabelMessageDelegate];
        // [self.delegate showExtraMessageDelegate:INFO_PICKED_CARD];
    }
    else if(!self.isItMyTurn)
    {
        [self removeInfoLabelMessage];
        [self showExtraMessage:INFO_NOT_MYTURN];
        if (self.delegate && [self.delegate respondsToSelector:@selector(removeInfoLabelMessageDelegate)])
        {
            [self.delegate removeInfoLabelMessageDelegate];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
        {
            [self.delegate showExtraMessageDelegate:INFO_NOT_MYTURN];
        }
    }
}

#pragma mark - CLOSED DECK Codes -

- (void)setupClosedDeckWithJoker:(NSString *)jokerCardId
{
    NSLog(@"JOKERID : %@",jokerCardId);
    [self placeJokerCard:jokerCardId];
    [self placeClosedDeckCard];
}

- (void)placeClosedDeckCard
{
    self.closedDeckButton.hidden = NO;
}

- (IBAction)closedDeckButtonAction:(UIButton *)sender
{
    if (self.noOfTableCards == 13
        && self.isItMyTurn)
    {
        DLog(@"caard picked from closed deck %d",self.isItMyTurn);
        
        [self didSelectClosedDeck];
        [self cardPickedFromClosedStack:nil];
    }
    else if(self.noOfTableCards == 14
            && self.isItMyTurn)
    {
        [self showExtraMessage:INFO_PICKED_CARD];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
        {
            [self.delegate showExtraMessageDelegate:INFO_PICKED_CARD];
        }
        DLog(@"caard picked from picked already deck else %d",self.isItMyTurn);
    }
    else if(!self.isItMyTurn)
    {
        [self removeInfoLabelMessage];
        [self showExtraMessage:INFO_NOT_MYTURN];
        if (self.delegate && [self.delegate respondsToSelector:@selector(removeInfoLabelMessageDelegate)])
        {
            [self.delegate removeInfoLabelMessageDelegate];
        }
        if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
        {
            [self.delegate showExtraMessageDelegate:INFO_NOT_MYTURN];
        }
        
        DLog(@"caard picked from closed deck else %d",self.isItMyTurn);
        
    }
}

- (void)cardPickedFromClosedStack:(NSString *)cardId
{
    NSLog(@"CARD PICK CHECKING CLOSE");

    if (cardId)
    {
        NSArray *nibContents;
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil  options:nil];
        
        TAJCardView *newCard = nibContents[index];
        CGRect startRect=[self.myCardDeckHolder convertRect:self.closedDeckCardHolder.frame fromView:self];
        
        newCard.frame=startRect;
        [newCard showCard:cardId isMomentary:YES];
        UIView *view=[self getLastCard];
        if ([TAJUtilities isIPhone])
        {
            
            newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.6f, 0.6f);
        }
        else
        {
            newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.9f, 0.95f);
        }
        
        CGRect finalRect=CGRectMake(view.frame.origin.x+18, view.frame.origin.y,  view.frame.size.width, view.frame.size.height);
        [self.myCardDeckHolder addSubview:newCard];
        [UIView animateWithDuration:0.05
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:
         ^{
             newCard.frame=finalRect;
             
         }
                         completion:^(BOOL finished)
         {
             [newCard removeFromSuperview];
         }];
        [self removeGroupButton];
    }
}

#pragma mark - Drop Setup

- (void)dropGameSetup
{
    [self otherPlayerTurnUserInteration];
}

- (void)updateMyDeckWithCards:(NSMutableArray *)myCardDecks
{
    
    DLog(@"on playtable %@",myCardDecks);
    for (int i = 0; i<self.myDeckCardsArray.count; i++ )
    {
        TAJCardView *card = self.myDeckCardsArray[i];
        [card removeFromSuperview];
    }
    
    [self.myDeckCardsArray removeAllObjects];
    [self.selectedCardsArray removeAllObjects];
    self.noOfTableCards = 0;
    for (int i = 0; i < myCardDecks.count; i++)
    {
        NSDictionary *cardInfo = myCardDecks[i];
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
        NSUInteger slotindex = [cardInfo[SLOT_KEY] unsignedIntegerValue];
        
        self.noOfTableCards++;
        NSArray *nibContents = [[NSArray alloc]init];
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                    owner:nil
                                                  options:nil];
        
        TAJCardView *card = nibContents[index];
        card.delegate = self;
        
        [card showCard:cardId isMomentary:NO];
        card.placeID = i;
        
        card.slotID = slotindex;
        
        if ([cardId isEqualToString:self.jokerCardId])
        {
            card.isJoker = YES;
        }
        card.draggable = YES;
        [self placeCard:card onMyDeckAtIndex:i];
        [self.myDeckCardsArray addObject:card];
    }
    DLog(@"on playtable mydeck %@",self.myDeckCardsArray);
}

- (void)placeCard:(TAJCardView *)card onMyDeckAtIndex:(NSUInteger)index
{
    NSString *suittype = [card cardSuit];
    CGRect holderFrame = self.myCardDeckHolder.frame;
    CGRect cardFrame = card.frame;
    if ([TAJUtilities isIPhone])
    {
        cardFrame.origin.x = (index + 1) * ([TAJUtilities isItIPhone5] ? CARD_OFFSET_X_IPHONE_5 : CARD_OFFSET_X_IPHONE_4 ) - 100;
        //cardFrame.origin.x = (index + 1) * CARD_OFFSET_X_IPHONE_5 ;
    }
    else
    {
        if([suittype isEqualToString:self.previoussuit])
        {
            cardFrame.origin.x = (index + 1) * CARD_OFFSET_X_IPAD;
        }
        else
        {
            cardFrame.origin.x = (index + 1) * CARD_OFFSET_X_IPAD + 10 ;
        }
    }
    cardFrame.origin.y = holderFrame.size.height - cardFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);
    card.frame = cardFrame;
    [self.myCardDeckHolder addSubview:card];
    self.previoussuit = suittype;
}

- (void)rearrangeMyDeckCards
{
#if CARD_GROUPING_SLOTS
    for (int i = 0; i < self.myDeckCardsArray.count; i++)
    {
        TAJCardView *card = self.myDeckCardsArray[i];
        [self placeSlotCards:card atSlotNumber:i];
    }
#else
    for (int i = 0; i < self.myDeckCardsArray.count; i++)
    {
        TAJCardView *card = self.myDeckCardsArray[i];
        [self placeCard:card onMyDeckAtIndex:i];
    }
#endif
}

- (void)removeCardFromMyDeckAtIndex:(NSUInteger)index
{
    TAJCardView *card = self.myDeckCardsArray[index];
    [self.myDeckCardsArray removeObjectAtIndex:index];
    [card removeFromSuperview];
    
    if (self.discardButton)
    {
        [self removeCardDiscardButton];
    }
}

#pragma mark - Helper object  -

- (void)removeCardIDFromMyDeck:(NSString *)cardId
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            
            NSString* deckCardID = [[card cardId] uppercaseString];
            
            if ([[cardId uppercaseString] isEqualToString:deckCardID])
            {
                [card removeFromSuperview];
                // remove from my deck array first then from view
                [self.myDeckCardsArray removeObject:card];
                
                break;
            }
        }
    }
#else
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        NSString* deckCardID = [[card cardId] uppercaseString];
        
        if ([[cardId uppercaseString] isEqualToString:deckCardID])
        {
            [card removeFromSuperview];
            // remove from my deck array first then from view
            [self.myDeckCardsArray removeObject:card];
            
            break;
        }
    }
#endif
    
    if (self.discardButton)
    {
        [self removeCardDiscardButton];
    }
}

- (void)unhideCardIDFromMyDeck:(NSString *)cardId
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            
            NSString* deckCardID = [[card cardId] uppercaseString];
            
            if ([[cardId uppercaseString] isEqualToString:deckCardID] && card.hidden)
            {
                card.hidden = NO;
                
                break;
            }
        }
    }
#else
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        NSString* deckCardID = [[card cardId] uppercaseString];
        
        if ([[cardId uppercaseString] isEqualToString:deckCardID] && card.hidden)
        {
            card.hidden = NO;
            break;
        }
    }
#endif
}

- (TAJCardView *)cardFromMyDeckCardId:(NSString *)cardId
{
    NSString *cardIdentifier;
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *cardInfo = self.myDeckCardsArray[i];
            cardIdentifier = [[cardInfo cardId] uppercaseString];
            if ([cardIdentifier isEqualToString:[cardId uppercaseString]])
            {
                return cardInfo;
            }
        }
    }
#else
    for (TAJCardView *cardInfo in self.myDeckCardsArray)
    {
        cardIdentifier = [[cardInfo cardId] uppercaseString];
        if ([cardIdentifier isEqualToString:[cardId uppercaseString]])
        {
            return cardInfo;
        }
    }
    
#endif
    return nil;
    
}

- (void)hideOrUnhideLastCard
{
#if CARD_GROUPING_SLOTS
    NSUInteger lastCardExistAt = 0;
    for (NSUInteger i = (self.myDeckCardsArray.count - 1); i < self.myDeckCardsArray.count; i--)
    {
        if ([self isCardExistAtSlotIndex:i])
        {
            lastCardExistAt = i;
            break;
        }
    }
    
    TAJCardView *card = [self.myDeckCardsArray objectAtIndex:lastCardExistAt];
    if (!card.hidden)
    {
        card.hidden = YES;
    }
    else
    {
        card.hidden = NO;
    }
    
#else
    TAJCardView *card = [self.myDeckCardsArray lastObject];
    if (!card.hidden)
    {
        card.hidden = YES;
    }
    else
    {
        card.hidden = NO;
    }
#endif
    
}

- (void)hideCardForCardID:(NSString *)cardId
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            NSString *deckCardID = [[card cardId] uppercaseString];
            
            if ([[cardId uppercaseString] isEqualToString:deckCardID] && !card.hidden)
            {
                card.hidden = YES;
                break;
            }
        }
    }
#else
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        NSString *deckCardID = [[card cardId] uppercaseString];
        
        if ([[cardId uppercaseString] isEqualToString:deckCardID] && !card.hidden)
        {
            card.hidden = YES;
            
            break;
        }
    }
#endif
    
}

- (void)removeTableSelectedCards
{
    [self.selectedCardsArray removeAllObjects];
}

#pragma mark - CardView delegates -

// State remains same untill changed
- (void)card:(TAJCardView *)card didChangeState:(BOOL)state
{
    if (state)
    {
        if (![self canMeldCards])
        {
            if (self.noOfTableCards == 13 || self.noOfTableCards == 14)
            {
                if (card.isSelected)
                {
                    [self.selectedCardsArray addObject:card];
                }
                
                if (self.noOfTableCards == 14)
                {
                    if (self.selectedCardsArray.count > 0 && self.selectedCardsArray.count < 2)
                    {
                        TAJCardView *tempCard = self.selectedCardsArray[0];
                        tempCard.isSelected = YES;
                        [self animateCard:tempCard];
                        
                    }
                    else if (self.selectedCardsArray.count > 0)
                    {
                        TAJCardView *tempCard = self.selectedCardsArray[0];
                        tempCard.isSelected = YES;
                        
                        if (self.selectedCardsArray.count ==2 )
                        {
                            TAJCardView *tempCard1 = self.selectedCardsArray[0];
                            tempCard1.isSelected = NO;
                            [self animateCard:tempCard1];
                            tempCard1.isSelected =YES;
                        }
                        
                    }
                }
            }
            else if(self.noOfTableCards == 14)
            {
                if (self.selectedCardsArray.count > 0)
                {
                    TAJCardView *tempCard = self.selectedCardsArray[0];
                    tempCard.isSelected = NO;
                    [self animateCard:tempCard];
                }
                
                [self.selectedCardsArray removeAllObjects];
                [self.selectedCardsArray addObject:card];
            }
        }
        else if([self canMeldCards])
        {
            if (self.selectedCardsArray.count < 5)
            {
                [self.selectedCardsArray addObject:card];
            }
            else
            {
                card.isSelected = NO;
            }
        }
    }
    else
    {
        if (self.noOfTableCards <=14)
        {
            TAJCardView *previousCard = self.selectedCardsArray[0];
            
            if (self.selectedCardsArray &&
                self.selectedCardsArray.count > 0 &&
                self.selectedCardsArray.count > [self.selectedCardsArray indexOfObject:card])
            {
                [self.selectedCardsArray removeObjectAtIndex:[self.selectedCardsArray indexOfObject:card]];
            }
            
            if (self.selectedCardsArray.count > 1)
            {
                TAJCardView *tempCard = self.selectedCardsArray[0];
                tempCard.isSelected =YES;
            }
            else if(self.selectedCardsArray.count == 1)
            {
                TAJCardView *tempCard = self.selectedCardsArray[0];
                tempCard.isSelected = YES;
                [self animateCard:tempCard];
            }
            else if(self.selectedCardsArray.count == 0)
            {
                previousCard.isSelected = NO;
                [self animateCard:previousCard];
            }
        }
        else if (self.noOfTableCards == 14)
        {
            card.isSelected = YES;
            [self animateCard:card];
        }
    }
    [self updateDiscardButton];
    [self updateGroupCardsButton];
    [self updateMeldButton];
    [self didSelectCards:self.selectedCardsArray];
}

- (NSString *)getJokerCardNumber
{
    NSString *jokerCardNumber = nil;
    jokerCardNumber = [self.jokerCard cardNumber];
    return jokerCardNumber;
}

// Single tap momentory card
- (void)didSelectCard:(TAJCardView *)card
{
    if (!self.openDeckAutomaticDiscardCard)
    {
        NSString *selectedCardNumber = [card cardNumber];
        // If joker is at the open deck dont allow to pick it up, but allow if it is first time
        if (([[self.jokerCard cardNumber] isEqualToString:@"0"] && [selectedCardNumber isEqualToString:@"1"]))
        {
            if (self.firstTimePickOpenCard)
            {
                if ([[TAJUtilities sharedUtilities] isInternetConnected])
                {
                    self.firstTimePickOpenCard = NO;
                    [self pickCardFromOpenDeck:card];
                }
            }
            else if(self.isItMyTurn)
            {
                if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
                {
                    [self.delegate showExtraMessageDelegate:CANT_PICK_JOKERCARD];
                }
                
                [self showExtraMessage:CANT_PICK_JOKERCARD];
            }
            else if (!self.isItMyTurn)
            {
                if (self.delegate && [self.delegate respondsToSelector:@selector(removeInfoLabelMessageDelegate)])
                {
                    [self.delegate removeInfoLabelMessageDelegate];
                }
                if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
                {
                    [self.delegate showExtraMessageDelegate:INFO_NOT_MYTURN];
                }
                
                [self removeInfoLabelMessage];
                [self showExtraMessage:INFO_NOT_MYTURN];
            }
        }
        else if (self.firstTimePickOpenCard
                 || (![selectedCardNumber isEqualToString:[self.jokerCard cardNumber]]
                     ^ [selectedCardNumber isEqualToString:@"0"]))
        {
            if ([[TAJUtilities sharedUtilities] isInternetConnected])
            {
                self.firstTimePickOpenCard = NO;
                [self pickCardFromOpenDeck:card];
            }
        }
        else if(self.isItMyTurn)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
            {
                [self.delegate showExtraMessageDelegate:CANT_PICK_JOKERCARD];
            }
            [self showExtraMessage:CANT_PICK_JOKERCARD];
        }
        else if (!self.isItMyTurn)
        {
            [self removeInfoLabelMessage];
            [self showExtraMessage:INFO_NOT_MYTURN];
            if (self.delegate && [self.delegate respondsToSelector:@selector(removeInfoLabelMessageDelegate)])
            {
                [self.delegate removeInfoLabelMessageDelegate];
            }
            if (self.delegate && [self.delegate respondsToSelector:@selector(showExtraMessageDelegate:)])
            {
                [self.delegate showExtraMessageDelegate:INFO_NOT_MYTURN];
            }
            
        }
    }
    else if (self.openDeckAutomaticDiscardCard)
    {
        if (self.delegate && [self.delegate respondsToSelector:@selector(discardCardFromPlayerMode)])
        {
            [self.delegate discardCardFromPlayerMode];
        }
        [self discardButtonAction:self.discardButton];
    }
}

- (void)didSelectClosedDeck
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectClosedDeck)])
    {
        [self.delegate didSelectClosedDeck];
    }
}

- (void)didSelectOpenDeck:(NSString *)cardNum withCardSuit:(NSString *)cardSuit
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didSelectOpenDeck:withCardSuit:)])
    {
        [self.delegate didSelectOpenDeck:cardNum withCardSuit:cardSuit];
    }
}

- (void)didDiscardedCardAtIndex:(NSUInteger)inndex
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didDiscardedCardAtIndex:)])
    {
        [self.delegate didDiscardedCardAtIndex:inndex];
    }
}

- (void)didEnableShowButton:(BOOL)boolValue withDiscardInfo:(NSDictionary *)discardCardInfo
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didEnableShowButton:withDiscardInfo:)])
    {
        [self.delegate didEnableShowButton:boolValue withDiscardInfo:discardCardInfo];
    }
}

- (void)didMeldCards:(NSMutableArray *)array
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(didMeldCards:)])
    {
        [self.delegate didMeldCards:array];
    }
}

- (void)didSelectCards:(NSMutableArray *)array
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(setCurrentSelectedCards:)])
    {
        [self.delegate setCurrentSelectedCards:array];
    }
}

- (void)groupCardsAtIndexes:(NSMutableArray *)indexArray
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(groupCardsAtIndexes:)])
    {
        [self.delegate groupCardsAtIndexes:indexArray];
    }
}

- (void)removeDiscardedCardForMeldSetup:(NSUInteger)index
{
    if (self.delegate && [self.delegate respondsToSelector:@selector(removeDiscardedCardForMeldSetup:)])
    {
        [self.delegate removeDiscardedCardForMeldSetup:index];
    }
}

- (BOOL)canMeldCards
{
    BOOL result = NO;
    if (self.delegate && [self.delegate respondsToSelector:@selector(canMeldCards)])
    {
        result = [self.delegate canMeldCards];
        
    }
    return result;
}

- (void)setIsItMyTurn:(BOOL)isItMyTurn
{
    self.isThisPlayerCardDiscarded = NO;
    DLog(@"set is my turn in lplaytable befiore %d",_isItMyTurn);
    if (isItMyTurn != _isItMyTurn)
    {
        _isItMyTurn = isItMyTurn;
        DLog(@"set is my turn in lplaytable %d",_isItMyTurn);
        
        // If not your turn remove discard button
        [self removeAllButtonsExceptGroup];
        
        if (isItMyTurn)
        {
            [self flipCards];
        }
    }
}

#pragma mark - Update info message -

- (void)showExtraMessage:(NSString *)message
{
    self.extraInfoLabel.text = message;
    self.extraInfoLabeliPhone.text = message;
    
    if ([TAJUtilities isIPhone]) {
        self.extraInfoLabel.hidden = YES;
        self.extraInfoLabel.alpha = 0;
        
        self.extraInfoLabeliPhone.hidden = NO;
        self.extraInfoLabeliPhone.alpha = 0.6f;
    }
    else {
        self.extraInfoLabel.hidden = NO;
        self.extraInfoLabel.alpha = 0.6f;
    }
    
    [self performSelector:@selector(removeExtraMessage) withObject:self afterDelay:2.0];
}

- (void)removeExtraMessage
{
    if ([TAJUtilities isIPhone]) {
        self.extraInfoLabel.hidden = YES;
        self.extraInfoLabel.alpha = 0.0f;
        
        self.extraInfoLabeliPhone.hidden = YES;
        self.extraInfoLabeliPhone.alpha = 0.0f;
    }
    else {
    self.extraInfoLabel.hidden = YES;
    self.extraInfoLabel.alpha = 0.0f;
    }
}

- (void)showInstruction:(NSString *)instruction
{
    [self removeExtraMessage];
    if ([TAJUtilities isIPhone]) {
        self.infoLabel.alpha = 0.6f;
        self.infoLabel.text = instruction;
        self.currentMessage = instruction;
        self.infoLabel.hidden = YES;
        
        self.infoLabeliPhone.text = instruction;
        self.infoLabeliPhone.hidden = NO;
        self.infoLabeliPhone.alpha = 0.6f;
        self.infoLabeliPhone.text = instruction;
    }
    else {
        self.infoLabel.alpha = 0.6f;
        self.infoLabel.text = instruction;
        self.currentMessage = instruction;
        self.infoLabel.hidden = NO;
        self.infoLabeliPhone.hidden = NO;
    }
}

- (void)removeInfoLabelMessage
{
    if ([TAJUtilities isIPhone]) {
        self.infoLabel.alpha = 0.0f;
        self.infoLabel.text = @"";
        self.currentMessage =  @"";
        self.infoLabel.hidden = YES;
        
        self.infoLabeliPhone.text = @"";
        self.infoLabeliPhone.hidden = YES;
    }
    else {
        self.infoLabel.alpha = 0.0f;
        self.infoLabel.text = @"";
        self.currentMessage =  @"";
        self.infoLabel.hidden = YES;
    }
}

- (void)hideInfoLabelMessage
{
    self.infoLabel.alpha = 0.0f;
    self.infoLabeliPhone.alpha = 0.0f;
}

- (void)showInfoLabelMessage
{
    [self removeExtraMessage];
    self.infoLabel.alpha = 0.6f;
    self.infoLabeliPhone.alpha = 0.0f;
}

#pragma mark - Animations -

- (void)animateCard:(TAJCardView *)card
{
    if (!card.isSelected)
    {
        card.userInteractionEnabled = NO;
        
        CGRect cardFrame = card.frame;
        
        cardFrame.origin.y = self.myCardDeckHolder.frame.size.height - cardFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);;
        card.yOffset = 0;
        
        [UIView animateWithDuration:0.2f
                         animations:^
         {
             card.frame = cardFrame;
         }
                         completion:^(BOOL finished)
         {
             card.userInteractionEnabled = YES;
         }];
    }
    else
    {
        card.userInteractionEnabled = NO;
        
        CGRect cardFrame = card.frame;
        
        float offset = 0.0f;
        
        if (self.isItMyTurn && self.noOfTableCards == 14)
        {
            offset = ([TAJUtilities isIPhone] ? CARD_MOVE_UP_OFFSET_Y_IPHONE : CARD_MOVE_UP_OFFSET_Y_IPAD);
        }
        card.yOffset = offset;
        
        cardFrame.origin.y -= offset;
        
        [UIView animateWithDuration:0.2f
                         animations:^
         {
             card.frame = cardFrame;
         }
                         completion:^(BOOL finished)
         {
             card.userInteractionEnabled = YES;
         }];
    }
}

- (void)otherPlayerPickedCardOpenDeckCard
{
    // For animation purposes
    NSDictionary *cardInfo = [self.openDeckCards lastObject];
    
    NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
    NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
    NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
    
    NSArray *nibContents;
    // Place new card
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                owner:nil
                                              options:nil];
    
    TAJCardView *card = nibContents[index];
    [self placeCard:card inHolder:self.openDeckHolder];
    card.delegate = self;
    [card showCard:cardId isMomentary:YES];
    DLog(@"otherplayerpicked card %@",cardId);
    [self cardPickedFromOpenStack:card];
    
}

#pragma mark - User Interation for player on any deck-

- (void)thisPlayerTurnUserInteration
{
    //enable holder if my turn
    self.closedDeckCardHolder.userInteractionEnabled = YES;     //closed stack deck
    self.openDeckHolder.userInteractionEnabled = YES;           //open deck
}

- (void)otherPlayerTurnUserInteration
{
    //disable holder for me if other player turn
    self.closedDeckCardHolder.userInteractionEnabled = NO; //closed stack deck
    self.openDeckHolder.userInteractionEnabled = NO; //open deck
}

- (void)enableUserTouchForPlayerModel:(BOOL)boolValue
{
    self.myCardDeckHolder.hidden = !boolValue;
    self.myCardDeckHolder.userInteractionEnabled = boolValue;
}

#pragma mark - Update meld cards -

- (void)removeRemainingCardsAfterSendCardFromMeld
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            card.hidden = YES;
        }
    }
#else
    //remove remaining cards after send card in meld view
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        card.hidden = YES;
        
    }
#endif
}

- (void)meldSetup
{
    //once click on show button discard the card from selected array
#if CARD_GROUPING_SLOTS
    if (self.selectedCardsArray && self.selectedCardsArray.count == 1)
    {
        TAJCardView *selectedCard = self.selectedCardsArray[0];
        
        //remove my card from array
        [self.selectedCardsArray removeAllObjects];
        
        //remove selected card from my deck
        [self removeDiscardedCardForMeldSetup:[self.myDeckCardsArray indexOfObject:selectedCard]];
    }
#else
    if (self.selectedCardsArray && self.selectedCardsArray.count == 1)
    {
        TAJCardView *selectedCard = self.selectedCardsArray[0];
        
        //remove my card from array
        [self.selectedCardsArray removeAllObjects];
        
        //remove selected card from my deck
        [self removeCardFromMyDeckAtIndex:[self.myDeckCardsArray indexOfObject:selectedCard]];
    }
#endif
}

- (void)meldButtonAction:(UIButton *)sender
{
    if (![self canMeldCards])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.delegate showMeldErrorPopup];
            }
            return;
            
        }
    }
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.selectedCardsArray];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    //at the end remove selected cards
    [self.selectedCardsArray removeAllObjects];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
    [self updateMeldGroupButton];
}

- (void)deselectForSelectedMeldCards
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            card.isSelected = NO;
        }
    }
#else
    //remove remaining cards after send card in meld view
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        card.isSelected = NO;
    }
#endif
}

- (void)updateMeldButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.selectedCardsArray &&
        (self.selectedCardsArray.count == 3 ||
         self.selectedCardsArray.count == 4 ||
         self.selectedCardsArray.count == 5))
    {
        [self removeAllButtons];
        [self removeMeldGroupAllButton];
        //add meld button
        TAJCardView *card = [self.selectedCardsArray lastObject];
        CGRect cardFrame = card.frame;
        float position = 41.0f;
        CGRect buttonFrame;
        if ([TAJUtilities isIPhone])
        {
            position = 24.0f;
            buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x, self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position ,cardFrame.size.width, position);
        }
        else
        {
            buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x-15, self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position + 3,cardFrame.size.width, position);
        }
        
        BOOL canShowMeldButton = YES;
        if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
        {
            canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        }
        //By default show the meld button even if he cannot meld.So 1
        if (1)
        {
            self.meldCardButton = [UIButton buttonWithType:UIButtonTypeCustom];
            
            [self.meldCardButton addTarget:self
                                    action:@selector(meldButtonAction:)
                          forControlEvents:UIControlEventTouchUpInside];
            
            [self.meldCardButton setTitle:@"Meld" forState:UIControlStateNormal];
            
            UIImage *newImage;
            if ([TAJUtilities isIPhone])
            {
                newImage = [UIImage imageNamed:@"discards_bg-568h.png"];
                self.meldCardButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPHONE];
            }
            else
            {
                newImage = [UIImage imageNamed:@"discards_bg.png"];
                self.meldCardButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPAD];
            }
            self.meldCardButton.backgroundColor = [UIColor clearColor];
            [self.meldCardButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
            [self.meldCardButton setBackgroundImage:newImage forState:UIControlStateNormal];
            self.meldCardButton.frame = buttonFrame;
            [self addSubview:self.meldCardButton];
        }
    }
    else
    {
        if (self.meldCardButton)
        {
            [self removeMeldCardButton];
        }
    }
}

- (void)removeMeldCardButton
{
    if (self.meldCardButton)
    {
        [self.meldCardButton removeFromSuperview];
        self.meldCardButton = nil;
    }
}

- (void)meldCancelled
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            card.hidden = NO;
        }
    }
#else
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        card.hidden = NO;
    }
#endif
    
}

- (void)didTapMeldCard:(NSUInteger)placeId isPlayerModeEnabled:(BOOL)boolValue
{
#if CARD_GROUPING_SLOTS
    for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            if (card.placeID == placeId)
            {
                card.hidden = NO;
                break;
            }
        }
    }
#else
    for (TAJCardView *card in self.myDeckCardsArray)
    {
        if (card.placeID == placeId)
        {
            card.hidden = NO;
            break;
        }
    }
#endif
    if (!boolValue)
    {
        [self updateMeldGroupButton];
    }
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    
}
#pragma mark - Touch Events -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    self.touchStartLocation = point;
    DLog(@"the start point %f %f", self.touchStartLocation.x, self.touchStartLocation.y);
//    NSLog(@"the start point %f %f", self.touchStartLocation.x, self.touchStartLocation.y);
    
    self.startRect = card.frame;
    self.leftCard = [self leftCardToCard:card];
    self.rightCard = [self rightCardToCard:card];
}

- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card
{
    UITouch *touch = [touches anyObject];
    CGPoint point = [touch locationInView:self];
    self.isCardMoved = YES;
    CGRect cardRect = [self convertRect:card.frame fromView:self.myCardDeckHolder];
    CGPoint center = card.center;
    float yDifference = 0;
    float xDifference = 0;
    CGFloat limitFactorX;
    if([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            limitFactorX = 0.0f;
        }
        else
        {
            limitFactorX = 0.05f;
        }
    }
    else
    {
        limitFactorX = 0.05f;
    }
    
    if((cardRect.origin.x + cardRect.size.width) > (self.frame.size.width * 0.95))
    {
        center.x = center.x - 1;
    }
    
    else if(cardRect.origin.x < self.frame.size.width * limitFactorX)
    {
        center.x = center.x + 1;
    }
    else
    {
        xDifference = self.touchStartLocation.x - point.x;
        center.x -= xDifference;
    }
    CGFloat limitFactor;
    if([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            limitFactor = 0.98f;
        }
        else
        {
            limitFactor = 1.2f;
        }
        
    }
    else
    {
        limitFactor = 0.98f;
    }
    DLog(@"The height is %f",self.frame.size.height);
    if((cardRect.origin.y + cardRect.size.height) < (self.frame.size.height * limitFactor))
    {
        if((cardRect.origin.y) >= (self.openDeckHolder.frame.origin.y + self.openDeckHolder.frame.size.height))
        {
            yDifference = self.touchStartLocation.y - point.y;
            center.y -= yDifference;
            DLog(@"yDifference % f", yDifference);
        }
        else
        {
            center.y = center.y + 0.4;
        }
    }
    else
    {
        if((cardRect.origin.y + cardRect.size.height) >= (self.frame.size.height * limitFactor))
        {
            center.y = center.y - 0.4;
        }
    }
    
    card.center = center;
    [self updateMeldGroupButton];
    
    self.touchStartLocation = point;
    
    [self removeMeldGroupAllButton];
    
    if (xDifference > 0 && self.leftCard)
    {
        // Left direction
        if ([self.leftCard isKindOfClass:[TAJCardView class]])
        {
            CGPoint leftCenter = self.leftCard.center;
            
            if ((leftCenter.x * 1.05f) >= center.x)
            {
                [self swapCard:card withCard:self.leftCard];
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.leftCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height );
                
                TAJCardView *newLeftCard = [self leftCardToCard:card];
                
                [self animateCard:self.leftCard withNewRect:tempRect];
                
                [self.myCardDeckHolder insertSubview:self.leftCard aboveSubview:card];
                
                self.rightCard = self.leftCard;
                self.leftCard = newLeftCard;
            }
        }
        else if ([self.leftCard isKindOfClass:[UIView class]])
        {
            DLog(@"left card called");
            CGPoint leftCenter = self.leftCard.center;
            
            if ((leftCenter.x * 1.05f) >= center.x)
            {
                DLog(@"left card inside called");
                NSUInteger cardAindex = [self.myDeckCardsArray indexOfObject:card];
                NSUInteger cardBindex = [self.myDeckCardsArray indexOfObject:self.leftCard];
                
                [self.myDeckCardsArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(swapCard:withCard:)])
                {
                    [self.delegate swapCard:cardAindex withCard:cardBindex];
                }
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.leftCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJCardView *newLeftCard = [self leftCardToCard:card];
                
                [self animateCard:self.leftCard withNewRect:tempRect];
                
                [self.myCardDeckHolder insertSubview:self.leftCard aboveSubview:card];
                
                self.rightCard = self.leftCard;
                self.leftCard = newLeftCard;
            }
        }
    }
    else if (xDifference < 0 && self.rightCard)
    {
        // right direction
        if ([self.rightCard isKindOfClass:[TAJCardView class]])
        {
            CGPoint rightCenter = self.rightCard.center;
            
            if ((rightCenter.x * 0.95f) <= center.x)
            {
                [self swapCard:card withCard:self.rightCard];
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.rightCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJCardView *newRightCard = [self rightCardToCard:card];
                [self animateCard:self.rightCard withNewRect:tempRect];
                
                [self.myCardDeckHolder insertSubview:self.rightCard belowSubview:card];
                
                self.leftCard = self.rightCard;
                self.rightCard = newRightCard;
            }
        }
        else if ([self.rightCard isKindOfClass:[UIView class]])
        {
            CGPoint rightCenter = self.rightCard.center;
            
            if ((rightCenter.x * 0.95f) <= center.x)
            {
                NSUInteger cardAindex = [self.myDeckCardsArray indexOfObject:card];
                NSUInteger cardBindex = [self.myDeckCardsArray indexOfObject:self.rightCard];
                
                [self.myDeckCardsArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
                
                if (self.delegate && [self.delegate respondsToSelector:@selector(swapCard:withCard:)])
                {
                    [self.delegate swapCard:cardAindex withCard:cardBindex];
                }
                
                CGRect tempRect = self.startRect;
                
                self.startRect = self.rightCard.frame;
                self.startRect = CGRectMake(self.startRect.origin.x, tempRect.origin.y,
                                            self.startRect.size.width, self.startRect.size.height);
                
                TAJCardView *newRightCard = [self rightCardToCard:card];
                [self animateCard:self.rightCard withNewRect:tempRect];
                
                [self.myCardDeckHolder insertSubview:self.rightCard belowSubview:card];
                
                self.leftCard = self.rightCard;
                self.rightCard = newRightCard;
            }
        }
    }
    [self updateGroupCardsButton];
    [self updateDiscardButton];
    //lingam
    [self updateMeldButton];
}

- (void)touchesEnded:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card
{
    DLog(@"card ended %f",card.frame.origin.x);
    CGRect cardFrame = card.frame;
    
    if(self.isCardMoved)
    {
        cardFrame.origin.y = self.myCardDeckHolder.frame.size.height - cardFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);
        card.frame = cardFrame;
    }
    
    [self animateCard:card withNewRect:self.startRect];
    self.leftCard = nil;
    self.rightCard = nil;
    
    self.isCardMoved = NO;
    //lingam
    if(![self.delegate playerShowsCard]){
        
    }
    
    [self updateMeldGroupButton];
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(resetCardSlots)])
    {
        [self.delegate resetCardSlots];
    }
    
    
}

- (void)touchesCancelled:(NSSet *)touches withEvent:(UIEvent *)event withCard:(TAJCardView *)card
{
    DLog(@"Touch Cancelled");
}

- (void)animateCard:(TAJCardView *)card withNewRect:(CGRect)newRect
{
    card.userInteractionEnabled = NO;
    
    CGRect oldRect = card.frame;
    newRect.origin.y = oldRect.origin.y;
    [UIView animateWithDuration:0.05f
                     animations:^
     {
         card.frame = newRect;
     }
                     completion:^(BOOL finished)
     {
         card.userInteractionEnabled = YES;
     }];
    
}

// Touch helper methods
- (TAJCardView *)leftCardToCard:(TAJCardView *)centerCard
{
    TAJCardView *leftcard = nil;
#if CARD_GROUPING_SLOTS
    if (self.myDeckCardsArray)
    {
        NSUInteger index = [self.myDeckCardsArray indexOfObject:centerCard];
        
        if (self.myDeckCardsArray.count > index && index != 0)
        {
            if ([self isCardExistAtSlotIndex:index])
            {
                leftcard = [self.myDeckCardsArray objectAtIndex:index - 1];
            }
            else
            {
                leftcard = [self.myDeckCardsArray objectAtIndex:index - 1];
            }
        }
    }
#else
    if (self.myDeckCardsArray)
    {
        NSUInteger index = [self.myDeckCardsArray indexOfObject:centerCard];
        
        if (self.myDeckCardsArray.count > index && index != 0)
        {
            leftcard = [self.myDeckCardsArray objectAtIndex:index - 1];
        }
    }
    
#endif
    return leftcard;
}

- (TAJCardView *)rightCardToCard:(TAJCardView *)centerCard
{
    TAJCardView *rightcard;
#if CARD_GROUPING_SLOTS
    if (self.myDeckCardsArray)
    {
        NSUInteger index = [self.myDeckCardsArray indexOfObject:centerCard];
        if (self.myDeckCardsArray.count > (index + 1))
        {
            if ([self isCardExistAtSlotIndex:(index + 1)])
            {
                rightcard = [self.myDeckCardsArray objectAtIndex:index + 1];
            }
            else
            {
                rightcard = [self.myDeckCardsArray objectAtIndex:index + 1];
            }
        }
    }
#else
    if (self.myDeckCardsArray)
    {
        NSUInteger index = [self.myDeckCardsArray indexOfObject:centerCard];
        
        if (self.myDeckCardsArray.count > (index + 1))
        {
            rightcard = [self.myDeckCardsArray objectAtIndex:index + 1];
        }
    }
#endif
    return rightcard;
}

- (void)swapCard:(TAJCardView *)cardA withCard:(TAJCardView *)cardB
{
    if (cardA && cardB)
    {
        NSUInteger cardAindex = [self.myDeckCardsArray indexOfObject:cardA];
        NSUInteger cardBindex = [self.myDeckCardsArray indexOfObject:cardB];
        
        [self.myDeckCardsArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
        
        if (self.delegate && [self.delegate respondsToSelector:@selector(swapCard:withCard:)])
        {
            [self.delegate swapCard:cardAindex withCard:cardBindex];
        }
    }
}

- (void)uselectOtherCardsExceptCard:(TAJCardView *)card
{
    for (int i = 0; i < self.selectedCardsArray.count; i++)
    {
        TAJCardView *otherCard = self.selectedCardsArray[i];
        if (otherCard != card)
        {
            [otherCard unselectCard];
        }
    }
}

- (void)removeAllButtons
{
    [self removeCardDiscardButton];
    [self removeMeldCardButton];
    [self removeGroupButton];
}

- (void)removeAllButtonsExceptDiscard
{
    [self removeMeldCardButton];
    [self removeGroupButton];
}

- (void)removeAllButtonsExceptGroup
{
    [self removeCardDiscardButton];
    [self removeMeldCardButton];
}

- (void)removeAllButtonsWithMeldGroup
{
    [self removeAllButtons];
    [self removeMeldGroupAllButton];
}

- (void)removeAllMeldButtonsExceptGroup
{
    [self removeCardDiscardButton];
    [self removeMeldGroupAllButton];
}


- (void)flipCards
{
    if (!self.areCardsFlipped && !self.isItMyTurn)
    {
        [self removeAllButtons];
#if CARD_GROUPING_SLOTS
        for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
        {
            if ([self isCardExistAtSlotIndex:i])
            {
                TAJCardView *card = self.myDeckCardsArray[i];
                [card flipCard:YES];
            }
        }
#else
        for (TAJCardView *eachCard in self.myDeckCardsArray)
        {
            [eachCard flipCard:YES];
        }
#endif
        self.areCardsFlipped = YES;
        self.myCardDeckHolder.userInteractionEnabled = NO;
    }
    else if (self.areCardsFlipped && !self.isItMyTurn)
    {
#if CARD_GROUPING_SLOTS
        for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
        {
            if ([self isCardExistAtSlotIndex:i])
            {
                TAJCardView *card = self.myDeckCardsArray[i];
                [card flipCard:NO];
            }
        }
#else
        for (TAJCardView *eachCard in self.myDeckCardsArray)
        {
            [eachCard flipCard:NO];
        }
#endif
        self.areCardsFlipped = NO;
        self.myCardDeckHolder.userInteractionEnabled = YES;
    }
    else if (self.areCardsFlipped)
    {
#if CARD_GROUPING_SLOTS
        for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
        {
            if ([self isCardExistAtSlotIndex:i])
            {
                TAJCardView *card = self.myDeckCardsArray[i];
                [card flipCard:NO];
            }
        }
#else
        for (TAJCardView *eachCard in self.myDeckCardsArray)
        {
            [eachCard flipCard:NO];
        }
#endif
        self.areCardsFlipped = NO;
        self.myCardDeckHolder.userInteractionEnabled = YES;
    }
}

#pragma mark - Group Related methods
- (void)removeGroupButton
{
    if (self.groupCardsButton)
    {
        [self.groupCardsButton removeFromSuperview];
        self.groupCardsButton = nil;
    }
}

- (void)updateGroupCardsButton
{
    if (![self canMeldCards] &&
        (self.noOfTableCards == 13 || self.noOfTableCards == 14) &&
        self.selectedCardsArray &&
        self.selectedCardsArray.count > 1)
    {
        [self removeGroupButton];
        TAJCardView *card = [self.selectedCardsArray lastObject];
        // First create button
        CGRect cardFrame = card.frame;
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 25.0f;
        }
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width+10.0, position);
        self.groupCardsButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.groupCardsButton.exclusiveTouch = YES;
        [self.groupCardsButton addTarget:self
                                  action:@selector(grouButtonAction:)
                        forControlEvents:UIControlEventTouchUpInside];
        [self.groupCardsButton setTitle:@"Group" forState:UIControlStateNormal];
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:@"discards_bg-568h.png"];
            self.groupCardsButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:@"discards_bg~ipad.png"];
            self.groupCardsButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_GENERAL_BUTTON_FONT_SIZE_IPAD];
        }
        
        self.groupCardsButton.backgroundColor = [UIColor clearColor];
        self.groupCardsButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentCenter;
        [self.groupCardsButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.groupCardsButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.groupCardsButton.frame = buttonFrame;
        
        [self addSubview:self.groupCardsButton];
    }
    else if (![self canMeldCards] &&
             self.selectedCardsArray &&
             self.selectedCardsArray.count < 2 &&
             self.groupCardsButton)
    {
        [self removeGroupButton];
    }
    else
    {
        [self removeGroupButton];
    }
}

- (void)grouButtonAction:(UIButton *)button
{
    // Place cards in selected array at the beginning
#if CARD_GROUPING_SLOTS
    if (![self canMeldCards] &&
        self.selectedCardsArray && self.selectedCardsArray.count > 1)
    {
        [self removeGroupButton];
        // Save this to inform controller
        NSMutableArray *indexesArray = [NSMutableArray array];
        for (TAJCardView *card in self.selectedCardsArray)
        {
            [indexesArray addObject:[NSNumber numberWithUnsignedInteger:[self.myDeckCardsArray indexOfObject:card]]];
            DLog(@"card id selected %@",[card cardId]);
            BOOL notifyToDelegate = NO;
            [card unselectCardAndNotify:notifyToDelegate];
            [card removeFromSuperview];
        }
        
        [self.selectedCardsArray removeAllObjects];
        DLog(@"selected index array %@",indexesArray);
        [self groupCardsAtIndexes:indexesArray];
    }
#else
    
    if (![self canMeldCards] &&
        self.selectedCardsArray && self.selectedCardsArray.count > 1)
    {
        [self removeGroupButton];
        // Save this to inform controller
        NSMutableArray *indexesArray = [NSMutableArray array];
        for (TAJCardView *card in self.selectedCardsArray)
        {
            [indexesArray addObject:[NSNumber numberWithUnsignedInteger:[self.myDeckCardsArray indexOfObject:card]]];
            BOOL notifyToDelegate = NO;
            [card unselectCardAndNotify:notifyToDelegate];
            [card removeFromSuperview];
        }
        
        [self.myDeckCardsArray removeObjectsInArray:self.selectedCardsArray];
        NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, self.selectedCardsArray.count)];
        [self.myDeckCardsArray insertObjects:self.selectedCardsArray atIndexes:indexSet];
        [self.selectedCardsArray removeAllObjects];
        [self groupCardsAtIndexes:indexesArray];
        [self rearrangeMyDeckCards];
    }
#endif
    
}

#pragma mark - RESTART GAME SETUP -

- (void)restartGameSetupForGameResult
{
    
}

- (void)restartGameSetupForGameEnd
{
    // reset all properties
    [self removeAllButtons];
    self.canMeldGroupCards = NO;
    self.openDeckAutomaticDiscardCard = NO;
    [self.firstMeldGroupSelectedCards removeAllObjects];
    [self.secondMeldGroupSelectedCards removeAllObjects];
    [self.thirdMeldGroupSelectedCards removeAllObjects];
    [self.fourthMeldGroupSelectedCards removeAllObjects];
    [self.fifthMeldGroupSelectedCards removeAllObjects];
    [self.sixthMeldGroupSelectedCards removeAllObjects];
    [self removeMeldGroupAllButton];
    [self updateClosedDeckCardHolder:YES];
    [self clearCardsinClosedCard];
    if (self.tossCardsArray &&
        self.tossCardsArray.count > 0)
    {
        for (TAJCardView *eachCard in self.tossCardsArray)
        {
            [eachCard removeFromSuperview];
        }
        [self.tossCardsArray removeAllObjects];
    }
    if (self.jokerCard)
    {
        [self.jokerCard removeFromSuperview];
    }
    [self otherPlayerTurnUserInteration];
    self.firstTimePickOpenCard = YES;
    [self removeOpenDeckHolderCards];
    if (self.selectedCardsArray)
    {
        [self.selectedCardsArray removeAllObjects];
    }
    [self removeMyDeckCardsFromHolder];
    [self.hiddenCardWhenMeld removeAllObjects];
    self.leftCard = nil;
    self.rightCard = nil;
}

- (void)removeOpenDeckHolderCards
{
    if (self.openDeckCards &&
        self.openDeckCards.count > 0)
    {
        if (self.openDeckHolder && self.openDeckHolder.subviews.count >0)
        {
            for (TAJCardView *card in self.openDeckHolder.subviews)
            {
                [card removeFromSuperview];
            }
        }
        
        [self.openDeckCards removeAllObjects];
    }
}

- (void)removeJokerHolderCards
{
    [self.jokerCard removeFromSuperview];
}

- (void)removeAnimatedCardFromMyDeck
{
    if (self.myDeckCardsArray &&
        self.myDeckCardsArray.count > 0)
    {
        for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
        {
            if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
            {
                TAJCardView *card = self.myDeckCardsArray[i];
                [card removeFromSuperview];
            }
            else if ([self.myDeckCardsArray[i] isKindOfClass:[UIView class]])
            {
                UIView *view = self.myDeckCardsArray[i];
                [view removeFromSuperview];
            }
        }
        [self.myDeckCardsArray removeAllObjects];
    }
}

- (void)removeMyDeckCardsFromHolder
{
#if CARD_GROUPING_SLOTS
    if (self.myDeckCardsArray &&
        self.myDeckCardsArray.count > 0)
    {
        for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
        {
            if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
            {
                TAJCardView *card = self.myDeckCardsArray[i];
                [card removeFromSuperview];
            }
            else if ([self.myDeckCardsArray[i] isKindOfClass:[UIView class]])
            {
                UIView *view = self.myDeckCardsArray[i];
                [view removeFromSuperview];
            }
        }
        [self.myDeckCardsArray removeAllObjects];
    }
#else
    if (self.myDeckCardsArray &&
        self.myDeckCardsArray.count > 0)
    {
        for (TAJCardView *eachCard in self.myDeckCardsArray)
        {
            [eachCard removeFromSuperview];
        }
        [self.myDeckCardsArray removeAllObjects];
    }
#endif
}

- (void)disableFirstTimePickOpenCard
{
    if (self.firstTimePickOpenCard)
    {
        self.firstTimePickOpenCard = NO;
    }
}

- (void)updateClosedDeckCardHolder:(BOOL)boolValue
{
    self.closedDeckCardHolder.hidden = boolValue;
}

- (void)keepDiscardCardAsClosedCard
{
    // place discard card as a card back image in closed deck once this player meld is success
    // place discard card in closed deck holder not in open deck
    //    [self.closedDeckHolder setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"show_card"]]];
    self.closedDeckHolder.hidden = NO;
}

- (void)clearCardsinClosedCard
{
    [self.closedDeckHolder setBackgroundColor:[UIColor clearColor]];
    self.closedDeckHolder.hidden = YES;
    
}

#pragma mark - Slots Helper Functions-

- (TAJCardView *)getLastCard
{
    TAJCardView *card = nil;
    for (int cardIndex=self.myDeckCardsArray.count-1; cardIndex >=0; cardIndex--)
    {
        if ([self.myDeckCardsArray[cardIndex] isKindOfClass:[TAJCardView class]])
        {
            card = self.myDeckCardsArray[cardIndex];
            break;
        }
    }
    
    return card;
}

- (TAJCardView *)cardAtSlotIndex:(NSInteger)index
{
    TAJCardView *card = nil;
    if ([self.myDeckCardsArray[index] isKindOfClass:[TAJCardView class]])
    {
        card = self.myDeckCardsArray[index];
    }
    return card;
}

- (BOOL)isCardExistAtSlotIndex:(NSInteger)index
{
    if ([self.myDeckCardsArray[index] isKindOfClass:[TAJCardView class]])
    {
        return YES;
    }
    return NO;
}

- (BOOL)isUnhideCardExistAtSlotIndex:(NSInteger)index
{
    if ([self.myDeckCardsArray[index] isKindOfClass:[TAJCardView class]])
    {
        TAJCardView *card = self.myDeckCardsArray[index];
        if (!card.hidden)
        {
            return YES;
        }
        return NO;
    }
    return NO;
}

- (void)removeCardSlotAtIndex:(NSInteger)index
{
    TAJCardView *card = nil;
    card = self.myDeckCardsArray[index];
    if ([self.myDeckCardsArray[index] isKindOfClass:[TAJCardView class]])
    {
        [self.myDeckCardsArray removeObjectAtIndex:index];
    }
}

- (CGPoint)cardPositionAtSlotIndex:(NSUInteger)index
{
    int xPosition = 0;
    
    if ([self.myDeckCardsArray[index] isKindOfClass:[TAJCardView class]])
    {
        if ([TAJUtilities isIPhone])
        {
            xPosition = (index +1) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 );
            //xPosition = (index +1) * SLOT_CARD_FACTOR_IPHONE_5;
        }
        else
        {
            xPosition = (index+1) * SLOT_CARD_FACTOR_IPAD;
            
        }
    }
    CGPoint newPoint = CGPointMake(xPosition, 0);
    return newPoint;
}

- (void)updateSlotCardsOnMyDeck:(NSMutableArray *)myCardDecks
{
    //[self cardsEmptyArray:myCardDecks];
    
    DLog(@"update slot %@",myCardDecks);
    NSLog(@"MYDECK CARDS : %@",myCardDecks);
    NSLog(@"MYDECK CARDS COUNT : %lu",(unsigned long)myCardDecks.count);
    
    [self removeImagesFromMyDeckHolder];
    self.hiddenCardWhenMeld = [NSMutableArray array];
    for (int i = 0; i< self.myDeckCardsArray.count; i++ )
    {
        if ([self.myDeckCardsArray[i] isKindOfClass:[TAJCardView class]])
        {
            TAJCardView *card = self.myDeckCardsArray[i];
            
            if (card.hidden)
            {
                [self.hiddenCardWhenMeld addObject:card];
            }
            [card removeFromSuperview];
        }
        else if ([self.myDeckCardsArray[i] isKindOfClass:[UIView class]])
        {
            UIView *view = self.myDeckCardsArray[i];
            [view removeFromSuperview];
        }
    }
    [self.myDeckCardsArray removeAllObjects];
    [self.selectedCardsArray removeAllObjects];
    
    self.noOfTableCards = 0;

    for (int i = 0; i < myCardDecks.count; i++)
    {
        if ([myCardDecks[i] isKindOfClass:[NSDictionary class]])
        {
            NSDictionary *cardInfo = myCardDecks[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
            NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
            NSUInteger slotindex = [cardInfo[SLOT_KEY] unsignedIntegerValue];
            NSUInteger placeindex = [cardInfo[kTaj_Card_Place_Id] unsignedIntegerValue];
            
            self.noOfTableCards++;
            
            NSArray *nibContents = [NSArray array];
            // Place new card
            int index = 0;
            
            if ([TAJUtilities isIPhone])
            {
                index = [TAJUtilities isItIPhone5]? 0 : 1;
            }
            nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                        owner:nil
                                                      options:nil];
            
            TAJCardView *card = nibContents[index];
            card.delegate = self;
            
            [card showCard:cardId isMomentary:NO];
            if (placeindex==0)
            {
                card.placeID = slotindex;
                
            }
            else
            {
                card.placeID = placeindex;
                
            }
            card.slotID =slotindex;
            
            if ([cardId isEqualToString:self.jokerCardId])
            {
                card.isJoker = YES;
            }
            card.draggable = YES;
            //card.backgroundColor = [UIColor blueColor];
            [self placeSlotCards:card atSlotNumber:i];

            
            [self.myDeckCardsArray addObject:card];
        }
        else if ([myCardDecks[i] isKindOfClass:[NSNull class]])
        {
            UIView *view = [[UIView alloc]init];
            [self placeSlotEmpty:view atSlotNumber:i];
            [self.myDeckCardsArray addObject:view];

       /*     UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 50, 30, 30)];
            [view setBackgroundColor:[UIColor greenColor]];
            [self placeSlotEmpty:view atSlotNumber:i];
            [self.myDeckCardsArray addObject:view];
            /*
            NSArray * nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                                  owner:nil
                                                                options:nil];
            BOOL shouldContinue = false;
            
            TAJCardView *card = nibContents[1];
            card.isJoker = NO;
            card.draggable = NO;
            //card.backgroundColor = [UIColor greenColor];
            //card.cardId = @"";
            
            UIView *view = [[UIView alloc]init];
            
//            CGRect aRect = CGRectMake(0, 0, 20, 90);
//
            
            //[self placeSlotCards:card atSlotNumber:i];
            [self placeSlotEmpty:view atSlotNumber:i];
            [self.myDeckCardsArray addObject:card];
          */
            //[self renderCards];
            
            //[self placeSlotEmpty:card atSlotNumber:i];
            
//            NSDictionary *cardInfo = myCardDecks[i];
//           // NSString *previousCardID = cardInfo[@"card_id"];
//
//            if([previousCard isKindOfClass:[NSNull class]] ) {
//                previousCard = card;
//                shouldContinue = false;
//            }
//            NSLog(@"card ID : %@",card.cardId);
//            NSLog(@"previousCard ID: %@",previousCard.cardId);
//            NSLog(@"shouldContinue %@",[NSNumber numberWithBool:shouldContinue]);
//
//            if (previousCard.cardId == card.cardId) {
//                continue;
//            }
//            if(shouldContinue) {
//                continue;
//            }
//
//            [self.myDeckCardsArray addObject:card];
//
//
//            previousCard.cardId = card.cardId;  // my idea is to compare current card is equal to old card
        }
        
    }
    NSLog(@"UPDATED ARRAY : %@",self.myDeckCardsArray);
//    NSMutableIndexSet *indexes = [NSMutableIndexSet indexSetWithIndex:0];
//    [indexes addIndex:1];
//    [self.myDeckCardsArray removeObjectsAtIndexes:indexes];
//    NSLog(@"REMOVED INDEXES ARRAY : %@",self.myDeckCardsArray);
    [self removeMeldGroupAllButton];
    
}

-(void)createAEmptyRect {
    
    CGRect aRect = CGRectMake(0, 0, 20, 90);

}

-(NSArray*)cardsEmptyArray:(NSArray *)listArray{
    NSMutableArray * updatedArray = [NSMutableArray new];
    
    for (int i = 0; i < listArray.count; i++) {
        if ([listArray[i] isKindOfClass:[NSNull class]])
        {
            continue;
        }
        NSDictionary *cardInfo = listArray[i];
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *slotindex = cardInfo[SLOT_KEY];
        NSString *cardId = [NSString stringWithFormat:@"%@%@%@",
                            cardNum,slotindex,cardType];
        [cardInfo setValue:cardId forKey:@"card_id"];
        [updatedArray addObject:cardInfo];
    }
    
    
    
    return updatedArray;
}

- (void)placeSlotCards:(TAJCardView *)card atSlotNumber:(NSUInteger)index
{
    CGRect holderFrame = self.myCardDeckHolder.frame;
    CGRect cardFrame = card.frame;
    if ([TAJUtilities isIPhone])
    {
        int position = 0;
        if ([TAJUtilities isItIPhone6]) {
            //position = 10;
        }
        cardFrame.origin.x = (index+1) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 ) + position - 10 ;
        //cardFrame.origin.x = (index+1) *  SLOT_CARD_FACTOR_IPHONE_5;
    }
    else
    {
        cardFrame.origin.x = (index+1) * SLOT_CARD_FACTOR_IPAD;
    }
    
    cardFrame.origin.y = holderFrame.size.height - cardFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);
    card.frame = cardFrame;
    
    //card.backgroundColor = [UIColor blueColor];
    [self.myCardDeckHolder addSubview:card];
}

- (void)placeSlotEmpty:(UIView *)view atSlotNumber:(NSUInteger)index
{
    CGRect holderFrame = self.myCardDeckHolder.frame;
    CGRect viewFrame = view.frame;

    if ([TAJUtilities isIPhone])
    {
        //position - ratheesh
        int position = 0;
        if ([TAJUtilities isItIPhone6]) {
            //position = 16;
        }
        viewFrame.origin.x = (index+1) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 ) ;
        //viewFrame.origin.x = (index +1) *  SLOT_CARD_FACTOR_IPHONE_5;
    }
    else
    {
        viewFrame.origin.x = (index +1) * SLOT_CARD_FACTOR_IPAD;

    }

    viewFrame.origin.y = holderFrame.size.height - viewFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 0);

    if ([TAJUtilities isIPhone])
    {
        if ([TAJUtilities isItIPhone5])
        {
            viewFrame.size.width = 50;
            viewFrame.size.height = 71;
        }
        else
        {
            //for 13 cards swaping(draging the cards,changing position left/right)
            viewFrame.size.width = 58;//50
            viewFrame.size.height = 90; // 82 - recent (28th may);//71 - very old
        }
    }
    else
    {
        viewFrame.size.width = 110;//103;
        viewFrame.size.height = 150;//143;
    }

    view.frame = viewFrame;
    //view.backgroundColor = [UIColor redColor];
    [self.myCardDeckHolder addSubview:view];
}

//- (void)placeSlotEmpty:(TAJCardView *)view atSlotNumber:(NSUInteger)index isGroup:(BOOL)isGroup
//{
//    CGRect holderFrame = self.myCardDeckHolder.frame;
//    CGRect viewFrame = view.frame;
//
//    if ([TAJUtilities isIPhone])
//    {
//        //position - ratheesh
//        int position = 0;
//        if ([TAJUtilities isItIPhone6]) {
//            position = 16;
//        }
//        viewFrame.origin.x = (index+1) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 ) + position;
//        //viewFrame.origin.x = (index +1) *  SLOT_CARD_FACTOR_IPHONE_5;
//    }
//    else
//    {
//        viewFrame.origin.x = (index +1) * SLOT_CARD_FACTOR_IPAD;
//    }
//
//    viewFrame.origin.y = holderFrame.size.height - viewFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);
//
//    if ([TAJUtilities isIPhone])
//    {
//        if ([TAJUtilities isItIPhone5])
//        {
//            viewFrame.size.width = 50;
//            viewFrame.size.height = 70;
//        }
//        else
//        {
//            viewFrame.size.width = 100;
//            viewFrame.size.height = 90;
//        }
//    }
//    else
//    {
//        viewFrame.size.width = 110;
//        viewFrame.size.height = 150;
//    }
//
//    view.frame = viewFrame;
//
//    [self.myCardDeckHolder addSubview:view];
//}


- (void)swapEmptyLeftSlotFromCard:(TAJCardView *)leftCard
{
    if (leftCard)
    {
        DLog(@"self.mydeck card %@",self.myDeckCardsArray);
        NSUInteger cardAindex = [self.myDeckCardsArray indexOfObject:leftCard];
        NSUInteger i;
        NSUInteger indexOfCardExist = 0;
        
        for (i = (cardAindex - 1); i < self.myDeckCardsArray.count; i--)
        {
            if (![self isCardExistAtSlotIndex:i])
            {
                
            }
            else if ([self isCardExistAtSlotIndex:i])
            {
                indexOfCardExist = i;
                break;
            }
        }
        DLog(@"index of card exist %d",indexOfCardExist);
        if (i == 0)
        {
        }
        else
        {
            [self.myDeckCardsArray exchangeObjectAtIndex:(indexOfCardExist + 1) withObjectAtIndex:cardAindex];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(removeEmptySlotFromIndex:withIndex:)])
            {
                [self.delegate removeEmptySlotFromIndex:(indexOfCardExist + 1) withIndex:cardAindex];
            }
            
        }
    }
}

- (void)swapEmptyRightSlotFromIndex:(TAJCardView *)rightCard
{
    if (rightCard)
    {
        NSUInteger cardAindex = [self.myDeckCardsArray indexOfObject:rightCard];
        NSUInteger i;
        NSUInteger indexOfCardExist = 0;
        
        for (i = (cardAindex + 1); i < self.myDeckCardsArray.count; i++)
        {
            if (![self isCardExistAtSlotIndex:i])
            {
                
            }
            else if ([self isCardExistAtSlotIndex:i])
            {
                indexOfCardExist = i;
                break;
            }
        }
        if (i == ([self.myDeckCardsArray count] - 1))
        {
        }
        else
        {
            [self.myDeckCardsArray exchangeObjectAtIndex:(indexOfCardExist - 1) withObjectAtIndex:cardAindex];
            
            if (self.delegate && [self.delegate respondsToSelector:@selector(removeEmptySlotFromIndex:withIndex:)])
            {
                [self.delegate removeEmptySlotFromIndex:(indexOfCardExist - 1) withIndex:cardAindex];
            }
        }
    }
}

#pragma mark - Fill MyDeckHolder -

-(void)fillAnimationCardWithIndex:(NSInteger)index
{
    UIImageView *cardBackImage = [[UIImageView alloc]init];
    
    UIImage *imageA = [UIImage imageNamed:@"card_back.png"];
    
    cardBackImage.image = imageA;
    CGRect holderFrame = self.myCardDeckHolder.frame;
    CGRect cardBackImageFrame = cardBackImage.frame;
    
    if ([TAJUtilities isIPhone])
    {
        cardBackImageFrame.origin.x =(index+4) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 );
       // cardBackImageFrame.origin.x =(index+4) *  SLOT_CARD_FACTOR_IPHONE_5;
        //50, 71
        cardBackImageFrame.size.width = 50;//60;
        cardBackImageFrame.size.height = 70;//92; // 82
    }
    else
    {
        cardBackImageFrame.origin.x = (index + 1) * ANIMATE_CARD_OFFSET_X_IPAD + 98;
        cardBackImageFrame.size.width = 110;
        cardBackImageFrame.size.height = 150;
    }
    
    cardBackImageFrame.origin.y = holderFrame.size.height - cardBackImageFrame.size.height - ([TAJUtilities isIPhone] ? 0 : 10);
    
    cardBackImage.frame = cardBackImageFrame;
    [self.myCardDeckHolder addSubview:cardBackImage];
}

- (void)removeImagesFromMyDeckHolder
{
    if (self.myCardDeckHolder && self.myCardDeckHolder.subviews.count > 0)
    {
        for (UIImageView *card in self.myCardDeckHolder.subviews)
        {
            if ([card isKindOfClass:[UIImageView class]])
            {
                [card removeFromSuperview];
            }
        }
    }
}

#pragma mark - Update Meld 4 Group cards -

- (void)wantToShowMeldGroupButton:(BOOL)boolValue
{
    self.canMeldGroupCards = boolValue;
}

- (void)updateMeldGroupButton
{
    if ([self canMeldCards] && self.canMeldGroupCards)
    {
        [self removeMeldGroupAllButton];
        [self removeMeldCardButton];
        
        NSUInteger indexOfFirstCardExist = 0;
        NSUInteger indexOfNullExist = 0;
        NSUInteger indexOfSecondNullExist = 0;
        NSUInteger indexOfSecondCardExist = 0;
        NSUInteger indexOfThirdNullExist = 0;
        NSUInteger indexOfThirdCardExist = 0;
        NSUInteger indexOfFourthNullExist = 0;
        NSUInteger indexOfFourthCardExist = 0;
        NSUInteger indexOfFifthNullExist = 0;
        NSUInteger indexOfFifthCardExist = 0;
        NSUInteger indexOfSixthNullExist = 0;
        NSUInteger indexOfSixthCardExist = 0;
        [self.firstMeldGroupSelectedCards removeAllObjects];
        [self.secondMeldGroupSelectedCards removeAllObjects];
        [self.thirdMeldGroupSelectedCards removeAllObjects];
        [self.fourthMeldGroupSelectedCards removeAllObjects];
        [self.fifthMeldGroupSelectedCards removeAllObjects];
        [self.sixthMeldGroupSelectedCards removeAllObjects];
        
        // For first meld group
        for (NSUInteger i = 0; i < self.myDeckCardsArray.count; i++)
        {
            if ([self isUnhideCardExistAtSlotIndex:i])
            {
                indexOfFirstCardExist = i;
                break;
            }
        }
        
        for (NSUInteger j = indexOfFirstCardExist; j < self.myDeckCardsArray.count; j++)
        {
            if ([self isUnhideCardExistAtSlotIndex:j])
            {
                if (self.firstMeldGroupSelectedCards.count < 12)
                {
                    [self fillFirstMeldGroupCards:self.myDeckCardsArray[j] atIndex:j];
                    indexOfSecondNullExist = j;
                }
                else
                {
                    self.meldGroupFirstButton.hidden = YES;
                    indexOfSecondNullExist = j;
                    break;
                }
            }
            else if(![self isUnhideCardExistAtSlotIndex:j])
            {
                indexOfSecondNullExist = j;
                break;
            }
        }
        // for second meld group
        if (indexOfSecondNullExist != 0)
        {
            for (NSUInteger i = indexOfSecondNullExist; i < self.myDeckCardsArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfSecondCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfSecondCardExist; k < self.myDeckCardsArray.count && indexOfSecondCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.secondMeldGroupSelectedCards.count < 12)
                    {
                        [self fillSecondMeldGroupCards:self.myDeckCardsArray[k] atIndex:k];
                        indexOfThirdNullExist = k;
                    }
                    else
                    {
                        indexOfThirdNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfThirdNullExist = k;
                    break;
                }
            }
        }
        
        // for third meld group
        if (indexOfThirdNullExist != 0)
        {
            for (NSUInteger i = indexOfThirdNullExist; i < self.myDeckCardsArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfThirdCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfThirdCardExist; k < self.myDeckCardsArray.count && indexOfThirdCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.thirdMeldGroupSelectedCards.count < 12)
                    {
                        [self fillThirdMeldGroupCards:self.myDeckCardsArray[k] atIndex:k];
                        indexOfFourthNullExist = k;
                    }
                    else
                    {
                        indexOfFourthNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfFourthNullExist = k;
                    break;
                }
            }
        }
        
        // for fourth meld group
        if (indexOfFourthNullExist != 0)
        {
            for (NSUInteger i = indexOfFourthNullExist; i < self.myDeckCardsArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfFourthCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfFourthCardExist; k < self.myDeckCardsArray.count && indexOfFourthCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.fourthMeldGroupSelectedCards.count < 12)
                    {
                        [self fillFouthMeldGroupCards:self.myDeckCardsArray[k] atIndex:k];
                        indexOfFifthNullExist = k;
                    }
                    else
                    {
                        indexOfNullExist = k;
                        indexOfFifthNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfNullExist = k;
                    indexOfFifthNullExist = k;
                    break;
                }
            }
        }
        //        DLog(@"card found for fourth meld at %lu and null exist %lu",(unsigned long)indexOfFourthCardExist,(unsigned long)indexOfFifthNullExist);
        // for fifth meld group
        if (indexOfFifthNullExist != 0 && indexOfFourthNullExist < indexOfFifthNullExist)
        {
            for (NSUInteger i = indexOfFifthNullExist; i < self.myDeckCardsArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfFifthCardExist = i;
                    break;
                }
            }
            
            for (NSUInteger k = indexOfFifthCardExist; k < self.myDeckCardsArray.count && indexOfFifthCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.fifthMeldGroupSelectedCards.count < 12)
                    {
                        [self fillFifthMeldGroupCards:self.myDeckCardsArray[k] atIndex:k];
                        indexOfSixthNullExist = k;
                    }
                    else
                    {
                        indexOfNullExist = k;
                        indexOfSixthNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfNullExist = k;
                    indexOfSixthNullExist = k;
                    break;
                }
            }
        }
        
        // for sixth meld group
        if (indexOfSixthNullExist != 0 && indexOfFifthNullExist < indexOfSixthNullExist)
        {
            for (NSUInteger i = indexOfSixthNullExist; i < self.myDeckCardsArray.count; i++)
            {
                if ([self isUnhideCardExistAtSlotIndex:i])
                {
                    indexOfSixthCardExist = i;
                    break;
                }
            }
            for (NSUInteger k = indexOfSixthCardExist; k < self.myDeckCardsArray.count && indexOfSixthCardExist != 0; k++)
            {
                if ([self isUnhideCardExistAtSlotIndex:k])
                {
                    if (self.sixthMeldGroupSelectedCards.count < 12)
                    {
                        [self fillSixthMeldGroupCards:self.myDeckCardsArray[k] atIndex:k];
                        indexOfNullExist = k;
                    }
                    else
                    {
                        indexOfNullExist = k;
                        break;
                    }
                }
                else if(![self isUnhideCardExistAtSlotIndex:k])
                {
                    indexOfNullExist = k;
                    break;
                }
            }
        }
        
    }
}

- (void)fillFirstMeldGroupCards:(TAJCardView *)card atIndex:(NSUInteger)index
{
    [self.firstMeldGroupSelectedCards addObject:card];
    if (self.firstMeldGroupSelectedCards.count <= 12)
    {
        [self updateFirstMeldGroupButton];
    }
    
}

- (void)fillSecondMeldGroupCards:(TAJCardView *)card atIndex:(NSUInteger)index
{
    [self.secondMeldGroupSelectedCards addObject:card];
    [self updateSecondMeldGroupButton];
}

- (void)fillThirdMeldGroupCards:(TAJCardView *)card atIndex:(NSUInteger)index
{
    [self.thirdMeldGroupSelectedCards addObject:card];
    [self updateThirdMeldGroupButton];
}

- (void)fillFouthMeldGroupCards:(TAJCardView *)card atIndex:(NSUInteger)index
{
    [self.fourthMeldGroupSelectedCards addObject:card];
    [self updateFourthMeldGroupButton];
}

- (void)fillFifthMeldGroupCards:(TAJCardView *)card atIndex:(NSUInteger)index
{
    [self.fifthMeldGroupSelectedCards addObject:card];
    [self updateFifthMeldGroupButton];
}

- (void)fillSixthMeldGroupCards:(TAJCardView *)card atIndex:(NSUInteger)index
{
    [self.sixthMeldGroupSelectedCards addObject:card];
    [self updateSixthMeldGroupButton];
}

- (void)removeMeldGroupAllButton
{
    [self removeFirstMeldGroupButton];
    [self removeSecondMeldGroupButton];
    [self removeThirdMeldGroupButton];
    [self removeFourthMeldGroupButton];
    [self removeFifthMeldGroupButton];
    [self removeSixthMeldGroupButton];
}

- (void)removeFirstMeldGroupButton
{
    if (self.meldGroupFirstButton)
    {
        [self.meldGroupFirstButton removeFromSuperview];
        self.meldGroupFirstButton = nil;
    }
}

- (void)removeSecondMeldGroupButton
{
    if (self.meldGroupSecondButton)
    {
        [self.meldGroupSecondButton removeFromSuperview];
        self.meldGroupSecondButton = nil;
    }
}

- (void)removeThirdMeldGroupButton
{
    if (self.meldGroupThirdButton)
    {
        [self.meldGroupThirdButton removeFromSuperview];
        self.meldGroupThirdButton = nil;
    }
}

- (void)removeFourthMeldGroupButton
{
    if (self.meldGroupFourthButton)
    {
        [self.meldGroupFourthButton removeFromSuperview];
        self.meldGroupFourthButton = nil;
    }
}

- (void)removeFifthMeldGroupButton
{
    if (self.meldGroupFifthButton)
    {
        [self.meldGroupFifthButton removeFromSuperview];
        self.meldGroupFifthButton = nil;
    }
}

- (void)removeSixthMeldGroupButton
{
    if (self.meldGroupSixthButton)
    {
        [self.meldGroupSixthButton removeFromSuperview];
        self.meldGroupSixthButton = nil;
    }
}

- (void)updateFirstMeldGroupButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.firstMeldGroupSelectedCards &&
        (self.firstMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllButtons];
        
        [self removeFirstMeldGroupButton];
        //add Meld Group button
        
        
        TAJCardView *card = [self.firstMeldGroupSelectedCards objectAtIndex:(self.firstMeldGroupSelectedCards.count/2)];
        
        CGRect cardFrame = card.frame;
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 17.0f;
        }
        
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width + ([TAJUtilities isIPhone] ? 10 : 15), position);
        
        self.meldGroupFirstButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.meldGroupFirstButton.exclusiveTouch = YES;
        [self.meldGroupFirstButton addTarget:self
                                      action:@selector(meldGroupFirstButtonAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupFirstButton setTitle:@"Meld Group" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE];
            self.meldGroupFirstButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPAD];
            self.meldGroupFirstButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldGroupFirstButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupFirstButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.meldGroupFirstButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupFirstButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupFirstButton];
    }
    else
    {
        [self removeFirstMeldGroupButton];
    }
}

- (void)updateSecondMeldGroupButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.secondMeldGroupSelectedCards &&
        (self.secondMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllButtons];
        [self removeSecondMeldGroupButton];
        
        //add Meld Group button
        
        TAJCardView *card = [self.secondMeldGroupSelectedCards objectAtIndex:(self.secondMeldGroupSelectedCards.count/2)];
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 17.0f;
        }
        
        CGRect cardFrame = card.frame;
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width + ([TAJUtilities isIPhone] ? 10 : 15), position);
        
        self.meldGroupSecondButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.meldGroupSecondButton.exclusiveTouch = YES;
        [self.meldGroupSecondButton addTarget:self
                                       action:@selector(meldGroupSecondButtonAction:)
                             forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupSecondButton setTitle:@"Meld Group" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE];
            self.meldGroupSecondButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPAD];
            self.meldGroupSecondButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldGroupSecondButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupSecondButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        [self.meldGroupSecondButton setBackgroundImage:newImage forState:UIControlStateNormal];
        self.meldGroupSecondButton.frame = buttonFrame;
        [self addSubview:self.meldGroupSecondButton];
    }
    else
    {
        [self removeSecondMeldGroupButton];
    }
}

- (void)updateThirdMeldGroupButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.thirdMeldGroupSelectedCards &&
        (self.thirdMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllButtons];
        [self removeThirdMeldGroupButton];
        
        //add Meld Group button
        
        TAJCardView *card = [self.thirdMeldGroupSelectedCards objectAtIndex:(self.thirdMeldGroupSelectedCards.count/2)];
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 17.0f;
        }
        
        CGRect cardFrame = card.frame;
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width + ([TAJUtilities isIPhone] ? 10 : 15), position);
        
        self.meldGroupThirdButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.meldGroupThirdButton.exclusiveTouch = YES;
        [self.meldGroupThirdButton addTarget:self
                                      action:@selector(meldGroupThirdButtonAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupThirdButton setTitle:@"Meld Group" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE];
            self.meldGroupThirdButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPAD];
            self.meldGroupThirdButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldGroupThirdButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupThirdButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.meldGroupThirdButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupThirdButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupThirdButton];
    }
    else
    {
        [self removeThirdMeldGroupButton];
    }
}

- (void)updateFourthMeldGroupButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.fourthMeldGroupSelectedCards &&
        (self.fourthMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllButtons];
        [self removeFourthMeldGroupButton];
        
        //add Meld Group button
        
        TAJCardView *card = [self.fourthMeldGroupSelectedCards objectAtIndex:(self.fourthMeldGroupSelectedCards.count/2)];
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 17.0f;
        }
        
        CGRect cardFrame = card.frame;
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width + ([TAJUtilities isIPhone] ? 10 : 15), position);
        
        self.meldGroupFourthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.meldGroupFourthButton.exclusiveTouch = YES;
        
        [self.meldGroupFourthButton addTarget:self
                                       action:@selector(meldGroupFourthButtonAction:)
                             forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupFourthButton setTitle:@"Meld Group" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE];
            self.meldGroupFourthButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPAD];
            self.meldGroupFourthButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldGroupFourthButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupFourthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.meldGroupFourthButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupFourthButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupFourthButton];
    }
    else
    {
        [self removeFourthMeldGroupButton];
    }
}

- (void)updateFifthMeldGroupButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.fifthMeldGroupSelectedCards &&
        (self.fifthMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllButtons];
        [self removeFifthMeldGroupButton];
        
        //add Meld Group button
        
        TAJCardView *card = [self.fifthMeldGroupSelectedCards objectAtIndex:(self.fifthMeldGroupSelectedCards.count/2)];
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 17.0f;
        }
        CGRect cardFrame = card.frame;
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width + ([TAJUtilities isIPhone] ? 10 : 15), position);
        self.meldGroupFifthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.meldGroupFifthButton.exclusiveTouch = YES;
        [self.meldGroupFifthButton addTarget:self
                                      action:@selector(meldGroupFifthButtonAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        [self.meldGroupFifthButton setTitle:@"Meld Group" forState:UIControlStateNormal];
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE];
            self.meldGroupFifthButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPAD];
            self.meldGroupFifthButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldGroupFifthButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupFifthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.meldGroupFifthButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupFifthButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupFifthButton];
    }
    else
    {
        [self removeFifthMeldGroupButton];
    }
}

- (void)updateSixthMeldGroupButton
{
    if ([self canMeldCards] &&
        self.myDeckCardsArray && self.noOfTableCards == 13 &&
        self.sixthMeldGroupSelectedCards &&
        (self.sixthMeldGroupSelectedCards.count >= 3))
    {
        [self removeAllButtons];
        [self removeSixthMeldGroupButton];
        
        //add Meld Group button
        
        TAJCardView *card = [self.sixthMeldGroupSelectedCards objectAtIndex:(self.sixthMeldGroupSelectedCards.count/2)];
        
        float position = 41.0f;
        if ([TAJUtilities isIPhone])
        {
            position = 17.0f;
        }
        
        CGRect cardFrame = card.frame;
        CGRect buttonFrame = CGRectMake(self.myCardDeckHolder.frame.origin.x + cardFrame.origin.x,
                                        self.myCardDeckHolder.frame.origin.y + cardFrame.origin.y - position,
                                        cardFrame.size.width + ([TAJUtilities isIPhone] ? 10 : 15), position);
        
        self.meldGroupSixthButton = [UIButton buttonWithType:UIButtonTypeCustom];
        self.meldGroupSixthButton.exclusiveTouch = YES;
        
        [self.meldGroupSixthButton addTarget:self
                                      action:@selector(meldGroupSixthButtonAction:)
                            forControlEvents:UIControlEventTouchUpInside];
        
        [self.meldGroupSixthButton setTitle:@"Meld Group" forState:UIControlStateNormal];
        
        UIImage *newImage;
        if ([TAJUtilities isIPhone])
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPHONE];
            self.meldGroupSixthButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPHONE];
        }
        else
        {
            newImage = [UIImage imageNamed:PLAY_TABLE_MELD_GROUP_IMAGE_IPAD];
            self.meldGroupSixthButton.titleLabel.font = [UIFont fontWithName:PLAY_TABLE_ALL_BUTTON_FONT_NAME size:PLAY_TABLE_MELD_GROUP_BUTTON_FONT_SIZE_IPAD];
        }
        self.meldGroupSixthButton.backgroundColor = [UIColor clearColor];
        [self.meldGroupSixthButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        
        [self.meldGroupSixthButton setBackgroundImage:newImage forState:UIControlStateNormal];
        
        self.meldGroupSixthButton.frame = buttonFrame;
        
        [self addSubview:self.meldGroupSixthButton];
    }
    else
    {
        [self removeSixthMeldGroupButton];
    }
}

- (void)meldGroupFirstButtonAction:(UIButton *)button
{
    if (![self canMeldCards])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.delegate showMeldErrorPopup];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.firstMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.firstMeldGroupSelectedCards removeAllObjects];
    
    [self removeFirstMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
}

- (void)meldGroupSecondButtonAction:(UIButton *)button
{
    if (![self canMeldCards])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.delegate showMeldErrorPopup];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.secondMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.secondMeldGroupSelectedCards removeAllObjects];
    
    [self removeSecondMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
}

- (void)meldGroupThirdButtonAction:(UIButton *)button
{
    if (![self canMeldCards])
    {
        return;
    }
    BOOL canShowMeldButton=YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                [self.delegate showMeldErrorPopup];
            }
            return;
        }
    }
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.thirdMeldGroupSelectedCards];
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.thirdMeldGroupSelectedCards removeAllObjects];
    
    [self removeThirdMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
}

- (void)meldGroupFourthButtonAction:(UIButton *)button
{
    if (![self canMeldCards])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                [self.delegate showMeldErrorPopup];
            }
            return;
        }
    }
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.fourthMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
        
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    
    //at the end remove selected cards
    [self.fourthMeldGroupSelectedCards removeAllObjects];
    
    [self removeFourthMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
}

- (void)meldGroupFifthButtonAction:(UIButton *)button
{
    if (![self canMeldCards])
    {
        return;
    }
    
    BOOL canShowMeldButton=YES;
    
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.delegate showMeldErrorPopup];
            }
            return;
            
        }
    }
    
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.fifthMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
        card.hidden = YES;
        [card unselectCard];
    }
    tempSelectedCards = nil;
    //at the end remove selected cards
    [self.fifthMeldGroupSelectedCards removeAllObjects];
    [self removeFifthMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
}

- (void)meldGroupSixthButtonAction:(UIButton *)button
{
    if (![self canMeldCards])
    {
        return;
    }
    BOOL canShowMeldButton=YES;
    if (self.delegate && [self.delegate respondsToSelector:@selector(getIsAnyMeldSlotEmpty)])
    {
        canShowMeldButton=[self.delegate getIsAnyMeldSlotEmpty];
        if (!canShowMeldButton)
        {
            if (self.delegate && [self.delegate respondsToSelector:@selector(showMeldErrorPopup)])
            {
                
                [self.delegate showMeldErrorPopup];
            }
            return;
        }
    }
    NSMutableArray *meldCardArray = [NSMutableArray array];
    NSMutableArray *tempSelectedCards = [NSMutableArray arrayWithArray:self.sixthMeldGroupSelectedCards];
    
    for (int i = 0; i < tempSelectedCards.count; i++)
    {
        TAJCardView *card = tempSelectedCards[i];
        [meldCardArray addObject:@{kTaj_Card_Face_Key: [card cardNumber],
                                   kTaj_Card_Suit_Key: [[card cardSuit] uppercaseString],
                                   SLOT_KEY: [NSNumber numberWithUnsignedInteger:[card slotID]],
                                   kTaj_Card_Place_Id: [NSNumber numberWithUnsignedInteger:[card placeID]]}];
        card.hidden = YES;
        [card unselectCard];
    }
    
    tempSelectedCards = nil;
    //at the end remove selected cards
    [self.sixthMeldGroupSelectedCards removeAllObjects];
    [self removeSixthMeldGroupButton];
    //pass array of dictionary cards for meld
    [self didMeldCards:meldCardArray];
}

- (NSString *)getJokerCardNumberForIamBackCell
{
    NSString *jokerCardNumber = nil;
    jokerCardNumber = [self.jokerCard cardNumber];
    DLog(@"[self.jokerCard cardNumber] = %@",[self.jokerCard cardNumber]);
    DLog(@"jokerCardNumber = %@",jokerCardNumber);
    return jokerCardNumber;
}


@end
