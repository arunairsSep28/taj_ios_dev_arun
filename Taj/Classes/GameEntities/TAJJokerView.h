/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJJokerView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 22/03/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJInfoPopupViewController.h"
@protocol TAJJokerViewDelegate;

@interface TAJJokerView : UIView <UIAlertViewDelegate,InfoPopupViewDelegate>

@property (weak, nonatomic) id<TAJJokerViewDelegate> jokerDelegate;
@property (strong, nonatomic) TAJInfoPopupViewController *infoPopupViewController;
- (void)updateJokerView:(NSMutableDictionary *)dictionary withBet:(int)previousBetValue gameType:(NSString *)gameType;

@end

@protocol TAJJokerViewDelegate <NSObject>

- (void)didSelectJoinJoker:(NSString *)buyInAmount;
- (void)didSelectCancelJoker;
- (void)didJokerViewTextFieldStartEditing;
- (void)didJokerViewTextFieldEndEditing;
- (void)showErrorAlert:(NSString *)message;
@end
