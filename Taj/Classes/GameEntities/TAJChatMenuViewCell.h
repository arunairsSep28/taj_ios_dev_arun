/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatMenuViewCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogish Poojary on 31/07/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJChatMenuViewCellDelegate<NSObject>
-(void)chatMenuButton_p:(NSInteger)buttonId;
@end
@interface TAJChatMenuViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *defaultTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondTextLabel;
@property (weak, nonatomic) IBOutlet UIButton *chatDefaultTextButton;
@property (strong, nonatomic) id<TAJChatMenuViewCellDelegate> chatMenuCellDelegate;

@end
