/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChairView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 18/06/14.
 Created by Sujit Yadawad on 04/02/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJPlayerModelProtocol;

@interface TAJPlayerModel : NSObject

@property (nonatomic, strong) NSString *playerName;
@property (nonatomic, strong) NSString *playerID;
@property (nonatomic, strong) NSString *tableID;
@property (nonatomic)       BOOL    isGameWatcher;
@property (nonatomic, strong) NSString *placeID;
@property (nonatomic, strong) NSString *playerPoints;
@property (nonatomic, strong) NSString *playerGender;
@property (nonatomic, strong) NSString *playerType;
@property (nonatomic, strong) NSString *playerCharNumber;
@property (nonatomic, strong) NSString *playerGameAppraisalRate;
@property (nonatomic, weak) id <TAJPlayerModelProtocol> delegate;
@property (nonatomic) BOOL isThisDevicePlayer;
//device info
@property (nonatomic,strong) NSString *playerDeviceId;
- (id)initWithDictionary:(NSDictionary *)dictionary;
@end
@protocol TAJPlayerModelProtocol <NSObject>

- (void)didChangePlaceID;
- (void)isPlayerWatchingGame;
- (void)showAllUI;
@end

// Till here
@protocol TAJChairViewProtocol;

@interface TAJChairView : UIView <TAJPlayerModelProtocol>

@property (nonatomic) BOOL                     isOccupied;
@property (nonatomic) BOOL                     isSpectator;
@property (nonatomic) BOOL                     isMiddleJoin;

@property (nonatomic, weak) id <TAJChairViewProtocol> delegate;
@property (nonatomic, strong) TAJPlayerModel *playerModel;
@property (nonatomic) NSInteger chairId;
@property (weak, nonatomic) IBOutlet UILabel *dropTextLabel;
@property (weak, nonatomic) IBOutlet UIImageView *dropBoxImageView;
@property (strong,nonatomic) IBOutlet UIImageView *deviceImageView;
@property (weak, nonatomic) IBOutlet UILabel *sitLabel;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewChunksLevel1;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewChunksLevel2;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewChunksLevel3;
@property (weak, nonatomic) IBOutlet UIView *viewChunksLevel;
@property (weak, nonatomic) IBOutlet UIView *viewAutoPlay;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoPlayLevel;



- (void)updateChairWithData:(TAJPlayerModel *)dictionary;
- (void)updatePlayerDeviceSymbol;
- (void)updateChairForDroppedPlayerWithData:(TAJPlayerModel *)playerModel;
- (void)updateChairImagesOnGameStart;
- (void)createNewEmptyChairWithChairID:(int)number;
- (BOOL)isSeatOccupied;
- (void)setUpPlayerPoints:(NSString *)playerPoints;
- (void)disableInteraction;
- (void)removeChairAvatars;
- (void)enableInteraction;
- (void)playerQuitTable:(BOOL)isGameScheduled;
- (void)playerQuitTableBeforeStartGame:(BOOL)isGameScheduled;
- (void)playerDropGame;
- (void)otherPlayerlayerDropGame;
- (void)otherPlayerLeftGame;
- (void)resetPlayerModel;
- (void)hideDropMessage;
- (void)standUpChair;
- (void)hideChair;
- (void)hideChairs;
- (void)updateStrikesGamewithBet:(int)betValue;
- (void)otherPlayerLeftGameOnThisPlayerDisconnect;

- (void)updateChairWithChunksData:(NSString *)playerData WithplaceID:(NSInteger)placeID;
-(void)setPlayer1ChunksLevelWithChunksLevel:(NSString *)chunksLevel;

@end

@protocol TAJChairViewProtocol <NSObject>

- (void)didSelectChair:(TAJChairView *)chair;

@end
