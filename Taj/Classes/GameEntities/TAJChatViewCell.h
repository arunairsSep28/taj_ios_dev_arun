/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatViewCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogish Poojary on 15/04/14.
 Created by Sujit Yadawad on 27/03/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJChatViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *chatLabel;

@end
