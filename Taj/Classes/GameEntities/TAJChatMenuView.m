/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatMenuView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogisha Poojary on 07/04/14.
 **/

#import "TAJChatMenuView.h"

@interface TAJChatMenuView ()

@property (nonatomic, strong) NSMutableArray  *preDefinedMessageArray;
@property (weak, nonatomic) IBOutlet UIButton *closeMenuButton;
- (IBAction)closeMenuButtonAction:(UIButton *)sender;

@end

@implementation TAJChatMenuView

- (IBAction)chatCloseMenu_p:(id)sender
{
    [self.delegate closeChatMenuButtonPressed];
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

-(void)awakeFromNib
{
   [self initialize]; 
}

-(void) initialize
{
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    
    //Author: Pradeep starts
    self.preDefinedMessageArray = [NSMutableArray arrayWithObjects:@"Hi",
                                   @"Good luck",
                                   @"Well played",
                                   @"Awesome",
                                   @"Thanks",
                                   @"Cheers",
                                   @"Sorry",
                                   @"Haha",
                                   @"You’re good!",
                                   @"Thanks",
                                   @"Oops",
                                   @"Good game",
                                   @"Sorry gotta run",
                                   @"One more game?",
                                   @"Epic game!",
                                   @"Keep it going!",
                                   @"You’re awesome!",
                                   @"How did I miss that?",
                                   @"I thought that was in!",
                                   @"I didn’t see that coming!", nil];
    //Author: Pradeep ends
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.preDefinedMessageArray.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";
    TAJMenuViewCell *cell = (TAJMenuViewCell *)[tableView dequeueReusableCellWithIdentifier:cellIdentifier];
      if(cell == nil)
   {
       NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"TAJMenuViewCell" owner:self options:nil];
       cell = [nib objectAtIndex:0];
   }
    
    if([TAJUtilities isIPhone])
    {
        cell.menuCellLabel.font = [UIFont fontWithName:@"AvantGardeITCbyBT-Demi" size:10.0];
    }
    else
    {
        cell.menuCellLabel.font = [UIFont fontWithName:@"AvantGardeITCbyBT-Demi" size:14.0];
    }
    cell.menuCellLabel.text = self.preDefinedMessageArray[indexPath.row];
    cell.contentView.backgroundColor = [UIColor clearColor];
    cell.backgroundColor = [UIColor clearColor];
    [cell.menuCellLabel adjustsFontSizeToFitWidth];
    return cell;

}

//- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    return 20;
//}

//Author: Pradeep starts
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *message = self.preDefinedMessageArray[indexPath.row];
    [self preDefinedTextSelected:message];
}

- (IBAction)closeMenuButtonAction:(UIButton *)sender
{
    [self closeChatMenu];
}


- (UIView *)hitTest:(CGPoint)point withEvent:(UIEvent *)event
{
    for (UIView *subview in self.subviews) {
        if (CGRectContainsPoint(subview.frame, point)) {
            return subview;
            break;
        }
    }
    
    // use this to pass the 'touch' onward in case no subviews trigger the touch
    return [super hitTest:point withEvent:event];
}
- (void)preDefinedTextSelected:(NSString *)message
{
    if (self.chatMenuDelegate && [self.chatMenuDelegate respondsToSelector:@selector(preDefinedTextSelected:)])
    {
        [self.chatMenuDelegate preDefinedTextSelected:message];
    }
}

- (void)closeChatMenu
{
    if (self.chatMenuDelegate && [self.chatMenuDelegate respondsToSelector:@selector(chatMenuClosed)])
    {
        [self.chatMenuDelegate chatMenuClosed];
    }
}
//Author: Pradeep ends

@end
