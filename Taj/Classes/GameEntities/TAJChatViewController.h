/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJChatViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogish Poojary on 13/06/14.
 Created by Sujit Yadawad on 27/03/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJChairView.h"
#import "TAJUITextField.h"

@protocol TAJChatViewControllerProtocol;

@interface TAJChatViewController : UIViewController
@property (nonatomic, strong) NSMutableArray     *chatHistoryDataSource;

@property (nonatomic, strong) TAJPlayerModel      *playerModel;
@property (nonatomic, weak) id<TAJChatViewControllerProtocol> chatViewDelegate;
@property (weak, nonatomic) IBOutlet UITextField *chatField;
@property (weak, nonatomic) IBOutlet UITableView *chatHistory;

- (void)updateChatPlayerModel:(TAJPlayerModel *)playerModel;
- (void)reloadChatHistoryTable;
- (BOOL)isChatViewShowing;
- (void)currentChatTableID:(NSString *)tableID;
- (void)reloadChatHistoryTableFromChatExtent:(NSMutableArray *)messageList;
- (void)clearChatHistory;
@end

@protocol TAJChatViewControllerProtocol <NSObject>

- (void)highLightChatButton;
- (void)showChatErrorAlert:(NSString *)message;
- (void)loadChatMenuViewInGamePlay;
- (void)loadChatExtentView:(NSMutableArray *)messageArray andPlayerModel:(TAJPlayerModel *)playerModel;
@end