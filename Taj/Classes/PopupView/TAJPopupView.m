/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPopupView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 14/02/14.
 **/

#import "TAJPopupView.h"
#import "TAJConstants.h"

@implementation TAJPopupView

@synthesize okButton = _okButton;
@synthesize yesButton = _yesButton;
@synthesize noButton = _noButton;
@synthesize cancelButton = _cancelButton;

- (id)initWithFrame:(CGRect)frame type:(int)pType message:(NSString*)pMessage
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
        
        //TODO: Provide the implementation for different alerts
        UIImageView *background = Nil;
      
        switch (pType) {
                
            case E_YES_NO_POPUP:
                
                break;
                
            case E_OK_POPUP:
                
                break;
                
            case E_OK_CANCEL_POPUP:
                
                break;
                
            case E_INFO_POPUP:
                break;
                
            case E_LARGE_POPUP:
                break;
                
            case E_WINNER_POPUP:
                break;
                
            default:
                break;
        }
        
        [self setFrame:[background frame]];
        [self addSubview:background];

//        UIButton *closeButton = [UIButton buttonWithType:UIButtonTypeCustom];
//        [closeButton setImage:[UIImage imageNamed:POPUP_VIEW_CLOSE_BUTTON] forState:UIControlStateNormal];
//        [self addSubview:closeButton];
//        closeButton.center = CGPointMake(self.frame.size.width, self.frame.size.height);
    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
