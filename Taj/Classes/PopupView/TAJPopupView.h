/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPopupView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 14/02/14.
 **/

#import <UIKit/UIKit.h>

typedef enum {
	E_YES_NO_POPUP			= 1,
	E_OK_POPUP				= 2,
	E_OK_CANCEL_POPUP		= 3,
    E_LARGE_POPUP           = 4,
    E_INFO_POPUP            = 5,
    E_WINNER_POPUP          = 6
}PopupTypes;

@interface TAJPopupView : UIView {
    
}
@property(nonatomic) UIButton *okButton;
@property(nonatomic) UIButton *yesButton;
@property(nonatomic) UIButton *noButton;
@property(nonatomic) UIButton *cancelButton;

- (id)initWithFrame:(CGRect)frame type:(int)pType message:(NSString*)pMessage;

@end
