/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 05/03/14.
 **/

#import <UIKit/UIKit.h>
static NSInteger playerCount = 0;

@interface TAJScoreBoardCell : UITableViewCell
@property (nonatomic, strong) NSMutableArray *playerQuitPoints;
@property (strong, nonatomic) IBOutlet UILabel *serialNumber;
@property (strong, nonatomic) IBOutlet UILabel *firstPlayerPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *secondPlayerPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *thirdPlayerPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *fourthPlayerPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *fifthPlayerPointLabel;
@property (strong, nonatomic) IBOutlet UILabel *sixPlayerPointLabel;
@property (strong, nonatomic) NSArray *initialUserIds;
@property (strong, nonatomic) NSMutableArray *currentUserIds;
@property (strong, nonatomic) NSMutableArray *playerLabelArray;

- (TAJScoreBoardCell *)fillPlayerLevelPoints:(NSMutableArray *)array withSeriesNumber:(NSUInteger)seriesNumber;

@end
