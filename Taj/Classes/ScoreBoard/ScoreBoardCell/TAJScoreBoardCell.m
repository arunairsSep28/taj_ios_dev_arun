/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 05/03/14.
 **/

#import "TAJScoreBoardCell.h"
#import "TAJConstants.h"

@implementation TAJScoreBoardCell

@synthesize serialNumber           = _serialNumber;
@synthesize firstPlayerPointLabel  = _firstPlayerPointLabel;
@synthesize secondPlayerPointLabel = _secondPlayerPointLabel;
@synthesize thirdPlayerPointLabel  = _thirdPlayerPointLabel;
@synthesize fourthPlayerPointLabel = _fourthPlayerPointLabel;
@synthesize fifthPlayerPointLabel  = _fifthPlayerPointLabel;
@synthesize sixPlayerPointLabel    = _sixPlayerPointLabel;
@synthesize playerLabelArray       = _playerLabelArray;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.playerQuitPoints = [NSMutableArray array];
        self.currentUserIds = [NSMutableArray array];
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJScoreBoardCell *)fillPlayerLevelPoints:(NSMutableArray *)array withSeriesNumber:(NSUInteger)seriesNumber
{
    
    DLog(@"DEBUG_VIEW :Number of enties %d in row = %d", array.count, seriesNumber);
    
    self.playerLabelArray = [NSMutableArray arrayWithObjects:self.firstPlayerPointLabel,self.secondPlayerPointLabel,
                             self.thirdPlayerPointLabel,self.fourthPlayerPointLabel,
                             self.fifthPlayerPointLabel,self.sixPlayerPointLabel, nil];    
    for (int i = 0; i < array.count; i++)
    {
        self.serialNumber.text = [NSString stringWithFormat:@"%d",seriesNumber + 1];
        UILabel *label = self.playerLabelArray[i];
        NSMutableDictionary *playerCount = [NSMutableDictionary dictionaryWithDictionary:array[i]];
//        DLog(@"playerCount= %@",playerCount);
        if([playerCount[kTaj_scoresheet_score] isEqualToString:@""])
        {
           label.text = @"X";
        }
        else
        {
            label.text = playerCount[kTaj_scoresheet_score];
        }
        
    }
    return self;
}

@end
