/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 05/03/14.
 **/

#import "TAJPlayerScoreBoardModel.h"
#import "TAJConstants.h"

@implementation TAJPlayerScoreBoardModel

@synthesize playerAllPointsArray   = _playerAllPointsArray;

- (TAJPlayerScoreBoardModel *)initWithPlayerLevelPoints:(NSArray *)array dictionary:(NSMutableDictionary*) dictionary type:(VariantsTypeEnum)variantsType
{
    // DLog(@"array= %@",array);
    self.playerAllPointsArray = [NSMutableArray array];

    if (variantsType == eStrikesType)
    {
        [self addLevelData:dictionary];
    }
    else
    {
        [self addEachLevelPlayerPoints:array];
    }
    return self;
}

- (NSMutableArray *)playerAllPointsArray
{
    for ( int i = 0; i < _playerAllPointsArray.count; i++)
    {
//        NSArray *entries = _playerAllPointsArray[i];
//        DLog(@"DEBUG_MODEL :Number of enties %d in row = %d", entries.count, i);
    }

    return _playerAllPointsArray;
}

- (TAJPlayerScoreBoardModel *)addEachLevelPlayerPoints:(NSArray *)array
{
    [self addLevelPointsToArray:array];
    
    return self;
}

- (void)addLevelPointsToArray:(NSArray *)array
{
    // local parameter array having each level score of all player
    [self.playerAllPointsArray addObject:array];
     // DLog(@"self.playerAllPointsArray = %@",self.playerAllPointsArray);
}

- (void)addLevelData:(NSMutableDictionary *)dictionary
{
    [self.playerAllPointsArray addObject:dictionary];
}

@end
