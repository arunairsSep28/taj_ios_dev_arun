/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 05/03/14.
 **/

#import <Foundation/Foundation.h>
#import "TAJConstants.h"

@interface TAJPlayerScoreBoardModel : NSObject

@property (nonatomic , strong) NSMutableDictionary *scoreSheetDictionary;
@property (strong, nonatomic) NSMutableArray *playerAllPointsArray;
@property (nonatomic, assign) VariantsTypeEnum varaintsType;

- (TAJPlayerScoreBoardModel *)initWithPlayerLevelPoints:(NSArray *)array dictionary:(NSMutableDictionary*) dictionary type:(VariantsTypeEnum)varaintsType;
- (TAJPlayerScoreBoardModel *)addEachLevelPlayerPoints:(NSArray *)array;
- (void)addLevelData:(NSMutableDictionary *)dictionary;

@end
