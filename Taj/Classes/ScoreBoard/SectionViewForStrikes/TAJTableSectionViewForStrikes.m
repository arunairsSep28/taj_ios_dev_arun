/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableSectionViewForStrikes.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 08/05/14.
 **/

#import "TAJTableSectionViewForStrikes.h"
#import "TAJConstants.h"

@interface TAJTableSectionViewForStrikes ()

@property (strong, nonatomic) IBOutlet UILabel *roundLabel;
@property (strong, nonatomic) IBOutlet UILabel *gameIDLabel;
@property (strong, nonatomic) IBOutlet UILabel *resultLabel;
@property (strong, nonatomic) IBOutlet UILabel *countLabel;
@property (strong, nonatomic) IBOutlet UILabel *scoreLabel;

@end

@implementation TAJTableSectionViewForStrikes

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (void)fillHeaderData
{
    //Move the strings to constants
    self.roundLabel.text = @"Round";
    self.gameIDLabel.text = @"Game ID";
    self.resultLabel.text = @"Result";
    self.countLabel.text = @"Count";
    self.scoreLabel.text = @"Score";
}

@end
