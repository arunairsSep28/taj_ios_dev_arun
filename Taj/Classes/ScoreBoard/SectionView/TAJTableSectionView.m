/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableSectionView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/03/14.
 **/

#import "TAJTableSectionView.h"
#import "TAJConstants.h"

@interface TAJTableSectionView ()

//@property (strong, nonatomic) IBOutlet UILabel *seriesLabel;
//@property (strong, nonatomic) IBOutlet UILabel *playerOneName;
//@property (strong, nonatomic) IBOutlet UILabel *playerTwoName;
//@property (strong, nonatomic) IBOutlet UILabel *playerThreeName;
//@property (strong, nonatomic) IBOutlet UILabel *playerFourthName;
//@property (strong, nonatomic) IBOutlet UILabel *playerFifthName;
//@property (strong, nonatomic) IBOutlet UILabel *playerSixName;

@property (strong, nonatomic) NSMutableArray *playerNameArray;

@end

@implementation TAJTableSectionView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.playerNameArray = [NSMutableArray array];
        
    }
    return self;
}

- (void)fillPlayerNameData:(NSMutableArray *)array
{
    self.playerNameArray = [NSMutableArray arrayWithObjects:self.playerOneName,self.playerTwoName,
                            self.playerThreeName,self.playerFourthName,
                            self.playerFifthName,self.playerSixName, nil];
    
    NSMutableDictionary *playerCount = [NSMutableDictionary dictionary];
    
    for (int i = 0; i < array.count; i++)
    {
        self.seriesLabel.text = @"Sl.No.";
        UILabel *label = self.playerNameArray[i];
        playerCount = array[i];
        label.text = playerCount[kTaj_scoresheet_nickname];
    }
}

- (void)fillPlayerTotalPoints:(NSMutableArray *)array
{
    //NSLog(@"FOOTER TOTAL SECTION");
    //NSLog(@"TOTAL ARRAY : %@",array);
    
    self.playerNameArray = [NSMutableArray arrayWithObjects:self.playerOneName,self.playerTwoName,
                            self.playerThreeName,self.playerFourthName,
                            self.playerFifthName,self.playerSixName, nil];
    
    NSMutableDictionary *playerCount = [NSMutableDictionary dictionary];
    
    for (int i = 0; i < array.count; i++)
    {
        self.seriesLabel.text = @"Total";
        
        UILabel *label = self.playerNameArray[i];
        playerCount = array[i];
        if([playerCount[kTaj_scoresheet_totalscore] isEqualToString:@""])
        {
            label.text = @"X";
        }
        else
        {
            label.text = playerCount[kTaj_scoresheet_totalscore];
            
        }
        //NSLog(@"TESTING COUNT %@",label.text);
    }
}

@end
