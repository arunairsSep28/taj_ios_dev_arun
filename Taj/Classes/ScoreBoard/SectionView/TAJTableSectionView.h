/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableSectionView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/03/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJTableSectionView : UIView

@property (strong, nonatomic) IBOutlet UILabel *seriesLabel;
@property (strong, nonatomic) IBOutlet UILabel *playerOneName;
@property (strong, nonatomic) IBOutlet UILabel *playerTwoName;
@property (strong, nonatomic) IBOutlet UILabel *playerThreeName;
@property (strong, nonatomic) IBOutlet UILabel *playerFourthName;
@property (strong, nonatomic) IBOutlet UILabel *playerFifthName;
@property (strong, nonatomic) IBOutlet UILabel *playerSixName;

- (void)fillPlayerNameData:(NSMutableArray *)array;
- (void)fillPlayerTotalPoints:(NSMutableArray *)array;

@end
