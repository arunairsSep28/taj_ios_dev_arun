/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardCellForStrikes.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 08/05/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJScoreBoardCellForStrikes : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *roundLabelValue;
@property (strong, nonatomic) IBOutlet UILabel *gameIDValue;
@property (strong, nonatomic) IBOutlet UILabel *resultValue;
@property (strong, nonatomic) IBOutlet UILabel *countValue;
@property (strong, nonatomic) IBOutlet UILabel *scoreValue;


- (TAJScoreBoardCellForStrikes *)fillPlayerLevelPoints:(NSMutableDictionary *)dictionary withSeriesNumber:(NSUInteger)seriesNumber;

@end
