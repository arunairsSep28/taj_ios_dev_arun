/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardCellForStrikes.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 08/05/14.
 **/

#import "TAJScoreBoardCellForStrikes.h"
#import "TAJConstants.h"

@implementation TAJScoreBoardCellForStrikes


- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

- (TAJScoreBoardCellForStrikes *)fillPlayerLevelPoints:(NSMutableDictionary *)dictionary withSeriesNumber:(NSUInteger)seriesNumber
{
    NSMutableDictionary *data = dictionary;
    //TODO: logic for updating the labels
    self.roundLabelValue.text = [NSString stringWithFormat:@"%d",seriesNumber + 1];
    //[self.roundLabelValue sizeToFit];
    
    self.gameIDValue.text = [data objectForKey:@"TAJ_game_id"];
    NSString *nickName = [data objectForKey:kTaj_nick_name];
    
    NSMutableArray *array = [data objectForKey:@"player"];
    
    for (NSDictionary *dictionary in array)
    {
        NSString *username = [dictionary objectForKey:kTaj_scoresheet_nickname];
        
        if ([nickName compare:username] == NSOrderedSame)
        {
            NSString *result = [dictionary objectForKey:kTaj_scoresheet_result];
            self.countValue.text = [dictionary objectForKey:kTaj_scoresheet_points];
            self.scoreValue.text = [dictionary objectForKey:kTaj_scoresheet_score];
            NSString *conversion = [data objectForKey:kTaj_scoresheet_conversion];
            conversion = [conversion substringFromIndex:([conversion length]-1)];
            int conversionValue = 1;
            conversionValue = [conversion intValue];
            
            NSString *score = Nil;
            score = dictionary[kTaj_scoresheet_score];
            float scoreValue = [score floatValue];
            
            float finalValue = scoreValue * conversionValue;
            
            if ([result isEqualToString:@"meld_timeout"]
                || [result isEqualToString:@"eliminate"]
                || [result isEqualToString:@"table_leave"]
                || [result isEqualToString:@"timeout"]
                || [result isEqualToString:@"meld"])
            {
                self.resultValue.text = kTaj_scoresheet_result_modified;
                NSString *dScore = Nil;
                dScore = dictionary[kTaj_scoresheet_score];
                float dScoreValue = [dScore floatValue];
                
                NSString *minusPointsValue = [NSString stringWithFormat:@"-%.02f",dScoreValue];
                self.scoreValue.text = minusPointsValue;
            }
            else if ([result isEqualToString:@"drop"])
            {
                self.resultValue.text = @"Dropped";
                NSString *dScore = Nil;
                dScore = dictionary[kTaj_scoresheet_score];
                float dScoreValue = [dScore floatValue];
                
                NSString *minusPointsValue = [NSString stringWithFormat:@"-%.02f",dScoreValue];
                self.scoreValue.text = minusPointsValue;
            }
            else
            {
               // self.resultValue.text = kTaj_scoresheet_result_modified;
                NSString *firstCapChar = [[dictionary[kTaj_scoresheet_result] substringToIndex:1] capitalizedString];
                NSString *cappedString = [dictionary[kTaj_scoresheet_result] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
                self.resultValue.text = cappedString;
                NSString *positivePointsValue = [NSString stringWithFormat:@"+%.02f",finalValue];
                self.scoreValue.text = positivePointsValue;
            }
            
        }
    }
    return self;
}

@end

