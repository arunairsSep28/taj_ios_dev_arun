/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Raghavendra Kamath 15/05/14.
 Created by Pradeep BM on 05/03/14.
 **/

#import "TAJScoreBoardViewController.h"
#import "TAJScoreBoardCell.h"
#import "TAJTableSectionView.h"
#import "TAJScoreBoardCellForStrikes.h"
#import "TAJTableSectionViewForStrikes.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"

@interface TAJScoreBoardViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *scoreBoardTableView;
@property (weak, nonatomic) IBOutlet UIView *headerView;
@property (weak, nonatomic) IBOutlet UIView *scoreBoardFooterView;
@property (strong, nonatomic) TAJTableSectionView *footerSection;
@property (nonatomic) CGRect footerFrame;
@property (weak, nonatomic) IBOutlet UIImageView *footerDividerImageView;
@property (strong, nonatomic) NSMutableArray *previousTotal;
@end

@implementation TAJScoreBoardViewController

#pragma mark - UI Life cycle -

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Variant:(VariantsTypeEnum)variantType
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    
    if(self)
    {
        self.scoreBoardType = variantType;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.quitUsers = [NSMutableArray array];
    self.initialUsers = [NSMutableArray array];
    self.footerFrame = self.scoreBoardFooterView.frame;
    self.previousTotal = [NSMutableArray array];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (void)viewDidAppear:(BOOL)animated
{
    [self scoreboardHeaderSection];
    
    if(self.scoreBoardType == ePoolOrDealType)
    {
        NSLog(@"SCOREBOARD FOOTER");
        if([TAJUtilities isIPhone])
        {
            if ([self.playerScoreModel.playerAllPointsArray count] > 0 && [self.playerScoreModel.playerAllPointsArray count]<= 6)
            {
                CGRect frame = self.scoreBoardFooterView.frame;
                //frame.origin.y -= (6 - ([self.playerScoreModel.playerAllPointsArray count])) * 20;
                self.scoreBoardFooterView.frame = frame;
                self.footerDividerImageView.hidden = YES;
                [self scoreboardFooterSection];
            }
            if ([self.playerScoreModel.playerAllPointsArray count] > 0 && [self.playerScoreModel.playerAllPointsArray count]> 6)
            {
                self.scoreBoardFooterView.frame = self.footerFrame;
                self.footerDividerImageView.hidden = NO;
                [self scoreboardFooterSection];
            }
            
        }
        else
        {
            if ([self.playerScoreModel.playerAllPointsArray count] > 0 && [self.playerScoreModel.playerAllPointsArray count]<= 7)
            {
                CGRect frame = self.scoreBoardFooterView.frame;
                //frame.origin.y -= (7 - ([self.playerScoreModel.playerAllPointsArray count])) * 47;
                self.scoreBoardFooterView.frame = frame;
                self.footerDividerImageView.hidden = YES;
                [self scoreboardFooterSection];
            }
            if ([self.playerScoreModel.playerAllPointsArray count] > 0 && [self.playerScoreModel.playerAllPointsArray count]> 7)
            {
                self.scoreBoardFooterView.frame = self.footerFrame;
                self.footerDividerImageView.hidden = NO;
                [self scoreboardFooterSection];
            }
            
        }
        
    }
}

-(void) viewDidDisappear:(BOOL)animated
{
    self.scoreBoardFooterView.frame = self.footerFrame;
    [self.footerSection removeFromSuperview];
}

- (BOOL)isDataExist
{
    if ([self.playerScoreModel.playerAllPointsArray count] > 0)
    {
        return YES;
    }
    return NO;
}

- (void)removeAllScoreBoardData
{
    [self.playerScoreModel.playerAllPointsArray removeAllObjects];
    [self.scoreBoardTableView reloadData];
    [self reloadTableViewData];
}

- (void)reloadTableViewData
{
    if ([self.playerScoreModel.playerAllPointsArray count] > 0)
    {
        //self.scoreBoardTableView.showsVerticalScrollIndicator = YES;
        [self.scoreBoardTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.playerScoreModel.playerAllPointsArray count] - 1 inSection:0]
                                        atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        
        //[self.scoreBoardTableView flashScrollIndicators];
        self.scoreBoardTableView.bounces = YES;
        
        
    }
}

- (BOOL)isShowing
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

#pragma mark - Reload points -

- (IBAction)scoreBoardCloseButton_p:(id)sender
{
    [self.delegate changeScoreBoardButtonProperty];
}

- (void)reloadScoreBoardPoints
{
    [self.scoreBoardTableView reloadData];
    [self reloadTableViewData];
}

#pragma mark - UITableView Delegate Datasource -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    //DLog(@"[self.playerScoreModel.playerAllPointsArray count] = %d",[self.playerScoreModel.playerAllPointsArray count]);
    //NSLog(@"[self.playerScoreModel.playerAllPointsArray count] = %lu",(unsigned long)[self.playerScoreModel.playerAllPointsArray count]);
    return [self.playerScoreModel.playerAllPointsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView  cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ScoreBoardCellID";
    
    UITableViewCell *cellTobeReturned = Nil;
    DLog(@"self.scoreBoardType = %d",self.scoreBoardType);
    
    if (self.scoreBoardType == ePoolOrDealType)
    {
        // deals and pools
        TAJScoreBoardCell *cell = (TAJScoreBoardCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
        for (int i = 0; i < [cell.contentView.subviews count]; i++)
        {
            [[cell.contentView.subviews objectAtIndex:i] removeFromSuperview];
        }
        if (!cell)
        {
            NSArray *array = Nil;
            array = [[NSBundle mainBundle] loadNibNamed:TAJScoreBoardCellNibName
                                                  owner:nil options:nil];
            
            {
                cell = array[0];
            }
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        NSArray *referenceArray = [self.playerScoreModel.playerAllPointsArray objectAtIndex:0];
        NSArray *currentArray = [NSMutableArray arrayWithArray:[self.playerScoreModel.playerAllPointsArray objectAtIndex:indexPath.row]];
        NSMutableArray *array = [NSMutableArray arrayWithArray:currentArray];
        
        DLog(@"currentArray = %d",currentArray.count);
        DLog(@"currentArray = %@",currentArray);
        
        if(array.count != referenceArray.count)
        {
            for( NSInteger index = 0; index < referenceArray.count; index++)
            {
                NSArray *keys = [NSArray arrayWithObjects:@"TAJ_aienable", @"TAJ_nick_name", @"TAJ_points", @"TAJ_result", @"TAJ_score", @"TAJ_total_score", @"TAJ_user_id", nil];
                NSArray *objects = [NSArray arrayWithObjects:@"", @"", @"",@"", @"", @"", @"", nil];
                NSDictionary  *dictionary = [NSDictionary dictionaryWithObjects:objects forKeys:keys];
                
                if(index < array.count)
                {
                    NSDictionary *reference = referenceArray[index];
                    NSDictionary *current = array[index];
                    
                    if(![current[@"TAJ_user_id"]isEqualToString:reference[@"TAJ_user_id"]])
                    {
                        [array insertObject:dictionary atIndex:index];
                    }
                }
                else
                {
                    [array addObject:dictionary];
                }
            }
            // DLog(@"currentArray After= %@",currentArray);
            [cell fillPlayerLevelPoints:array withSeriesNumber:indexPath.row];
        }
        else
        {
            [cell fillPlayerLevelPoints:[self.playerScoreModel.playerAllPointsArray objectAtIndex:indexPath.row] withSeriesNumber:indexPath.row];
        }
        //DLog(@"self.playerScoreModel.playerAllPointsArray objectAtIndex:indexPath.row = %@",[self.playerScoreModel.playerAllPointsArray objectAtIndex:indexPath.row]);
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        cellTobeReturned =  cell;
    }
    else
    {
        // deals Strikes pools
        TAJScoreBoardCellForStrikes *cell = (TAJScoreBoardCellForStrikes*) [tableView dequeueReusableCellWithIdentifier:identifier];
        for (int i = 0; i < [cell.contentView.subviews count]; i++)
        {
            [[cell.contentView.subviews objectAtIndex:i] removeFromSuperview];
        }
        if (!cell)
        {
            NSArray *array = Nil;
            array = [[NSBundle mainBundle] loadNibNamed:TAJScoreBoardCellStrikesNibName
                                                  owner:nil options:nil];
            
            cell = array[0];
        }
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        [cell fillPlayerLevelPoints:[self.playerScoreModel.playerAllPointsArray objectAtIndex:indexPath.row] withSeriesNumber:indexPath.row];
        cell.contentView.backgroundColor = [UIColor clearColor];
        cell.backgroundColor = [UIColor clearColor];
        //[self scoreboardHeaderSection];
        cellTobeReturned =  cell;
    }
    return cellTobeReturned;
}

- (void)scoreboardHeaderSection
{
    NSArray *nibContents = [NSArray array];
    // Header view
    int index = 0;
    if (self.scoreBoardType == ePoolOrDealType)
    {
        // deals and pools
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJTableSectionNibName
                                                    owner:nil
                                                  options:nil];
        TAJTableSectionView *headerSection;
        headerSection = nibContents[index];
        
        //get any index object for player name because in each level player will be the same untill he quit the game
        NSMutableArray *playerNameArray = [self.playerScoreModel.playerAllPointsArray objectAtIndex:0];
        DLog(@"[self.playerScoreModel.playerAllPointsArray objectAtIndex:0] = %@",[self.playerScoreModel.playerAllPointsArray objectAtIndex:0]);
        
        [headerSection fillPlayerNameData:playerNameArray];
        //        headerSection.frame = self.headerView.bounds;
        
        CGRect frame = headerSection.frame;
        frame.size.width = self.headerView.frame.size.width;
        frame.size.height = self.headerView.frame.size.height;
        headerSection.frame = frame;
        
        [self.headerView addSubview: headerSection];
    }
    else
    {
        // no joker
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJTableSectionViewStrikesNibName
                                                    owner:nil
                                                  options:nil];
        TAJTableSectionViewForStrikes *headerSection;
        headerSection = nibContents[index];
        [headerSection fillHeaderData];
        
        CGRect frame = headerSection.frame;
        frame.size.width = self.headerView.frame.size.width;
        frame.size.height = self.headerView.frame.size.height;
        headerSection.frame = frame;
        
        [self.headerView addSubview: headerSection];
    }
    
}

- (void)scoreboardFooterSection
{
    if (self.scoreBoardType == ePoolOrDealType)
    {
        NSArray *nibContents = [NSArray array];
        // Footer view
        int index = 0;
        
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJTableSectionNibName
                                                    owner:nil
                                                  options:nil];
        
        _footerSection = nibContents[index];
        
        //get last object because total score of overall game will be exist in last object of an array
        
        NSUInteger lastObjectIndex = self.playerScoreModel.playerAllPointsArray.count - 1;
        NSArray *playerTotalArray = [self.playerScoreModel.playerAllPointsArray objectAtIndex:lastObjectIndex];
        NSArray *referenceArray = [self.playerScoreModel.playerAllPointsArray objectAtIndex:0];
        NSMutableArray *finalArray = [NSMutableArray arrayWithArray:playerTotalArray];
        if(finalArray.count != referenceArray.count)
        {
            for( NSInteger index = 0; index < referenceArray.count; index++)
            {
                if(index < finalArray.count)
                {
                    NSDictionary *reference = referenceArray[index];
                    NSDictionary *current = finalArray[index];
                    // current = currentArray[index];
                    if(![current[@"TAJ_user_id"]isEqualToString:reference[@"TAJ_user_id"]])
                    {
                        [finalArray insertObject:self.previousTotal[index] atIndex:index];
                        //break;
                    }
                }
                
                else
                {
                    
                    [finalArray addObject:self.previousTotal[index]];
                }
            }
            [self.previousTotal removeAllObjects];
            [self.previousTotal addObjectsFromArray:finalArray];
            [self.footerSection fillPlayerTotalPoints:finalArray];
            NSLog(@"FOOTER IF");
            NSLog(@"FOOTER IF ARRAY : %@",finalArray);
        }
        else
        {
            NSLog(@"FOOTER ELSE");
            [self.previousTotal removeAllObjects];
            [self.previousTotal addObjectsFromArray:playerTotalArray];
            [_footerSection fillPlayerTotalPoints:[self.playerScoreModel.playerAllPointsArray objectAtIndex:lastObjectIndex]];
            
            NSLog(@"FOOTER ELSE ARRAY : %@",[self.playerScoreModel.playerAllPointsArray objectAtIndex:lastObjectIndex]);
        }
        _footerSection.frame = self.scoreBoardFooterView.bounds;
        [self.scoreBoardFooterView addSubview:_footerSection];
        
        
    }
    else
    {
        NSArray *nibContents = [NSArray array];
        // Footer view
        int index = 0;
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJTableSectionViewStrikesNibName
                                                    owner:nil
                                                  options:nil];
        
        TAJTableSectionViewForStrikes *footerSection;
        footerSection = nibContents[index];
        
        footerSection.frame = self.scoreBoardFooterView.bounds;
        [self.scoreBoardFooterView addSubview:footerSection];
    }
    
}

- (void)setPlayerInitialIds:(NSMutableArray *)userArray
{
    for (int i = 0; i<userArray.count; i++)
    {
        NSMutableDictionary *playerCount = [NSMutableDictionary dictionaryWithDictionary:userArray[i]];
        DLog(@"playerCount= %@",playerCount);
        NSString *userId = playerCount[@"TAJ_user_id"];
        self.initialUsers = [NSMutableArray arrayWithObject:userId];
    }
}

- (void)quitUserIds:(NSMutableArray *)quitArray
{
    self.quitUsers = quitArray;
}

@end
