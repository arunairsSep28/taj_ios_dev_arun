/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreBoardViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Raghavendra Kamath 15/05/14.
 Created by Pradeep BM on 05/03/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJPlayerScoreBoardModel.h"



@protocol TAJScoreBoardViewControllerDelegate
-(void) changeScoreBoardButtonProperty;

@end


@interface TAJScoreBoardViewController : UIViewController

@property (strong, nonatomic) TAJPlayerScoreBoardModel *playerScoreModel;
@property (strong, nonatomic) id<TAJScoreBoardViewControllerDelegate> delegate;
@property (strong, nonatomic) IBOutlet UIButton *scoreBoardCloseButton;
@property (nonatomic) VariantsTypeEnum scoreBoardType;
@property(nonatomic ,strong) NSMutableArray *quitUsers;
@property(nonatomic ,strong) NSMutableArray *initialUsers;

-(id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil Variant:(VariantsTypeEnum)variantType;

- (IBAction)scoreBoardCloseButton_p:(id)sender;

- (void)reloadScoreBoardPoints;
- (BOOL)isDataExist;
- (void)removeAllScoreBoardData;
- (BOOL)isShowing;
- (void)setPlayerInitialIds:(NSMutableArray *)userArray;
- (void)quitUserIds:(NSMutableArray *)quitArray;
@end
