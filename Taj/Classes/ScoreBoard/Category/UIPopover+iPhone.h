/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		UIPopover+iPhone.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/03/14.
 **/

#import <Foundation/Foundation.h>

@interface UIPopoverController (overrides)

// Reference :http://stackoverflow.com/questions/18205903/popover-controller-for-iphone

+(BOOL)_popoversDisabled;

@end
