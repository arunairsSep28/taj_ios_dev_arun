/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJDiscardedCardViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJDiscardCardModel.h"
#import "TAJBaseViewController.h"
#import "TAJIamBackViewController.h"
@interface TAJDiscardedCardViewController : TAJBaseViewController

@property (nonatomic, strong) TAJDiscardCardModel   *discardCardModel;
@property (nonatomic, strong) NSString   *jokerCardId;

@property (weak, nonatomic) IBOutlet UIScrollView *cardsScrolView;

- (void)showStarImageForCard:(NSMutableDictionary *)starImageCardInfo;
- (BOOL)isDiscardViewShowing;
- (void)reloadDiscardCardHistory:(NSMutableDictionary *)dictionary;
- (void)removeCardsAlreadyInScrollView;
- (void)hideAllDiscardHistoryButtons;
- (void)clearArrayContents;
- (void)setJokerCard:(NSString *)jokerCard;

@end
