/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJDiscardCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 28/04/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJDiscardCardView : UIView

@property (nonatomic, strong) NSString  *cardId;
@property (nonatomic)   BOOL        isStarCardImage;
@property (nonatomic)   BOOL        isJoker;

- (void)showCard:(NSString *)cardID isStarCardImage:(BOOL)isStarCardImage isAutoPlayCard:(NSString *)autoPlay;
- (void)showCard:(NSString *)cardID isStarCardImage:(BOOL)isStarCardImage isAutoPlayCard:(NSString *)autoPlay withFlagStatus:(BOOL) flag;
- (void)setIsJoker:(BOOL)isJoker;
@end
