/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJDiscardCardView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 28/04/14.
 **/

#import "TAJDiscardCardView.h"

@interface TAJDiscardCardView ()

@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *starImageView;
@property (weak, nonatomic) IBOutlet UIImageView *autoplayImage;
@property (weak, nonatomic) IBOutlet UIImageView *jokerImage;

@end

@implementation TAJDiscardCardView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        self.isJoker=NO;
        self.jokerImage.hidden = YES;
    }
    return self;
}

-(void)setIsJoker:(BOOL)isJoker
{
    if (isJoker)
    {
        self.jokerImage.hidden = NO;
    }
    else
    {
        self.jokerImage.hidden = YES;
    }
}

- (void)showCard:(NSString *)cardID isStarCardImage:(BOOL)isStarCardImage isAutoPlayCard:(NSString *)autoPlay
{
    DLog(@"autoPlay = %@",autoPlay);
    self.isStarCardImage = isStarCardImage;
    self.cardId = [cardID uppercaseString];
    
    // Set card image according to the input string
    UIImage *cardImage = [UIImage imageNamed:cardID];
    self.cardImageView.image = cardImage;
    
    self.cardImageView.layer.cornerRadius = 4.0f;
    
    self.jokerImage.hidden = YES;
    
    if (isStarCardImage)
    {
        self.starImageView.hidden = NO;
    }
    if ([autoPlay isEqualToString:@"True"])
    {
        self.autoplayImage.hidden = NO;
    }
    else if ([autoPlay isEqualToString:@"False"])
    {
        self.autoplayImage.hidden = YES;
    }
}

- (void)showCard:(NSString *)cardID isStarCardImage:(BOOL)isStarCardImage isAutoPlayCard:(NSString *)autoPlay withFlagStatus:(BOOL)flag
{
    self.isStarCardImage = isStarCardImage;
    self.cardId = [cardID uppercaseString];
    
    // Set card image according to the input string
    UIImage *cardImage = [UIImage imageNamed:cardID];
    self.cardImageView.image = cardImage;
    
    self.cardImageView.layer.cornerRadius = 4.0f;
    
    self.jokerImage.hidden = YES;
    
    if (isStarCardImage)
    {
        self.starImageView.hidden = NO;
    }
    if(flag == YES)
    {
        self.autoplayImage.hidden = NO;
    }
}

- (NSString *)cardNumber
{
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.cardId];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    return numberString;
}

- (NSString *)cardSuit
{
    // Intermediate
    NSString *suitString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.cardId];
    NSCharacterSet *characters = [NSCharacterSet letterCharacterSet];
    
    // Throw away characters before the first number.
    [scanner scanUpToCharactersFromSet:characters intoString:NULL];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:characters intoString:&suitString];
    
    // Result.
    return [suitString lowercaseString];
}

@end
