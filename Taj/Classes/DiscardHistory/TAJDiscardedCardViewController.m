/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJDiscardedCardViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/04/14.
 **/

#import "TAJDiscardedCardViewController.h"
#import "TAJDiscardCardView.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"

static  CGFloat xindex = 0.0;
//static CGFloat scrollViewContentOffset;
@interface TAJDiscardedCardViewController ()<UIScrollViewDelegate>

@property (nonatomic, strong) NSArray       *allPlayerNameArray;
// to reload the cards for current player
@property (nonatomic) int      currentIndexPlayer;

@property (weak, nonatomic) IBOutlet UIButton *playerOneButton;
@property (weak, nonatomic) IBOutlet UIButton *playerTwoButton;
@property (weak, nonatomic) IBOutlet UIButton *playerThreeButton;
@property (weak, nonatomic) IBOutlet UIButton *playerFourButton;
@property (weak, nonatomic) IBOutlet UIButton *playerFiveButton;
@property (weak, nonatomic) IBOutlet UIButton *playerSixButton;

@property (strong, nonatomic) IBOutlet UIButton *discardLeftArrowButton;
@property CGFloat scrollViewContentOffset;
@property (nonatomic) NSInteger iamBackArrayCount;
@property (strong, nonatomic) UIColor *orangeColor;

@property (nonatomic, strong) NSMutableArray       *playerDiscardListArray;
- (IBAction)playerOneAction:(UIButton *)sender;
- (IBAction)playerTwoAction:(UIButton *)sender;
- (IBAction)playerThreeAction:(UIButton *)sender;
- (IBAction)playerFourAction:(UIButton *)sender;
- (IBAction)playerFiveAction:(UIButton *)sender;
- (IBAction)playerSixAction:(UIButton *)sender;
- (IBAction)discardLeftArrowButtonAction:(id)sender;

@end

@implementation TAJDiscardedCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // set border for view
    self.view.layer.cornerRadius = 4.0f;
    self.view.layer.borderColor = [UIColor grayColor].CGColor;
    self.view.layer.borderWidth = 0.5f;
    
    self.orangeColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.cardsScrolView.delegate = self;
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(iamBackThisPlayerArrayCountForDiscard:)
                                                 name:SHOW_IAMBACKARRAY_FOR_DISCARD_ALERT
                                               object:nil];
    if (self.discardCardModel)
    {
        self.allPlayerNameArray = [NSArray arrayWithArray:[self.discardCardModel.discardedCardDictionary allKeys]];
        [self unhideButtonsForPlayersWithPlayerArray:self.allPlayerNameArray];
        [self firstPlayerDiscardCards];
        [self unselectButton:self.playerOneButton];
        // self.cardsScrolView.userInteractionEnabled = NO;
    }
}

- (void)viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_IAMBACKARRAY_FOR_DISCARD_ALERT object:nil];
    
}

- (void)iamBackThisPlayerArrayCountForDiscard:(NSNotification *) notification
{
    NSDictionary *dict = [notification userInfo];
    NSNumber *value = [dict objectForKey:@"arrayCount"];
    self.iamBackArrayCount = [value integerValue];
    DLog(@"self.iamBackArrayCount =%d",self.iamBackArrayCount);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)isDiscardViewShowing
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

- (void)reloadDiscardCardHistory:(NSMutableDictionary *)dictionary
{
    if (self.discardCardModel)
    {
        self.allPlayerNameArray = [NSArray arrayWithArray:[self.discardCardModel.discardedCardDictionary allKeys]];
        
        [self unhideButtonsForPlayersWithPlayerArray:self.allPlayerNameArray];
        [self removeCardsAlreadyInScrollView];
        // when ever view is showing if the clicked player is discading card we need to update card for the who has currently discarded
        NSString *playerName = dictionary[kTaj_nickname];
        // NSString *playerNameAlreadyExist = self.discardedCardDictionary[playerName];
        // NSString *currentPlayerName = self.allPlayerNameArray[self.currentIndexPlayer];
        
        int playerIndex=[self.allPlayerNameArray indexOfObject:playerName];
        
        switch (playerIndex)
        {
            case 0:
                [self unselectButton:self.playerOneButton];
                break;
            case 1:
                [self unselectButton:self.playerTwoButton];
                break;
            case 2:
                [self unselectButton:self.playerThreeButton];
                break;
            case 3:
                [self unselectButton:self.playerFourButton];
                break;
            case 4:
                [self unselectButton:self.playerFiveButton];
                break;
            case 5:
                [self unselectButton:self.playerSixButton];
                break;
            case 6:
                [self unselectButton:self.playerSixButton];
                break;
            default:
                [self unselectButton:self.playerOneButton];
                break;
        }
        
        self.currentIndexPlayer = playerIndex;
        
        
        NSMutableArray *currentPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[playerName];
        
        [self placeCardInScrollView:currentPlayerDiscardCards];
        
        
        //[self unselectButton:sender];
        //[self scrollCards:currentPlayerDiscardCards  withShift:YES];
    }
}

- (void)setJokerCard:(NSString *)jokerCard
{
    DLog(@"setJokerCard setJokerCard %@",jokerCard);
    
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:jokerCard];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    
    self.jokerCardId = numberString;
}

- (void)showStarImageForCard:(NSMutableDictionary *)starImageCardInfo
{
    NSString *playerName = starImageCardInfo[kTaj_nickname];
    //    NSMutableDictionary *cardInfo = starImageCardInfo[kTaj_StarCardInfo];
    if (playerName && self.discardCardModel.discardedCardDictionary)
    {
        [self removeCardsAlreadyInScrollView];
        
        NSMutableArray *playerDiscardCards = self.discardCardModel.discardedCardDictionary[playerName];
        
        // remove last object to show star card image (last discarded card is at last position)
        NSMutableDictionary *lastCardInfo = [playerDiscardCards lastObject];
        // add star card info
        [lastCardInfo setObject:[NSNumber numberWithBool:YES] forKey:kTaj_Star_card];
        
        [self placeCardInScrollView:playerDiscardCards];
        //[self scrollCards:playerDiscardCards withShift:NO];
        
        int playerIndex=[self.allPlayerNameArray indexOfObject:playerName];
        
        switch (playerIndex)
        {
            case 0:
                [self unselectButton:self.playerOneButton];
                break;
            case 1:
                [self unselectButton:self.playerTwoButton];
                break;
            case 2:
                [self unselectButton:self.playerThreeButton];
                break;
            case 3:
                [self unselectButton:self.playerFourButton];
                break;
            case 4:
                [self unselectButton:self.playerFiveButton];
                break;
            case 5:
                [self unselectButton:self.playerSixButton];
                break;
            case 6:
                [self unselectButton:self.playerSixButton];
                break;
            default:
                [self unselectButton:self.playerOneButton];
                break;
        }
    }
}

#pragma mark - Pre executable functions -

- (void)unhideButtonsForPlayersWithPlayerArray:(NSArray *)playerArray
{
    for (int i = 0; i < playerArray.count; i++)
    {
        if (i == 0)
        {
            self.playerOneButton.hidden = NO;
            [self.playerOneButton setTitle:playerArray[i] forState:UIControlStateNormal];
        }
        else if (i == 1)
        {
            self.playerTwoButton.hidden = NO;
            [self.playerTwoButton setTitle:playerArray[i] forState:UIControlStateNormal];
        }
        else if (i == 2)
        {
            self.playerThreeButton.hidden = NO;
            [self.playerThreeButton setTitle:playerArray[i] forState:UIControlStateNormal];
        }
        else if (i == 3)
        {
            self.playerFourButton.hidden = NO;
            [self.playerFourButton setTitle:playerArray[i] forState:UIControlStateNormal];
        }
        else if (i == 4)
        {
            self.playerFiveButton.hidden = NO;
            [self.playerFiveButton setTitle:playerArray[i] forState:UIControlStateNormal];
        }
        else if (i == 5)
        {
            self.playerSixButton.hidden = NO;
            [self.playerSixButton setTitle:playerArray[i] forState:UIControlStateNormal];
        }
    }
}

- (void)hideAllDiscardHistoryButtons
{
    self.playerOneButton.hidden = YES;
    self.playerTwoButton.hidden = YES;
    self.playerThreeButton.hidden = YES;
    self.playerFourButton.hidden = YES;
    self.playerFiveButton.hidden = YES;
    self.playerSixButton.hidden = YES;
    self.currentIndexPlayer = 0;
}

#pragma mark - IBActons -

- (void)unselectButton:(UIButton *)button
{
    NSArray *buttonArray = [NSArray arrayWithObjects:self.playerOneButton,self.playerTwoButton,self.playerThreeButton,self.playerFourButton,self.playerFiveButton,self.playerSixButton, nil];
    UIButton *selectedButton = button;
    for(int index=0;index<buttonArray.count;index++)
    {
        UIButton *referenceButton = buttonArray[index];
        if([TAJUtilities isIPhone])
        {
            if(selectedButton == referenceButton)
            {
                [selectedButton setTitleColor:self.orangeColor forState:UIControlStateNormal];
                [selectedButton setBackgroundImage:[UIImage imageNamed:@"discards_bg-568h~iphone.png"] forState:UIControlStateNormal];
            }
            else
                
            {
                [referenceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [referenceButton setBackgroundImage:[UIImage imageNamed:@"discard_box-568h~iphone.png"] forState:UIControlStateNormal];
            }
            
        }
        else
        {
            if(selectedButton == referenceButton)
            {
                [selectedButton setTitleColor:self.orangeColor forState:UIControlStateNormal];
                [selectedButton setBackgroundImage:[UIImage imageNamed:@"discard_callout.png"] forState:UIControlStateNormal];
            }
            else
                
            {
                [referenceButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
                [referenceButton setBackgroundImage:[UIImage imageNamed:@"discard_box.png"] forState:UIControlStateNormal];
            }
            
        }
        
    }
}

- (IBAction)playerOneAction:(UIButton *)sender
{
    [self firstPlayerDiscardCards];
    [self unselectButton:sender];
}

- (IBAction)playerTwoAction:(UIButton *)sender
{
    
    [self secondPlayerDiscardCards];
    [self unselectButton:sender];
    
}

- (IBAction)playerThreeAction:(UIButton *)sender
{
    
    [self thirdPlayerDiscardCards];
    [self unselectButton:sender];
}

- (IBAction)playerFourAction:(UIButton *)sender
{
    
    [self fouthPlayerDiscardCards];
    [self unselectButton:sender];
    
}

- (IBAction)playerFiveAction:(UIButton *)sender
{
    
    [self fifthPlayerDiscardCards];
    [self unselectButton:sender];
    
}

- (IBAction)playerSixAction:(UIButton *)sender
{
    
    [self sixthPlayerDiscardCards];
    [self unselectButton:sender];
    
}

- (IBAction)discardLeftArrowButtonAction:(id)sender
{
    CGFloat  newContentOffsetX = self.cardsScrolView.contentOffset.x;
    CGFloat temp = 0.0;
    if([TAJUtilities isIPhone])
    {
        temp = 25;
    }
    else
    {
        temp =  55;
    }
    
    newContentOffsetX = self.cardsScrolView.contentOffset.x + temp;
    [self.cardsScrolView setContentOffset:CGPointMake(newContentOffsetX, 0) animated:YES];
    
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    CGFloat  contentOffsetX = self.cardsScrolView.contentOffset.x;
    CGFloat temp = 0.0;
    if([TAJUtilities isIPhone])
    {
        temp = 23.0;
    }
    else
    {
        temp =  30.0;
    }
    CGFloat result = self.cardsScrolView.contentSize.width - (contentOffsetX + temp);
    
    if(result <= self.cardsScrolView.frame.size.width)
    {
        self.discardLeftArrowButton.enabled = NO;
        
    }
    else
    {
        self.discardLeftArrowButton.enabled = YES;
        
    }
}

#pragma mark - Player card Details -

- (void)removeCardsAlreadyInScrollView
{
    for (TAJDiscardCardView * card in self.cardsScrolView.subviews)
    {
        if (card)
        {
            [card removeFromSuperview];
        }
    }
    [self.cardsScrolView setContentSize:CGSizeMake(0,0)];
}
- (void)firstPlayerDiscardCards
{
    if (self.allPlayerNameArray.count > 0)
    {
        self.currentIndexPlayer = 0;
        [self removeCardsAlreadyInScrollView];
        NSString *firstPlayerName = self.allPlayerNameArray[0];
        NSMutableArray *firstPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[firstPlayerName];
        [self placeCardInScrollView:firstPlayerDiscardCards];
    }
}

- (void)secondPlayerDiscardCards
{
    if (self.allPlayerNameArray.count > 0 && self.allPlayerNameArray.count > 1)
    {
        self.currentIndexPlayer = 1;
        [self removeCardsAlreadyInScrollView];
        
        NSString *secondPlayerName = self.allPlayerNameArray[1];
        NSMutableArray *secondPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[secondPlayerName];
        [self placeCardInScrollView:secondPlayerDiscardCards];
    }
}

- (void)thirdPlayerDiscardCards
{
    if (self.allPlayerNameArray.count > 0 && self.allPlayerNameArray.count > 2)
    {
        self.currentIndexPlayer = 2;
        [self removeCardsAlreadyInScrollView];
        
        NSString *thirdPlayerName = self.allPlayerNameArray[2];
        NSMutableArray *thirdPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[thirdPlayerName];
        
        [self placeCardInScrollView:thirdPlayerDiscardCards];
    }
}

- (void)fouthPlayerDiscardCards
{
    if (self.allPlayerNameArray.count > 0 && self.allPlayerNameArray.count > 3)
    {
        self.currentIndexPlayer = 3;
        [self removeCardsAlreadyInScrollView];
        
        NSString *fouthPlayerName = self.allPlayerNameArray[3];
        NSMutableArray *fourthPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[fouthPlayerName];
        [self placeCardInScrollView:fourthPlayerDiscardCards];
    }
}

- (void)fifthPlayerDiscardCards
{
    if (self.allPlayerNameArray.count > 0 && self.allPlayerNameArray.count > 4)
    {
        self.currentIndexPlayer = 4;
        [self removeCardsAlreadyInScrollView];
        NSString *fifthPlayerName = self.allPlayerNameArray[4];
        NSMutableArray *fifthPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[fifthPlayerName];
        [self placeCardInScrollView:fifthPlayerDiscardCards];
    }
}

- (void)sixthPlayerDiscardCards
{
    if (self.allPlayerNameArray.count > 0 && self.allPlayerNameArray.count > 5)
    {
        self.currentIndexPlayer = 5;
        [self removeCardsAlreadyInScrollView];
        NSString *sixthPlayerName = self.allPlayerNameArray[5];
        NSMutableArray *sixthPlayerDiscardCards = self.discardCardModel.discardedCardDictionary[sixthPlayerName];
        [self placeCardInScrollView:sixthPlayerDiscardCards];
    }
}


#pragma mark - Place Cards-

- (void)placeCardInScrollView:(NSMutableArray *)playerCards
{
    for (int i = playerCards.count-1; i >= 0; i--)
    {
        NSDictionary *cardInfo = playerCards[i];
        DLog(@"cardInfo = %@",cardInfo);
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
        
        NSArray *nibContents = [NSArray array];
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = 0;//[TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJDiscardCardViewNibName
                                                    owner:nil
                                                  options:nil];
        
        TAJDiscardCardView *card = nibContents[index];
        [card showCard:cardId isStarCardImage:[cardInfo[kTaj_Star_card] boolValue] isAutoPlayCard:cardInfo[@"TAJ_autoplay"]];
        
        
        if ([self.jokerCardId isEqualToString:cardNum])
        {
            [card setIsJoker:YES];
        }
        else
        {
            [card setIsJoker:NO];
        }
        
        CGRect cardFrame = card.frame;
        if ([TAJUtilities isIPhone])
        {
            cardFrame.size.width = 20;
            cardFrame.size.height = 26;
        }
        
        else
        {
            cardFrame.size.width = 50;
            cardFrame.size.height = 70;
        }
        card.frame = cardFrame;
        [self placeCard:card onScrollView:i];
    }
}

- (void)placeCard:(TAJDiscardCardView *)card onScrollView:(NSUInteger)index
{
    CGRect holderFrame = self.cardsScrolView.frame;
    CGRect cardFrame = card.frame;
    if ([TAJUtilities isIPhone])
    {
        if ([TAJUtilities isIPhone])
        {
            cardFrame.origin.x = index * (cardFrame.size.width + 5);
            [self.cardsScrolView setContentSize:CGSizeMake(self.cardsScrolView.contentSize.width +(cardFrame.size.width + 5) , self.cardsScrolView.frame.size.height)];
            cardFrame.origin.y = holderFrame.origin.y + 0.25;
            
        }
        else
        {
            cardFrame.origin.x = index * (cardFrame.size.width + 5);
            [self.cardsScrolView setContentSize:CGSizeMake(self.cardsScrolView.contentSize.width +(cardFrame.size.width + 5) , self.cardsScrolView.frame.size.height)];
            cardFrame.origin.y = holderFrame.origin.y;
            
        }
    }
    else
    {
        cardFrame.origin.x = index  * (cardFrame.size.width + 5);
        [self.cardsScrolView setContentSize:CGSizeMake(self.cardsScrolView.contentSize.width +(cardFrame.size.width + 5) , self.cardsScrolView.frame.size.height)];
        cardFrame.origin.y = holderFrame.origin.y;
    }
    
    // set height for card to fit in scroll view
    card.frame = cardFrame;
    [self.cardsScrolView addSubview:card];
    [self.cardsScrolView scrollRectToVisible:CGRectMake(self.cardsScrolView.contentSize.width -(cardFrame.size.width), 0, cardFrame.size.width , cardFrame.size.height) animated:YES];
    xindex++;
}

-(void)clearArrayContents
{
    [self removeCardsAlreadyInScrollView];
    [self.discardCardModel removePlayerDiscardedCardsOnReconnect];
}

@end
