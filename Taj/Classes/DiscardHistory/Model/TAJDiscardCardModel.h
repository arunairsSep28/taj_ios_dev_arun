/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJDiscardCardModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/04/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJDiscardCardModel : NSObject

@property (nonatomic, strong) NSMutableDictionary *discardedCardDictionary;

- (TAJDiscardCardModel *)initWithPlayerList:(NSMutableDictionary *)dictionary;
- (TAJDiscardCardModel *)updatePlayerList:(NSMutableDictionary *)dictionary;
- (TAJDiscardCardModel *)updatePlayerDiscardedCards:(NSMutableDictionary *)dictionary;
- (void)removePlayerDiscardedCardsOnReconnect;


@end
