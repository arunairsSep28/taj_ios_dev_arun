/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJDiscardCardModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/04/14.
 **/

#import "TAJDiscardCardModel.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"

@implementation TAJDiscardCardModel

- (TAJDiscardCardModel *)initWithPlayerList:(NSMutableDictionary *)dictionary
{
    self.discardedCardDictionary = [NSMutableDictionary dictionary];
    
    NSMutableArray *playerDiscardedCards = [NSMutableArray array];
    [self.discardedCardDictionary setObject:playerDiscardedCards forKey:dictionary[kTaj_nickname]];
    
    return self;
}

- (TAJDiscardCardModel *)updatePlayerList:(NSMutableDictionary *)dictionary
{
    NSString *playerName = dictionary[kTaj_nickname];
    NSString *playerNameAlreadyExist = self.discardedCardDictionary[playerName];
    if (playerNameAlreadyExist == nil)
    {
        NSMutableArray *anotherPlayer = [NSMutableArray array];
        [self.discardedCardDictionary setObject:anotherPlayer forKey:dictionary[kTaj_nickname]];
    }
    return self;
}

- (TAJDiscardCardModel *)updatePlayerDiscardedCards:(NSMutableDictionary *)dictionary
{
    BOOL isCardStar = NO;
    NSString *playerName = dictionary[kTaj_nickname];
    NSString *playerNameAlreadyExist = self.discardedCardDictionary[playerName];
    
    if (playerNameAlreadyExist == nil)
    {
        NSMutableArray *playerDiscardedCards = [NSMutableArray array];
        NSMutableDictionary *playerCardInfo = [NSMutableDictionary dictionary];
        [playerCardInfo setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
        [playerCardInfo setObject:dictionary[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
        [playerCardInfo setObject:dictionary[kTaj_autoPlay_status] forKey:kTaj_autoPlay_status];
        [playerCardInfo setObject:[NSNumber numberWithBool:isCardStar] forKey:kTaj_Star_card];
        
        [playerDiscardedCards addObject:playerCardInfo];
        
        [self.discardedCardDictionary setObject:playerDiscardedCards forKey:dictionary[kTaj_nickname]];
    }
    else
    {
        NSMutableArray *playerAlreadyDiscardedCards = self.discardedCardDictionary[playerName];
        NSMutableDictionary *playerCardInfo = [NSMutableDictionary dictionary];
        [playerCardInfo setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
        [playerCardInfo setObject:dictionary[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
        [playerCardInfo setObject:dictionary[kTaj_autoPlay_status] forKey:kTaj_autoPlay_status];
        [playerCardInfo setObject:[NSNumber numberWithBool:isCardStar] forKey:kTaj_Star_card];
        
        [playerAlreadyDiscardedCards addObject:playerCardInfo];
    }
    return self;
}

- (void)removePlayerDiscardedCardsOnReconnect
{
    [self.discardedCardDictionary removeAllObjects];
}


@end
