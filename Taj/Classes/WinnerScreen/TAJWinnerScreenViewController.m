/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWinnerScreenViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 03/04/14.
 **/

#import "TAJWinnerScreenViewController.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"

@interface TAJWinnerScreenViewController ()

@property (weak, nonatomic) IBOutlet UIButton *closeButton;
- (IBAction)closeButtonAction:(UIButton *)sender;

@end

@implementation TAJWinnerScreenViewController

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    
}

- (void)updateWinnerScreen:(NSMutableArray *)array
{
    TAJWinnerScreen *winnerView = nil;
    if (self.winnerScreenContainer.subviews && self.winnerScreenContainer.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [self.winnerScreenContainer subviews];
        winnerView = (TAJWinnerScreen *)subviews[0];
    }
    else
    {
        NSArray *nibContents;
        int index = 0;
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJWinnerViewNibName
                                                    owner:nil
                                                  options:nil];
        winnerView = nibContents[index];
        DLog(@"winnerscreen:%f,%f",winnerView.frame.origin.x,winnerView.frame.origin.y);
        DLog(@"winnerscreen:%f,%f",winnerView.frame.size.width,winnerView.frame.size.height);
       [self.winnerScreenContainer addSubview:winnerView];
    }
    
    [winnerView showWinners:array];
}

- (void)updateSplitResult:(NSMutableArray *)array
{
    TAJWinnerScreen *winnerView = nil;
    if (self.winnerScreenContainer.subviews && self.winnerScreenContainer.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [self.winnerScreenContainer subviews];
        winnerView = (TAJWinnerScreen *)subviews[0];
    }
    else
    {
        NSArray *nibContents;
        
        int index = 0;
        
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJWinnerViewNibName
                                                    owner:nil
                                                  options:nil];
        winnerView = nibContents[index];
        [self.winnerScreenContainer addSubview:winnerView];
    }
    [winnerView showSplitResult:array];
}

- (IBAction)closeButtonAction:(UIButton *)sender
{
    [self removeWinnerScreen];
}

- (void)removeWinnerScreen
{
    if ([self.winnerDelegate respondsToSelector:@selector(didWinnerScreenClosed)])
    {
        [self.winnerDelegate didWinnerScreenClosed];
    }
}

@end
