/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWinnerScreenViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 03/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJWinnerModel.h"
#import "TAJWinnerScreen.h"

@protocol TAJWinnerScreenDelegate;

@interface TAJWinnerScreenViewController : UIViewController

@property (nonatomic, strong) TAJWinnerModel    *winnerModel;
@property (weak, nonatomic) id<TAJWinnerScreenDelegate> winnerDelegate;
@property (weak, nonatomic) IBOutlet UIView *winnerScreenContainer;

- (void)updateWinnerScreen:(NSMutableArray *)array;
- (void)updateSplitResult:(NSMutableArray *)array;

@end

@protocol TAJWinnerScreenDelegate <NSObject>

- (void)didWinnerScreenClosed;

@end