/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWinnerModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/03/14.
 **/

#import "TAJWinnerModel.h"

@implementation TAJWinnerModel

- (TAJWinnerModel *)initWithWinnerResult:(NSMutableArray *)winnerArray
{
    return self;
}

@end
