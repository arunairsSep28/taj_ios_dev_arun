/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWinnerScreen.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/03/14.
 **/

#import "TAJWinnerScreen.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"

@interface TAJWinnerScreen ()

@property (nonatomic, weak) IBOutlet UILabel *winnerOneName;
@property (nonatomic, weak) IBOutlet UILabel *winnerTwoName;
@property (weak, nonatomic) IBOutlet UILabel *winnerThreeName;
@property (nonatomic, weak) IBOutlet UILabel *winnerOnePoints;
@property (nonatomic, weak) IBOutlet UILabel *winnerTwoPoints;
@property (weak, nonatomic) IBOutlet UILabel *winnerThreePoints;
@property (weak, nonatomic) IBOutlet UILabel *winner1RupeesSymbol;
@property (weak, nonatomic) IBOutlet UILabel *winner3RupeesSymbol;
@property (weak, nonatomic) IBOutlet UIImageView *winnerDeviderOne;
@property (weak, nonatomic) IBOutlet UIImageView *winnerDeviderTwo;

@property (weak, nonatomic) IBOutlet UILabel *winner2RupeesSymbol;
@property (nonatomic) CGRect winnerOneNameLabelFrame;
@property (nonatomic) CGRect winnerOnePointsLabelFrame;
@property (nonatomic) CGRect winner1RupeesSymbolLabelFrame;
@property (nonatomic) CGRect winnerTwoNameLabelFrame;
@property (nonatomic) CGRect winnerTwoPointsLabelFrame;
@property (nonatomic) CGRect winner2RupeesSymbolLabelFrame;

@end

@implementation TAJWinnerScreen

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        self.winnerOneNameLabelFrame = self.winnerOneName.frame;
        self.winnerOnePointsLabelFrame = self.winnerOnePoints.frame;
        self.winner1RupeesSymbolLabelFrame = self.winner1RupeesSymbol.frame;
        self.winnerTwoNameLabelFrame = self.winnerTwoName.frame;
        self.winnerTwoPointsLabelFrame = self.winnerTwoPoints.frame;
        self.winner2RupeesSymbolLabelFrame = self.winner2RupeesSymbol.frame;
    }
    return self;
}

- (void)showWinners:(NSMutableArray *)array
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    [formatter setMaximumFractionDigits:3];
    
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    if (array && array.count == 1)
    {
        // for pool winner / best 2, 3 6
        NSMutableDictionary *winnerDict = array[0]; //only one object
        // Capitalize first letter of winner name
        NSString *firstCapChar = [[winnerDict[kTaj_winner_name] substringToIndex:1] capitalizedString];
        NSString *cappedString = [winnerDict[kTaj_winner_name] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
        self.winnerOneName.text = cappedString;
        DLog(@"winnerDict[kTaj_winner_prizemoney] = %@",winnerDict[kTaj_winner_prizemoney]);
        if ([self checkIntOrFloat:winnerDict[kTaj_winner_prizemoney]])
        {
            float x = [winnerDict[kTaj_winner_prizemoney]floatValue];
            int y = floorf(x);
            if(y == x)
            {
              self.winnerOnePoints.text = [NSString stringWithFormat:@"%d",[winnerDict[kTaj_winner_prizemoney]intValue]];
            }
            else
            {
                NSRange range = [winnerDict[kTaj_winner_prizemoney] rangeOfString:@"."];
                
                if (range.length == 1)
                {
                    self.winnerOnePoints.text = winnerDict[kTaj_winner_prizemoney];
                }
                else if (range.length == 2)
                {
                   self.winnerOnePoints.text = [NSString stringWithFormat:@"%.02f",[winnerDict[kTaj_winner_prizemoney]floatValue]];
                }
                else if (range.length >= 3)
                {
                    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:[winnerDict[kTaj_split_amount] floatValue]]];
                    
                    DLog(@"Result...%@",numberString);//Result 10.36
                    self.winnerOnePoints.text = numberString;

                  // self.winnerOnePoints.text = [NSString stringWithFormat:@"%.03f",[winnerDict[kTaj_winner_prizemoney]floatValue]];
                }
            }
           
        }
        else
        {
            self.winnerOnePoints.text =winnerDict[kTaj_winner_prizemoney];
        }
        self.winner1RupeesSymbol.hidden = NO;
        self.winnerOneName.hidden = NO;
        self.winnerOnePoints.hidden = NO;
        self.winnerDeviderOne.hidden = YES;
        self.winnerTwoName.hidden = YES;
        self.winner2RupeesSymbol.hidden = YES;
        self.winnerTwoPoints.hidden = YES;
        self.winnerDeviderTwo.hidden = YES;
        self.winnerThreePoints.hidden = YES;
        self.winner3RupeesSymbol.hidden = YES;
        self.winnerThreeName.hidden = YES;
        CGRect xframe = self.winnerOneName.frame;
        CGRect zframe = self.winnerOnePoints.frame;
        CGRect pframe = self.winner1RupeesSymbol.frame;
        CGRect yframe = self.frame;
        xframe.origin.y = yframe.size.height/2 + 5;
        zframe.origin.y = yframe.size.height/2 + 5;
        pframe.origin.y = yframe.size.height/2 + 5;
        self.winnerOneName.frame = xframe;
        self.winnerOnePoints.frame = zframe;
        self.winner1RupeesSymbol.frame = pframe;
    }
}

- (void)showSplitResult:(NSMutableArray *)array
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    
    [formatter setNumberStyle:NSNumberFormatterDecimalStyle];
    
    [formatter setMaximumFractionDigits:3];
    
    [formatter setRoundingMode: NSNumberFormatterRoundUp];
    if(array && array.count == 2)
    {
        // for split_result
        // only 2 object will be exist
        NSMutableDictionary *winnerOneDict = array[0];
        // Capitalize first letter of winner name
        NSString *firstCapChar = [[winnerOneDict[kTaj_split_nickname] substringToIndex:1] capitalizedString];
        NSString *cappedString = [winnerOneDict[kTaj_split_nickname] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
        self.winnerOneName.text = cappedString;
        if ([self checkIntOrFloat:winnerOneDict[kTaj_split_amount]])
        {
            float x = [winnerOneDict[kTaj_split_amount]floatValue];
            int y = floorf(x);
            if(y == x)
            {
                self.winnerOnePoints.text = [NSString stringWithFormat:@"%d",[winnerOneDict[kTaj_split_amount]intValue]];
            }
            else
            {
                NSRange range = [winnerOneDict[kTaj_split_amount] rangeOfString:@"."];
                
                if (range.length == 1)
                {
                    self.winnerOnePoints.text =winnerOneDict[kTaj_split_amount];
                }
                else if (range.length == 2)
                {
                    self.winnerOnePoints.text = [NSString stringWithFormat:@"%.02f",[winnerOneDict[kTaj_split_amount]floatValue]];
                }
                else if (range.length >=3)
                {
                    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:[winnerOneDict[kTaj_split_amount] floatValue]]];
                    
                    DLog(@"Result...%@",numberString);//Result 10.36
                    self.winnerOnePoints.text = numberString;
                }
                
            }
        }
        else
        {
            self.winnerOnePoints.text = winnerOneDict[kTaj_split_amount];
        }
        self.winnerOnePoints.hidden = NO;
        self.winner1RupeesSymbol.hidden = NO;
        self.winnerOneName.hidden = NO;
        // second winner
        NSMutableDictionary *winnerTwoDict = array[1];
        // Capitalize first letter of winner name
        NSString *secondCapChar = [[winnerTwoDict[kTaj_split_nickname] substringToIndex:1] capitalizedString];
        NSString *secondCappedString = [winnerTwoDict[kTaj_split_nickname] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:secondCapChar];
        self.winnerTwoName.text = secondCappedString;
        if ([self checkIntOrFloat:winnerTwoDict[kTaj_split_amount]])
        {
            float x = [winnerTwoDict[kTaj_split_amount]floatValue];
            int y = floorf(x);
            if(y == x)
            {
                self.winnerTwoPoints.text = [NSString stringWithFormat:@"%d",[winnerTwoDict[kTaj_split_amount]intValue]];
            }
            else
            {
                NSRange range = [winnerTwoDict[kTaj_split_amount] rangeOfString:@"."];
                
                if (range.length == 1)
                {
                    self.winnerTwoPoints.text = winnerTwoDict[kTaj_split_amount];
                }
                else if (range.length == 2)
                {
                    self.winnerTwoPoints.text = [NSString stringWithFormat:@"%.02f",[winnerTwoDict[kTaj_split_amount]floatValue]];
                }
                else if (range.length >=3)
                {
                    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:[winnerTwoDict[kTaj_split_amount] floatValue]]];
                    
                    DLog(@"Result...%@",numberString);//Result 10.36
                    self.winnerTwoPoints.text = numberString;
                }
            }
        }
        else
        {
            self.winnerTwoPoints.text = winnerTwoDict[kTaj_split_amount];
        }

        
        self.winnerTwoName.hidden = NO;
        self.winner2RupeesSymbol.hidden = NO;
        self.winnerTwoPoints.hidden = NO;
        self.winnerDeviderOne.hidden = NO;
        self.winnerDeviderTwo.hidden = NO;
        self.winnerThreePoints.hidden = YES;
        self.winner3RupeesSymbol.hidden = YES;
        self.winnerThreeName.hidden = YES;
    }
    if(array && array.count == 3)
    {
        NSMutableDictionary *winnerOneDict = array[0];
        // Capitalize first letter of winner name
        NSString *firstCapChar = [[winnerOneDict[kTaj_split_nickname] substringToIndex:1] capitalizedString];
        NSString *cappedString = [winnerOneDict[kTaj_split_nickname] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
        self.winnerOneName.text = cappedString;
        if ([self checkIntOrFloat:winnerOneDict[kTaj_split_amount]])
        {
            float x = [winnerOneDict[kTaj_split_amount]floatValue];
            int y = floorf(x);
            if(y == x)
            {
                self.winnerOnePoints.text = [NSString stringWithFormat:@"%d",[winnerOneDict[kTaj_split_amount]intValue]];
            }
            else
            {
                NSRange range = [winnerOneDict[kTaj_split_amount] rangeOfString:@"."];
                
                if (range.length == 1)
                {
                    self.winnerOnePoints.text = winnerOneDict[kTaj_split_amount];
                }
                else if (range.length == 2)
                {
                    self.winnerOnePoints.text = [NSString stringWithFormat:@"%.02f",[winnerOneDict[kTaj_split_amount]floatValue]];
                }
                else if (range.length >=3)
                {
                    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:[winnerOneDict[kTaj_split_amount] floatValue]]];
                    
                    DLog(@"Result...%@",numberString);//Result 10.36
                    self.winnerOnePoints.text = numberString;
                }
                
            }
        }
        else
        {
            self.winnerOnePoints.text = winnerOneDict[kTaj_split_amount];
        }
        self.winnerOnePoints.hidden = NO;
        self.winner1RupeesSymbol.hidden = NO;
        self.winnerOneName.hidden = NO;
        // Second winner
        NSMutableDictionary *winnerTwoDict = array[1];
        // Capitalize first letter of winner name
        NSString *secondCapChar = [[winnerTwoDict[kTaj_split_nickname] substringToIndex:1] capitalizedString];
        NSString *secondCappedString = [winnerTwoDict[kTaj_split_nickname] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:secondCapChar];
        self.winnerTwoName.text = secondCappedString;
        self.winnerTwoName.hidden = NO;
        if ([self checkIntOrFloat:winnerTwoDict[kTaj_split_amount]])
        {
            float x = [winnerTwoDict[kTaj_split_amount]floatValue];
            int y = floorf(x);
            if(y == x)
            {
                self.winnerTwoPoints.text = [NSString stringWithFormat:@"%d",[winnerTwoDict[kTaj_split_amount]intValue]];
            }
            else
            {
                NSRange range = [winnerTwoDict[kTaj_split_amount] rangeOfString:@"."];
                
                if (range.length == 1)
                {
                    self.winnerTwoPoints.text = winnerTwoDict[kTaj_split_amount];
                }
                else if (range.length == 2)
                {
                    self.winnerTwoPoints.text = [NSString stringWithFormat:@"%.02f",[winnerTwoDict[kTaj_split_amount]floatValue]];
                }
                else if (range.length >=3)
                {
                    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:[winnerTwoDict[kTaj_split_amount] floatValue]]];
                    
                    DLog(@"Result...%@",numberString);//Result 10.36
                    self.winnerTwoPoints.text = numberString;
                }
                
            }
        }
        else
        {
            self.winnerTwoPoints.text = winnerTwoDict[kTaj_split_amount];
        }

        self.winner2RupeesSymbol.hidden = NO;
        self.winnerTwoPoints.hidden = NO;
        self.winnerDeviderOne.hidden = NO;
        self.winnerDeviderTwo.hidden = NO;
        // Third winner
        NSMutableDictionary *winnerThreeDict = array[2];
        // Capitalize first letter of winner name
        NSString *thirdCapChar = [[winnerThreeDict[kTaj_split_nickname] substringToIndex:1] capitalizedString];
        NSString *thirdCappedString = [winnerThreeDict[kTaj_split_nickname] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:thirdCapChar];
        self.winnerThreeName.text = thirdCappedString;
        if ([self checkIntOrFloat:winnerThreeDict[kTaj_split_amount]])
        {
            float x = [winnerThreeDict[kTaj_split_amount]floatValue];
            int y = floorf(x);
            if(y == x)
            {
                self.winnerThreePoints.text = [NSString stringWithFormat:@"%d",[winnerThreeDict[kTaj_split_amount]intValue]];
            }
            else
            {
                NSRange range = [winnerThreeDict[kTaj_split_amount] rangeOfString:@"."];
                
                if (range.length == 1)
                {
                    self.winnerThreePoints.text = winnerThreeDict[kTaj_split_amount];

                }
                else if (range.length == 2)
                {
                    self.winnerThreePoints.text = [NSString stringWithFormat:@"%.02f",[winnerThreeDict[kTaj_split_amount] floatValue]];

                }
                else if (range.length >=3)
                {
                    
                    
                    NSString *numberString = [formatter stringFromNumber:[NSNumber numberWithFloat:[winnerThreeDict[kTaj_split_amount] floatValue]]];
                    
                    DLog(@"Result...%@",numberString);//Result 10.36
                    self.winnerThreePoints.text = numberString;

                }
            }
        }
        else
        {
            self.winnerThreePoints.text = winnerThreeDict[kTaj_split_amount];
        }
        self.winnerThreePoints.hidden = NO;
        self.winner3RupeesSymbol.hidden = NO;
        self.winnerThreeName.hidden = NO;
    }
}

- (BOOL)checkIntOrFloat:(NSString *)givenString
{
    NSRange range = [givenString rangeOfString:@"."];
    
    if (range.location != NSNotFound)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

@end
