/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSplashViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogish Poojary on 24/04/14.
 Created by Raghavendra Kamat on 03/04/14.
 **/

#import "TAJSplashViewController.h"
#import "TAJUtilities.h"
#import "TAJGameEngine.h"
#import "TAJClientServerCommunicator.h"
#import "TAJPreLobbyViewController.h"
#import "TAJAppDelegate.h"


@interface TAJSplashViewController ()
@property(strong,nonatomic) NSArray *imageViewAnimationArray;
@property(strong,nonatomic) NSString *username, *password, *sessionID;

@end

@implementation TAJSplashViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
        _infoPopupViewController = Nil;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    //Initializing the shared utilities
    [TAJUtilities sharedUtilities];
    
//    if([TAJUtilities isIPhone])
//    {
//        self.imageViewAnimationArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"loading_animation1-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation2-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation3-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation4-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation5-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation6-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation7-568h@2x~iphone.png"],[UIImage imageNamed:@"loading_animation8-568h@2x~iphone.png"], nil];
//    }
//    else
//    {
        self.imageViewAnimationArray = [NSArray arrayWithObjects:[UIImage imageNamed:@"loading_1.png"],[UIImage imageNamed:@"loading_2.png"],[UIImage imageNamed:@"loading_3.png"],[UIImage imageNamed:@"loading_4.png"],[UIImage imageNamed:@"loading_5.png"],[UIImage imageNamed:@"loading_6.png"],[UIImage imageNamed:@"loading_7.png"],[UIImage imageNamed:@"loading_8.png"], nil];
//    }
    
    self.loginLoadingIndicatorImageView.animationImages = self.imageViewAnimationArray;
    self.loginLoadingIndicatorImageView.animationDuration = 0.55f;
    self.loginLoadingIndicatorImageView.animationRepeatCount = 0;
    
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    // Do any additional setup after loading the view from its nib.
    self.loginHomeScreenTimer = Nil;
    
    [self handleLoginCases];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [NSNOTIFICATION_CENTRE_DEFAULT addObserver:self selector:@selector(moveToHomePage:) name:MOVETOHOMEPAGE object:Nil];
    [NSNOTIFICATION_CENTRE_DEFAULT addObserver:self selector:@selector(handleSocketError:) name:ON_SOCKET_ERROR object:Nil];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [NSNOTIFICATION_CENTRE_DEFAULT removeObserver:self name:MOVETOHOMEPAGE object:Nil];
    [NSNOTIFICATION_CENTRE_DEFAULT removeObserver:self name:ON_SOCKET_ERROR object:Nil];
    [super viewWillDisappear:animated];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
    
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)moveToHomePage:(NSNotification*)notification
{
    TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
    
#if NEW_DESIGN_IMPLEMENTATION
    [delegate loadPrelobbyScreen];
#else
    [delegate loadHomePageWindow];
#endif
    
}

- (void)handleSocketError:(NSNotification*)notification
{
    [self.retry setHidden:NO];
    [self.activityMessage setText:TAP_HERE_TO_RETRY];
    
    [self.loginLoadingIndicatorImageView stopAnimating];
    self.loginLoadingIndicatorImageView.hidden = YES;
    self.centerIconImageView.hidden = YES;
    
    if (!self.infoPopupViewController)
    {
        self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:PLEASE_CHECK_YOUR_INTERNET_CONNECTION];
        
        [self.infoPopupViewController show];
        
        //self.infoPopupViewController.view.center = self.view.center;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (IBAction)retryLogin:(id)sender
{
    [self handleLoginCases];
}

- (void) handleLoginCases
{
    
#if USERNAME_PASSWORD_STORE_CHECK
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    self.username = Nil;
    self.password = Nil;
    self.sessionID = Nil;

    self.username = [defaults objectForKey:USENAME_KEY];
    self.password = [defaults objectForKey:PASSWORD_KEY];
    self.sessionID = [defaults objectForKey:UNIQUE_SESSION_KEY];
#endif
    //If username and password is saved
    if (self.username && self.password)
    {
        //manual login here to change ip to DYNAMIC
        
        if ([[TAJUtilities sharedUtilities] isInternetConnected])
        {
            [self.activityMessage setText:LOGGING_IN];
            self.centerIconImageView.hidden = NO;
            self.loginLoadingIndicatorImageView.hidden = NO;
            [self.loginLoadingIndicatorImageView startAnimating];
            [self.retry setHidden:YES];
            
            //start game engine [login action]
//            [TAJ_GAME_ENGINE createUserModelWithUserName: self.username password: self.password sessionId:self.sessionID];
//            [TAJ_CLIENT_SERVER_COMMUNICATOR createSocketAndConnect];
//            [TAJ_GAME_ENGINE setCurrentController:self];
            [self manualLogin];
            
        }
        else
        {
            self.loginLoadingIndicatorImageView.hidden = YES;
            self.centerIconImageView.hidden = YES;
            [self.retry setHidden:NO];
        }
    }
    else  //If username and password is not saved
    {
        if ([[TAJUtilities sharedUtilities] isInternetConnected])
        {
            //Show activity and load login home screen
            self.centerIconImageView.hidden = NO;
            self.loginLoadingIndicatorImageView.hidden = NO;
            [self.loginLoadingIndicatorImageView startAnimating];
            [self.retry setHidden:YES];
            [self.activityMessage setText:LOADING_LOGIN_HOME];
            self.loginHomeScreenTimer = [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(showLoginHomeScreen) userInfo:nil repeats:NO];
        }
        else
        {
            if (!self.infoPopupViewController)
            {
                self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:PLEASE_CHECK_YOUR_INTERNET_CONNECTION];
                [self.infoPopupViewController show];
                
                //self.infoPopupViewController.view.center = self.view.center;
            }
        
            [self.loginLoadingIndicatorImageView stopAnimating];
            self.retry.hidden = NO;
            self.loginLoadingIndicatorImageView.hidden = YES;
            self.centerIconImageView.hidden = YES;
        }
    }
    if (![[TAJUtilities sharedUtilities] isInternetConnected])
    {
        if (!self.infoPopupViewController)
        {
            self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:PLEASE_CHECK_YOUR_INTERNET_CONNECTION];
            [self.infoPopupViewController show];
            
            //self.infoPopupViewController.view.center = self.view.center;
        }
    }
}

- (void) hideTheActivity
{
    
}

- (void) showLoginHomeScreen
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for showLoginHomeScreen");
#endif
    
    [self.loginHomeScreenTimer invalidate];
    self.loginHomeScreenTimer = Nil;
    
    TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate loadMainWindow];
    
}

-(void) yesButtonPressedForInfoPopup:(id)object
{
    
}

-(void) noButtonPressedForInfoPopup:(id)object
{
    
}

-(void) okButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

-(void) closeButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}


-(void)manualLogin
{
    NSLog(@"CHECKING LOGIN EVERY TIME");
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * deviceType;
    
    if ([TAJUtilities isIPhone])
    {
        deviceType = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceType = @"Tablet";//@"IPAD";
    }
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSDictionary *params = @{@"username": self.username,
                             @"password": self.password,
                             //@"device_id": @"1",
                             @"device_type" : deviceType,
                             @"client_type" : @"iOS",
                             @"version" : buildNumber,
                             @"device_id": [UIDevice currentDevice].identifierForVendor,
                             @"device_brand": [TAJUtilities checkIfStringIsEmpty:APP_DELEGATE.brandName]
                             };
#if DEBUG
    NSLog(@"LOGIN PARAMS: %@", params);
#endif
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,LOGIN];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:NO headerValue:nil withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view hideToastActivity];
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        NSDictionary * jsonDictionay = jsondata;
                        NSDictionary *tempDict = [jsonDictionay dictionaryRemovingNSNullValues];
                        //NSLog(@"%@", tempDict);
#if DEBUG
                        NSLog(@"LOGIN RESPONSE: %@", tempDict);
                        NSLog(@"unique_id : %@",[tempDict valueForKey:@"unique_id"]);
                        NSLog(@"playerid : %@",[tempDict valueForKey:@"playerid"]);
                        NSLog(@"token : %@",[tempDict valueForKey:@"token"]);
#endif
                        APP_DELEGATE.dynamicIP = [tempDict valueForKey:@"ip"];
                        
                        NSString *playerId = [tempDict valueForKey:@"playerid"];
                        NSString * mobile = [tempDict valueForKey:@"mobile"];

                        if ([mobile isEqualToString:@""]) {
                            //
                        }
                        else {
                            [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"mobile"] forKey:USERMOBILE_KEY];
                        }
                        
                        
                        //need to save for future reference - token
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"unique_id"] forKey:UNIQUE_SESSION_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"email"] forKey:USEREMAIL_KEY];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"username"] forKey:USENAME_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"token"] forKey:TOKEN_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:playerId forKey:USERID];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSString * uniqueID = [jsondata valueForKey:@"unique_id"];
                        
                        [TAJ_GAME_ENGINE createUserModelWithUserName: self.username password: self.password sessionId:uniqueID];
                        [TAJ_CLIENT_SERVER_COMMUNICATOR createSocketAndConnect];
                        [TAJ_GAME_ENGINE setCurrentController:self];
                        NSLog(@"CHECKING DYNAMIC IP");
                        
                    }
                    else {
                        NSLog(@"REST Error : %@",jsondata);
                        [self.view makeToast:[jsondata valueForKey:@"error"]];
                        
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                [self.view makeToast:@"Error while fetching data from server"];
            }
        });
    }];
    
}

@end
