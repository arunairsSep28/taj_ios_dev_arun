/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJActivityIndicatorView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogisha Poojary on 24/04/14.
 **/

#import "TAJActivityIndicatorView.h"

NSString* const TAJActivityAnimation = @"ActivityRunning";
NSString* const TAJAnimationID  = @"AnimationID";

@implementation TAJActivityIndicatorView


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    
    if(self)
    {
        self.hidden = YES;
        _period = 1.35f;
    }
    
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    
    if(self)
    {
        self.hidden = YES;
    }
    
    return self;
}

- (void)startAnimating
{
    _period = 1.35f;
    
    if([self.layer animationForKey:TAJActivityAnimation] == nil)
    {
        self.hidden = NO;
        CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform.rotation.z"];
        animation.delegate      = self;
        animation.fromValue     = [NSNumber numberWithFloat:0.0f];
        animation.toValue       = [NSNumber numberWithFloat:2.0f * M_PI];
        animation.beginTime     = CACurrentMediaTime() + 0.0f;
        animation.duration      = self.period;
        animation.repeatCount   = HUGE_VAL;
        animation.autoreverses  = NO;
        [animation setValue:TAJActivityAnimation forKey:TAJAnimationID];
        
        animation.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionLinear];
        [self.layer addAnimation:animation forKey:TAJActivityAnimation];
    }
    else
    {
        self.hidden = NO;
    }
}

- (void)stopAnimating
{
    [self.layer removeAnimationForKey:TAJActivityAnimation];
    self.hidden = YES;
}

@end
