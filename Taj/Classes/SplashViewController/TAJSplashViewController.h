/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSplashViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogish Poojary on 24/04/14.
 Created by Raghavendra Kamat on 03/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJBaseViewController.h"
#import "TAJActivityIndicatorView.h"
#import "TAJInfoPopupViewController.h"


@interface TAJSplashViewController : TAJBaseViewController<InfoPopupViewDelegate>
{
    
}

@property (strong, nonatomic) NSTimer *loginHomeScreenTimer;
@property (strong,nonatomic) TAJActivityIndicatorView *spinner;

@property (strong, nonatomic) IBOutlet UIView *view;
@property (strong, nonatomic) IBOutlet UILabel *activityMessage;
@property (strong, nonatomic) IBOutlet UIButton *retry;
@property (strong, nonatomic) IBOutlet UIImageView *loginLoadingIndicatorImageView;
@property (strong, nonatomic) IBOutlet UIImageView *centerIconImageView;
@property (strong, nonatomic) TAJInfoPopupViewController *infoPopupViewController;

- (IBAction)retryLogin:(id)sender;

- (void) handleLoginCases;
- (void) hideTheActivity;
- (void) showLoginHomeScreen;

@end
