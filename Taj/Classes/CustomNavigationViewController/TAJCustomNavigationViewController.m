/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJCustomNavigationViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 09/05/14.
 **/

#import "TAJCustomNavigationViewController.h"

@interface TAJCustomNavigationViewController ()

@end

@implementation TAJCustomNavigationViewController

//- (BOOL)shouldAutorotate
//{
//    return self.topViewController.shouldAutorotate;
//}
- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return self.topViewController.supportedInterfaceOrientations;
}




@end
