/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLiveFeedManager.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 09/04/14.
 **/

#import "TAJLiveFeedManager.h"
#import "TAJConstants.h"

@implementation TAJLiveFeedManager

+ (NSString *)playerJoinedMessage:(NSDictionary *)dictionary
{
    NSString *playerJoinMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_JOIN,dictionary[kTaj_nickname]];
    
    return playerJoinMsg;
}

+ (NSString *)playerNewGameMessage:(NSDictionary *)dictionary isJokerGame:(BOOL)jokerGame noOfRound:(int)rounds
{
    NSString *playerStartMsg = (jokerGame ? LIVE_FEED_JOKER_GAME_START : LIVE_FEED_NORMAL_GAME_START);
    NSString *tableRoundMessage = [NSString stringWithFormat:@"%@%d",playerStartMsg,rounds];
    
    return tableRoundMessage;
}

+ (NSString *)playerWonTossMessage:(NSDictionary *)dictionary
{
    NSString *playerWonTossMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_WON_TOSS,dictionary[kTaj_Toss_Winner_Key]];
    
    return playerWonTossMsg;
}

+ (NSString *)playerDealerMessage:(NSDictionary *)dictionary
{
    NSString *playerDealerMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_DEALER,dictionary[kTaj_Dealer_name]];
    
    return playerDealerMsg;
}

+ (NSString *)playerDiscardMessage:(NSDictionary *)dictionary
{
    NSString *discardedCard = [self getCardSuitName:dictionary[kTaj_Card_Suit_Key]];
    NSString *playerDiscardMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_DISCARD_MSG,dictionary[kTaj_nickname],discardedCard,[self cardNumber:[dictionary[kTaj_Card_Face_Key] integerValue]]];
    
    return playerDiscardMsg;
}

+ (NSString *)getCardSuitName:(NSString *)cardSuitKey
{
    NSString *cardSuitString;
    int cardSuit = [self getSuitName:cardSuitKey];
    switch (cardSuit)
    {
        case 1:
            return cardSuitString = @"Spade";
            break;
        case 2:
            return cardSuitString = @"Heart";
            break;
        case 3:
            return cardSuitString = @"Diamond";
            break;
        case 4:
            return cardSuitString = @"Club";
            break;
        case 5:
            return cardSuitString = @"Joker";
            break;
        default:
            break;
    }
    return nil;
}

+ (int)getSuitName:(NSString *)suit
{
    if ([suit isEqualToString:@"s"])
    {
        return 1;
    }
    else if ([suit isEqualToString:@"h"])
    {
        return 2;
    }
    else if ([suit isEqualToString:@"d"])
    {
        return 3;
    }
    else if ([suit isEqualToString:@"c"])
    {
        return 4;
    }
    else if ([suit isEqualToString:@"jo"])
    {
        return 5;
    }
    return 0;
}

+ (NSString *)cardNumber:(int)cardNumber
{
    NSString *cardNumberString;
    switch (cardNumber)
    {
        case 1:
            cardNumberString = @"A";
            break;
        case 2:
            cardNumberString = @"2";
            break;
        case 3:
            cardNumberString = @"3";
            break;
        case 4:
            cardNumberString = @"4";
            break;
        case 5:
            cardNumberString = @"5";
            break;
        case 6:
            cardNumberString = @"6";
            break;
        case 7:
            cardNumberString = @"7";
            break;
        case 8:
            cardNumberString = @"8";
            break;
        case 9:
            cardNumberString = @"9";
            break;
        case 10:
            cardNumberString = @"10";
            break;
        case 11:
            cardNumberString = @"J";
            break;
        case 12:
            cardNumberString = @"Q";
            break;
        case 13:
            cardNumberString = @"K";
            break;
            
        default:
            cardNumberString = @""; // for joker card number is zero, so no card number
            break;
    }
    return cardNumberString;
}

+ (NSString *)playerPickOpenDeckMessage:(NSDictionary *)dictionary
{
    NSString *playerPickMsg = [NSString stringWithFormat:LIVE_FEED_PICK_FROM_OPEN_DECK,dictionary[kTaj_nickname]];
    
    return playerPickMsg;
}

+ (NSString *)playerPickClosedDeckMessage:(NSDictionary *)dictionary
{
    NSString *playerPickMsg = [NSString stringWithFormat:LIVE_FEED_PICK_FROM_CLOSED_DECK,dictionary[kTaj_nickname]];
    return playerPickMsg;
}

+ (NSString *)playerDropOrTimeOutMessage:(NSDictionary *)dictionary
{
    NSString *playerDropMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_DROP_MSG,dictionary[kTaj_nickname]];
    return playerDropMsg;
}

+ (NSString *)playerExtraTimeMessage:(NSDictionary *)dictionary
{
    NSString *playerTimeMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_EXTRA_TIME,dictionary[kTaj_nickname]];
    return playerTimeMsg;
}

+ (NSString *)playerLeftTableMessage:(NSDictionary *)dictionary
{
    NSString *playerLeftMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_LEFT_TABLE,dictionary[kTaj_nickname]];
    return playerLeftMsg;
}

+ (NSString *)playerRequestedShowMessage:(NSDictionary *)dictionary
{
    NSString *playerShowMsg;
    if(dictionary[kTaj_nickname])
    {
        playerShowMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_REQUEST_SHOW,dictionary[kTaj_nickname]];
   
    }
    else
    {
        playerShowMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_REQUEST_SHOW,dictionary[@"TAJ_sucess_nickname"]];

    }
    return playerShowMsg;
}

+ (NSString *)playerInvalidShowMessage:(NSDictionary *)dictionary
{

    NSString *playerWrongShowMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_INVALID_SHOW,dictionary[kTaj_nickname]];
    return playerWrongShowMsg;
}

+ (NSString *)playerValidShowMessage:(NSDictionary *)dictionary
{
    NSString *playerCorrectShowMsg;
    if(dictionary[kTaj_nickname])
    {
        playerCorrectShowMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_VALID_SHOW,dictionary[kTaj_nickname]];

    }
    else
    {
        playerCorrectShowMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_VALID_SHOW,dictionary[@"TAJ_sucess_nickname"]];
 
    }
    return playerCorrectShowMsg;
}

+ (NSString *)playerEliminatedFromGameMessage:(NSDictionary *)dictionary
{
    NSString *playerEliminateMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_ELIMINATED,dictionary[kTaj_nickname]];
    return playerEliminateMsg;
}

+ (NSString *)playerMiddleJoinMessage:(NSDictionary *)dictionary
{
    NSString *playerMiddleJoinMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_MIDDLE_JOIN,dictionary[kTaj_nickname]];
    return playerMiddleJoinMsg;
}

+ (NSString *)playerRejoinMessage:(NSDictionary *)dictionary
{
    NSString *playerRejoinMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_REJOIN_MSG,dictionary[kTaj_nickname],dictionary[kTaj_rejoin_score]];
    return playerRejoinMsg;
}

+ (NSString *)playerSplitRequestPrizeMessage:(NSDictionary *)dictionary
{
    NSString *playerSplitReqMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_SPLIT_REQUEST,dictionary[kTaj_split_requester]];
    return playerSplitReqMsg;
}

+ (NSString *)playerSplitRejectedMessage:(NSDictionary *)dictionary
{
    NSString *playerSplitRejectMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_SPLIT_REJECT,dictionary[kTaj_split_status]];
    return playerSplitRejectMsg;
}

+ (NSString *)playerDisconnectedMessage:(NSDictionary *)dictionary
{
    NSString *playerDisconnectMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_DISCONNECTED,dictionary[kTaj_nickname]];
    return playerDisconnectMsg;
}

+ (NSString *)playerConnectedBackMessage:(NSDictionary *)dictionary
{
    NSString *playerConnectMsg = [NSString stringWithFormat:LIVE_FEED_PLAYER_CONNECTED,dictionary[kTaj_nickname]];
    return playerConnectMsg;
}

@end
