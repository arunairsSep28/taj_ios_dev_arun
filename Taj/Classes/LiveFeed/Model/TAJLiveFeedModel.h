/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLiveFeedModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 09/04/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJLiveFeedModel : NSObject

@property (nonatomic, strong) NSMutableArray *liveFeedSource;

- (TAJLiveFeedModel *)initWithFeedModel:(NSString *)feedMessage;
- (TAJLiveFeedModel *)updateFeedMessage:(NSString *)feedMessage;

@end
