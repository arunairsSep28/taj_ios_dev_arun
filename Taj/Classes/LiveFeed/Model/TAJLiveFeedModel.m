/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLiveFeedModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 09/04/14.
 **/

#import "TAJLiveFeedModel.h"

@implementation TAJLiveFeedModel

- (TAJLiveFeedModel *)initWithFeedModel:(NSString *)feedMessage
{
    self.liveFeedSource = [NSMutableArray array];
    
    [self.liveFeedSource addObject:feedMessage];
    return self;
}

- (TAJLiveFeedModel *)updateFeedMessage:(NSString *)feedMessage
{
    [self.liveFeedSource addObject:feedMessage];
    
    return self;
}

@end
