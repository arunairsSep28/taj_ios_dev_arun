/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLiveFeedManager.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 09/04/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJLiveFeedManager : NSObject

+ (NSString *)playerJoinedMessage:(NSDictionary *)dictionary;
+ (NSString *)playerNewGameMessage:(NSDictionary *)dictionary isJokerGame:(BOOL)jokerGame noOfRound:(int)rounds;
+ (NSString *)playerWonTossMessage:(NSDictionary *)dictionary;
+ (NSString *)playerDealerMessage:(NSDictionary *)dictionary;
+ (NSString *)playerDiscardMessage:(NSDictionary *)dictionary;
+ (NSString *)playerPickOpenDeckMessage:(NSDictionary *)dictionary;
+ (NSString *)playerPickClosedDeckMessage:(NSDictionary *)dictionary;
+ (NSString *)playerDropOrTimeOutMessage:(NSDictionary *)dictionary;
+ (NSString *)playerExtraTimeMessage:(NSDictionary *)dictionary;
+ (NSString *)playerLeftTableMessage:(NSDictionary *)dictionary;
+ (NSString *)playerRequestedShowMessage:(NSDictionary *)dictionary;
+ (NSString *)playerInvalidShowMessage:(NSDictionary *)dictionary;
+ (NSString *)playerValidShowMessage:(NSDictionary *)dictionary;
+ (NSString *)playerEliminatedFromGameMessage:(NSDictionary *)dictionary;
+ (NSString *)playerMiddleJoinMessage:(NSDictionary *)dictionary;
+ (NSString *)playerRejoinMessage:(NSDictionary *)dictionary;
+ (NSString *)playerSplitRequestPrizeMessage:(NSDictionary *)dictionary;
+ (NSString *)playerSplitRejectedMessage:(NSDictionary *)dictionary;
+ (NSString *)playerDisconnectedMessage:(NSDictionary *)dictionary;
+ (NSString *)playerConnectedBackMessage:(NSDictionary *)dictionary;

@end
