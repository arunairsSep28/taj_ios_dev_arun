/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLiveFeedViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 09/04/14.
 **/

#import "TAJLiveFeedViewController.h"
#import "TAJLiveFeedCell.h"

#import "TAJConstants.h"
#import "TAJUtilities.h"

@interface TAJLiveFeedViewController ()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic)  int                          scrollPosition;
@property (weak, nonatomic) IBOutlet UITableView *liveFeedTableView;
@property (weak, nonatomic) IBOutlet UIButton *upArraowButton;
@property (weak, nonatomic) IBOutlet UIButton *downArrowButton;
@property (nonatomic) BOOL isDataReloaded;
- (IBAction)upArrowButtonAction:(UIButton *)sender;
- (IBAction)downArrowButtonAction:(UIButton *)sender;

@end

@implementation TAJLiveFeedViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self.upArraowButton setEnabled: NO];
    [self.downArrowButton setEnabled:NO];
    self.isDataReloaded = NO;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)isLiveFeedShowing
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

- (void)reloadLiveFeedData
{
    //DLog(@"count = %d",self.feedModel.liveFeedSource.count);
    
    
    if (self.feedModel.liveFeedSource.count > 0)
    {
        [self.liveFeedTableView reloadData];
        
        [self.liveFeedTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.feedModel.liveFeedSource.count-1 inSection:0]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        [self.upArraowButton setEnabled: YES];
        [self.downArrowButton setEnabled:YES];
    }
}

- (IBAction)upArrowButtonAction:(UIButton *)sender
{
    NSArray *visible = [self.liveFeedTableView indexPathsForVisibleRows];
    
    NSIndexPath *indexpath = (NSIndexPath*)[visible firstObject];
    self.scrollPosition = indexpath.row - 1;
    if ((self.scrollPosition <= (self.feedModel.liveFeedSource.count - 1)) && self.scrollPosition >= 0)
    {
        [self.liveFeedTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.scrollPosition inSection:0]
                                      atScrollPosition:UITableViewScrollPositionTop animated:YES];
    }
    
    //DLog(@"self.scrollPosition = %d",self.scrollPosition);
}

- (IBAction)downArrowButtonAction:(UIButton *)sender
{
    NSArray *visible = [self.liveFeedTableView indexPathsForVisibleRows];
    
    NSIndexPath *indexpath = (NSIndexPath*)[visible lastObject];
    self.scrollPosition = indexpath.row + 1;
    
    //self.scrollPosition++;
    if ((self.scrollPosition <= (self.feedModel.liveFeedSource.count - 1)) && self.scrollPosition >= 0)
    {
        [self.liveFeedTableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:self.scrollPosition  inSection:0]
                                      atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    }
    //DLog(@"self.scrollPosition = %d",self.scrollPosition);
}

#pragma mark - Table view data source -

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.feedModel.liveFeedSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"LiveFeedCell";
    TAJLiveFeedCell *cell = [tableView dequeueReusableCellWithIdentifier:identifier];
    
    if (!cell)
    {
        NSArray *array = [[NSBundle mainBundle] loadNibNamed:TAJLiveFeedCellNibName
                                                       owner:nil options:nil];
        
        if([TAJUtilities isIPhone])
        {
            if([TAJUtilities isItIPhone5])
            {
                cell = array[0];
            }
            else
            {
                cell = array[1];
            }
        }
        else
        {
            cell = array[0];
        }
        
    }
    cell.userInteractionEnabled = NO;
    cell.liveFeedCell.text = self.feedModel.liveFeedSource[indexPath.row];
    cell.backgroundColor = [UIColor clearColor];
    cell.contentView.backgroundColor = [UIColor clearColor];
    return cell;
}

@end
