/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLiveFeedViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 09/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJLiveFeedModel.h"
#import "TAJBaseViewController.h"

@interface TAJLiveFeedViewController : TAJBaseViewController

@property (nonatomic, strong) TAJLiveFeedModel *feedModel;

- (void)reloadLiveFeedData;
- (BOOL)isLiveFeedShowing;

@end
