/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableDataModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 28/01/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJTableDataModel : NSObject

@property(nonatomic,strong) NSString *bet;
@property(nonatomic,strong) NSString *currentPlayers;
@property(nonatomic,strong) NSString *currentPlayer;

@property(nonatomic,strong) NSString *gameSchedule;
@property(nonatomic,strong) NSString *gameStart;

@property(nonatomic,strong) NSString *maxBuyin;
@property(nonatomic,strong) NSString *minBuyin;

@property(nonatomic,strong) NSString *maxPlayer;
@property(nonatomic,strong) NSString *minPlayer;

@property(nonatomic,strong) NSString *scheduleName;
@property(nonatomic,strong) NSString *tableCost;
@property(nonatomic,strong) NSString *tableType;

@property(nonatomic,strong) NSString *tableId;
@property(nonatomic,strong) NSString *totalPlayer;

@property(assign, nonatomic) BOOL favorite;

@property(nonatomic,strong) NSDictionary* tableDictionary;

- (TAJTableDataModel*)initWithTableDictionary:(NSMutableDictionary *)dictionary;

-(NSString *)getProperNameForTableType:(NSString*)type;

@end
