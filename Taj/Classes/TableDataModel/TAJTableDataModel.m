/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJTableDataModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 28/01/14.
 **/

#import "TAJTableDataModel.h"
#import "TAJConstants.h"

@implementation TAJTableDataModel

@synthesize bet             = _bet;
@synthesize currentPlayers  = _currentPlayers;
@synthesize currentPlayer = _currentPlayer;
@synthesize gameSchedule    = _gameSchedule;
@synthesize gameStart       = _gameStart;
@synthesize maxBuyin        = _maxBuyin;
@synthesize maxPlayer       = _maxPlayer;
@synthesize minBuyin        = _minBuyin;
@synthesize minPlayer       = _minPlayer;
@synthesize scheduleName    = _scheduleName;
@synthesize tableCost       = _tableCost;
@synthesize tableId         = _tableId;
@synthesize tableType       = _tableType;
@synthesize totalPlayer     = _totalPlayer;
@synthesize favorite = _favorite;

@synthesize tableDictionary = _tableDictionary;

- (TAJTableDataModel*)initWithTableDictionary:(NSMutableDictionary *)dictionary
{
    self.tableDictionary=dictionary;
    
    self.bet = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_BET] ];
    self.maxPlayer = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_MAX_PLAYER] ];
    self.currentPlayer = [NSString stringWithFormat:@"%@",[self.tableDictionary objectForKey:@"TAJ_current_players"]];
    
    self.currentPlayers = [NSString stringWithFormat:@"%@ of %@",[self.tableDictionary objectForKey:TAJ_CURRENT_PLAYER],self.maxPlayer];
    self.gameSchedule = [ NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_GAME_SCHEDULE] ];
    self.gameStart = [ NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_GAME_START] ];
    self.maxBuyin = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_MAX_BUYIN] ];
    self.minBuyin = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_MIN_BUYIN] ];
    self.minPlayer = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_MIN_PLAYER] ];
    self.scheduleName = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_SCHEDULE_NAME] ];
    self.tableCost = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_TABLE_COST] ];
    self.tableId = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_TABLE_ID] ];
    self.tableType = [self getProperNameForTableType:[NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_TABLE_TYPE]]];
    self.totalPlayer = [NSString stringWithFormat:@"%@" ,[self.tableDictionary objectForKey:TAJ_TOTAL_PLAYER] ];
    
    if([self isContainsKey:self.tableDictionary key:TAJ_FAVOURITE]) {
        self.favorite = [[self.tableDictionary objectForKey:TAJ_FAVOURITE] boolValue];
    }
    else {
        //NSLog(@"CHECK EMPTY KEY");
        self.favorite = 0;
    }
    
    return self;
}

-(NSString *)getProperNameForTableType:(NSString*)type
{
    NSString *properType = Nil;
    if ([type compare:TABLE_TYPE_101_POOL] == NSOrderedSame)
    {
        properType = TABLE_TYPE_101_POOL_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_201_POOL] == NSOrderedSame)
    {
        properType = TABLE_TYPE_201_POOL_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_BEST_OF_2] == NSOrderedSame)
    {
        properType = TABLE_TYPE_BEST_OF_2_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_BEST_OF_3] == NSOrderedSame)
    {
        properType = TABLE_TYPE_BEST_OF_3_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_BEST_OF_6] == NSOrderedSame)
    {
        properType = TABLE_TYPE_BEST_OF_6_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_PR_JOKER] == NSOrderedSame)
    {
        properType = TABLE_TYPE_PR_JOKER_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_PR_NO_JOKER] == NSOrderedSame)
    {
        properType = TABLE_TYPE_PR_NO_JOKER_DISPLAY_STRING;
    }
    return properType;
}

- (BOOL)isContainsKey:(NSDictionary *)player key:(NSString *)key
{
    BOOL retVal = 0;
    NSArray *allKeys = [player allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

@end
