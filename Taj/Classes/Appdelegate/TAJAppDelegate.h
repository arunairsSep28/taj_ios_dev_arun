/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAppDelegate.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified  by Raghavendra Kamat on 03/04/14
 Created by Raghavendra Kamat on 07/01/14.
 **/

#import <UIKit/UIKit.h>

#import "TAJLoginHomeViewController.h"
#import "TAJPreLobbyViewController.h"
#import "TAJLoginViewController.h"
#import "TAJWebPageViewController.h"
#import <UserNotifications/UserNotifications.h>
#import "LocationHandler.h"
#import <CoreData/CoreData.h>
#import <FirebaseMessaging/FirebaseMessaging.h>

//Author : RK
@class TAJSplashViewController;
@class TAJMainScreenViewController;
@class TAJAccountRegistrationViewController;

@interface TAJAppDelegate : UIResponder <UIApplicationDelegate, UNUserNotificationCenterDelegate,FIRMessagingDelegate>

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) TAJHomePageViewController *homeViewController;
@property (strong, nonatomic) TAJLoginHomeViewController *loginHomeViewController;
@property (strong, nonatomic) TAJSplashViewController *splashViewController;
@property (strong, nonatomic) UINavigationController *navigationController;
@property (strong, nonatomic) TAJMainScreenViewController *mainScreenController;
@property (strong, nonatomic) TAJPreLobbyViewController *preLobbyController;
@property (strong, nonatomic) TAJAccountRegistrationViewController *registrationController;
@property (strong, nonatomic) TAJLoginViewController *loginController;
@property (strong, nonatomic) TAJWebPageViewController *webPageViewController;
@property (strong, nonatomic) UIView *topView;
@property (nonatomic) double  currentTime;
@property (nonatomic) BOOL    isAppInBackground;

@property (strong, nonatomic) id completionHandler;

//ratheesh
@property (assign, nonatomic) NSInteger gameTableIndex;
@property (assign, nonatomic) NSInteger tablesCount;

@property (assign, nonatomic) BOOL isManualLogin;
@property (strong, nonatomic) LocationHandler *locHandler;
@property (strong, nonatomic) NSString *gameType;
@property (strong, nonatomic) NSString *tableType;
@property (assign, nonatomic) BOOL isMiddleJoin;
@property (assign, nonatomic) BOOL isMiddleJoinDeviceTurn;

@property (strong, nonatomic) NSString *dynamicIP;


@property (strong, nonatomic) NSString *jokerNumber;
@property (strong, nonatomic) NSString *brandName;

//ratheesh

+ (void) setScreenSizeForRes:(UIView *)screenView;
+ (void) setScreenSize : (UIView*) screenView;//ReDIM Changes
+ (void) setScreenSize : (UIView*) screenView fromView : (UIView*) view;//ReDIM Changes
+ (void) setPScreenSize : (UIView*) screenView fromView : (UIView*) view;//ReDIM Changes

//Loading Splash View
- (void) loadSplashView;
- (void) loadMainWindow;
- (void) loadMainWindowWithPlayNow;
- (void) loadMainScreenWindow;
- (void) loadWebPage:(WebPageViewOption)option;
- (void) loadPrelobbyScreen;
- (void) loadRegistrationScreen;
- (void) loadLoginScreenWindow;
- (void)loadPrelobbyScreenAfterRegistrationSuccessful;
- (void)saveResultForKey:(BOOL) result;
- (BOOL)retrieveResultForKey;


//Core Data
@property (readonly, strong) NSPersistentContainer *persistentContainer;

- (void)saveContext;

@end
