/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAppDelegate.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 07/01/14.
 **/

#import "TAJAppDelegate.h"
#import "Firebase.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>


#import "TAJNavigationBar.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJProgressAlertView.h"
#import "TAJSplashViewController.h"
#import "TAJMainScreenViewController.h"
#import "TAJAccountRegistrationViewController.h"
#import "TAJCustomNavigationViewController.h"
#import "TAJGameEngine.h"
#import "TAJSwitchLobbyViewController.h"
#import <WebEngage/WebEngage.h>
#import <sys/utsname.h>

//#import <WebEngageBannerPush/WEXPushNotificationService.h>

#import <ZDCChat/ZDCChat.h>
@import FirebaseMessaging;

NSDate* DateFromString(NSString* dateString)
{
    if([dateString length] == 0 || dateString == nil)
        return nil ;
    
    NSDate *outDate ;
    
    NSRange yearRange = NSMakeRange(0,4) ;
    NSRange monthRange = NSMakeRange(5,2) ;
    NSRange dateRange = NSMakeRange(8,2) ;
    NSRange hourRange = NSMakeRange(12,2) ;
    NSRange minRange =  NSMakeRange(15,2) ;
    NSRange secondRange = NSMakeRange(18,2) ;
    
    int year = [[dateString  substringWithRange:yearRange] intValue];
    int month = [[dateString  substringWithRange:monthRange] intValue];
    int date = [[dateString  substringWithRange:dateRange] intValue];
    int hours = [[dateString  substringWithRange:hourRange] intValue];
    int mins = [[dateString  substringWithRange:minRange] intValue];
    int secs = [[dateString  substringWithRange:secondRange] intValue];
    
    NSDateComponents *components = [[NSDateComponents alloc]init];
    [components setDay:date];
    [components setYear:year];
    [components setMonth:month];
    [components setHour:hours];
    [components setMinute:mins];
    [components setSecond:secs];
    
    outDate = [[NSCalendar currentCalendar] dateFromComponents:components];
    
    return outDate ;
}

bool IsBuildExpired(NSString *expiryDateString)
{
    BOOL status = FALSE;
    NSDate *expiryDate = DateFromString(expiryDateString);
    NSDate *today = [NSDate date];
    if([today compare:expiryDate] == NSOrderedDescending)
    {
        status = TRUE;
    }
    
    return status;
    
}

@interface TAJAppDelegate()

@property (nonatomic, assign)BOOL presentInLandscape;
@property (assign, nonatomic) BOOL isAppInstalled;

@end

static NSString* const hasRunAppOnceKey = @"hasRunAppOnceKey";

@implementation TAJAppDelegate

@synthesize gameTableIndex;
@synthesize isManualLogin;
@synthesize gameType;
@synthesize isMiddleJoin, isMiddleJoinDeviceTurn;
@synthesize jokerNumber;
@synthesize brandName;
@synthesize tablesCount;
@synthesize tableType;
@synthesize dynamicIP;

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    [self saveResultForKey:NO];
    self.presentInLandscape = NO;
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Forcefully set game table index to 1-> to load first game tab
    if (tablesCount == 2) {
        self.gameTableIndex = 0;
    }
    else {
        self.gameTableIndex = 1;
    }
    NSLog(@"APP DELEGATE TABLES COUNT : %ld",(long)tablesCount);
    
    self.isManualLogin = NO;
    
    // register notification
    [self registerForRemoteNotifications];
    
    //For reducing back light
    [[UIApplication sharedApplication] setIdleTimerDisabled:YES];
    
#if INCLUDE_SPLASH
    
    [self loadSplashView];
    
#else
    [self loadMainScreenWindow];
#endif
    [self.window makeKeyAndVisible];
    
    [[WebEngage sharedInstance] application:application
              didFinishLaunchingWithOptions:launchOptions];
    
    [self styleApp];
    
    [ZDCChat initializeWithAccountKey:ZDCHAT_LIVE_KEY];
    
    [ZDCLog enable:YES];
    [ZDCLog setLogLevel:ZDCLogLevelInfo];
    
    NSSetUncaughtExceptionHandler(&uncaughtExceptionHandler);
    
    // push notifications
    [self requestPermissions];
    
    // make key window
    [self.window makeKeyAndVisible];
    
    // notification
    [ZDCChat didReceiveRemoteNotification:launchOptions];
    
    self.brandName = [self deviceName];
    
    self.isAppInstalled = [[NSUserDefaults standardUserDefaults] boolForKey:hasRunAppOnceKey];
    
    if (self.isAppInstalled == NO) {
        //service call for installation check
        [self appDownloadEntry];
    }
    
    [FIRApp configure];
    [Fabric with:@[[Crashlytics class]]];
    [FIRMessaging messaging].delegate = self;
    
    return YES;
}

- (void)appDownloadEntry {
    
    NSString * deviceType;
    
    if ([TAJUtilities isIPhone])
    {
        deviceType = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceType = @"Tablet";//@"IPAD";
    }
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSDictionary *params = @{@"device_brand": self.brandName,
                             @"device_id": [UIDevice currentDevice].identifierForVendor,
                             @"version" : buildNumber,
                             @"device_type" : deviceType,
                             @"client_type" : @"iOS"
                             };
#if DEBUG
    NSLog(@"APP_DOWNLOAD_ENTRY PARAMS: %@", params);
#endif
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,APP_DOWNLOAD_ENTRY];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:NO headerValue:nil withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    NSLog(@"APP_DOWNLOAD_ENTRY RESPONSE: %@", jsondata);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
                        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:hasRunAppOnceKey];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                    }
                    else {
                        
                        NSLog(@"APP_DOWNLOAD_ENTRY Error :%@",[jsondata valueForKey:@"message"]);
                    }
                });
            }
            else
            {
                
            }
        });
    }];
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [[TAJGameEngine sharedGameEngine] destroySocketOnIncommingCall];
    self.currentTime = CACurrentMediaTime();
    [self saveResultForKey:YES];
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    //self.isAppInBackground = NO;
    [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    
}

- (void)saveResultForKey:(BOOL) result
{
    NSNumber *resultCheck = [NSNumber numberWithBool:result];
    if( resultCheck != nil)
    {
        [[NSUserDefaults standardUserDefaults] setBool:result forKey:@"background"];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(BOOL)retrieveResultForKey
{
    NSNumber *resultCheck;
    BOOL result;
    result = [[NSUserDefaults standardUserDefaults] boolForKey:@"background"];
    resultCheck = [NSNumber numberWithBool:result];
    
    return  result;
}

- (void) loadSplashView
{
    self.presentInLandscape = NO;//Assign no if the view should be displayed in portrait
    
    self.splashViewController = [[TAJSplashViewController alloc] init];
    
    self.window.rootViewController = nil;
    self.navigationController = [[TAJCustomNavigationViewController alloc] initWithRootViewController:self.splashViewController];
    self.window.rootViewController = self.navigationController;
}

- (void)loadMainWindow
{
    self.presentInLandscape = NO;//Assign no if the view should be displayed in portrait
    if ([TAJUtilities isIPhone])
    {
        self.loginHomeViewController = [[TAJLoginHomeViewController alloc] init:eLoginOption];
    }
    else
    {
        self.loginHomeViewController = [[TAJLoginHomeViewController alloc] init:eLoginOption];
    }
    self.navigationController = [[TAJCustomNavigationViewController alloc] initWithRootViewController:self.loginHomeViewController];
    
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    if([TAJUtilities isIPhone])
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    else
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.x = newFrame.origin.x - newFrame.size.width;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
        
    }
    
}

- (void)loadMainWindowWithPlayNow
{
    self.presentInLandscape = NO;//Assign no if the view should be displayed in portrait
    if ([TAJUtilities isIPhone])
    {
        self.loginHomeViewController = [[TAJLoginHomeViewController alloc] init:ePlayNowOption];
    }
    else
    {
        self.loginHomeViewController = [[TAJLoginHomeViewController alloc] init:ePlayNowOption];
    }
    self.navigationController = [[TAJCustomNavigationViewController alloc] initWithRootViewController:self.loginHomeViewController];
    
    self.window.rootViewController = self.navigationController;
    [self.window makeKeyAndVisible];
    if([TAJUtilities isIPhone])
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    else
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.x = newFrame.origin.x - newFrame.size.width;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    
}

- (void)loadMainScreenWindow
{
    //NSLog(@"CUSTOM NAVIGATION");
    
    self.presentInLandscape = NO;//Assign YES if the view should be displayed in landscape
    self.mainScreenController = [[TAJMainScreenViewController alloc] init];
    self.window.rootViewController = nil;
    UINavigationController *navigationScreen = [[TAJCustomNavigationViewController alloc] initWithRootViewController:self.mainScreenController];
    self.window.rootViewController = navigationScreen;
    
}

- (void)loadWebPage:(WebPageViewOption)option
{
    self.presentInLandscape=NO;
    
    self.webPageViewController = [[TAJWebPageViewController alloc]  initWithNibName:@"TAJWebPageViewController" bundle:nil webPageOption:option];
    
    self.window.rootViewController = nil;
    self.window.rootViewController = self.webPageViewController;
}

- (void)loadPrelobbyScreenAfterRegistrationSuccessful
{
    self.presentInLandscape = NO;//Assign no if the view should be displayed in portrait
    {
        self.preLobbyController = [[TAJPreLobbyViewController alloc] init];
    }
    self.window.rootViewController = nil;
    self.window.rootViewController = self.preLobbyController;
    [self.preLobbyController loadRegistrationSuccessMessageInfoPopUP];
    if([TAJUtilities isIPhone])
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    else
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    
}

- (void)loadPrelobbyScreen
{
    self.presentInLandscape = NO;//Assign no if the view should be displayed in portrait
    
    self.preLobbyController = [[TAJPreLobbyViewController alloc] init];
    self.window.rootViewController = nil;
    self.window.rootViewController = self.preLobbyController;
    if([TAJUtilities isIPhone])
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    else
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
        
    }
}

- (void) loadRegistrationScreen
{
    self.presentInLandscape = NO;//Assign no if the view should be displayed in portrait
    
    self.registrationController = [[TAJAccountRegistrationViewController alloc] init];
    self.window.rootViewController = nil;
    self.navigationController = [[TAJCustomNavigationViewController alloc] initWithRootViewController:self.registrationController];
    self.window.rootViewController = self.navigationController;
    if([TAJUtilities isIPhone])
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    else
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.x = newFrame.origin.x - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
}

- (void)loadLoginScreenWindow
{
    self.presentInLandscape = YES;//Assign YES if the view should be displayed in landscape
    if ([TAJUtilities isIPhone])
    {
        //by ratheesh
        APP_DELEGATE.gameTableIndex = 1;
        //end ratheesh
        self.loginController = [[TAJLoginViewController alloc]initWithNibName:@"TAJLoginViewController" bundle:nil];
    }
    else
    {
        self.loginController = [[TAJLoginViewController alloc]init];
    }
    
    self.window.rootViewController = nil;
    self.navigationController = [[TAJCustomNavigationViewController alloc] initWithRootViewController:self.loginController];
    self.window.rootViewController = self.navigationController;
    if([TAJUtilities isIPhone])
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.y = newFrame.origin.y - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    else
    {
        CGRect originalFrame = self.window.frame;
        CGRect newFrame = self.window.frame;
        newFrame.origin.x = newFrame.origin.x - newFrame.size.height;
        self.window.frame = newFrame;
        [UIView animateWithDuration:0.8f
                              delay:0.0f
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             self.window.frame = originalFrame;
                         }
                         completion:nil];
    }
    
}

+ (void) setScreenSizeForRes:(UIView *)screenView {//ReDIM Changes
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    CGRect frame = screenView.frame;
    frame.size.width = screenSize.width;
    frame.size.height = screenSize.height;
    screenView.frame = frame;
}

+ (void) setScreenSize:(UIView *)screenView {//ReDIM Changes
    // if([TAJUtilities isIPhone]) {
    [TAJAppDelegate setScreenSizeForRes: screenView];
    //}
}

+ (void) setScreenSize : (UIView*) screenView fromView : (UIView*) view {
    //ReDIM Changes
    CGRect frame = screenView.frame;
    frame.size.width = view.frame.size.width;
    frame.size.height = view.frame.size.height;
    screenView.frame = frame;
    
}

+ (void) setPScreenSize : (UIView*) screenView fromView : (UIView*) view
{
    //ReDIM Changes
    CGRect frame = screenView.frame;
    frame.size.width = view.frame.size.height;
    frame.size.height = view.frame.size.width;
    screenView.frame = frame;
}

#pragma mark -
- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      : @"Simulator",
                              @"x86_64"    : @"Simulator",
                              @"iPod1,1"   : @"iPod Touch",        // (Original)
                              @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" : @"iPhone",            // (Original)
                              @"iPhone1,2" : @"iPhone",            // (3G)
                              @"iPhone2,1" : @"iPhone",            // (3GS)
                              @"iPad1,1"   : @"iPad",              // (Original)
                              @"iPad2,1"   : @"iPad 2",            //
                              @"iPad3,1"   : @"iPad",              // (3rd Generation)
                              @"iPhone3,1" : @"iPhone 4",          // (GSM)
                              @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" : @"iPhone 4S",         //
                              @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   : @"iPad",              // (4th Generation)
                              @"iPad2,5"   : @"iPad Mini",         // (Original)
                              @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" : @"iPhone 6 Plus",     //
                              @"iPhone7,2" : @"iPhone 6",          //
                              @"iPhone8,1" : @"iPhone 6S",         //
                              @"iPhone8,2" : @"iPhone 6S Plus",    //
                              @"iPhone8,4" : @"iPhone SE",         //
                              @"iPhone9,1" : @"iPhone 7",          //
                              @"iPhone9,3" : @"iPhone 7",          //
                              @"iPhone9,2" : @"iPhone 7 Plus",     //
                              @"iPhone9,4" : @"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",          // CDMA
                              @"iPhone10,4": @"iPhone 8",          // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                              @"iPhone10,3": @"iPhone X",          // CDMA
                              @"iPhone10,6": @"iPhone X",          // GSM
                              @"iPhone11,2": @"iPhone XS",         //
                              @"iPhone11,4": @"iPhone XS Max",     //
                              @"iPhone11,6": @"iPhone XS Max",     // China
                              @"iPhone11,8": @"iPhone XR",         //
                              
                              @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    return deviceName;
}



#pragma mark - ZDChat UI Configuration
- (void) styleApp
{
    // status bar
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
    
    // nav bar
    NSDictionary *navbarAttributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                      [UIColor blackColor] ,NSForegroundColorAttributeName, nil];
    [[UINavigationBar appearance] setTintColor:[UIColor blackColor]];
    [[UINavigationBar appearance] setTitleTextAttributes:navbarAttributes];
    [[UINavigationBar appearance] setBarTintColor:[UIColor whiteColor]];
    
    if ([self isVersionOrNewer:@"8.0"]) {
        
        // For translucent nav bars set YES
        [[UINavigationBar appearance] setTranslucent:YES];
    }
}

- (void) requestPermissions
{
    UIUserNotificationSettings *s = [UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil];
    
    [[UIApplication sharedApplication] registerUserNotificationSettings:s];
}

- (void) application:(UIApplication*)application
didRegisterUserNotificationSettings:(UIUserNotificationSettings*)notificationSettings
{
    [[UIApplication sharedApplication] registerForRemoteNotifications];
}

- (void) application:(UIApplication*)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)tokenData
{
    NSString *strDeviceToken = [[[[tokenData description]
                                  stringByReplacingOccurrencesOfString: @"<" withString: @""]
                                 stringByReplacingOccurrencesOfString: @">" withString: @""]
                                stringByReplacingOccurrencesOfString: @" " withString: @""];
#if DEBUG
    NSLog(@"Token Data-----> %@\n", tokenData);
    NSLog(@"Device Token-----> %@\n", strDeviceToken);
#endif
    
    [[NSUserDefaults standardUserDefaults] setValue:strDeviceToken forKey:@"device_token"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    [ZDCLog i:@"App obtained push token"];
    [ZDCChat setPushToken:tokenData];
    
    APP_DELEGATE.locHandler = [LocationHandler SharedLocationHandler];
}

- (void) application:(UIApplication*)app didFailToRegisterForRemoteNotificationsWithError:(NSError*)err
{
    APP_DELEGATE.locHandler = [LocationHandler SharedLocationHandler];
    [ZDCLog i:@"App failed to obtain push token"];
}


- (void) application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo
{
    [ZDCChat didReceiveRemoteNotification:userInfo];
}

void uncaughtExceptionHandler(NSException *exception) {
    [ZDCLog e:@"CRASH: %@", exception];
    [ZDCLog e:@"Stack Trace: %@", [exception callStackSymbols]];
}

- (BOOL) isVersionOrNewer:(NSString*)majorVersionNumber {
    return [[[UIDevice currentDevice] systemVersion] compare:majorVersionNumber options:NSNumericSearch] != NSOrderedAscending;
}

#pragma mark - User Notification Delegates

- (void)registerForRemoteNotifications
{
    if (SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(@"10.0")) {
        if (@available(iOS 10.0, *)) {
            UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
            center.delegate = self;
            [center requestAuthorizationWithOptions:(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge) completionHandler:^(BOOL granted, NSError * _Nullable error) {
                if (!error) {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [[UIApplication sharedApplication] registerForRemoteNotifications];
                    });
                }
            }];
        } else {
            // Fallback on earlier versions
            // Code for old versions
            [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
            [[UIApplication sharedApplication] registerForRemoteNotifications];
        }
    }
    else {
        // Code for old versions
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
}

// called when a notification is delivered when app is in foreground.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center willPresentNotification:(UNNotification *)notification withCompletionHandler:(void (^)(UNNotificationPresentationOptions options))completionHandler
API_AVAILABLE(ios(10.0)) API_AVAILABLE(ios(10.0)){
#if DEBUG
    NSLog(@"User Info : %@",notification.request.content.userInfo);
#endif
    completionHandler(UNAuthorizationOptionSound | UNAuthorizationOptionAlert | UNAuthorizationOptionBadge);
}

// called to let your app know which action was selected by the user for a given notification.
- (void)userNotificationCenter:(UNUserNotificationCenter *)center didReceiveNotificationResponse:(UNNotificationResponse *)response withCompletionHandler:(void(^)(void))completionHandler
API_AVAILABLE(ios(10.0)) {
#if DEBUG
    NSLog(@"User Info : %@",response.notification.request.content.userInfo);
#endif
    completionHandler();
}

- (void)application:(UIApplication *)application handleActionWithIdentifier:(NSString *)identifier
forRemoteNotification:(NSDictionary *)notification completionHandler:(void(^)(void))completionHandler
{
#if DEBUG
    NSLog(@"Received push notification: %@, identifier: %@", notification, identifier); // iOS 8
#endif
    completionHandler();
}

- (void)WEGHandleDeeplink:(NSString *)deeplink userData:(NSDictionary *)data
{
#if DEBUG
    NSLog(@"DEEPLINKING DATA : %@",data);
#endif
}


#pragma mark - Core Data stack

@synthesize persistentContainer = _persistentContainer;

- (NSPersistentContainer *)persistentContainer {
    // The persistent container for the application. This implementation creates and returns a container, having loaded the store for the application to it.
    @synchronized (self) {
        if (_persistentContainer == nil) {
            _persistentContainer = [[NSPersistentContainer alloc] initWithName:@"TajRummy"];
            [_persistentContainer loadPersistentStoresWithCompletionHandler:^(NSPersistentStoreDescription *storeDescription, NSError *error) {
                if (error != nil) {
                    // Replace this implementation with code to handle the error appropriately.
                    // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                    
                    /*
                     Typical reasons for an error here include:
                     * The parent directory does not exist, cannot be created, or disallows writing.
                     * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                     * The device is out of space.
                     * The store could not be migrated to the current model version.
                     Check the error message to determine what the actual problem was.
                     */
                    NSLog(@"Unresolved error %@, %@", error, error.userInfo);
                    abort();
                }
            }];
        }
    }
    
    return _persistentContainer;
}

#pragma mark - Core Data Saving support

- (void)saveContext {
    NSManagedObjectContext *context = self.persistentContainer.viewContext;
    NSError *error = nil;
    if ([context hasChanges] && ![context save:&error]) {
        // Replace this implementation with code to handle the error appropriately.
        // abort() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
        NSLog(@"Unresolved error %@, %@", error, error.userInfo);
        abort();
    }
}


//#pragma mark - Custom Firebase Methods
//
//- (void)tokenRefreshCallback:(NSNotification *)notification
//{
//    NSString *refreshToken =  [[FIRInstanceID instanceID] token];
//    [Session sharedInstance].fcmToken = refreshToken;
//    [[Session sharedInstance] saveUserDataFromDefaults];
//    
//#if DEBUG
//    NSLog(@"InstanceId Token: %@", refreshToken);
//#endif
//    // Connect to FCM since connection may have failed when attempting before having a token.
//    [self connectToFireBase];
//    
//}
//
//- (void)connectToFireBase
//{
//    [[FIRMessaging messaging] shouldEstablishDirectChannel];
//}

@end
