/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJForgotPasswordViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Yogish Poojary on 18/06/14.
 Created by  Raghavendra Kamat on 06/05/14.
 **/

#import "TAJForgotPasswordViewController.h"
#import "TAJUtilities.h"
#import "TAJInfoPopupViewController.h"
#import "TAJConstants.h"
#import "TAJAppDelegate.h"

@interface TAJForgotPasswordViewController ()

@property (weak, nonatomic) IBOutlet UILabel *forgotPasswordInfoLabel;
- (IBAction)backButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UILabel *emailIdLabel;
@property (strong, nonatomic) UITapGestureRecognizer *tapGesture;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@property (weak, nonatomic) IBOutlet UILabel *lblStage;
@end

@implementation TAJForgotPasswordViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        self.infoPopupViewController = Nil;
        
    }
    return self;
}

- (void)viewDidLoad
{
    //Enter you E-mail ID / Username - removed username placeholder by ratheesh
    [super viewDidLoad];
    
    self.lblStage.layer.cornerRadius = 4;
    self.lblStage.layer.borderColor = [UIColor grayColor].CGColor;

}


- (void) viewWillAppear:(BOOL)animated
{
   self.emailTextField.text = @"";
}
-(void) keyboardDisappeared
{
    [self.view removeGestureRecognizer:self.tapGesture];
}

-(void) keyboardAppeared
{
    self.tapGesture =  [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(removeFistResponder)];
    self.tapGesture.numberOfTapsRequired = 1;
    self.tapGesture.numberOfTouchesRequired = 1;
    [self.view addGestureRecognizer:self.tapGesture];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)removeFistResponder
{
    [self.emailTextField resignFirstResponder];
}

#pragma mark - Helpers

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    
    if ([self.emailTextField.text length] == 0) {
        [self.view makeToast:@"Please enter required data"];
        return YES;
    }
    return NO;
}
-(void)getForgotPassword {
    
    [self.view makeToastActivity:CSToastPositionCenter];

    NSString * URL = [NSString stringWithFormat:@"%@%@%@?email=%@",BASE_URL,API_BASE_URL,FORGOTPASSWORD,self.emailTextField.text];
    //NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:nil URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
#if DEBUG
            NSLog(@"FORGOTPASSWORD Response :%@",responseData);
#endif
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
            VerificationViewController * viewController = [[VerificationViewController alloc] initWithData:self.emailTextField.text];
            [self.navigationController pushViewController:viewController animated:YES];
            }
            else {
                [self.view makeToast:[responseData objectForKey:@"message"]];
            }
            //NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
        });

    } errorBlock:^(NSString *errorString) {
        NSLog(@"FORGOTPASSWORD ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

- (void)forgotPasswordAuthentication
{
    NSString *urlAsString;
    
#if USING_LIVE_SERVER
   // urlAsString = FORGOT_PASSWORD_URL_LIVE;
    urlAsString = [NSString stringWithFormat:@"%@%@",BASE_URL,FORGOT_PASSWORD_URL_LIVE];
#else
    urlAsString = FORGOT_PASSWORD_URL;
    
#endif
    
    if ( ([_emailTextField.text length] > 0) && (NSOrderedSame != [_emailTextField.text compare:@" "]) )
    {
        if ([self validateEmailWithString:_emailTextField.text])
        {
            NSMutableString *email = [_emailTextField.text mutableCopy];
            [email encode];
            //NSLog(@"FORGOT URL %@",urlAsString);
            urlAsString = [NSString stringWithFormat:urlAsString,email];
            NSURL *url = [NSURL URLWithString:urlAsString];
            NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
            [urlRequest setTimeoutInterval:120.0f];
            [urlRequest setHTTPMethod:@"GET"];
            NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:YES];
            if(connection)
            {
                DLog(@"Connection Established");
            }
            
        }
        else
        {
            [self hideKeyboard];
            [self.view makeToast:@"Invalid email address."];
            
         }
        
    }
    else
    {
        [self hideKeyboard];
        //Handle the empty string case
        //self.errorTextLabel.text = @"This field is required. Invalid email address.";
        [self.view makeToast:@"This field is required. Invalid email address."];
        //[self.emailTextField becomeFirstResponder];
    }
    self.submitButton.enabled = YES;

}

- (IBAction)submitPressed:(UIButton *)sender
{
    [self hideKeyboard];
    BOOL error = [self validateInput];
    if (!error) {
        [self getForgotPassword];
    }
    //[self forgotPasswordAuthentication];
}

- (IBAction) returnLoginPressed:(UIButton *)sender {
    [self.navigationController popViewControllerAnimated: YES];;
}

- (IBAction) createPressed:(UIButton *)sender {
    [(TAJAppDelegate*)TAJ_APP_DELEGATE loadRegistrationScreen];
}

# pragma HelperMethods

- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}


-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL)shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    
}

//PopupType will be passed
-(void) yesButtonPressedForInfoPopup:(id)object
{
    
}

-(void) noButtonPressedForInfoPopup:(id)object
{
    
}

-(void) okButtonPressedForInfoPopup:(id)object
{
    DLog(@"okButtonPressedForInfoPopup");
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
    self.emailTextField.text = @"";
}

-(void) closeButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
     self.emailTextField.text = @"";
}

- (void)chooseInfopopForDifferentDevices:(NSString *)message
{
    self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:message];
    [self.infoPopupViewController show];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    [self.view makeToast:[NSString stringWithFormat:@"%@",[error description]]];
}


#pragma mark - Touches Delegates -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [self.emailTextField resignFirstResponder];
}

#pragma mark - UITextfield Delegates -

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    [textField resignFirstResponder];
    if (textField == self.emailTextField)
    {
        
        [self forgotPasswordAuthentication];
      }
    return YES;
}

- (IBAction)backButtonPressed:(id)sender
{
    [self.view endEditing:YES];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
