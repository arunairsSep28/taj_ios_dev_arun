//
//  VerificationViewController.h
//  36R
//
//  Created by Fazululla Kalluru on 27/06/18.
//  Copyright © 2018 safacs. All rights reserved.
//


@interface VerificationViewController : UIViewController

- (instancetype)initWithData:(NSString *)data;

@end
