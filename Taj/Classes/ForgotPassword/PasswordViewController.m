//
//  PasswordViewController.m
//  TajRummy
//
//  Created by Grid Logic on 17/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "PasswordViewController.h"

@interface PasswordViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lblStage;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldPassword;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldConfirmPassword;

@property (weak, nonatomic) IBOutlet UIImageView *imgViewPasswordHide;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewConfirmPasswordHide;

@property BOOL isPasswordToggle;
@property BOOL isConfirmPasswordToggle;

@property (strong, nonatomic) NSString *token;

@end

@implementation PasswordViewController

- (instancetype)initWithToken:(NSString *)token {
    self = [super init];
    if (self) {
        _token = token;
    }
    return self;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    [self configureView];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}


#pragma mark - Helpers

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldPassword.text length] == 0) {
        [self.view makeToast:@"New password can not be empty"];
        return YES;
    }
    else if ([self.txtFieldConfirmPassword.text length] == 0) {
        [self.view makeToast:@"Confirm Password can not be empty"];
        return YES;
    }
    else if ([self.txtFieldPassword.text length] < PasswordMinLength) {
        
        [self.view makeToast:@"Password minimum should be 5 characters"];
        return YES;
    }
    else if ([self.txtFieldConfirmPassword.text length] < PasswordMinLength) {
        [self.view makeToast:@"Confirm Password minimum should be 5 characters"];
        return YES;
    }
    
    else if(![self.txtFieldPassword.text isEqualToString:self.txtFieldConfirmPassword.text]) {
        [self.view makeToast:@"Password not matching"];
        return YES;
    }
    
//    else if([self.txtFieldPassword.text isEqualToString:self.txtFieldConfirmPassword.text]) {
//        [self.view makeToast:@"New password and Current password should not be same"];
//        return YES;
//    }
    return NO;
}

-(void)updatePassword
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"password" : self.txtFieldPassword.text,
                             @"password1" : self.txtFieldConfirmPassword.text,
                             @"pwd_token" : self.token
                             };
#if DEBUG
    NSLog(@"CHANGE_PASSWORD PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"header: %@", header);
#endif
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,CHANGE_PASSWORD];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"CHANGE_PASSWORD RESPONSE: %@", jsondata);
#endif
                        SuccessViewController * viewController = [[SuccessViewController alloc] init];
                        [self.navigationController pushViewController:viewController animated:YES];
                        
                    }
                    else {
                        [self.view hideToastActivity];
                        NSLog(@"CHANGE_PASSWORD Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
                [self.view hideToastActivity];
            }
        });
    }];
    
}

- (void)configureView
{
    // initialization
    
    self.lblStage.layer.cornerRadius = 4;
    self.lblStage.layer.borderColor = [UIColor grayColor].CGColor;
}

#pragma mark - Actions

- (IBAction)backClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)togglePasswordClicked:(UIButton *)sender {
    
    if(self.isPasswordToggle == true) {
        self.txtFieldPassword.secureTextEntry = YES;
        self.imgViewPasswordHide.image = [UIImage imageNamed:@"eye_cross"];
        self.isPasswordToggle = NO;
    } else {
        self.txtFieldPassword.secureTextEntry = NO;
        self.imgViewPasswordHide.image = [UIImage imageNamed:@"eye"];
        self.isPasswordToggle = YES;
    }
}


- (IBAction)toggleConfirmPasswordClicked:(UIButton *)sender {
    
    if(self.isPasswordToggle == true) {
        self.txtFieldConfirmPassword.secureTextEntry = YES;
        self.imgViewConfirmPasswordHide.image = [UIImage imageNamed:@"eye_cross"];
        self.isPasswordToggle = NO;
    } else {
        self.txtFieldConfirmPassword.secureTextEntry = NO;
        self.imgViewConfirmPasswordHide.image = [UIImage imageNamed:@"eye"];
        self.isPasswordToggle = YES;
    }
}

- (IBAction)confirmClicked:(UIButton *)sender
{
    [self hideKeyboard];
    
//    SuccessViewController * viewController = [[SuccessViewController alloc] init];
//    [self.navigationController pushViewController:viewController animated:YES];
    
    BOOL error = [self validateInput];
    if (!error) {
        [self updatePassword];
    }
}

@end
