/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJForgotPasswordViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Yogish Poojary on 18/06/14.
 Created by  Raghavendra Kamat on 06/05/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJUITextField.h"
#import "TAJInfoPopupViewController.h"
#import "NSMutableString+EncodingExtensions.h"


@class TAJInfoPopupViewController;
@interface TAJForgotPasswordViewController : UIViewController<InfoPopupViewDelegate, NSURLConnectionDelegate>

@property (strong, nonatomic) IBOutlet UITextField *emailTextField;
@property (strong, nonatomic) TAJInfoPopupViewController *infoPopupViewController;
@property (strong, nonatomic) IBOutlet UIButton *submitButton;

- (IBAction)submitPressed:(UIButton *)sender;
- (IBAction)returnLoginPressed:(UIButton *)sender;
- (IBAction)createPressed:(UIButton *)sender;

@end
