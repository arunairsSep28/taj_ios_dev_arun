//
//  PasswordViewController.h
//  TajRummy
//
//  Created by Grid Logic on 17/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PasswordViewController : UIViewController

- (instancetype)initWithToken:(NSString *)token;

@end

NS_ASSUME_NONNULL_END
