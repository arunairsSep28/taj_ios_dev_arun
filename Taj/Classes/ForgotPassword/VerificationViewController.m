//
//  VerificationViewController.m
//  36R
//
//  Created by Fazululla Kalluru on 27/06/18.
//  Copyright © 2018 safacs. All rights reserved.
//

#import "VerificationViewController.h"

@interface VerificationViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBack;

@property (weak, nonatomic) IBOutlet UILabel *lblHeader;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldFirstDigit;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSecondDigit;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldThirdDigit;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldForthDigit;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldFifthDigit;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldSixthDigit;

@property (weak, nonatomic) IBOutlet UIView *viewFirstDigit;
@property (weak, nonatomic) IBOutlet UIView *viewSecondDigit;
@property (weak, nonatomic) IBOutlet UIView *viewThirdDigit;
@property (weak, nonatomic) IBOutlet UIView *viewForthDigit;
@property (weak, nonatomic) IBOutlet UIView *viewdFifthDigit;
@property (weak, nonatomic) IBOutlet UIView *viewdSixthDigit;

@property (weak, nonatomic) IBOutlet UILabel *lblStage;

@property (weak, nonatomic) IBOutlet UIButton *btnConfirm;
@property (strong, nonatomic) UIColor *filledColor;


@property (strong, nonatomic) NSString *data;
@end

@implementation VerificationViewController

#pragma mark - Life Cycle

- (instancetype)initWithData:(NSString *)data
{
    self = [super init];
    if (self) {
        _data = data;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self configureView];
    
}
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}


- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
}



- (void)configureView
{
    // initialization
    self.btnBack.hidden = NO;
    self.filledColor = [UIColor colorWithRed:72.0f/255.0f green:169.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    
    self.lblStage.layer.cornerRadius = 4;
    self.lblStage.layer.borderColor = [UIColor grayColor].CGColor;
}

#pragma mark - Helpers

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)validateInput
{
    if ([self.txtFieldFirstDigit.text length] == 0 || [self.txtFieldSecondDigit.text length] == 0 || [self.txtFieldThirdDigit.text length] == 0 || [self.txtFieldForthDigit.text length] == 0) {
        [self.view makeToast:@"Enter OTP"];
        return YES;
    }
    return NO;
}

-(void)resendOTP {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@?email=%@",BASE_URL,API_BASE_URL,FORGOTPASSWORD,self.data];
    //NSLog(@"HEADER : %@",header);
    [[Service sharedInstance] getRequestWithHeader:nil URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
#if DEBUG
            NSLog(@"FORGOTPASSWORD Response :%@",responseData);
#endif
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                [self.view makeToast:[responseData valueForKey:@"message"]];
            }
            else
            {
                [self.view makeToast:[responseData valueForKey:@"message"]];
            }
            //NSLog(@"DATA : %@",[responseData valueForKey:@"data"]);
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"FORGOTPASSWORD ERROR : %@",errorString);
        [self.view hideToastActivity];
    }];
}

-(void)verifyOTP
{
    [self.view makeToastActivity:CSToastPositionCenter];
    NSString *otp = [NSString stringWithFormat:@"%@%@%@%@%@%@", self.txtFieldFirstDigit.text, self.txtFieldSecondDigit.text, self.txtFieldThirdDigit.text, self.txtFieldForthDigit.text, self.txtFieldFifthDigit.text, self.txtFieldSixthDigit.text];
    
    NSDictionary *params = @{@"token" : otp,
                             @"mobile": self.data};
#if DEBUG
    NSLog(@"verifyOTP PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"header: %@", header);
#endif
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,FORGOTPASSWORD];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"verifyOTP RESPONSE: %@", jsondata);
#endif
                        PasswordViewController * viewController = [[PasswordViewController alloc] initWithToken:[jsondata objectForKey:@"token"]];
                        [self.navigationController pushViewController:viewController animated:YES];
                        
                    }
                    else {
                        [self.view hideToastActivity];
                        NSLog(@"verifyOTP Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                        
                    }
                });
            }
            else
            {
                [self.view makeToast:@"Error while fetching data."];
                [self.view hideToastActivity];
            }
        });
    }];
    
}

#pragma mark - Actions

- (IBAction)backClicked:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)resendClicked:(UIButton *)sender
{
    [self resendOTP];
}

- (IBAction)confirmClicked:(UIButton *)sender
{
    [self hideKeyboard];
    
    BOOL error = [self validateInput];
    if (!error) {
//        PasswordViewController * viewController = [[PasswordViewController alloc] init];
//        [self.navigationController pushViewController:viewController animated:YES];
        [self verifyOTP];
    }
    
}

#pragma mark - TextField

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    
    if ((textField.text.length < 1) && (string.length > 0)) {
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (! nextResponder) {
            [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder) {
            [self performSelector:@selector(updateUI) withObject:nil afterDelay:0.01];
            [nextResponder becomeFirstResponder];
        }
        
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length > 0)) {
        //FOR MAXIMUM 1 TEXT
        
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (!nextResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (nextResponder) {
            [nextResponder becomeFirstResponder];
        }
        [self updateUI];
        return NO;
    }
    else if ((textField.text.length >= 1) && (string.length == 0)) {
        // on deleteing value from Textfield
        
        NSInteger prevTag = textField.tag - 1;
        // Try to find prev responder
        UIResponder* prevResponder = [textField.superview viewWithTag:prevTag];
        if (! prevResponder){
            [textField resignFirstResponder];
        }
        textField.text = string;
        if (prevResponder)
            // Found next responder, so set it.
            [prevResponder becomeFirstResponder];
        
        [self updateUI];
        return NO;
    }
    return YES;
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Remove any white line/new line characters
    textField.text = [textField.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

- (void)updateUI
{
    if (([self.txtFieldFirstDigit.text length] == 0) ) {
       self.viewFirstDigit.backgroundColor = [UIColor clearColor];
    }
    if ([self.txtFieldSecondDigit.text length] == 0) {
        self.viewSecondDigit.backgroundColor = [UIColor clearColor];
    }
    if ([self.txtFieldThirdDigit.text length] == 0) {
        self.viewThirdDigit.backgroundColor = [UIColor clearColor];
    }
    if ([self.txtFieldForthDigit.text length] == 0) {
         self.viewForthDigit.backgroundColor = [UIColor clearColor];
    }
    if ([self.txtFieldFifthDigit.text length] == 0) {
        self.viewdFifthDigit.backgroundColor = [UIColor clearColor];
    }
    if ([self.txtFieldSixthDigit.text length] == 0) {
        self.viewdSixthDigit.backgroundColor = [UIColor clearColor];
    }
    
    
    if (([self.txtFieldFirstDigit.text length] > 0)  && ![self.txtFieldFirstDigit.text isEqualToString:@" "]) {
        self.viewFirstDigit.backgroundColor = self.filledColor;
        
    }
    if (([self.txtFieldSecondDigit.text length] > 0)  && ![self.txtFieldFirstDigit.text isEqualToString:@" "]) {
        self.viewSecondDigit.backgroundColor = self.filledColor;
        
    }
    if (([self.txtFieldThirdDigit.text length] > 0)  && ![self.txtFieldFirstDigit.text isEqualToString:@" "]) {
        self.viewThirdDigit.backgroundColor = self.filledColor;

    }
    if ([self.txtFieldForthDigit.text length] > 0 && ![self.txtFieldSecondDigit.text isEqualToString:@" "]) {
        self.viewForthDigit.backgroundColor = self.filledColor;

    }
    if ([self.txtFieldFifthDigit.text length] > 0 && ![self.txtFieldThirdDigit.text isEqualToString:@" "]) {
        self.viewdFifthDigit.backgroundColor = self.filledColor;
    }
    if ([self.txtFieldSixthDigit.text length] > 0 && ![self.txtFieldForthDigit.text isEqualToString:@" "]) {
        self.viewdSixthDigit.backgroundColor = self.filledColor;
    }
}

@end
