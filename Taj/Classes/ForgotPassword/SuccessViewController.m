//
//  SuccessViewController.m
//  TajRummy
//
//  Created by Grid Logic on 17/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "SuccessViewController.h"

@interface SuccessViewController ()

@end

@implementation SuccessViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self performSelector:@selector(navigateToLogin) withObject:self afterDelay:2.0 ];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}


#pragma mark - Actions

- (IBAction)backClicked:(UIButton *)sender
{
   
}

-(void)navigateToLogin {
     [(TAJAppDelegate*)TAJ_APP_DELEGATE loadLoginScreenWindow];
}

@end
