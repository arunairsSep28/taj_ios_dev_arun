/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJGameTableController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 13/06/14.
 Created by Sujit Yadawad on 03/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJInfoPopupViewController.h"
#import "TAJPlayTableView.h"
#import "TAJUserModel.h"
#import "TAJChairView.h"
#import "TAJChatViewController.h"
#import "Reachability.h"


@protocol TAJGameTableControllerDelegate;

@interface TAJGameTableController : TAJBaseViewController<UIAlertViewDelegate,InfoPopupViewDelegate>


@property(nonatomic, strong) Reachability *reachability;

@property (nonatomic, strong) NSMutableDictionary      *playTableDictionary;
@property (nonatomic,strong) NSString                  *currentTableId;
@property (nonatomic, strong) TAJPlayTableView  *playTable;
@property (nonatomic)           BOOL            isRequestedNewTable;
@property (nonatomic)         BOOL              isAutoplayMode;
@property (nonatomic)         BOOL              isDoubleLoggedIn;
@property (nonatomic)   BOOL    isCardDiscarded;
@property (nonatomic)   BOOL    isThisDeviceTurn;
@property (nonatomic)   BOOL    isAlertTobeShown;
@property (nonatomic)   BOOL    isMyTurn;
@property (nonatomic)   BOOL    isTournament;
@property (nonatomic, strong)   NSString    *tourneyId;


@property(nonatomic,weak) id<TAJGameTableControllerDelegate> gameTableDelegate;
@property (nonatomic)           BOOL            isIamBackButtonPressed;
@property (strong, nonatomic) TAJInfoPopupViewController  *infoPopUpController;
@property (strong, nonatomic) TAJInfoPopupViewController *errorInfoPopupViewController;
@property (nonatomic, strong) NSString *buyInAmount;
@property (nonatomic, strong) TAJPlayerModel    *thisPlayerModel;
@property (strong, nonatomic) TAJChatViewController *chatViewController;
@property (nonatomic) BOOL isMiddleJoin;
@property (nonatomic) BOOL didSendMeldCards;
@property (nonatomic, strong) NSTimer               *playerTime;
@property (nonatomic, strong) NSTimer               *playerTimeForNegetiveDiscard;

@property (nonatomic) float                         playerTimeoutTime;
@property (nonatomic) float                         playerExtraTimeoutTime;

- (void)initializeGameTableController;

/*!
 @abstract
    Create a instance to quit table with dictionary.
 
 @param dictionary
    The dictionary which contain table data.
 
 @discussion
    The quitTableReplay: does the quit the table once click on leave table.
*/

- (void)addNotificationObserver;
- (BOOL)isRequestedToJoinNewTable;
- (BOOL)wantToShowIAmBack;
- (BOOL)canAllowUserToGamePlay;
- (BOOL)playerJoinedSpecator;
- (NSInteger)gameTableCurrentID;
- (void)startWaitingTimerForDuration:(double)duration andData:(NSDictionary *)dataDictionary;
- (void)playerReconnectedBackWithGetTableExtraEvent:(NSDictionary *)dictionary;
- (void)refreshControllerInGetTableDeatilsEvent:(NSDictionary*)dictionary;
- (void)resetTableAfterPlayerReconnectedBack:(NSDictionary *)dictionary;
- (void)playersAfterReconnectForDiscardCardHistory:(NSDictionary *)dictionary;
- (NSString *)avatarImageForPlayerIAmBackView;
- (void)removeGameTableMessage;
- (void)invalidatePlaayerTimer;
- (void)quitGame;
- (void)stopTimer;
- (void)showAlertMessageOnServerError:(NSString *)alertMessage;
- (void)removeAllNotifications;
//stop progressTimer
- (void) stopProgressTimer;

@end

@protocol TAJGameTableControllerDelegate <NSObject>
@optional
- (void)informingCurrenttableClosed;
- (void)createChatExtentView:(NSMutableArray *)messageList andPlayerModel:(TAJPlayerModel*)playerModel;
- (void)removeChatExtentViewController;
-(void)clearTheMessageDataSourceWhenReconnectedBack;
-(void)updateChatPlayerModelExtendView:(TAJPlayerModel*)playerModel;
- (void)tableIDForQuit:(int)tableID;
- (BOOL)isLobbyScreenShowing:(int)tableID;

- (void)highLightButtonWhenMyTurn:(int)tableID;
- (void)showAlertIndicatorWhenMyTurn;
- (void)hideAlertImageWhenOtherTurn;
- (void)hideTableAlertImageViewWhenTurnTimeOut;
- (void)newTableRequested:(int)tableID;
- (void)changeAlphaValueOfLobbyAndTableButtons;
- (void)resetAlphaValueOfLobbyAndTableButtons;
- (void)informControllerGameEnded:(NSString*)tableID;
// for auto play
- (void)informGameEngineForGameStarted:(int)tableID;
- (void)informGameEngineForGameEnded:(int)tableID;
- (void)cardDiscardedWhenAutoPlayMode:(NSDictionary *)dictionary;
- (void)cardPickedWhenAutoPlayMode:(NSDictionary *)dictionary;
- (void)canAllowUserToGamePlayInAutoPlayMode;
- (void)canShowIAmBackView:(BOOL)value;
- (void)removeThisPlayerDiscardCardWhenScheduleGame:(NSString*)tableID;
- (void)devicePlayerInAutoGamePlay:(NSString *)autoPlayCount WithTotalAutoPlayCount:(NSString *)totalCount;

//game rules web page
- (void) loadGameRulesWebPage;
- (void)chunksExtraTimeIntialization;


@end
