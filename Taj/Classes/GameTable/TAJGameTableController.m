/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJGameTableController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Pradeep BM on 13/06/14.
 Created by Sujit Yadawad on 03/02/14.
 **/

#import <AudioToolbox/AudioServices.h>

#import "TAJGameTableController.h"

#import "TAJScoreSheetViewController.h"
#import "TAJScoreBoardViewController.h"
#import "TAJLiveFeedViewController.h"
#import "TAJDiscardedCardViewController.h"
#import "TAJLiveFeedManager.h"
#import "UIPopover+iPhone.h"
#import "TAJScoreSheetModel.h"
#import "TAJNavigationBar.h"
#import "TAJGameEngine.h"
#import "TAJUserModel.h"
#import "TAJUtilities.h"
#import "TAJGameTable.h"
#import "TAJLobby.h"
#import "TAJAutoPlayView.h"
#import "TAJChatMenuViewController.h"
#import "TAJMeldCardView.h"
#import "TAJSmartCorrectionView.h"
#import "TAJJokerView.h"
#import "TAJWinnerScreenViewController.h"
#import "TAJMenuListView.h"
#import "TAJPlayerTurnView.h"
#import "TAJInfoPopupViewController.h"
#import "TAJReportaBugView.h"
#import "GameInformationView.h"//new
#import "TAJAppDelegate.h"
#import "TAJSlotCardManager.h"
#import "NSMutableArray+TAJNSMutableArray.h"
#import "TAJSoundHandler.h"
#import "TAJPlayerModelView.h"

#import "Reachability.h"

#import "ChunksView.h"

#define GAME_TABLE_IPHONE_LABEL          @" %@ Table Id:%@  Bet: %@ Game Type: %@"
#define MELD_VIEW_TOP_OFFSET_IPAD        35.0f
#define MELD_VIEW_TOP_OFFSET_IPHONE      15.0f
#define JOKER_VIEW_TOP_OFFSET_IPAD       25.0F
#define JOKER_VIEW_TOP_OFFSET_IPHONE     15.0f
#define WINNER_VIEW_TOP_OFFSET_IPAD      25.0f
#define WINNER_VIEW_TOP_OFFSET_IPHONE    15.0f
#define SLOT_CARD_FACTOR_IPHONE_5        18
#define SLOT_CARD_FACTOR_IPHONE_4        16

#define SETTING_BUTTON_UNSELECT_IMAGE_IPHONE   @"cog_bg_button-568h.png"
#define SETTING_BUTTON_UNSELECT_IMAGE_IPAD     @"cog_bg.png"

@interface TAJGameTableController () <TAJPlayTableProtocol, TAJScoreSheetViewControllerDelegate, TAJChairViewProtocol, TAJMeldCardViewDelegate,TAJSmartCorrectionViewDelegate, TAJJokerViewDelegate, TAJWinnerScreenDelegate,TAJMenuListViewDelegate, TAJChatViewControllerProtocol,TAJScoreBoardViewControllerDelegate, TAJSlotCardManagerDelegate, TAJAutoPlayViewDelegate, TAJPlayerModelViewDelegate, TAJReportaBugViewDelegate,GameInformationViewDelegate,TAJChatMenuViewControllerProtocol>

//-(void)reachabilityChanged:(NSNotification*)note;
@property (weak, nonatomic) IBOutlet UIView *viewAutoDrop;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewAutoDropBG;
@property (weak, nonatomic) IBOutlet UIButton *btnAutoDrop;
@property (weak, nonatomic) IBOutlet UILabel *lblAutoDrop;



@property (weak, nonatomic) IBOutlet UIImageView *imgViewAutoDrop;
@property (assign, nonatomic) BOOL  isAutoDrop;


// player card bg
@property (weak, nonatomic) IBOutlet UILabel        *scoreBoardLabel;
@property (weak, nonatomic) IBOutlet UIImageView    *playerOneCardBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView    *playerTwoCardBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView    *playerThreeCardBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView    *playerFourCardBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView    *playerFiveCardBgImageView;
@property (weak, nonatomic) IBOutlet UIImageView    *playerSixCardBgImageView;
@property (weak, nonatomic) __block NSMutableArray *firstPlayerAnimationImages;// = [NSMutableArray array];

@property (weak, nonatomic) IBOutlet UIView         *cardBackPlayerOneView;
@property (weak, nonatomic) IBOutlet UIView         *cardBackPlayerTwoView;
@property (weak, nonatomic) IBOutlet UIView         *cardBackPlayerThreeView;
@property (weak, nonatomic) IBOutlet UIView         *cardBackPlayerForthView;
@property (weak, nonatomic) IBOutlet UIView         *cardBackPlayerFifthView;
@property (weak, nonatomic) IBOutlet UIView         *cardBackPlayerSixthView;
@property (nonatomic, strong) NSMutableArray    *dealerPlayersArray;
@property (nonatomic, strong) NSMutableArray        *playerCardsBgImage;
@property (strong, nonatomic) NSMutableArray           *chairsArray;
@property (strong, nonatomic) NSMutableArray           *quitPlayersId;

//players
@property (weak, nonatomic) IBOutlet UIImageView    *playerOne;
@property (weak, nonatomic) IBOutlet UIImageView    *playerTwo;
@property (weak, nonatomic) IBOutlet UIImageView    *playerThree;
@property (weak, nonatomic) IBOutlet UIImageView    *playerFour;
@property (weak, nonatomic) IBOutlet UIImageView    *playerFive;
@property (weak, nonatomic) IBOutlet UIImageView    *playerSix;

//dealers
@property (weak, nonatomic) IBOutlet UIImageView    *dealerPlayerOne;
@property (weak, nonatomic) IBOutlet UIImageView    *dealerPlayerTwo;
@property (weak, nonatomic) IBOutlet UIImageView    *dealerPlayerThree;
@property (weak, nonatomic) IBOutlet UIImageView    *dealerPlayerFour;
@property (weak, nonatomic) IBOutlet UIImageView    *dealerPlayerFive;
@property (weak, nonatomic) IBOutlet UIImageView    *dealerPlayerSix;
@property (nonatomic) BOOL                          isChatButtonShow;
@property (nonatomic) BOOL                          isDiscardButtonShow;
@property (nonatomic) BOOL                          isFeedButtonShow;
@property (nonatomic) BOOL                          isScoreBoardButtonPressed;
@property (nonatomic) BOOL                          isPlayerQuit;
@property (nonatomic) BOOL                          isRejoinSuccessCame;

// auto play views
@property (nonatomic) BOOL                          canShowAutoPlayView;
@property (nonatomic) BOOL                          isKeyBoardShow;
@property (nonatomic) float                         keyBoardHig;
@property (nonatomic, strong) NSMutableArray        *playersAutoPlayViewArray;
@property (weak, nonatomic) IBOutlet UIView         *autoPlayPlayerSixthView;
@property (weak, nonatomic) IBOutlet UIView         *autoPlayPlayerFifthView;
@property (weak, nonatomic) IBOutlet UIView         *autoPlayPlayerFourthView;
@property (weak, nonatomic) IBOutlet UIView         *autoPlayPlayerThirdView;
@property (weak, nonatomic) IBOutlet UIView         *autoPlayPlayerSecondView;
@property (weak, nonatomic) IBOutlet UIView         *autoPlayPlayerFirstView;

@property (nonatomic, strong) TAJMeldCardView   *meldViewTable;
@property (nonatomic, strong) TAJSmartCorrectionView   *smartCorrectionView;

@property (nonatomic, strong) NSTimer           *gameStartTimer;
@property (nonatomic) double                    startTimerDuration;
@property (nonatomic) BOOL                      isFirstRoundGameAlreadyStarted;
@property (nonatomic) BOOL                      isGameStarted;
@property (nonatomic) BOOL                      isGameEnded;
@property (nonatomic) BOOL                      isScheduleGameCame;
@property (nonatomic) BOOL                      isSeatRequestPending;

@property (nonatomic,strong) UIView             *thisDevicePlayer;
@property (nonatomic) BOOL                      isTossWinner;
@property (nonatomic)   NSUInteger              maxPlayers;
@property (nonatomic)   NSUInteger              currentPlayers;
@property (nonatomic)   NSUInteger              minPlayers;


@property (nonatomic,strong) NSString           *thisChairID;

@property (nonatomic,strong) NSString           *previousMessage;
@property (nonatomic, strong) TAJAppDelegate    *appDelegate;

// middle join
@property (nonatomic)   BOOL                    canMiddleJoin;
@property (nonatomic)   BOOL                    isStrikesMiddleJoin;
@property (nonatomic)   BOOL                    isPoolDealMiddleJoin;
@property (nonatomic)   BOOL                    isGetTableDetailsEventCame;
@property (nonatomic)   BOOL                    canQuitGame;
@property (nonatomic)   BOOL                    isRejoinedJoin;
@property (nonatomic)   BOOL                    canAnimate;

@property (nonatomic, strong) NSMutableArray    *closedDeckArray;
@property (nonatomic, strong) NSMutableArray    *myCardsDealArray;
@property (nonatomic, strong) NSMutableArray    *mySendDealCardsArray;
@property (nonatomic, strong) NSMutableArray    *myFinalHandCardsArray;

@property (nonatomic, strong) NSString          *jokerCardId;
@property (nonatomic, strong) NSString          *jokerCardNumber;
@property (nonatomic, strong) NSString          *openCardId;
//watching game
@property (nonatomic)         BOOL              isSpectator;
@property (nonatomic)         BOOL              isSendDealCame;
@property (nonatomic)         BOOL              isSendSlotsCame;

//yogish
@property (weak, nonatomic) IBOutlet UIView *spectatorView;
@property (weak, nonatomic) IBOutlet UIImageView *chatAlertImageView;
@property (nonatomic)         BOOL              isJokerGameStarted;
@property (nonatomic, strong) NSMutableArray    *dropImageArray;
@property (nonatomic)         BOOL              isReconnected;
@property (nonatomic)         BOOL              isTossCardCame;

@property (nonatomic, strong) ChunksView      *chunksView;

//joker and no-joker table-type
@property (nonatomic, strong) TAJJokerView      *jokerPopTableView;
@property (nonatomic, weak)   TAJChairView      *jokerChairView;
@property (nonatomic)       BOOL                isRebuyin;
@property (nonatomic)       int                 jokerMinBuyin;
@property (nonatomic)       BOOL                isJokerTypeGame;
@property (nonatomic)       BOOL                isBestOfSixTypeGame;
@property (nonatomic)       BOOL                isBestOfTwoOrThreeTypeGame;

//timer for player to pick and leave card
@property (nonatomic, strong)   TAJPlayerTurnView   *playerTurnView;
@property (nonatomic, strong) NSMutableDictionary   *pickedCard;
@property (nonatomic)       BOOL            isCardPicked;

//timer for player to placing show
@property (nonatomic) float                     showTimeoutTime;
@property (nonatomic, strong) NSTimer           *playerShowTime;
@property (nonatomic, strong)   NSString        *meldThisMsgUUid;
@property (nonatomic, strong)   NSString        *smartCorrectionThisMsgUUid;

@property (nonatomic, strong)   NSString        *meldOtherMsgUUid;

//winner
@property (nonatomic)   BOOL                    isWinGame;

//table closed
@property (nonatomic)   BOOL                    isCurrentTableClosed;

//timer for restart new game
@property (nonatomic) float                     restartTimeoutTime;
@property (nonatomic, strong) NSTimer           *restartTime;

//is my turn

//this player dropped
@property (nonatomic) BOOL                      isThisDevicePlayerDroppedGame;

//game result
@property (nonatomic) BOOL                      isGameResultEventCame;

//reconnection
@property (atomic) BOOL                         isPlayerReconnected;

//Card distribution state
@property (nonatomic) BOOL                      isCardDistributionState;
@property (nonatomic) BOOL                      isGameScheduleState;
@property (nonatomic) BOOL                      canLeaveTable;
@property (nonatomic, strong) NSString          *leaveTableMessage;
@property (weak, nonatomic) IBOutlet UIImageView *winnerAIImageView;

//check player can play next level of game
@property (nonatomic, strong) TAJWinnerScreenViewController   *winnerScreenController;
@property (nonatomic, strong) NSString          *messageTitle;
@property (nonatomic, strong) NSString          *mainTitle;
@property (nonatomic) float                     rejoinTime;
@property (nonatomic, strong) NSTimer           *rejoinNSTimer;
@property (nonatomic, strong) NSString          *rejoinMsgUUid;
@property (nonatomic) float                     splitTime;


@property (nonatomic, strong) NSTimer           *splitNSTimer;
@property (nonatomic, strong) NSString          *splitMsgUUid;
@property (nonatomic)         BOOL              isSplitRequested;
@property(nonatomic ,strong) NSMutableArray *quitUsers;
@property(nonatomic ,strong) NSMutableArray *initialUsers;

@property (nonatomic, strong) NSMutableArray    *initialPlayerArray;
@property (nonatomic, strong) NSMutableArray    *currentPlayerArray;
@property (nonatomic, strong) NSArray    *thisPlayerCloseAnimationArray;


// UIAlertView's
#if NEW_INFO_VIEW_IMPLEMENTATION

@property (nonatomic, strong) TAJInfoPopupViewController   *leaveAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *cantLeaveAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *noHandAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *noEntryAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *showButtonAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *dropButtonAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *searchJoinAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *reshuffleAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *rebuyAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *splitRequestAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *splitRejectAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *rejoinAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *leaveForJokerAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *jokerJoinAlertView;
@property (nonatomic, strong) TAJInfoPopupViewController   *tourneyEleminateAlertView;


#else

@property (nonatomic, strong) UIAlertView       *leaveAlertView;
@property (nonatomic, strong) UIAlertView       *cantLeaveAlertView;
@property (nonatomic, strong) UIAlertView       *noHandAlertView;
@property (nonatomic, strong) UIAlertView       *noEntryAlertView;
@property (nonatomic, strong) UIAlertView       *showButtonAlertView;
@property (nonatomic, strong) UIAlertView       *dropButtonAlertView;
@property (nonatomic, strong) UIAlertView       *searchJoinAlertView;
@property (nonatomic, strong) UIAlertView       *reshuffleAlertView;
@property (nonatomic, strong) UIAlertView       *rebuyAlertView;
@property (nonatomic, strong) UIAlertView       *splitRequestAlertView;
@property (nonatomic, strong) UIAlertView       *splitRejectAlertView;
@property (nonatomic, strong) UIAlertView       *rejoinAlertView;
@property (nonatomic, strong) UIAlertView       *leaveForJokerAlertView;
@property (nonatomic, strong) UIAlertView       *insufficientChipsAlertView;//ratheesh

#endif

@property (weak, nonatomic) IBOutlet UIView     *playTableHolderView;

@property (weak, nonatomic) IBOutlet UIView     *chair1;
@property (weak, nonatomic) IBOutlet UIView     *chair2;
@property (weak, nonatomic) IBOutlet UIView     *chair3;
@property (weak, nonatomic) IBOutlet UIView     *chair4;
@property (weak, nonatomic) IBOutlet UIView     *chair5;
@property (weak, nonatomic) IBOutlet UIView     *chair6;
@property (weak, nonatomic) TAJChairView        *currentTurnChair;

//IBOutlet labels
@property (weak, nonatomic) IBOutlet UILabel *usernameLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableIDLabel;
@property (weak, nonatomic) IBOutlet UILabel *tableBetLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameTypeLabel;
@property (weak, nonatomic) IBOutlet UILabel *gameIDLabel;
@property (strong, nonatomic) NSString *gameIDString;
@property (strong, nonatomic) NSString *tableIDString;

@property (weak, nonatomic) IBOutlet UILabel *prizeLabel;
@property (weak, nonatomic) IBOutlet UILabel *balanceLabel;

//leave button
@property (weak, nonatomic) IBOutlet UIButton *leaveButton;

// Chat , Discard Live feed view holder
@property (weak, nonatomic) IBOutlet UIView  *viewHolderForChatDiscardLiveFeed;

// chat view

@property (weak, nonatomic) IBOutlet UIButton *chatButton;
- (IBAction)chatButtonAction:(UIButton *)sender;

//Live Feed
@property (strong, nonatomic) TAJLiveFeedViewController *liveFeedController;
@property (strong, nonatomic) NSMutableDictionary       *lastOpenDeckCardDict;
@property (weak, nonatomic) IBOutlet UIButton           *liveFeedButton;
- (IBAction)liveFeedButtonAction:(UIButton *)sender;

// discarded cards view
@property (strong, nonatomic) TAJDiscardedCardViewController   *discardCardHistoryController;
@property (strong, nonatomic) NSMutableDictionary              *starImageForCardInfo;
@property (weak, nonatomic) IBOutlet UIButton                  *discardHistoryButton;
- (IBAction)discardHistoryButtonAction:(UIButton *)sender;

//Game id and prize update method
- (void)updateGameIDAndPrize:(NSDictionary*)    dictonary;

//to sort array of deck
@property (weak, nonatomic) IBOutlet UIButton  *sortButton;
- (IBAction)sortDeckCard:(UIButton *)sender;

//extra time
@property (weak, nonatomic) IBOutlet UIButton *extraTimeButton;
- (IBAction)extraTimeButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UILabel *extraTimeLabel;
@property (weak, nonatomic) IBOutlet UIImageView *extraTimeBg;

//Show button
@property (weak, nonatomic) IBOutlet UIButton       *showButton;
@property (strong , nonatomic) NSMutableDictionary  *discardCardInfoForShow;
- (IBAction)showButtonAction:(UIButton *)sender;

//Drop button
@property (weak, nonatomic) IBOutlet UIButton       *dropButton;
- (IBAction)dropButtonAction:(UIButton *)sender;

//flip cards
@property (weak, nonatomic) IBOutlet UIButton       *flipCardsButton;
- (IBAction)flipCardsAction:(UIButton *)sender;

//pass game result or last hand
@property (nonatomic, strong) TAJScoreSheetViewController *scoreSheetViewController;
//@property (weak, nonatomic) IBOutlet UIButton             *lastHandButton;
//- (IBAction)lastHandButtonAction:(UIButton *)sender;

//score board
@property (strong, nonatomic) TAJScoreBoardViewController *boardScoreController;
@property (weak, nonatomic) IBOutlet UIButton             *scoreBoardButton;
- (IBAction)scoreBoardButtonAction:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *thisPlayerCloseAnimationImageView;

//Author: Yogish start
//SettingsMenu
@property (weak, nonatomic) IBOutlet UIButton *settingsMenuButton;
- (IBAction)settingsMenuButtonAction:(UIButton *)sender;
@property (strong , nonatomic) TAJMenuListView *settingMenuListView;
@property BOOL isMenuListShowing;
//ChatMenuView
@property (strong , nonatomic) TAJChatMenuViewController *chatMenuView;
//@property (strong , nonatomic) TAJChatExtentViewController *chatExtentView;
//Author: Yogish end
@property (weak, nonatomic) IBOutlet UIImageView *devicePlayerAvatarImageView;
@property (weak, nonatomic) IBOutlet UILabel *rupeesSymbolLabel;
@property (weak, nonatomic) IBOutlet UILabel *betRupeesSymbol;
@property (weak, nonatomic) IBOutlet UIImageView *slectedStateImageViewForLeaveTable;

//Report a Bug View
@property (strong , nonatomic) TAJReportaBugView *reportBugView;

@property (strong , nonatomic) GameInformationView *gameInformationView;


@property (nonatomic)   int  noOfGameRounds;

@property (weak, nonatomic) IBOutlet UIImageView *cardBackAnimationImageViewForFirstPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *cardBackAnimationImageViewForSecondPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *cardBackAnimationImageViewForThirdPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *cardBackAnimationImageViewForFourthPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *cardBackAnimationImageViewForFifthPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *cardBackAnimationImageViewForSixthPlayer;

@property (weak, nonatomic) IBOutlet UIImageView *dropCardImageViewForFirstPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *dropCardImageViewForSecondPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *dropCardImageViewForThirdPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *dropCardImageViewForFourthPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *dropCardImageViewForFifthPlayer;
@property (weak, nonatomic) IBOutlet UIImageView *dropCardImageViewForSixthPlayer;

@property(nonatomic, strong) NSMutableArray *cardBackAnimationImageViewArray;
@property(nonatomic, strong) NSMutableArray *dropCardImageViewArray;
@property(nonatomic, strong) NSMutableArray *cardBackCloseAnimationPlayerArray;
@property(nonatomic, strong) NSMutableArray *cardBackAnimationPlayerArray;

@property(nonatomic, strong) NSMutableArray *firstPlayerCardBackImageArray;
@property(nonatomic, strong) NSMutableArray *firstPlayerCloseCardBackImageArray;

@property(nonatomic, strong) NSMutableArray *secondPlayerCardBackImageArray;
@property(nonatomic, strong) NSMutableArray *secondPlayerCloseCardBackImageArray;

@property(nonatomic, strong) NSMutableArray *thirdPlayerCardBackImageArray;
@property(nonatomic, strong) NSMutableArray *thirdPlayerCloseCardBackImageArray;

@property(nonatomic, strong) NSMutableArray *fourthPlayerCardBackImageArray;
@property(nonatomic, strong) NSMutableArray *fourthPlayerCloseCardBackImageArray;

@property(nonatomic, strong) NSMutableArray *fifthPlayerCardBackImageArray;
@property(nonatomic, strong) NSMutableArray *fifthPlayerCloseCardBackImageArray;

@property(nonatomic, strong) NSMutableArray *sixthPlayerCardBackImageArray;
@property(nonatomic, strong) NSMutableArray *sixthPlayerCloseCardBackImageArray;
@property(nonatomic, strong) TAJInfoPopupViewController *infoPopUpViewController;

// slot operations
@property (strong, nonatomic)   TAJSlotCardManager  *cardSlotManager;
@property (strong, nonatomic)   NSMutableArray  *slotCardInfoArray;
// IAm Back View
@property (nonatomic)   BOOL    canShowIamBackView;
@property (nonatomic)   BOOL    isPlayerCanEnterGamePlayInAutoPlayMode;
@property (nonatomic)   BOOL    isPlayerDisconnected;
@property (nonatomic)   BOOL    isCardAlreadyAnimated;
@property (nonatomic)   BOOL    isValidShowPreGameResult;

@property (nonatomic, strong)   NSString* avatarImageForIAmBack;
@property (nonatomic, strong)   NSMutableArray  *tossCardArray;
@property (nonatomic) float delayCount;
// Player Model View
@property (nonatomic, strong)   TAJPlayerModelView  *playerModelView;
@property (nonatomic)       BOOL            isPlayerModelSlided;
@property (nonatomic)       BOOL            isPlayerModeButtonClicked;
@property (weak, nonatomic) IBOutlet UIView *playerModelViewHolder;
@property (weak, nonatomic) IBOutlet UIButton *playerModelButton;
- (IBAction)playerModelButtonAction:(UIButton *)sender;

@property (nonatomic) NSInteger currentUserId;
@property (nonatomic) BOOL isExtraTimeButtonClicked;

//Dual login fix
@property (nonatomic) BOOL isDisConnectReconnect;

@property (weak, nonatomic) IBOutlet UIView* runningStatusView;
@property (weak, nonatomic) IBOutlet UILabel* runningLevelLbl;
@property (weak, nonatomic) IBOutlet UILabel* runningTimerLbl;
@property (strong, nonatomic) NSTimer* stopWatchTimer;
@property (nonatomic) double levelTimer;
@property (weak, nonatomic) IBOutlet UIView * viewGameDetails;

@property (weak, nonatomic) IBOutlet UIView * viewTourneyDetails;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyID;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyGameID;
@property (weak, nonatomic) IBOutlet UIImageView* imgViewMoreInfo;
@property (nonatomic) BOOL isMoreInfo;
@property (weak, nonatomic) IBOutlet UIView * viewTourneyInfo;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyType;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyEntry;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyBet;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyRebuy;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyRank;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyBalance;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyPrize;
@property (weak, nonatomic) IBOutlet UILabel* lblTourneyLevel;

@property(strong, nonatomic) NSMutableArray * eventsArray;
@property (nonatomic, strong)   NSString* savedFirstTableID;
@property (nonatomic, strong)   NSString* savedSecondTableID;


@property (weak, nonatomic) IBOutlet UIView *viewChunksPlayer1;
@property (weak, nonatomic) IBOutlet UIView *viewChunksPlayer2;
@property (weak, nonatomic) IBOutlet UIView *viewChunksPlayer3;
@property (weak, nonatomic) IBOutlet UIView *viewChunksPlayer4;
@property (weak, nonatomic) IBOutlet UIView *viewChunksPlayer5;
@property (weak, nonatomic) IBOutlet UIView *viewChunksPlayer6;
//palyer1 Chuncks levels
@property (weak, nonatomic) IBOutlet UIView *player1ChunksLevel1;
@property (weak, nonatomic) IBOutlet UIView *player1ChunksLevel2;
@property (weak, nonatomic) IBOutlet UIView *player1ChunksLevel3;

@property (weak, nonatomic) IBOutlet UIView *player2ChunksLevel1;
@property (weak, nonatomic) IBOutlet UIView *player2ChunksLevel2;
@property (weak, nonatomic) IBOutlet UIView *player2ChunksLevel3;

@property (weak, nonatomic) IBOutlet UIView *player3ChunksLevel1;
@property (weak, nonatomic) IBOutlet UIView *player3ChunksLevel2;
@property (weak, nonatomic) IBOutlet UIView *player3ChunksLevel3;

@property (weak, nonatomic) IBOutlet UIView *player4ChunksLevel1;
@property (weak, nonatomic) IBOutlet UIView *player4ChunksLevel2;
@property (weak, nonatomic) IBOutlet UIView *player4ChunksLevel3;

@property (weak, nonatomic) IBOutlet UIView *player5ChunksLevel1;
@property (weak, nonatomic) IBOutlet UIView *player5ChunksLevel2;
@property (weak, nonatomic) IBOutlet UIView *player5ChunksLevel3;

@property (weak, nonatomic) IBOutlet UIView *player6ChunksLevel1;
@property (weak, nonatomic) IBOutlet UIView *player6ChunksLevel2;
@property (weak, nonatomic) IBOutlet UIView *player6ChunksLevel3;

//Extra Timer
@property (weak, nonatomic) IBOutlet UIView *viewExtraTimerPlayer1;
@property (weak, nonatomic) IBOutlet UIView *viewExtraTimerPlayer2;
@property (weak, nonatomic) IBOutlet UIView *viewExtraTimerPlayer3;
@property (weak, nonatomic) IBOutlet UIView *viewExtraTimerPlayer4;
@property (weak, nonatomic) IBOutlet UIView *viewExtraTimerPlayer5;
@property (weak, nonatomic) IBOutlet UIView *viewExtraTimerPlayer6;

@property (weak, nonatomic) IBOutlet UILabel *lblPlayer1ExtraTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayer2ExtraTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayer3ExtraTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayer4ExtraTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayer5ExtraTime;
@property (weak, nonatomic) IBOutlet UILabel *lblPlayer6ExtraTime;

@property (strong, nonatomic)  NSTimer *timer;
@property (assign, nonatomic) int currMinute;
@property (assign, nonatomic) int currSeconds;

@property (assign, nonatomic) NSInteger currentPlayerTurn;

@property (nonatomic, strong)   NSString* realChipsValue, *realInPlayValue, *funChipsValue, *funInPlayValue;;
@property (weak, nonatomic) IBOutlet UIView *viewReportBug;

@property (weak, nonatomic) IBOutlet UIView *viewTopGo;


@end

@implementation TAJGameTableController
int chunksExtraTimer = 0;

@synthesize isTournament,tourneyId,runningStatusView,runningLevelLbl,runningTimerLbl;
@synthesize stopWatchTimer,levelTimer;
- (id)init
{
    self = [super init];
    if (self)
    {
        // Custom initialization
        [self initializeGameTableController];
    }
    return self;
}

#pragma mark - APP LIFE CYCLE

- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self initializeGameTableController];
    self.isSplitRequested = NO;
    self.isValidShowPreGameResult = NO;
    self.isMiddleJoin = NO;
    self.didSendMeldCards = NO;
    self.isRejoinSuccessCame = NO;
    self.isDisConnectReconnect = NO;
    
    [self.view endEditing:YES];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Game Table"];
    
    if ([TAJUtilities isItIPhonex] || [TAJUtilities isItIPhone6Plus] || [TAJUtilities isItIPhoneXSMax]) {
        CGRect Frame = self.autoPlayPlayerFourthView.frame;
        Frame.origin.x = self.autoPlayPlayerFourthView.frame.origin.x - 4;
        self.autoPlayPlayerFourthView.frame = Frame;
    }
    
    [self hideTourneyInfo];
    
    self.reachability = Nil;
    self.reachability = [Reachability reachabilityForInternetConnection];
    [self.reachability startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(internetConnectionChanged) name:kReachabilityChangedNotification object:Nil];
    
    [self setDefaultsChunks];
    [self setDefaultsExtraTimers];
    
    [self configureView];
    
}

-(void)configureView {
    
    self.viewTopGo.layer.borderColor = [[UIColor lightGrayColor] CGColor];
    
    // green color
    UIColor *greenColorBorder = [UIColor colorWithRed:142/255.0
                                                green:218/255.0
                                                 blue:57/255.0
                                                alpha:1];
    
    self.viewExtraTimerPlayer1.layer.borderColor = [greenColorBorder CGColor];
    self.viewExtraTimerPlayer2.layer.borderColor = [greenColorBorder CGColor];
    self.viewExtraTimerPlayer3.layer.borderColor = [greenColorBorder CGColor];
    self.viewExtraTimerPlayer4.layer.borderColor = [greenColorBorder CGColor];
    self.viewExtraTimerPlayer5.layer.borderColor = [greenColorBorder CGColor];
    self.viewExtraTimerPlayer6.layer.borderColor = [greenColorBorder CGColor];
    
    [self setDefaultsExtraTimers];
    
    if ([TAJUtilities isItIPhone6]) {
        CGRect Frame = self.viewExtraTimerPlayer1.frame;
        Frame.origin.x = self.viewExtraTimerPlayer1.frame.origin.x - 4;
        self.viewExtraTimerPlayer1.frame = Frame;
        
        CGRect Frame2 = self.viewExtraTimerPlayer2.frame;
        Frame2.origin.x = self.viewExtraTimerPlayer2.frame.origin.x - 2;
        self.viewExtraTimerPlayer2.frame = Frame2;
        
        CGRect Frame3 = self.viewExtraTimerPlayer3.frame;
        Frame3.origin.x = self.viewExtraTimerPlayer3.frame.origin.x - 4;
        self.viewExtraTimerPlayer3.frame = Frame3;
        
        CGRect Frame4 = self.viewExtraTimerPlayer4.frame;
        Frame4.origin.x = self.viewExtraTimerPlayer4.frame.origin.x - 4;
        self.viewExtraTimerPlayer4.frame = Frame4;
        
        CGRect chunksFrame2 = self.viewChunksPlayer2.frame;
        chunksFrame2.origin.x = self.viewChunksPlayer2.frame.origin.x - 4;
        self.viewChunksPlayer2.frame = chunksFrame2;
        
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    self.isSplitRequested = NO;
    self.chatButton.exclusiveTouch = YES;
    self.discardHistoryButton.exclusiveTouch = YES;
    self.liveFeedButton.exclusiveTouch = YES;
    self.scoreBoardButton.exclusiveTouch = YES;
    self.settingsMenuButton.exclusiveTouch = YES;
    self.sortButton.exclusiveTouch = YES;
    self.dropButton.exclusiveTouch = YES;
    self.showButton.exclusiveTouch = YES;
    self.isChatButtonShow = YES;
    self.isSeatRequestPending = NO;
    self.viewReportBug.hidden = YES;
    [self createChatView];
}

- (void)hidePlayerImages{
    
    self.playerOne.hidden = NO;
    self.playerTwo.hidden = YES;
    self.playerThree.hidden = YES;
    self.playerFour.hidden = NO;
    self.playerFive.hidden = YES;
    self.playerSix.hidden = YES;
}

- (void)unHidePlayerImages{
    self.playerOne.hidden = NO;
    self.playerTwo.hidden = NO;
    self.playerThree.hidden = NO;
    self.playerFour.hidden = NO;
    self.playerFive.hidden = NO;
    self.playerSix.hidden = NO;
}


- (void)viewDidAppear:(BOOL)animated
{
    //[ self createChatView];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iamBackButtonPressed:) name:SHOW_IAMBACKBUTTON_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(iamBackThisPlayerArrayCount:) name:SHOW_IAMBACKARRAY_ALERT object:nil];
    
    self.isSplitRequested = NO;
    [UIViewController attemptRotationToDeviceOrientation];
}

- (void)showTourneyInfo {
    self.imgViewMoreInfo.image = [UIImage imageNamed:@"minus"];
    self.isMoreInfo = YES;
    self.viewTourneyInfo.hidden = NO;
}

- (void)hideTourneyInfo {
    self.imgViewMoreInfo.image = [UIImage imageNamed:@"plus_tourney"];
    self.isMoreInfo = NO;
    self.viewTourneyInfo.hidden = YES;
}

- (IBAction)moreInfoClick:(UIButton *)sender
{
    if (self.isMoreInfo) {
        [self hideTourneyInfo];
    }
    else {
        [self showTourneyInfo];
    }
}

- (void)iamBackThisPlayerArrayCount:(NSNotification *) notification
{
    NSDictionary *dict = [notification userInfo];
    
    if(self.discardCardHistoryController)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_IAMBACKARRAY_FOR_DISCARD_ALERT object:nil userInfo:dict];
    }
}

- (void)iamBackButtonPressed:(NSNotification *)notification
{
    self.isIamBackButtonPressed = YES;
    self.isAutoplayMode = NO;
    [self createDiscardCardHistoryView];
    [self playersAfterReconnectForDiscardCardHistory:self.playTableDictionary];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (BOOL)prefersStatusBarHidden
{
    return YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self removeAllNotifications];
}

//- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//
//    UITouch *touch = [touches anyObject];
//    UIView *view = touch.view;
//
//    if(![view isEqual:self.reportBugView.reportBugContainerView])
//    {
//        [self.reportBugView.bugReportTextField resignFirstResponder];
//        //[self resetSettingMenuButton];
//
//        // [self removeReportBugView];
//#if DEBUG
//        NSLog(@"CONTAINER");
//#endif
//
//    }
//    if ([view isEqual:self.scoreSheetViewController.view]) {
//    
//    }
//    //    if(![view isEqual:self.reportBugView.reportBugContainerView])
//    //    {
//    //        [self.reportBugView.bugReportTextField resignFirstResponder];
//    //        [self resetSettingMenuButton];
//    //        [self removeReportBugView];
//    //         NSLog(@"CONTAINER");
//    //    }
//
//    if ([view isEqual:self.reportBugView])
//    {
//        [self.reportBugView.bugReportTextField resignFirstResponder];
//        [self resetSettingMenuButton];
//#if DEBUG
//        NSLog(@"MAIN C");
//#endif
//        [self removeReportBugView];
//    }
//    [self removeViewForTouchBegan];
//}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}

- (void)viewDidUnload
{
    [self removeAllNotifications];
    [super viewDidUnload];
    
}

- (void)removeViewForTouchBegan
{
    if (self.boardScoreController)
    {
        [self removeScoreBoard];
    }
    
    if (self.settingMenuListView)
    {
        [self.settingMenuListView removeFromSuperview];
        self.settingMenuListView = nil;
    }
    
    //    if ([TAJUtilities isIPhone])
    //    {
    //        if (!self.reportBugView)
    //        {
    //            [self.settingsMenuButton setBackgroundImage:[UIImage imageNamed:SETTING_BUTTON_UNSELECT_IMAGE_IPHONE] forState:UIControlStateNormal];
    //        }
    //    }
    //    else
    //    {
    //        if (!self.reportBugView)
    //        {
    //            [self.settingsMenuButton setBackgroundImage:[UIImage imageNamed:SETTING_BUTTON_UNSELECT_IMAGE_IPAD] forState:UIControlStateNormal];
    //        }
    //    }
    
    if(self.chatViewController)
    {
        [self.chatViewController.chatField resignFirstResponder];
    }
}

#pragma mark - SEARCH_JOIN_TABLE -

- (BOOL)isRequestedToJoinNewTable
{
    return self.isRequestedNewTable;
}

#pragma mark - I AM BACK -

- (BOOL)wantToShowIAmBack
{
    return self.canShowIamBackView;
}

- (BOOL)canAllowUserToGamePlay
{
    return self.isPlayerCanEnterGamePlayInAutoPlayMode;
}

- (void)saveResultForKey:(BOOL) result
{
    NSNumber *resultCheck = [NSNumber numberWithBool:result];
    if(self.currentTableId && resultCheck != nil)
    {
        [[NSUserDefaults standardUserDefaults] setBool:result forKey:self.currentTableId];
        [[NSUserDefaults standardUserDefaults] synchronize];
    }
}

-(BOOL)retrieveResultForKey
{
    NSNumber *resultCheck;
    BOOL result;
    if(self.currentTableId)
    {
        result = [[NSUserDefaults standardUserDefaults] boolForKey:self.currentTableId];
        resultCheck = [NSNumber numberWithBool:result];
        if(resultCheck != nil)
        {
            return  result;
        }
    }
    return NO;
}

#pragma mark - initializing game table control -

- (void)controllerPropertySetUp
{
    [[TAJGameEngine sharedGameEngine] setCurrentController:self];
    self.isFirstRoundGameAlreadyStarted = NO;
    self.isIamBackButtonPressed = YES;
    self.isGameStarted = NO;
    self.isKeyBoardShow = NO;
    self.isCardDistributionState = NO;
    self.isGameScheduleState = NO;
    self.isRequestedNewTable = NO;
    self.canLeaveTable = YES;
    self.isSpectator = YES;
    self.isWinGame = NO;
    self.isCurrentTableClosed = NO;
    self.isRebuyin = NO;
    self.isJokerTypeGame = NO;
    self.isBestOfSixTypeGame = NO;
    self.isJokerGameStarted = NO;
    self.isCardDiscarded = NO;
    self.canShowIamBackView = YES;
    self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    self.isPlayerDisconnected = NO;
    self.isThisDeviceTurn = NO;
    self.isPlayerModelSlided = NO;
    self.isGameEnded = NO;
    self.canShowAutoPlayView = NO;
    self.isStrikesMiddleJoin = NO;
    self.isPoolDealMiddleJoin = NO;
    self.isSendDealCame = NO;
    self.isSendSlotsCame = NO;
    self.isPlayerDisconnected = NO;
    self.isReconnected = NO;
    self.noOfGameRounds = 0;
    self.isGetTableDetailsEventCame = NO;
    self.isTossCardCame = NO;
    self.currentTurnChair = nil;
    self.jokerCardId = nil;
    self.isSplitRequested = NO;
    self.isPlayerQuit = NO;
    self.didSendMeldCards = NO;
    self.canMiddleJoin = YES;
    self.isBestOfTwoOrThreeTypeGame = NO;
    self.isDoubleLoggedIn = NO;
    
    self.tossCardArray = [NSMutableArray array];
    self.quitPlayersId = [NSMutableArray array];
    
    self.initialPlayerArray = [NSMutableArray array];
    self.currentPlayerArray = [NSMutableArray array];
    self.quitUsers = [NSMutableArray array];
    self.initialUsers = [NSMutableArray array];
    
    self.playerCardsBgImage = [NSMutableArray arrayWithObjects:self.playerOneCardBgImageView,self.playerTwoCardBgImageView,
                               self.playerThreeCardBgImageView,self.playerFourCardBgImageView,self.playerFiveCardBgImageView,self.playerSixCardBgImageView, nil];
    
    self.dealerPlayersArray = [NSMutableArray arrayWithObjects:self.dealerPlayerOne,self.dealerPlayerTwo,
                               self.dealerPlayerThree,self.dealerPlayerFour,self.dealerPlayerFive,self.dealerPlayerSix, nil];
    
    self.playersAutoPlayViewArray = [NSMutableArray arrayWithObjects:self.autoPlayPlayerFirstView,self.autoPlayPlayerSecondView,
                                     self.autoPlayPlayerThirdView,self.autoPlayPlayerFourthView,self.autoPlayPlayerFifthView,self.autoPlayPlayerSixthView, nil];
    
    self.cardBackAnimationImageViewArray = [NSMutableArray arrayWithObjects:self.cardBackAnimationImageViewForFirstPlayer,self.cardBackAnimationImageViewForSecondPlayer,self.cardBackAnimationImageViewForThirdPlayer,self.cardBackAnimationImageViewForFourthPlayer,self.cardBackAnimationImageViewForFifthPlayer,self.cardBackAnimationImageViewForSixthPlayer,nil];
    self.dropCardImageViewArray = [NSMutableArray arrayWithObjects:self.dropCardImageViewForFirstPlayer,self.dropCardImageViewForSecondPlayer,self.dropCardImageViewForThirdPlayer,self.dropCardImageViewForFourthPlayer,self.dropCardImageViewForFifthPlayer,self.dropCardImageViewForSixthPlayer,nil];
    
    
    if([TAJUtilities isIPhone])
    {
        
        self.firstPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPHONE], nil];
        //For device player
        
        self.thisPlayerCloseAnimationArray = [NSArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        
        
        // For Spectator
        self.firstPlayerCloseCardBackImageArray = [NSMutableArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        
        self.secondPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPHONE], nil];
        
        self.secondPlayerCloseCardBackImageArray = [NSMutableArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        self.thirdPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPHONE], nil];
        
        self.thirdPlayerCloseCardBackImageArray = [NSMutableArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        
        self.fourthPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPHONE], nil];
        
        self.fourthPlayerCloseCardBackImageArray = [NSMutableArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        
        
        self.fifthPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPHONE], nil];
        
        self.fifthPlayerCloseCardBackImageArray = [NSMutableArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        
        self.sixthPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPHONE], nil];
        
        self.sixthPlayerCloseCardBackImageArray = [NSMutableArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPHONE], nil];
        
        self.dropImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPHONE],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPHONE], nil];
    }
    
    else
        
    {
        self.firstPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPAD], nil];
        //For device player
        
        self.thisPlayerCloseAnimationArray = [NSArray  arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_7_IPAD], nil];
        
        
        // For Spectator
        self.firstPlayerCloseCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        
        self.secondPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPAD], nil];
        
        self.secondPlayerCloseCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        self.thirdPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPAD], nil];
        
        self.thirdPlayerCloseCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        
        self.fourthPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPAD], nil];
        
        self.fourthPlayerCloseCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        
        
        self.fifthPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPAD], nil];
        
        self.fifthPlayerCloseCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        
        self.sixthPlayerCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_ADDING_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_8_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_9_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_10_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_11_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_12_IPAD],[UIImage imageNamed:FOURTH_PLAYER_ADDING_13_IPAD], nil];
        
        self.sixthPlayerCloseCardBackImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_1_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_2_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_3_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_4_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_5_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_6_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        
        self.dropImageArray = [NSMutableArray arrayWithObjects:[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD],[UIImage imageNamed:FOURTH_PLAYER_DELETING_SPEC_7_IPAD], nil];
        
    }
    self.cardBackAnimationPlayerArray = [NSMutableArray arrayWithObjects:self.firstPlayerCardBackImageArray,self.secondPlayerCardBackImageArray,self.thirdPlayerCardBackImageArray,self.fourthPlayerCardBackImageArray,self.fifthPlayerCardBackImageArray,self.sixthPlayerCardBackImageArray, nil];
    
    self.cardBackCloseAnimationPlayerArray = [NSMutableArray arrayWithObjects: self.firstPlayerCloseCardBackImageArray,self.secondPlayerCloseCardBackImageArray,self.thirdPlayerCloseCardBackImageArray,self.fourthPlayerCloseCardBackImageArray,self.fifthPlayerCloseCardBackImageArray,self.sixthPlayerCloseCardBackImageArray, nil];
    
    self.isChatButtonShow = YES;
    self.isDiscardButtonShow = YES;
    self.isFeedButtonShow = YES;
    self.isScoreBoardButtonPressed = YES;
}

- (void)initializeGameTableController
{
    
    //rey edo call osthundi CEO tho unnam
    
    [self controllerPropertySetUp];
    self.isPlayerReconnected = NO;
    self.isScheduleGameCame = NO;
    self.isExtraTimeButtonClicked = NO;
    [self.view insertSubview:self.extraTimeBg belowSubview:self.extraTimeButton];
    self.myFinalHandCardsArray = [[NSMutableArray alloc] init];
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = NO;
    }
    
    //create instance for slot card manager
    self.cardSlotManager = [[TAJSlotCardManager alloc] init];
    self.cardSlotManager.slotDelegate = self;
    
    self.view.multipleTouchEnabled = NO;
    
    self.dropButton.hidden = YES;

    [self hideAutoDrop];
    
    //Author:RK commenting this code to enable the it after loadALLUI
    // [self createViewForGamePlay];
    [self createDiscardCardHistoryView];
    _isMyTurn = NO;
    
    //Author :Pradeep start
    // self.playTableDictionary is initialzing in switch lobby controller
    // so no need to create external game engine to load content for game table
    self.currentTableId = [self.playTableDictionary valueForKeyPath:GAME_TABLE_ID_VALUE];
    //Author :Pradeep ends
    // register notification
    [self addNotificationObserver];
    
    //NSLog(@"FIRST DATA %@",self.playTableDictionary);
    
    // Get max player in table
    self.maxPlayers = [[self.playTableDictionary valueForKeyPath:GAME_GET_MAX_PLAYER] integerValue];
    self.minPlayers = [[self.playTableDictionary valueForKeyPath:GAME_GET_MIN_PLAYER] integerValue];
    
    if (self.maxPlayers == TWO_PLAYER)
    {
        // If two player game other player is opposite thisplayer
        self.chairsArray = [NSMutableArray arrayWithObjects:self.chair1, self.chair4, nil];
        [self hidePlayerImages];
    }
    else
    {
        self.chairsArray = [NSMutableArray arrayWithObjects:self.chair1, self.chair2, self.chair3,self.chair4, self.chair5, self.chair6, nil];
        [self unHidePlayerImages];
    }
    
    NSDictionary * playersCountDict = @{
                                        @"table_id" : self.currentTableId,
                                        @"maxplayer" : [NSNumber numberWithInteger:self.maxPlayers],
                                        @"count" : [NSNumber numberWithInteger:1]
                                        };
    //NSLog(@"GAME TABLE POST DATA%@",playersCountDict);
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self hideViewForNewGame];
    [self refreshControllerForNewGameEnd];
    
    [self loadAllUI];
    
    //Author:RK moved this here to make the game play proper
    [self createViewForGamePlay];
    
    [self updateGameInfo:self.playTableDictionary];
    
    if ([TAJUtilities retrieveFromUserDefaultsForKey:USER_DEFAULT_SOUND]==nil)
    {
        [[TAJSoundHandler sharedManager] EnableSounds];
        [TAJUtilities saveToUserDefaults:[NSNumber numberWithBool:YES] forKey:USER_DEFAULT_SOUND];
    }
    else
    {
        if ([((NSString*)[TAJUtilities retrieveFromUserDefaultsForKey:USER_DEFAULT_SOUND]) boolValue])
        {
            [[TAJSoundHandler sharedManager] EnableSounds];
        }
        else
        {
            [[TAJSoundHandler sharedManager] DisableSounds];
        }
    }
    
    if ([TAJUtilities retrieveFromUserDefaultsForKey:USER_DEFAULT_VIBRATE]==nil)
    {
        [TAJUtilities saveToUserDefaults:[NSNumber numberWithBool:YES] forKey:USER_DEFAULT_VIBRATE];
    }
    self.leaveTableMessage = CANNOT_LEAVE_TABLE_MESSAGE;
    
}

//setter method for turn
-(void)setIsMyTurn:(BOOL)isMyTurn
{
    if (_isMyTurn != isMyTurn)
    {
        _isMyTurn = isMyTurn;
        [self.playTable setIsItMyTurn:_isMyTurn];
        // set player model for turn
#if PLAYER_MODEL_GAME_CARD
        [self.playerModelView setIsItMyTurn:_isMyTurn];
#endif
    }
}

-(void)hideAutoDrop {
    self.viewAutoDrop.hidden = YES;
    self.btnAutoDrop.enabled = NO;
    self.imgViewAutoDropBG.hidden = YES;
    self.imgViewAutoDrop.hidden = YES;
    self.lblAutoDrop.hidden = YES;
}

-(void)unHideAutoDrop {
    self.viewAutoDrop.hidden = NO;
    self.btnAutoDrop.enabled = YES;
    self.imgViewAutoDropBG.hidden = NO;
    self.imgViewAutoDrop.hidden = NO;
    self.lblAutoDrop.hidden = NO;
}

- (void)loadAllUI
{
    NSString *tableType = [self.playTableDictionary valueForKeyPath:kTaj_Table_Type_Path];
    self.viewAutoDrop.hidden = YES;
    if (![tableType isEqualToString:kBEST_OF_3] &&
        ![tableType isEqualToString:kBEST_OF_2])
    {
        self.dropButton.hidden = NO;
        self.dropButton.enabled = NO;
        //self.dropButton.backgroundColor = [UIColor grayColor];
        [self hideAutoDrop];
    }
    else
    {
        self.dropButton.hidden = NO;
        self.dropButton.enabled = NO;
        [self hideAutoDrop];
    }
    
    if([tableType isEqualToString:kBEST_OF_3] ||
       [tableType isEqualToString:kBEST_OF_2])
    {
        self.isBestOfTwoOrThreeTypeGame = YES;
    }
    
    if ([tableType isEqualToString:kPR_JOKER]
        ||[tableType isEqualToString:kPR_NOJOKER])
    {
        NSLog(@"kPR_JOKER");
        
        self.isJokerTypeGame = YES;
        self.jokerMinBuyin = [[self.playTableDictionary valueForKeyPath:kTaj_Table_min_buyin] integerValue];
    }
    if ([tableType isEqualToString:kBEST_OF_6])
    {
        self.isBestOfSixTypeGame = YES;
    }
    
    if (self.meldViewTable)
    {
        [self removeMeldView];
    }
    
    if (self.smartCorrectionView)
    {
        [self removeSmartCorrectionView];
    }
    
    NSString *hideExtraTimeButton = [self.playTableDictionary valueForKeyPath:kTaj_Table_extraTime];
    
    if ([hideExtraTimeButton isEqualToString:@"True"])
    {
        self.extraTimeButton.hidden = YES;//recently changed
        //self.extraTimeButton.hidden = NO;
        //self.extraTimeBg.hidden = NO;
        self.extraTimeBg.hidden = YES;//ratheesh
    }
    else
    {
        if (self.isSpectator)
        {
            self.extraTimeButton.hidden = YES;
            self.extraTimeBg.hidden = YES;
        }
    }
    
    NSString *gameStarted;
    gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
    if ([gameStarted isEqualToString:@"True"])
    {
        self.isGameScheduleState = YES;
        self.isSendDealCame = YES;
    }
    
    NSString *isMiddleJoin = [self.playTableDictionary valueForKeyPath:kTaj_Table_middleJoin];
    if ([isMiddleJoin isEqualToString:@"False"])
    {
        self.canMiddleJoin = NO;
    }
    else
    {
        self.canMiddleJoin = YES;
    }
    
    self.playerModelButton.hidden = YES;
    self.didSendMeldCards = NO;
    
    [self updateSortButton:NO];
    [self updateExtraTimeButton:NO];
    self.showButton.enabled = NO;
    [self updateShowButton:NO];
    
    
    // Setup Table
    [self loadTableUI];
    // Setup chairs
    [self loadAllChairs];
}

- (void)createViewForGamePlay
{
    // setup views for game play
    [self createPlayerModelView];
    [self createScoreBoard];
    [self createChatViewController];
    [self createLiveFeedController];
    [self createDiscardCardHistoryView];
    [self createViewForAutoPlay];
    
    if ([[self.playTableDictionary valueForKeyPath:GAME_TABLE_IS_SPECATOR] isEqualToString:@"True"])
    {
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeExtraMessagePlayerMode];
        }
        [self.playTable removeExtraMessage];
    }
    else
    {
        
    }
}

-(void)setDefault {
    self.imgViewAutoDrop.image = [UIImage imageNamed:@"Checkbox_Box"];
    self.isAutoDrop = NO;
}

-(void)setAutoDrop {
    self.imgViewAutoDrop.image = [UIImage imageNamed:@"Checkbox_Tick"];
    self.isAutoDrop = YES;
}

#pragma mark - UI Button Interation -

- (IBAction)closedBugReportClicked:(UIButton *)sender {
    self.viewReportBug.hidden = YES;
}

- (IBAction)autoDropClicked:(UIButton *)sender {
    if (self.isAutoDrop == YES) {
        self.isAutoDrop = NO;
        [self setDefault];
    }
    else {
        self.isAutoDrop = YES;
        [self setAutoDrop];
    }
}

- (void)autoDrop {
    NSLog(@"AUTO DROP CHECKING");
    [[TAJGameEngine sharedGameEngine] playerDropEventWithNickName:self.thisPlayerModel.playerName
                                                          tableID:self.currentTableId
                                                           userID:self.thisPlayerModel.playerID];
    
    self.dropButton.enabled = NO;
    [self.playTable dropGameSetup];
    self.dropButtonAlertView = nil;
}

- (void)updateSortButton:(BOOL)boolValue
{
    self.sortButton.enabled = boolValue;
}

- (void)updateExtraTimeButton:(BOOL)boolValue
{
    if(self.isExtraTimeButtonClicked)
    {
        self.extraTimeButton.enabled = NO;
    }
    else
    {
        self.extraTimeButton.enabled = NO;
        //self.extraTimeButton.enabled = boolValue;
    }
}

- (void)updateShowButton:(BOOL)boolValue
{
    self.showButton.enabled = boolValue;
}

- (void)updateDropButton
{
    if (self.isMyTurn &&
        self.myCardsDealArray.count == 13)
    {
        if (!self.isBestOfTwoOrThreeTypeGame)
        {
            self.dropButton.enabled = YES;
        }
    }
    else
    {
        self.dropButton.enabled = NO;
    }
}

- (void)updateDropButtonWhenTurnTimeZero
{
    if (self.isMyTurn &&
        self.myCardsDealArray.count == 13)
    {
        self.dropButton.enabled = NO;
    }
}

- (void)updateFlipCardButton:(BOOL)boolValue
{
    self.flipCardsButton.enabled = boolValue;
}

#pragma mark - GAME INFO VIEW UPDATE FUNCTIONS -

- (void)updateGameInfo:(NSDictionary *)dictionary
{
    self.savedFirstTableID = [[NSUserDefaults standardUserDefaults] objectForKey:FIRSTTABLEID];
    self.savedSecondTableID = [[NSUserDefaults standardUserDefaults] objectForKey:SECONDTABLEID];
    NSLog(@"savedFirstTableID : %@",_savedFirstTableID);
    NSLog(@"savedSecondTableID : %@",_savedSecondTableID);
    
    NSLog(@"Game INFO : %@",dictionary);
    
    TAJGameEngine *engine = NULL;
    engine = [TAJGameEngine sharedGameEngine];
    
    self.usernameLabel.text = [[NSString stringWithFormat:USER_NAME_LABEL  ,
                                engine.usermodel.displayName] lowercaseString];
    
    self.tableIDLabel.text =  [NSString stringWithFormat:GAME_TABLE_ID_LABEL ,
                               [dictionary valueForKeyPath:GAME_TABLE_ID_VALUE ]];
    self.gameTypeLabel.text = [[TAJUtilities sharedUtilities] getProperNameForTableType:[dictionary valueForKeyPath:GAME_TABLE_TYPE_VALUE]];
    [self.gameTypeLabel adjustsFontSizeToFitWidth];
    
    if([[dictionary valueForKeyPath:GAME_TABLE_COST_TYPE] isEqualToString:CASH_GAMES])
    {
        
        NSString *cashText = [NSString stringWithFormat:GAME_TABLE_BET_LABEL ,[dictionary valueForKeyPath:GAME_TABLE_BET_VALUE]];
        
        self.tableBetLabel.text = [cashText stringByAppendingString:CASH_TEXT];
        //self.tableBetLabel.text = cashText;
        [self.tableBetLabel adjustsFontSizeToFitWidth];
        
        self.balanceLabel.text = self.realChipsValue;
        
    }
    else if([[dictionary valueForKeyPath:GAME_TABLE_COST_TYPE] isEqualToString:FREE_GAMES])
    {
        NSString *cashText = [NSString stringWithFormat:GAME_TABLE_BET_LABEL ,[dictionary valueForKeyPath:GAME_TABLE_BET_VALUE]];
        
        self.tableBetLabel.text = [cashText stringByAppendingString:FREE_TEXT];
        //self.tableBetLabel.text = cashText;
        [self.tableBetLabel adjustsFontSizeToFitWidth];
        
        self.balanceLabel.text = self.funChipsValue;
    }
    
    if (dictionary)
    {
        NSDictionary *extraGameInfo = [dictionary valueForKeyPath:GAME_TABLE_PRIZE_INFO];
        [self updateGameIDAndPrize:extraGameInfo];
        
        if(isTournament){
            //Tournament
            self.lblTourneyID.text = @"";
            self.lblTourneyGameID.text = [NSString stringWithFormat:@"%@",[dictionary valueForKeyPath:GAME_ID_VALUE]];
            self.lblTourneyType.text = [NSString stringWithFormat:@"%@",[dictionary valueForKeyPath:GAME_TABLE_TYPE_VALUE]];
            self.lblTourneyEntry.text = @"";
            self.lblTourneyBet.text = @"";
            self.lblTourneyRebuy.text = @"";
            self.lblTourneyRank.text = @"";
            self.lblTourneyBalance.text = @"";
            self.lblTourneyPrize.text = [NSString stringWithFormat:GAME_PRIZE_LABEL ,
                                         [[dictionary valueForKeyPath:GAME_PRIZE_MONEY_VALUE] floatValue]];
            self.lblTourneyLevel.text = @"";
        }
        
    }
    
    
    if ([self.savedFirstTableID isEqualToString:[dictionary valueForKey:@"TAJ_table_id"]]) {
        self.firstGameBet = [dictionary valueForKeyPath:GAME_TABLE_BET_VALUE];
    }
    else {
        self.secondGameBet = [dictionary valueForKeyPath:GAME_TABLE_BET_VALUE];
    }
}

- (void)updateGameIDAndPrize:(NSDictionary*)dictonary
{
    NSLog(@"GAME ID INFO : %@",dictonary);
    
    if (dictonary)
    {
        self.rupeesSymbolLabel.hidden = YES;
        self.betRupeesSymbol.hidden = YES;
        //self.rupeesSymbolLabel.hidden = NO;
        //self.betRupeesSymbol.hidden = NO;
        
        self.gameIDLabel.text = [NSString stringWithFormat:GAME_ID_LABEL ,
                                 [dictonary valueForKeyPath:GAME_ID_VALUE]];
        
        self.gameIDString = [NSString stringWithFormat:@"%@",
                             [dictonary valueForKeyPath:GAME_ID_VALUE]];
        self.tableIDString = [NSString stringWithFormat:@"%@",[dictonary valueForKey:@"TAJ_table_id"]];
        
        self.prizeLabel.text =  [NSString stringWithFormat:GAME_PRIZE_LABEL ,
                                 [[dictonary valueForKeyPath:GAME_PRIZE_MONEY_VALUE] floatValue]];
        isTournament = [[self.playTableDictionary valueForKeyPath:@"TAJ_tournament_table"] boolValue];
    }
    else
    {
        self.gameIDLabel.text = @"";
        self.prizeLabel.text = @"";
        self.rupeesSymbolLabel.hidden = YES;
        // self.betRupeesSymbol.hidden = YES;
    }
}

#pragma mark - Table and all Chair setup -

- (void)loadTableUI
{
    if (!self.playTable)
    {
        NSArray *nibContents;
        // table not available
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = 0;//ReDIM Changes [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJPlayTableViewNibName
                                                    owner:nil
                                                  options:nil];
        self.playTable = nibContents[index];
        [TAJAppDelegate setScreenSize : self.playTable fromView : self.playTableHolderView];
        [self.playTableHolderView addSubview:self.playTable];
        [self.playTable initialiseAllUIWith:self.playTableDictionary];
        
        // Set this controller as delegate
        self.playTable.delegate = self;
        
    }
}

- (void)loadAllChairs
{
    isTournament = [[self.playTableDictionary valueForKeyPath:@"TAJ_tournament_table"] boolValue];
    
    if(isTournament){
        runningStatusView.hidden = NO;
        /*self.viewTourneyDetails.hidden = NO;
        self.viewGameDetails.hidden = YES;
        */
//        runningStatusView.hidden = YES;
//        self.viewTourneyDetails.hidden = YES;
//        self.viewGameDetails.hidden = NO;
        tourneyId = [[TAJGameEngine sharedGameEngine] getTournamentID];
        [[TAJGameEngine sharedGameEngine] requestGetLevelTimer:tourneyId msgUUID:[[TAJUtilities sharedUtilities] getTheUUID]];
    } else {
        runningStatusView.hidden = YES;
       /* self.viewTourneyDetails.hidden = YES;
        self.viewGameDetails.hidden = NO;
       */
//        runningStatusView.hidden = NO;
//        self.viewTourneyDetails.hidden = NO;
//        self.viewGameDetails.hidden = YES;
    }
    
    //newly added.
    if(self.quitPlayersId.count > 0)
    {
        [self.quitPlayersId removeAllObjects];
    }
    
    // Get max player in table
    [self setupChairForPayers:self.maxPlayers];
    // update all player status for spectator
    [self playersStatusForSpectator:self.playTableDictionary];
    [self performSelector:@selector(checkPlayerDropped:) withObject:self.playTableDictionary afterDelay:0.8f];
    if ([[self.playTableDictionary valueForKeyPath:kTaj_Table_middleJoin] isEqualToString:@"False"])
    {
        self.canMiddleJoin = NO;
    }
    
    NSString *gameStarted ,*gameCount;
    gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
    gameCount = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamecount];
    
    DLog(@"game start  %@ , %@" ,gameStarted,gameCount);
    
    if ([gameStarted isEqualToString:@"True"] && [gameCount intValue] >= 1)
    {
        [self disableChairs];
    }
}

- (void)setupChairForPayers:(NSUInteger) totalPlayers
{
    if (!self.chairsArray)
    {
        return;
    }
    
    // Enable chairs
    for (int i = 0; i < totalPlayers && i < self.chairsArray.count; i++)
    {
        UIView* chair = [self.chairsArray objectAtIndex:i];
        chair.hidden = NO;
    }
    
    [self fillPlayer:nil];
    
    // Fill up chairs if any player is already sitting
    [self fillChairs];
    
    if(!isTournament){
#if DEBUG
        NSLog(@"sit here Action");
#endif
        [self sitHereAction];
    }
    
}

- (void)sitHereAction {
    if([self getEmptyChair] != nil){
        TAJChairView *result = [self getEmptyChair];
        [self didSelectChair:result];
    }
}

- (void)fillChairs
{
    if (self.playTableDictionary &&
        [[self.playTableDictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
    {
        // Only 1 player available
        NSMutableDictionary* onePlayerDict = [self.playTableDictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        [onePlayerDict setObject:self.currentTableId forKey:TAJ_TABLE_ID];
        
        [self fillPlayer:onePlayerDict];
    }
    else if (self.playTableDictionary &&
             [[self.playTableDictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
    {
        // Get the players array
        NSMutableArray *playersArray = [self.playTableDictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        
        // More than 1 player data available
        for (int i = 0; i < playersArray.count; i++)
        {
            NSMutableDictionary *playerDictionary = [playersArray objectAtIndex:i];
            [playerDictionary setObject:self.currentTableId forKey:TAJ_TABLE_ID];
            
            [self fillPlayer:playerDictionary];
        }
        
        if ([[self.playTableDictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] isKindOfClass:[NSArray class]] &&  [self.playTableDictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST])
        {
            NSMutableArray *droppedPlayers = [self.playTableDictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST];
            for (int i = 0; i < droppedPlayers.count; i++)
            {
                NSMutableDictionary *allPlayerDropped = droppedPlayers[i];
                BOOL isPlayerAlreadyExistsInPlayersList= NO;
                
                for (int index=0;index < playersArray.count; index++)
                {
                    if ([playersArray[index][kTaj_userid_Key] isEqualToString:allPlayerDropped[kTaj_userid_Key]])
                    {
                        isPlayerAlreadyExistsInPlayersList = YES;
                    }
                }
                if (!isPlayerAlreadyExistsInPlayersList)
                {
                    if ([allPlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:DROP_TEXT] || [allPlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:ELIMINATE_TEXT] || [allPlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:TIMEOUT_TEXT])
                    {
                        [allPlayerDropped setObject:TABLE_LEAVE_TEXT forKey:GAME_DEFFERENT_SCREEN_PLAYERTYPE];
                    }
                    [self fillDroppedPlayer:allPlayerDropped];
                }
            }
        }
        else if ([[self.playTableDictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] isKindOfClass:[NSDictionary class]] && [self.playTableDictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] )
        {
            NSMutableDictionary *singlePlayerDropped = [self.playTableDictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST];
            BOOL isPlayerAlreadyExistsInPlayersList= NO;
            
            for (int index=0;index < playersArray.count; index++)
            {
                if ([playersArray[index][kTaj_userid_Key] isEqualToString:singlePlayerDropped[kTaj_userid_Key]])
                {
                    isPlayerAlreadyExistsInPlayersList = YES;
                }
            }
            if (!isPlayerAlreadyExistsInPlayersList)
            {
                if ([singlePlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:DROP_TEXT] || [singlePlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:ELIMINATE_TEXT]||[singlePlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:TIMEOUT_TEXT])
                {
                    [singlePlayerDropped setObject:TABLE_LEAVE_TEXT forKey:GAME_DEFFERENT_SCREEN_PLAYERTYPE];
                }
                
                [self fillDroppedPlayer:singlePlayerDropped];
            }
            //            DLog(@"dropped player %@",singlePlayerDropped);
        }
        
        NSString *gameStarted ,*gameCount;
        gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
        gameCount = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamecount];
        
        if (self.startTimerDuration <= 0 && self.isJokerTypeGame  )
        {
            //[self disableChairs];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeInfoLabelMessagePlayerMode];
            }
            [self.playTable removeInfoLabelMessage];
        }
        else if(!self.isBestOfSixTypeGame)
        {
            [self disableChairs];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeInfoLabelMessagePlayerMode];
            }
            [self.playTable removeInfoLabelMessage];
        }
    }
    else
    {
        //No Player joined yet
        [self fillPlayer:nil];
    }
    [self fillMiddleJoinedPlayers:self.playTableDictionary];
    
}

- (void)fillPlayer:(NSDictionary *)playerInfoDictionary
{
    if ([TAJUtilities isIPhone])
    {
        self.avatarImageForIAmBack = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
        
    }
    else
    {
        self.avatarImageForIAmBack = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
    }
    
    if (playerInfoDictionary)
    {
        int chairID = [playerInfoDictionary[kTaj_Place_Key] integerValue];
        chairID = (self.maxPlayers == TWO_PLAYER && chairID == 4) ? 2 : chairID;
        chairID = chairID - 1;
        
        UIView *view = self.chairsArray[chairID];
        NSArray *subviews = [view subviews];
        TAJChairView *aChair = (TAJChairView *)subviews[0];
        
        // Init player model also
        TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:playerInfoDictionary];
        [aChair updateChairWithData:model];
        
        if ([self isThisDevicePlayer:view])
        {
            self.thisDevicePlayer = view;
            
            // Set user Id also
            TAJChairView *chair = [self chairFromView:view];
            self.thisPlayerModel = chair.playerModel;
            self.isSpectator = NO;
            self.playTableHolderView.userInteractionEnabled = YES;
            
            [self.playTable searchPlayerJoinTable:self.playTableDictionary isThisDevicePlayer:YES];
            
            [self disableChairs];
            NSString *welcomeImageName;
            // set image for this device player in welcome screen
            if ([TAJUtilities isIPhone])
            {
                welcomeImageName = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",chair.playerModel.playerGender,chair.playerModel.playerCharNumber];
            }
            else
            {
                welcomeImageName = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",chair.playerModel.playerGender,chair.playerModel.playerCharNumber];
            }
            self.devicePlayerAvatarImageView.image = [UIImage imageNamed:welcomeImageName];
            self.devicePlayerAvatarImageView.layer.cornerRadius = 4.0f;
            // Updated to user model to know the player image name
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            TAJUserModel *model = engine.usermodel;
            [model updateThisDeviceImageName:welcomeImageName];
        }
    }
    else
    {
        for (int i = 0; i < self.maxPlayers; i++)
        {
            UIView *view = self.chairsArray[i];
            TAJChairView *aChair = nil;
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                aChair = (TAJChairView *)subviews[0];
            }
            else
            {
                NSArray *nibContents;
                // Players not available
                int index = 0;
                
                if ([TAJUtilities isIPhone])
                {
                    index = [TAJUtilities isItIPhone5]? 0 : 1;
                }
                
                nibContents = [[NSBundle mainBundle] loadNibNamed:TAJChairViewNibName
                                                            owner:nil
                                                          options:nil];
                aChair = nibContents[index];
                [view addSubview:aChair];
                aChair.delegate = self;
                aChair.playerModel =nil;
                
            }
            [aChair createNewEmptyChairWithChairID:i+1];
        }
    }
}

- (void)fillMiddleJoinedPlayers:(NSMutableDictionary *)dictionary
{
    NSMutableArray *middleJoinedPlayers =[NSMutableArray array];
    if (dictionary)
    {
        if ([[dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST] isKindOfClass:[NSArray class]] && [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST])
        {
            NSMutableArray *tempMiddleJoinedPlayersArray = [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST];
            for (int i = 0; i < tempMiddleJoinedPlayersArray.count; i++)
            {
                NSMutableDictionary *singlePlayer = tempMiddleJoinedPlayersArray[i];
                [middleJoinedPlayers addObject:singlePlayer];
            }
        }
        else if ([[dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST] isKindOfClass:[NSDictionary class]]  && [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST])
        {
            NSMutableDictionary *singlePlayer = [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST];
            [middleJoinedPlayers addObject:singlePlayer];
        }
    }
    
    // add middle joined player to table
    for(int index=0;index <middleJoinedPlayers.count; index++)
    {
        int chairID = [middleJoinedPlayers[index][kTaj_Place_Key] integerValue];
        chairID = chairID - 1;
        
        UIView *view = self.chairsArray[chairID];
        NSArray *subviews = [view subviews];
        TAJChairView *aChair = (TAJChairView *)subviews[0];
        
        // Init player model also
        TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:middleJoinedPlayers[index]];
        [aChair updateChairWithData:model];
        [aChair createNewEmptyChairWithChairID:chairID];
        aChair.isMiddleJoin = YES;
        if (aChair.playerModel)
        {
            aChair.playerModel.placeID = [NSString stringWithFormat:@"%d",chairID+1];
        }
        [view addSubview:aChair];
        DLog(@"middle joined player chair %d",chairID);
    }
}

- (void)fillDroppedPlayer:(NSMutableDictionary *)playerInfoDictionary
{
    
    if ( playerInfoDictionary)
    {
        //        DLog(@"filldroppedplayer");
        int chairID = [playerInfoDictionary[kTaj_Place_Key] integerValue];
        NSString* userID = playerInfoDictionary[kTaj_userid_Key] ;
        //        chairID = (self.maxPlayers == TWO_PLAYER && chairID == 4) ? 2 : chairID;
        chairID = chairID - 1;
        self.thisChairID = [NSString stringWithFormat:@"%d",chairID];
        NSString* thisUserID=[[[TAJGameEngine sharedGameEngine] usermodel] userId];
        if([userID isEqualToString:thisUserID])
        {
            UIView *view = self.chairsArray[chairID];
            NSArray *subviews = [view subviews];
            TAJChairView *aChair = (TAJChairView *)subviews[0];
            [playerInfoDictionary setObject:@"table_leave" forKey:GAME_DEFFERENT_SCREEN_PLAYERTYPE];
            
            // Init player model also
            TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:playerInfoDictionary];
            [aChair updateChairForDroppedPlayerWithData:model];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeInfoLabelMessagePlayerMode];
            }
            [self.playTable removeInfoLabelMessage];
            UIImageView *playerCardImageViewNew;
            playerCardImageViewNew = self.dropCardImageViewArray[chairID];
            // playerCardImageView.hidden = NO;
            if ([TAJUtilities isIPhone])
            {
                playerCardImageViewNew.image = self.dropImageArray[chairID];// [UIImage imageNamed:@"card_back-568@2x~iphone.png"];
            }
            else
            {
                playerCardImageViewNew.image = self.dropImageArray[chairID];
            }
            playerCardImageViewNew.hidden = NO;
            
            if ([self isThisDevicePlayer:view])
            {
                self.thisDevicePlayer = view;
                self.isSpectator = YES;
                [self disableChairs];
            }
        }
        else
        {
            [self fillDroppedPlayerOnReconnect:playerInfoDictionary];
        }
    }
}


- (void)playersStatusForSpectator:(NSDictionary *)dictionary
{
    if (dictionary)
    {
        // place dealer image
        if ([dictionary valueForKeyPath:GAME_TABLE_DEALER_ID])
        {
            [self dealerImageForPlayer:[dictionary valueForKeyPath:kTaj_Table_details]];
        }
        
        // place auto play label
        if ([[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
        {
            NSMutableArray *autoPlayPlayers = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
            NSString *zeroValueString = @"0";
            
            for (int i = 0; i < autoPlayPlayers.count; i++)
            {
                NSMutableDictionary *singlePlayerAutoPlay = autoPlayPlayers[i];
                [singlePlayerAutoPlay setObject:zeroValueString forKey:kTaj_autoPlay_cardscount];
                [singlePlayerAutoPlay setObject:zeroValueString forKey:kTaj_autoPlay_totalCount];
                
                [self prepareAutoPlayViewForTurnUpdate:singlePlayerAutoPlay];
            }
        }
        else if ([[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *singlePlayerAutoPlay = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
            
            [self prepareAutoPlayViewForTurnUpdate:singlePlayerAutoPlay];
        }
    }
}

#pragma mark - IBAction methods -

- (IBAction)quitGameAction:(UIButton *)sender
{
    [self.settingMenuListView removeFromSuperview];
    self.isMenuListShowing=NO;
    [self leaveTable];
}

- (void)showNoInternetConnectionAlert
{
    
}

#if NEW_INFO_VIEW_IMPLEMENTATION

- (void)yesButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_CAN_LEAVE_TAG:
        {
            DLog(@"Button YES was selected.");
            if (self.playTableDictionary)
            {
                [self quitGame];
            }
            
            [self.leaveAlertView hide];
            self.leaveAlertView = nil;
        }
            break;
            
        case ALERT_SHOW_BUTTON_TAG:
        {
            [self.showButtonAlertView hide];
            self.showButtonAlertView = nil;
            [self.showButton setTitle: @"DECLARE" forState: UIControlStateNormal];
            [self placeShow];
            // [self handleSmartCorrectionEvent:nil];
            NSLog(@"SHOW SUCCESS");
            //[self saveToCoreDataWithEvent:@"Show"];
        }
            
        case ALERT_DROP_BUTTON_TAG:
        {
            
            [[TAJGameEngine sharedGameEngine] playerDropEventWithNickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId
                                                                   userID:self.thisPlayerModel.playerID];
            
            self.dropButton.enabled = NO;
            [self.playTable dropGameSetup];
            [self.dropButtonAlertView hide];
            self.dropButtonAlertView = nil;
        }
            break;
            
            //added ratheesh
        case INSUUFICIENT_CHIPS_TAG:
        {
            
            
            [self.noHandAlertView hide];
            self.noHandAlertView = nil;
            
            [self leaveTable];
            //            NSString * url = [NSString stringWithFormat:@"%@%@",BASE_URL,SENDPAYMENTREQUEST];
            //            UIApplication *application = [UIApplication sharedApplication];
            //            NSURL *URL = [NSURL URLWithString:url];
            //            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            //                if (success) {
            //                    NSLog(@"Opened url : %@",url);
            //                }
            //            }];
            
            //            TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
            //
            //#if NEW_DESIGN_IMPLEMENTATION
            //            [delegate loadPrelobbyScreen];
            //#else
            //            [delegate loadHomePageWindow];
            //#endif
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"closePopUp" object:nil];
            
        }
            break;
            //added ratheesh
            
        case ALERT_CAN_NOT_REJOIN_PLAY_TAG:
        {
            NSLog(@"CHECKING RE JOIN");
            // if this player exist to join new table
            // for asking do you want to continue game once he loss
            
            [self removeScoreSheetView];
            
            [self removeWinnerScreenView];
            [self refreshChairView];
            [self.searchJoinAlertView hide];
            self.searchJoinAlertView = nil;
            
            // player is requested to join new table
            self.isRequestedNewTable = YES;
            
            // VERY IMPORTANT: call delegate after self.isRequestedNewTable = YES
            //APP_DELEGATE.gameTableIndex = 2;
            if ([self.gameTableDelegate respondsToSelector:@selector(newTableRequested:)])
            {
                [self.gameTableDelegate newTableRequested:[self.currentTableId integerValue]];
            }
            
            NSString *uuID = Nil;
            uuID = [[TAJUtilities sharedUtilities] getTheUUID];
            
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            
            switch (engine.lobby.runningTableType)
            {
                case eOpenTableFirst:
                    
                    engine.lobby.firstTable.msgUUIDForSearchJoinTable = uuID;
                    break;
                    
                case eOpenTableSecond:
                    
                    engine.lobby.secondTable.msgUUIDForSearchJoinTable = uuID;
                    break;
                    
                default:
                    break;
            }
            
            [self removeAllNotifications];
            
            
            [[TAJGameEngine sharedGameEngine] searchJoinTableWithUserID:self.thisPlayerModel.playerID
                                                               nickName:self.thisPlayerModel.playerName
                                                                    bet:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_BET]
                                                             maxplayers:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_MAXPLAYER]
                                                              tableType:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_TABLETYPE]
                                                              tableCost:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_CASH_TYPE]
                                                                tableID:self.currentTableId
                                                             conversion:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_CONVERSION]
                                                             streamName:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_STREAM_NAME]
                                                               streamID:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_STREAM_ID]
                                                         gameSettingsID:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_GAME_SETTING_ID]
                                                                msgUUID:uuID];
            
            
            
        }
            break;
            
        case ALERT_REBUYIN_JOKER_TAG:
        {
            [self removeScoreSheetView];
            
            
            if (!self.jokerPopTableView)
            {
                NSLog(@"CHECK JOCKER 1926");
                [self createJokerPopTableView];
                //NSLog(@"GAME TYPE : %@",self.playTableDictionary);
                
                [self.jokerPopTableView updateJokerView:self.playTableDictionary[kTaj_Table_details] withBet:[self.buyInAmount intValue] gameType:APP_DELEGATE.gameType];
            }
            self.leaveButton.enabled = NO;
            [self.rebuyAlertView hide];
            self.rebuyAlertView = nil;
        }
            break;
            
            
        case ALERT_CAN_REJOIN_PLAY_TAG:
        {
            // if this player exist to rejoin same table
            // for asking do you want to rejoin the same table once again once he loss
            
            // if click on OK
            
            // if NSTimer is running
            [self.rejoinNSTimer invalidate];
            [self.rejoinAlertView hide];
            self.rejoinAlertView = nil;
            
#if NEW_INFO_VIEW_IMPLEMENTATION
            [self.rejoinAlertView hide];
#endif
            self.rejoinAlertView = nil;
            
            //            self.rejoinTime = 0;
            [self vibrateDevice];
            
            [[TAJGameEngine sharedGameEngine] rejoinReplyYesWithUserID:self.thisPlayerModel.playerID
                                                              nickName:self.thisPlayerModel.playerName
                                                               tableID:self.currentTableId
                                                               msgUUID:self.rejoinMsgUUid];
            
        }
            break;
            
        case ALERT_SPLIT_REQUEST_TIMER_TAG:
        {
            [[TAJGameEngine sharedGameEngine] splitAcceptReplyWithMsgUUID:self.splitMsgUUid
                                                                   userID:self.thisPlayerModel.playerID
                                                                 nickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId];
            [self.splitRequestAlertView hide];
            self.splitRequestAlertView = nil;
            [self.playTable removeOpenDeckHolderCards];
            [self disableChairs];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showInstructionPlayerMode:SPLIT_REQUESTED_MSG];
            }
            [self.playTable showInstruction:SPLIT_REQUESTED_MSG];
        }
            break;
        default:
            break;
    }
}

- (void)noButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_CAN_LEAVE_TAG:
        {
            DLog(@"Button NO was selected.");
            // at last
            [self.leaveAlertView hide];
            self.leaveAlertView = nil;
        }
            break;
            
            //added ratheesh
        case INSUUFICIENT_CHIPS_TAG:
        {
            [self.noHandAlertView hide];
            self.noHandAlertView = nil;
            
            [self leaveTable];
            
        }
            break;
            //added ratheesh
            
        case ALERT_SHOW_BUTTON_TAG:
        {
            // for show
            [self.showButtonAlertView hide];
            self.showButtonAlertView = nil;
        }
            break;
            
        case ALERT_DROP_BUTTON_TAG:
        {
            [self.dropButtonAlertView hide];
            self.dropButtonAlertView = nil;
        }
            break;
            
        case ALERT_CAN_NOT_REJOIN_PLAY_TAG:
        {
            // if this player exist to join new table
            // for asking do you want to continue game once he loss
            [self.searchJoinAlertView hide];
            self.searchJoinAlertView = nil;
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView performSelector:@selector(showInstructionPlayerMode:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.0];
            }
            [self.playTable performSelector:@selector(showInstruction:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.0];
            
            [self.playTable removeOpenDeckHolderCards];
            for(int i=0; i<6; i++)
            {
                UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
                UIImageView *imageView2= self.playerCardsBgImage[i];
                imageView1.hidden = YES;
                imageView1.image = nil;
                imageView2.hidden = YES;
                imageView2.image = nil;
            }
            [self refreshChairView];
            [self performSelector:@selector(hideChairView) withObject:nil afterDelay:1.2f];
            //[self disableChairs];
        }
            break;
            
        case ALERT_REBUYIN_JOKER_TAG:
        {
            
            self.isRebuyin = NO;
            // at last
            [self.rebuyAlertView hide];
            self.rebuyAlertView = nil;
        }
            break;
            
        case ALERT_CAN_REJOIN_PLAY_TAG:
        {
            // if click on CANCEL
            [self refreshChairView];
            [self cancelRejoin];
        }
            break;
            
        case ALERT_SPLIT_REQUEST_TIMER_TAG:
        {
            [[TAJGameEngine sharedGameEngine] splitRejectReplyWithMsgUUID:self.splitMsgUUid
                                                                   userID:self.thisPlayerModel.playerID
                                                                 nickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId];
            [self.splitRequestAlertView hide];
            self.splitRequestAlertView = nil;
            [self disableChairs];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showInstructionPlayerMode:SPLIT_REQUESTED_MSG];
            }
            
            [self.playTable showInstruction:SPLIT_REQUESTED_MSG];
            
        }
            break;
            
        default:
            break;
    }
}

- (void)okButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    
    switch (conroller.tag)
    {
        case ALERT_CAN_LEAVE_TAG:
            
            break;
            
        case ALERT_CAN_NOT_LEAVE_TAG:
        {
            DLog(@"Button OK was selected.");
            // at last
            [self.cantLeaveAlertView hide];
            self.cantLeaveAlertView = nil;
        }
            break;
            
        case ALERT_NO_LAST_HAND_TAG:
        {
            [self.noHandAlertView hide];
            self.noHandAlertView = nil;
        }
            break;
            //added ratheesh
        case INSUUFICIENT_CHIPS_TAG:
        {
            [self.noHandAlertView hide];
            self.noHandAlertView = nil;
            //https://www.tajrummy.com/sendpaymentrequest/ - PURCHASE_URL
            
            [self leaveTable];
            
            //            UIApplication *application = [UIApplication sharedApplication];
            //            NSURL *URL = [NSURL URLWithString:PURCHASE_URL];
            //            [application openURL:URL options:@{} completionHandler:^(BOOL success) {
            //                if (success) {
            //                    NSLog(@"Opened url");
            //                }
            //            }];
        }
            break;
            //added ratheesh
            
        case ALERT_NO_LAST_ENTRIES_TAG:
        {
            [self.noEntryAlertView hide];
            self.noEntryAlertView = nil;
        }
            break;
            
        case ALERT_RESHUFFLE_TAG:
        {
            [self.reshuffleAlertView hide];
            self.reshuffleAlertView = nil;
        }
            break;
            
        case ALERT_SPLIT_REJECT_TAG:
        {
            [self.splitRejectAlertView hide];
            self.splitRejectAlertView = nil;
        }
            break;
        case ALERT_SHOW_BUTTON_TAG:
        {
            [self.showButtonAlertView hide];
            self.showButtonAlertView = nil;
            [self.showButton setTitle: @"DECLARE" forState: UIControlStateNormal];
            [self placeShow];
            NSLog(@"SHOW SUCCESS");
            if ([self.savedFirstTableID isEqualToString:self.tableIDString]) {
                //[self saveToCoreDataWithEvent:@"Show"];
            }
            else {
                //[self saveToCoreDataWithSecondTableEvent:@"Show"];
            }
            // [self handleSmartCorrectionEvent:nil];
            
        }
            break;
            
        case ALERT_CAN_REJOIN_PLAY_TAG:
        {
            // if this player exist to rejoin same table
            // for asking do you want to rejoin the same table once again once he loss
            
            // if click on OK
            
            // if NSTimer is running
            [self.rejoinNSTimer invalidate];
            [self.rejoinAlertView hide];
            self.rejoinAlertView = nil;
            
#if NEW_INFO_VIEW_IMPLEMENTATION
            [self.rejoinAlertView hide];
#endif
            self.rejoinAlertView = nil;
            
            // self.rejoinTime = 0;
            [self vibrateDevice];
            
            [[TAJGameEngine sharedGameEngine] rejoinReplyYesWithUserID:self.thisPlayerModel.playerID
                                                              nickName:self.thisPlayerModel.playerName
                                                               tableID:self.currentTableId
                                                               msgUUID:self.rejoinMsgUUid];
            
        }
            break;
            
        case  ALERT_LEAVE_JOKER_TABLE_TAG:
        {
            if (self.playTableDictionary)
            {
                NSLog(@"playTableDictionary : %@",_playTableDictionary);
                
                [self quitGame];
                NSLog(@"ELIMINATED");
                if ([self.savedFirstTableID isEqualToString:[self currentTableId]]) {
                //[self saveToCoreDataWithEvent:@"Player Eliminate"];
                }
                else {
                  //  [self saveToCoreDataWithSecondTableEvent:@"Player Eliminate"];
                }
            }
            
            // at last
            [self.leaveForJokerAlertView hide];
            self.leaveForJokerAlertView = nil;
        }
            break;
        case ALERT_MELD_INVALID:
            [self.infoPopUpController hide];
            self.infoPopUpController = Nil;
            break;
        case ALERT_ERROR:
            [self.errorInfoPopupViewController hide];
            self.errorInfoPopupViewController = Nil;
            break;
            
        case ALERT_REBUYIN_JOKER_TAG:
        {
            [self removeScoreSheetView];
            
            
            if (!self.jokerPopTableView)
            {
                NSLog(@"CHECK JOCKER 2056");
                [self createJokerPopTableView];
                //NSLog(@"GAME TYPE : %@",self.playTableDictionary);
                [self.jokerPopTableView updateJokerView:self.playTableDictionary[kTaj_Table_details] withBet:[self.buyInAmount intValue] gameType:APP_DELEGATE.gameType];
            }
            self.leaveButton.enabled = NO;
            [self.rebuyAlertView hide];
            self.rebuyAlertView = nil;
        }
            break;
            
            
        case ALERT_JOKER_CREATE_TAG:
        {
            //[self.jokerJoinAlertView hide];
            //self.jokerJoinAlertView = nil;
            
            [self leaveTable];//ratheesh
#if DEBUG
            NSLog(@"CHECKING BACKK");
#endif
        }
            break;
        case TOURNAMENT_ELEMINATE_TAG:
        {
            [self.tourneyEleminateAlertView hide];
            self.tourneyEleminateAlertView = nil;
        }
            break;
        default:
            break;
    }
}

- (void)closeButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_CAN_LEAVE_TAG:
        {
            DLog(@"Button NO was selected.");
            // at last
            [self.leaveAlertView hide];
            self.leaveAlertView = nil;
        }
            break;
        case ALERT_CAN_NOT_LEAVE_TAG:
        {
            DLog(@"Button OK was selected.");
            // at last
            [self.cantLeaveAlertView hide];
            self.cantLeaveAlertView = nil;
        }
            break;
        case ALERT_NO_LAST_HAND_TAG:
        {
            [self.noHandAlertView hide];
            self.noHandAlertView = nil;
        }
            break;
        case ALERT_NO_LAST_ENTRIES_TAG:
        {
            [self.noEntryAlertView hide];
            self.noEntryAlertView = nil;
        }
            break;
        case ALERT_JOKER_CREATE_TAG:
        {
            [self.jokerJoinAlertView hide];
            self.jokerJoinAlertView = nil;
        }
            break;
            
        case ALERT_SHOW_BUTTON_TAG:
        {
            // for show
            [self.showButtonAlertView hide];
            self.showButtonAlertView = nil;
        }
            break;
        case ALERT_DROP_BUTTON_TAG:
        {
            [self.dropButtonAlertView hide];
            self.dropButtonAlertView = nil;
        }
            break;
        case ALERT_CAN_NOT_REJOIN_PLAY_TAG:
        {
            // if this player exist to join new table
            // for asking do you want to continue game once he loss
            [self.searchJoinAlertView hide];
            self.searchJoinAlertView = nil;
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView performSelector:@selector(showInstructionPlayerMode:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.2];
                
            }
            [self.playTable performSelector:@selector(showInstruction:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.2];
            [self refreshChairView];
            [self performSelector:@selector(hideChairView) withObject:nil afterDelay:1.2f];
            [self.playTable removeOpenDeckHolderCards];
            for(int i=0; i<6; i++)
            {
                UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
                UIImageView *imageView2= self.playerCardsBgImage[i];
                imageView1.hidden = YES;
                imageView1.image = nil;
                imageView2.hidden = YES;
                imageView2.image = nil;
            }
        }
            break;
        case ALERT_RESHUFFLE_TAG:
        {
            [self.reshuffleAlertView hide];
            self.reshuffleAlertView = nil;
        }
            break;
        case ALERT_REBUYIN_JOKER_TAG:
        {
            self.isRebuyin = NO;
            // at last
            [self.rebuyAlertView hide];
            self.rebuyAlertView = nil;
        }
            break;
        case ALERT_CAN_REJOIN_PLAY_TAG:
        {
            // if click on CANCEL
            [self cancelRejoin];
        }
            break;
        case ALERT_SPLIT_REJECT_TAG:
        {
            [self.splitRejectAlertView hide];
            self.splitRejectAlertView = nil;
        }
            break;
        case  ALERT_LEAVE_JOKER_TABLE_TAG:
        {
            if (self.playTableDictionary)
            {
                [self quitGame];
            }
            // at last
            [self.leaveForJokerAlertView hide];
            self.leaveForJokerAlertView = nil;
        }
            break;
        case ALERT_SPLIT_REQUEST_TIMER_TAG:
        {
            [[TAJGameEngine sharedGameEngine] splitRejectReplyWithMsgUUID:self.splitMsgUUid
                                                                   userID:self.thisPlayerModel.playerID
                                                                 nickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId];
            [self.splitRequestAlertView hide];
            self.splitRequestAlertView = nil;
        }
            break;
        case ALERT_MELD_INVALID:
            [self.infoPopUpController hide];
            self.infoPopUpController = Nil;
            break;
        case ALERT_ERROR:
            [self.errorInfoPopupViewController hide];
            self.errorInfoPopupViewController = Nil;
            break;
        case TOURNAMENT_ELEMINATE_TAG:
            [self.tourneyEleminateAlertView hide];
            self.tourneyEleminateAlertView = nil;
            break;
        default:
            break;
    }
}

#endif

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == ALERT_CAN_NOT_REJOIN_PLAY_TAG)
    {
        // if this player exist to join new table
        // for asking do you want to continue game once he loss
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:NO_STRING])
        {
            self.searchJoinAlertView = nil;
            if(self.isPlayerModelSlided)
            {
                
                [self.playerModelView performSelector:@selector(showInstructionPlayerMode:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.2];
                
            }
            [self.playTable performSelector:@selector(showInstruction:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.2];
            [self refreshChairView];
            [self hideChairView];
            [self.playTable removeOpenDeckHolderCards];
            for(int i=0; i<6; i++)
            {
                UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
                UIImageView *imageView2= self.playerCardsBgImage[i];
                imageView1.hidden = YES;
                imageView1.image = nil;
                imageView2.hidden = YES;
                imageView2.image = nil;
            }
            
        }
        else if([title isEqualToString:YES_STRING])
        {
            [self removeScoreSheetView];
            
            [self removeWinnerScreenView];
            
            self.searchJoinAlertView = nil;
            // player is requested to join new table
            self.isRequestedNewTable = YES;
            
            // VERY IMPORTANT: call delegate after self.isRequestedNewTable = YES
            //APP_DELEGATE.gameTableIndex = 2;
            if ([self.gameTableDelegate respondsToSelector:@selector(newTableRequested:)])
            {
                [self.gameTableDelegate newTableRequested:[self.currentTableId integerValue]];
            }
            
            NSString *uuID = Nil;
            uuID = [[TAJUtilities sharedUtilities] getTheUUID];
            
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            
            switch (engine.lobby.runningTableType)
            {
                case eOpenTableFirst:
                    engine.lobby.firstTable.msgUUIDForSearchJoinTable = uuID;
                    break;
                    
                case eOpenTableSecond:
                    engine.lobby.secondTable.msgUUIDForSearchJoinTable = uuID;
                    break;
                    
                default:
                    break;
            }
            
            [self removeAllNotifications];
            
            [[TAJGameEngine sharedGameEngine] searchJoinTableWithUserID:self.thisPlayerModel.playerID
                                                               nickName:self.thisPlayerModel.playerName
                                                                    bet:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_BET]
                                                             maxplayers:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_MAXPLAYER]
                                                              tableType:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_TABLETYPE]
                                                              tableCost:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_CASH_TYPE]
                                                                tableID:self.currentTableId
                                                             conversion:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_CONVERSION]
                                                             streamName:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_STREAM_NAME]
                                                               streamID:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_STREAM_ID]
                                                         gameSettingsID:[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_GAME_SETTING_ID]
                                                                msgUUID:uuID];
        }
    }
    else if (alertView.tag == ALERT_CAN_REJOIN_PLAY_TAG)
    {
        // if this player exist to rejoin same table
        // for asking do you want to rejoin the same table once again once he loss
        if (buttonIndex == 0)
        {
            // if click on OK
            
            // if NSTimer is running
            if ([self.rejoinNSTimer isValid])
            {
                [self.rejoinNSTimer invalidate];
            }
            self.rejoinAlertView = nil;
            
            [[TAJGameEngine sharedGameEngine] rejoinReplyYesWithUserID:self.thisPlayerModel.playerID
                                                              nickName:self.thisPlayerModel.playerName
                                                               tableID:self.currentTableId
                                                               msgUUID:self.rejoinMsgUUid];
            
        }
        else if(buttonIndex == 1)
        {
            // if click on CANCEL
            [self cancelRejoin];
        }
    }
    else if (alertView.tag == ALERT_SPLIT_REQUEST_TIMER_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:NO_STRING])
        {
            [[TAJGameEngine sharedGameEngine] splitRejectReplyWithMsgUUID:self.splitMsgUUid
                                                                   userID:self.thisPlayerModel.playerID
                                                                 nickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId];
            self.splitRequestAlertView = nil;
        }
        else if([title isEqualToString:YES_STRING])
        {
            [[TAJGameEngine sharedGameEngine] splitAcceptReplyWithMsgUUID:self.splitMsgUUid
                                                                   userID:self.thisPlayerModel.playerID
                                                                 nickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId];
            self.splitRequestAlertView = nil;
        }
    }
    else if (alertView.tag == ALERT_SPLIT_REJECT_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            self.splitRejectAlertView = nil;
        }
    }
    else if(alertView.tag == ALERT_REBUYIN_JOKER_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            [self removeScoreSheetView];
            
            if (!self.jokerPopTableView)
            {
                NSLog(@"CHECK JOCKER 2382");
                [self createJokerPopTableView];
                
            }
            self.leaveButton.enabled = NO;
            self.rebuyAlertView = nil;
        }
        else if([title isEqualToString:CANCEL_STRING])
        {
            self.isRebuyin = NO;
            // at last
            self.rebuyAlertView = nil;
        }
        
    }
    else if(alertView.tag == ALERT_CAN_NOT_LEAVE_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            DLog(@"Button OK was selected.");
            // at last
            self.cantLeaveAlertView = nil;
        }
    }
    else if(alertView.tag == ALERT_CAN_LEAVE_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:NO_STRING])
        {
            DLog(@"Button NO was selected.");
            // at last
            self.leaveAlertView = nil;
        }
        else if([title isEqualToString:YES_STRING])
        {
            DLog(@"Button YES was selected.");
            if (self.playTableDictionary)
            {
                [self quitGame];
            }
            self.leaveAlertView = nil;
        }
    }
    else if (alertView.tag == ALERT_NO_LAST_HAND_TAG)
    {
        // for score board
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            self.noHandAlertView = nil;
        }
    }
    else if (alertView.tag == ALERT_NO_LAST_ENTRIES_TAG)
    {
        // for score sheet
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            self.noEntryAlertView = nil;
        }
    }
    else if(alertView.tag == ALERT_SHOW_BUTTON_TAG)
    {
        // for show
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            [self placeShow];
            self.showButtonAlertView = nil;
        }
        else if([title isEqualToString:CANCEL_STRING])
        {
            self.showButtonAlertView = nil;
        }
    }
    else if(alertView.tag == ALERT_DROP_BUTTON_TAG)
    {
        // for drop
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        if([title isEqualToString:OK_STRING])
        {
            [[TAJGameEngine sharedGameEngine] playerDropEventWithNickName:self.thisPlayerModel.playerName
                                                                  tableID:self.currentTableId
                                                                   userID:self.thisPlayerModel.playerID];
            
            self.dropButton.enabled = NO;
            [self.playTable dropGameSetup];
            self.dropButtonAlertView = nil;
            
            NSLog(@"DROP SUCCESS");
            if ([self.savedFirstTableID isEqualToString:[self currentTableId]]) {
            //[self saveToCoreDataWithEvent:@"Drop"];
            }
            else {
               // [self saveToCoreDataWithSecondTableEvent:@"Drop"];
            }
            
        }
        else if([title isEqualToString:CANCEL_STRING])
        {
            self.dropButtonAlertView = nil;
        }
    }
    else if(alertView.tag == ALERT_LEAVE_JOKER_TABLE_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        
        if([title isEqualToString:OK_STRING])
        {
            if (self.playTableDictionary)
            {
                [self quitGame];
            }
            // at last
            alertView = nil;
        }
    }
    else if (alertView.tag == ALERT_RESHUFFLE_TAG)
    {
        NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
        
        if([title isEqualToString:OK_STRING])
        {
            // at last
            self.reshuffleAlertView = nil;
        }
    }
}

//leave the game and send quitTable request to server
- (void)quitGame
{
    
    if (self.isSeatRequestPending)
    {
        return;
    }
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(removeThisPlayerDiscardCardWhenScheduleGame:)])
    {
        [self.gameTableDelegate removeThisPlayerDiscardCardWhenScheduleGame:self.currentTableId];
    }
    [self removeAllNotifications];
    //If current table is closed by the admin not sending the quit table message again
    if (!self.isCurrentTableClosed)
    {
        DLog(@"game table send the quit table for %@",self.currentTableId);
        if(isTournament){
            [[TAJGameEngine sharedGameEngine] requestLeaveTourney:tourneyId];
        }else{
            [[TAJGameEngine sharedGameEngine] quitTableWithTableID:self.currentTableId];
        }
        TAJGameEngine *engine = TAJ_GAME_ENGINE;
        TAJLobby *lobby = engine.lobby;
        [lobby destroyTable:self.currentTableId];
    }
    else
    {
        //[[TAJGameEngine sharedGameEngine] quitTableWithTableID:self.currentTableId];
        if(isTournament){
            [[TAJGameEngine sharedGameEngine] requestLeaveTourney:tourneyId];
        }else{
            [[TAJGameEngine sharedGameEngine] quitTableWithTableID:self.currentTableId];
        }
        //Destroy the table in lobby
        TAJGameEngine *engine = TAJ_GAME_ENGINE;
        TAJLobby *lobby = engine.lobby;
        [lobby destroyTable:self.currentTableId];
    }
    
    // this is to inform for game engine to know game started or not
    self.canShowIamBackView = NO;
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(informGameEngineForGameEnded:)])
    {
        [self.gameTableDelegate informGameEngineForGameEnded:[self.currentTableId integerValue]];
    }
    if ([self.gameStartTimer isValid])
    {
        // Stop timer
        [self.gameStartTimer invalidate];
    }
    
    if ([self.playerTime isValid])
    {
        // Stop timer
        [self.playerTime invalidate];
    }
    // Sound
    [[TAJSoundHandler sharedManager] StopSounds];
    //irresponsible of quit table reply without table id
    //we are going to pass table id number in delegate
    if ([self.gameTableDelegate respondsToSelector:@selector(tableIDForQuit:)])
    {
        [self.gameTableDelegate tableIDForQuit:[self.currentTableId integerValue]];
    }
}

- (void)removeAllNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REQUEST_JOIN_TABLE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_IAMBACKBUTTON_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_IAMBACKARRAY_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TABLE_CLOSED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_DESCHEDULE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_JOIN object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_SCHEDULE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_QUIT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TABLE_TOSS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_SITTING_SEQUENCE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:START_GAME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEND_STACK object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEND_DEAL object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_DEAL object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TURN_UPDATE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CARD_DISCARD object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CARD_PICK object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_RESULT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_END object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TURN_TIMEOUT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TURN_EXTRA_TIME object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TURN_EXTRATIME_RECONNECT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_EXTRATIME_RECONNECT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MELD_EXTRATIME_RECONNECT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STACK_RESUFFLE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_DROP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_EVENTS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SMART_CORRECTION object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MELD_SUCCESS_CURRENT_PLAYER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MELD_REPLY_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:CHECKMELD_REPLY_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PRE_GAME_RESULT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MELD_REQUEST_NOTIFICATION_FOR_OTHER_PLAYER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MELD_FAIL object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_ELIMINATE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REJOIN_REQUEST object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REJOIN_EVENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLIT_STATUS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLIT_FALSE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLIT_REPLY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLIT_REQUEST object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SPLIT_RESULT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:POOL_WINNER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BEST_OF_WINNER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REBUYIN object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_DISCONNECTED object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ON_SOCKET_CONNECTION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AUTO_PLAY_STATUS_REPLY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:AUTO_PLAY_STATUS_EVENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEND_SLOTS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ON_SOCKET_ERROR object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:STAND_UP object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MOVETOLOBBY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:DEVICE_CHANGE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REBUYIN_REPLY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REBUYIN_EVENT object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LEVEL_START object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LEVEL_SCHEDULE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_LEVEL_END object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNAMENT_GET_LEVEL_TIMER object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:END_TOURNAMENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNEY_REBUYIN_REPLY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNEY_REBUYIN_EVENT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TOURNEY_ELEMINATE object:nil];
    
}

- (void)leaveTable
{
    //Leave table alert
    
    //
    //    Player cannot leave the table when there is 14 hand cards. Proper dialogue should appear.
    //    @"You cannot leave the table at this stage."
    //    Player cannot leave during card distribution
    
    //Author : RK
    //Alert
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        if (([self.myCardsDealArray count] == CARDS_PICKED_COUNT || !self.canLeaveTable) && !self.isSpectator)
        {
            // player joined as play
            if ([self.myCardsDealArray count] == CARDS_PICKED_COUNT)
            {
                self.leaveTableMessage = LEAVE_TABLE_14CARD;
            }
            
            if (!self.cantLeaveAlertView)
            {
#if NEW_INFO_VIEW_IMPLEMENTATION
                self.cantLeaveAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_NOT_LEAVE_TAG popupButtonType:eOK_Type message:self.leaveTableMessage];
#else
                self.cantLeaveAlertView = [[UIAlertView alloc]initWithTitle:ALERT_EMPTY_TITLE message:self.leaveTableMessage delegate:self cancelButtonTitle:OK_STRING otherButtonTitles: nil];
                self.cantLeaveAlertView.tag = ALERT_CAN_NOT_LEAVE_TAG;
#endif
                [self.cantLeaveAlertView show];
            }
        }
        else if(!self.isSpectator)
        {
            // player joined as play
            if (!self.leaveAlertView)
            {
#if NEW_INFO_VIEW_IMPLEMENTATION
                
                self.leaveAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_LEAVE_TAG popupButtonType:eYes_No_Type message:LEAVE_TABLE_MESSAGE];
#else
                self.leaveAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                                 message:LEAVE_TABLE_MESSAGE
                                                                delegate:self
                                                       cancelButtonTitle:NO_STRING
                                                       otherButtonTitles:YES_STRING, nil];
                self.leaveAlertView.tag = ALERT_CAN_LEAVE_TAG;
#endif
                [self.leaveAlertView show];
            }
        }
        else if(self.isSpectator)
        {
            // player joined as watch(view)
            if (self.playTableDictionary)
            {
                [self quitGame];
            }
        }
    }
    else
    {
        [self showNoInternetConnectionAlert];
    }
    //       Leaving the table during " Please wait your game starts in -- sec" should lead directly to lobby. Opponent end- 'Left player should be disappeared and " Player less then minimum player limit" text should appear
    
    //     Leaving the table during game play should lead to lobby directly. Opponent end - Game result screen is loaded.
}

//extra time button action
- (IBAction)extraTimeButtonAction:(UIButton *)sender
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [engine turnExtraTimeEventWithNickName:self.thisPlayerModel.playerName
                                       tableID:self.currentTableId
                                        userID:self.thisPlayerModel.playerID];
        self.isExtraTimeButtonClicked = NO;
        [self updateExtraTimeButton:NO];
    }
    else
    {
        [self showNoInternetConnectionAlert];
    }
}

- (IBAction)showButtonAction:(UIButton *)sender
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        return;
    }
    
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        NSString *title=self.showButton.titleLabel.text;
        
        if ([title isEqualToString:@"DECLARE"]){
            NSLog(@"DECLARED");
            if ([self.savedFirstTableID isEqualToString:[self currentTableId]]) {
                //[self saveToCoreDataWithEvent:@"Declare"];
            }
            else {
                //[self saveToCoreDataWithSecondTableEvent:@"Declare"];
            }
            
            [self createMeldTableView];
            if (self.meldViewTable)
            {
                
#if DEBUG
                NSLog(@"CHECK POPUP MELD!!!");
#endif
                [self.meldViewTable setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
                [self.meldViewTable setHidden:NO];
                self.winnerAIImageView.hidden = NO;
                [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];//by ratheesh
                NSLog(@"HIDE BUTTONS 1");
            }
        }else{
            
            if (!self.showButtonAlertView) //ReDIM Changes
            {
#if DEBUG
                NSLog(@"CHECK POPUP MELD");
#endif
                self.showButtonAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_SHOW_BUTTON_TAG popupButtonType:eOK_Cancel_Type message:SHOW_BUTTON_TITLE];
                [self.showButtonAlertView show];
            }
        }
    }
    else
    {
        [self showNoInternetConnectionAlert];
    }
}

//- (void) checkMeld{
////        [[TAJGameEngine sharedGameEngine] cardsMeldForOtherPlayerWithTableID:self.currentTableId cards:[self meldCardsStringForArray:meldCards] withMsgUUid:self.meldOtherMsgUUid];
//}

- (void)placeShow
{
    if ([self.playerTime isValid])
    {
        [self.playerTime invalidate];
    }
    
    [self playerTurnView];
    
    if (self.discardCardInfoForShow)
    {
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [engine showEventWithSuit:[self.discardCardInfoForShow[kTaj_Card_Suit_Key] lowercaseString]
                             face:self.discardCardInfoForShow[kTaj_Card_Face_Key]
                          tableID:self.currentTableId
                           userID:self.thisPlayerModel.playerID
                         nickName:self.thisPlayerModel.playerName];
        //remove from card info from my deck
        [self removeCardWithCardNumber:self.discardCardInfoForShow[kTaj_Card_Face_Key] withSuit:[self.discardCardInfoForShow[kTaj_Card_Suit_Key] lowercaseString]];
    }
    //apply meld by discarding selected card
    [self.playTable meldSetup];
#if PLAYER_MODEL_GAME_CARD
    // remove discard card info for show in player model
    [self.playerModelView meldSetupForModel];
#endif
    //[self updateShowButton:NO]; ReDIM Changes
    
    [self updateExtraTimeButton:NO];
}

- (IBAction)dropButtonAction:(UIButton *)sender
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        return;
    }
    
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        if (!self.dropButtonAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            
            self.dropButtonAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_DROP_BUTTON_TAG popupButtonType:eYes_No_Type message:DROP_BUTTON_TITLE];
#else
            self.dropButtonAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                                  message:DROP_BUTTON_TITLE
                                                                 delegate:self
                                                        cancelButtonTitle:OK_STRING
                                                        otherButtonTitles:CANCEL_STRING, nil];
            
            self.dropButtonAlertView.tag = ALERT_DROP_BUTTON_TAG;
#endif
            [self.dropButtonAlertView show];
        }
    }
    else
    {
        [self showNoInternetConnectionAlert];
    }
    [self.playTable removeAllButtons];
    [self.playerModelView removeAllModelButtons];
}

- (IBAction)flipCardsAction:(UIButton *)sender
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        [self.playTable flipCards];
#if PLAYER_MODEL_GAME_CARD
        [self.playerModelView flipModelCards];
#endif
    }
    else
    {
        [self showNoInternetConnectionAlert];
    }
}

- (IBAction)scoreBoardButtonAction:(UIButton *)sender
{
    [self removeRespectiveViewForJokerGame];
    [self.settingMenuListView removeFromSuperview];
    self.isMenuListShowing=NO;
    
    if (self.boardScoreController && ![self.boardScoreController isShowing])
    {
        //[self.boardScoreController quitUserIds:self.quitUsers];
        if ([self.boardScoreController isDataExist])
        {
            [self.boardScoreController reloadScoreBoardPoints];
            [TAJAppDelegate setScreenSize: self.boardScoreController.view fromView: self.view];
            [self.view addSubview:self.boardScoreController.view];
            [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
            NSLog(@"HIDE BUTTONS 2");
        }
        else
        {
            if (!self.noEntryAlertView)
            {
#if NEW_INFO_VIEW_IMPLEMENTATION
                self.noEntryAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_NO_LAST_ENTRIES_TAG popupButtonType:eOK_Type message:SCORE_BOARD_NO_ENTRY];
#else
                self.noEntryAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                                   message:SCORE_BOARD_NO_ENTRY
                                                                  delegate:self
                                                         cancelButtonTitle:OK_STRING
                                                         otherButtonTitles: nil];
                self.noEntryAlertView.tag = ALERT_NO_LAST_ENTRIES_TAG;
#endif
                [self.noEntryAlertView show];
            }
        }
    }
    else
    {
        self.scoreBoardLabel.textColor = [UIColor whiteColor];
    }
}

- (IBAction)chatButtonAction:(UIButton *)sender
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        return;
    }
    //[self liveFeedStatus];
    [self createChatView];
}

- (void)createChatView
{
    self.chatAlertImageView.hidden = YES;
    //   DLog(@"count = %d",count);
    if(self.isChatButtonShow)
    {
        if([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"iphn5_selected.png"] forState:UIControlStateNormal];
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected.png"] forState:UIControlStateNormal];
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected_discard.png"] forState:UIControlStateNormal];
            }
            else
            {
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected_discard-568h@2x~iphone.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.chatButton setBackgroundImage:[UIImage imageNamed:@"non_selected"] forState:UIControlStateNormal];
            [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected_discard"] forState:UIControlStateNormal];
            
        }
        if (self.viewHolderForChatDiscardLiveFeed)
        {
            [self removeLiveFeedView];
            [self removeDiscardHistoryView];
            // at this point remove all views which has been added as subview to viewHolderForChatDiscardLiveFeed Holder
            if([TAJUtilities isIPhone]) {
                [TAJAppDelegate setScreenSize: self.chatViewController.view fromView: self.viewHolderForChatDiscardLiveFeed];
            }
            
            [self.viewHolderForChatDiscardLiveFeed addSubview:self.chatViewController.view];
            // after that allow to user to chat
            
            if (self.chatViewController)
            {
                CGRect xframe = self.viewHolderForChatDiscardLiveFeed.frame;
                CGRect yframe = self.chatViewController.view.frame;
                yframe.origin.y += xframe.size.height;
                yframe.size.height = xframe.size.height;//ReDIM Changes
                self.chatViewController.view.frame = yframe;
                // z*__weak weakself = self.chatViewController.view;
                [UIView animateWithDuration:0.8
                                      delay:0.0
                                    options: UIViewAnimationOptionCurveEaseIn
                                 animations:
                 ^{
                     CGRect finalRect;
                     finalRect  = yframe;
                     finalRect.origin = CGPointZero;
                     //finalRect.origin.y = 0;
                     self.chatViewController.view.frame = finalRect;
                 }
                                 completion:nil
                 ];
            }
        }
        self.isChatButtonShow = NO;
        self.isFeedButtonShow = YES;
        self.isDiscardButtonShow = YES;
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected.png"] forState:UIControlStateNormal];
            }
            else
            {
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
            }
            
            // [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
            // [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
        }
        else
        {
            [self.chatButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            //  [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            // [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            
        }
        if (self.chatViewController)
        { [self.chatViewController.view removeFromSuperview];
        }
        
        self.isChatButtonShow = YES;
        
    }
    [self.chatViewController reloadChatHistoryTable];
}

- (IBAction)liveFeedButtonAction:(UIButton *)sender
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        return;
    }
    [self liveFeedStatus];
}

- (void)liveFeedStatus
{
    if(self.isFeedButtonShow)
    {
        if([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"iphn5_selected.png"] forState:UIControlStateNormal];
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected.png"] forState:UIControlStateNormal];
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected_discard.png"] forState:UIControlStateNormal];
            }
            else
            {
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected_discard-568h@2x~iphone.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"non_selected"] forState:UIControlStateNormal];
            [self.chatButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected_discard"] forState:UIControlStateNormal];
        }
        
        if (self.viewHolderForChatDiscardLiveFeed)
        {
            [self removeChatView];
            [self removeDiscardHistoryView];
            // at this point remove all views which as been added as subview to viewHolderForChatDiscardLiveFeed Holder
            [self.viewHolderForChatDiscardLiveFeed addSubview:self.liveFeedController.view];
            // after that allow user to see the live feed
            if (!self.isSpectator || self.liveFeedController)
            {
                CGRect xframe = self.viewHolderForChatDiscardLiveFeed.frame;
                CGRect yframe = self.liveFeedController.view.frame;
                yframe.origin.y += xframe.size.height;
                yframe.size.height = xframe.size.height;//ReDIM Changes
                self.liveFeedController.view.frame = yframe;
                [UIView animateWithDuration:0.8
                                      delay:0.0
                                    options: UIViewAnimationOptionCurveEaseIn
                                 animations:
                 ^{
                     CGRect finalRect;
                     finalRect  = yframe;
                     finalRect.origin = CGPointZero;
                     self.liveFeedController.view.frame = finalRect;
                 }
                                 completion:nil
                 ];
                
                [self.liveFeedController reloadLiveFeedData];
            }
        }
        self.isFeedButtonShow = NO;
        self.isChatButtonShow = YES;
        self.isDiscardButtonShow = YES;
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected.png"] forState:UIControlStateNormal];
            }
            else
            {
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
        }
        if (self.viewHolderForChatDiscardLiveFeed)
        {
            // at this point remove all views which as been added as subview to viewHolderForChatDiscardLiveFeed Holder
            [self removeChatView];
            [self removeDiscardHistoryView];
            
            if (!self.isSpectator || self.liveFeedController)
            {
                [self.liveFeedController.view removeFromSuperview];
            }
        }
        self.isFeedButtonShow = YES;
    }
    
}
- (IBAction)discardHistoryButtonAction:(UIButton *)sender
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        return;
    }
    [self discardHistoryView];
}

-(void)discardHistoryView
{
    if(self.isDiscardButtonShow)
    {
        if([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"iphn5_selected_discard.png"] forState:UIControlStateNormal];
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected.png"] forState:UIControlStateNormal];
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected.png"] forState:UIControlStateNormal];
            }
            else
            {
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"selected_discard-568h@2x~iphone.png"] forState:UIControlStateNormal];
                [self.chatButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
                [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"non_selected-568h@2x~iphone.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"selected_discard"] forState:UIControlStateNormal];
            [self.chatButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
            [self.liveFeedButton setBackgroundImage:[UIImage imageNamed:@"selected"] forState:UIControlStateNormal];
        }
        if (self.viewHolderForChatDiscardLiveFeed)
        {
            [self removeChatView];
            [self removeLiveFeedView];
            // at this point remove all views which as been added as subview to viewHolderForChatDiscardLiveFeed Holder
            [self.viewHolderForChatDiscardLiveFeed addSubview:self.discardCardHistoryController.view];
            
            // after that allow user to see the live feed
            if (!self.isSpectator || self.discardCardHistoryController)
            {
                CGRect xframe = self.viewHolderForChatDiscardLiveFeed.frame;
                CGRect yframe = self.discardCardHistoryController.view.frame;
                yframe.origin.y = xframe.size.height;
                yframe.size.height = xframe.size.height;//ReDIM Changes
                self.discardCardHistoryController.view.frame = yframe;
                [UIView animateWithDuration:0.8
                                      delay:0.0
                                    options: UIViewAnimationOptionCurveEaseIn
                                 animations:
                 ^{
                     CGRect finalRect;
                     finalRect  = yframe;
                     finalRect.origin = CGPointZero;
                     self.discardCardHistoryController.view.frame = finalRect;
                     
                 }
                                 completion:nil
                 ];
            }
        }
        self.isDiscardButtonShow = NO;
        self.isFeedButtonShow = YES;
        self.isChatButtonShow = YES;
    }
    
    else
    {
        if([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"iphn5_non_selected_discard.png"] forState:UIControlStateNormal];
            }
            else
            {
                [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected_discard-568h@2x~iphone.png"] forState:UIControlStateNormal];
            }
        }
        else
        {
            [self.discardHistoryButton setBackgroundImage:[UIImage imageNamed:@"non_selected_discard"] forState:UIControlStateNormal];
        }
        
        if (!self.isSpectator || self.discardCardHistoryController)
        {
            [self.discardCardHistoryController.view removeFromSuperview];
        }
        self.isDiscardButtonShow = YES;
        
    }
}
#pragma mark - NOTIFICATION METHODS -

-(void)addNotificationObserver
{
    [self removeAllNotifications];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
    selector:@selector(chunksTimerUpdate:)
        name:@"chunks_extraTime"
      object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInformationOnBalanceUpdate:) name:BALANCE_UPDATE object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(iamBackButtonPressed:)
                                                 name:SHOW_IAMBACKBUTTON_ALERT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(iamBackThisPlayerArrayCount:)
                                                 name:SHOW_IAMBACKARRAY_ALERT
                                               object:nil];
    
    // Error code
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorWithCode:)
                                                 name:ERROR_NOTIFICATION
                                               object:nil];
    // Table Closed
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closeTable:)
                                                 name:TABLE_CLOSED
                                               object:nil];
    
    // De-schedule game if player leave table in between
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(descheduleGameOnce:)
                                                 name:GAME_DESCHEDULE
                                               object:nil];
    
    // Player joined
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerJoined:)
                                                 name:PLAYER_JOIN
                                               object:nil];
    
    // All players joined can start game
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scheduleGame:)
                                                 name:GAME_SCHEDULE
                                               object:nil];
    
    // Player Quit the game
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDidQuitGame:)
                                                 name:PLAYER_QUIT
                                               object:nil];
    // Listen for table toss
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tableTossDidEnd:)
                                                 name:TABLE_TOSS
                                               object:nil];
    
    // Sitting sequence
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rearrangeChairs:)
                                                 name:GAME_SITTING_SEQUENCE
                                               object:nil];
    
    // Start game event
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(startGameEvent:)
                                                 name:START_GAME
                                               object:nil];
    // Send stack event
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendStack:)
                                                 name:SEND_STACK
                                               object:nil];
    
    // Send deal event
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendDeal:)
                                                 name:SEND_DEAL
                                               object:nil];
    
    // Show Deal event
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showDealForSpectator:)
                                                 name:SHOW_DEAL
                                               object:nil];
    // Card discard event
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cardDiscard:)
                                                 name:CARD_DISCARD
                                               object:nil];
    
    // Card Pick
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(cardPick:)
                                                 name:CARD_PICK
                                               object:nil];
    
    
    // For Turn update
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(turnUpdate:)
                                                 name:TURN_UPDATE
                                               object:nil];
    
    // Game Result
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gameResultAtEnd:)
                                                 name:GAME_RESULT
                                               object:nil];
    
    // Game End
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gameEndNotified:)
                                                 name:GAME_END
                                               object:nil];
    // Turn Timout
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(turnTimeoutNotified:)
                                                 name:TURN_TIMEOUT
                                               object:nil];
    // Extra Turn Time
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(extraTurnTimeNotify:)
                                                 name:TURN_EXTRA_TIME
                                               object:nil];
    // Extra Turn Time Reconnect
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(extraTurnTimeReconnect:)
                                                 name:TURN_EXTRATIME_RECONNECT
                                               object:nil];
    // Show extra Time Reconnect
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showExtraTimeReconnect:)
                                                 name:SHOW_EXTRATIME_RECONNECT
                                               object:nil];
    
    // Meld extra Time Reconnect
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(meldExtraTimeReconnect:)
                                                 name:MELD_EXTRATIME_RECONNECT
                                               object:nil];
    
    // Stack Reshuffle
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(stackReshuffleNotify:)
                                                 name:STACK_RESUFFLE
                                               object:nil];
    
    // Player Drop Game
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDropGame:)
                                                 name:PLAYER_DROP
                                               object:nil];
    // Player placing ShowEvent
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerPlacingShowNotified:)
                                                 name:SHOW_EVENTS
                                               object:nil];
    
    // Smart Correction ShowEvent
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(smartCorrectionNotified:)
                                                 name:SMART_CORRECTION
                                               object:nil];
    
    // This player meld Success
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(thisPlayerMeldSuccess:)
                                                 name:MELD_SUCCESS_CURRENT_PLAYER
                                               object:nil];
    
    // This player meld Reply
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(thisPlayerMeldReply:)
                                                 name:MELD_REPLY_NOTIFICATION
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(thisCheckPlayerMeldReply:)
                                                 name:CHECKMELD_REPLY_NOTIFICATION
                                               object:nil];
    
    // Pre-Game Result once this player meld success
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(preGameResultLoad:)
                                                 name:PRE_GAME_RESULT
                                               object:nil];
    
    // Create meld view for other once this player meld success
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(createMeldForOtherPlayer:)
                                                 name:MELD_REQUEST_NOTIFICATION_FOR_OTHER_PLAYER
                                               object:nil];
    
    // This player meld failue
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(thisPlayerMeldFailure:)
                                                 name:MELD_FAIL
                                               object:nil];
    
    // Player Eliminate
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerEliminate:)
                                                 name:PLAYER_ELIMINATE
                                               object:nil];
    
    // Player rejoin to play same table once again, once he cross the max level points
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestToRejoin:)
                                                 name:REJOIN_REQUEST
                                               object:nil];
    
    // Make player to rejoin same table
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rejoinToPlaySameTable:)
                                                 name:REJOIN_EVENT
                                               object:nil];
    // Split request
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(splitRequestForThisPlayer:)
                                                 name:SPLIT_STATUS
                                               object:nil];
    
    // Split request with timer for other player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(splitRequestForOtherPlayer:)
                                                 name:SPLIT_REQUEST
                                               object:nil];
    
    // Split request with timeout for other player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(splitTimeOutForOtherPlayer:)
                                                 name:SPLIT_FALSE
                                               object:nil];
    
    // Split reply for this player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(splitReplyForThisPlayer:)
                                                 name:SPLIT_REPLY
                                               object:nil];
    // Split result
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(splitResultForPlayer:)
                                                 name:SPLIT_RESULT
                                               object:nil];
    // Pool 101 and 201 Winner
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(poolWinner:)
                                                 name:POOL_WINNER
                                               object:nil];
    // Best of 2, 3 and 6 winner
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(bestOfWinner:)
                                                 name:BEST_OF_WINNER
                                               object:nil];
    
    // Rebuy in joker , non joker this player table
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rebuyInOptionThisPlayer:)
                                                 name:REBUYIN_REPLY
                                               object:nil];
    
    // Rebuy in joker , non joker other player table
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(rebuyInOptionOtherPlayer:)
                                                 name:REBUYIN_EVENT
                                               object:nil];
    
    // Player disconnected
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerDisconnected:)
                                                 name:PLAYER_DISCONNECTED
                                               object:nil];
    
    // Player reconnected
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(playerReconnected:)
                                                 name:ON_SOCKET_CONNECTION
                                               object:nil];
    
    // Autoplay status This device player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoPlayStatusThisPlayer:)
                                                 name:AUTO_PLAY_STATUS_REPLY
                                               object:nil];
    
    // Autoplay status Other player
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(autoPlayStatusOtherPlayer:)
                                                 name:AUTO_PLAY_STATUS_EVENT
                                               object:nil];
    
    // Send slots for this player reconnect
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(sendSlotsForThisPlayer:)
                                                 name:SEND_SLOTS
                                               object:nil];
    
    // On Socket Error in game play
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onSocketErrorInGamePlay:)
                                                 name:ON_SOCKET_ERROR
                                               object:nil];
    
    // Stand up this player reply
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(standUpThisPlayer:)
                                                 name:STAND_UP
                                               object:nil];
    // Report a Bug reply
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(reportaBugReply:)
                                                 name:REPORT_BUG
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(checkThisPlayerExistInTable:)
                                                 name:MOVETOLOBBY
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(deviceChangeEvent:)
                                                 name:DEVICE_CHANGE
                                               object:nil];
    
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLevelStart:)
                                                 name:TOURNAMENT_LEVEL_START
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLevelSchedule:)
                                                 name:TOURNAMENT_LEVEL_SCHEDULE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentLevelEnd:)
                                                 name:TOURNAMENT_LEVEL_END
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tournamentGetLeveltimer:)
                                                 name:TOURNAMENT_GET_LEVEL_TIMER
                                               object:nil];
    //    [[NSNotificationCenter defaultCenter] addObserver:self
    //                                             selector:@selector(requestJoinTable:)
    //                                                 name:REQUEST_JOIN_TABLE
    //                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(endTournament:)
                                                 name:END_TOURNAMENT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tourneyRebuyinReply:)
                                                 name:TOURNEY_REBUYIN_REPLY
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tourneyRebuyinEvent:)
                                                 name:TOURNEY_REBUYIN_EVENT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(tourneyEleminatevent:)
                                                 name:TOURNEY_ELEMINATE
                                               object:nil];

    /* {
     "TAJ_chips" = "200.0";
     "TAJ_event_name" = "tournament_eleminate";
     "TAJ_minimumchips" = 800;
     "TAJ_msg_uuid" = "c7896f3c-163a-11e8-85a9-1cc1de039106";
     "TAJ_name" = event;
     "TAJ_nick_name" = redimuser1;
     "TAJ_reason" = 7100;
     "TAJ_timestamp" = "1519130234.0";
     "TAJ_tournament_id" = 3955;
     "TAJ_user_id" = 7320;
     }*/
}


- (void) updateUserInformationOnBalanceUpdate:(NSNotification *)notification
{
    [self updatePlayersInformation];
}

- (void) updatePlayersInformation
{
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    TAJUserModel *model = engine.usermodel;
    
    
    self.realChipsValue = [NSString stringWithFormat:@"%.02f",[model.realChips floatValue]];
    self.realInPlayValue = [NSString stringWithFormat:@"%.02f",[model.realInPlay floatValue]];
    self.funChipsValue = [NSString stringWithFormat:@"%.02f",[model.funchips floatValue]];
    self.funInPlayValue = [NSString stringWithFormat:@"%.02f",[model.funInPlay floatValue]];
    NSLog(@"realChipsValue : %@",self.realChipsValue);
    NSLog(@"realInPlayValue : %@",self.realInPlayValue);
    NSLog(@"funChipsValue : %@",self.funChipsValue);
    NSLog(@"funInPlayValue : %@",self.funInPlayValue);
}


#pragma mark - Notification listener functions -

- (void)tourneyEleminatevent:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    //    [self.playTable removeInfoLabelMessage];
    //    [self.playTable showInstruction:END_TOURNEY];
    
    
    self.tourneyEleminateAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:TOURNAMENT_ELEMINATE_TAG popupButtonType:eOK_Type message:ELEMINATE_TOURNEY_MESSAGE];
    
    // self.tourneyEleminateAlertView.tag = TOURNAMENT_ELEMINATE_TAG;
    [self.tourneyEleminateAlertView show];
}

- (void)requestJoinTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [[TAJGameEngine sharedGameEngine] requestJoinTableReply:dictionary[@"TAJ_msg_uuid"]];
    [[TAJGameEngine sharedGameEngine] getTableDetailsAndJoinTableAsPlayTournament:dictionary];
}

- (void)endTournament:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    //This tournamnet is completed
    [self.playTable removeInfoLabelMessage];
    [self.playTable showInstruction:END_TOURNEY];
    self.isSpectator = YES;
    self.canLeaveTable = YES;
    [self.playTable removeOpenDeckHolderCards];
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    
    [self refreshChairView];
    [self performSelector:@selector(hideChairView) withObject:nil afterDelay:1.2f];
    
    [self stopTimerLevel];
}

- (void)tourneyRebuyinReply:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
}

- (void)tourneyRebuyinEvent:(NSNotification *) notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    [engine tourneyRebuyinWithTourneyId:@"" level:@"" rebuyinAmt:@""];
}

//checks whether current device player exist in the table after reconnection if doesnt exist player will be moved out of the game
- (void)checkThisPlayerExistInTable:(NSNotification *)notification
{
    NSArray *array;
    array = [NSArray arrayWithArray:[[[TAJGameEngine sharedGameEngine] usermodel] tableIDsArray]];
    //    DLog(@"game table controller array table %@ ",array);
    if (array == NULL )
    {
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [engine iAmBackWithUserID:[[engine usermodel] userId]];
        //        DLog(@"arr null  ---checkThisPlayerExistInTable-----array table %@ ",array);
        [self quitGame];
    }
    else if(array)
    {
        BOOL playerPresentInTable = NO;
        for (int index = 0; index < array .count; index++)
        {
            if ([self.currentTableId isEqualToString:array[index]])
            {
                playerPresentInTable = YES;
            }
        }
        if (!playerPresentInTable )
        {
            [self quitGame];
        }
    }
}

//change the device symbol chairs for notified player by the server

- (void)deviceChangeEvent:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [[dictionary valueForKeyPath:TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        DLog(@"deive has chajnge %@",dictionary);
        UIView *chairHolder = [self chairHolderFromUserId:[dictionary[kTaj_userid_Key] integerValue]];
        if (chairHolder)
        {
            TAJChairView *chair = [self chairFromView:chairHolder];
            if (chair)
            {
                chair.playerModel.playerDeviceId = dictionary[@"TAJ_device_id"];
                [chair updatePlayerDeviceSymbol];
                
            }
        }
        
    }
}

- (void)reportaBugReply:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [[dictionary valueForKeyPath:TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleReportBugReply:dictionary];
    }
}

// Stand up this player reply notifications
- (void)standUpThisPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [[dictionary valueForKeyPath:TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleStandUpThisPlayerReply:dictionary];
    }
}


// On Socket Error in game play notifications / implementation
- (void)onSocketErrorInGamePlay:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
}

// Send slots for this player reconnect notifications
- (void)sendSlotsForThisPlayer:(NSNotification *)notification
{
    DLog(@"Send slot event");
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [[dictionary valueForKeyPath:TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        NSMutableDictionary *tableCards = [NSMutableDictionary dictionaryWithDictionary:[dictionary valueForKeyPath:TAJ_HEART_BEAT_TABLE]];
        
        [self handleSendSlotsForThisPlayerEvent:[tableCards valueForKeyPath:TAJ_HEART_BEAT_CARDS]];
    }
}

// Auto play status for this player notifications
- (void)autoPlayStatusThisPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    // Server not providing table id for this player auto play status
    [self handleAutoPlayStatusThisPlayerEvent:dictionary];
}

// Auto play status for other player notifications
- (void)autoPlayStatusOtherPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [[dictionary valueForKeyPath:TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleAutoPlayStatusOtherPlayerEvent:dictionary];
    }
}

// Player reconnected notifications / implementation
- (void)playerReconnected:(NSNotification *)notification
{
    if ([self.savedFirstTableID isEqualToString:self.tableIDString]) {
        //[self saveToCoreDataWithEvent:@"Engine Connect"];
    } else {
       // [self saveToCoreDataWithSecondTableEvent:@"Engine Connect"];
    }
    self.isPlayerReconnected = YES;
    DLog(@"player reconnected");
    self.winnerAIImageView.hidden = YES;
    self.isScheduleGameCame = NO;
    // self.winnerAIImageView.hidden = NO;
    self.isIamBackButtonPressed = NO;
    self.isChatButtonShow = YES;
    self.canAnimate = NO;
    self.isExtraTimeButtonClicked = NO;
    [self saveResultForKey:YES];
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    self.isValidShowPreGameResult = NO;
    self.isPlayerDisconnected = NO;
    self.isReconnected = YES;
    [self setIsMyTurn:NO];
    
    [self.playTable clearCardsinClosedCard];
    if (!self.isFirstRoundGameAlreadyStarted)
    {
        self.canQuitGame = YES;
        if(self.isSendDealCame)
        {
            self.canQuitGame = NO;
        }
        if(self.canQuitGame)
        {
            [self quitGame];
        }
        else
        {
            if (self.meldViewTable)
            {
                if (self.meldViewTable.sendCardButton.isHidden && !self.meldViewTable.isTimeOut)
                {
                    [self.meldViewTable playerReconnectMeldHandlerForDisconnection];
                    [self removeMeldView];
                }
                else
                {
                    //[self removeMeldView];
                }
            }
            if (self.smartCorrectionView)
            {
                if (!self.smartCorrectionView.isTimeOut)
                {
                    [self.smartCorrectionView playerReconnectMeldHandlerForDisconnection];
                    [self removeSmartCorrectionView];
                }
                else
                {
                    //[self removeMeldView];
                }
            }
        }
    }
    else
    {
        if (self.meldViewTable)
        {
            if (self.meldViewTable.sendCardButton.isHidden && !self.meldViewTable.isTimeOut)
            {
                [self.meldViewTable playerReconnectMeldHandlerForDisconnection];
                [self removeMeldView];
            }
            else
            {
                //[self removeMeldView];
            }
        }
        if (self.smartCorrectionView)
        {
            if (!self.smartCorrectionView.isTimeOut)
            {
                [self.smartCorrectionView playerReconnectMeldHandlerForDisconnection];
                [self removeSmartCorrectionView];
            }
            else
            {
                //[self removeMeldView];
            }
        }
    }
    if (self.isSpectator)
    {
        [self quitGame];
    }
#if PLAYER_MODEL_GAME_CARD
    [self slideDownPlayerModel];
    self.playerModelButton.hidden = YES;
#endif
    
    [self.playTable removeOpenDeckHolderCards];
    [self.playTable removeMyDeckCardsFromHolder];
    [self.playerModelView removeMyDeckModelCardsFromHolder];
    //    [self refreshControllerForNewGameEnd];
    [self.playTable updateClosedDeckCardHolder:YES];
    [self removeTossCardsFromChair];
    [self.playTable removeJokerHolderCards];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeInfoLabelMessagePlayerMode];
    }
    [self.playTable removeInfoLabelMessage];
    [self.discardCardHistoryController clearArrayContents];
    [self playersAfterReconnectForDiscardCardHistory:self.playTableDictionary];
    //#389 bug fix
    if(self.infoPopUpController)
    {
        [self.infoPopUpController hide];
        self.infoPopUpController = nil;
    }
    
    if(self.chatViewController)
    {
        if(self.chatViewController.chatHistoryDataSource.count > 0)
        {
            [self.chatViewController clearChatHistory];
            [self.gameTableDelegate clearTheMessageDataSourceWhenReconnectedBack];
        }
    }
}

// Player disconnected notifications / implementation
- (void)playerDisconnected:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    self.isPlayerDisconnected = YES;
    if ([self.savedFirstTableID isEqualToString:self.tableIDString]) {
        //[self saveToCoreDataWithEvent:@"Engine Disconnect"];
    } else {
        //[self saveToCoreDataWithSecondTableEvent:@"Engine Disconnect"];
    }
    self.isReconnected = NO;
    self.isGetTableDetailsEventCame = NO;
    self.isSeatRequestPending = NO;
    [[NSNotificationCenter defaultCenter]postNotificationName:RESET_TIMER_NOTIFICATION object:dictionary];
    [self inValidateTimerIfRunning];
    [self.splitNSTimer invalidate];
    self.splitNSTimer = nil;
    [self.playerTurnView stopRunningTimer];
    if (self.meldViewTable)
    {
        [self.meldViewTable invalidateMeldTimer];
    }
    [self.meldViewTable playerReconnectMeldHandlerForDisconnection];
    [self removeMeldView];
    if (self.smartCorrectionView)
    {
        [self.smartCorrectionView invalidateMeldTimer];
    }
    [self.smartCorrectionView playerReconnectMeldHandlerForDisconnection];
    [self removeSmartCorrectionView];
    [self.scoreSheetViewController resetGameResultOnDisconnection];
}

// Rebuyin joker/non joker this player notifications
- (void)rebuyInOptionThisPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleRebuyInOptionThisPlayerEvent:dictionary];
    }
}

// Rebuyin joker/non joker other player notifications
- (void)rebuyInOptionOtherPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleRebuyInOptionOtherPlayerEvent:dictionary];
    }
}

// Best of 2, 3 and 6 winner notifications
- (void)bestOfWinner:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleBestOfWinnerEvent:dictionary];
    }
}

// Pool 101, 201 winner notifications
- (void)poolWinner:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handlePoolWinnerEvent:dictionary];
    }
}

// Split result notifications
- (void)splitResultForPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSplitResultForPlayerEvent:dictionary];
    }
}

// Split timeout for other player notifications
- (void)splitTimeOutForOtherPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSplitTimeOutForOtherPlayerEvent:dictionary];
    }
}

// Split reply for this player notifications
- (void)splitReplyForThisPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSplitReplyForThisPlayerEvent:dictionary];
    }
}

// Split request timer other player notifications
- (void)splitRequestForOtherPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSplitRequestForOtherPlayerEvent:dictionary];
    }
}

// Split request for this player notifications
- (void)splitRequestForThisPlayer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSplitRequestForThisPlayerEvent:dictionary];
    }
}

// Make player to rejoin same table notifications
- (void)rejoinToPlaySameTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleRejoinToPlaySameTableEvent:dictionary];
    }
}

// Player request for rejoin notifications
- (void)requestToRejoin:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleRequestToRejoinEvent:dictionary];
    }
}

// This player meld reply notification
- (void)thisPlayerMeldReply:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleThisPlayerMeldReplyEvent:dictionary];
    }
}

- (void)thisCheckPlayerMeldReply:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleThisCheckPlayerMeldReplyEvent:dictionary];
    }
}

// Pre-Game Result notification when current device player has placed a valid show
- (void)preGameResultLoad:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        self.isValidShowPreGameResult = YES;
        self.canLeaveTable = NO;
        [self handlePreGameResultLoadEvent:dictionary];
    }
}

// Player eliminate notification
- (void)playerEliminate:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handlePlayerEliminateEvent:dictionary];
    }
}

// This player meld failue notification
- (void)thisPlayerMeldFailure:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleThisPlayerMeldFailureEvent:dictionary];
    }
}

// Create meld view for other once this player meld success notification
- (void)createMeldForOtherPlayer:(NSNotification *)notification
{
    NSLog(@"CHECK OTHER VALID SHOW");
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleCreateMeldForOtherPlayer:dictionary];
    }
}

// This player Meld Success notification
- (void)thisPlayerMeldSuccess:(NSNotification *)notification
{
    NSLog(@"CHECK THIS PLAYER VALID SHOW");
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleThisPlayerMeldSuccessEvents:dictionary];
    }
}

// Player placing show notification
- (void)playerPlacingShowNotified:(NSNotification *)notification
{
    [self setDefaultsExtraTimers];
    [self setDefaultsChunks];
    
    NSLog(@"CHECK Player placing show");
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handlePlayerPlacingShowEvent:dictionary];
    }
}

- (void)smartCorrectionNotified:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSmartCorrectionEvent:dictionary];
    }
}

// Player Drop notification
- (void)playerDropGame:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handlePlayerDropEvent:dictionary];
    }
}

// Stack reshuffle notification
- (void)stackReshuffleNotify:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleStackReshuffleEvent:dictionary];
    }
}

// Meld extra time reconnect notification
- (void)meldExtraTimeReconnect:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleMeldExtraTimeReconnectEvent:dictionary];
    }
}

// Show extra time reconnect notification
- (void)showExtraTimeReconnect:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleShowExtraTimeReconnectEvent:dictionary];
    }
}

// Extra Turn time reconnect notification
- (void)extraTurnTimeReconnect:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleExtraTurnTimeReconnectEvent:dictionary];
    }
}

// Extra Turn time notification
- (void)extraTurnTimeNotify:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleExtraTurnTimeEvent:dictionary];
    }
}

// Turn Timeout notification
- (void)turnTimeoutNotified:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleTurnTimeOutEvent:dictionary];
    }
}

// Game end notification
- (void)gameEndNotified:(NSNotification *)notification
{
    NSLog(@"GAME END");
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleGameEndEvent:dictionary];
    }
}

// Game Result notification
- (void)gameResultAtEnd:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleGameResultEvent:dictionary];
    }
}

// Card pick Event notification
- (void)cardPick:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleCardPickEvent:dictionary];
    }
}

// For Card discard notification
-  (void)cardDiscard:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleCardDiscardEvent:dictionary];
    }
}

// For turn update notification
- (void)turnUpdate:(NSNotification *)notification
{
    DLog(@"turn update came");
    NSLog(@"CHECK DROP");
    
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleTurnUpdateEvent:dictionary];
        
        [self.playerShowTime invalidate];
        self.playerShowTime = nil;
        if (!self.isJokerTypeGame)
        {
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView performSelector:@selector(removeInfoLabelMessagePlayerMode) withObject:nil afterDelay:1.0];
            }
            [self.playTable removeInfoLabelMessage];
        }
        if (self.isJokerTypeGame)
        {
            self.isScheduleGameCame = NO;
        }
    }
}

// Show deal for spectator notification
- (void)showDealForSpectator:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleShowDealForSpectatorEvent:dictionary];
    }
}

// Send deal notification
- (void) sendDeal:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleSendDealEvent:dictionary];
        
    }
    if (self.splitRejectAlertView)
    {
        [self.splitRejectAlertView hide];
        self.splitRejectAlertView = nil;
    }
}

// Send stack notification
- (void)sendStack:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        // send bundle of closed cards
        [self handleSendStackData:dictionary];
    }
}

// Start Game notification
- (void)startGameEvent:(NSNotification *)notification
{
   // NSLog(@"GAME START : %@",[notification object]);
    
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        self.canShowIamBackView = YES;
        
        [self handleStartGameEvent:dictionary];
        [self updateGameIDAndPrize:dictionary];
        
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(informGameEngineForGameStarted:)])
            
        {
            NSLog(@"GAME STARTED");
            NSLog(@"GAME DETAILS : %@",dictionary);
            
            APP_DELEGATE.tableType = [dictionary valueForKey:@"TAJ_table_type"];
            NSLog(@"AUTODROP CHECK :%@",APP_DELEGATE.tableType);
            
            [CrashlyticsKit setObjectValue:[dictionary valueForKey:@"TAJ_table_id"] forKey:@"table_id"];
            [CrashlyticsKit setObjectValue:[dictionary valueForKey:@"TAJ_game_id"] forKey:@"game_id"];
            [CrashlyticsKit setObjectValue:[dictionary valueForKey:@"TAJ_table_cost"] forKey:@"chips_type"];
            [CrashlyticsKit setObjectValue:[dictionary valueForKey:@"TAJ_table_type"] forKey:@"chips_type"];
            
            [self.gameTableDelegate informGameEngineForGameStarted:[self.currentTableId integerValue]];
            
            //NSLog(@"CORE DATA : %@,%@,%@,%@,%@,%@",self.firstGameTableId,self.firstGameId,self.firstGameSubId,self.firstGameChipsType,self.firstGameBet,self.firstGameType);
           // NSLog(@"MY USER ID : %@",[[NSUserDefaults standardUserDefaults] objectForKey:USERID]);
            
            if ([self.savedFirstTableID isEqualToString:[dictionary valueForKey:@"TAJ_table_id"]]) {
                NSString *gameId, *gameSubId;
                if ([[dictionary valueForKey:@"TAJ_table_type"] isEqualToString:@"PR_JOKER"]) {
                    gameId = [dictionary valueForKey:@"TAJ_game_id"];
                    gameSubId = @"0";
                }
                else {
                    NSString *fullGameId = [dictionary valueForKey:@"TAJ_game_id"];  //is your str
                    
                    NSArray *items = [fullGameId componentsSeparatedByString:@"-"];   //take the one array for split the string
                    
                    gameId=[items objectAtIndex:0];   //shows Description
                    gameSubId=[items objectAtIndex:1];
                }
                
                
                self.firstGameTableId = [dictionary valueForKey:@"TAJ_table_id"];
                self.firstGameId = gameId;
                self.firstGameSubId = gameSubId;
                self.firstGameChipsType = [dictionary valueForKey:@"TAJ_table_cost"];
                self.firstGameType = [dictionary valueForKey:@"TAJ_table_type"];
                
                //[self saveToCoreDataWithEvent:@"Join Game"];
               // [self saveToCoreDataWithEvent:@"Start Game"];
                
            }
            else {
                NSString *gameId, *gameSubId;
                if ([[dictionary valueForKey:@"TAJ_table_type"] isEqualToString:@"PR_JOKER"]) {
                    gameId = [dictionary valueForKey:@"TAJ_game_id"];
                    gameSubId = @"0";
                }
                else {
                    NSString *fullGameId = [dictionary valueForKey:@"TAJ_game_id"];  //is your str
                    
                    NSArray *items = [fullGameId componentsSeparatedByString:@"-"];   //take the one array for split the string
                    
                    gameId=[items objectAtIndex:0];   //shows Description
                    gameSubId=[items objectAtIndex:1];
                }
                
                self.secondGameTableId = [dictionary valueForKey:@"TAJ_table_id"];
                self.secondGameId = gameId;
                self.secondGameSubId = gameSubId;
                self.secondGameChipsType = [dictionary valueForKey:@"TAJ_table_cost"];
                self.secondGameType = [dictionary valueForKey:@"TAJ_table_type"];
                
                [self saveToCoreDataWithSecondTableEvent:@"Join Game"];
                [self saveToCoreDataWithSecondTableEvent:@"Start Game"];
                
            }
            
        }
    }
}

// Sitting sequence notification
- (void)rearrangeChairs:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self rearrangeChairsWithData:dictionary];
    }
    
}

// Table toss notification
- (void)tableTossDidEnd:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self showTableTossResult:dictionary];
        if (!self.isJokerGameStarted && self.isJokerTypeGame && self.startTimerDuration <= 5.0)
        {
            [self disableChairs];
        }
    }
}

// Player quit table notification
- (void)playerDidQuitGame:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handlePlayerQuitGameEvent:dictionary];
    }
}

// Schedule game notification
- (void)scheduleGame:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    self.canAnimate = YES;
    [self saveResultForKey:NO];
    self.isGameResultEventCame = NO;
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        self.isSendDealCame = NO;
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(removeThisPlayerDiscardCardWhenScheduleGame:)])
        {
            [self.gameTableDelegate removeThisPlayerDiscardCardWhenScheduleGame:self.currentTableId];
        }
        self.isDisConnectReconnect = NO;
        self.isGameEnded = NO;
        [self inValidateTimerIfRunning];
        [self.playerShowTime invalidate];
        self.playerShowTime = nil;
        [self refreshChairView];
        [self removePlayerTurnTime];
        [self removeAllAutoPlayView];
        [self removeJokerTableView];
        
        [self removeTossCardsFromChair];
        double currentTime = [dictionary[GAME_TIMER_STAMP] doubleValue];
        double endTime = [dictionary[GAME_START_TIMER] doubleValue];
        double duration = endTime - currentTime;
        if(self.isDisConnectReconnect)
        {
            double loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
            DLog(@"loadTime %f",loadTime);
            duration = duration - round(loadTime);
            self.isDisConnectReconnect = NO;
        }
        
        [self startWaitingTimerForDuration:duration andData:dictionary];
        if (self.isJokerTypeGame && duration > 5.0 && !self.thisPlayerModel)
        {
            [self unSelectChair];
            if(self.isPlayerModelSlided)
            {
                //  [self.playerModelView showInstructionPlayerMode:JOKER_TABLE_SIT_MESSAGE];
            }
            // [self.playTable showInstruction:JOKER_TABLE_SIT_MESSAGE];
        }
        NSString *gameStarted ,*gameCount;
        gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
        gameCount = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamecount];
        if(!self.isJokerTypeGame && [gameCount intValue] == 0 && [gameStarted isEqualToString:@"False"] && !self.thisPlayerModel  && duration > 5.0 && !self.isFirstRoundGameAlreadyStarted)
        {
            [self unSelectChair];
            if(self.isPlayerModelSlided)
            {
                // [self.playerModelView showInstructionPlayerMode:JOKER_TABLE_SIT_MESSAGE];
            }
            // [self.playTable showInstruction:JOKER_TABLE_SIT_MESSAGE];
        }
        else if(!self.isJokerTypeGame )
        {
            [self disableChairs];
        }
        
        if ( [gameCount intValue] > 0)
        {
            self.canShowIamBackView =YES;
            if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(informGameEngineForGameStarted:)])
            {
                [self.gameTableDelegate informGameEngineForGameStarted:[self.currentTableId integerValue]];
            }
        }
        
        self.discardCardInfoForShow = nil;
        self.isStrikesMiddleJoin = NO;
        
        for(NSInteger chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
        {
            TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
            chair.isMiddleJoin = NO;
        }
    }
}

// Table close notification / implementation
- (void)closeTable:(NSNotification *)notification
{
    // self.isChatButtonShow = NO;
    //[self.gameTableDelegate informControllerGameEnded];
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    //NSLog(@"GAME END INFO : %@",dictionary);
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(informControllerGameEnded:)])
        {
            [self.gameTableDelegate informControllerGameEnded:self.currentTableId];
        }
        
        [self hideViewForNewGame];
        [self removeChairViews];
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(removeThisPlayerDiscardCardWhenScheduleGame:)])
        {
            [self.gameTableDelegate removeThisPlayerDiscardCardWhenScheduleGame:self.currentTableId];
        }
        self.canLeaveTable = YES;
        
        self.playerModelButton.hidden = YES;
        
        [self updateSortButton:NO];
        [self updateFlipCardButton:NO];
        [self updateExtraTimeButton:NO];
        
        [self removePlayerTurnTime];
        [self inValidateTimerIfRunning];
        [[NSNotificationCenter defaultCenter]postNotificationName:RESET_TIMER_NOTIFICATION object:dictionary];
        
        self.isCurrentTableClosed = YES;
        //        [self.playTable showInstruction:dictionary[kTaj_Table_close_msg]];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
        self.previousMessage = nil;
        NSString *message = TABLE_CLOSE_MESSAGE;
        NSLog(@"TABLE ENDED");
        // NSLog(@"ENDED DATA :%@",dictionary);
        
        NSDictionary * playersCountDict = @{
                                            @"table_id" : [dictionary objectForKey:@"TAJ_table_id"],
                                            @"maxplayer" : [NSNumber numberWithInteger:self.maxPlayers],
                                            @"count" : [NSNumber numberWithInt:0]
                                            };
        NSLog(@"GAME END POST DATA : %@",playersCountDict);
        NSLog(@"RESULT GAME END 2");
        
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_PLAYERS object:playersCountDict];
        
        if ([self.savedFirstTableID isEqualToString:[dictionary valueForKey:@"TAJ_table_id"]]) {
        //[self saveToCoreDataWithEvent:@"Game End"];
            //[self fetchDataFromCoreData];
        }
        else {
            //[self saveToCoreDataWithSecondTableEvent:@"Game End"];
            //[self fetchSecondTableDataFromCoreData];
        }
        
        if(self.isPlayerModelSlided)
        {
            
            [self.playerModelView performSelector:@selector(showInstructionPlayerMode:) withObject:message afterDelay:2.0f];
        }
        
        [self.playTable performSelector:@selector(showInstruction:) withObject:message afterDelay:2.0f];
        [self.playTable removeOpenDeckHolderCards];
        
        [self refreshChairView];
        [self performSelector:@selector(hideChairView) withObject:nil afterDelay:1.2f];
        [self performSelector:@selector(disableChairs) withObject:nil afterDelay:1.0];
        [self.playTable removeOpenDeckHolderCards];
        [self.playTable removeMyDeckCardsFromHolder];
        
        self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
        
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
        {
            [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
        }
        if(self.isPlayerModelSlided)
        {
            
            [self.playerModelView showInstructionPlayerMode:message];
        }
        
        [self.playTable showInstruction:message];
        
        if (self.meldViewTable)
        {
            [self removeMeldView];
        }
        if (self.smartCorrectionView)
        {
            [self removeSmartCorrectionView];
        }
        
        [self.playTable removeAllButtonsWithMeldGroup];
        [self.chatViewController updateChatPlayerModel:nil];
        // self.viewHolderForChatDiscardLiveFeed.userInteractionEnabled = YES;
        
#if PLAYER_MODEL_GAME_CARD
        [self.playerModelView resetPlayerModelView];
        [self.playerModelView removeAllModelButtons];
        [self.playerModelView removeMeldGroupAllButton];
#endif
        
    }
}

// De- Schedule game notification / implementation
- (void)descheduleGameOnce:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self inValidateTimerIfRunning];
        [self removePlayerTurnTime];
        [self hideViewForNewGame];
        self.isStrikesMiddleJoin = NO;
        [self updateExtraTimeButton:NO];
        [self performSelector:@selector(refreshChairsOnGameDeschedule) withObject:nil afterDelay:1.5];
        [[NSNotificationCenter defaultCenter]postNotificationName:RESET_TIMER_NOTIFICATION object:dictionary];
        self.startTimerDuration = 0;
        self.isGameScheduleState = YES;
        self.canLeaveTable = YES;
        if ([dictionary[kTaj_Deschedule_reason] isEqualToString:SPLIT_REQUESTED_MSG])
        {
            if(self.isPlayerModelSlided)
            {
                
                [self.playerModelView showInstructionPlayerMode:SPLIT_REQUESTED_MSG];
            }
            
            
            [self.playTable showInstruction:SPLIT_REQUESTED_MSG];
            
        }
        else
        {
            self.isFirstRoundGameAlreadyStarted = NO;
            NSString *message = PLAYER_LESS_THEN_MINIMUM;
            [self.playTable removeExtraMessage];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
                [self.playerModelView showInstructionPlayerMode:message];
            }
            
            [self.playTable showInstruction:message];
        }
        self.isScheduleGameCame = NO;
        
    }
    if (self.isJokerTypeGame && dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        self.spectatorView.hidden = YES;
        NSString *data = [dictionary valueForKey:TAJ_REASON_KEY];
        if (NSOrderedSame == [data compare:PLAYER_LESS_THEN_MINIMUM])
        {
            self.isFirstRoundGameAlreadyStarted = NO;
            [self.playTable removeExtraMessage];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            if (self.thisPlayerModel)
            {
                [self.playTable removeInfoLabelMessage];
                if(self.isPlayerModelSlided)
                {
                    //[self.playerModelView removeExtraMessagePlayerMode];
                    [self.playerModelView removeInfoLabelMessagePlayerMode];
                    [self.playerModelView showInstructionPlayerMode:PLAYER_LESS_THEN_MINIMUM];
                }
                
                [self.playTable showInstruction:PLAYER_LESS_THEN_MINIMUM];
                self.previousMessage = PLAYER_LESS_THEN_MINIMUM;
                [self refreshControllerForNewGameEnd];
                [self updateChair];
            }
            else
            {
                [self.playTable removeInfoLabelMessage];
                if(self.isPlayerModelSlided)
                {
                    [self.playerModelView removeInfoLabelMessagePlayerMode];
                    
                    //                    [self.playerModelView showInstructionPlayerMode:PLEASE_TAKE_SEAT_MESSAGE];
                }
                //                [self.playTable showInstruction:PLEASE_TAKE_SEAT_MESSAGE];
                self.previousMessage = PLEASE_TAKE_SEAT_MESSAGE;
                [self unSelectChair];
                [self updateChair];
                [self refreshChairsOnGameDeschedule];
            }
            
        }
    }
    else if([dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        self.isFirstRoundGameAlreadyStarted = NO;
        NSString *data = [dictionary valueForKey:TAJ_REASON_KEY];
        if (NSOrderedSame == [data compare:PLAYER_LESS_THEN_MINIMUM])
        {
            [self.playTable removeExtraMessage];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            if (self.thisPlayerModel)
            {   [self.playTable removeInfoLabelMessage];
                if(self.isPlayerModelSlided)
                {
                    [self.playerModelView removeInfoLabelMessagePlayerMode];
                    
                    [self.playerModelView showInstructionPlayerMode:PLAYER_LESS_THEN_MINIMUM];
                }
                
                [self.playTable showInstruction:PLAYER_LESS_THEN_MINIMUM];
                self.previousMessage = PLAYER_LESS_THEN_MINIMUM;
                [self refreshControllerForNewGameEnd];
                [self updateChair];
                
            }
            else
            {
                [self.playTable removeInfoLabelMessage];
                if(self.isPlayerModelSlided)
                {
                    [self.playerModelView removeInfoLabelMessagePlayerMode];
                    
                    //                    [self.playerModelView showInstructionPlayerMode:PLEASE_TAKE_SEAT_MESSAGE];
                }
                //                [self.playTable showInstruction:PLEASE_TAKE_SEAT_MESSAGE];
                self.previousMessage = PLEASE_TAKE_SEAT_MESSAGE;
                [self unSelectChair];
                [self updateChair];
                
            }
        }
    }
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    if (notification && self.chatViewController && !self.reportBugView)
    {
        NSDictionary *dict = notification.userInfo;
        float keyboardHeight = [dict[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
        
        self.keyBoardHig = keyboardHeight;
        CGRect oldframe = self.viewHolderForChatDiscardLiveFeed.frame;
        
        oldframe.origin.y -= keyboardHeight / 2;
        
        [UIView animateWithDuration:0.1f animations:^
         {
             self.viewHolderForChatDiscardLiveFeed.frame = oldframe;
         }
                         completion:^(BOOL finished)
         {
             self.isKeyBoardShow = YES;
         }];
    }
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    if (notification && self.chatViewController && !self.reportBugView)
    {
        NSDictionary *dict = notification.userInfo;
        float keyboardHeight = [dict[UIKeyboardFrameBeginUserInfoKey] CGRectValue].size.width;
        
        CGRect oldframe = self.viewHolderForChatDiscardLiveFeed.frame;
        oldframe.origin.y += keyboardHeight / 2;
        
        [self dropDownTheViewHolderForChat:oldframe];
    }
}

- (void)dropDownTheViewHolderForChat:(CGRect)oldframe
{
    [UIView animateWithDuration:0.1f animations:^
     {
         self.viewHolderForChatDiscardLiveFeed.frame = oldframe;
     }
                     completion:^(BOOL finished)
     {
         self.isKeyBoardShow = NO;
     }];
}

// Error code notification
- (void)errorWithCode:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        [self handleErrorWithCodeEvent:dictionary];
    }
}

#pragma mark - Notification implementation functions -
- (void)handleReportBugReply:(NSDictionary *)dictionary
{
    DLog(@"dictionary = %@",dictionary);
}

// Stand up this player reply implementation
- (void)handleStandUpThisPlayerReply:(NSDictionary *)dictionary
{
    //TODO:not implemented by server
}

// Send slots for this player reconnect implementation
- (void)handleSendSlotsForThisPlayerEvent:(NSArray *)arrayOfCards
{
    if (!arrayOfCards)
    {
        return;
    }
    DLog(@"send slot event came");
    self.isSendSlotsCame = YES;
    [self inValidateTimerIfRunning];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
    
    [self.playTable canDiscardCard:YES];
    
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView canModelDiscardCard:YES];
#endif
    
#if CARD_GROUPING_SLOTS
    
    self.slotCardInfoArray = [NSMutableArray array];
    
    [self fillNullObjectsForSlotArray:self.slotCardInfoArray];
    
    /*arrayOfCards is send_slot card information*/
    if (arrayOfCards.count == 13)
    {
        for (NSUInteger i = 0; i < arrayOfCards.count; i++)
        {
            if ([arrayOfCards[i] isKindOfClass:[NSDictionary class]])
            {
                [self.slotCardInfoArray replaceObjectAtIndex:[arrayOfCards[i][TAJ_SLOT_INDEX] integerValue] withObject:arrayOfCards[i]];
            }
        }
        //        DLog(@"slot cards afer reconnect %@",self.slotCardInfoArray);
    }
    else if(arrayOfCards.count == 14)
    {
        for (NSUInteger i = 0; i < arrayOfCards.count; i++)
        {
            if ([arrayOfCards[i] isKindOfClass:[NSDictionary class]])
            {
                [self.slotCardInfoArray replaceObjectAtIndex:[arrayOfCards[i][TAJ_SLOT_INDEX] integerValue] withObject:arrayOfCards[i]];
            }
        }
        //        DLog(@"slot before  array  %@",self.slotCardInfoArray);
        NSDictionary *cardTobeRemove = nil;
        for (NSUInteger j = 0; j < arrayOfCards.count; j++)
        {
            if ([arrayOfCards[j] isKindOfClass:[NSDictionary class]])
            {
                NSDictionary *Dict = arrayOfCards[j];
                if (![self isSlotExistWithCardNumber:Dict[kTaj_Card_Face_Key] withSuit:Dict[kTaj_Card_Suit_Key]])
                {
                    cardTobeRemove = [NSDictionary dictionaryWithDictionary:Dict];
                    break;
                }
                else
                {
                    continue;
                }
            }
        }
        //        DLog(@"card to be removed %@",cardTobeRemove);
        if (cardTobeRemove && self.myCardsDealArray.count <= 13)
        {
            [self removeSlotCardInfoCardno:cardTobeRemove[kTaj_Card_Face_Key] withSuit:cardTobeRemove[kTaj_Card_Suit_Key]];
        }
    }
    //    DLog(@"deals  array  %@",self.myCardsDealArray);
    
    [self.cardSlotManager initializeSlotArray:self.slotCardInfoArray];
    
    // update cards for table
    [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
    if (!self.isPlayerModelSlided)
    {
        [self.playTable updateMeldGroupButton];
    }
    [self.playerModelView updateMeldGroupModelButton];
    
#if PLAYER_MODEL_GAME_CARD
    // update cards for player model
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
    [self.playerModelView updateMeldGroupModelButton];
    //self.playerModelButton.hidden = NO; ReDIM changes
#endif
    // reset if meld table exist
    if (self.meldViewTable)
    {
        [self.meldViewTable setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
        
        [self.meldViewTable playerReconnectMeldHandler];
        
        [self.meldViewTable unHideMeldSendCardButton];
        
        self.meldViewTable.userInteractionEnabled = YES;
    }
    
    if (self.smartCorrectionView)
    {
        [self.smartCorrectionView setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
        
        [self.smartCorrectionView playerReconnectMeldHandler];
    }
#else
    
    self.myCardsDealArray = [NSMutableArray arrayWithArray:arrayOfCards];
    [self.playTable updateMyDeckWithCards:self.myCardsDealArray];
    
    if (self.meldViewTable)
    {
        [self.meldViewTable setupMyDeckCardsWithArray:self.myCardsDealArray withJokerNumber:self.jokerCardId];
        
        [self.meldViewTable playerReconnectMeldHandler];
    }
    
    if (self.smartCorrectionView)
    {
        [self.smartCorrectionView setupMyDeckCardsWithArray:self.myCardsDealArray withJokerNumber:self.jokerCardId];
        
        [self.smartCorrectionView playerReconnectMeldHandler];
    }
    
#endif
    
}

// Auto play status for this player implementation
- (void)handleAutoPlayStatusThisPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    // hide auto play view for this device player
    [self autoPlayPlayerViewAtPosition:[self.thisPlayerModel.placeID integerValue] wantToShow:NO];
    
    // set up message for live feed
    if (self.isSendDealCame)
    {
        if(dictionary[kTaj_nickname])
        {
            [dictionary setValue:self.thisPlayerModel.playerName forKey:kTaj_nickname];
            [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerConnectedBackMessage:dictionary]];
            
        }
    }
    
    
}

// Auto play status for other player implementation
- (void)handleAutoPlayStatusOtherPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    // hide auto play view for other player
    [self isPlayerCameToAutoPlayMode:dictionary];
    
    // set up message for live feed
    NSString *autoPlayStatus = dictionary[kTaj_autoplaystatus];
    if ([autoPlayStatus isEqualToString:@"True"])
    {
        
        if (self.isSendDealCame)
        {
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            [self.playTable removeExtraMessage];
            NSString * message = [NSString stringWithFormat:LIVE_FEED_PLAYER_DISCONNECTED,dictionary[kTaj_nickname]];
            self.previousMessage = self.playTable.currentMessage;
            [self.playTable hideInfoLabelMessage];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showExtraMessagePlayerMode:message];
            }
            [self.playTable showExtraMessage:message];
        }
    }
    else
    {
        if (self.isSendDealCame)
        {
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            [self.playTable removeExtraMessage];
            [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerConnectedBackMessage:dictionary]];
            
            NSString * message = [NSString stringWithFormat:PLAYER_RECONNECTED_MESSAGE_TABLE,dictionary[kTaj_nickname]];
            self.previousMessage = self.playTable.currentMessage;
            [self.playTable hideInfoLabelMessage];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showExtraMessagePlayerMode:message];
            }
            [self.playTable showExtraMessage:message];
        }
        
        
    }
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView performSelector:@selector(showInfoLabelMessagePlayerMode) withObject:nil afterDelay:2.1f];
    }
    [self.playTable performSelector:@selector(showInfoLabelMessage) withObject:nil afterDelay:2.1f];
    
}

// Rebuyin joker/ non joker this player implementation
- (void)handleRebuyInOptionThisPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    if (self.thisDevicePlayer)
    {
        UIView *chairHolder = [self chairHolderFromUserId:[[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue]];
        
        TAJChairView *chair = [self chairFromView:chairHolder];
        [chair setUpPlayerPoints:dictionary[kTaj_rebuyin_this_amt]];
    }
}

// Rebuyin joker/ non joker other player implementation
- (void)handleRebuyInOptionOtherPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    DLog(@"handleRebuyInOptionEvent-- rebuyin option-- %@",dictionary);
    
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    if (chairHolder)
    {
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chair = [self chairFromView:chairHolder];
            [chair setUpPlayerPoints:dictionary[kTaj_rebuyin_other_amt]];
        }
    }
}

// Best of 2, 3 and 6 winner  implementation
- (void)handleBestOfWinnerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    DLog(@"handleBestOfWinnerEvent--best of winner - %@",dictionary);
    
    // this event will fire to every one
    self.isWinGame = YES;
    
    self.canLeaveTable = YES;
    
    [self disableChairs];
    
    //Sound
    [[TAJSoundHandler sharedManager]playWinnersSound];
    
    // invalidate if any timer is running
    [self inValidateTimerIfRunning];
    
    if (!self.winnerScreenController)
    {
        NSMutableArray *winnerArray = [NSMutableArray array];
        [winnerArray addObject:dictionary];
        
        [self createWinnerScreen];
        [self.winnerScreenController updateWinnerScreen:winnerArray];
    }
    
    if (self.searchJoinAlertView)
    {
        // NO BUTTON Pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.searchJoinAlertView hide];
#else
        [self.searchJoinAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.searchJoinAlertView = nil;
    }
    
    // at this point display who is the winner
    if ([self.thisPlayerModel.playerID isEqualToString:dictionary[kTaj_winner_key]])
    {
        self.mainTitle = ALERT_LEVEL_CONGRATULATION_TITLE;
        self.messageTitle = ALERT_LEVEL_CHECKING_SUCCESS_TITLE;
    }
    else
    {
        self.mainTitle = ALERT_EMPTY_TITLE;
        self.messageTitle = ALERT_LEVEL_CHECKING_FAIL_TITLE;
    }
    
    // display message to all
    if (!self.searchJoinAlertView && !self.isSpectator)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        
        self.searchJoinAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_NOT_REJOIN_PLAY_TAG popupButtonType:eYes_No_Type message:self.messageTitle];
        
#else
        self.searchJoinAlertView = [[UIAlertView alloc] initWithTitle:self.mainTitle
                                                              message:self.messageTitle
                                                             delegate:self
                                                    cancelButtonTitle:YES_STRING
                                                    otherButtonTitles:NO_STRING, nil];
        
        self.searchJoinAlertView.tag = ALERT_CAN_NOT_REJOIN_PLAY_TAG;
#endif
        //[self.searchJoinAlertView show];
        
        //self
        
        [self alertPopUpDisplay];
    }
}

- (void) alertPopUpDisplay
{
    self.searchJoinAlertView.view.frame = self.view.frame;
    [self.view addSubview:self.searchJoinAlertView.view];
    [self.searchJoinAlertView frameSettingForContainerView];
    
}

// Pool 101, 201 winner implementation
- (void)handlePoolWinnerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    DLog(@"handlePoolWinnerEvent--pool wnner- %@",dictionary);
    
    // who has won the game they will get pool winner event
    // who has lost the game, this event wont be fire but server automatically eliminate them.
    // who ever lost, we are displaying sorry message for the lose player in PLAYER_QUIT event
    self.isWinGame = YES;
    
    self.canLeaveTable = YES;
    
    [self disableChairs];
    
    //Sound
    [[TAJSoundHandler sharedManager]playWinnersSound];
    
    // invalidate if any timer is running
    [self inValidateTimerIfRunning];
    
    if (!self.winnerScreenController)
    {
        NSMutableArray *winnerArray = [NSMutableArray array];
        [winnerArray addObject:dictionary];
        
        [self createWinnerScreen];
        [self.winnerScreenController updateWinnerScreen:winnerArray];
    }
    
    if (self.searchJoinAlertView)
    {
        // NO BUTTON Pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.searchJoinAlertView hide];
#else
        [self.searchJoinAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.searchJoinAlertView = nil;
    }
    
    // at this point display who is the winner
    if (!self.searchJoinAlertView && !self.isSpectator)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        
        self.searchJoinAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_NOT_REJOIN_PLAY_TAG popupButtonType:eYes_No_Type message:ALERT_LEVEL_CHECKING_SUCCESS_TITLE];
#else
        self.searchJoinAlertView = [[UIAlertView alloc] initWithTitle:ALERT_LEVEL_CONGRATULATION_TITLE
                                                              message:ALERT_LEVEL_CHECKING_SUCCESS_TITLE
                                                             delegate:self
                                                    cancelButtonTitle:YES_STRING
                                                    otherButtonTitles:NO_STRING, nil];
        self.searchJoinAlertView.tag = ALERT_CAN_NOT_REJOIN_PLAY_TAG;
#endif
        // [self.searchJoinAlertView show];
        [self alertPopUpDisplay];
    }
}

// Split result implementation
- (void)handleSplitResultForPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    DLog(@"handleSplitResultForPlayerEvent--split result- %@",dictionary);
    // at this point display split result
    self.isWinGame = YES;
    
    self.canLeaveTable = YES;
    [self inValidateTimerIfRunning];
    [self disableChairs];
    
    //Sound
    [[TAJSoundHandler sharedManager]playWinnersSound];
    
    if (self.searchJoinAlertView)
    {
        // NO BUTTON Pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.searchJoinAlertView hide];
#else
        [self.searchJoinAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.searchJoinAlertView = nil;
    }
    
    if (!self.winnerScreenController)
    {
        [self createWinnerScreen];
        [self.winnerScreenController updateSplitResult:[dictionary valueForKeyPath:kTaj_split_winners]];
    }
    if(self.isPlayerModelSlided)
    {
        
        //[self.playerModelView showInstructionPlayerMode:TABLE_CLOSE_MESSAGE];
        [self.playerModelView performSelector:@selector(showInstructionPlayerMode:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.2];
    }
    //[self.playTable showInstruction:TABLE_CLOSE_MESSAGE];
    [self.playTable performSelector:@selector(showInstruction:) withObject:TABLE_CLOSE_MESSAGE afterDelay:2.2];
    
    [self disableChairs];
    [self.playTable removeOpenDeckHolderCards];
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    
    [self refreshChairView];
    [self performSelector:@selector(hideChairView) withObject:nil afterDelay:1.2f];
    
    // at this point display who is the winner
    if (!self.searchJoinAlertView && !self.isSpectator)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        
        self.searchJoinAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_NOT_REJOIN_PLAY_TAG popupButtonType:eYes_No_Type message:ALERT_LEVEL_CHECKING_SUCCESS_TITLE];
        
#else
        self.searchJoinAlertView = [[UIAlertView alloc] initWithTitle:ALERT_LEVEL_CONGRATULATION_TITLE
                                                              message:ALERT_LEVEL_CHECKING_SUCCESS_TITLE
                                                             delegate:self
                                                    cancelButtonTitle:YES_STRING
                                                    otherButtonTitles:NO_STRING, nil];
        self.searchJoinAlertView.tag = ALERT_CAN_NOT_REJOIN_PLAY_TAG;
#endif
        //[self.searchJoinAlertView show];
        [self alertPopUpDisplay];
    }
}

// Split timeout for other player implementation
- (void)handleSplitTimeOutForOtherPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    DLog(@"handleSplitTimeOutForOtherPlayerEvent--rejected split- %@",dictionary);
    // at this point we need to show "player has rejected the split"
    
    if (![self.thisPlayerModel.playerName isEqualToString:dictionary[kTaj_split_status]] && !self.isSpectator)
    {
        NSString *message = [NSString stringWithFormat:ALERT_SPLIT_REJECT_TITLE,dictionary[kTaj_split_status]];
        if (!self.splitRejectAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            self.splitRejectAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_SPLIT_REJECT_TAG popupButtonType:eOK_Type message:message];
#else
            self.splitRejectAlertView =[[UIAlertView alloc]initWithTitle:ALERT_EMPTY_TITLE
                                                                 message:message
                                                                delegate:self
                                                       cancelButtonTitle:OK_STRING
                                                       otherButtonTitles: nil];
            self.splitRejectAlertView.tag = ALERT_SPLIT_REJECT_TAG;
#endif
            [self.splitRejectAlertView show];
        }
    }
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerSplitRejectedMessage:dictionary]];
}

// Split reply for this player player implementation
- (void)handleSplitReplyForThisPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    [self inValidateTimerIfRunning];
    DLog(@"handleSplitReplyForThisPlayerEvent--requested msg- %@",dictionary);
    if(self.isPlayerModelSlided)
    {
        
        //[self.playerModelView showInstructionPlayerMode:TABLE_CLOSE_MESSAGE];
        [self.playerModelView showInstructionPlayerMode:SPLIT_REQUESTED_MSG];
    }
    
    [self.playTable showInstruction:SPLIT_REQUESTED_MSG];
}

// Split request timer other player implementation
- (void)handleSplitRequestForOtherPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    DLog(@"handleSplitRequestForOtherPlayerEvent---split timer for other-- %@",dictionary);
    // at this point we need to show pop up for split-timer for other player
    
    if (!self.isSpectator)
    {
        self.splitMsgUUid = dictionary[TAJ_MSG_UUID];
        [self splitTimerForOtherPlayer:[dictionary[kTaj_split_timeout] doubleValue] andData:dictionary];
    }
    [self inValidateTimerIfRunning];
    if(self.isPlayerModelSlided)
    {
        
        //[self.playerModelView showInstructionPlayerMode:TABLE_CLOSE_MESSAGE];
        [self.playerModelView showInstructionPlayerMode:SPLIT_REQUESTED_MSG];
    }
    
    
    [self.playTable showInstruction:SPLIT_REQUESTED_MSG];
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerSplitRequestPrizeMessage:dictionary]];
}

// Split request for player implementation
- (void)handleSplitRequestForThisPlayerEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    DLog(@"handleSplitRequestForThisPlayerEvent--enable split button---- %@",dictionary);
    // at this point we need to hide/ unhide split button in scoresheet / game result
    if (!self.isSpectator &&
        self.scoreSheetViewController &&
        [self.scoreSheetViewController isShowing] &&
        [dictionary[kTaj_split_status] isEqualToString:kTaj_split_true])
    {
        [self.scoreSheetViewController showSplitButton];
    }
    else if (!self.isSpectator &&
             self.scoreSheetViewController &&
             [self.scoreSheetViewController isShowing] &&
             [dictionary[kTaj_split_status] isEqualToString:kTaj_split_false])
    {
        [self.scoreSheetViewController hideSplitButton];
        [self inValidateTimerIfRunning];
    }
    
}

// Make player to rejoin same table implementation
- (void)handleRejoinToPlaySameTableEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    DLog(@"handleRejoinToPlaySameTableEvent----success rejoin--- %@",dictionary);
    // at this point make player to play same table after rejoin
    // but you need to change total count of rejoined player
    // if NSTimer is running
    [self.rejoinNSTimer invalidate];
    self.rejoinAlertView = nil;
    self.rejoinTime = 0;
    
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    NSString *loggedInUserID = [[[TAJGameEngine sharedGameEngine] usermodel] userId];
    
    if ([userId isEqualToString:loggedInUserID])
    {
        self.isRejoinSuccessCame = YES;
    }
    // remove old drop image
    if (chairHolder)
    {
        NSInteger placeId = [self getPlaceIdFromChairUserID:[userId integerValue]];
        UIImageView * cardBackImageView;
        if (self.maxPlayers == TWO_PLAYER && placeId == 2)
        {
            placeId = 4;
        }
        //modified code
        if(placeId > 0 && placeId <= 6)
        {
            TAJChairView *chair = [self chairAtPlaceIndex:placeId];
            
            cardBackImageView = self.cardBackAnimationImageViewArray[placeId  - 1];
            cardBackImageView.hidden = YES;
            cardBackImageView.image = nil;
            [chair resetPlayerModel];
            [chair hideDropMessage];
            // [eachChair playerQuitTable:YES];
            chair.dropBoxImageView.hidden = YES;
            chair.dropTextLabel.hidden = YES;
        }
        
        NSString *quitPlaceID;
        if(self.quitPlayersId.count > 0)
        {
            for(int i = 0; i < self.quitPlayersId.count; i++)
            {
                quitPlaceID = [self.quitPlayersId objectAtIndex:i];
                if([quitPlaceID integerValue] == placeId)
                {
                    [self.quitPlayersId removeObjectAtIndex:i];
                }
                else
                {
                    continue;
                }
            }
        }
    }
    if (chairHolder.subviews && chairHolder.subviews.count > 0)
    {
        TAJChairView *chairView = chairHolder.subviews[0];
        if (dictionary[kTaj_rejoin_score] && [dictionary[kTaj_rejoin_status] isEqualToString:@"yes"])
        {
            [chairView setUpPlayerPoints:dictionary[kTaj_rejoin_score]];
        }
    }
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerRejoinMessage:dictionary]];
}

// Player request for rejoin implementation
- (void)handleRequestToRejoinEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    DLog(@"handleRequestToRejoinEvent---rejoin timer-- %@",dictionary);
    
    if (!self.isSpectator)
    {
        self.isRejoinSuccessCame =  NO;
        self.rejoinMsgUUid = dictionary[TAJ_MSG_UUID];
        [self rejoinTimerToJoin:[dictionary[kTaj_rejoin_timeout] doubleValue] andData:dictionary];
    }
}

- (void)handleSmartCorrectionEvent:(NSDictionary *)dictionary
{
    if (self.meldViewTable)
    {
        [self.meldViewTable invalidateMeldTimer];
    }
    [self removeMeldView];
    self.winnerAIImageView.hidden = NO;
    [self createSmartCorrectionView];
    self.smartCorrectionThisMsgUUid = dictionary[TAJ_MSG_UUID];
    [self.smartCorrectionView setupMyDeckCardsWithArray:dictionary withJokerNumber:self.jokerCardId];
    [self.smartCorrectionView setHidden:NO];
    double loadTime;
    if(self.isDisConnectReconnect)
    {
        loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
    }
    else
    {
        loadTime = 0;
    }
    [self.smartCorrectionView runMeldTimerwithDictionary:dictionary withTimeDeduct:loadTime];
}

// Player placing show event implementation
- (void)handlePlayerPlacingShowEvent:(NSDictionary *)dictionary
{
    
    NSLog(@"CHECK EVENT DATA : %@",dictionary);
//    int TAJ_user_id = [[dictionary objectForKey:@"TAJ_user_id"] intValue] ;
//    
//    int loginUserId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] intValue];
//    if (loginUserId == TAJ_user_id) {
//        
//         NSLog(@"CHECK MELD WINDOW IF");
        if (!dictionary)
        {
            return;
        }
        
        if ([self.playerTime isValid])
        {
            [self.playerTime invalidate];
        }
        if ([self.playerShowTime isValid])
        {
            [self.playerShowTime invalidate];
            self.showTimeoutTime = 0;
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeInfoLabelMessagePlayerMode];
            }
            [self.playTable removeInfoLabelMessage];
        }
        
        [self.playTable otherPlayerTurnUserInteration];
        
        [self updateFlipCardButton:NO];
        [self removePlayerTurnTime];
        
        [self.playTable keepDiscardCardAsClosedCard];
        [self.playTable removeAllButtonsExceptGroup];
        
        //[self.playTable removeAllButtons]; // TODO : carefull for the doing the Meld group button
        
#if PLAYER_MODEL_GAME_CARD
        // remove if any buttons exist in player model
        [self.playerModelView removeAllModelButtons];
#endif
        
        NSString *userId = dictionary[kTaj_userid_Key];
        UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
        
        if ([self isThisDevicePlayer:chairHolder])
        {
            if(!self.isPlayerModelSlided)
            {
                self.winnerAIImageView.hidden = NO;
            }
            self.meldThisMsgUUid = dictionary[TAJ_MSG_UUID];
            // create meld view
            if (!self.meldViewTable)
            {
                self.canLeaveTable = NO;
                self.leaveTableMessage = CANNOT_LEAVE_TABLE_MESSAGE;
                if (dictionary[kTaj_Card_Face_Key] && dictionary[kTaj_Card_Suit_Key])
                {
                    self.discardCardInfoForShow = [NSMutableDictionary dictionaryWithObjectsAndKeys:dictionary[kTaj_Card_Face_Key],kTaj_Card_Face_Key,
                                                   dictionary[kTaj_Card_Suit_Key],kTaj_Card_Suit_Key, nil];
                    
                }
                
                [self vibrateDevice];
                
                [self createMeldTableView];
                
                double loadTime;
                if(self.isDisConnectReconnect)
                {
                    loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
                    //                DLog(@"loadTime %f",loadTime);
                }
                else
                {
                    loadTime = 0;
                }
                
                [self.meldViewTable runMeldTimerwithDictionary:dictionary withTimeDeduct:loadTime];
                
                if (self.meldViewTable.meldTimer >= 10.0f)
                {
                    self.didSendMeldCards = NO;
                }
                
                if (self.didSendMeldCards)
                {
                    [self removeMeldView];
                }
                [self.playerModelView disablePlayerModeSlideButton];
                
            }
            else if (self.meldViewTable)
            {
                if (dictionary[kTaj_Card_Face_Key] && dictionary[kTaj_Card_Suit_Key])
                {
                    self.discardCardInfoForShow = [NSMutableDictionary dictionaryWithObjectsAndKeys:dictionary[kTaj_Card_Face_Key],kTaj_Card_Face_Key,
                                                   dictionary[kTaj_Card_Suit_Key],kTaj_Card_Suit_Key, nil];
                    
                }
                
                double loadTime;
                if(self.isDisConnectReconnect)
                {
                    loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
                    DLog(@"loadTime %f",loadTime);
                    self.isDisConnectReconnect = NO;
                }
                else
                {
                    loadTime = 0;
                }
                
                [self.meldViewTable runMeldTimerwithDictionary:dictionary withTimeDeduct:loadTime];
            }
            
            if (!self.isPlayerModelSlided)
            {
                [self.playTable updateMeldGroupButton];
            }
            [self.playerModelView updateMeldGroupModelButton];
            [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerRequestedShowMessage:dictionary]];
            [self.playerModelView disablePlayerModeSlideButton];
            
        }
        else
        {
            // tell some other players has placing show
            // schedule timer for player show for other player to get to know
            [self startTimerForOtherPlayerToShow:[dictionary[kTaj_playershow_timeout] floatValue] andData:dictionary];
            [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerRequestedShowMessage:dictionary]];
            
            NSString *message = [NSString stringWithFormat:SHOW_MESSAGE_OTHER, dictionary[kTaj_nickname]];
            if(self.isPlayerModelSlided)
            {
                
                [self.playerModelView showInstructionPlayerMode:message];
            }
            
            [self.playTable showInstruction:message];
        }
        
//    }
//    else {
//        [self.showButton setTitle: @"DECLARE" forState: UIControlStateNormal];
//
//        //[self.meldViewTable setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
////#if CARD_GROUPING_SLOTS
////        [self.showButton setTitle: @"DECLARE" forState: UIControlStateNormal];
////
////        [self.meldViewTable setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
////#else
////        [self.meldViewTable setupMyDeckCardsWithArray:self.myCardsDealArray withJokerNumber:self.jokerCardId];
////#endif
//        NSLog(@"CHECK MELD WINDOW ELSE");
//    }
}

- (void)handleThisPlayerMeldReplyEvent:(NSDictionary *)dictionary
{
    //    NSLog(@"handleThisPlayerMeldReplyEvent : %@", dictionary);
    if (!dictionary)
    {
        return;
    }
    
}

- (void)handleThisCheckPlayerMeldReplyEvent:(NSDictionary *)dictionary
{
    if (!dictionary){
        return;
    }
    [self.meldViewTable setScore:dictionary withGameId:self.gameIDString];
}

// Pre-Game Result implementation
- (void)handlePreGameResultLoadEvent:(NSDictionary *)dictionary
{
    //NSLog(@"PRE GAME RESULT : %@",dictionary);
    
    if (!dictionary)
    {
        return;
    }
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView showInstructionPlayerMode:@"Please wait for other player to meld"];
    }
    
    [self.playTable showInstruction:@"Please wait for other player to meld"];
    [self slideDownPlayerModel];
    self.sortButton.enabled = NO;
    [self.playerModelView removeMyDeckModelCardsFromHolder];
    [self.playTable removeMyDeckCardsFromHolder];
    self.playerModelButton.hidden =YES;
    if (!self.scoreSheetViewController)
    {
        // remove score board if already showing
        [self removeScoreBoard];
        
        // at very first time we need to load score sheet
        [self createScoreSheetView:dictionary];
    }
    else if(self.scoreSheetViewController)
    {
        // remove score board if already showing
        [self removeScoreBoard];
        
        // Reset new model
        self.scoreSheetViewController.scoreSheet = [[TAJScoreSheetModel alloc] initWithGameResult:dictionary];
        
        if (![self.scoreSheetViewController isShowing])
        {
            [self showScoreSheet];
            //[self.scoreSheetViewController reloadScoreSheet];
        }
        else
        {
            self.scoreSheetViewController.winner_AIImageView.hidden = NO;
            //[self.scoreSheetViewController reloadScoreSheet];
        }
        [self.scoreSheetViewController reloadScoreSheet];
    }
    
    [self disableUserToEnterMeldDuringAutoplay:YES];
    
    if (self.meldViewTable)
    {
        [self removeMeldView];
    }
    
    if (self.smartCorrectionView)
    {
        [self removeSmartCorrectionView];
    }
    [self.playTable removeMeldGroupAllButton];
    
}

// This player meld failue implementation
- (void)handleThisPlayerMeldFailureEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    [self.playTable clearCardsinClosedCard];
    
    NSString *userId = dictionary[kTaj_userid_Key];
    
    // set up card bg image for dropped player
    NSUInteger placeID = [self getPlaceIdFromChairUserID:[userId integerValue]];
    
    if (self.maxPlayers == TWO_PLAYER && placeID == 2)
    {
        // only for top of middle player in 2 player table
        placeID = 4;
    }
    [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeID] afterDelay:0.75f];
    
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    if (chairHolder)
    {
        // set up chair view
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            [chairView playerDropGame];
        }
        
        // disable user interation for drop player
        if ([self isThisDevicePlayer:chairHolder])
        {
            [self thisPlayerGameDrop];
        }
    }
    if ([self isThisDevicePlayer:chairHolder])
    {
        if (self.meldViewTable)
        {
            [self.meldViewTable updateMeldMessageLabel:THIS_PLAYER_WRONG_MELD];
        }
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
        self.previousMessage = self.playTable.currentMessage;
        if(self.isPlayerModelSlided)
        {
            
            [self.playerModelView showInstructionPlayerMode:THIS_PLAYER_WRONG_MELD];
        }
        [self.playTable showInstruction:THIS_PLAYER_WRONG_MELD];
        [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
        [self updateSortButton:NO];
    }
    else
    {
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeExtraMessagePlayerMode];
        }
        [self.playTable removeExtraMessage];
        NSString *wrongMeldMessage = [NSString stringWithFormat:OTHER_PLAYER_WRONG_MELD,dictionary[kTaj_nickname]];
        [self.playerShowTime invalidate];
        self.playerShowTime = nil;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
        self.previousMessage = self.playTable.currentMessage;
        if(self.isPlayerModelSlided)
        {
            
            [self.playerModelView showInstructionPlayerMode:wrongMeldMessage];
        }
        [self.playTable showInstruction:wrongMeldMessage];
        [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
    }
    
    if (self.maxPlayers == 6)
    {
        // if this player meld fails, place the discarded card in open deck
        NSString *openCardId = [NSString stringWithFormat:@"%@%@", dictionary[kTaj_Card_Face_Key], [dictionary[kTaj_Card_Suit_Key] uppercaseString]];
        
        [self.playTable placeFaceUpCard:openCardId];
    }
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerInvalidShowMessage:dictionary]];
}

// player eliminate implementation
- (void)handlePlayerEliminateEvent:(NSDictionary *)dictionary
{
    NSLog(@"ELIMINATION");
    if (!dictionary)
    {
        return;
    }
    //Vibrate
    [self vibrateDevice];
    [self.playTable clearCardsinClosedCard];
    
    NSString *userId = dictionary[kTaj_userid_Key];
    
    // set up card bg image for dropped player
    NSUInteger placeID = [self getPlaceIdFromChairUserID:[userId integerValue]];
    
    if (self.maxPlayers == TWO_PLAYER && placeID == 2)
    {
        // only for top of middle player in 2 player table
        placeID = 4;
    }
    
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    //Sound
    [[TAJSoundHandler sharedManager]playDropSound];
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDropOrTimeOutMessage:dictionary]];
    
    if (chairHolder)
    {
        UIImageView * cardBackImageViewOld,*cardBackImageViewNew;
        
        if(placeID > 0 && placeID <= 6)
        {
            if(placeID == 1)
            {
                if(self.isSpectator)
                {
                    cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeID  - 1];
                    cardBackImageViewNew.image = nil;
                    cardBackImageViewOld.hidden = YES;
                    cardBackImageViewNew = self.playerCardsBgImage[placeID  - 1];
                    cardBackImageViewNew.hidden = NO;
                    [self dropandLeaveTableEventAnimationForPlayer:placeID withImageView:cardBackImageViewNew];
                }
                else
                {
                    cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeID  - 1];
                    cardBackImageViewNew.image = nil;
                    cardBackImageViewOld.hidden = YES;
                    cardBackImageViewNew = self.playerCardsBgImage[placeID  - 1];
                    cardBackImageViewNew.hidden = YES;
                    self.thisPlayerCloseAnimationImageView.hidden = NO;
                    self.thisPlayerCloseAnimationImageView.animationImages = self.thisPlayerCloseAnimationArray;
                    self.thisPlayerCloseAnimationImageView.animationDuration = 0.5f;
                    self.thisPlayerCloseAnimationImageView.animationRepeatCount = 1;
                    [self.thisPlayerCloseAnimationImageView startAnimating];
                }
            }
            else
            {
                cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeID  - 1];
                cardBackImageViewNew.image = nil;
                cardBackImageViewOld.hidden = YES;
                cardBackImageViewNew = self.playerCardsBgImage[placeID  - 1];
                cardBackImageViewNew.hidden = NO;
                [self dropandLeaveTableEventAnimationForPlayer:placeID withImageView:cardBackImageViewNew];
            }
        }
        
    }
    
    [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeID] afterDelay:0.75f];
    if (chairHolder)
    {
        // set up chair view
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            [chairView playerDropGame];
        }
        
        // disable user interation for drop player
        if ([self isThisDevicePlayer:chairHolder])
        {
#if PLAYER_MODEL_GAME_CARD
            [self slideDownPlayerModel];
            self.playerModelButton.hidden = YES;
#endif
            [self thisPlayerGameDrop];
            NSString *message = [NSString stringWithFormat:@"Your show is invalid, you are currently dropped from the game."];
            self.previousMessage = self.playTable.currentMessage;
            if(self.isPlayerModelSlided)
            {
                
                [self.playerModelView showInstructionPlayerMode:message];
            }
            
            [self.playTable showInstruction:message];
            
            [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
            if (dictionary[kTaj_Card_Face_Key] && dictionary[kTaj_Card_Suit_Key])
            {
                self.lastOpenDeckCardDict = [NSMutableDictionary dictionary];
                [self.lastOpenDeckCardDict setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
                [self.lastOpenDeckCardDict setObject:[dictionary[kTaj_Card_Suit_Key] lowercaseString] forKey:kTaj_Card_Suit_Key];
                
            }
        }
        else
        {
            [self.playerShowTime invalidate];
            self.playerShowTime = nil;
            DLog(@"player eliminate event %@",dictionary);
            if (dictionary[kTaj_Card_Face_Key] && dictionary[kTaj_Card_Suit_Key])
            {
                self.lastOpenDeckCardDict = [NSMutableDictionary dictionary];
                [self.lastOpenDeckCardDict setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
                [self.lastOpenDeckCardDict setObject:[dictionary[kTaj_Card_Suit_Key] lowercaseString] forKey:kTaj_Card_Suit_Key];
                
                
            }
            DLog(@"player eliminate eventlast open deck %@",self.lastOpenDeckCardDict);
        }
    }
    DLog(@"handlePlayerEliminateEvent--- %@",dictionary);
    
}

// Create meld view for other once this player meld success implementation
- (void)handleCreateMeldForOtherPlayer:(NSDictionary *)dictionary
{
    [self setDefaultsChunks];
    [self setDefaultsExtraTimers];
    
    if (!dictionary)
    {
        return;
    }
    
    // set up message for live feed
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    self.meldOtherMsgUUid = dictionary[TAJ_MSG_UUID];
    
    if (!self.meldViewTable &&
        ![self isThisDevicePlayer:chairHolder])
    {
        [self updateShowButton:YES];
        NSString *message = [NSString stringWithFormat:MELD_OTHER_PLAYER,dictionary[kTaj_meld_success_user]];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showExtraMessagePlayerMode:message];
        }
        [self.playTable showExtraMessage:message];
        
        // remove discard card info for other player
        // bcoz other player has only 13 card so that no discard card information
        self.discardCardInfoForShow = nil;
        
        // create meld view for other player once my meld is success
        //[self createMeldTableView];
        
#if CARD_GROUPING_SLOTS
        [self.showButton setTitle: @"DECLARE" forState: UIControlStateNormal];
        
        [self.meldViewTable setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
#else
        [self.meldViewTable setupMyDeckCardsWithArray:self.myCardsDealArray withJokerNumber:self.jokerCardId];
#endif
        
        // run timer for other player to meld
        double loadTime;
        if(self.isDisConnectReconnect)
        {
            loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
            DLog(@"loadTime %f",loadTime);
        }
        else
        {
            loadTime = 0;
        }
        
        
        [self.meldViewTable runMeldTimerwithDictionary:dictionary withTimeDeduct:loadTime];
        
        // set up message for live feed
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerValidShowMessage:dictionary]];
    }
    else if(self.meldViewTable && ![self isThisDevicePlayer:chairHolder])
    {
        DLog(@"handleCreateMeldForOtherPlayer ");
        
        NSString *message = [NSString stringWithFormat:MELD_OTHER_PLAYER,dictionary[kTaj_meld_success_user]];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showExtraMessagePlayerMode:message];
        }
        [self.playTable showExtraMessage:message];
        
        // remove discard card info for other player
        // bcoz other player has only 13 card so that no discard card information
        self.discardCardInfoForShow = nil;
        
        // run timer for other player to meld
        double loadTime;
        if(self.isDisConnectReconnect)
        {
            loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
        }
        else
        {
            loadTime = 0;
        }
        
        
        [self.meldViewTable runMeldTimerwithDictionary:dictionary withTimeDeduct:loadTime];
        
        // set up message for live feed
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerValidShowMessage:dictionary]];
    }
    else if ([self isThisDevicePlayer:chairHolder] && [self.scoreSheetViewController isShowing])
    {
        // this player will have pre game result so display the timer to know the other player melding
        [self.scoreSheetViewController runPreGameResultTimer:dictionary];
    }
    [self.playerModelView disablePlayerModeSlideButton];
    if ( ![self isThisDevicePlayer:chairHolder])
    {
        [[TAJSoundHandler sharedManager] playBellSound];
        
        [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
        [self.playerModelView updateMeldGroupModelButton];
    }
}

// This player Meld Success implementation
- (void)handleThisPlayerMeldSuccessEvents:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    if (self.meldViewTable)
    {
        [self removeMeldView];
    }
    if(self.isPlayerModelSlided)
    {
        
        [self.playerModelView showInstructionPlayerMode:THIS_PLAYER_MELD_SUCCESS];
    }
    
    [self.playTable showInstruction:THIS_PLAYER_MELD_SUCCESS];
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerValidShowMessage:dictionary]];
}

// Player Drop implementation
- (void)handlePlayerDropEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    //Vibrate
    [self vibrateDevice];
    
    NSString *message = [NSString stringWithFormat:PLAYER_DROP_MESSAGE,dictionary[kTaj_nickname]];
    self.previousMessage = self.playTable.currentMessage;
    if(self.isPlayerModelSlided)
    {
        
        [self.playerModelView showInstructionPlayerMode:message];
    }
    
    [self.playTable showInstruction:message];
    [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
    
    //Sound
    [[TAJSoundHandler sharedManager]playDropSound];
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDropOrTimeOutMessage:dictionary]];
    
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    if (chairHolder)
    {
        NSInteger placeId = [self getPlaceIdFromChairUserID:[userId integerValue]];
        UIImageView * cardBackImageViewOld,*cardBackImageViewNew;
        
        if (self.maxPlayers == TWO_PLAYER && placeId == 2)
        {
            placeId = 4;
        }
        
        if(placeId > 0 && placeId <= 6)
        {
            if(placeId == 1)
            {
                if(self.isSpectator)
                {
                    cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                    cardBackImageViewNew.image = nil;
                    cardBackImageViewOld.hidden = YES;
                    cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                    cardBackImageViewNew.hidden = NO;
                    [self dropandLeaveTableEventAnimationForPlayer:placeId withImageView:cardBackImageViewNew];
                    
                }
                else
                {
                    cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                    cardBackImageViewNew.image = nil;
                    cardBackImageViewOld.hidden = YES;
                    cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                    cardBackImageViewNew.hidden = YES;
                    self.thisPlayerCloseAnimationImageView.hidden = NO;
                    self.thisPlayerCloseAnimationImageView.animationImages = self.thisPlayerCloseAnimationArray;
                    self.thisPlayerCloseAnimationImageView.animationDuration = 0.5f;
                    self.thisPlayerCloseAnimationImageView.animationRepeatCount = 1;
                    [self.thisPlayerCloseAnimationImageView startAnimating];
                }
            }
            else
            {
                cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                cardBackImageViewNew.image = nil;
                cardBackImageViewOld.hidden = YES;
                cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                cardBackImageViewNew.hidden = NO;
                [self dropandLeaveTableEventAnimationForPlayer:placeId withImageView:cardBackImageViewNew];
                
            }
        }
        
        // set up chair view
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            [chairView playerDropGame];
        }
        
        // disable user interation for drop player
        if ([self isThisDevicePlayer:chairHolder])
        {
            [self thisPlayerGameDrop];
#if PLAYER_MODEL_GAME_CARD
            [self slideDownPlayerModel];
            self.playerModelButton.hidden = YES;
#endif
        }
        
        [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeId] afterDelay:0.75f];
    }
    
}

- (void)thisPlayerGameDrop
{
    // remove deck of card
    [self.playTable removeMyDeckCardsFromHolder];
    [self updateSortButton:NO];
    self.isThisDevicePlayerDroppedGame = YES;
    [self.cardSlotManager removeAllSlotCards];
    [self.slotCardInfoArray removeAllObjects];
    [self.myCardsDealArray removeAllObjects];
    [self.playTable otherPlayerTurnUserInteration];
    
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView removeMyDeckModelCardsFromHolder];
#endif
    
    // disable user interation
    [self.playTable otherPlayerTurnUserInteration];
    
}

-(void)dropandLeaveTableEventAnimationForPlayer:(NSInteger)placeId withImageView:(UIImageView *)cardBackImageView
{
    cardBackImageView.image = nil;
    cardBackImageView.hidden = NO;
    UIImageView *tempImageView = cardBackImageView;
    if(self.isSpectator)
    {
        if(placeId > 0)
        {
            tempImageView.hidden = NO;
            NSMutableArray *animationPlayerArray = [NSMutableArray arrayWithArray:self.cardBackCloseAnimationPlayerArray[placeId - 1]];
            [self handleCloseAnimationForImageView:tempImageView andArray:animationPlayerArray];
        }
        
    }
    else
    {
        if(placeId > 1)
        {
            tempImageView.hidden = NO;
            NSMutableArray *animationPlayerArray = [NSMutableArray arrayWithArray:self.cardBackCloseAnimationPlayerArray[placeId - 1]];
            [self handleCloseAnimationForImageView:tempImageView andArray:animationPlayerArray];
        }
        
    }
}

- (void)handleCloseAnimationForImageView:(UIImageView *)cardBackImageView andArray:(NSArray *)animationArray
{
    
    cardBackImageView.animationImages = animationArray;
    cardBackImageView.animationDuration = 0.5f;
    cardBackImageView.animationRepeatCount = 1;
    [cardBackImageView startAnimating];
    
}

- (void)checkPlayerDropped:(NSDictionary *)dictionary
{
    if (dictionary)
    {
        if ([[dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] isKindOfClass:[NSArray class]] && [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST])
        {
            NSMutableArray *droppedPlayers = [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST];
            for (int i = 0; i < droppedPlayers.count; i++)
            {
                NSMutableDictionary *allPlayerDropped = droppedPlayers[i];
                [self placeDropImageOnChair:allPlayerDropped];
                
            }
        }
        else if ([[dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] isKindOfClass:[NSDictionary class]] && [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST])
        {
            NSMutableDictionary *singlePlayerDropped = [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST];
            [self placeDropImageOnChair:singlePlayerDropped];
        }
    }
}

- (void)placeDropImageOnChair:(NSMutableDictionary *)dictionary
{
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    if (dictionary && [dictionary[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:TABLE_LEAVE_TEXT])
    {
        // if any player leaves we didnt created the chair for drop list, we are creating chair for only player tag
        // So better we need to check the with place id
        TAJChairView *chairView = chairHolder.subviews[0];
        [chairView otherPlayerLeftGame];
        [chairView disableInteraction];
        NSInteger placeId = [self getPlaceIdFromChairUserID:[userId integerValue]];
        UIImageView * cardBackImageViewOld,*cardBackImageViewNew;
        
        if (self.maxPlayers == TWO_PLAYER && placeId == 2)
        {
            placeId = 4;
        }
        
        if(placeId > 0 && placeId <= 6)
        {
            cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
            cardBackImageViewOld.hidden = YES;
            cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
            cardBackImageViewNew.hidden = NO;
        }
        [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeId] afterDelay:0.75f];
    }
    else
    {
        if (chairHolder)
        {
            NSInteger placeId = [self getPlaceIdFromChairUserID:[userId integerValue]];
            UIImageView * cardBackImageViewOld,*cardBackImageViewNew;
            
            if (self.maxPlayers == TWO_PLAYER && placeId == 2)
            {
                placeId = 4;
            }
            
            if(placeId > 0 && placeId <= 6)
            {
                cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                cardBackImageViewOld.hidden = YES;
                cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                cardBackImageViewNew.hidden = NO;
            }
            
            // set up chair view
            if (chairHolder.subviews && chairHolder.subviews.count > 0)
            {
                TAJChairView *chairView = chairHolder.subviews[0];
                [chairView otherPlayerlayerDropGame];
            }
            [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeId] afterDelay:0.75f];
            if ([userId isEqualToString:self.thisPlayerModel.playerID])
            {
                self.isThisDevicePlayerDroppedGame = YES;
                [self.playTable removeMyDeckCardsFromHolder];
                [self updateSortButton:NO];
                [self.playTable otherPlayerTurnUserInteration];
                if([dictionary[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:ELIMINATE_TEXT])
                {
                    if (self.meldViewTable)
                    {
                        [self.meldViewTable updateMeldMessageLabel:THIS_PLAYER_WRONG_MELD];
                    }
                }
            }
        }
    }
}

// Player quit table implementation
- (void)handlePlayerQuitGameEvent:(NSDictionary *)dictionary
{
    self.isPlayerQuit = YES;
    if (!dictionary)
    {
        return;
    }
    
    if ([dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_PLAY_VALUE] ||[dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_MIDDLE_VALUE] )
    {
        NSString *userId = dictionary[kTaj_userid_Key];
        NSInteger placeId;
        [self.quitUsers addObject:userId];
        UIView *chairHolder = [self chairHolderFromUserIdForQuit:[userId integerValue]];
        //NSLog(@"player quit %d",userId);
        if(chairHolder == nil){
            return;
        }
        // Animation
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            placeId =[chairView.playerModel.placeID integerValue];
        }
        UIImageView * cardBackImageViewOld,*cardBackImageViewNew ;
        if (self.maxPlayers == TWO_PLAYER && placeId == 2)
        {
            placeId = 4;
        }
        // self.quitPlayersId = [NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%d",placeId]];
        if(placeId > 0 && placeId <= 6)
        {
            cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
            cardBackImageViewOld.image = nil;
            cardBackImageViewOld.hidden =YES;
            if(self.isSendDealCame)
            {
                cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                cardBackImageViewNew.image = nil;
                cardBackImageViewNew.hidden = NO;
            }
        }
        self.quitPlayersId = [NSMutableArray  arrayWithObject:[NSString stringWithFormat:@"%d",placeId]];
        if (!self.isSendDealCame)
        {
            [self dropandLeaveTableEventAnimationForPlayer:placeId withImageView:cardBackImageViewNew];
        }
        
        if (chairHolder)
        {
            if(self.isSendDealCame)
            {
                [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeId] afterDelay:0.75f];
            }
        }
        if(self.isJokerTypeGame && [userId isEqualToString:[[[TAJGameEngine sharedGameEngine] usermodel] userId]])
        {
            [self removeAllNotifications];
            if ([self.gameTableDelegate respondsToSelector:@selector(tableIDForQuit:)])
            {
                [self.gameTableDelegate tableIDForQuit:[self.currentTableId integerValue]];
            }
            TAJGameEngine *engine = TAJ_GAME_ENGINE;
            TAJLobby *lobby = engine.lobby;
            [lobby destroyTable:self.currentTableId];
            
        }
        //[self.chairsArray removeObjectAtIndex:placeId];
        // check is this player
        if ([self isThisDevicePlayer:chairHolder] && !self.isWinGame)
        {
            if (!self.isJokerTypeGame)
            {
                if (!self.searchJoinAlertView && !self.isSpectator)
                {
#if NEW_INFO_VIEW_IMPLEMENTATION
                    self.searchJoinAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_NOT_REJOIN_PLAY_TAG popupButtonType:eYes_No_Type message:ALERT_LEVEL_CHECKING_FAIL_TITLE];
#else
                    self.searchJoinAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                                          message:ALERT_LEVEL_CHECKING_FAIL_TITLE
                                                                         delegate:self
                                                                cancelButtonTitle:YES_STRING
                                                                otherButtonTitles:NO_STRING, nil];
                    self.searchJoinAlertView.tag = ALERT_CAN_NOT_REJOIN_PLAY_TAG;
#endif
                    NSLog(@"ELIMINATED");
                    if ([self.savedFirstTableID isEqualToString:[dictionary valueForKey:@"TAJ_table_id"]]) {
                       // [self saveToCoreDataWithEvent:@"Player Eliminate"];
                    }
                    else {
                       // [self saveToCoreDataWithSecondTableEvent:@"Player Eliminate"];
                    }
                    // [self.searchJoinAlertView show];
                    [self alertPopUpDisplay];
                }
            }
            if (self.isJokerTypeGame)
            {
                
#if NEW_INFO_VIEW_IMPLEMENTATION
                
                self.leaveForJokerAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_LEAVE_JOKER_TABLE_TAG popupButtonType:eOK_Type message:ALERT_JOKER_ELIMINATE_TITLE];
#else
                self.leaveForJokerAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                                         message:ALERT_JOKER_ELIMINATE_TITLE
                                                                        delegate:self
                                                               cancelButtonTitle:OK_STRING
                                                               otherButtonTitles:nil];
                self.leaveForJokerAlertView.tag = ALERT_LEAVE_JOKER_TABLE_TAG;
#endif
            }
            
            self.isWinGame = NO;
            
        }
        
        // update chair view for quit player
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            
            // call this function before the playerQuitTable: Because in playerQuitTable: function player model will be nil
            // Refresh chair
            if ([[self.playTableDictionary valueForKeyPath:GAME_NEW_TABLE_TABLETYPE] isEqualToString:kBEST_OF_6])
            {
                if (!self.isSendDealCame)
                {
                    [chairView playerQuitTableBeforeStartGame:self.isGameScheduleState];
                    
                }
                else
                {
                    [chairView playerQuitTable:YES];
                }
                chairView.playerModel = nil;
            }
            else if (self.isJokerTypeGame && [dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_MIDDLE_VALUE])
            {
                [chairView playerQuitTableBeforeStartGame:YES];
                [self performSelector:@selector(middleJoinedPlayerQuitTable:) withObject:[NSNumber numberWithInteger:placeId] afterDelay:1.0];
            }
            else
            {
                if (!self.isSendDealCame)
                {
                    [chairView playerQuitTableBeforeStartGame:self.isGameScheduleState];
                }
                else
                {
                    [chairView playerQuitTable:self.isGameScheduleState];
                }
                chairView.playerModel = nil;
                
            }
        }
        //Author: RK changes of the game quit
        if (self.isGameScheduleState && (TWO_PLAYER == self.maxPlayers) )
        {
            [self inValidateTimerIfRunning];        }
        else
        {
            //6 Player game - Game will continue among others for more than 2 players in a table. Proper drop image is shown in the table for the droppedplayer.
            if (!self.isSpectator)
            {
                if (self.isSendDealCame)
                {
                    NSString *playerLeftMessage = [NSString stringWithFormat:PLAYER_LEFT_MESSGAE,dictionary[kTaj_nickname]];
                    self.previousMessage = self.playTable.currentMessage;
                    if(self.isPlayerModelSlided)
                    {
                        
                        [self.playerModelView showInstructionPlayerMode:playerLeftMessage];
                        //[self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
                    }
                    
                    [self.playTable showInstruction:playerLeftMessage];
                    
                    [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
                }
                
            }
        }
    }
    NSString *gameCount = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamecount];
    
    if (!self.isJokerTypeGame && self.thisPlayerModel)
    {
        [self disableChairs];
    }
    else if (self.isJokerTypeGame && !self.isSendDealCame && !self.thisPlayerModel)
    {
        [self unSelectChair];
    }
    else if (self.isJokerTypeGame && self.thisPlayerModel)
    {
        [self disableChairs];
        
    }
    else if (!self.thisPlayerModel && !self.isSpectator)
    {
        [self unSelectChair];
    }
    else if (!self.isJokerTypeGame && self.isSpectator && !self.isSendDealCame && (self.isFirstRoundGameAlreadyStarted ||[gameCount intValue] >= 1))
    {
        [self disableChairs];
    }
    if (self.isBestOfSixTypeGame )
    {
        self.playTable.totalPlayers -- ;
        NSInteger totalPlayers =  self.playTable.totalPlayers;
        [self.playTable playerLeftTable:totalPlayers minimumPlayers:self.minPlayers isThisDevicePlayerSpectator:self.isSpectator];
    }
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerLeftTableMessage:dictionary]];
}

//update chair for middle joined player quits
- (void)middleJoinedPlayerQuitTable:(NSNumber *)placeId
{
    UIImageView * cardBackImageViewOld,*cardBackImageViewNew ;
    
    if([placeId integerValue]> 0 && [placeId integerValue]<= 6)
    {
        cardBackImageViewOld = self.cardBackAnimationImageViewArray[[placeId integerValue] - 1];
        cardBackImageViewOld.image = nil;
        cardBackImageViewOld.hidden =YES;
        if(self.isSendDealCame)
        {
            cardBackImageViewNew = self.playerCardsBgImage[[placeId integerValue]  - 1];
            cardBackImageViewNew.image = nil;
            cardBackImageViewNew.hidden = YES;
        }
    }
    
}

// Stack Reshuffle implementation
- (void)handleStackReshuffleEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    NSArray *cardStack = [dictionary valueForKeyPath:kFaceDown_Cards_Key];
    
    NSString *jokerCardNum;
    NSString *jokerCardType;
    if ([dictionary[kTaj_isJoker_card] isEqualToString:@"True"])
    {
        jokerCardNum = dictionary[kTaj_Card_Face_Key];
        jokerCardType = [dictionary[kTaj_Card_Suit_Key] uppercaseString];
        self.jokerCardNumber = jokerCardNum;
        self.jokerCardId = [NSString stringWithFormat:@"%@%@", jokerCardNum, jokerCardType];
    }
    
    if (cardStack)
    {
        NSMutableArray *reshuffleClosedDeckArray = [NSMutableArray array];
        
        for (NSDictionary *cardInfo in cardStack)
        {
            NSMutableDictionary *cardDictionary = [NSMutableDictionary dictionaryWithDictionary:cardInfo];
            [reshuffleClosedDeckArray addObject:cardDictionary];
        }
        
        self.closedDeckArray = [NSMutableArray arrayWithArray:reshuffleClosedDeckArray];
        
        [self.playTable refreshOpenDeckAfterReshuffle];
    }
    
    if (!self.reshuffleAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        
        self.reshuffleAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_RESHUFFLE_TAG popupButtonType:eOK_Type message:ALERT_STACK_RESHUFFLE_TITLE];
#else
        self.reshuffleAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                             message:ALERT_STACK_RESHUFFLE_TITLE
                                                            delegate:self
                                                   cancelButtonTitle:OK_STRING
                                                   otherButtonTitles: nil];
        
        self.reshuffleAlertView.tag = ALERT_RESHUFFLE_TAG;
#endif
        [self.reshuffleAlertView show];
    }
}

// Meld extra time reconnect implementation
- (void)handleMeldExtraTimeReconnectEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    //TODO: meld extra time reconnect
    //kTaj_playershow_timeout
    
    // According to document
    // while some one is in meld screen , if he lose internet connection and connected back
    // this event will fire who is not in meld screen
    
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    if (![self isThisDevicePlayer:chairHolder])
    {
        // for other player
        
        // tell some other players has placing show
        // schedule timer for player show for other player to get to know
        [self startTimerForOtherPlayerToShow:[dictionary[kTaj_playershow_timeout] floatValue] andData:dictionary];
        
        NSString *message = [NSString stringWithFormat:SHOW_MESSAGE_OTHER, dictionary[kTaj_nickname]];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:message];
        }
        [self.playTable showInstruction:message];
    }
}

// Show extra time reconnect implementation
- (void)handleShowExtraTimeReconnectEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    
    // According to document
    // while some one is in meld screen , if he lose internet connection and connected back
    // this event will fire who is not in meld screen
    
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    if (![self isThisDevicePlayer:chairHolder])
    {
        // for other player
        
        // tell some other players has placing show
        // schedule timer for player show for other player to get to know
        [self startTimerForOtherPlayerToShow:[dictionary[kTaj_playershow_timeout] floatValue] andData:dictionary];
        
        NSString *message = [NSString stringWithFormat:SHOW_MESSAGE_OTHER, dictionary[kTaj_nickname]];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:message];
        }
        
        [self.playTable showInstruction:message];
    }
}

// Extra turn time reconnect implementation
- (void)handleExtraTurnTimeReconnectEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    NSString *autoPlayStatus = dictionary[kTaj_autoPlay_status];
    if (dictionary[kTaj_autoPlay_status]/*check first this key is exist or not*/ && [autoPlayStatus isEqualToString:@"True"])
    {
        DLog(@"extra time reconnect came");
        
        NSString *playerName = dictionary[kTaj_nickname];
        NSString *extraMessage = nil;
        if ([self.thisPlayerModel.playerName isEqualToString:playerName])
        {
            // this device player taken extra time
            [self updateExtraTimeButton:NO];
            [self updateFlipCardButton:NO];
            [self updateDropButton];
            [self setIsMyTurn:YES];
            [self.playTable canDiscardCard:YES];
            self.isAlertTobeShown = YES;
            
#if PLAYER_MODEL_GAME_CARD
            // Remove if any button exist in player model view
            [self.playerModelView canModelDiscardCard:YES];
#endif
            if(self.chatViewController)
            {
                if(self.chatMenuView)
                {
                    [self removeChatMenuViewController];
                }
                else
                {
                    [self.gameTableDelegate removeChatExtentViewController];
                }
            }
            if(self.chatMenuView)
            {
                [self removeChatMenuViewController];
            }
            [self.playTable thisPlayerTurnUserInteration];
            
            //schedule timer for this player
            [dictionary setValue:self.thisPlayerModel.playerID forKey:kTaj_userid_Key];
            [self startTimerForPlayerToDiscardCard:[dictionary[kTaj_player_timeout] floatValue] andData:dictionary];
            // set auto play status false for the autoplay view
            [dictionary setValue:@"False" forKey:kTaj_autoplaystatus];
            [self isPlayerCameToAutoPlayMode:dictionary];
        }
        else
        {
            self.isAlertTobeShown = NO;
            
            // other player taken extra time
            [self updateExtraTimeButton:NO];
            [self updateFlipCardButton:NO];
            self.isAlertTobeShown = NO;
            [self updateDropButton];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            [self.playTable removeExtraMessage];
            if(self.discardCardHistoryController)
            {
                self.discardCardHistoryController.cardsScrolView.userInteractionEnabled = YES;
            }
            
            extraMessage = [NSString stringWithFormat:EXTRATIME_OTHERPLAYER_INFO,dictionary[kTaj_nickname]];
            self.previousMessage = self.playTable.currentMessage;
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeInfoLabelMessagePlayerMode];
                [self.playerModelView showInstructionPlayerMode:extraMessage];
            }
            
            [self.playTable removeInfoLabelMessage];
            [self.playTable showInstruction:extraMessage];
            [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
            //schedule timer for other player
            [self startTimerForPlayerToDiscardCard:[dictionary[kTaj_player_timeout] floatValue] andData:dictionary];
            
            // set auto play status false for the autoplay view
            [dictionary setValue:@"False" forKey:kTaj_autoplaystatus];
            [self isPlayerCameToAutoPlayMode:dictionary];
        }
    }
    
}
- (void)removePlayTableMessage
{
    if (self.previousMessage !=nil && ([self.previousMessage isEqualToString:PLEASE_TAKE_SEAT_MESSAGE] || [self.previousMessage isEqualToString:SPLIT_REQUESTED_MSG] || [self.previousMessage isEqualToString:PLAYER_LESS_THEN_MINIMUM] ||[self.previousMessage isEqualToString:JOKER_TABLE_SIT_CONFIRM] ||[self.previousMessage isEqualToString:JOKER_TABLE_SIT_MESSAGE]))
    {
        if(self.isPlayerModelSlided)
        {
            
            [self.playerModelView showInstructionPlayerMode:self.previousMessage];
        }
        [self.playTable showInstruction:self.previousMessage];
        
    }
    else
    {
        if(self.isPlayerModelSlided)
        {
            
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        
        [self.playTable removeInfoLabelMessage];
        
    }
    
}

// Extra turn time implementation
- (void)handleExtraTurnTimeEvent:(NSDictionary *)dictionary
{
    NSLog(@"handleExtraTurnTimeEvent :%@",dictionary);

    if (!dictionary)
    {
        return;
    }
    
    // updated if this player choosen
    NSString *playerName = dictionary[kTaj_nickname];
    NSString *extraMessage = nil;
    if ([self.thisPlayerModel.playerName isEqualToString:playerName])
    {
        // this device player taken extra time
        if(self.chatViewController)
        {
            if(self.chatMenuView)
            {
                [self removeChatMenuViewController];
            }
            else
            {
                [self.gameTableDelegate removeChatExtentViewController];
            }
        }
        self.isAlertTobeShown = YES;
        self.isThisDeviceTurn = YES;
        [self setIsMyTurn:YES];
        [self updateExtraTimeButton:NO];
        [self updateFlipCardButton:NO];
        [self updateDropButton];
        
        [self.playTable thisPlayerTurnUserInteration];
        
        [self.playTable canDiscardCard:YES];
        
#if PLAYER_MODEL_GAME_CARD
        [self.playerModelView canModelDiscardCard:YES];
#endif
        
        //schedule timer for this player
        [dictionary setValue:self.thisPlayerModel.playerID forKey:kTaj_userid_Key];
        [self startTimerForPlayerToDiscardCard:[dictionary[kTaj_player_timeout] floatValue] andData:dictionary];
    }
    else
    {
        self.isAlertTobeShown = NO;
        
        // other player taken extra time
        if(self.discardCardHistoryController)
        {
            self.discardCardHistoryController.cardsScrolView.userInteractionEnabled = YES;
        }
        self.isThisDeviceTurn = NO;
        self.isAlertTobeShown = NO;
        [self updateExtraTimeButton:NO];
        [self updateFlipCardButton:NO];
        [self updateDropButton];
        
        [self.playTable canDiscardCard:NO];
        
#if PLAYER_MODEL_GAME_CARD
        [self.playerModelView canModelDiscardCard:NO];
#endif
        
        extraMessage = [NSString stringWithFormat:EXTRATIME_OTHERPLAYER_INFO,dictionary[kTaj_nickname]];
        self.previousMessage = self.playTable.currentMessage;
        [self.playTable hideInfoLabelMessage];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showExtraMessagePlayerMode:extraMessage];
            [self.playerModelView performSelector:@selector(showInfoLabelMessagePlayerMode) withObject:nil afterDelay:2.0f];
        }
        [self.playTable showExtraMessage:extraMessage];
        [self.playTable performSelector:@selector(showInfoLabelMessage) withObject:nil afterDelay:2.0f];
        
        //schedule timer for other player
        [self startTimerForPlayerToDiscardCard:[dictionary[kTaj_player_timeout] floatValue] andData:dictionary];
    }
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerExtraTimeMessage:dictionary]];
}

// Turn time out implementation
- (void)handleTurnTimeOutEvent:(NSDictionary *)dictionary
{
    NSLog(@"handleTurnTimeOutEvent Data : %@",dictionary);
    
    if (!dictionary)
    {
        return;
    }
    [self.playTable removeAllButtons];
    self.isAlertTobeShown = NO;
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDropOrTimeOutMessage:dictionary]];
    self.isAlertTobeShown = NO;
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    if (chairHolder)
    {
        NSInteger placeId = [self getPlaceIdFromChairUserID:[userId integerValue]];
        UIImageView * cardBackImageViewOld,*cardBackImageViewNew;
        
        if (self.maxPlayers == TWO_PLAYER && placeId == 2)
        {
            placeId = 4;
        }
        if(placeId > 0 && placeId <= 6)
        {
            if(placeId == 1)
            {
                if(self.isSpectator)
                {
                    cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                    cardBackImageViewNew.image = nil;
                    cardBackImageViewOld.hidden = YES;
                    cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                    cardBackImageViewNew.hidden = NO;
                    [self dropandLeaveTableEventAnimationForPlayer:placeId withImageView:cardBackImageViewNew];
                    
                }
                else
                {
                    cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                    cardBackImageViewNew.image = nil;
                    cardBackImageViewOld.hidden = YES;
                    cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                    cardBackImageViewNew.hidden = YES;
                    self.thisPlayerCloseAnimationImageView.hidden = NO;
                    self.thisPlayerCloseAnimationImageView.animationImages = self.thisPlayerCloseAnimationArray;
                    self.thisPlayerCloseAnimationImageView.animationDuration = 0.5f;
                    self.thisPlayerCloseAnimationImageView.animationRepeatCount = 1;
                    [self.thisPlayerCloseAnimationImageView startAnimating];
                }
            }
            else
            {
                cardBackImageViewOld = self.cardBackAnimationImageViewArray[placeId  - 1];
                cardBackImageViewNew.image = nil;
                cardBackImageViewOld.hidden = YES;
                cardBackImageViewNew = self.playerCardsBgImage[placeId  - 1];
                cardBackImageViewNew.hidden = NO;
                [self dropandLeaveTableEventAnimationForPlayer:placeId withImageView:cardBackImageViewNew];
                
            }
        }
        
        // set up chair view
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            [chairView playerDropGame];
        }
        // disable user interation for drop player
        if ([self isThisDevicePlayer:chairHolder])
        {
#if PLAYER_MODEL_GAME_CARD
            [self slideDownPlayerModel];
            self.playerModelButton.hidden = YES;
#endif
            
            [self vibrateDevice];
            
            [self thisPlayerGameDrop];
            [self.gameTableDelegate hideTableAlertImageViewWhenTurnTimeOut];
        }
        
        [self performSelector:@selector(placeCardDropImage:) withObject:[NSNumber numberWithInt:placeId] afterDelay:0.75f];
        
        //extra time
        //[self runExtraTimerWithPalyerTurn:placeId extraTime:0];
        //end here
    }
    
    
    if(!self.thisPlayerModel)
    {
        self.sortButton.enabled = NO;
    }
}

// Game End implementation
- (void)handleGameEndEvent:(NSDictionary *)dictionary
{
    
    if (!dictionary)
    {
        return;
    }
    self.isGameEnded = YES;
    self.isSendDealCame = NO;
    // refresh controller
    [self hideViewForNewGame];
    [self refreshControllerForNewGameEnd];
    DLog(@"game end came");
    //NSLog(@"GAME END CAME");
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(informGameEngineForGameEnded:)])
    {
        [self.gameTableDelegate informGameEngineForGameEnded:[self.currentTableId integerValue]];
    }
    
    self.canLeaveTable = YES;
    
    [self removePlayerTurnTime];
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    
    
    if (self.isPlayerModelSlided)
    {
        self.playerModelButton.hidden = YES;
        [self slideDownPlayerModel];
    }
    else
    {
        self.playerModelButton.hidden = YES;
    }
}

// Game Result implementation
- (void)updateGameResultOnReconnection:(NSDictionary *)dictionary
{
    // Game result / score sheet
    if (!self.scoreSheetViewController)
    {
        // create new score sheet at final stage to know the final result
        [self createScoreSheetView:dictionary];
    }
    else if(self.scoreSheetViewController)
    {
        // Reset new model
        self.scoreSheetViewController.scoreSheet = [[TAJScoreSheetModel alloc] initWithGameResult:dictionary];
        
        if (![self.scoreSheetViewController isShowing])
        {
            [self showScoreSheet];
        }
        [self.scoreSheetViewController reloadScoreSheet];
    }
    
    if (!self.isGameResultEventCame)
    {
        [self removeScoreSheetView];
    }
    
}

- (void)handleGameResultEvent:(NSDictionary *)dictionary
{
    //NSLog(@"GAME RESULT : %@",dictionary);
    // [self removeAllDiscardCardHistoryContent];
    self.isAlertTobeShown = NO;
    
    if ([self.gameTableDelegate respondsToSelector:@selector(hideAlertImageWhenOtherTurn)])
    {
        [self.gameTableDelegate hideAlertImageWhenOtherTurn];
    }
    //if this player meld is success 2 times we need to show scoresheet
    //one case at pre game result event (handlePreGameResultLoadEvent: )
    //another case is at game result (handleGameResultEvent: )
    self.isGameResultEventCame = YES;
    // if this player meld is failure 1 time score sheet will be load
    
    if (!dictionary)
    {
        return;
    }
    if ([self.playerShowTime isValid])
    {
        [self.playerShowTime invalidate];
        self.showTimeoutTime = 0;
    }
    self.isAlertTobeShown = NO;
    if ([self.gameTableDelegate respondsToSelector:@selector(hideAlertImageWhenOtherTurn)])
    {
        [self.gameTableDelegate hideAlertImageWhenOtherTurn];
    }
    if (self.meldViewTable)
    {
        [self removeMeldView];
    }
    if (self.smartCorrectionView)
    {
        [self removeSmartCorrectionView];
    }
    [self hideViewForNewGame];
    [self removeReportBugView];
    [self resetControllerForGameResult];
    self.canLeaveTable = YES;
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeInfoLabelMessagePlayerMode];
    }
    [self.playTable removeInfoLabelMessage];
    self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
    [self.playTable removeOpenDeckHolderCards];
    self.isPlayerDisconnected = NO;
    [self updateSortButton:NO];
    [self updateFlipCardButton:NO];
    [self removeInstanceAlertViews];
    [self removeScoreBoard];
    [self refreshChairView];
    
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    for(NSInteger chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        chair.isMiddleJoin = NO;
    }
    TAJUserModel *model = [[TAJGameEngine sharedGameEngine] usermodel];
    
    // Score board
    if (self.boardScoreController)
    {
        if ([dictionary[kTaj_scoresheet_player] isKindOfClass:[NSArray class]])
        {
            //            DLog(@"game result came %@ ",[dictionary[kTaj_scoresheet_player] valueForKey:kTaj_scoresheet_userid] );
            
            if (self.initialPlayerArray.count==0)
            {
                [self.initialPlayerArray addObjectsFromArray:[dictionary[kTaj_scoresheet_player] valueForKey:kTaj_scoresheet_userid]];
            }
            
            if (self.currentPlayerArray.count>0)
            {
                [self.currentPlayerArray removeAllObjects];
            }
            
            [self.currentPlayerArray addObjectsFromArray:[dictionary[kTaj_scoresheet_player] valueForKey:kTaj_scoresheet_userid]];
            
            NSMutableArray* emptySeats=[NSMutableArray array];
            
            if (self.currentPlayerArray.count != self.initialPlayerArray.count)
            {
                for (int i=0; i<self.initialPlayerArray.count; i++)
                {
                    if (![self.currentPlayerArray containsObject:self.initialPlayerArray[i]])
                    {
                        [emptySeats addObject:[NSString stringWithFormat:@"%d",i]];
                    }
                }
            }
            DLog(@"Empty seats %@",emptySeats);
            if ([self isUserExistInArray:dictionary[kTaj_scoresheet_player]])
            {
                if (!self.boardScoreController.playerScoreModel)
                {
                    if (self.isJokerTypeGame)
                    {
                        //For strikes
                        NSMutableDictionary * dictionaryWithNickName = [NSMutableDictionary dictionaryWithDictionary:dictionary];
                        if(!self.isSpectator)
                        {
                            [dictionaryWithNickName setObject:model.nickName forKey:kTaj_nick_name];
                        }
                        self.boardScoreController.playerScoreModel = [[TAJPlayerScoreBoardModel alloc] initWithPlayerLevelPoints:Nil dictionary:dictionaryWithNickName type:eStrikesType];
                    }
                    else
                    {
                        //For deals and pools
                        if ([dictionary[kTaj_scoresheet_player] isKindOfClass:[NSArray class]])
                        {
                            self.boardScoreController.playerScoreModel = [[TAJPlayerScoreBoardModel alloc] initWithPlayerLevelPoints:dictionary[kTaj_scoresheet_player] dictionary:Nil type:ePoolOrDealType];
                        }
                    }
                }
                else // if model not created
                {
                    if (self.isJokerTypeGame)
                    {
                        //For strikes
                        NSMutableDictionary * dictionaryWithNickName = [NSMutableDictionary dictionaryWithDictionary:dictionary];
                        if(!self.isSpectator)
                        {
                            [dictionaryWithNickName setObject:model.nickName forKey:kTaj_nick_name];
                        }
                        
                        [self.boardScoreController.playerScoreModel addLevelData:[NSMutableDictionary dictionaryWithDictionary:dictionaryWithNickName]];
                    }
                    else
                    {
                        //For deals and pools
                        if ([dictionary[kTaj_scoresheet_player] isKindOfClass:[NSArray class]])
                        {
                            self.boardScoreController.playerScoreModel = [self.boardScoreController.playerScoreModel addEachLevelPlayerPoints:dictionary[kTaj_scoresheet_player]];
                        }
                    }
                }
            }
        }
    }
    
    // Game result / score sheet
    if (!self.scoreSheetViewController)
    {
        // remove score board if already showing
        [self removeScoreBoard];
        
        // create new score sheet at final stage to know the final result
        [self createScoreSheetView:dictionary];
    }
    else if(self.scoreSheetViewController)
    {
        // remove score board if already showing
        [self removeScoreBoard];
        
        // Reset new model
        self.scoreSheetViewController.scoreSheet = [[TAJScoreSheetModel alloc] initWithGameResult:dictionary];
        
        if (![self.scoreSheetViewController isShowing])
        {
            [self showScoreSheet];
        }
        else
        {
            self.scoreSheetViewController.winner_AIImageView.hidden = NO;
        }
        [self.scoreSheetViewController reloadScoreSheet];
    }
    
    if ([dictionary[kTaj_scoresheet_player] isKindOfClass:[NSArray class]])
    {
        [self updatePlayerPoints:dictionary[kTaj_scoresheet_player]];
        
        
        NSArray *playersArray = dictionary[kPlayers_Key];
        
        for (NSDictionary *playerInfo in playersArray)
        {
            if ([playerInfo[kTaj_userid_Key]isEqualToString:[[[TAJGameEngine sharedGameEngine]usermodel]userId ]])
            {
                if (playerInfo[kTaj_scoresheet_totalscore] && self.thisPlayerModel)
                {
                    self.buyInAmount=playerInfo[kTaj_scoresheet_totalscore] ;
                    
                }
                else
                {
                    self.buyInAmount=[NSString stringWithFormat:@"0"] ;
                    
                }
            }
            
        }
        
        NSLog(@"CHECK buyInAmount %@",self.buyInAmount);
        
        if (self.isJokerTypeGame)
        {
            NSLog(@"CHECK FIRST C");
            [self checkReBuyInOption:dictionary[kTaj_scoresheet_player]];
        }
    }
    else if ([dictionary[kTaj_scoresheet_player] isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *singlePlayerDict = [NSMutableDictionary dictionaryWithDictionary:dictionary[kTaj_scoresheet_player]];
        NSMutableArray *singlePlayer = [NSMutableArray arrayWithObjects:singlePlayerDict, nil];
        [self updatePlayerPoints:singlePlayer];
        
        if (self.isJokerTypeGame)
        {
            NSLog(@"CHECK SECOND C");
            [self checkReBuyInOption:singlePlayer];
        }
    }
    self.sortButton.enabled = NO;
    self.didSendMeldCards = NO;
    
}

- (void)updatePlayerPoints:(NSArray *)array
{
    if (array &&
        array.count > 0)
    {
        for (int i = 0; i < array.count; i++)
        {
            NSMutableDictionary *singlePlayerDict = array[i];
            NSString *userID = singlePlayerDict[kTaj_scoresheet_userid];
            UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
            //added to fix #322 and 317
            if (([singlePlayerDict[kTaj_scoresheet_result] isEqualToString:@"table_leave"] || (self.isMiddleJoin)) && [userID isEqual:[[[TAJGameEngine sharedGameEngine] usermodel] userId]] && self.isJokerTypeGame)
            {
                
                self.isMiddleJoin = NO;
                continue;
            }
            
            if (chairHolder.subviews && chairHolder.subviews.count > 0)
            {
                NSString *playerPoints = singlePlayerDict[kTaj_scoresheet_totalscore];
                
                TAJChairView *chairView = chairHolder.subviews[0];
                [chairView setUpPlayerPoints:playerPoints];
            }
        }
    }
}

- (void)checkReBuyInOption:(NSArray *)array
{
    int loginUserId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] intValue];
    
    int playerGainPoints = 0;
    NSMutableDictionary *singlePlayerDict;
    
    if (array &&
        array.count > 0)
    {
        for (int i = 0; i < array.count; i++)
        {
            int userId = [[array valueForKey:@"TAJ_user_id"][i] intValue] ;
            NSLog(@"userId :%d",userId);
            NSLog(@"loginUserId :%d",loginUserId);
            //NSLog(@"kTaj_scoresheet_userid :%@",userId);
            if (loginUserId == userId) {
                
                singlePlayerDict = array[i];
                //NSLog(@"singlePlayerDict :%@",singlePlayerDict);
                playerGainPoints = [singlePlayerDict[kTaj_scoresheet_totalscore] intValue];
                NSLog(@"GAIN POINTS %d",playerGainPoints);
            }
            
        }
        NSLog(@"jokerMinBuyin : %d",self.jokerMinBuyin);
        int maxReachingPoints = 2 * self.jokerMinBuyin;
        NSLog(@"maxReachingPoints : %d",maxReachingPoints);
        NSLog(@"playerGainPoints : %d",playerGainPoints);            //            DLog(@"playerGainPoints %d %@ %d",playerGainPoints,singlePlayerDict[kTaj_scoresheet_nickname],[self.buyInAmount intValue]);
        
        if (maxReachingPoints >= playerGainPoints)
        {
            NSString *userID = singlePlayerDict[kTaj_scoresheet_userid];
            UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
            TAJChairView *chair = [self chairFromView:chairHolder];
            NSLog(@"CHECK BAL : %d",[self isThisDevicePlayer:chairHolder] && maxReachingPoints);
            NSLog(@"CHECK PLAYER BAL %d",[chair.playerModel.playerPoints intValue]);
            
            if ([self isThisDevicePlayer:chairHolder] && maxReachingPoints >= [chair.playerModel.playerPoints intValue])
            {
                NSDictionary* dictionary= self.playTableDictionary[kTaj_Table_details];
                
                float minBet = [dictionary[TAJ_MIN_BUYIN] floatValue];
                float currentBalance;
                if ([APP_DELEGATE.gameType isEqualToString:CASH_GAME_LOBBY]) {
                    currentBalance = [[[[TAJGameEngine sharedGameEngine] usermodel]realChips] floatValue];
                }
                else {
                    currentBalance = [[[[TAJGameEngine sharedGameEngine] usermodel]funchips] floatValue];
                }
                
                NSLog(@"CUrrent bal : %f",currentBalance);
                NSLog(@"minBet bal : %f",minBet);
                if (currentBalance >= minBet)
                {
                    self.isRebuyin = YES;
                    if (!self.rebuyAlertView && !self.isSpectator)
                    {
#if NEW_INFO_VIEW_IMPLEMENTATION
                        // ratheesh replaced eOK_Cancel_Type with eYes_No_Type for Re-buy
                        self.rebuyAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_REBUYIN_JOKER_TAG popupButtonType:eYes_No_Type  message:ALERT_REBUYIN_REQUEST_TITLE];
                        
                        self.rebuyAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_REBUYIN_JOKER_TAG popupButtonType:eYes_No_Type message:ALERT_REBUYIN_REQUEST_TITLE];
                        //                            }
#else
                        self.rebuyAlertView = [[UIAlertView alloc]initWithTitle:ALERT_EMPTY_TITLE
                                                                        message:ALERT_REBUYIN_REQUEST_TITLE
                                                                       delegate:self
                                                              cancelButtonTitle:OK_STRING
                                                              otherButtonTitles:CANCEL_STRING, nil];
                        
                        self.rebuyAlertView.tag = ALERT_REBUYIN_JOKER_TAG;
#endif
                        [self.rebuyAlertView show];
                        
                    }
                }
            }
        }
    }
}

// Show TableToss implementation
- (void)showTableTossResult:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    self.canAnimate = YES;
    self.isDisConnectReconnect = NO;
    [self saveResultForKey:NO];
    [self resetPlayerDealerArray];
    self.isFirstRoundGameAlreadyStarted = YES;
    self.canShowAutoPlayView = YES;
    self.isCardDistributionState = YES; //Card distribution starts
    self.isGameScheduleState = YES;      //Game schedule ends
    self.canLeaveTable = NO;
    self.leaveTableMessage = LEAVE_TABLE_TOSS;
    [self refreshChairView];
    [self removeInstanceAlertViews];
    
    [self removeScoreBoard];
    [self removeScoreSheetView];
    
    //Sound
    [[TAJSoundHandler sharedManager] playTossSound];
    
    //Vibrate
    [self vibrateDevice];
    
    if ([self.thisPlayerModel.playerName isEqualToString:dictionary[kTaj_Toss_Winner_Key]])
    {
        self.isTossWinner = YES;
    }
    
    if ([dictionary valueForKeyPath:kPlayers_From_Toss_Key] &&
        [[dictionary valueForKeyPath:kPlayers_From_Toss_Key] isKindOfClass:[NSArray class]])
    {
        NSArray *plarersArray = [dictionary valueForKeyPath:kPlayers_From_Toss_Key];
        for (int i = 0; i< plarersArray.count; i++)
        {
            if (plarersArray[i])
            {
                NSDictionary *playerInfo = plarersArray[i];
                if (playerInfo)
                {
                    NSString *userId = playerInfo[kTaj_userid_Key];
                    NSString *cardNum = playerInfo[kTaj_Card_Face_Key];
                    NSString *cardSuit = [playerInfo[kTaj_Card_Suit_Key] uppercaseString];
                    NSString *finalCardName = [NSString stringWithFormat:@"%@%@",cardNum,cardSuit];
                    [self showTossCard:finalCardName forPlayer:userId];
                }
            }
        }
    }
    
    // set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerWonTossMessage:dictionary]];
    
    // Show who is the winner
    NSString *message = [NSString stringWithFormat:TOSS_WINNER_MESSAGE, dictionary[kTaj_Toss_Winner_Key]];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView showInstructionPlayerMode:message];
    }
    
    [self.playTable showInstruction:message];
    
    if (!self.isJokerGameStarted && self.isJokerTypeGame && self.startTimerDuration <= 5.0)
    {
        [self disableChairs];
    }
    [self removeReportBugView];
    [self removeJokerTableView];
}

// RearrangeChairs /Sitting sequence implementation
- (void)rearrangeChairsWithData:(NSDictionary *)dictionary
{
    //NSLog(@"REARRANGE : %@",dictionary);
    NSArray * players = [dictionary objectForKey:@"player"];
    // NSLog(@"PLAYERS COUNT : %lu",(unsigned long)players.count);
    NSDictionary * playersCountDict = @{
                                        @"table_id" : [dictionary objectForKey:@"TAJ_table_id"],
                                        @"maxplayer" : [NSNumber numberWithInteger:self.maxPlayers],
                                        @"count" : [NSNumber numberWithInteger:players.count]
                                        };
    //NSLog(@"REARRANGE POST DATA%@",playersCountDict);
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_PLAYERS object:playersCountDict];
    
    NSMutableArray *arrayPlayers = [NSMutableArray new];
    
    for (int i = 0; i < [players count]; i++) {
        NSMutableDictionary *playersDict = [NSMutableDictionary new];
        
        NSString * userID = [[players objectAtIndex:i] objectForKey:@"TAJ_user_id"];
        NSString *nickName = [[players objectAtIndex:i] objectForKey:@"TAJ_nick_name"];
        
        int playerIndex = i +1;
        NSString * playerKey = [NSString stringWithFormat:@"player_%d",playerIndex];
        NSString * plyaerInfo = [NSString stringWithFormat:@"%@ - %@",userID,nickName];
        [CrashlyticsKit setObjectValue:plyaerInfo forKey:playerKey];
        
        [playersDict setValue:plyaerInfo forKey:playerKey];
        
        [arrayPlayers addObject:playersDict];
    }
    
   // NSLog(@"PLAYERS ARRAY : %@",arrayPlayers);
    //[CrashlyticsKit setObjectValue:arrayPlayers forKey:@"players"];
   // [CrashlyticsKit setValue:arrayPlayers forKey:@"players"];
    
    if (!dictionary)
    {
        return;
    }
    
    self.isGameEnded = NO;
    self.canAnimate = YES;
    [self saveResultForKey:NO];
    // set player dictionary for Discard Card History
    // Note IMP: for joker type game has a feature middle join so better to initialize the model during Toss Event
    
    [self extractPlayersInGameForDiscardCardHistory:dictionary];
    
    if (!self.isJokerTypeGame)
    {
        self.isPoolDealMiddleJoin = YES;
    }
    
    [self.playTable clearCardsinClosedCard];
    self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
    
    // update player position for player model after toss
    if ([dictionary valueForKeyPath:kPlayers_Key] &&
        [[dictionary valueForKeyPath:kPlayers_Key] isKindOfClass:[NSArray class]])
    {
        [self updatePlayerPositions:[dictionary valueForKeyPath:kPlayers_Key]];
    }
    
    //set up message for live feed
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDealerMessage:dictionary]];
    
    // show message on table
    if (self.noOfGameRounds == 1 && !self.isJokerTypeGame)
    {
        // for pool and deals display rearrange message at 1st round only
        NSString *message = REARRANGING_SEATS;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:message];
        }
        
        [self.playTable showInstruction:message];
    }
    
    else if(self.isJokerTypeGame)
    {
        NSString *message = REARRANGING_SEATS;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:message];
        }
        
        [self.playTable showInstruction:message];
    }
    
    if ([dictionary valueForKeyPath:kPlayers_Key] &&
        [[dictionary valueForKeyPath:kPlayers_Key] isKindOfClass:[NSArray class]])
    {
        NSArray *playersArray = dictionary[kPlayers_Key];
        NSMutableArray *reorderedArray = [NSMutableArray array];
        NSMutableArray *chairsarray = [NSMutableArray arrayWithArray:self.chairsArray];
        
        for (NSDictionary *playerInfo in playersArray)
        {
            UIView *chairHolder = [self chairHolderFromUserId:[playerInfo[kTaj_userid_Key] integerValue]];
            TAJChairView *chair = [self chairFromView:chairHolder];
            [reorderedArray addObject:chair];
           
            [chair removeFromSuperview];
            [chairsarray removeObject:chairHolder];
        }
       
        int totalPlayingPlayers = reorderedArray.count;
        
        self.currentPlayers = totalPlayingPlayers;
        for (UIView *view in chairsarray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                TAJChairView *chair = view.subviews[0];
                chair.playerModel =nil;
                [reorderedArray addObject:chair];
                [chair removeFromSuperview];
            }
        }
        
        if (totalPlayingPlayers == 2 && self.chairsArray.count > 2)
        {
            // Special case for two players
            [reorderedArray exchangeObjectAtIndex:1 withObjectAtIndex:3];
            DLog(@"exchange objects");
        }
        
        int index = 0;
        for (TAJChairView *chair in reorderedArray)
        {
            UIView *chairHolder = self.chairsArray[index];
            
            //[chair createNewEmptyChairWithChairID:index + 1];
            //[chair createNewEmptyChairWithChairID:index + 2];
            [chairHolder addSubview:chair];
            index ++;
        }
        
        //[self.playTable removeCardsFromTossDeck];
        [self removeTossCardsFromChair];
    }
    
    [self performSelector:@selector(dealerImageForPlayer:) withObject:dictionary afterDelay:2.0];
    [self removeReportBugView];
    [self removeJokerTableView];
    
}

// StartGameEvent implementation
- (void)handleStartGameEvent:(NSDictionary *)dictionary
{
    if ([self.gameStartTimer isValid])
    {
        [self.gameStartTimer invalidate];
    }
    if (!self.isJokerGameStarted && self.isJokerTypeGame && self.startTimerDuration <= 5.0)
    {
        [self disableChairs];
    }
    [self removeReportBugView];
    [self removeJokerTableView];
}

// SendStack implementation
- (void)handleSendStackData:(NSDictionary *)dictionary
{
    self.isGameStarted = YES;
    self.isFirstRoundGameAlreadyStarted = YES;
    [self inValidateTimerIfRunning];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    [self.playTable.openDeckCards removeAllObjects];
    
    //remove if any toss card image
    [self removeTossCardsFromChair];
    DLog(@"send stack event game table");
    
    if (self.isJokerTypeGame)
    {
        self.isStrikesMiddleJoin = YES;
    }
    else if (!self.isJokerTypeGame)
    {
        self.isPoolDealMiddleJoin = YES;
    }
    
    // dismiss the score sheet view if it's exist when card distribution
    [self removeScoreSheetView];
    
    self.canLeaveTable = YES;
    
    [self.playTable updateClosedDeckCardHolder:NO];
    
   //NSArray *cardStack = [NSArray arrayWithArray:[dictionary valueForKeyPath:kFaceDown_Cards_Key]];
    
    NSArray * cardStack = [NSArray arrayWithObject:[dictionary valueForKeyPath:kFaceDown_Cards_Key]];
    NSLog(@"CARD STACK : %@",cardStack);
    
    NSString *jokerCardNum;
    NSString *jokerCardType;
    if ([dictionary[kTaj_isJoker_card] isEqualToString:@"True"])
    {
        // check wheter joker card we need to show in game
        jokerCardNum = dictionary[kTaj_Card_Face_Key];
        jokerCardType = [dictionary[kTaj_Card_Suit_Key] uppercaseString];
        self.jokerCardNumber = jokerCardNum;
        self.jokerCardId = [NSString stringWithFormat:@"%@%@", jokerCardNum, jokerCardType];
        
        [self.discardCardHistoryController setJokerCard:self.jokerCardId];
    }
    
    if (cardStack)
    {
        self.closedDeckArray = [NSMutableArray array];
        
        for (NSDictionary *cardInfo in cardStack)
        {
            NSMutableDictionary *cardDictionary = [NSMutableDictionary dictionaryWithDictionary:cardInfo];
            [self.closedDeckArray addObject:cardDictionary];
        }
        
        [self.playTable setupClosedDeckWithJoker:self.jokerCardId];
    }
    //to get open card
    if ([dictionary valueForKeyPath:kFaceUp_Cards_Key]
        && [[dictionary valueForKeyPath:kFaceUp_Cards_Key] isKindOfClass:[NSDictionary class]])
    {
        // if player joined to play new game
        NSDictionary *openCard = [dictionary valueForKeyPath:kFaceUp_Cards_Key];
        
        NSString *openCardNum = openCard[kTaj_Card_Face_Key];
        NSString *openCardType = [openCard[kTaj_Card_Suit_Key] uppercaseString];
        self.openCardId = [NSString stringWithFormat:@"%@%@", openCardNum, openCardType];
        
        [self.playTable placeFaceUpCard:self.openCardId];
        // store the last open card content, to make used for live feed
        self.lastOpenDeckCardDict = [NSMutableDictionary dictionary];
        [self.lastOpenDeckCardDict setObject:openCardNum forKey:kTaj_Card_Face_Key];
        [self.lastOpenDeckCardDict setObject:openCardType forKey:kTaj_Card_Suit_Key];
        
    }
    else if ([dictionary valueForKeyPath:kFaceUp_Cards_Key]
             && [[dictionary valueForKeyPath:kFaceUp_Cards_Key] isKindOfClass:[NSArray class]])
    {
        // if player joined to watch already running game
        // server will send sendStackEvent
        
        // to know the opencard and joker card already existing
        NSArray *openCardsArray = [dictionary valueForKeyPath:kFaceUp_Cards_Key];
        
        // get last card to know the open card, the players which discarded already
        for (int index=0; index<openCardsArray.count-1; index++)
        {
            NSDictionary *openCard = [openCardsArray objectAtIndex:index];
            
            NSString *openCardNum = openCard[kTaj_Card_Face_Key] ;
            NSString *openCardType = [openCard[kTaj_Card_Suit_Key] uppercaseString];
            [self.playTable.openDeckCards addObject:@{kTaj_Card_Face_Key: openCardNum,kTaj_Card_Suit_Key: openCardType}];
            
        }
        NSDictionary *openCard = [openCardsArray lastObject];
        
        NSString *openCardNum = openCard[kTaj_Card_Face_Key];
        NSString *openCardType = [openCard[kTaj_Card_Suit_Key] uppercaseString];
        self.openCardId = [NSString stringWithFormat:@"%@%@", openCardNum, openCardType];
        [self.playTable placeFaceUpCard:self.openCardId];
        
        // store the last open card content, to make used for live feed
        self.lastOpenDeckCardDict = [NSMutableDictionary dictionary];
        [self.lastOpenDeckCardDict setObject:openCardNum forKey:kTaj_Card_Face_Key];
        [self.lastOpenDeckCardDict setObject:openCardType forKey:kTaj_Card_Suit_Key];
    }
    else if (![dictionary valueForKeyPath:kFaceUp_Cards_Key])
    {
        [self.playTable removeOpenDeckHolderCards];
    }
    if (self.isJokerTypeGame  &&
        self.maxPlayers == SIX_PLAYER && !self.thisPlayerModel && self.canMiddleJoin)
    {
        [self unSelectChair];
        if(self.isPlayerModelSlided)
        {
            // [self.playerModelView showInstructionPlayerMode:JOKER_TABLE_SIT_MESSAGE];
        }
        
        //[self.playTable showInstruction:JOKER_TABLE_SIT_MESSAGE];
    }
    // disable interation and chairs
    if (self.isSpectator)
    {
        if (self.isJokerTypeGame
            &&
            self.maxPlayers == SIX_PLAYER)
        {
            self.isJokerGameStarted = YES;
            if(self.isPlayerModelSlided)
            {
                // [self.playerModelView showInstructionPlayerMode:JOKER_TABLE_SIT_MESSAGE];
            }
            
            //[self.playTable showInstruction:JOKER_TABLE_SIT_MESSAGE];
            NSString *gameStarted;
            gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
            [self createViewForGamePlay];
            if ([gameStarted isEqualToString:@"True"])
            {
                [self playersDiscardCardHistoryOnThisPlayerSpectator:self.playTableDictionary];
            }
            
            if (!self.isTossCardCame)
            {
                [self performSelector:@selector(showCardBackImagesForChairs) withObject:nil afterDelay:0.5f ];
            }
            else
            {
                self.isTossCardCame = NO;
            }
        }
        else
        {
            [self disableChairs];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            [self.playTable removeExtraMessage];
            [self showSpectatorView];
        }
    }
}

- (void)distributeCardToAllPlayers:(NSDictionary *)dictionary
{
    self.isSendDealCame = YES;
    
    [self inValidateTimerIfRunning];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    
    [self removeScoreBoard];
    [self removeScoreSheetView];
    [self removeWinnerScreenView];
    [self removeInstanceAlertViews];
    [self removeMeldView];
    [self refreshChairView];
    
    self.canLeaveTable = NO;
    
    self.leaveTableMessage = LEAVE_TABLE_CARD_DISTRIBUTION;
    
    [self inValidateTimerIfRunning];
    
    //remove if any toss card image
    [self.playTable removeCardsFromTossDeck];
    [self removeTossCardsFromChair];
    
    [self.playTable canDiscardCard:YES];
    
#if PLAYER_MODEL_GAME_CARD
    //Update player model button
    [self.playerModelView canModelDiscardCard:YES];
    
#endif
    
    NSArray *cardStack = [dictionary valueForKey:kCards_Key];
    self.myCardsDealArray = [NSMutableArray array];
    
    for (NSDictionary *cardInfo in cardStack)
    {
        NSMutableDictionary *cardMDict = [NSMutableDictionary dictionaryWithDictionary:[cardInfo mutableCopy]];
        [self.myCardsDealArray addObject:cardMDict];
    }
    DLog(@"self.chairsArray.count = %d",self.chairsArray.count);
    [self testingSeatNumber];
    
    [self fillOutMyDeckWhenIamBackEnabled];
    
}

// senddeal for Spectator
- (void)handleSendDealForSpectator:(NSDictionary *)dictionary
{
    
}

// SendDeal implementation
- (void)handleSendDealEvent:(NSDictionary *)dictionary
{
    self.isSendDealCame = YES;
    DLog(@"send deal event %@",dictionary);
    [self inValidateTimerIfRunning];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeInfoLabelMessagePlayerMode];
    }
    [self.playTable removeInfoLabelMessage];
    
    // [self.playTable removeImagesFromMyDeckHolder];
    [self.playTable updateClosedDeckCardHolder:NO];
    [self playersAfterReconnectForDiscardCardHistory:self.playTableDictionary];
    [self removeScoreBoard];
    [self removeScoreSheetView];
    [self removeWinnerScreenView];
    [self removeInstanceAlertViews];
    [self removeInstanceAlertViews];
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(informGameEngineForGameStarted:)])
    {
        [self.gameTableDelegate informGameEngineForGameStarted:[self.currentTableId integerValue]];
    }
    
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        if (self.meldViewTable)
        {
            if (self.meldViewTable.sendCardButton.isHidden && !self.meldViewTable.isTimeOut)
            {
                DLog(@"Timeout out");
                [self.meldViewTable playerReconnectMeldHandlerForDisconnection];
                [self removeMeldView];
            }
            else
            {
                DLog(@"Rmmoved meld %@",dictionary);
                //[self removeMeldView];
                
            }
        }
        
    }
    [self inValidateTimerIfRunning];
    
    //remove if any toss card image
    [self removeTossCardsFromChair];
    
    [self.playTable canDiscardCard:YES];
    
#if PLAYER_MODEL_GAME_CARD
    //Update player model button
    [self.playerModelView canModelDiscardCard:YES];
#endif
    
    NSArray *cardStack = [dictionary valueForKey:kCards_Key];
    self.myCardsDealArray = [NSMutableArray array];
    
    for (NSDictionary *cardInfo in cardStack)
    {
        NSMutableDictionary *cardMDict = [NSMutableDictionary dictionaryWithDictionary:[cardInfo mutableCopy]];
        [self.myCardsDealArray addObject:cardMDict];
    }
    self.mySendDealCardsArray =[NSMutableArray arrayWithArray:self.myCardsDealArray];
    [self removeScoreBoard];
    
    [self testingSeatNumber];
    self.delayCount = 1.4;
    BOOL value = [self retrieveResultForKey];
    if(value == NO && self.canAnimate)
    {
        value = YES;
        [self saveResultForKey:value];
        self.canAnimate = NO;
        [self playerCardDistributionAnimation];
    }
    else
    {
        [self fillOutMyDeckWhenIamBackEnabled];
        [self performSelector:@selector(fillupMyDeckArray) withObject:nil afterDelay:0.0];
    }
}

-(void)fillOutMyDeckWhenIamBackEnabled
{
    
    if(_firstPlayerAnimationImages.count > 0)
    {
        [self removeFirstPlayerImages:_firstPlayerAnimationImages];
    }
    [self removeJokerTableView];
    [self removeReportBugView];
    __block UIImageView *cardBackAnimationImageViewA,*cardBackAnimationImageViewB,*cardBackAnimationImageViewC,*cardBackAnimationImageViewD,*cardBackAnimationImageViewE,*cardBackAnimationImageViewF;
    __block  NSMutableArray *animationPlayerArray;
    //    __block NSMutableArray *animationArray;
    
    for(NSInteger chairIndex = 0; chairIndex<self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        NSInteger placeId = [chair.playerModel.placeID integerValue];
        NSString *playType=chair.playerModel.playerType;
        //        DLog(@"InitialPlaceId= %d",placeId);
        if (self.maxPlayers == TWO_PLAYER && placeId == 2 )
        {
            placeId = 4;
        }
        
        if (!chair.isMiddleJoin && ![playType isEqualToString:@"table_leave"] && ![playType isEqualToString:@"drop"] && ![playType isEqualToString:@"timeout"])
        {
            if (placeId > 0 && placeId <= 6)
            {
                //                DLog(@"Place_ID = %d",placeId);
                if(placeId == 1)
                {
                    if(self.isSpectator)
                    {
                        cardBackAnimationImageViewA  = self.cardBackAnimationImageViewArray[0];
                        animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[0]];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewA withImage:animationPlayerArray[12]];
                        
                    }
                    else
                    {
                    }
                }
                
                if (placeId == 2)
                {
                    cardBackAnimationImageViewB  = self.cardBackAnimationImageViewArray[1];
                    animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[1]];
                    [self fillupHandCardImageOnImageView:cardBackAnimationImageViewB withImage:animationPlayerArray[12]];
                }
                if (placeId == 3)
                {
                    cardBackAnimationImageViewC  = self.cardBackAnimationImageViewArray[2];
                    animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[2]];
                    
                    [self fillupHandCardImageOnImageView:cardBackAnimationImageViewC withImage:animationPlayerArray[12]];
                    
                }
                if (placeId == 4)
                {
                    cardBackAnimationImageViewD  = self.cardBackAnimationImageViewArray[3];
                    animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[3]];
                    [self fillupHandCardImageOnImageView:cardBackAnimationImageViewD withImage:animationPlayerArray[12]];
                    
                }
                if (placeId == 5)
                {
                    cardBackAnimationImageViewE  = self.cardBackAnimationImageViewArray[4];
                    animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[4]];
                    [self fillupHandCardImageOnImageView:cardBackAnimationImageViewE withImage:animationPlayerArray[12]];
                }
                if (placeId == 6)
                {
                    cardBackAnimationImageViewF  = self.cardBackAnimationImageViewArray[5];
                    animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[5]];
                    [self fillupHandCardImageOnImageView:cardBackAnimationImageViewF withImage:animationPlayerArray[12]];
                }
            }
        }
    }
}

-(void)addSubViewToMainView:(UIImageView *)imageView
{
    imageView.hidden = NO;
}


- (void)playerCardDistributionAnimation
{
    if(self.chatMenuView)
    {
        [self removeChatMenuViewController];
    }
    
    if(self.settingMenuListView)
    {
        [self removeSettingMenuListView];
    }
    self.canLeaveTable = NO;
    self.leaveTableMessage = LEAVE_TABLE_CARD_DISTRIBUTION;
    if (self.isJokerTypeGame  &&
        self.maxPlayers == SIX_PLAYER && !self.thisPlayerModel )
    {
        [self disableChairs];
        [self.playTable removeInfoLabelMessage];
    }
    [self removeJokerTableView];
    [self removeReportBugView];
    
    //Sound
    [[TAJSoundHandler sharedManager] playCardDistributeSound];
    NSMutableArray  *cardImagesArray =[NSMutableArray array];
    CGRect          cardFinalFrame;
    UIImageView     *cardBackImage;
    CGPoint         startPoint;
    CGFloat         delay = 0.0f;
    CGPoint         finalPoint;
    TAJChairView *chair;
    __block  NSMutableArray *animationPlayerArray;
    
    startPoint =  [self.view convertPoint:self.playTable.closedDeckCardHolder.center fromView:self.playTable];
    for (int cardIndex = 0; cardIndex < 13; cardIndex++)
    {
        for (int chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
        {
            chair = [self chairFromView:self.chairsArray[chairIndex]];
            NSInteger placeId = [chair.playerModel.placeID integerValue];
            if (!chair.isMiddleJoin)
            {
                if (self.maxPlayers == TWO_PLAYER && placeId == 2 )
                {
                    placeId = 4;
                }
                if([TAJUtilities isIPhone])
                {
                    
                    cardBackImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_back-568h@2x~iphone.png"]];
                }
                else
                {
                    cardBackImage = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"card_back.png"]];
                }
                
                CGRect cardRect = [self.view convertRect:self.playTable.closedDeckCardHolder.frame fromView:self.playTable];
                if([TAJUtilities isIPhone])
                {
                    cardRect.size.width -= 20;
                    cardRect.size.height -=10;
                    cardRect.origin.x += 15;
                    cardRect.origin.y += 5;
                }
                else
                {
                    cardRect.size.width -= 48;
                    cardRect.size.height -=17;
                    cardRect.origin.x += 38;
                    cardRect.origin.y += 10;
                }
                
                cardBackImage.frame = cardRect;
                [self.view addSubview:cardBackImage];
                [cardImagesArray addObject:cardBackImage];
                if (placeId > 0 && placeId <= 6)
                {
                    
                    if(self.isSpectator)
                    {
                        if( placeId > 0)
                        {
                            __block UIImageView *imageView =  self.playerCardsBgImage[placeId - 1];
                            imageView.hidden = NO;
                            
                            if([TAJUtilities isIPhone])
                            {
                                finalPoint.x = imageView.frame.origin.x + cardIndex * 2;
                                finalPoint.y = imageView.frame.origin.y;
                            }
                            else
                            {
                                finalPoint.x = imageView.frame.origin.x + cardIndex * 5;
                                finalPoint.y = imageView.frame.origin.y;
                            }
                        }
                        
                    }
                    else
                    {
                        if(placeId == 1)
                        {
                            cardFinalFrame =  [self.view convertRect:self.playTable.myCardDeckHolder.frame fromView:self.playTable];
                            if([TAJUtilities isIPhone])
                            {
                                //recent change for distribution position
                                cardFinalFrame.origin.x =(cardIndex + 7) * ([TAJUtilities isItIPhone5] ? SLOT_CARD_FACTOR_IPHONE_5 : SLOT_CARD_FACTOR_IPHONE_4 ) + ([TAJUtilities isItIPhone5] ? 15 : 100 );
                                cardFinalFrame.origin.y = ([TAJUtilities isItIPhone5] ? cardFinalFrame.origin.y + 5.0 : cardFinalFrame.origin.y - 3.0);
                                cardFinalFrame.size.width = 50.0;
                                cardFinalFrame.size.height = 70.0;
                            }
                            else
                            {
                                cardFinalFrame.origin.y = cardFinalFrame.origin.y - 20.0;
                                cardFinalFrame.origin.x = (cardIndex + 1) * 34 + 208;
                                cardFinalFrame.size.width = 103.0;
                                cardFinalFrame.size.height = 140.0;
                                
                            }
                            
                        }
                        if( placeId > 1 )
                        {
                            __block UIImageView *imageView =  self.playerCardsBgImage[placeId - 1];
                            imageView.hidden = NO;
                            
                            if([TAJUtilities isIPhone])
                            {
                                finalPoint.x = imageView.frame.origin.x + cardIndex * 2;
                                finalPoint.y = imageView.frame.origin.y;
                            }
                            else
                            {
                                finalPoint.x = imageView.frame.origin.x + cardIndex * 5;
                                finalPoint.y = imageView.frame.origin.y;
                            }
                        }
                        
                    }
                    
                    [UIView animateWithDuration:0.1
                                          delay:delay
                                        options: UIViewAnimationOptionCurveEaseOut
                                     animations:
                     ^{
                         if(self.isSpectator)
                         {
                             cardBackImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.3f, 0.3f);
                             cardBackImage.center = finalPoint;
                         }
                         else
                         {
                             if(placeId == 1)
                             {
                                 
                                 cardBackImage.frame = cardFinalFrame;
                             }
                             else
                             {
                                 cardBackImage.transform = CGAffineTransformScale(CGAffineTransformIdentity, 0.3f, 0.3f);
                                 cardBackImage.center = finalPoint;
                             }
                             
                         }
                     }
                                     completion:^(BOOL finished)
                     {
                         if(self.isSpectator)
                         {
                             [cardBackImage removeFromSuperview];
                         }
                         else
                         {
                             if( !(placeId == 1))
                             {
                                 [cardBackImage removeFromSuperview];
                             }
                             
                         }
                         if (placeId > 0 && placeId <= 6)
                         {
                             if(self.isSpectator)
                             {
                                 if(placeId > 0)
                                 {
                                     UIImageView     *cardBackONE = self.cardBackAnimationImageViewArray[placeId - 1];
                                     cardBackONE.hidden = NO;
                                     animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[placeId - 1]];
                                     [self cardDistributionAnimationWithArray: animationPlayerArray andIndex:cardIndex andImageView:cardBackONE];
                                     if(cardIndex == 12)
                                     {
                                         cardBackONE.hidden = YES;
                                         UIImageView     *cardBackTWO = self.playerCardsBgImage[placeId - 1];
                                         animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[placeId - 1]];
                                         [self fillupHandCardImageOnImageView:cardBackTWO withImage:animationPlayerArray[12]];
                                     }
                                 }
                             }
                             else
                             {
                                 if(placeId > 1)
                                 {
                                     
                                     UIImageView     *cardBackONE = self.cardBackAnimationImageViewArray[placeId - 1];
                                     cardBackONE.hidden = NO;
                                     animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[placeId - 1]];
                                     [self cardDistributionAnimationWithArray: animationPlayerArray andIndex:cardIndex andImageView:cardBackONE];
                                     if(cardIndex == 12)
                                     {
                                         cardBackONE.hidden = YES;
                                         UIImageView     *cardBackTWO = self.playerCardsBgImage[placeId - 1];
                                         animationPlayerArray = [NSMutableArray  arrayWithArray:self.cardBackAnimationPlayerArray[placeId - 1]];
                                         [self fillupHandCardImageOnImageView:cardBackTWO withImage:animationPlayerArray[12]];
                                     }
                                 }
                             }
                         }
                         
                     }
                     
                     ];
                    
                    delay = cardIndex * 0.1;
                }
            }
            
        }
    }
    for(NSInteger chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        chair.isMiddleJoin = NO;
    }
    
    [self performSelector:@selector(removeFirstPlayerImages:) withObject:cardImagesArray afterDelay:delay];
    [self performSelector:@selector(fillupMyDeckArray) withObject:nil afterDelay: delay + 0.1];
    
}

- (void)removeFirstPlayerImages:(NSMutableArray *) array
{
    for (UIImageView *img in array)
    {
        [img removeFromSuperview];
    }
}

- (void)cardDistributionAnimationWithArray:(NSArray *)imageArray andIndex:(NSInteger)index andImageView:(UIImageView *)cardBackImageView
{
    NSMutableArray *animationArray = [NSMutableArray arrayWithObject:imageArray[index]];
    cardBackImageView.contentMode = UIViewContentModeScaleAspectFit;
    cardBackImageView.clipsToBounds = YES;
    cardBackImageView.animationImages = animationArray;
    cardBackImageView.animationDuration = 0.0f;
    cardBackImageView.animationRepeatCount = index;
    [cardBackImageView startAnimating];
}

- (void)fillupHandCardImageOnImageView:(UIImageView *)cardBackAnimationImageView withImage:(UIImage *)referenceImage
{
    cardBackAnimationImageView.image = referenceImage;
    cardBackAnimationImageView.contentMode = UIViewContentModeScaleAspectFit;
    cardBackAnimationImageView.clipsToBounds = YES;
    cardBackAnimationImageView.hidden = NO;
    
    
}
- (void)fillupMyDeckArray
{
#if PLAYER_MODEL_GAME_CARD
    //Update player model button
    if (self.thisPlayerModel)
    {
        [self performSelector:@selector(updatePlayerModeButton) withObject:nil afterDelay:0.0];
        if(self.isPlayerModeButtonClicked)
        {
            [self performSelector:@selector(playerModelButtonAction:) withObject:self.playerModelButton afterDelay:0.0];
        }
        
    }
    [self.playerModelView canModelDiscardCard:YES];
    [self.playerModelView enablePlayerModeSlideButton];
    
#endif
#if CARD_GROUPING_SLOTS
    [self fillSlotCardArray:self.myCardsDealArray];
#else
    [self.playTable updateMyDeckWithCards:self.myCardsDealArray];
#endif
    if (!self.isSpectator)
    {
        [self updateSortButton:YES];
    }
}

- (void)updatePlayerModeButton
{
    //self.playerModelButton.hidden = NO; ReDIM changes
}
-(void)animateCardBackImage:(UIImageView *)cardBackImageView withPlaceId:(NSInteger)placeId
{
    //CGRect xframe = self.view.frame;
    //359.0
    UIImageView *animationImageView = [[UIImageView alloc] initWithFrame:CGRectMake(359.0, 259, 61.0, 91.0)];
    animationImageView.image = [UIImage imageNamed:@"card_back.png"];
    
    [self.view addSubview:animationImageView];
    
    __block CGPoint yCeneter = animationImageView.center;
    __block CGPoint zCenter;
    
    //UIViewAnimationOptionCurveEaseOut
    [UIView animateWithDuration:0.5 delay:0.01 options:UIViewAnimationOptionCurveEaseOut animations:
     ^{
         zCenter = [self.view convertPoint:cardBackImageView.center fromView:self.playTableHolderView];
         yCeneter = zCenter;
         animationImageView.center = yCeneter;
     }
                     completion:^(BOOL finished)
     {
         [animationImageView removeFromSuperview];
     }];
}

// Show deal for spectator implementation
- (void)handleShowDealForSpectatorEvent:(NSDictionary *)dictionary
{
    // this event will come only for how is actually watching the game
    self.isSpectator = YES;
    
    [self handleSendDealEvent:dictionary];
    
    if (self.isJokerTypeGame && self.maxPlayers == SIX_PLAYER)
    {
        self.isJokerGameStarted = YES;
        self.spectatorView.hidden = YES;
        
        [self removeJokerTableView];
        [self updateSortButton:NO];
    }
    else
    {
        self.spectatorView.hidden = NO;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeExtraMessagePlayerMode];
        }
        [self.playTable removeExtraMessage];
        self.spectatorView.hidden = NO;
        
        [self updateSortButton:NO];
        [self updateFlipCardButton:NO];
    }
}

// TurnUpdate implementation
- (void)handleTurnUpdateEvent:(NSDictionary *)dictioonary
{
    NSLog(@"turn update came %d,%@",self.isMyTurn,dictioonary);
    
    int loginUserId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] intValue];
    
    int turnUserId = [[dictioonary valueForKey:@"TAJ_user_id"] intValue];
    
    //using it for chunks update
    NSInteger placeId = [self getPlaceIdFromChairUserID:turnUserId];
    
    NSString * extraChunks = [dictioonary valueForKey:@"TAJ_auto_extra_chunks"];
    int extraChunksTime = [[dictioonary valueForKey:@"TAJ_auto_extra_time"] intValue];
    
    [self setPlayerChunkcLevelWithPlayeTurn:placeId chunksLevel:extraChunks extraTime:extraChunksTime];
    
    //end here
    
    if (loginUserId == turnUserId) {
        
        NSString * extraTimeChuks = [dictioonary valueForKey:@"TAJ_auto_extra_chunks"];
        NSLog(@"TAJ_auto_extra_chunks : %@",extraTimeChuks);
        
        if ([extraTimeChuks isEqualToString:@"0/3"]) {
            [self updateExtraTimeButton:YES];
            NSLog(@"extraTimeChuks : YES");
            //self.extraTimeButton.hidden = NO;
        }
        else {
            [self updateExtraTimeButton:NO];
            NSLog(@"extraTimeChuks : NO");
            //self.extraTimeButton.hidden = YES;
        }
        
        NSDictionary * stack = [dictioonary objectForKey:@"stack"];
        NSArray * cardStack = [NSArray arrayWithObject:[stack valueForKeyPath:kFaceDown_Cards_Key]];
        
        if (cardStack)
        {
            self.closedDeckArray = [NSMutableArray array];
            
            for (NSDictionary *cardInfo in cardStack)
            {
                NSMutableDictionary *cardDictionary = [NSMutableDictionary dictionaryWithDictionary:cardInfo];
                [self.closedDeckArray addObject:cardDictionary];
            }
            NSLog(@"CLOSED DEACK ARRAY : %@",self.closedDeckArray);
            
            [self.playTable setupClosedDeckWithJoker:self.jokerCardId];
        }
    }
    
    if(_firstPlayerAnimationImages.count > 0)
    {
        [self removeFirstPlayerImages:_firstPlayerAnimationImages];
    }
    DLog(@"turn update came %d,%@",self.isMyTurn,dictioonary);
    
    
    
    //updateExtraTimeButton
    
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    
    [self.playTable removeExtraMessage];
    self.canShowAutoPlayView = YES;
    self.isCardDistributionState = NO;
    self.canLeaveTable = YES;
    [self.playerTimeForNegetiveDiscard invalidate];
    self.playerTimeForNegetiveDiscard =nil;
    self.playerExtraTimeoutTime =0;
    self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
    
    [self removeScoreBoard];
    [self removeScoreSheetView];
    
    // show auto play label who has lost internet conncetion
    [self prepareAutoPlayViewForTurnUpdate:dictioonary];
    
    // show turn update
    NSString *userId = dictioonary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    
    // Now this device player turn
    [self.playTable removeAllButtonsExceptGroup];
    [self.playTable canDiscardCard:YES];
    
#if PLAYER_MODEL_GAME_CARD
    // Remove if any button exist in player model view
    [self.playerModelView removeAllModelButtonsExceptGroup];
    [self.playerModelView canModelDiscardCard:YES];
#endif
    if (!self.isThisDevicePlayerDroppedGame)
    {
        [self.playTable thisDivicePlayerTurn:dictioonary];
    }
    self.currentTurnChair = [self chairFromView:chairHolder];
    NSString *playerName = dictioonary[kTaj_nickname];
    
    if ([self.thisPlayerModel.playerName isEqualToString:playerName] && [dictioonary[kTaj_player_timeout] floatValue] > 5.0)
    {
        self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    }
    else if ([self.thisPlayerModel.playerName isEqualToString:playerName] && [dictioonary[kTaj_player_timeout] floatValue] <= 5.0  )
    {
        self.isPlayerCanEnterGamePlayInAutoPlayMode = NO;
    }
    
    if (![self.thisPlayerModel.playerName isEqualToString:playerName] )
    {
        self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    }
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
    
    if ([self.thisPlayerModel.playerName isEqualToString:playerName] && [dictioonary[kTaj_player_timeout] floatValue] > 4.0)
    {
        [self setIsMyTurn:YES];
    }
    else
    {
        [self.playTable canDiscardCard:NO];
        [self.playerModelView canModelDiscardCard:NO];
        [self setIsMyTurn:NO];
    }
    if ([self isThisDevicePlayer:chairHolder])
    {
        //Vibrate
        [self vibrateDevice];
        self.isAlertTobeShown = YES;
        
        if(self.chatViewController)
        {
            if(self.chatMenuView)
            {
                [self removeChatMenuViewController];
            }
            else
            {
                [self.gameTableDelegate removeChatExtentViewController];
            }
            
        }
        if(self.chatMenuView)
        {
            [self removeChatMenuViewController];
        }
        self.isThisDeviceTurn = YES;
        [self updateSortButton:YES];
        
        if ([[TAJUtilities sharedUtilities] isInternetConnected])
        {
            //Sound
            [[TAJSoundHandler sharedManager] playBellSound];
        }
        
        //[self updateExtraTimeButton:YES];
        [self updateFlipCardButton:NO];
        
        //highlight button if player is in other screen except game play
        BOOL isLobbyView = [self isPlayerInLobbyScreen];
        if ([self.gameTableDelegate respondsToSelector:@selector(showAlertIndicatorWhenMyTurn)])
        {
            [self.gameTableDelegate showAlertIndicatorWhenMyTurn];
        }
        if (!isLobbyView)
        {
            [self highLightButton];
        }
        
        //schedule timer for player
        [self startTimerForPlayerToDiscardCard:[dictioonary[kTaj_player_timeout] floatValue] andData:dictioonary];
    }
    else
    {
        self.isAlertTobeShown = NO;
        self.isExtraTimeButtonClicked = NO;
        if(self.discardCardHistoryController)
        {
            self.discardCardHistoryController.cardsScrolView.userInteractionEnabled = YES;
        }
        self.isThisDeviceTurn = NO;
        
        
        [self updateExtraTimeButton:NO];
        
        if (!self.isSpectator)
        {
            [self updateFlipCardButton:YES];
        }
        if ([self.gameTableDelegate respondsToSelector:@selector(hideAlertImageWhenOtherTurn)])
        {
            [self.gameTableDelegate hideAlertImageWhenOtherTurn];
        }
        //schedule timer for player
        [self startTimerForPlayerToDiscardCard:[dictioonary[kTaj_player_timeout] floatValue] andData:dictioonary];
        
        if ([self.gameTableDelegate respondsToSelector:@selector(hideAlertImageWhenOtherTurn)])
        {
            [self.gameTableDelegate hideAlertImageWhenOtherTurn];
        }
        
        
    }
    
    
    if (self.isSpectator)
    {
        [self updateSortButton:NO];
        if (self.meldViewTable)
        {
            [self updateSortButton:YES];
        }
        else
        {
            [self updateSortButton:NO];
        }
    }
    [self updateDropButton];
}


// CardDiscard implementation
- (void)handleCardDiscardEvent:(NSDictionary *)dictionary
{
    DLog(@"carddiscardevent %@",dictionary);
    NSString *userId = dictionary[kTaj_userid_Key];
    NSInteger placeId =[self getPlaceIdFromChairUserID:[userId integerValue]];
    
    [self removeScoreBoard];
    [self removeScoreSheetView];
    [self removeWinnerScreenView];
    [self removeInstanceAlertViews];
    if (self.meldViewTable)
    {
        if (self.meldViewTable.sendCardButton.isHidden && !self.meldViewTable.isTimeOut)
        {
        }
        else
        {
            [self removeMeldView];
        }
    }
    else
    {
        [self removeMeldView];
    }
    
    // NSDictionary *cardInfo = [self.openDeckCards lastObject];
    self.isCardDiscarded = YES;
    NSMutableDictionary *cardInfo = [NSMutableDictionary dictionary];
    [cardInfo setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
    [cardInfo setObject:dictionary[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
    
    // UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    UIImageView * cardBackImageView;
    if (self.maxPlayers == TWO_PLAYER && placeId == 2)
    {
        placeId = 4;
    }
    // DLog(@"placeId = %d",placeId);
    if (placeId > 0 && placeId <= 6)
    {
        cardBackImageView = self.playerCardsBgImage[placeId - 1];
    }
    
    UIImageView *cardImageView;
    if([TAJUtilities isIPhone])
    {
        cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cardBackImageView.center.x, cardBackImageView.center.y, 21.0, 28.0)];
        cardImageView.image = [ UIImage imageNamed:@"card_set06-568h~iphone.png"];
    }
    else
    {
        cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(cardBackImageView.center.x, cardBackImageView.center.y, 31.0, 43.0)];
        cardImageView.image = [ UIImage imageNamed:@"card_set06.png"];
    }
    
    [self.view addSubview:cardImageView];
    __block CGPoint xframe = cardImageView.center;
    
    CGPoint  rect =  [self.view convertPoint:self.playTable.openDeckHolder.center fromView:self.playTable];
    xframe = CGPointMake(rect.x, rect.y);
    
    [UIView animateWithDuration:0.08
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseOut
                     animations:
     ^{
         cardImageView.center = xframe;
     }
                     completion:^(BOOL finished)
     {
         [cardImageView removeFromSuperview];
     }];
    
    [self.playTable disableFirstTimePickOpenCard];
    
    if ([self.thisPlayerModel.playerID isEqualToString:userId])
    {
        [self.playTable thisPlayerCardDiscarded:dictionary];
        [self.cardSlotManager discardAutoPlayCardForPlayerPicked:dictionary[kTaj_Card_Face_Key] withSuit:dictionary[kTaj_Card_Suit_Key]];
        
        [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
        
#if PLAYER_MODEL_GAME_CARD
        [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
        
    }
    else
    {
        [self.playTable otherPlayerCardDiscarded:dictionary];
    }
    
    // store the last open card content, to make used for live feed
    self.lastOpenDeckCardDict = [NSMutableDictionary dictionary];
    [self.lastOpenDeckCardDict setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
    [self.lastOpenDeckCardDict setObject:dictionary[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
    
    // set up message for live feed
    NSMutableDictionary *discardedCard = [NSMutableDictionary dictionary];
    [discardedCard setObject:dictionary[kTaj_nickname] forKey:kTaj_nickname];
    [discardedCard setObject:dictionary[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
    [discardedCard setObject:dictionary[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDiscardMessage:discardedCard]];
    
    // store the card content to discard card history
    [self addPlayersDiscardedCard:[dictionary mutableCopy]];
    self.starImageForCardInfo = [NSMutableDictionary dictionary];
    [self.starImageForCardInfo setObject:dictionary[kTaj_nickname] forKey:kTaj_nickname];
    [self.starImageForCardInfo setObject:cardInfo forKey:kTaj_StarCardInfo];
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(cardDiscardedWhenAutoPlayMode:)])
    {
        [self.gameTableDelegate cardDiscardedWhenAutoPlayMode:dictionary];
    }
    
    // show auto play label for player
    [self isPlayerCameToAutoPlayMode:dictionary];
}

// CardPick implementation
- (void)handleCardPickEvent:(NSDictionary *)dictionary
{
    DLog(@"card pick event %@",dictionary);
    [self removeScoreBoard];
    [self removeScoreSheetView];
    [self removeWinnerScreenView];
    [self removeInstanceAlertViews];
    
    if (self.meldViewTable)
    {
        if (self.meldViewTable.sendCardButton.isHidden && !self.meldViewTable.isTimeOut)
        {
            
        }
        else
        {
            [self removeMeldView];
        }
    }
    else
    {
        [self removeMeldView];
    }

    
    NSString *userId = dictionary[kTaj_userid_Key];
    UIView *chairHolder = [self chairHolderFromUserId:[userId integerValue]];
    NSInteger placeId =[self getPlaceIdFromChairUserID:[userId integerValue]];
    UIImageView * cardBackImageView;
    
    if (self.maxPlayers == TWO_PLAYER && placeId == 2)
    {
        placeId = 4;
    }
    
    if (placeId > 0 && placeId <= 6)
    {
        cardBackImageView = self.playerCardsBgImage[placeId - 1];
    }
    
    if ([dictionary[kTaj_Stack_Key] isEqualToString:kFace_Down_Stack_Key])
    {
        NSMutableDictionary *closedDeckLastCard = [self.closedDeckArray lastObject];
        
        [self.closedDeckArray removeLastObject];
        
        [closedDeckLastCard setObject:dictionary[kTaj_autoPlay_status] forKey:kTaj_autoPlay_status];
        [closedDeckLastCard setObject:kFace_Down_Stack_Key forKey:kTaj_Network_pick_type];
        [closedDeckLastCard setObject:dictionary[kTaj_userid_Key] forKey:kTaj_userid_Key];
        [closedDeckLastCard setObject:dictionary[kTaj_nickname] forKey:kTaj_nickname];
        [closedDeckLastCard setObject:@"pick" forKey:kTaj_Network_card_type];
        
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(cardPickedWhenAutoPlayMode:)])
        {
            [self.gameTableDelegate cardPickedWhenAutoPlayMode:closedDeckLastCard];
        }
        
        // set up message for closed deck live feed
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerPickClosedDeckMessage:dictionary]];
        
        // Call this function after above executes
        if ([self isThisDevicePlayer:chairHolder])
        {
            self.pickedCard = [NSMutableDictionary dictionaryWithObjectsAndKeys:closedDeckLastCard[kTaj_Card_Face_Key], kTaj_Card_Face_Key,
                               [closedDeckLastCard[kTaj_Card_Suit_Key] uppercaseString], kTaj_Card_Suit_Key, nil];
            
            [self.cardSlotManager insertPickedCardToSlotArray:self.pickedCard];
            
            [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
            
#if PLAYER_MODEL_GAME_CARD
            // Update player model card
            [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
        }
        
        UIImageView *cardImageView;
        if ([TAJUtilities isIPhone])
        {
            CGRect  rect = [self.view convertRect:self.playTable.closedDeckCardHolder.frame fromView:self.playTable];
            cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y,21.0,28.0)];
            cardImageView.image = [ UIImage imageNamed:@"card_set06-568h~iphone.png"];
            cardImageView.transform = CGAffineTransformMakeRotation(3.142/4.0);
        }
        else
        {
            CGRect  rect = [self.view convertRect:self.playTable.closedDeckCardHolder.frame fromView:self.playTable];
            cardImageView = [[UIImageView alloc] initWithFrame:CGRectMake(rect.origin.x, rect.origin.y,31.0, 43.0)];
            cardImageView.image = [ UIImage imageNamed:@"card_set06.png"];
            cardImageView.transform = CGAffineTransformMakeRotation(3.142/4.0);
        }
        
        [self.view addSubview:cardImageView];
        CGPoint xframe = cardImageView.center;
        CGPoint yframe;
        
        if([TAJUtilities isIPhone])
        {
            yframe= cardBackImageView.center;
            
        }
        else
        {
            yframe= cardBackImageView.center;
            
            
        }
        xframe = yframe;
        [UIView animateWithDuration:0.10
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:
         ^{
             cardImageView.center = xframe;
             
         }
                         completion:^(BOOL finished)
         {
             [cardImageView removeFromSuperview];
         }];
    }
    else if ([dictionary[kTaj_Stack_Key] isEqualToString:kFace_Up_Stack_Key])
    {
        [self.playTable otherPlayerPickedCardOpenDeckCard];
        
        // set up message for open deck live feed
        NSMutableDictionary *openDeck = [NSMutableDictionary dictionary];
        [openDeck setObject:dictionary[kTaj_nickname] forKey:kTaj_nickname];
        [openDeck setObject:self.lastOpenDeckCardDict[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
        [openDeck setObject:self.lastOpenDeckCardDict[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
        
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerPickOpenDeckMessage:openDeck]];
        
        // show star image in discarded card
        if (self.isCardDiscarded && self.discardCardHistoryController && self.starImageForCardInfo)
        {
            [self.discardCardHistoryController showStarImageForCard:self.starImageForCardInfo];
        }
        
        NSMutableDictionary *openDeckCardDict = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        [openDeckCardDict setObject:kFace_Up_Stack_Key forKey:kTaj_Network_pick_type];
        [openDeckCardDict setObject:self.lastOpenDeckCardDict[kTaj_Card_Face_Key] forKey:kTaj_Card_Face_Key];
        [openDeckCardDict setObject:self.lastOpenDeckCardDict[kTaj_Card_Suit_Key] forKey:kTaj_Card_Suit_Key];
        [openDeckCardDict setObject:@"pick" forKey:kTaj_Network_card_type];
        
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(cardPickedWhenAutoPlayMode:)])
        {
            [self.gameTableDelegate cardPickedWhenAutoPlayMode:openDeckCardDict];
        }
        // Call this function after above executes
        if ([self isThisDevicePlayer:chairHolder])
        {
            self.isCardPicked = NO;
            self.pickedCard = [NSMutableDictionary dictionaryWithObjectsAndKeys:self.lastOpenDeckCardDict[kTaj_Card_Face_Key], kTaj_Card_Face_Key,
                               [self.lastOpenDeckCardDict[kTaj_Card_Suit_Key] uppercaseString], kTaj_Card_Suit_Key, nil];
            
            [self.cardSlotManager insertPickedCardToSlotArray:self.pickedCard];
            
            [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
            
#if PLAYER_MODEL_GAME_CARD
            // Update player model card
            [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
        }
        
        NSDictionary *cardInfo = self.lastOpenDeckCardDict;
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
        NSArray *nibContents = [[NSArray alloc]init];
        // Place new card
        int index = 0;
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                    owner:nil
                                                  options:nil];
        TAJCardView *card;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
            card  = nibContents[index];
            CGRect  rect = [self.view convertRect:self.playTable.openDeckHolder.frame fromView:self.playTable];
            card.frame = CGRectMake(rect.origin.x, rect.origin.y, 35.0, 49.0);
            
        }
        else
        {
            card  = nibContents[index];
            CGRect  rect = [self.view convertRect:self.playTable.openDeckHolder.frame fromView:self.playTable];
            card.frame = CGRectMake(rect.origin.x, rect.origin.y, 61.0, 91.0);
        }
        
        [self.view addSubview:card];
        [card showCard:cardId isMomentary:YES];
        card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.5f, 0.6f);
        CGPoint xframe = card.center;
        CGPoint yframe;
        
        if([TAJUtilities isIPhone])
        {
            yframe = cardBackImageView.center;
            
        }
        else
        {
            yframe = cardBackImageView.center;
            yframe.x +=40;
            
        }
        xframe = yframe;
        
        [UIView animateWithDuration:0.25
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:
         ^{
             card.center = xframe;
         }
                         completion:^(BOOL finished)
         {
             [card removeFromSuperview];
         }
         ];
    }
    
    // show whether player in auto play mode
    [self isPlayerCameToAutoPlayMode:dictionary];
    self.pickedCard = nil;
    
}

// Toss Result implementation
- (void)showTossCard:(NSString *)cardId forPlayer:(NSString *)userID
{
    [self removeCardBgPlayerImage];
    [self removeScoreBoard];
    [self removeScoreSheetView];
    [self removeSettingMenuListView];
    [self removeWinnerScreenView];
    [self refreshChairView];
    
    if(self.errorInfoPopupViewController)
    {
        [self.errorInfoPopupViewController hide];
        self.errorInfoPopupViewController = nil;
    }
    
    UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
    NSUInteger placeID = [self getPlaceIdFromChairUserID:[userID integerValue]];
    if (self.maxPlayers == TWO_PLAYER && placeID == 2)
    {
        placeID = 4;
    }
    
    if (chairHolder.subviews && chairHolder.subviews.count > 0)
    {
        NSArray *nibContents = [NSArray array];
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                    owner:nil
                                                  options:nil];
        
        TAJCardView *card = nibContents[index];
        [card showCard:cardId isMomentary:YES];
        
        CGRect cardFrame = card.frame;
        if (placeID == 1 || placeID == 2 || placeID == 6)
        {
            // at the bottom
            if ( placeID == 2 )
            {
                if ([TAJUtilities isIPhone])
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x - ([TAJUtilities isItIPhone5] ? 50 : 50 );
                    
                    cardFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height + ([TAJUtilities isItIPhone5] ? 12 : 10 ));
                }
                else
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x - chairHolder.frame.size.width/2 - 30 ;//+ ([TAJUtilities isIPhone] ? 15 : 38 );
                    
                    cardFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height +  20 );
                }
            }
            else if ( placeID == 6 )
            {
                if ([TAJUtilities isIPhone])
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width/2 + ([TAJUtilities isItIPhone5] ? 30 : 30 );
                    cardFrame.origin.y = chairHolder.frame.origin.y +  (chairHolder.frame.size.height + ([TAJUtilities isItIPhone5] ? 12 : 10 ) );
                }
                else
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width;
                    cardFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height + 20);
                }
                
            }
            else  if(placeID == 1)
            {
                if([TAJUtilities isIPhone])
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + ([TAJUtilities isItIPhone5] ? 4 : 6 );
                    cardFrame.origin.y = chairHolder.frame.origin.y- (chairHolder.frame.size.height + ([TAJUtilities isItIPhone5] ? 41 : 37));
                }
                else
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + 21 ;
                    cardFrame.origin.y = chairHolder.frame.origin.y- (chairHolder.frame.size.height + 50);
                }
                
            }
            
        }
        else if (placeID == 4 || placeID == 5 || placeID == 3)
        {
            // at the top
            if(placeID == 4)
            {
                if([TAJUtilities isIPhone])
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + ([TAJUtilities isItIPhone5] ? 4 : 6 );
                    cardFrame.origin.y = (chairHolder.frame.origin.y + chairHolder.frame.size.height) + ([TAJUtilities isItIPhone5] ? 2 : 2);
                }
                else
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + 21 ;
                    cardFrame.origin.y = (chairHolder.frame.origin.y + chairHolder.frame.size.height) + 3;
                }
                
            }
            else if(placeID == 5)
            {
                if ([TAJUtilities isIPhone])
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width/2 +([TAJUtilities isItIPhone5] ? 30 : 30 );
                }
                else
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width; //([TAJUtilities isIPhone] ? 15 : 38 );
                }
                cardFrame.origin.y = (chairHolder.frame.origin.y + chairHolder.frame.size.height) - ([TAJUtilities isIPhone] ? 18 : 20);
            }
            else  if(placeID == 3)
            {
                if ([TAJUtilities isIPhone])
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x - ([TAJUtilities isItIPhone5] ? 50 : 50 );
                }
                else
                {
                    cardFrame.origin.x = chairHolder.frame.origin.x - chairHolder.frame.size.width/2 - 30 ;//+ ([TAJUtilities isIPhone] ? 15 : 38 );
                }
                cardFrame.origin.y = (chairHolder.frame.origin.y + chairHolder.frame.size.height) - ([TAJUtilities isIPhone] ? 18 : 20);
            }
            
        }
        if ([TAJUtilities isIPhone])
        {
            cardFrame.size.height = ([TAJUtilities isItIPhone5] ? 71 : 71 );
            cardFrame.size.width  = ([TAJUtilities isItIPhone5] ? 50 :50 );
        }
        card.frame = cardFrame;
        [self.view addSubview:card];
        [self.tossCardArray addObject:card];
    }
    [self removeReportBugView];
    self.isTossCardCame = YES;
    
}

- (void)removeTossCardsFromChair
{
    for (UIView *card in self.tossCardArray)
    {
        [card removeFromSuperview];
    }
    [self.tossCardArray removeAllObjects];
    
}

// PlayerJoined implementation
- (void)playerJoined:(NSNotification *)notification
{
    //NSLog(@"GAME JOINED : %@",[notification object]);
    
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
        //NSLog(@"PLAYER JOINED : %@",dictionary);
        NSString *userID = dictionary[kTaj_userid_Key];
        NSString *loggedInUserID = [[[TAJGameEngine sharedGameEngine] usermodel] userId];
        if ([dictionary[TAJ_TABLE_TYPE] isEqualToString:kPR_JOKER] && [userID isEqualToString:loggedInUserID] && [dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_MIDDLE_VALUE]) {
            APP_DELEGATE.isMiddleJoin = YES;
            APP_DELEGATE.isMiddleJoinDeviceTurn = YES;
            NSLog(@"YES MIDDLE JOINED");
        }
        
    }
    if (dictionary &&
        [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
        
    {
        NSLog(@"PLAYER JOINED INFO :%@",dictionary);
        NSLog(@"GAME JOINED");

         if ([[dictionary valueForKeyPath:@"TAJ_table_cost"] isEqualToString:FREE_GAMES])
         {
             self.balanceLabel.text = self.funChipsValue;
         }
         else if ([[dictionary valueForKeyPath:@"TAJ_table_cost"] isEqualToString:CASH_GAMES]) {
             self.balanceLabel.text = self.realChipsValue;
         }
        NSLog(@"BALANCE : %@",self.balanceLabel.text);
        
        self.isSeatRequestPending = NO;
    }
    
    if (dictionary &&
        [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId] &&
        [dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_PLAY_VALUE])
    {
        int chairID = [dictionary[kTaj_Seat_key] integerValue];
        NSString *userID = dictionary[kTaj_userid_Key];
        NSString *loggedInUserID = [[[TAJGameEngine sharedGameEngine] usermodel] userId];
        //#512 fix
        if([userID isEqualToString:loggedInUserID])
        {
            UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
            if (chairHolder)
            {
                TAJChairView *chair = [self chairFromView:chairHolder];
                chair.playerModel = nil;
                [chair hideChair];
            }
        }
        if (!self.isJokerTypeGame)
        {
            UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
            if (chairHolder)
            {
                TAJChairView *chair = [self chairFromView:chairHolder];
                chair.playerModel = nil;
                [chair hideChair];
            }
        }
        
        UIView *view = [self chairHolderAtPlaceId:chairID];
        NSArray *subviews = [view subviews];
        TAJChairView *aChair = (TAJChairView *)subviews[0];
        
        if(aChair.isOccupied && aChair.playerModel)
        {
            if (self.chairsArray && self.chairsArray.count > 0)
            {
                int chairPlace= 1;
                for (UIView *chairHolder in self.chairsArray)
                {
                    if (chairHolder.subviews && chairHolder.subviews.count > 0)
                    {
                        // Already added
                        NSArray *subviews = [chairHolder subviews];
                        TAJChairView *chair = (TAJChairView *)subviews[0];
                        
                        if (!chair.playerModel && !chair.isOccupied)
                        {
                            chairID =chairPlace;
                            break;
                        }
                        else
                        {
                            chairPlace++;
                        }
                    }
                }
            }
        }
        view = [self chairHolderAtPlaceId:chairID];
        subviews = [view subviews];
        aChair = (TAJChairView *)subviews[0];
        // Sound
        [[TAJSoundHandler sharedManager] playSitSound];
        // Init player model also
        TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:dictionary];
        
        if (dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] &&
            [dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isKindOfClass:[NSString class]])
        {
            model.playerType = dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY];
            
            if ([dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:@"middle"])
            {
                self.isMiddleJoin = YES ;
            }
            else
            {
                self.isMiddleJoin = NO ;
                
            }
        }
        
        if (dictionary[ kTaj_buyin_amt_this] && [dictionary[ kTaj_userid_Key] isEqualToString:[[[TAJGameEngine sharedGameEngine]usermodel]userId]] && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
        {
            self.buyInAmount=dictionary[ kTaj_buyin_amt_this];
        }
        else
        {
            self.buyInAmount=[NSString stringWithFormat:@"0"] ;
        }
        
        //        DLog(@"%d",[self.buyInAmount intValue]);
        [aChair updateChairWithData:model];
        aChair.playerModel.placeID = [NSString stringWithFormat:@"%d",chairID] ;
        if ([self isThisDevicePlayer:view])
        {
            self.thisDevicePlayer = view;
            // Set user Id also
            TAJChairView *chair = [self chairFromView:view];
            self.thisPlayerModel = chair.playerModel;
            if ([TAJUtilities isIPhone])
            {
                self.avatarImageForIAmBack = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
            }
            else
            {
                self.avatarImageForIAmBack = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
            }
            
            NSString *welcomeImageName;
            // set image for this device player in welcome screen
            if ([TAJUtilities isIPhone])
            {
                welcomeImageName = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",chair.playerModel.playerGender,chair.playerModel.playerCharNumber];
            }
            else
            {
                welcomeImageName = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",chair.playerModel.playerGender,chair.playerModel.playerCharNumber];
            }
            self.devicePlayerAvatarImageView.image = [UIImage imageNamed:welcomeImageName];
            self.devicePlayerAvatarImageView.layer.cornerRadius = 4.0f;
            // Updated to user model to know the player image name
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            TAJUserModel *model = engine.usermodel;
            [model updateThisDeviceImageName:welcomeImageName];
            
            self.isSpectator = NO;
            
            [self.playTable playerJoined:self.playTableDictionary isThisDevicePlayerSpectator:self.isSpectator];
            
            self.playTableHolderView.userInteractionEnabled = YES;
            
            // update model for chat
            if (self.chatViewController)
            {
                [self.chatViewController updateChatPlayerModel:self.thisPlayerModel];
            }
            
            [self disableChairs];
        }
        else
        {
            [self.playTable playerJoined:self.playTableDictionary isThisDevicePlayerSpectator:self.isSpectator];
        }
        
        // set up message for live feed for join as play
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerJoinedMessage:dictionary]];
        
    }
    else if (dictionary &&
             [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId] &&
             [dictionary[GAME_PLAYER_JOIN_AS_PLAY_KEY] isEqualToString:GAME_PLAYER_JOIN_AS_MIDDLE_VALUE])
    {
        int chairID = [dictionary[kTaj_Seat_key] integerValue];
        NSString *userID = dictionary[kTaj_userid_Key];
        NSString *loggedInUserID = [[[TAJGameEngine sharedGameEngine] usermodel] userId];
        if([userID isEqualToString:loggedInUserID])
        {
            UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
            if (chairHolder)
            {
                TAJChairView *chair = [self chairFromView:chairHolder];
                chair.playerModel = nil;
            }
        }
        UIView *view = [self chairHolderAtPlaceId:chairID];
        NSArray *subviews = [view subviews];
        TAJChairView *aChair = (TAJChairView *)subviews[0];
        if(aChair.isOccupied && aChair.playerModel)
        {
            if (self.chairsArray && self.chairsArray.count > 0)
            {
                int chairPlace = 1;
                for (UIView *chairHolder in self.chairsArray)
                {
                    if (chairHolder.subviews && chairHolder.subviews.count > 0)
                    {
                        // Already added
                        NSArray *subviews = [chairHolder subviews];
                        TAJChairView *chair = (TAJChairView *)subviews[0];
                        
                        if (!chair.playerModel && !chair.isOccupied)
                        {
                            chairID =chairPlace;
                            break;
                        }
                        else
                        {
                            chairPlace++;
                        }
                    }
                }
            }
            
        }
        view = [self chairHolderAtPlaceId:chairID];
        subviews = [view subviews];
        aChair = (TAJChairView *)subviews[0];
        
        // Sound
        [[TAJSoundHandler sharedManager] playSitSound];
        
        // Init player model also
        TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:dictionary];
        
        if (dictionary[ kTaj_buyin_amt_this] && [dictionary[ kTaj_userid_Key] isEqualToString:[[[TAJGameEngine sharedGameEngine]usermodel]userId]] && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
        {
            self.buyInAmount=dictionary[ kTaj_buyin_amt_this];
        }
        else
        {
            self.buyInAmount=[NSString stringWithFormat:@"0"] ;
        }
        
        [aChair updateChairWithData:model];
        aChair.playerModel.placeID = [NSString stringWithFormat:@"%d",chairID] ;
        
        
        if ([self isThisDevicePlayer:view])
        {
            self.thisDevicePlayer = view;
            
            // Set user Id also
            TAJChairView *chair = [self chairFromView:view];
            self.thisPlayerModel = chair.playerModel;
            NSString *welcomeImageName;
            if ([TAJUtilities isIPhone])
            {
                self.avatarImageForIAmBack = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
            }
            else
            {
                self.avatarImageForIAmBack = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
                
            }
            // set image for this device player in welcome screen
            if ([TAJUtilities isIPhone])
            {
                welcomeImageName = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
                
            }
            else
            {
                welcomeImageName = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
                
            }
            self.devicePlayerAvatarImageView.image = [UIImage imageNamed:welcomeImageName];
            self.devicePlayerAvatarImageView.layer.cornerRadius = 4.0f;
            
            // Updated to user model to know the player image name
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            TAJUserModel *model = engine.usermodel;
            [model updateThisDeviceImageName:welcomeImageName];
            
            if (self.isSpectator && self.isJokerTypeGame && self.isJokerGameStarted &&
                self.maxPlayers == SIX_PLAYER)
            {
                if(self.isPlayerModelSlided)
                {
                    [self.playerModelView showInstructionPlayerMode:JOKER_TABLE_SIT_CONFIRM];
                }
                
                [self.playTable showInstruction:JOKER_TABLE_SIT_CONFIRM];
                [self disableChairs];
            }
            
            self.isSpectator = NO;
            self.playTableHolderView.userInteractionEnabled = YES;
            [self.playTable playerJoined:self.playTableDictionary isThisDevicePlayerSpectator:self.isSpectator];
            
            // update model for chat
            if (self.chatViewController)
            {
                [self.chatViewController updateChatPlayerModel:self.thisPlayerModel];
            }
        }
        else
        {
            [self.playTable playerJoined:self.playTableDictionary isThisDevicePlayerSpectator:self.isSpectator];
        }
        
        // set up live history for middle join
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerMiddleJoinMessage:dictionary]];
    }
}

// Error code implementation
- (void)handleErrorWithCodeEvent:(NSDictionary *)dictionary
{
    NSLog(@"ERROR CODE DATA :%@",dictionary);
    if (!dictionary)
    {
        return;
    }
    
    [self processErrorEvent:[dictionary[kTaj_Error_code] integerValue]];
}

- (void)cancelRejoin
{
    // if NSTimer is running
    if ([self.rejoinNSTimer isValid])
    {
        [self.rejoinNSTimer invalidate];
    }
#if NEW_INFO_VIEW_IMPLEMENTATION
    [self.rejoinAlertView hide];
#endif
    self.rejoinAlertView = nil;
    
    // at this point of time score sheet will be existing
    // so that remove score sheet and take him to lobby screen
    [self removeScoreSheetView];
    
    [[TAJGameEngine sharedGameEngine] rejoinReplyNoWithUserID:self.thisPlayerModel.playerID
                                                     nickName:self.thisPlayerModel.playerName
                                                      tableID:self.currentTableId
                                                      msgUUID:self.rejoinMsgUUid];
    
    if (self.playTableDictionary)
    {
        [self quitGame];
    }
}

#pragma mark - Spectator Case -

- (void)showSpectatorView
{
    NSString *gameStarted;
    gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
    
    if (self.isSpectator)
    {
        [self createViewForGamePlay];
        if ([gameStarted isEqualToString:@"True"])
        {
            [self playersDiscardCardHistoryOnThisPlayerSpectator:self.playTableDictionary];
        }
        if (self.isJokerTypeGame)
        {
            [self conditionForScheduleTimer:(int)self.startTimerDuration];
            self.isJokerGameStarted = YES;
            [self removeJokerTableView];
            self.spectatorView.hidden = NO;
            
            if ([gameStarted isEqualToString:@"True"])
            {
                [self showCardBackImagesForChairs];
            }
        }
        else
        {
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeInfoLabelMessagePlayerMode];
            }
            [self.playTable removeInfoLabelMessage];
            self.spectatorView.hidden = NO;
            [self updateSortButton:NO];
            [self updateFlipCardButton:NO];
            if (!self.isTossCardCame)
            {
                [self performSelector:@selector(showCardBackImagesForChairs) withObject:nil afterDelay:0.5f ];
            }
            else
            {
                self.isTossCardCame = NO;
            }
        }
    }
}

- (void)showCardBackImagesForChairs
{
    __block UIImageView *cardBackAnimationImageViewA,*cardBackAnimationImageViewB,*cardBackAnimationImageViewC,*cardBackAnimationImageViewD,*cardBackAnimationImageViewE,*cardBackAnimationImageViewF;
    
    for(NSInteger chairIndex = 0; chairIndex<self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        NSInteger placeId = [chair.playerModel.placeID integerValue];
        NSString *playType=chair.playerModel.playerType;
        if (self.maxPlayers == TWO_PLAYER && placeId == 2 )
        {
            placeId = 4;
        }
        if (!chair.isMiddleJoin )
        {
            //DLog(@"placeId = %d",placeId);
            if (![playType isEqualToString:@"table_leave"] && ![playType isEqualToString:@"drop"]  && ![playType isEqualToString:@"timeout"])
            {
                if (placeId > 0 && placeId <= 6)
                {
                    
                    if(placeId == 1 && self.isSpectator)
                    {
                        cardBackAnimationImageViewA  = self.cardBackAnimationImageViewArray[0];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewA withImage:self.firstPlayerCardBackImageArray[12]];
                        
                    }
                    if (placeId == 2)
                    {
                        cardBackAnimationImageViewB  = self.cardBackAnimationImageViewArray[1];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewB withImage:self.secondPlayerCardBackImageArray[12]];
                        
                    }
                    if (placeId == 3)
                    {
                        cardBackAnimationImageViewC  = self.cardBackAnimationImageViewArray[2];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewC withImage:self.thirdPlayerCardBackImageArray[12]];
                        
                    }
                    if (placeId == 4)
                    {
                        cardBackAnimationImageViewD  = self.cardBackAnimationImageViewArray[3];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewD withImage:self.fourthPlayerCardBackImageArray[12]];
                        
                    }
                    if (placeId == 5)
                    {
                        cardBackAnimationImageViewE  = self.cardBackAnimationImageViewArray[4];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewE withImage:self.fifthPlayerCardBackImageArray[12]];
                        
                    }
                    if (placeId == 6)
                    {
                        cardBackAnimationImageViewF  = self.cardBackAnimationImageViewArray[5];
                        [self fillupHandCardImageOnImageView:cardBackAnimationImageViewF withImage:self.sixthPlayerCardBackImageArray[12]];
                    }
                }
            }
        }
    }
}
#pragma mark - HIGHLIGHT BUTTONS -

- (BOOL)isPlayerInLobbyScreen
{
    BOOL isLobby = NO;
    if ([self.gameTableDelegate respondsToSelector:@selector(isLobbyScreenShowing:)])
    {
        isLobby = [self.gameTableDelegate isLobbyScreenShowing:[self.currentTableId integerValue]];
    }
    
    return isLobby;
}

- (void)highLightButton
{
    if ([self.gameTableDelegate respondsToSelector:@selector(highLightButtonWhenMyTurn:)])
    {
        [self.gameTableDelegate highLightButtonWhenMyTurn:[self.currentTableId integerValue]];
    }
    
}

#pragma mark - ALL TIMER FUNCTIONS -

- (void)inValidateTimerIfRunning
{
    if ([self.gameStartTimer isValid])
    {
        [self.gameStartTimer invalidate];
        self.gameStartTimer=nil;
    }
    if ([self.restartTime isValid])
    {
        [self.restartTime invalidate];
        self.restartTime = nil;
    }
}

// For start new game
- (void)startWaitingTimerForDuration:(double)duration andData:(NSDictionary *)dataDictionary
{
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    if (!self.isSpectator)
    {
        self.isThisDevicePlayerDroppedGame = NO;
    }
    for(NSInteger chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        chair.isMiddleJoin = NO;
    }
    [self removePlayerTurnTime];
    [self resetPlayerDealerArray];
    
    self.isGameScheduleState = NO;
    self.canLeaveTable = YES;
    self.canAnimate = YES;
    [self saveResultForKey:NO];
    self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
    for(NSInteger chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        chair.isMiddleJoin = NO;
    }
    
    self.noOfGameRounds++;
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    self.startTimerDuration = duration;
    
    if (self.isFirstRoundGameAlreadyStarted)
    {
        self.isFirstRoundGameAlreadyStarted = YES;
    }
    else if(!self.isFirstRoundGameAlreadyStarted)
    {
        self.isFirstRoundGameAlreadyStarted = NO;
    }
    
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView resetPlayerModelView];
#endif
    
    if ([self.previousMessage isEqualToString: SPLIT_REQUESTED_MSG])
    {
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
    }
    self.isScheduleGameCame = YES;
    self.gameStartTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                         selector:@selector(updateStartTimer:)
                                                         userInfo:dataDictionary
                                                          repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.gameStartTimer forMode:NSRunLoopCommonModes];
#endif
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    
}

- (void)updateStartTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateStartTimer ");
#endif
    
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    [self resetPlayerDealerArray];
    --self.startTimerDuration;
    [self updateChair];
    [self removePlayerTurnTime];
    
    if (self.startTimerDuration <= 0.0f)
    {
        if (self.isJokerTypeGame)
        {
            self.isStrikesMiddleJoin = YES;
        }
        NSString *description = PLEASE_WAIT_FOR_TOSS;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
        self.previousMessage = self.playTable.currentMessage;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:description];
        }
        [self.playTable showInstruction:description];
        [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
        
        // set up message for for live feed
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerNewGameMessage:[timer userInfo] isJokerGame:self.isJokerTypeGame noOfRound:self.noOfGameRounds]];
        // Stop timer
        [self refreshChairView];
        [self.gameStartTimer invalidate];
        self.gameStartTimer = Nil;
        self.canShowIamBackView = YES;
        
    }
    else
    {
        [self.playTable removeOpenDeckHolderCards];
        [self.playTable removeMyDeckCardsFromHolder];
        [self.playTable clearCardsinClosedCard];
        
        if (self.meldViewTable)
        {
            [self removeMeldView];
        }
        if (self.playTable && !self.isGameStarted)
        {
            [self conditionForScheduleTimer:(int)self.startTimerDuration];
            
            // Show timer in table view
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            [self.playTable removeExtraMessage];
            NSString *description = [NSString stringWithFormat:PLESE_WAIT_FOR_START_GAME, (int)self.startTimerDuration];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showInstructionPlayerMode:description];
            }
            
            [self.playTable showInstruction:description];
        }
        else if (self.playTable && self.isGameStarted)
        {
            [self conditionForScheduleTimer:(int)self.startTimerDuration];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            // Show whose turn is and how much time left
            [self.playTable removeExtraMessage];
            
            NSString *description = [NSString stringWithFormat:PLESE_WAIT_FOR_START_GAME, (int)self.startTimerDuration];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showInstructionPlayerMode:description];
            }
            
            [self.playTable showInstruction:description];
        }
    }
    if (self.startTimerDuration <= 5.0f)
    {
        [self removeJokerTableView];
        self.isFirstRoundGameAlreadyStarted = YES;
    }
}

- (void)conditionForScheduleTimer:(int)time
{
    if (time <= 10)
    {
        self.canLeaveTable = NO;
        self.leaveTableMessage = CANNOT_LEAVE_TABLE_MESSAGE;
        [self removeInstanceAlertViews];
        if (self.splitRejectAlertView)
        {
            [self.splitRejectAlertView hide];
            self.splitRejectAlertView = nil;
        }
        
    }
    if (time <= 5)
    {
        self.isGameScheduleState = YES;
        if (self.isSpectator)
        {
            //if any watching player exist
            [self disableChairs];
            if (self.isJokerTypeGame)
            {
                self.isJokerGameStarted = YES;
            }
        }
    }
    [self updateSortButton:NO];
    [self updateFlipCardButton:NO];
    [self updateExtraTimeButton:NO];
}

// For player to discard card
- (void)startTimerForPlayerToDiscardCard:(float)duration andData:(NSDictionary *)dataDictionary
{
    NSLog(@"startTimerForPlayerToDiscardCard : %@",dataDictionary);
    
    NSInteger extraTime = [[dataDictionary valueForKey:@"TAJ_auto_extra_time"] integerValue];
    
    [self removePlayerTurnTime];
    [self removeScoreSheetView];
    
    //at any given point if NSTimer running
    if ([self.playerTime isValid])
    {
        //after discard card of any player stop timer
        [self.playerTime invalidate];
        
    }
    
    // here if joker type game we need to allow the specator to take a seat to play the game
    if (self.isSpectator && self.isJokerTypeGame && self.isJokerGameStarted &&
        self.maxPlayers == SIX_PLAYER && self.canMiddleJoin && !self.thisPlayerModel)
    {
        [self unSelectChair];
        if(self.isPlayerModelSlided)
        {
            // [self.playerModelView performSelector:@selector(showInstructionPlayerMode:) withObject:JOKER_TABLE_SIT_MESSAGE afterDelay:0.0f];
        }
        
        // [self.playTable showInstruction:JOKER_TABLE_SIT_MESSAGE];
        
    }
    NSTimeInterval oldTime = [[NSDate date] timeIntervalSince1970];
    //NSLog(@"loadTime %f,%@",oldTime,dataDictionary);
    
    [self setUpPlayerTurnTime:(int)duration forUserID:[dataDictionary[kTaj_userid_Key] integerValue] extraTime:extraTime];
    
    self.playerTimeoutTime = duration;
    self.playerTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                     selector:@selector(updatePlayerTimer:)
                                                     userInfo:dataDictionary
                                                      repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.playerTime forMode:NSRunLoopCommonModes];
#endif
}

- (void) autoDiscardPickedCardOnTimeOut
{
    //    DLog(@"auto discard %@",self.pickedCard);
    if (self.myCardsDealArray.count == 14 && self.maxPlayers == SIX_PLAYER  && self.isIamBackButtonPressed && self.currentPlayers > 2)
    {
        [self.playerTimeForNegetiveDiscard invalidate];
        self.playerTimeForNegetiveDiscard = nil;
        self.playerExtraTimeoutTime = 0;
        if (self.pickedCard)
        {
            NSString *cardNum = self.pickedCard[kTaj_Card_Face_Key];
            NSString *cardSuit = self.pickedCard[kTaj_Card_Suit_Key];
            [[TAJGameEngine sharedGameEngine] cardAutoDiscardWithFace:cardNum
                                                         withUserName:self.thisPlayerModel.playerName
                                                             withSuit:[cardSuit lowercaseString]
                                                              tableID:self.currentTableId
                                                               userID:self.thisPlayerModel.playerID];
            NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, [cardSuit uppercaseString]];
            
            DLog(@"auto discard");
            NSLog(@"AUTO DISCARD");
            [self.playTable placeCardInOpenDeck:cardId];
        }
        
        self.pickedCard =nil;
        
    }
    
}

- (void)stopTimer
{
    [self performSelectorOnMainThread:@selector(stopTimerVars) withObject:nil waitUntilDone:NO];
    
}

- (void)stopTimerVars
{
    [self.playerTime invalidate];
    self.playerTime = nil;
    self.isDoubleLoggedIn = YES;
    
}
- (void)updatePlayerTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updatePlayerTimer ");
#endif
    
    NSDictionary *playerTurnDict = nil;
    
    if (self.isDoubleLoggedIn)
    {
        [self.playerTime invalidate];
        self.playerTime = nil;
        return;
    }
    --self.playerTimeoutTime;
    if (!self.isMyTurn && self.isIamBackButtonPressed)
    {
        if (self.myCardsDealArray.count == 14 ||self.myCardsDealArray.count == 12)
        {
            [[TAJGameEngine sharedGameEngine] destroySocketOnError];
            if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canShowIAmBackView:)])
            {
                [self.gameTableDelegate canShowIAmBackView:YES];
            }
            
        }
    }
    if (self.playerTimeoutTime == 0)
    {
        if(self.isIamBackButtonPressed)
        {
            [self performSelector:@selector(autoDiscardPickedCardOnTimeOut) withObject:Nil afterDelay:2.0f];
        }
    }
    if(self.playerTimeoutTime > 5)
    {
        [self updateDropButton];
        self.canLeaveTable = YES;
        if (self.isThisDeviceTurn/* && self.currentTurnChair */)
        {
            self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
        }
        
        if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
        {
            [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
        }
    }
    
    if (self.playerTimeoutTime <= 0)
    {
        playerTurnDict = [timer userInfo];
        NSLog(@"playerTurnDict : %@",playerTurnDict);
        
        //for extra timer run - 10seconds
        int turnUserId = [[playerTurnDict valueForKey:@"TAJ_user_id"] intValue];
        NSInteger extraTime = [[playerTurnDict valueForKey:@"TAJ_auto_extra_time"] integerValue];
        //using it for chunks update
        NSInteger placeId = [self getPlaceIdFromChairUserID:turnUserId];
        //[self playerExtraTimerSetUpWithPlayerTurn:placeId];
        [self runExtraTimerWithPalyerTurn:placeId extraTime:extraTime];
        
        //TAJChairView *chairView =  [[TAJChairView alloc] init];
        //[chairView updateChairWithChunksData:playerTurnDict WithplaceID:placeId];
        
        //end here
        
        [self.gameTableDelegate hideTableAlertImageViewWhenTurnTimeOut];
        //[self removePlayerTurnTime];
        
        [self removeInstanceAlertViews];
        
        if (self.currentTurnChair)
        {
            // Note : In future, if any change comes for current turn player we can make use of self.currentTurnChair
            [self updateDropButtonWhenTurnTimeZero];
            if (self.isCardPicked || self.myCardsDealArray.count == 14)
            {
                if (self.isMyTurn)
                {
                    self.playerExtraTimeoutTime = 10;
                    self.playerTimeForNegetiveDiscard = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                                                       selector:@selector(disableCardDiscard:)
                                                                                       userInfo:nil
                                                                                        repeats:YES];
                    NSLog(@"CHECKING DISABLES 1");
                }
            }
            else
            {
                NSLog(@"CHECKING DISABLES");
                [self.playTable removeAllButtons];
                [self.playTable canDiscardCard:NO];
                [self.playerModelView removeAllModelButtons];
                [self.playerModelView canModelDiscardCard:NO];
                
            }
            
            //commented to implement extra time
            //[self.playTable otherPlayerTurnUserInteration];
            
            //[self updateExtraTimeButton:NO];
        }
        else
        {
            [self.playTable removeAllButtons];
            
        }
        // Stop timer
        [self.playerTime invalidate];
        self.playerTime = nil;
    }
    else if (self.playerTimeoutTime <= 10)
    {
        playerTurnDict = [timer userInfo];
        
        // Show whose card turn is and how much time left
        if (self.playerTimeoutTime <= 10)
        {
            if (self.isThisDeviceTurn)
            {
                if ([[TAJUtilities sharedUtilities] isInternetConnected])
                {
                    //Sound
                    if (self.playerTimeoutTime == 10)
                    {
                        [[TAJSoundHandler sharedManager] playBellSound];
                    }
                    else
                    {
                        [[TAJSoundHandler sharedManager] playClockSound];
                    }
                    
                    if (self.playerTimeoutTime == 5)
                    {
                        [self vibrateDevice];
                    }
                }
            }
        }
        if(self.playerTimeoutTime <= 5)
        {
            self.canLeaveTable = NO;
            self.leaveTableMessage = CANNOT_LEAVE_TABLE_MESSAGE;
            
            if (self.isThisDeviceTurn/* && self.currentTurnChair */)
            {
                self.isPlayerCanEnterGamePlayInAutoPlayMode = NO;
            }
            
            if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
            {
                [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
            }
        }
    }
    else
    {
        playerTurnDict = [timer userInfo];
    }
}

//do not allow player to discard card after -5 seconds of player time
- (void)disableCardDiscard:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for disableCardDiscard ");
#endif
    
    self.playerExtraTimeoutTime--;
    if (self.playerExtraTimeoutTime == 0)
    {
        [self.playTable canDiscardCard:NO];
        [self.playTable removeAllButtons];
        self.isCardPicked = NO;
        [self.playerModelView canModelDiscardCard:NO];
        [self.playerModelView removeAllModelButtons];
        self.playerExtraTimeoutTime = 0;
        [self.playerTimeForNegetiveDiscard invalidate];
    }
}

// For other player placing show
- (void)startTimerForOtherPlayerToShow:(float)duration andData:(NSDictionary *)dataDictionary
{
    //at any given point if NSTimer running
    if ([self.playerShowTime isValid])
    {
        //stop timer
        [self.playerShowTime invalidate];
        
    }
    self.showTimeoutTime = duration;
    [self vibrateDevice];
    [[TAJSoundHandler sharedManager] playMeldSound];
    self.playerShowTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                         selector:@selector(updateShowTimer:)
                                                         userInfo:dataDictionary
                                                          repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.playerShowTime forMode:NSRunLoopCommonModes];
#endif
    
    NSString *message = [NSString stringWithFormat:SHOW_MESSAGE_OTHER, dataDictionary[kTaj_nickname]];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView showInstructionPlayerMode:message];
    }
    
    [self.playTable showInstruction:message];
}

- (void)updateShowTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateShowTimer ");
#endif
    
    NSDictionary *playerShowDict = nil;
    
    --self.showTimeoutTime;
    if (self.showTimeoutTime <= 0)
    {
        // Stop timer
        [self.playerShowTime invalidate];
        self.playerShowTime = nil;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
        
    }
    else
    {
        playerShowDict = [timer userInfo];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeExtraMessagePlayerMode];
        }
        [self.playTable removeExtraMessage];
        // Show whose is placing show
        NSString *description = [NSString stringWithFormat:PLAYER_SHOW_MESSAGE, playerShowDict[kTaj_nickname] , (int)self.showTimeoutTime];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:description];
        }
        [self.playTable showInstruction:description];
    }
}

// For restart new game
- (void)restartTimerToNewGame:(double)duration andData:(NSDictionary *)dictionary
{
    self.playTable.isThisPlayerCardDiscarded = NO;
    [self.playerShowTime invalidate];
    self.playerShowTime = nil;
    
    self.didSendMeldCards = NO;
    
    if (self.splitRejectAlertView)
    {
        [self.splitRejectAlertView hide];
        self.splitRejectAlertView = nil;
    }
    for(NSInteger chairIndex = 0; chairIndex < self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        chair.isMiddleJoin = NO;
    }
    [self hideViewForNewGame];
    self.isGameScheduleState = YES;
    if ([self.gameStartTimer isValid])
    {
        [self.gameStartTimer invalidate];
        self.gameStartTimer = nil;
    }
    //at any given point if NSTimer running
    if ([self.restartTime isValid])
    {
        //stop timer
        [self.restartTime invalidate];
        
    }
    if(!self.isJokerTypeGame)
    {
        [self disableChairs];
    }
    
    if (self.meldViewTable)
    {
        [self removeMeldView];
    }
    
    [self.playTable removeOpenDeckHolderCards];
    [self.playTable removeMyDeckCardsFromHolder];
    [self.playTable removeAllButtonsWithMeldGroup];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
    
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView resetPlayerModelView];
    [self.playerModelView removeAllModelButtons];
    [self.playerModelView removeMeldGroupAllButton];
#endif
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    //initialize all controller once again
    self.restartTimeoutTime = duration;
    self.restartTime = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                      selector:@selector(updateRestartTimer:)
                                                      userInfo:dictionary
                                                       repeats:YES];
    
    
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.restartTime forMode:NSRunLoopCommonModes];
#endif
    
}

- (void)updateRestartTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateRestartTimer ");
#endif
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    NSDictionary *playerTurnDict = nil;
    if(!self.isJokerTypeGame)
    {
        [self disableChairs];
    }
    [self hideViewForNewGame];
    [self.playTable removeOpenDeckHolderCards];
    --self.restartTimeoutTime;
    [self removeAllDiscardCardHistoryContent];
    [self.playTable removeOpenDeckHolderCards];
    [self removePlayerTurnTime];
    if (self.restartTimeoutTime <= 0)
    {
        NSString *description;
        // Show timer in info
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeInfoLabelMessage];
        
        if (self.isJokerTypeGame)
        {
            description = PLEASE_WAIT_FOR_TOSS;
            self.previousMessage = self.playTable.currentMessage;
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView showInstructionPlayerMode:description];
            }
            [self.playTable showInstruction:description];
            [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
            [self refreshChairView];
        }
        if(self.isPlayerModelSlided)
        {    [self.playerModelView removeExtraMessagePlayerMode];
            [self.playerModelView removeInfoLabelMessagePlayerMode];
        }
        [self.playTable removeExtraMessage];
        
        [self.playTable removeInfoLabelMessage];
        self.previousMessage = self.playTable.currentMessage;
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:description];
        }
        [self.playTable showInstruction:description];
        [self performSelector:@selector(removePlayTableMessage) withObject:nil afterDelay:2.0f];
        
        // set up message for live feed
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerNewGameMessage:[timer userInfo] isJokerGame:self.isJokerTypeGame noOfRound:self.noOfGameRounds]];
        
        // Stop timer
        if ([self.restartTime isValid])
        {
            [self.restartTime invalidate];
        }
        self.canShowIamBackView = YES;
        
    }
    else
    {
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeExtraMessagePlayerMode];
        }
        playerTurnDict = [timer userInfo];
        [self conditionForScheduleTimer:(int)self.restartTimeoutTime];
        // Show info for start game
        [self.playTable removeExtraMessage];
        
        NSString *description = [NSString stringWithFormat:PLESE_WAIT_FOR_START_GAME, (int)self.restartTimeoutTime];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView showInstructionPlayerMode:description];
        }
        [self.playTable showInstruction:description];
    }
    if (self.restartTimeoutTime <= 5.0f)
    {
        [self removeJokerTableView];
        self.isFirstRoundGameAlreadyStarted = YES;
    }
    
}

// split timer for other player
-(void)splitTimerForOtherPlayer:(double)duration andData:(NSDictionary *)dictionary
{
    if ([self.splitNSTimer isValid])
    {
        [self.splitNSTimer invalidate];
    }
    
    [self removeScoreSheetView];
    [self refreshChairView];
    
    //initialize all controller once again
    self.splitTime = duration;
    
    NSString *description = [NSString stringWithFormat:ALERT_SPLIT_TIMER_TITLE,dictionary[kTaj_split_requester], (int)self.splitTime];
    
    if (!self.splitRequestAlertView && !self.isSpectator)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        self.splitRequestAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_SPLIT_REQUEST_TIMER_TAG popupButtonType:eYes_No_Type message:description];
#else
        self.splitRequestAlertView = [[UIAlertView alloc]initWithTitle:ALERT_EMPTY_TITLE
                                                               message:description
                                                              delegate:self
                                                     cancelButtonTitle:YES_STRING
                                                     otherButtonTitles:NO_STRING,nil];
        
        self.splitRequestAlertView.tag = ALERT_SPLIT_REQUEST_TIMER_TAG;
#endif
        [self.splitRequestAlertView show];
    }
    
    self.splitNSTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                       selector:@selector(updateSplitTimer:)
                                                       userInfo:dictionary
                                                        repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.splitNSTimer forMode:NSRunLoopCommonModes];
#endif
    
}

- (void)updateSplitTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateSplitTimer ");
#endif
    
    NSDictionary *playerSplitDict = nil;
    playerSplitDict = [timer userInfo];
    
    --self.splitTime;
    if (self.splitTime <= 0)
    {
        // Stop timer
        [self.splitNSTimer invalidate];
        
        NSString *description = [NSString stringWithFormat:ALERT_SPLIT_TIMER_TITLE,playerSplitDict[kTaj_split_requester], (int)self.splitTime];
        
        if (self.splitRequestAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            self.splitRequestAlertView.messageString = description;
            self.splitRequestAlertView.message.text = description;
#else
            self.splitRequestAlertView.message = description;
            
#endif
            [self removeAlertView:self.splitRequestAlertView];
        }
        
    }
    else
    {
        // Show info for split
        NSString *description = [NSString stringWithFormat:ALERT_SPLIT_TIMER_TITLE,playerSplitDict[kTaj_split_requester], (int)self.splitTime];
        
        if (self.splitRequestAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            self.splitRequestAlertView.messageString = description;
            self.splitRequestAlertView.message.text = description;
#else
            self.splitRequestAlertView.message = description;
#endif
        }
        
    }
}

// to rejoin to same table
- (void)rejoinTimerToJoin:(double)duration andData:(NSDictionary *)dictionary
{
    if ([self.rejoinNSTimer isValid])
    {
        [self.rejoinNSTimer invalidate];
    }
    
    //initialize all controller once again
    self.rejoinTime = duration;
    NSString *description = [NSString stringWithFormat:REJOIN_TIMER_MESSAGE, (int)self.rejoinTime];
    
    if (!self.rejoinAlertView && !self.isSpectator)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        
        self.rejoinAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_CAN_REJOIN_PLAY_TAG popupButtonType:eOK_Cancel_Type message:description];
        
#else
        self.rejoinAlertView = [[UIAlertView alloc]initWithTitle:ALERT_LEVEL_SORRY_TITLE
                                                         message:description
                                                        delegate:self
                                               cancelButtonTitle:OK_STRING
                                               otherButtonTitles:CANCEL_STRING,nil];
        
        self.rejoinAlertView.tag = ALERT_CAN_REJOIN_PLAY_TAG;
#endif
        [self.rejoinAlertView show];
    }
    
    self.rejoinNSTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                        selector:@selector(updateRejoinTimer:)
                                                        userInfo:dictionary
                                                         repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.rejoinNSTimer forMode:NSRunLoopCommonModes];
#endif
    
}

- (void)updateRejoinTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateRejoinTimer ");
#endif
    
    NSDictionary *playerRejoinDict = nil;
    
    --self.rejoinTime;
    if (self.rejoinTime <= 0)
    {
        NSString *description = [NSString stringWithFormat:REJOIN_TIMER_MESSAGE, (int)self.rejoinTime];
        
        if (self.rejoinAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            self.rejoinAlertView.messageString = description;
            self.rejoinAlertView.message.text = description;
#else
            self.rejoinAlertView.message = description;
#endif
            [self removeAlertView:self.rejoinAlertView];
        }
        
        // Stop timer
        [self.rejoinNSTimer invalidate];
    }
    else
    {
        playerRejoinDict = [timer userInfo];
        
        // Show info for rejoin
        NSString *description = [NSString stringWithFormat:REJOIN_TIMER_MESSAGE, (int)self.rejoinTime];
        
        if (self.rejoinAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            self.rejoinAlertView.messageString = description;
            self.rejoinAlertView.message.text = description;
#else
            self.rejoinAlertView.message = description;
#endif
        }
        
    }
}

#pragma mark - Alert view instance -

#if NEW_INFO_VIEW_IMPLEMENTATION

- (void)removeAlertView:(TAJInfoPopupViewController *)alert
{
    if (alert.tag == ALERT_CAN_REJOIN_PLAY_TAG)
    {
        //rejoin alert timeout
        [alert hide];
        
        // if NSTimer is running
        if ([self.rejoinNSTimer isValid])
        {
            [self.rejoinNSTimer invalidate];
        }
        self.rejoinAlertView = nil;
        // at this point of time score sheet will be existing
        // so that remove score sheet and take him to lobby screen
        [self removeScoreSheetView];
        
        if (self.playTableDictionary)
        {
            if (!self.isRejoinSuccessCame)
            {
                [self quitGame];
            }
        }
        
    }
    else if(alert.tag == ALERT_SPLIT_REQUEST_TIMER_TAG)
    {
        // forcibely NO button tapping
        [[TAJGameEngine sharedGameEngine] splitRejectReplyWithMsgUUID:self.splitMsgUUid
                                                               userID:self.thisPlayerModel.playerID
                                                             nickName:self.thisPlayerModel.playerName
                                                              tableID:self.currentTableId];
        
        [alert hide];
        self.splitRequestAlertView = nil;
    }
}

#else

- (void)removeAlertView:(UIAlertView *)alert
{
    if (alert.tag == ALERT_CAN_REJOIN_PLAY_TAG)
    {
        //rejoin alert timeout
        [alert dismissWithClickedButtonIndex:1 animated:NO];
        
        // if NSTimer is running
        if ([self.rejoinNSTimer isValid])
        {
            [self.rejoinNSTimer invalidate];
        }
        self.rejoinAlertView = nil;
        // at this point of time score sheet will be existing
        // so that remove score sheet and take him to lobby screen
        [self removeScoreSheet];
        
        if (self.playTableDictionary)
        {
            //[self quitGame];
        }
        
    }
    else if(alert.tag == ALERT_SPLIT_REQUEST_TIMER_TAG)
    {
        // forcibely NO button tapping
        [[TAJGameEngine sharedGameEngine] splitRejectReplyWithMsgUUID:self.splitMsgUUid
                                                               userID:self.thisPlayerModel.playerID
                                                             nickName:self.thisPlayerModel.playerName
                                                              tableID:self.currentTableId];
        
        [alert dismissWithClickedButtonIndex:1 animated:NO];
        self.splitRequestAlertView = nil;
    }
}

#endif

- (void)removeInstanceAlertViews
{
    if (self.cantLeaveAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.cantLeaveAlertView hide];
#else
        [self.cantLeaveAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.cantLeaveAlertView = nil;
    }
    if (self.leaveAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.leaveAlertView hide];
#else
        [self.leaveAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.leaveAlertView = nil;
    }
    if (self.noHandAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.noHandAlertView hide];
#else
        [self.noHandAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.noHandAlertView = nil;
    }
    if (self.noEntryAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.noEntryAlertView hide];
#else
        [self.noEntryAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.noEntryAlertView = nil;
    }
    //remove drop or show button if exist once loading of game result screen
    if (self.dropButtonAlertView)
    {
        //CANCEL BUTTON pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.dropButtonAlertView hide];
#else
        [self.dropButtonAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.dropButtonAlertView = nil;
    }
    if (self.showButtonAlertView)
    {
        //CANCEL BUTTON pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.showButtonAlertView hide];
#else
        [self.showButtonAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.showButtonAlertView = nil;
    }
    if (self.reshuffleAlertView)
    {
        // OK BUTTON Presssed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.reshuffleAlertView hide];
#else
        [self.reshuffleAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.reshuffleAlertView = nil;
    }
    if (self.rebuyAlertView)
    {
        // CANCEL BUTTON Presssed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.rebuyAlertView hide];
#else
        [self.rebuyAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.rebuyAlertView = nil;
    }
    
    if (self.leaveForJokerAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.leaveForJokerAlertView hide];
#else
        [self.leaveForJokerAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.leaveForJokerAlertView = Nil;
    }
}

- (void)removeUserTouchRequestAlertViews
{
    if (self.cantLeaveAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.cantLeaveAlertView hide];
#else
        [self.cantLeaveAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.cantLeaveAlertView = nil;
    }
    if (self.leaveAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.leaveAlertView hide];
#else
        [self.leaveAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.leaveAlertView = nil;
    }
    if (self.noHandAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.noHandAlertView hide];
#else
        [self.noHandAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.noHandAlertView = nil;
    }
    if (self.noEntryAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.noEntryAlertView hide];
#else
        [self.noEntryAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.noEntryAlertView = nil;
    }
    //remove drop or show button if exist once loading of game result screen
    if (self.dropButtonAlertView)
    {
        //CANCEL BUTTON pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.dropButtonAlertView hide];
#else
        [self.dropButtonAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.dropButtonAlertView = nil;
    }
    if (self.showButtonAlertView)
    {
        //CANCEL BUTTON pressed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.showButtonAlertView hide];
#else
        [self.showButtonAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.showButtonAlertView = nil;
    }
    if (self.reshuffleAlertView)
    {
        // OK BUTTON Presssed
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.reshuffleAlertView hide];
#else
        [self.reshuffleAlertView dismissWithClickedButtonIndex:0 animated:YES];
#endif
        self.reshuffleAlertView = nil;
    }
    
    if (self.leaveForJokerAlertView)
    {
#if NEW_INFO_VIEW_IMPLEMENTATION
        [self.leaveForJokerAlertView hide];
#else
        [self.leaveForJokerAlertView dismissWithClickedButtonIndex:1 animated:YES];
#endif
        self.leaveForJokerAlertView = Nil;
    }
}

#pragma mark - Play Table View Delegate methods -

- (void)showExtraMessageDelegate:(NSString *)message
{
    if (self.isPlayerModelSlided)
    {
        [self.playerModelView showExtraMessagePlayerMode:message];
    }
}
- (void)removeExtraMessageDelegate
{
    if (self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
}
- (void)removeInfoLabelMessageDelegate
{
    if (self.isPlayerModelSlided)
    {
        [self.playerModelView removeInfoLabelMessagePlayerMode];
    }
}
- (void)showInstructionDelegate:(NSString *)instruction
{
    if (self.isPlayerModelSlided)
    {
        [self.playerModelView showInstructionPlayerMode:instruction];
    }
}
- (void)discardCardFromPlayerMode
{
    if (self.isPlayerModelSlided)
    {
        [self.playerModelView automaticCardDiscardToOpenDeck];
    }
}

- (void)didEnableShowButton:(BOOL)boolValue withDiscardInfo:(NSDictionary *)discardCardInfo
{
    if (discardCardInfo)
    {
        self.discardCardInfoForShow = nil;
        [self updateShowButton:YES];
        
        [self.playTable canAutomaticDiscardCard:YES];
        self.discardCardInfoForShow = [NSMutableDictionary dictionaryWithDictionary:discardCardInfo];
    }
    else if(!discardCardInfo)
    {
        [self updateShowButton:NO];
        
        [self.playTable canAutomaticDiscardCard:NO];
    }
}

- (void)groupCardsAtIndexes:(NSMutableArray *)indexArray
{
#if CARD_GROUPING_SLOTS
    
    [self.cardSlotManager exchangeSlotCardsWhileGroup:indexArray];
    
    // update table cards
    [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
    
    [self.playTable wantToShowMeldGroupButton:YES];
    
#if PLAYER_MODEL_GAME_CARD
    // Updated player model cards
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
    
    [self.playerModelView canShowMeldGroupForPlayerModel:YES];
#endif
    
#else
    NSMutableArray *subArray = [NSMutableArray array];
    for (NSNumber *indexNumber in indexArray)
    {
        [subArray addObject:[self.myCardsDealArray objectAtIndex:[indexNumber unsignedIntegerValue]]];
    }
    
    for (int i = 0; i < subArray.count; i++)
    {
        [self removeCardWithCardNumber:subArray[i][kTaj_Card_Face_Key] withSuit:subArray[i][kTaj_Card_Suit_Key]];
    }
    
    NSIndexSet *indexSet = [NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, subArray.count)];
    [self.myCardsDealArray insertObjects:subArray atIndexes:indexSet];
    
    [subArray removeAllObjects];
#endif
    
}

- (void)swapCard:(NSUInteger)cardAindex withCard:(NSUInteger)cardBindex
{
#if CARD_GROUPING_SLOTS
    
    [self.cardSlotManager swapCard:cardAindex withCard:cardBindex];
    [self.playTable wantToShowMeldGroupButton:YES];
    
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView canShowMeldGroupForPlayerModel:YES];
#endif
    
#else
    [self.myCardsDealArray exchangeObjectAtIndex:cardAindex withObjectAtIndex:cardBindex];
#endif
}

- (void)resetCardSlots
{
    [self.cardSlotManager resetCardSlotsInSlotManager];
}

- (void)removeEmptySlotFromIndex:(NSUInteger)cardAindex withIndex:(NSUInteger)cardBindex
{
#if CARD_GROUPING_SLOTS
    [self.cardSlotManager swapCard:cardAindex withCard:cardBindex];
    
    [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
}

- (void)removeDiscardedCardForMeldSetup:(NSUInteger)index
{
#if CARD_GROUPING_SLOTS
    [self.cardSlotManager discardedCardAtSlotIndex:index];
    
    // update table cards
    [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
#if PLAYER_MODEL_GAME_CARD
    // update player model cards
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
    
#endif
}

- (void)didDiscardedCardAtIndex:(NSUInteger)index
{
    [self removePlayerTurnTime];
    self.isCardDiscarded = YES;
    [self.playerTimeForNegetiveDiscard invalidate];
    self.playerTimeForNegetiveDiscard = nil;
    self.playerExtraTimeoutTime = 0;
    //Sound
    [[TAJSoundHandler sharedManager] playDrawCard_DiscardSound];
    
#if CARD_GROUPING_SLOTS
    
    NSDictionary *cardInfo = self.slotCardInfoArray[index];
    
    NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
    NSString *cardSuit = cardInfo[kTaj_Card_Suit_Key];
    
    [[TAJGameEngine sharedGameEngine] cardDiscardWithFace:cardNum
                                             withUserName:self.thisPlayerModel.playerName
                                                 withSuit:[cardSuit lowercaseString]
                                                  tableID:self.currentTableId
                                                   userID:self.thisPlayerModel.playerID];
    
    [self.cardSlotManager discardedCardAtSlotIndex:index];
    [self.playTable performSelector:@selector(updateSlotCardsOnMyDeck:) withObject:([self.cardSlotManager getSlotCardsArray]) afterDelay:0.3];
    // [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
    [self setIsMyTurn:NO];
#if PLAYER_MODEL_GAME_CARD
    // update player model cards
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
    
#else
    NSDictionary *cardInfo = self.myCardsDealArray[index];
    
    NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
    NSString *cardSuit = cardInfo[kTaj_Card_Suit_Key];
    
    [[TAJGameEngine sharedGameEngine] cardDiscardWithFace:cardNum
                                             withUserName:self.thisPlayerModel.playerName
                                                 withSuit:[cardSuit lowercaseString]
                                                  tableID:self.currentTableId
                                                   userID:self.thisPlayerModel.playerID];
    
    //Remove card from my deck
    [self removeCardAtindex:index fromDeck:self.myCardsDealArray];
    [self.playTable performSelector:@selector(updateMyDeckWithCards:) withObject:(self.myCardsDealArray) afterDelay:0.3];
#endif
    
    [self updateShowButton:NO];
    [self updateExtraTimeButton:NO];
    
    if (!self.isSpectator)
    {
        [self updateFlipCardButton:YES];
    }
    
    //IMP: Who ever goint to discard card they wont get CARD_DISCARD Event
    // store the last open card content, to make used for live feed
    self.lastOpenDeckCardDict = [NSMutableDictionary dictionary];
    [self.lastOpenDeckCardDict setObject:cardNum forKey:kTaj_Card_Face_Key];
    [self.lastOpenDeckCardDict setObject:[cardSuit lowercaseString] forKey:kTaj_Card_Suit_Key];
    
    // set up message for live feed
    NSMutableDictionary *discardedCard = [NSMutableDictionary dictionary];
    [discardedCard setObject:self.thisPlayerModel.playerName forKey:kTaj_nickname];
    [discardedCard setObject:cardNum forKey:kTaj_Card_Face_Key];
    [discardedCard setObject:[cardSuit lowercaseString] forKey:kTaj_Card_Suit_Key];
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDiscardMessage:discardedCard]];
    // store the card content to discard card history
    discardedCard = [NSMutableDictionary dictionary];
    [discardedCard setObject:self.thisPlayerModel.playerName forKey:kTaj_nickname];
    [discardedCard setObject:cardNum forKey:kTaj_Card_Face_Key];
    [discardedCard setObject:[cardSuit lowercaseString] forKey:kTaj_Card_Suit_Key];
    [discardedCard setObject:@"False" forKey:kTaj_autoPlay_status];
    [self addPlayersDiscardedCard:discardedCard];
    self.starImageForCardInfo = [NSMutableDictionary dictionary];
    [self.starImageForCardInfo setObject:self.thisPlayerModel.playerName forKey:kTaj_nickname];
    [self.starImageForCardInfo setObject:cardInfo forKey:kTaj_StarCardInfo];
}

- (void)didSelectClosedDeck
{
    if (self.closedDeckArray.count > 0)
    {
        //Sound
        [[TAJSoundHandler sharedManager] playDrawCard_DiscardSound];
        
        NSDictionary *cardInfo = [self.closedDeckArray lastObject];
        
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = cardInfo[kTaj_Card_Suit_Key];
        
        if (self.isPlayerModelSlided)
        {
#if PLAYER_MODEL_GAME_CARD_ANIMATION
            NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, [cardType uppercaseString]];
            // animation goes here
            NSArray *nibContents;
            // Place new card
            int index = 0;
            
            if ([TAJUtilities isIPhone])
            {
                index = [TAJUtilities isItIPhone5]? 0 : 1;
            }
            nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil  options:nil];
            
            TAJCardView *newCard = nibContents[index];
            // float width, height;
            
            CGRect startRect=[self.playerModelView.myDeckHolder convertRect:self.playTable.closedDeckCardHolder.frame fromView:self.playTable];
            
            newCard.frame=startRect;
            
            [newCard showCard:cardId isMomentary:YES];
            //        UIView *view=[self.myDeckCardsArray lastObject];
            UIView *view=[self.playerModelView getLastCard];
            
            if ([TAJUtilities isIPhone])
            {
                newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.6f, 0.6f);
            }
            else
            {
                newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.8f, 0.8f);
            }
            CGRect finalRect;
            if ([TAJUtilities isIPhone])
            {
                finalRect = CGRectMake(view.frame.origin.x+18, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            }
            else
            {
                finalRect= CGRectMake(view.frame.origin.x+30, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            }
            
            [self.playerModelView.myDeckHolder addSubview:newCard];
            [UIView animateWithDuration:0.10
                                  delay:0.0
                                options: UIViewAnimationOptionCurveEaseOut
                             animations:
             ^{
                 newCard.frame=finalRect;
                 
             }
                             completion:^(BOOL finished)
             {
                 [newCard removeFromSuperview];
             }];
#endif
        }
        
        [[TAJGameEngine sharedGameEngine] cardPickWithFace:cardNum
                                                 withStack:kFace_Down_Stack_Key
                                                  withSuit:[cardType lowercaseString]
                                                   tableID:self.currentTableId
                                                    userID:self.thisPlayerModel.playerID
                                                  nickName:self.thisPlayerModel.playerName];
        self.isCardPicked = YES;
        
        self.pickedCard = [NSMutableDictionary dictionaryWithObjectsAndKeys:cardNum, kTaj_Card_Face_Key,
                           [cardType uppercaseString], kTaj_Card_Suit_Key, nil];
        DLog(@"picked card from closed deck %@",self.pickedCard);
        
        [self.playTable cardPickedFromClosedStack:[NSString stringWithFormat:@"%@%@",cardNum, [cardType uppercaseString]]];
        
        if (self.pickedCard)
        {
            [self.myCardsDealArray addObject:self.pickedCard];
            
#if CARD_GROUPING_SLOTS
            [self.cardSlotManager insertPickedCardToSlotArray:self.pickedCard];
            [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
            
#if PLAYER_MODEL_GAME_CARD
            // Update player model card
            [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
            
#else
            [self.playTable updateMyDeckWithCards:self.myCardsDealArray];
#endif
        }
        
        //remove closed deck card because we wont get responce for this player
        [self.closedDeckArray removeLastObject];
        
        [self updateDropButton];
        
        // set up message for closed deck live feed
        NSMutableDictionary *dictionary = [NSMutableDictionary dictionary];
        [dictionary setObject:self.thisPlayerModel.playerName forKey:kTaj_nickname];
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerPickClosedDeckMessage:dictionary]];
    }
}

- (void)didSelectOpenDeck:(NSString *)cardNum withCardSuit:(NSString *)cardSuit
{
    if (self.isPlayerModelSlided)
    {
#if PLAYER_MODEL_GAME_CARD_ANIMATION
        
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, [cardSuit uppercaseString]];
        // animation goes here
        NSArray *nibContents;
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName owner:nil  options:nil];
        
        TAJCardView *newCard = nibContents[index];
        // float width, height;
        
        CGRect startRect=[self.playerModelView.myDeckHolder convertRect:self.playTable.openDeckHolder.frame fromView:self.playTable];
        
        newCard.frame=startRect;
        
        [newCard showCard:cardId isMomentary:YES];
        //UIView *view=[self.myDeckCardsArray lastObject];
        UIView *view=[self.playerModelView getLastCard];
    //newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.8f, 0.8f);
        if ([TAJUtilities isIPhone])
        {
            newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.6f, 0.6f);
        }
        else
        {
            newCard.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.8f, 0.8f);
        }
        
        CGRect finalRect;
        if ([TAJUtilities isIPhone])
        {
            finalRect = CGRectMake(view.frame.origin.x+18, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            
        }
        else
        {
            finalRect= CGRectMake(view.frame.origin.x+30, view.frame.origin.y, view.frame.size.width, view.frame.size.height);
            
        }
        [self.playerModelView.myDeckHolder addSubview:newCard];
        [UIView animateWithDuration:0.10
                              delay:0.0
                            options: UIViewAnimationOptionCurveEaseOut
                         animations:
         ^{
             newCard.frame=finalRect;
             
         }
                         completion:^(BOOL finished)
         {
             [newCard removeFromSuperview];
         }];
#endif
    }
    //Sound
    [[TAJSoundHandler sharedManager] playDrawCard_DiscardSound];
    
    [[TAJGameEngine sharedGameEngine] cardPickWithFace:cardNum
                                             withStack:kFace_Up_Stack_Key
                                              withSuit:cardSuit
                                               tableID:self.currentTableId
                                                userID:self.thisPlayerModel.playerID
                                              nickName:self.thisPlayerModel.playerName];
    
    self.pickedCard = [NSMutableDictionary dictionaryWithObjectsAndKeys:cardNum, kTaj_Card_Face_Key,
                       [cardSuit uppercaseString], kTaj_Card_Suit_Key, nil];
    self.isCardPicked = YES;
    
    if (self.pickedCard)
    {
        [self.myCardsDealArray addObject:self.pickedCard];
        
#if CARD_GROUPING_SLOTS
        [self.cardSlotManager insertPickedCardToSlotArray:self.pickedCard];
        [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
        
#if PLAYER_MODEL_GAME_CARD
        // Update player model card
        [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
        
#else
        [self.playTable updateMyDeckWithCards:self.myCardsDealArray];
#endif
    }
    [self updateDropButton];
    
    // set up message for open deck live feed
    NSMutableDictionary *openDeck = [NSMutableDictionary dictionary];
    [openDeck setObject:self.thisPlayerModel.playerName forKey:kTaj_nickname];
    [openDeck setObject:cardNum forKey:kTaj_Card_Face_Key];
    [openDeck setObject:cardSuit forKey:kTaj_Card_Suit_Key];
    
    [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerPickOpenDeckMessage:openDeck]];
    
    // show star image in discarded card
    if (self.isCardDiscarded && self.discardCardHistoryController && self.starImageForCardInfo)
    {
        [self.discardCardHistoryController showStarImageForCard:self.starImageForCardInfo];
    }
}

- (void)unSelectChair
{
    for (UIView* view in self.chairsArray)
    {
        NSArray *subviews = view.subviews;
        if (subviews && subviews.count > 0)
        {
            TAJChairView *eachChair = subviews[0];
            if(!eachChair.playerModel)
            {
                [eachChair enableInteraction];
            }
        }
    }
}

- (TAJChairView *)getEmptyChair
{
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                
                if (![result isSeatOccupied])
                {
                    return result;
                }
            }
        }
    }
    return nil;
}


- (void)didSelectChair:(TAJChairView *)chair
{
    if (self.isScheduleGameCame)
    {
        if (self.startTimerDuration <= 5.0 )
        {
            return;
        }
    }
    if (self.isJokerTypeGame)
    {
        // this condition only for joker and no joker table type
        // to create a pop up to enter buy in amount
        
        NSDictionary* dictionary= self.playTableDictionary[kTaj_Table_details];
        
        float minBet = [dictionary[TAJ_MIN_BUYIN] floatValue];
        
        float currentBalance = [[[[TAJGameEngine sharedGameEngine] usermodel]funchips] floatValue];
        
        if (currentBalance < minBet)
        {
            
            if (!self.jokerJoinAlertView)
            {
#if NEW_INFO_VIEW_IMPLEMENTATION
                self.jokerJoinAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_JOKER_CREATE_TAG popupButtonType:eOK_Type message:ALERT_JOKER_SIT_INSUFFICIENT];
#else
                self.jokerJoinAlertView = [[UIAlertView alloc]initWithTitle:ALERT_EMPTY_TITLE
                                                                    message:ALERT_REBUYIN_REQUEST_TITLE
                                                                   delegate:self
                                                          cancelButtonTitle:OK_STRING
                                                          otherButtonTitles:CANCEL_STRING, nil];
                
                self.jokerJoinAlertView.tag = ALERT_REBUYIN_JOKER_TAG;
#endif
                [self.jokerJoinAlertView show];
                
            }
            [chair showAllUI];
            
            return;
            
        }
        
        if (!self.jokerPopTableView)
        {
            NSLog(@"CHECK JOCKER 11561");
            [self createJokerPopTableView];
            if (!self.jokerChairView)
            {
                self.jokerChairView = chair;
            }
            
            self.leaveButton.enabled = NO;
        }
    }
    else
    {
        // Sit here tapped
        int placeID = chair.chairId;
        
        if (self.maxPlayers == TWO_PLAYER && placeID == 2)
        {
            placeID = 4;
        }
        
        NSLog(@"sit here placeID %d",placeID);
        [[TAJGameEngine sharedGameEngine] joinSeat:placeID tableID:self.currentTableId buyInAmount:nil middleJoin:self.isPoolDealMiddleJoin];
        
        [self disableChairs];
        
        self.isSeatRequestPending = YES;
        
    }
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    [engine iAmBackWithUserID:[[engine usermodel] userId]];
    
}

- (void)disableChairs
{
    for (UIView* view in self.chairsArray)
    {
        NSArray *subviews = view.subviews;
        if (subviews && subviews.count > 0)
        {
            TAJChairView *eachChair = subviews[0];
            [eachChair disableInteraction];
        }
    }
}

- (void)removeChairViews
{
    for (UIView* view in self.chairsArray)
    {
        NSArray *subviews = view.subviews;
        if (subviews && subviews.count > 0)
        {
            TAJChairView *eachChair = subviews[0];
            [eachChair removeChairAvatars];
        }
    }
}

- (void)removeChairViewFromHolder
{
    for (UIView* view in self.chairsArray)
    {
        NSArray *subviews = view.subviews;
        if (subviews && subviews.count > 0)
        {
            TAJChairView *eachChair = subviews[0];
            [eachChair removeFromSuperview];
            eachChair = nil;
        }
    }
}

#pragma mark - Remove card info from controller -

- (void)removeCardAtindex:(NSUInteger)index fromDeck:(NSMutableArray *)array
{
    [array removeObjectAtIndex:index];
}

- (void)removeCardWithCardNumber:(NSString *)inCardNo withSuit:(NSString *)inSuit
{
    int indexOfObjectToRemove = 9999;
    for (int i = 0; i < self.myCardsDealArray.count; i++)
    {
        NSMutableDictionary *cardInfo = self.myCardsDealArray[i];
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardSuit = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
        
        if ([cardNum isEqualToString:inCardNo] &&
            [cardSuit isEqualToString:[inSuit lowercaseString]])
        {
            indexOfObjectToRemove = i;
            break;
        }
    }
    if (indexOfObjectToRemove != 9999)
    {
        [self.myCardsDealArray removeObjectAtIndex:indexOfObjectToRemove];
    }
}

- (BOOL)isSlotExistWithCardNumber:(NSString *)inCardNo withSuit:(NSString *)inSuit
{
    BOOL isCardExist = NO;
    int indexOfObjectToRemove = 9999;
    for (int i = 0; i < self.mySendDealCardsArray.count; i++)
    {
        NSMutableDictionary *cardInfo = self.mySendDealCardsArray[i];
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardSuit = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
        
        if ([cardNum isEqualToString:inCardNo] &&
            [cardSuit isEqualToString:[inSuit lowercaseString]])
        {
            isCardExist = YES;
            indexOfObjectToRemove = i;
            [self.mySendDealCardsArray removeObjectAtIndex:i];
            break;
        }
    }
    return isCardExist;
}

- (void)removeSlotCardInfoCardno:(NSString *)inCardNo withSuit:(NSString *)inSuit
{
    int indexOfObjectToRemove = 9999;
    for (int i = 0; i < self.slotCardInfoArray.count; i++)
    {
        if ([self.slotCardInfoArray[i] isKindOfClass:[NSDictionary class]])
        {
            NSMutableDictionary *cardInfo = self.slotCardInfoArray[i];
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardSuit = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
            
            if ([cardNum isEqualToString:inCardNo] &&
                [cardSuit isEqualToString:[inSuit lowercaseString]])
            {
                indexOfObjectToRemove = i;
                //                DLog(@"slot after array  index  %d",indexOfObjectToRemove);
                
                break;
            }
        }
    }
    if (indexOfObjectToRemove != 9999)
    {
        [self.slotCardInfoArray removeObjectAtIndex:indexOfObjectToRemove];
    }
    
}

#pragma mark - Helper Methods -

- (void)updatePlayerPositions:(NSArray *)playersArray
{
    for(NSInteger chairIndex = 0; chairIndex<self.chairsArray.count; chairIndex++)
    {
        TAJChairView *chair = [self chairFromView:self.chairsArray[chairIndex]];
        if([chair isSeatOccupied])
        {
            [chair setIsOccupied:NO];
        }
    }
    
    for (int i = 0; i < playersArray.count; i++)
    {
        NSMutableDictionary *playerDict = playersArray[i];
        NSString *userID = playerDict[kTaj_userid_Key];
        UIView *chairHolder = [self chairHolderFromUserId:[userID integerValue]];
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            TAJChairView *chairView = chairHolder.subviews[0];
            [chairView setIsOccupied:YES];
            
            // Actually we dont want to check rearrange data with position because the order the server send seat position according to index only
            if (self.maxPlayers == SIX_PLAYER && [playersArray count] == 2 && i == 1)
            {
                // in 6 player table if 2 player only exist, the second index of player position is 4
                chairView.playerModel.placeID = [NSString stringWithFormat:@"%d",4];
            }
            else
            {
                // this is for 2 player table as well as 6 players(players >= 3) table
                chairView.playerModel.placeID = [NSString stringWithFormat:@"%d",i+1];
            }
        }
        
        // set player model for device player
        if ([self isThisDevicePlayer:chairHolder])
        {
            self.thisDevicePlayer = chairHolder;
            
            // Set player model for device player
            TAJChairView *chair = [self chairFromView:chairHolder];
            self.thisPlayerModel = chair.playerModel;
        }
    }
}

- (TAJChairView *)chairAtPlaceIndex:(NSUInteger)placeIndex
{
    TAJChairView *result = nil;
    if (self.chairsArray && placeIndex < self.chairsArray.count)
    {
        if (self.chairsArray[placeIndex])
        {
            UIView *view = self.chairsArray[placeIndex];
            
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                result = (TAJChairView *)subviews[0];
            }
        }
    }
    return result;
}

- (TAJPlayerModel *)playerModelAtPlaceIndex:(NSUInteger)placeIndex
{
    TAJChairView *view = [self chairAtPlaceIndex:placeIndex];
    if (view)
    {
        return view.playerModel;
    }
    return nil;
}

- (BOOL)isThisDevicePlayer:(UIView *)view
{
    BOOL returnValue = NO;
    
    if (view.subviews && view.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [view subviews];
        TAJChairView *result = (TAJChairView *)subviews[0];
        
        if (result.playerModel)
        {
            int loggedInUserID = [[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue];
            
            if ([result.playerModel.playerID integerValue] == loggedInUserID)
            {
                result.playerModel.isThisDevicePlayer = YES;
                returnValue = YES;
            }
        }
    }
    return returnValue;
}

// Parameter should be one of the chair holder view
- (TAJChairView *)chairFromView:(UIView *)view
{
    TAJChairView *result = nil;
    if (view.subviews && view.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [view subviews];
        result = (TAJChairView *)subviews[0];
    }
    return result;
}

- (UIView *)chairHolderFromUserId:(NSInteger)userId
{
    UIView *chairHolder = nil;
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                
                if ([result.playerModel.playerID integerValue] == userId)
                {
                    chairHolder = view;
                    break;
                }
            }
        }
    }
    return chairHolder;
}

- (UIView *)chairHolderFromUserIdForQuit:(NSInteger)userId
{
    UIView *chairHolder = nil;
    //    DLog(@"chairholderfromuserid %d",userId);
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (int index = self.chairsArray.count;index > 0;index--)
        {
            UIView *view =self.chairsArray[index-1];
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                //                DLog(@"chairholderfromuserid result %@",result.playerModel.playerID);
                
                if ([result.playerModel.playerID integerValue] == userId)
                {
                    chairHolder = view;
                    break;
                }
            }
        }
    }
    return chairHolder;
}

- (UIView *)chairHolderAtPlaceId:(NSInteger)placeID
{
    if (self.chairsArray)
    {
        if (self.maxPlayers == TWO_PLAYER && placeID == 4)
        {
            return self.chairsArray[2 - 1];
        }
        else
        {
            return self.chairsArray[placeID -1];
        }
    }
    return nil;
}

- (NSUInteger)placeIDForDictionary:(NSDictionary *)dictionary
{
    int placeID = [dictionary[kTaj_Seat_key] intValue];
    
    if (self.maxPlayers == TWO_PLAYER && placeID == 4)
    {
        return 2;
    }
    else
    {
        return placeID;
    }
}

- (NSUInteger)noOfChairOccupied
{
    NSUInteger seatOccuiped = 0;
    
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                
                if ([result isSeatOccupied])
                {
                    seatOccuiped++;
                }
            }
        }
    }
    return seatOccuiped;
}

- (TAJPlayerModel *)playerModelForUserID:(NSUInteger)userID
{
    TAJPlayerModel *playerModel = nil;
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                
                if ([result.playerModel.playerID integerValue] == userID)
                {
                    playerModel = result.playerModel;
                    break;
                }
            }
        }
    }
    return playerModel;
}

- (NSUInteger)getPlaceIdFromChairUserID:(NSInteger)userID
{
    NSUInteger placeID = 0;
    
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                
                if ([result.playerModel.playerID integerValue] == userID)
                {
                    placeID = [result.playerModel.placeID integerValue];
                    break;
                }
            }
        }
    }
    return placeID;
}

- (BOOL)isUserExistInArray:(NSArray *)array
{
    BOOL isPlayerPlayingGame = NO;
    if (array)
    {
        for (NSUInteger i = 0; i < array.count; i++)
        {
            NSDictionary *playerData = array[i];
            if ([playerData[kTaj_userid_Key] integerValue] == [[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue])
            {
                isPlayerPlayingGame = YES;
                break;
            }
        }
    }
    return YES;
}

- (void)testingSeatNumber
{
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                
                if (result)
                {
                    //                    DLog(@"chair place index %@",result.playerModel.placeID);
                }
            }
        }
    }
}

#pragma mark - Dealer image show -

- (void)dealerImageForPlayer:(NSDictionary *)dictionary
{
    //NSLog(@"PLAYER INFO : %@",dictionary);
    
    [self resetPlayerDealerArray];
    /*IMP: this function only for placing card bg image for players,
     at this function dont update(but you can access) any chair player model data.
     Only for placing card images
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    NSUInteger userID = [dictionary[kTaj_Dealer_id] integerValue];
    NSUInteger playerPosition = [self getPlaceIdFromChairUserID:userID];
    
    if (playerPosition <= 6 && playerPosition > 0 )
    {
        if (self.dealerPlayersArray.count > 5)
        {
            UIImageView *dealerImageView;
            
            if (self.maxPlayers == TWO_PLAYER && playerPosition == 2)
            {
                // only for top of middle player in 2 player table
                dealerImageView = self.dealerPlayersArray[3];
            }
            else
            {
                // for all player
                dealerImageView = self.dealerPlayersArray[playerPosition - 1];
            }
            
            dealerImageView.hidden = NO;
        }
    }
}

#pragma mark - Show player cards bg -
//- (void)integerFromNumberObject: (NSNumber *)number
//{
//    int primaryKey = [number intValue];
//    DLog("%i", primaryKey);
//}
- (void)placeCardDropImage:(NSNumber *)position
{
    NSUInteger playerPosition = [position unsignedIntegerValue];
    if (playerPosition <= 6 && playerPosition > 0 )
    {
        if (self.cardBackAnimationImageViewArray.count > 5)
        {
            /*IMP: this function only for placing card bg image for players,
             at this function dont update(but you can access) any chair player model data.
             Only for placing card images
             DONT UPDATE CHAIR PLAYER MODEL*/
            
            UIImageView *playerCardImageViewOld,*playerCardImageViewNew;
            playerCardImageViewOld = self.playerCardsBgImage[playerPosition - 1];
            playerCardImageViewNew.image = nil;
            playerCardImageViewOld.hidden = YES;
            playerCardImageViewNew = self.cardBackAnimationImageViewArray[playerPosition - 1];
            // playerCardImageView.hidden = NO;
            if ([TAJUtilities isIPhone])
            {
                playerCardImageViewNew.image = self.dropImageArray[playerPosition - 1];// [UIImage imageNamed:@"card_back-568@2x~iphone.png"];
            }
            else
            {
                playerCardImageViewNew.image = self.dropImageArray[playerPosition - 1];
            }
            playerCardImageViewNew.hidden = NO;
        }
    }
}

- (void)showCardBgImageForPlayer:(NSUInteger)playerPosition
{
    /*IMP: this function only for placing card bg image for players,
     at this function dont update(but you can access) any chair player model data.
     Only for placing card images
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    if (playerPosition <= 6 && playerPosition > 0 )
    {
        UIImageView *imageView;
        
        if (self.maxPlayers == TWO_PLAYER && playerPosition == 2)
        {
            // only for top of middle player in 2 player table
            imageView = self.playerCardsBgImage[3];
            imageView.hidden = NO;
        }
        else
        {
            // for all player
            imageView = self.playerCardsBgImage[playerPosition - 1];
            imageView.hidden = NO;
        }
        
        if (playerPosition == 1)
        {
            // how as sitted down(i.e. devicePlayer), dont show card bg image because we are showing 13 card on mydeck-cards holder
            if (!self.isSpectator)
            {
                imageView.hidden = YES;
            }
        }
    }
}

- (void)placeCardBgImageForPlayers
{
    /*IMP: this function only for placing card bg image for players,
     at this function dont update(but you can access) any chair player model data.
     Only for placing card images
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    if (self.chairsArray && self.chairsArray.count > 0)
    {
        for (UIView *view in self.chairsArray)
        {
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                TAJChairView *result = (TAJChairView *)subviews[0];
                if (result.playerModel)
                {
                    [self showCardBgImageForPlayer:[result.playerModel.placeID integerValue]];
                }
            }
        }
    }
}

#pragma mark - Auto play functions -

//Yogish

- (void)createViewForAutoPlay
{
    for(NSInteger index = 0; index < self.playersAutoPlayViewArray.count; index++)
    {
        UIView *view = self.playersAutoPlayViewArray[index];
        TAJAutoPlayView *autoPlayView = nil;
        if(view.subviews && view.subviews.count > 0)
        {
            NSArray *subviews = [view subviews];
            autoPlayView = (TAJAutoPlayView *)subviews[0];
        }
        else
        {
            NSArray *nibContents;
            // Autoplay view not available
            int nibIndex = 0;
            nibContents = [[NSBundle mainBundle] loadNibNamed:TAJAutoPlayViewNibName
                                                        owner:nil
                                                      options:nil];
            autoPlayView = nibContents[nibIndex];
            autoPlayView.autoPlayDelegate = self;
            autoPlayView.tag = index + 1;
            [view addSubview:autoPlayView];
        }
    }
}

- (TAJAutoPlayView *)autoPlayViewAtPlaceIndex:(NSUInteger)placeIndex
{
    TAJAutoPlayView *result = nil;
    if (self.playersAutoPlayViewArray && self.playersAutoPlayViewArray.count > 0)
    {
        if (placeIndex > 0 && placeIndex <= 6)
        {
            if (self.maxPlayers == TWO_PLAYER && placeIndex == 2)
            {
                placeIndex = 4;
            }
            
            UIView *view = self.playersAutoPlayViewArray[placeIndex - 1];
            
            if (view.subviews && view.subviews.count > 0)
            {
                // Already added
                NSArray *subviews = [view subviews];
                result = (TAJAutoPlayView*)subviews[0];
            }
        }
    }
    return result;
}

- (TAJAutoPlayView *)autoPlayViewFromViewHolder:(UIView *)view
{
    TAJAutoPlayView *result = nil;
    if (view.subviews && view.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [view subviews];
        result = (TAJAutoPlayView *)subviews[0];
    }
    return result;
}

- (UIView *)autoPlayViewHolderAtPlaceId:(NSInteger)placeID
{
    if (self.playersAutoPlayViewArray && self.playersAutoPlayViewArray.count > 0)
    {
        if (placeID > 0 && placeID <= 6)
        {
            if (self.maxPlayers == TWO_PLAYER && placeID == 2)
            {
                placeID = 4;
            }
            return self.playersAutoPlayViewArray[placeID - 1];
        }
    }
    return nil;
}

//Pradeep

- (void)prepareAutoPlayViewForTurnUpdate:(NSDictionary *)dictionary
{
    /*IMP: this function only for showing auto play label,
     at this function dont update(but you can access) any chair player model data.
     Only for showing auto play label
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    //NOTE: this function call at TURN_UPDATE Event and (playersStatusForSpectator:)
    
    NSString *autoPlayStatus = dictionary[kTaj_autoPlay_status];
    if (dictionary[kTaj_autoPlay_status]/*check first this key is exist or not*/ && [autoPlayStatus isEqualToString:@"True"] && self.canShowAutoPlayView)
    {
        NSString *userId = dictionary[kTaj_userid_Key];
        NSUInteger playerPlaceID = [self getPlaceIdFromChairUserID:[userId integerValue]];
        
        [self turnShowAutoPlayHolderView:playerPlaceID withAutoPlayCards:dictionary[kTaj_autoPlay_cardscount] withTotalCount:dictionary[kTaj_autoPlay_totalCount]];
        
        // set up message for live feed
        
        // sleep(2.0f);
        //[self performSelectorOnMainThread:@selector(removePlayTableMessage)
        //     withObject:nil waitUntilDone:YES];
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView performSelector:@selector(showInfoLabelMessagePlayerMode) withObject:nil afterDelay:2.1f];
        }
        
        [self.playTable performSelector:@selector(showInfoLabelMessage) withObject:nil afterDelay:2.1f];
        
        [self setUpHistoryForLiveFeed:[TAJLiveFeedManager playerDisconnectedMessage:dictionary]];
        
        // check is this device player disconnected
        if ([self.thisPlayerModel.playerID integerValue] == [userId integerValue])
        {
            if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(devicePlayerInAutoGamePlay:WithTotalAutoPlayCount:)])
            {
                [self.gameTableDelegate devicePlayerInAutoGamePlay:dictionary[kTaj_autoPlay_cardscount] WithTotalAutoPlayCount:dictionary[kTaj_autoPlay_totalCount]];
            }
        }
        else
        {
            if (self.isSendDealCame)
            {
                if(self.isPlayerModelSlided)
                {
                    [self.playerModelView removeExtraMessagePlayerMode];
                }
                [self.playTable removeExtraMessage];
                NSString * message = [NSString stringWithFormat:LIVE_FEED_PLAYER_DISCONNECTED,dictionary[kTaj_nickname]];
                self.previousMessage = self.playTable.currentMessage;
                [self.playTable hideInfoLabelMessage];
                if(self.isPlayerModelSlided)
                {
                    [self.playerModelView showExtraMessagePlayerMode:message];
                    // [self.playerModelView performSelector:@selector(showInfoLabelMessagePlayerMode) withObject:nil afterDelay:2.0f];
                }
                [self.playTable showExtraMessage:message];
            }
            
        }
    }
}

- (void)turnShowAutoPlayHolderView:(NSUInteger)playerPosition withAutoPlayCards:(NSString *)autoPlayCount withTotalCount:(NSString *)totalCount
{
    /*IMP: this function only for showing auto play label,
     at this function dont update(but you can access) any chair player model data.
     Only for showing auto play label
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    if (self.playersAutoPlayViewArray.count > 0)
    {
        UIView *autoPlayHolderView;
        TAJAutoPlayView *autoPlayView;
        
        autoPlayHolderView = [self autoPlayViewHolderAtPlaceId:playerPosition];
        autoPlayView = [self autoPlayViewAtPlaceIndex:playerPosition];
        
        autoPlayHolderView.hidden = NO;
        
        [autoPlayView updateAutoplayCount:[autoPlayCount integerValue] andTotalCount:[totalCount integerValue]];
        
        if (playerPosition == 1)
        {
            // how as sitted down(i.e. devicePlayer), dont show auto play label because we are showing i'm back for that user who lost internet connection
            if (!self.isSpectator)
            {
                autoPlayView.hidden = YES;
            }
        }
    }
}

- (void)isPlayerCameToAutoPlayMode:(NSDictionary *)dictionary
{
    /*IMP: this function only for showing auto play label,
     at this function dont update(but you can access) any chair player model data.
     Only for showing auto play label
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    //NOTE: this function call at AUTO_PLAY_STATUS (this / other player) , CARD_PICK and CARD_DISCARD Event
    if (dictionary && self.canShowAutoPlayView)
    {
        BOOL wantToPlaceAutoPlayView = NO;
        NSString *isPlayerInAutoPlayMode;
        
        if (dictionary[kTaj_autoplaystatus])
        {
            // for AUTO_PLAY_STATUS event having TAJ_status Key,
            // but dont have TAJ_autoplay(CARD_PICK, CARD_DISCARD Events) Key
            
            isPlayerInAutoPlayMode = dictionary[kTaj_autoplaystatus];
        }
        else if (dictionary[kTaj_autoPlay_status])
        {
            // for CARD_PICK and CARD_DISCARD event having TAJ_autoplay Key,
            // but dont have TAJ_status(AUTO_PLAY_STATUS Event) Key
            
            isPlayerInAutoPlayMode = dictionary[kTaj_autoPlay_status];
        }
        
        if ([isPlayerInAutoPlayMode isEqualToString:@"False"])
        {
            wantToPlaceAutoPlayView = NO;
        }
        else if ([isPlayerInAutoPlayMode isEqualToString:@"True"])
        {
            wantToPlaceAutoPlayView = YES;
        }
        
        NSString *userId = dictionary[kTaj_userid_Key];
        NSUInteger playerPlaceID = [self getPlaceIdFromChairUserID:[userId integerValue]];
        
        [self autoPlayPlayerViewAtPosition:playerPlaceID wantToShow:wantToPlaceAutoPlayView];
    }
}

- (void)autoPlayPlayerViewAtPosition:(NSUInteger)playerPosition wantToShow:(BOOL)isShowing
{
    /*IMP: this function only for showing auto play label,
     at this function dont update(but you can access) any chair player model data.
     Only for showing auto play label
     DONT UPDATE CHAIR PLAYER MODEL*/
    
    if (self.playersAutoPlayViewArray.count > 0)
    {
        UIView *autoPlayViewHolder;
        
        // for all player
        if (!isShowing)
        {
            // reset to 0/5 if hidding autoplayview
            [self resetAutoPlayCountForView:playerPosition];
        }
        
        autoPlayViewHolder = [self autoPlayViewHolderAtPlaceId:playerPosition];
        
        autoPlayViewHolder.hidden = !isShowing;
    }
}

- (void)removeAutoPlayView:(NSInteger)index
{
    // future can be implemented.
    if(index > 0)
    {
        [self resetAutoPlayCountForView:(index - 1)];
        
        UIView *holderView = self.playersAutoPlayViewArray[index - 1];
        holderView.hidden = YES;
    }
}

- (void)removeAllAutoPlayView
{
    for (int index=1; index <= self.playersAutoPlayViewArray.count; index++)
    {
        if(index > 0)
        {
            [self resetAutoPlayCountForView:(index - 1)];
            
            UIView *holderView = self.playersAutoPlayViewArray[index - 1];
            holderView.hidden = YES;
        }
        
    }
}

- (void)resetAutoPlayCountForView:(NSUInteger)playerPosition
{
    // reset to 0/5 if hidding autoplayview
    TAJAutoPlayView* autoPlayView = [self autoPlayViewAtPlaceIndex:playerPosition];
    [autoPlayView resetCountForAutoPlay];
}

#pragma mark - Sort Deck Card Array -

- (IBAction)sortDeckCard:(UIButton *)sender
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        return;
    }
    [self.playTable removeAllButtons];
    [self.playTable removeMeldGroupAllButton];
    
#if PLAYER_MODEL_GAME_CARD
    // remove all player model buttons
    [self.playerModelView removeAllModelButtons];
#endif
    
#if CARD_GROUPING_SLOTS
    
    self.slotCardInfoArray = [NSMutableArray arrayWithArray:[self sortMyDeckWithCards:self.myCardsDealArray]];
    
    [self.cardSlotManager initializeSlotArray:self.slotCardInfoArray];
    
    // update table cards
    [self.playTable wantToShowMeldGroupButton:YES];
    [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
    if (!self.isPlayerModelSlided)
    {
        [self.playTable updateMeldGroupButton];
    }
    
#if PLAYER_MODEL_GAME_CARD
    // Update player model cards
    [self.playerModelView canShowMeldGroupForPlayerModel:YES];
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
    [self.playerModelView updateMeldGroupModelButton];
#endif
    
#else
    self.myCardsDealArray = [NSMutableArray arrayWithArray:[self sortMyDeckWithCards:self.myCardsDealArray]];
    
    [self.playTable updateMyDeckWithCards:self.myCardsDealArray];
    
    if (!self.isPlayerModelSlided)
    {
        [self.playTable updateMeldGroupButton];
    }
#endif
    
}

- (NSMutableArray *)sortMyDeckWithCards:(NSMutableArray *)array
{
    //we need same case to check array data to be in-casesensetive
    for (NSMutableDictionary *cardInfo in array)
    {
        cardInfo[kTaj_Card_Suit_Key] = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
    }
    
    NSPredicate *predicateDiamond = [NSPredicate predicateWithFormat:kTaj_predicate_suit,kTaj_suit_diamond];
    NSPredicate *predicateClues = [NSPredicate predicateWithFormat:kTaj_predicate_suit,kTaj_suit_clube];
    NSPredicate *predicateSpades = [NSPredicate predicateWithFormat:kTaj_predicate_suit,kTaj_suit_spade];
    NSPredicate *predicateHeart = [NSPredicate predicateWithFormat:kTaj_predicate_suit,kTaj_suit_heart];
    NSPredicate *predicateJoker = [NSPredicate predicateWithFormat:kTaj_predicate_suit,kTaj_suit_joker];
    
    NSMutableArray *diamondArray = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicateDiamond]];
    NSMutableArray *cluesArray = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicateClues]];
    NSMutableArray *spadeArray = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicateSpades]];
    NSMutableArray *heartArray = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicateHeart]];
    NSMutableArray *jokerArray = [NSMutableArray arrayWithArray:[array filteredArrayUsingPredicate:predicateJoker]];
    
    diamondArray = [self sortFaceCard:diamondArray];
    cluesArray = [self sortFaceCard:cluesArray];
    spadeArray = [self sortFaceCard:spadeArray];
    heartArray = [self sortFaceCard:heartArray];
    jokerArray = [self sortFaceCard:jokerArray];
    
    NSMutableArray *finalArray = [NSMutableArray array];
#if CARD_GROUPING_SLOTS
    [finalArray addObject:[NSNull null]];
    [finalArray addObject:[NSNull null]];
#endif
    for (NSMutableDictionary *diamondDict in diamondArray)
    {
        [finalArray addObject:diamondDict];
    }
#if CARD_GROUPING_SLOTS
    [finalArray addObject:[NSNull null]];
    //    [finalArray addObject:[NSNull null]];
    
#endif
    for (NSMutableDictionary *cluesDict in cluesArray)
    {
        [finalArray addObject:cluesDict];
    }
    
    if (cluesArray.count > 0)
    {
#if CARD_GROUPING_SLOTS
        [finalArray addObject:[NSNull null]];
        //        [finalArray addObject:[NSNull null]];
#endif
    }
    
    for (NSMutableDictionary *spadeDict in spadeArray)
    {
        [finalArray addObject:spadeDict];
    }
    
    if (spadeArray.count > 0)
    {
#if CARD_GROUPING_SLOTS
        [finalArray addObject:[NSNull null]];
        //        [finalArray addObject:[NSNull null]];
#endif
    }
    
    for (NSMutableDictionary *heartDict in heartArray)
    {
        [finalArray addObject:heartDict];
    }
    
    if (heartArray.count > 0)
    {
#if CARD_GROUPING_SLOTS
        [finalArray addObject:[NSNull null]];
        //        [finalArray addObject:[NSNull null]];
#endif
    }
    
    for (NSMutableDictionary *jokerDict in jokerArray)
    {
        [finalArray addObject:jokerDict];
    }
#if CARD_GROUPING_SLOTS
    [finalArray addObject:[NSNull null]];
    //    [finalArray addObject:[NSNull null]];
#endif
    return finalArray;
}

- (NSMutableArray *)sortFaceCard:(NSArray *)array
{
    NSArray *sortedArray = [array sortedArrayUsingComparator: ^(id obj1, id obj2) {
        
        if ([obj1[kTaj_Card_Face_Key] integerValue] > [obj2[kTaj_Card_Face_Key] integerValue])
        {
            return NSOrderedDescending;
        }
        
        else if ([obj1[kTaj_Card_Face_Key] integerValue] < [obj2[kTaj_Card_Face_Key] integerValue])
        {
            return NSOrderedAscending;
        }
        return NSOrderedSame;
    }];
    return [sortedArray mutableCopy];
}

#pragma mark - Player Turn Time View -

- (void)setUpPlayerTurnTime:(int)playerTime forUserID:(NSInteger)userID extraTime:(NSInteger)extraTime {
    
    //[self.playTable removeImagesFromMyDeckHolder];
    if(_firstPlayerAnimationImages.count > 0)
    {
        [self removeFirstPlayerImages:_firstPlayerAnimationImages];
    }
    [self removeScoreSheetView];
    [self.playerTurnView stopRunningTimer];
    if (!self.playerTurnView)
    {
        NSInteger placeId = [self getPlaceIdFromChairUserID:userID];
        if (self.maxPlayers == TWO_PLAYER && placeId == 2 )
        {
            placeId = 4;
        }
        
        NSArray *nibContents;
        //player turn view not available
        int index = 0;
        
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJPlayerTurnViewNibName  owner:nil options:nil];
        self.playerTurnView = nibContents[index];
        
        if(self.meldViewTable || self.jokerPopTableView || self.reportBugView)
        {
            if(self.jokerPopTableView)
            {
                if(placeId == 1 || placeId == 2 || placeId == 3 || placeId == 4 || placeId == 5 || placeId == 6 )
                {
                    [self.view insertSubview:self.playerTurnView belowSubview:self.jokerPopTableView];
                }
            }
            if(self.meldViewTable)
            {
                if(placeId == 3 || placeId == 4 || placeId == 5)
                {
                    [self.view insertSubview:self.playerTurnView belowSubview:self.meldViewTable];
                    //[self.view sendSubviewToBack:self.playerTurnView];
                }
                else
                {
                    [self.view addSubview:self.playerTurnView];
                    [self.view insertSubview:self.playerTurnView belowSubview:self.extraTimeButton];
                    [self.view insertSubview:self.extraTimeBg belowSubview:self.playerTurnView];
                    
                }
            }
            if(self.reportBugView)
            {
                if(placeId == 3 || placeId == 4)
                {
                    [self.view insertSubview:self.playerTurnView belowSubview:self.reportBugView];
                    //[self.view sendSubviewToBack:self.playerTurnView];
                }
                else
                {
                    [self.view addSubview:self.playerTurnView];
                    [self.view insertSubview:self.playerTurnView belowSubview:self.extraTimeButton];
                    [self.view insertSubview:self.extraTimeBg belowSubview:self.playerTurnView];
                }
            }
        }
        else
        {
            [self.view addSubview:self.playerTurnView];
            [self.view insertSubview:self.playerTurnView belowSubview:self.extraTimeButton];
            [self.view insertSubview:self.extraTimeBg belowSubview:self.playerTurnView];
            
        }
        //[self removeViewForTurnTime];
        if(self.settingMenuListView)
        {
            [self removeSettingMenuListView];
        }
        
        if(self.isDisConnectReconnect)
        {
            double loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
            DLog(@"loadTime %f",loadTime);
            int newPlayerTime;
            newPlayerTime = playerTime - round(loadTime);
            
            if (playerTime > loadTime)
            {
                [self.playerTurnView startPlayerTimer:newPlayerTime turn:self.isMyTurn placeId:placeId chunksTimer:chunksExtraTimer ischunks:NO];
                chunksExtraTimer = 0;
                //chunks implementation
                [self playerChunksSetUpWithPlayeTurn:placeId];
                [self playerExtraTimerSetUpWithPlayerTurn:placeId extraTime:extraTime];
                
                if ([APP_DELEGATE.tableType isEqualToString:@"BEST_OF_2"] || [APP_DELEGATE.tableType isEqualToString:@"BEST_OF_3"]) {
                    //self.viewAutoDrop.hidden = YES;
                    [self hideAutoDrop];
                }
                
                else {
                    if (self.isMyTurn){
                        if (self.isAutoDrop) {
                            self.viewAutoDrop.hidden = NO;
                            [self performSelector:@selector(autoDrop) withObject:self afterDelay:1.0 ];
                        }
                        else {
                            //self.viewAutoDrop.hidden = YES;
                            [self hideAutoDrop];
                        }
                    }
                    else {
                        self.viewAutoDrop.hidden = NO;
                        [self unHideAutoDrop];
                    }
                }
                
            }
            else
            {
                [self.playerTurnView startPlayerTimer:playerTime turn:self.isMyTurn placeId:placeId chunksTimer:chunksExtraTimer ischunks:NO];
                chunksExtraTimer = 0;
                //chunks implementation
                [self playerChunksSetUpWithPlayeTurn:placeId];
                [self playerExtraTimerSetUpWithPlayerTurn:placeId extraTime:extraTime];
                
                if ([APP_DELEGATE.tableType isEqualToString:@"BEST_OF_2"] || [APP_DELEGATE.tableType isEqualToString:@"BEST_OF_3"]) {
                    //self.viewAutoDrop.hidden = YES;
                    [self hideAutoDrop];
                }
                
                else {
                    if (self.isMyTurn){
                        if (self.isAutoDrop) {
                            self.viewAutoDrop.hidden = NO;
                            [self performSelector:@selector(autoDrop) withObject:self afterDelay:1.0 ];
                            
                        }
                        else {
                            //self.viewAutoDrop.hidden = YES;
                            [self hideAutoDrop];
                        }
                    }
                    else {
                        //self.viewAutoDrop.hidden = NO;
                        [self unHideAutoDrop];
                    }
                }
                
            }
            self.isDisConnectReconnect = NO;
        }
        else
        {
            [self.playerTurnView startPlayerTimer:playerTime turn:self.isMyTurn placeId:placeId chunksTimer:chunksExtraTimer ischunks:NO];
            chunksExtraTimer = 0;

            //chunks implementation
            [self playerChunksSetUpWithPlayeTurn:placeId];
            [self playerExtraTimerSetUpWithPlayerTurn:placeId extraTime:extraTime];
            
            if ([APP_DELEGATE.tableType isEqualToString:@"BEST_OF_2"] || [APP_DELEGATE.tableType isEqualToString:@"BEST_OF_3"]) {
                //self.viewAutoDrop.hidden = YES;
                [self hideAutoDrop];
            }
            
            else {
                if (self.isMyTurn){
                    if (self.isAutoDrop) {
                        self.viewAutoDrop.hidden = NO;
                        [self performSelector:@selector(autoDrop) withObject:self afterDelay:1.0 ];
                        
                    }
                    else {
                        //                        self.viewAutoDrop.hidden = YES;
                        [self hideAutoDrop];
                    }
                }
                else {
                    //                    self.viewAutoDrop.hidden = NO;
                    [self unHideAutoDrop];
                }
            }
            
        }
        
        [self setPositionForUserIdPlayerTurnView:userID];
    }
    
    else if (self.playerTurnView)
    {
        NSInteger placeId = [self getPlaceIdFromChairUserID:userID];//ratheesh
        
        if(self.isDisConnectReconnect)
        {
            double loadTime = CACurrentMediaTime() - [[TAJGameEngine sharedGameEngine]loginTime] ;
            DLog(@"loadTime %f",loadTime);
            int newPlayerTime;
            newPlayerTime = playerTime - round(loadTime);
            
            if (playerTime > loadTime)
            {
                           [self.playerTurnView startPlayerTimer:newPlayerTime turn:self.isMyTurn placeId:placeId chunksTimer:chunksExtraTimer ischunks:NO];
                chunksExtraTimer = 0;


                //chunks implementation
                [self playerChunksSetUpWithPlayeTurn:placeId];
                [self playerExtraTimerSetUpWithPlayerTurn:placeId extraTime:extraTime];
                
                if ([APP_DELEGATE.tableType isEqualToString:@"BEST_OF_2"] || [APP_DELEGATE.tableType isEqualToString:@"BEST_OF_3"]) {
                    //                    self.viewAutoDrop.hidden = YES;
                    [self hideAutoDrop];
                }
                
                else {
                    if (self.isMyTurn){
                        if (self.isAutoDrop) {
                            self.viewAutoDrop.hidden = NO;
                            [self performSelector:@selector(autoDrop) withObject:self afterDelay:1.0 ];
                            
                        }
                        else {
                            //self.viewAutoDrop.hidden = YES;
                            [self hideAutoDrop];
                        }
                    }
                    else {
                        //                        self.viewAutoDrop.hidden = NO;
                        [self unHideAutoDrop];
                    }
                }
                
            }
            else
            {
                [self.playerTurnView startPlayerTimer:playerTime turn:self.isMyTurn placeId:placeId chunksTimer:chunksExtraTimer ischunks:NO];
                chunksExtraTimer = 0;

                //chunks implementation
                [self playerChunksSetUpWithPlayeTurn:placeId];
                [self playerExtraTimerSetUpWithPlayerTurn:placeId extraTime:extraTime];
                
                if ([APP_DELEGATE.tableType isEqualToString:@"BEST_OF_2"] || [APP_DELEGATE.tableType isEqualToString:@"BEST_OF_3"]) {
                    //                    self.viewAutoDrop.hidden = YES;
                    [self hideAutoDrop];
                }
                
                else {
                    if (self.isMyTurn){
                        if (self.isAutoDrop) {
                            self.viewAutoDrop.hidden = NO;
                            [self performSelector:@selector(autoDrop) withObject:self afterDelay:1.0 ];
                            
                        }
                        else {
                            [self hideAutoDrop];
                        }
                    }
                    else {
                        [self unHideAutoDrop];
                    }
                }
                
            }
            self.isDisConnectReconnect = NO;
        }
        else
        {
            [self.playerTurnView startPlayerTimer:playerTime turn:self.isMyTurn placeId:placeId chunksTimer:chunksExtraTimer ischunks:NO];
            chunksExtraTimer = 0;

            //chunks implementation
            [self playerChunksSetUpWithPlayeTurn:placeId];
            [self playerExtraTimerSetUpWithPlayerTurn:placeId extraTime:extraTime];
            
            if ([APP_DELEGATE.tableType isEqualToString:@"BEST_OF_2"] || [APP_DELEGATE.tableType isEqualToString:@"BEST_OF_3"]) {
                //                self.viewAutoDrop.hidden = YES;
                [self hideAutoDrop];
            }
            
            else {
                if (self.isMyTurn){
                    if (self.isAutoDrop) {
                        self.viewAutoDrop.hidden = NO;
                        [self performSelector:@selector(autoDrop) withObject:self afterDelay:1.0 ];
                        
                    }
                    else {
                        //                        self.viewAutoDrop.hidden = YES;
                        [self hideAutoDrop];
                    }
                }
                else {
                    //                    self.viewAutoDrop.hidden = NO;
                    [self unHideAutoDrop];
                }
            }
        }
    }
}

-(void)playerChunksSetUpWithPlayeTurn:(NSInteger)playerTurn {
    /*
    if (playerTurn == 1) {
        [self setPlayer1Chunks];
    }
    else if (playerTurn == 2) {
        [self setPlayer2Chunks];
    }
    else if (playerTurn == 3) {
        [self setPlayer3Chunks];
    }
    else if (playerTurn == 4) {
        [self setPlayer4Chunks];
    }
    else if (playerTurn == 5) {
        [self setPlayer5Chunks];
    }
    else {
        [self setPlayer6Chunks];
    }
     */
}

-(void)setDefaultsChunks {
    self.viewChunksPlayer1.hidden = YES;
    self.viewChunksPlayer2.hidden = YES;
    self.viewChunksPlayer3.hidden = YES;
    self.viewChunksPlayer4.hidden = YES;
    self.viewChunksPlayer5.hidden = YES;
    self.viewChunksPlayer6.hidden = YES;
}

-(void)setPlayer1Chunks {
    [self setDefaultsChunks];
    self.viewChunksPlayer1.hidden = NO;
}
-(void)setPlayer2Chunks {
    [self setDefaultsChunks];
    self.viewChunksPlayer2.hidden = NO;
}
-(void)setPlayer3Chunks {
    [self setDefaultsChunks];
    self.viewChunksPlayer3.hidden = NO;
}
-(void)setPlayer4Chunks {
    [self setDefaultsChunks];
    self.viewChunksPlayer4.hidden = NO;
}
-(void)setPlayer5Chunks {
    [self setDefaultsChunks];
    self.viewChunksPlayer5.hidden = NO;
}
-(void)setPlayer6Chunks {
    [self setDefaultsChunks];
    self.viewChunksPlayer6.hidden = NO;
}

-(void)setPlayerChunkcLevelWithPlayeTurn:(NSInteger)playerTurn chunksLevel:(NSString *)chunksLevel extraTime:(int )extraTime {
    NSLog(@"PLAYER PLACE ID :%ld",(long)playerTurn);
    NSLog(@"PLAYER CHUNKS LEVEl : %@",chunksLevel);
    NSLog(@"CHAIRS ARRAY : %@",self.chairsArray);
//    if (self.maxPlayers == TWO_PLAYER && playerTurn == 2 )
//    {
////        playerTurn = 4;
//        int index = 0;
//
//        if (playerTurn == 2) {
//            index =  1;
//
//        }
//        UIView *view = self.chairsArray[index];
//        NSArray *subviews = [view subviews];
//        TAJChairView *chairView = (TAJChairView *)subviews[0];
//
//        //    TAJChairView *chairView =  [[TAJChairView alloc] init];
//        //[chairView updateChairWithChunksData:chunksLevel WithplaceID:playerTurn];
//        [chairView setPlayer1ChunksLevelWithChunksLevel:chunksLevel];
//
//    }
//    else {
    
    UIView *view = self.chairsArray[playerTurn - 1];
    NSArray *subviews = [view subviews];
    TAJChairView *chairView = (TAJChairView *)subviews[0];
    
    //    TAJChairView *chairView =  [[TAJChairView alloc] init];
    //[chairView updateChairWithChunksData:chunksLevel WithplaceID:playerTurn];
    [chairView setPlayer1ChunksLevelWithChunksLevel:chunksLevel];
    
//     [self.playerTurnView startPlayerTimer:30 turn:self.isMyTurn placeId:playerTurn chunksTimer:10 ischunks:YES];
    
    chunksExtraTimer = extraTime;

//    }
    /*
     if (playerTurn == 1) {
     if ([chunksLevel isEqualToString:@"3/3"]) {
     [self setPlayer1ChunksLevele3];
     }
     else if ([chunksLevel isEqualToString:@"2/3"]) {
     [self setPlayer1ChunksLevele2];
     }
     else if ([chunksLevel isEqualToString:@"1/3"]) {
     [self setPlayer1ChunksLevele1];
     }
     else {
     [self setPlayer1ChunksLevelDefault];
     }
     
     }
     else if (playerTurn == 2) {
     if ([chunksLevel isEqualToString:@"3/3"]) {
     [self setPlayer2ChunksLevele3];
     }
     else if ([chunksLevel isEqualToString:@"2/3"]) {
     [self setPlayer2ChunksLevele2];
     }
     else if ([chunksLevel isEqualToString:@"1/3"]) {
     [self setPlayer2ChunksLevele1];
     }
     else {
     [self setPlayer1ChunksLevelDefault];
     }
     }
     else if (playerTurn == 3) {
     if ([chunksLevel isEqualToString:@"3/3"]) {
     [self setPlayer3ChunksLevele3];
     }
     else if ([chunksLevel isEqualToString:@"2/3"]) {
     [self setPlayer3ChunksLevele2];
     }
     else if ([chunksLevel isEqualToString:@"1/3"]) {
     [self setPlayer3ChunksLevele1];
     }
     else {
     [self setPlayer1ChunksLevelDefault];
     }
     }
     else if (playerTurn == 4) {
     if ([chunksLevel isEqualToString:@"3/3"]) {
     [self setPlayer4ChunksLevele3];
     }
     else if ([chunksLevel isEqualToString:@"2/3"]) {
     [self setPlayer4ChunksLevele2];
     }
     else if ([chunksLevel isEqualToString:@"1/3"]) {
     [self setPlayer4ChunksLevele1];
     }
     else {
     [self setPlayer1ChunksLevelDefault];
     }
     }
     else if (playerTurn == 5) {
     if ([chunksLevel isEqualToString:@"3/3"]) {
     [self setPlayer5ChunksLevele3];
     }
     else if ([chunksLevel isEqualToString:@"2/3"]) {
     [self setPlayer5ChunksLevele2];
     }
     else if ([chunksLevel isEqualToString:@"1/3"]) {
     [self setPlayer5ChunksLevele1];
     }
     else {
     [self setPlayer1ChunksLevelDefault];
     }
     }
     else {
     if ([chunksLevel isEqualToString:@"3/3"]) {
     [self setPlayer6ChunksLevele3];
     }
     else if ([chunksLevel isEqualToString:@"2/3"]) {
     [self setPlayer6ChunksLevele2];
     }
     else if ([chunksLevel isEqualToString:@"1/3"]) {
     [self setPlayer6ChunksLevele1];
     }
     else {
     [self setPlayer1ChunksLevelDefault];
     }
     }
     */
}

-(void)setPlayer1ChunksLevelDefault {
    self.player1ChunksLevel1.hidden = YES;
    self.player1ChunksLevel2.hidden = YES;
    self.player1ChunksLevel3.hidden = YES;
    
    self.player2ChunksLevel1.hidden = YES;
    self.player2ChunksLevel2.hidden = YES;
    self.player2ChunksLevel3.hidden = YES;
    
    self.player3ChunksLevel1.hidden = YES;
    self.player3ChunksLevel2.hidden = YES;
    self.player3ChunksLevel3.hidden = YES;
    
    self.player4ChunksLevel1.hidden = YES;
    self.player4ChunksLevel2.hidden = YES;
    self.player4ChunksLevel3.hidden = YES;
    
    self.player5ChunksLevel1.hidden = YES;
    self.player5ChunksLevel2.hidden = YES;
    self.player5ChunksLevel3.hidden = YES;
    
    self.player6ChunksLevel1.hidden = YES;
    self.player6ChunksLevel2.hidden = YES;
    self.player6ChunksLevel3.hidden = YES;
}

-(void)setPlayer1ChunksLevele1{
    [self setPlayer1ChunksLevelDefault];
    self.player1ChunksLevel1.hidden = NO;
}

-(void)setPlayer1ChunksLevele2{
    [self setPlayer1ChunksLevelDefault];
    self.player1ChunksLevel1.hidden = NO;
    self.player1ChunksLevel2.hidden = NO;
}

-(void)setPlayer1ChunksLevele3{
    [self setPlayer1ChunksLevelDefault];
    self.player1ChunksLevel1.hidden = NO;
    self.player1ChunksLevel2.hidden = NO;
    self.player1ChunksLevel3.hidden = NO;
}

-(void)setPlayer2ChunksLevele1{
    [self setPlayer1ChunksLevelDefault];
    self.player2ChunksLevel1.hidden = NO;
}

-(void)setPlayer2ChunksLevele2{
    [self setPlayer1ChunksLevelDefault];
    self.player1ChunksLevel1.hidden = NO;
    self.player1ChunksLevel2.hidden = NO;
}

-(void)setPlayer2ChunksLevele3{
    [self setPlayer1ChunksLevelDefault];
    self.player2ChunksLevel1.hidden = NO;
    self.player2ChunksLevel2.hidden = NO;
    self.player2ChunksLevel3.hidden = NO;
}
//
-(void)setPlayer3ChunksLevele1{
    [self setPlayer1ChunksLevelDefault];
    self.player3ChunksLevel1.hidden = NO;
}

-(void)setPlayer3ChunksLevele2{
    [self setPlayer1ChunksLevelDefault];
    self.player3ChunksLevel1.hidden = NO;
    self.player3ChunksLevel2.hidden = NO;
}

-(void)setPlayer3ChunksLevele3{
    [self setPlayer1ChunksLevelDefault];
    self.player3ChunksLevel1.hidden = NO;
    self.player3ChunksLevel2.hidden = NO;
    self.player3ChunksLevel3.hidden = NO;
}
//
-(void)setPlayer4ChunksLevele1{
    [self setPlayer1ChunksLevelDefault];
    self.player4ChunksLevel1.hidden = NO;
}

-(void)setPlayer4ChunksLevele2{
    [self setPlayer1ChunksLevelDefault];
    self.player4ChunksLevel1.hidden = NO;
    self.player4ChunksLevel2.hidden = NO;
}

-(void)setPlayer4ChunksLevele3{
    [self setPlayer1ChunksLevelDefault];
    self.player4ChunksLevel1.hidden = NO;
    self.player4ChunksLevel2.hidden = NO;
    self.player4ChunksLevel3.hidden = NO;
}
//
-(void)setPlayer5ChunksLevele1 {
    [self setPlayer1ChunksLevelDefault];
    self.player5ChunksLevel1.hidden = NO;
}

-(void)setPlayer5ChunksLevele2{
    [self setPlayer1ChunksLevelDefault];
    self.player5ChunksLevel1.hidden = NO;
    self.player5ChunksLevel2.hidden = NO;
}

-(void)setPlayer5ChunksLevele3{
    [self setPlayer1ChunksLevelDefault];
    self.player5ChunksLevel1.hidden = NO;
    self.player5ChunksLevel2.hidden = NO;
    self.player5ChunksLevel3.hidden = NO;
}
//
-(void)setPlayer6ChunksLevele1{
    [self setPlayer1ChunksLevelDefault];
    self.player6ChunksLevel1.hidden = NO;
}

-(void)setPlayer6ChunksLevele2{
    [self setPlayer1ChunksLevelDefault];
    self.player6ChunksLevel1.hidden = NO;
    self.player6ChunksLevel2.hidden = NO;
}

-(void)setPlayer6ChunksLevele3{
    [self setPlayer1ChunksLevelDefault];
    self.player6ChunksLevel1.hidden = NO;
    self.player6ChunksLevel2.hidden = NO;
    self.player6ChunksLevel3.hidden = NO;
}

//extra timer
-(void)setDefaultsExtraTimers {
    self.viewExtraTimerPlayer1.hidden = YES;
    self.viewExtraTimerPlayer2.hidden = YES;
    self.viewExtraTimerPlayer3.hidden = YES;
    self.viewExtraTimerPlayer4.hidden = YES;
    self.viewExtraTimerPlayer5.hidden = YES;
    self.viewExtraTimerPlayer6.hidden = YES;
}

-(void)setPlayer1ExtraTimer {
    [self setDefaultsExtraTimers];
    self.viewExtraTimerPlayer1.hidden = NO;
    self.lblPlayer1ExtraTime.text = @"10";
}

-(void)setPlayer2ExtraTimer {
    [self setDefaultsExtraTimers];
    self.viewExtraTimerPlayer2.hidden = NO;
    self.lblPlayer2ExtraTime.text = @"10";
}

-(void)setPlayer3ExtraTimer {
    [self setDefaultsExtraTimers];
    self.viewExtraTimerPlayer3.hidden = NO;
    self.lblPlayer3ExtraTime.text = @"10";
}

-(void)setPlayer4ExtraTimer {
    [self setDefaultsExtraTimers];
    self.viewExtraTimerPlayer4.hidden = NO;
    self.lblPlayer4ExtraTime.text = @"10";
}

-(void)setPlayer5ExtraTimer {
    [self setDefaultsExtraTimers];
    self.viewExtraTimerPlayer5.hidden = NO;
    self.lblPlayer5ExtraTime.text = @"10";
}

-(void)setPlayer6ExtraTimer {
    [self setDefaultsExtraTimers];
    self.viewExtraTimerPlayer6.hidden = NO;
}

-(void)playerExtraTimerSetUpWithPlayerTurn:(NSInteger)playerTurn extraTime:(NSInteger)extraTime {
    [self.timer invalidate];
    /*
    if (extraTime == 10) {
        if (playerTurn == 1) {
            [self setPlayer1ExtraTimer];
        }
        else if (playerTurn == 2) {
            [self setPlayer2ExtraTimer];
        }
        else if (playerTurn == 3) {
            [self setPlayer3ExtraTimer];
        }
        else if (playerTurn == 4) {
            [self setPlayer4ExtraTimer];
        }
        else if (playerTurn == 5) {
            [self setPlayer5ExtraTimer];
        }
        else {
            [self setPlayer6ExtraTimer];
        }
    }
    else {
        if (playerTurn == 1) {
            [self setPlayer1ExtraTimer];
            self.lblPlayer1ExtraTime.text = @"0";
        }
        else if (playerTurn == 2) {
            [self setPlayer2ExtraTimer];
            self.lblPlayer2ExtraTime.text = @"0";
        }
        else if (playerTurn == 3) {
            [self setPlayer3ExtraTimer];
            self.lblPlayer3ExtraTime.text = @"0";
        }
        else if (playerTurn == 4) {
            [self setPlayer4ExtraTimer];
            self.lblPlayer4ExtraTime.text = @"0";
        }
        else if (playerTurn == 5) {
            [self setPlayer5ExtraTimer];
            self.lblPlayer5ExtraTime.text = @"0";
        }
        else {
            [self setPlayer6ExtraTimer];
            self.lblPlayer6ExtraTime.text = @"0";
        }
        //[self runExtraTimerWithPalyerTurn:playerTurn];
    }
     */
}

-(void)runExtraTimerWithPalyerTurn:(NSInteger)playerTurn extraTime:(NSInteger)extraTime {
    NSLog(@"EXTRA TIME TURN :%ld",(long)playerTurn);
    
    [self.timer invalidate];
    self.currMinute = 1;
    self.currSeconds = 0;
    if (extraTime == 10) {
        
        if (playerTurn == 1) {
            [self startTimerWithTurn:playerTurn];
        }
        else if (playerTurn == 2) {
            [self startTimerWithTurn:playerTurn];
        }
        else if (playerTurn == 3) {
            [self startTimerWithTurn:playerTurn];
        }
        else if (playerTurn == 4) {
            [self startTimerWithTurn:playerTurn];
        }
        else if (playerTurn == 5) {
            [self startTimerWithTurn:playerTurn];
        }
        else {
            [self startTimerWithTurn:playerTurn];
        }
    }
    else {
        if (playerTurn == 1) {
            self.lblPlayer1ExtraTime.text = @"0";
        }
        else if (playerTurn == 2) {
            self.lblPlayer2ExtraTime.text = @"0";
        }
        else if (playerTurn == 3) {
            self.lblPlayer3ExtraTime.text = @"0";
        }
        else if (playerTurn == 4) {
            self.lblPlayer4ExtraTime.text = @"0";
        }
        else if (playerTurn == 5) {
            self.lblPlayer5ExtraTime.text = @"0";
        }
        else {
            self.lblPlayer6ExtraTime.text = @"0";
        }
    }
}

-(void)startTimerWithTurn:(NSInteger)playerTurn
{
    self.currentPlayerTurn = playerTurn;
    
    self.timer=[NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerFiredWithTurn) userInfo:nil repeats:YES];
}

-(void)timerFiredWithTurn
{
    if((self.currMinute>0 || self.currSeconds>=0) && self.currMinute>=0)
    {
        if(self.currSeconds==0)
        {
            self.currMinute-=1;
            self.currSeconds=10;
        }
        else if(self.currSeconds>0)
        {
            self.currSeconds-=1;
        }
        if(self.currMinute>-1)
            if (self.currentPlayerTurn == 1) {
                [self.lblPlayer1ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
            }
            if (self.currentPlayerTurn == 2) {
                [self.lblPlayer2ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
            }
            if (self.currentPlayerTurn == 3) {
                [self.lblPlayer3ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
            }
            if (self.currentPlayerTurn == 4) {
                [self.lblPlayer4ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
            }
            if (self.currentPlayerTurn == 5) {
                [self.lblPlayer5ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
            }
            else {
                [self.lblPlayer6ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
            }
            //[self.lblPlayer1ExtraTime setText:[NSString stringWithFormat:@"%02d",self.currSeconds]];
    }
    else
    {
        [self.timer invalidate];
    }
}

- (void) stopProgressTimer
{
    [self.playerTurnView stopRunningTimer];
    if(self.meldViewTable)
    {
        [self.meldViewTable invalidateMeldTimer];
    }
}

- (void)setPositionForUserIdPlayerTurnView:(NSInteger)userID
{
    NSUInteger playerPosition = [self getPlaceIdFromChairUserID:userID];
    CGPoint center = CGPointZero;
    
    DLog(@"the userids are %d", userID);
    self.currentUserId = userID;
    
    if (self.maxPlayers == TWO_PLAYER && playerPosition == 2)
    {
        playerPosition = 4;
    }
    
    if (playerPosition > 0 && playerPosition <= 6)
    {
        UIView *chairHolder = [self chairHolderFromUserId:userID];
        
        if (chairHolder.subviews && chairHolder.subviews.count > 0)
        {
            CGRect chairFrame = chairHolder.frame;
            if (playerPosition == 1 || playerPosition == 4)
            {
                if ([TAJUtilities isIPhone])
                {
                    if (playerPosition == 4)
                    {
                        if ([TAJUtilities isItIPhone5])
                        {
                            chairFrame.origin.x = chairHolder.frame.origin.x - (self.playerTurnView.frame.size.width * 0.48);
                            chairFrame.origin.y = self.spectatorView.frame.origin.y + self.spectatorView.frame.size.height -16;
                        }
                        else
                        {
                            chairFrame.origin.x = chairHolder.frame.origin.x - (self.playerTurnView.frame.size.width * 0.48);
                            chairFrame.origin.y = self.spectatorView.frame.origin.y + self.spectatorView.frame.size.height - 16;
                        }
                    }
                    else
                    {
                        if ([TAJUtilities isItIPhone5])
                        {
                            if(self.isSpectator)
                            {
                                [self.extraTimeButton setHidden:YES];
                                [self.extraTimeLabel setHidden:YES];
                                self.extraTimeBg.hidden = YES;
                                chairFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width + 12;
                                chairFrame.origin.y = chairHolder.frame.origin.y;
                            }
                            else
                            {
                                [self.extraTimeButton setHidden:NO];
                                [self.extraTimeLabel setHidden:NO];
                                //self.extraTimeBg.hidden = NO;
                                self.extraTimeBg.hidden = YES;
                                //ratheesh
                                chairFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width + 12;
                                chairFrame.origin.y = chairHolder.frame.origin.y - 8;
                                center = self.extraTimeButton.center;
                                
                            }
                        }
                        else
                        {
                            if(self.isSpectator)
                            {
                                [self.extraTimeButton setHidden:YES];
                                [self.extraTimeLabel setHidden:YES];
                                self.extraTimeBg.hidden = YES;
                                chairFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width * 1.1;
                                chairFrame.origin.y = chairHolder.frame.origin.y + self.playerTurnView.frame.size.height * 0.05;
                            }
                            else
                            {
                                [self.extraTimeButton setHidden:NO];
                                [self.extraTimeLabel setHidden:NO];
                                //self.extraTimeBg.hidden = NO;
                                self.extraTimeBg.hidden = YES;//rat
                                chairFrame.origin.x = chairHolder.frame.origin.x + chairHolder.frame.size.width - 5;
                                chairFrame.origin.y = chairHolder.frame.origin.y - 13;
                                center = self.extraTimeButton.center;
                            }
                        }
                    }
                }
                else
                {
                    // for ipad
                    if (playerPosition == 4)
                    {
                        chairFrame.origin.x = chairHolder.frame.origin.x - (self.playerTurnView.frame.size.width * 0.48);
                    }
                    else
                    {
                        if(self.isMyTurn)
                        {
                            [self.extraTimeButton setHidden:NO];
                            [self.extraTimeLabel setHidden:NO];
                            //self.extraTimeBg.hidden = NO;
                            self.extraTimeBg.hidden = YES;//rat
                            chairFrame.origin.x = chairHolder.frame.origin.x + (self.playerTurnView.frame.size.width + 60);
                            chairFrame.origin.y = chairHolder.frame.origin.y -5;
                        }
                        else
                        {
                            if (self.isSpectator)
                            {
                                [self.extraTimeButton setHidden:YES];
                                [self.extraTimeLabel setHidden:YES];
                                self.extraTimeBg.hidden = YES;
                            }
                            chairFrame.origin.x = chairHolder.frame.origin.x - (self.playerTurnView.frame.size.width * 0.85);
                            chairFrame.origin.y = chairHolder.frame.origin.y + self.playerTurnView.frame.size.height * 0.20;
                        }
                        
                    }
                }
            }
            else if (playerPosition == 2)
            {
                if([TAJUtilities isIPhone])
                {
                    if([TAJUtilities isItIPhone5])
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y - (self.playerTurnView.frame.size.height - 25);
                            //chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.65;
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.40;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.55;
                        }
                    }
                    else
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y - (self.playerTurnView.frame.size.height - 25);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.40;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22 + 5);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.50 + 7;
                        }
                    }
                }
                else
                {
                    if(!self.isPlayerModelSlided)
                    {
                        chairFrame.origin.y = chairHolder.frame.origin.y - (self.playerTurnView.frame.size.height - 25);
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.65;
                    }
                    else
                    {
                        chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.41);
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.84;
                    }
                }
                
            }
            else if (playerPosition == 3)
            {
                if([TAJUtilities isIPhone])
                {
                    if([TAJUtilities isItIPhone5])
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.40;
                            chairFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height) + 6;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.55;
                        }
                    }
                    else
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.40;
                            chairFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height) + 6;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22 + 5);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.50 + 7;
                        }
                    }
                }
                else
                {
                    if(!self.isPlayerModelSlided)
                    {
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.65;
                        chairFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height) + 6;
                    }
                    else
                    {
                        chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.41);
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.84;
                    }
                }
            }
            else if (playerPosition == 5)
            {
                if([TAJUtilities isIPhone])
                {
                    if([TAJUtilities isItIPhone5])
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.1;
                            chairFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height) + 6;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22);
                            chairFrame.origin.x = chairHolder.frame.origin.x;
                        }
                        
                    }
                    else
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.1;
                            chairFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height) + 6;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22 + 3);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.02;
                        }
                    }
                }
                else
                {
                    if(!self.isPlayerModelSlided)
                    {
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.1;
                        chairFrame.origin.y = chairHolder.frame.origin.y + (chairHolder.frame.size.height) + 6;
                    }
                    else
                    {
                        chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.41);
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.04;
                    }
                }
            }
            else if (playerPosition == 6)
            {
                if([TAJUtilities isIPhone])
                {
                    if([TAJUtilities isItIPhone5])
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y - (self.playerTurnView.frame.size.height - 25);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.1;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.02;
                        }
                    }
                    else
                    {
                        if(!self.isPlayerModelSlided)
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y - (self.playerTurnView.frame.size.height - 25);
                            chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.1;
                        }
                        else
                        {
                            chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.22 + 3);
                            chairFrame.origin.x = chairHolder.frame.origin.x ;
                        }
                    }
                    
                }
                else
                {
                    if(!self.isPlayerModelSlided)
                    {
                        chairFrame.origin.y = chairHolder.frame.origin.y - (self.playerTurnView.frame.size.height - 25);
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.1;
                    }
                    else
                    {
                        chairFrame.origin.y = chairHolder.frame.origin.y + (self.playerTurnView.frame.size.height * 0.41);
                        chairFrame.origin.x = chairHolder.frame.origin.x + self.playerTurnView.frame.size.width * 0.04;
                    }
                }
                
            }
            
            chairFrame.size.width = self.playerTurnView.frame.size.width;
            chairFrame.size.height = self.playerTurnView.frame.size.height;
            self.playerTurnView.frame = chairFrame;
            
            if (center.x != 0 && center.y != 0)
            {
                self.playerTurnView.center = center;
            }
            if (playerPosition ==1)
            {
                [self.view insertSubview:self.playerTurnView aboveSubview:self.playerModelViewHolder];
                [self.view insertSubview:self.extraTimeButton aboveSubview:self.playerTurnView];
                [self.view insertSubview:self.extraTimeLabel aboveSubview:self.extraTimeButton];
                [self.view insertSubview:self.extraTimeBg belowSubview:self.playerTurnView];
            }
        }
    }
}

- (void)removePlayerTurnTime
{
    if (self.playerTurnView)
    {
        [self.playerTurnView stopRunningTimer];
        [self.playerTurnView removeFromSuperview];
        self.playerTurnView = nil;
    }
}

- (void)invalidatePlaayerTimer
{
    if (self.playerTurnView)
    {
        [self.playerTurnView stopRunningTimer];
    }
}

- (void)removeViewForTurnTime
{
    if (self.reportBugView)
    {
        [self.reportBugView removeFromSuperview];
        self.reportBugView = nil;
    }
    if(self.settingMenuListView)
    {
        [self removeSettingMenuListView];
    }
}

#pragma mark - Chat view controller -

- (void)createChatViewController
{
    if (!self.chatViewController)
    {
        self.viewHolderForChatDiscardLiveFeed.hidden = NO;
        
        self.chatViewController = [[TAJChatViewController alloc] initWithNibName:@"TAJChatViewController" bundle:[NSBundle mainBundle]];
        
        [self.chatViewController currentChatTableID:self.currentTableId];
        self.chatViewController.chatViewDelegate = self;
        
        if (self.thisPlayerModel)
        {
            [self.chatViewController updateChatPlayerModel:self.thisPlayerModel];
        }
    }
}

- (void)removeChatView
{
    if (self.chatViewController && self.viewHolderForChatDiscardLiveFeed)
    {
        [self.chatViewController.view removeFromSuperview];
        if(self.chatViewController.chatHistoryDataSource.count > 0)
        {
            [self.chatViewController clearChatHistory];
        }
    }
}

- (void)highLightChatButton
{
    self.chatAlertImageView.hidden = NO;
}

#pragma mark - ChatViewcontroller Delegate -
-(void)loadChatMenuViewInGamePlay
{
    if(!self.chatMenuView)
    {
        [self createChatMenuView];
    }
    else
    {
        [self removeChatMenuView];
    }
}

-(void)loadChatExtentView:(NSMutableArray *)messageArray andPlayerModel:(TAJPlayerModel *)playerModel
{
    if(self.chatMenuView)
    {
        [self removeChatMenuView];
        [self.chatViewController.chatField resignFirstResponder];
        return;
    }
    [self.gameTableDelegate createChatExtentView:messageArray andPlayerModel:playerModel];
}

-(void)createChatMenuView
{
    if (!self.chatMenuView)
    {
        self.chatMenuView = [[TAJChatMenuViewController alloc]initWithNibName:@"TAJChatMenuViewController" bundle:nil];
        
        CGRect frame = self.chatMenuView.view.frame;
        frame.origin.x = self.view.frame.origin.x;
        frame.origin.y = self.view.frame.origin.y;
        self.chatMenuView.view.frame =frame;
        [self.view addSubview:self.chatMenuView.view];
        self.chatMenuView.chatMenuViewDelegate = self;
    }
}

-(void)removeChatMenuView
{
    [self removeChatMenuViewController];
}

-(void)removeChatMenuViewController
{
    if(self.chatMenuView)
    {
        [self.chatMenuView.view removeFromSuperview];
        self.chatMenuView = nil;
    }
}

#pragma mark - LiveFeed controller -

- (void)createLiveFeedController
{
    self.viewHolderForChatDiscardLiveFeed.hidden = NO;
    
    if (!self.liveFeedController)
    {
        self.liveFeedController = [[TAJLiveFeedViewController alloc] initWithNibName:@"TAJLiveFeedViewController" bundle:nil];
    }
}

- (void)setUpHistoryForLiveFeed:(NSString *)history
{
    if (self.liveFeedController)
    {
        if (!self.liveFeedController.feedModel)
        {
            self.liveFeedController.feedModel = [[TAJLiveFeedModel alloc]initWithFeedModel:history];
        }
        else if (self.liveFeedController.feedModel)
        {
            self.liveFeedController.feedModel = [self.liveFeedController.feedModel updateFeedMessage:history];
        }
        [self.liveFeedController reloadLiveFeedData];
    }
}

- (void)removeLiveFeedView
{
    if (self.liveFeedController && self.viewHolderForChatDiscardLiveFeed)
    {
        [self.liveFeedController.view removeFromSuperview];
    }
}

#pragma mark - Discarded cards history view controller -

- (void)createDiscardCardHistoryView
{
    self.viewHolderForChatDiscardLiveFeed.hidden = NO;
    if (!self.discardCardHistoryController)
    {
        self.discardCardHistoryController = [[TAJDiscardedCardViewController alloc]initWithNibName:@"TAJDiscardedCardViewController" bundle:nil];
    }
}

- (void)addPlayersDiscardedCard:(NSMutableDictionary *)playerCardsDictionary
{
    if (self.discardCardHistoryController)
    {
        // here for each player you need to update model for each player discarded cards
        if (self.discardCardHistoryController.discardCardModel)
        {
            self.discardCardHistoryController.discardCardModel = [self.discardCardHistoryController.discardCardModel updatePlayerDiscardedCards:playerCardsDictionary];
        }
        
        // refresh the card for player if already discard history is showing
        if ([self.discardCardHistoryController isDiscardViewShowing])
        {
            [self.discardCardHistoryController reloadDiscardCardHistory:playerCardsDictionary];
        }
    }
}

- (void)addPlayersListToDiscardHistory:(NSMutableDictionary *)dictionary
{
    if (self.discardCardHistoryController)
    {
        // here first you need to create model for all player who are exist in a game play
        if (!self.discardCardHistoryController.discardCardModel)
        {
            self.discardCardHistoryController.discardCardModel = [[TAJDiscardCardModel alloc]initWithPlayerList:dictionary];
        }
        else if (self.discardCardHistoryController.discardCardModel)
        {
            self.discardCardHistoryController.discardCardModel = [self.discardCardHistoryController.discardCardModel updatePlayerList:dictionary];
        }
        // refresh the card for player if already discard history is showing
        if ([self.discardCardHistoryController isDiscardViewShowing])
        {
            [self.discardCardHistoryController reloadDiscardCardHistory:dictionary];
        }
    }
}

- (void)addPlayersListToDiscardHistoryOnReconnect:(NSMutableDictionary *)dictionary
{
    if (self.discardCardHistoryController)
    {
        // here first you need to create model for all player who are exist in a game play
        if (!self.discardCardHistoryController.discardCardModel)
        {
            self.discardCardHistoryController.discardCardModel = [[TAJDiscardCardModel alloc]initWithPlayerList:dictionary];
        }
        else if (self.discardCardHistoryController.discardCardModel)
        {
            self.discardCardHistoryController.discardCardModel = [self.discardCardHistoryController.discardCardModel updatePlayerList:dictionary];
        }
    }
}

- (void)removeDiscardHistoryView
{
    if (self.discardCardHistoryController && self.viewHolderForChatDiscardLiveFeed)
    {
        [self.discardCardHistoryController.view removeFromSuperview];
    }
}

- (void)removeAllDiscardCardHistoryContent
{
    if (self.discardCardHistoryController)
    {
        //remove all discard history once new game start
        if (self.discardCardHistoryController.discardCardModel)
        {
            self.discardCardHistoryController.discardCardModel = nil;
        }
        // refresh the card for player if already discard history is showing
        if ([self.discardCardHistoryController isDiscardViewShowing])
        {
            [self.discardCardHistoryController removeCardsAlreadyInScrollView];
        }
        [self.discardCardHistoryController hideAllDiscardHistoryButtons];
    }
}

- (void)extractPlayersInGameForDiscardCardHistory:(NSDictionary *)dictionary
{
    NSArray *playersArray;
    playersArray = [dictionary valueForKeyPath:kPlayers_Key];
    for (NSDictionary *playerDict in playersArray)
    {
        NSMutableDictionary *playerDictionary = [NSMutableDictionary dictionary];
        [playerDictionary setObject:playerDict[kTaj_nickname] forKey:kTaj_nickname];
        
        // here first you need to create model for all player1 who are exist in a game play
        [self addPlayersListToDiscardHistory:playerDictionary];
    }
}

-(void)playersDiscardCardHistoryOnThisPlayerSpectator:(NSMutableDictionary *)dictionary
{
    if (dictionary && [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
    {
        NSArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        for (NSDictionary *playerDict in playersArray)
        {
            NSMutableDictionary *playerDictionary = [NSMutableDictionary dictionary];
            [playerDictionary setObject:playerDict[kTaj_nickname] forKey:kTaj_nickname];
            
            // here first you need to create model for all player who are exist in a game play
            [self addPlayersListToDiscardHistoryOnReconnect:playerDictionary];
        }
        if ([self.discardCardHistoryController isDiscardViewShowing])
        {
            [self.discardCardHistoryController reloadDiscardCardHistory:dictionary];
        }
    }
}

#pragma mark - Player reconnect operation -

- (void)playersAfterReconnectForDiscardCardHistory:(NSMutableDictionary *)dictionary
{
    // NSLog(@"IAM BACK DATA : %@",dictionary);
    if (dictionary && [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
    {
        NSArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        
        for (NSDictionary *playerDict in playersArray)
        {
            NSMutableDictionary *playerDictionary = [NSMutableDictionary dictionary];
            [playerDictionary setObject:playerDict[kTaj_nickname] forKey:kTaj_nickname];
            
            // here first you need to create model for all player who are exist in a game play
            [self addPlayersListToDiscardHistoryOnReconnect:playerDictionary];
        }
        if ([self.discardCardHistoryController isDiscardViewShowing])
        {
            [self.discardCardHistoryController reloadDiscardCardHistory:dictionary];
        }
    }
}

- (void)refreshControllerInGetTableDeatilsEvent:(NSDictionary*)dictionary
{
    self.isGetTableDetailsEventCame = YES;
    self.isPlayerCanEnterGamePlayInAutoPlayMode = YES;
    self.isDisConnectReconnect = YES;
    
    [self removeUserTouchRequestAlertViews];
    DLog(@"gettabledetals");
    [self.playTable canDiscardCard:YES];
    [self.playTable removeAllButtons];
    [self.playTable removeTableSelectedCards];
    [self playersAfterReconnectForDiscardCardHistory:dictionary];
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView canModelDiscardCard:YES];
#endif
    [self updateChair];
    NSString *gameStart ,*gameCount;
    gameStart = [dictionary valueForKeyPath:kTaj_Table_gamestart];
    gameCount = [dictionary valueForKeyPath:kTaj_Table_gamecount];
    if([gameCount intValue] == 0 && [gameStart isEqualToString:@"False"])
    {
        [self quitGame];
        return;
    }
    
    self.playTableDictionary = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    
    if (dictionary &&
        [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
    {
        NSUInteger thisPlayerIndex = 0;
        NSUInteger thisPlayerServerPlaceIndex = 0;
        NSMutableArray *reorderPlaceChairArray = [NSMutableArray array];
        NSMutableArray *middleJoinedPlayers =[NSMutableArray array];
        NSMutableArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        self.currentPlayers = playersArray.count;
        //add player to playersArray if player has left the table
        if ([[dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] isKindOfClass:[NSArray class]] &&  [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST])
        {
            NSMutableArray *droppedPlayers = [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST];
            for (int playerIndex = 0; playerIndex < droppedPlayers.count; playerIndex++)
            {
                NSMutableDictionary *allPlayerDropped = droppedPlayers[playerIndex];
                if (allPlayerDropped )
                {
                    BOOL isPlayerAlreadyExistsInPlayersList= NO;
                    //check whether player exist in players array
                    for (int index=0;index < playersArray.count; index++)
                    {
                        if ([playersArray[index][kTaj_userid_Key] isEqualToString:allPlayerDropped[kTaj_userid_Key]])
                        {
                            isPlayerAlreadyExistsInPlayersList = YES;
                        }
                    }
                    if (!isPlayerAlreadyExistsInPlayersList)
                    {
                        if ([allPlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:DROP_TEXT] || [allPlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:ELIMINATE_TEXT] ||[allPlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:TIMEOUT_TEXT])
                        {
                            [allPlayerDropped setObject:TABLE_LEAVE_TEXT forKey:GAME_DEFFERENT_SCREEN_PLAYERTYPE];
                        }
                        
                        [self fillDroppedPlayerOnReconnect:allPlayerDropped];
                        [playersArray addObject:allPlayerDropped];
                    }
                }
                
            }
        }
        else if ([[dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] isKindOfClass:[NSDictionary class]] && [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST] )
        {
            NSMutableDictionary *singlePlayerDropped = [dictionary valueForKeyPath:GAME_DROPPED_PLAYERS_LIST];
            if (singlePlayerDropped)
            {
                BOOL isPlayerAlreadyExistsInPlayersList= NO;
                
                for (int playerIndex=0;playerIndex < playersArray.count; playerIndex++)
                {
                    if ([playersArray[playerIndex][kTaj_userid_Key] isEqualToString:singlePlayerDropped[kTaj_userid_Key]])
                    {
                        isPlayerAlreadyExistsInPlayersList = YES;
                    }
                }
                if (!isPlayerAlreadyExistsInPlayersList)
                {
                    if ([singlePlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:DROP_TEXT] || [singlePlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:ELIMINATE_TEXT] || [singlePlayerDropped[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:TIMEOUT_TEXT])
                    {
                        [singlePlayerDropped setObject:TABLE_LEAVE_TEXT forKey:GAME_DEFFERENT_SCREEN_PLAYERTYPE];
                    }
                    [self fillDroppedPlayerOnReconnect:singlePlayerDropped];
                    [playersArray addObject:singlePlayerDropped];
                    
                }
            }
        }
        
        NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:kTaj_Place_Key ascending:YES];
        NSArray *sortDescriptors = [NSArray arrayWithObject:sortDescriptor];
        NSArray *sortedPlayersArray = [playersArray sortedArrayUsingDescriptors:sortDescriptors];
        
        NSMutableArray *playerChairArray = [NSMutableArray array];
        NSMutableArray *removeChairArray = [NSMutableArray arrayWithArray:self.chairsArray];
        
        for (NSUInteger k = 0; k < sortedPlayersArray.count; k++)
        {
            NSUInteger userID = [sortedPlayersArray[k][kTaj_userid_Key] integerValue];
            [reorderPlaceChairArray addObject:[NSNumber numberWithInteger:[sortedPlayersArray[k][kTaj_Place_Key] integerValue]]];
            
            if (userID == [[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue])
            {
                thisPlayerServerPlaceIndex = [sortedPlayersArray[k][kTaj_Place_Key] integerValue];
                thisPlayerIndex = k;
                break;
            }
        }
        
        for (NSUInteger j = (thisPlayerIndex); j < sortedPlayersArray.count; j++)
        {
            UIView *chairHolder = [self chairHolderFromUserId:[sortedPlayersArray[j][kTaj_userid_Key] integerValue]];
            if (!chairHolder)
            {
                [self createChairForPlayerOnReconnect:sortedPlayersArray[j]];
                chairHolder = [self chairHolderFromUserId:[sortedPlayersArray[j][kTaj_userid_Key] integerValue]];
            }
            TAJChairView *chair = [self chairFromView:chairHolder];
            [removeChairArray removeObject:chairHolder];
            [chair removeFromSuperview];
            [playerChairArray addObject:chair];
            
        }
        
        for (NSUInteger i =0 ; i < thisPlayerIndex; i++)
        {
            UIView *chairHolder = [self chairHolderFromUserId:[sortedPlayersArray[i][kTaj_userid_Key] integerValue]];
            if (!chairHolder)
            {
                [self createChairForPlayerOnReconnect:sortedPlayersArray[i]];
                chairHolder = [self chairHolderFromUserId:[sortedPlayersArray[i][kTaj_userid_Key] integerValue]];
            }
            TAJChairView *chair = [self chairFromView:chairHolder];
            [removeChairArray removeObject:chairHolder];
            [chair removeFromSuperview];
            [playerChairArray addObject:chair];
        }
        
        
        for (UIView *chairHolder in removeChairArray)
        {
            TAJChairView *chair = [self chairFromView:chairHolder];
            //added for testing
            chair.playerModel = nil ;
            [chair otherPlayerLeftGame];
            [chair otherPlayerLeftGameOnThisPlayerDisconnect];
            [playerChairArray addObject:chair];
        }
        
        if (playersArray.count == 2 && self.chairsArray.count > 2)
        {
            // Special case for two players
            [playerChairArray exchangeObjectAtIndex:1 withObjectAtIndex:3];
        }
        //check dictionary for middle joined players
        if (dictionary)
        {
            if ([[dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST] isKindOfClass:[NSArray class]] && [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST])
            {
                NSMutableArray *droppedPlayers = [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST];
                for (int i = 0; i < droppedPlayers.count; i++)
                {
                    NSMutableDictionary *allPlayerDropped = droppedPlayers[i];
                    [middleJoinedPlayers addObject:allPlayerDropped];
                }
            }
            else if ([[dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST] isKindOfClass:[NSDictionary class]]  && [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST])
            {
                NSMutableDictionary *singlePlayer = [dictionary valueForKeyPath:GAME_MIDDLE_PLAYERS_LIST];
                [middleJoinedPlayers addObject:singlePlayer];
            }
        }
        
        int index = 0;
        for (TAJChairView *chair in playerChairArray)
        {
            UIView *chairHolder = self.chairsArray[index];
            
            [chair createNewEmptyChairWithChairID:index + 1];
            if (chair.playerModel.playerPoints)
            {
                for (NSUInteger k = 0; k < sortedPlayersArray.count; k++)
                {
                    NSString* userID = sortedPlayersArray[k][kTaj_userid_Key];
                    
                    if ([userID isEqualToString:chair.playerModel.playerID])
                    {
                        chair.playerModel.playerPoints=sortedPlayersArray[k][kTaj_buyin_amt_other];
                        [chair setUpPlayerPoints:sortedPlayersArray[k][kTaj_buyin_amt_other]];
                        break;
                    }
                }
            }
            if (chair.playerModel)
            {
                chair.playerModel.placeID = [NSString stringWithFormat:@"%d",index + 1];
            }
            [chairHolder addSubview:chair];
            index ++;
        }
        
        // add middle joined player to table
        for(int index=0;index <middleJoinedPlayers.count; index++)
        {
            int chairID = [middleJoinedPlayers[index][kTaj_Place_Key] integerValue];
            chairID = chairID - 1;
            
            UIView *view = self.chairsArray[chairID];
            NSArray *subviews = [view subviews];
            TAJChairView *aChair = (TAJChairView *)subviews[0];
            
            // Init player model also
            TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:middleJoinedPlayers[index]];
            [aChair updateChairWithData:model];
            [aChair createNewEmptyChairWithChairID:chairID];
            aChair.isMiddleJoin = YES;
            if (aChair.playerModel)
            {
                aChair.playerModel.placeID = [NSString stringWithFormat:@"%d",chairID+1];
            }
            [view addSubview:aChair];
        }
        UIView *chairHolder = [self chairHolderFromUserId:[[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue]];
        if (chairHolder)
        {
            self.thisDevicePlayer = chairHolder;
            
            // Set player model for device player
            TAJChairView *chair = [self chairFromView:chairHolder];
            self.thisPlayerModel = chair.playerModel;
            if(self.thisPlayerModel)
            {
                [self createChatView];
            }
        }
    }
    //For Points type game if only one player present in the table
    else if (dictionary &&
             [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
    {
        if (1)
        {
            [self quitGame];
            return;
        }
        
        NSMutableArray *playerChairArray = [NSMutableArray array];
        NSMutableArray *removeChairArray = [NSMutableArray arrayWithArray:self.chairsArray];
        NSMutableDictionary *playersDetails = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        
        if (dictionary)
        {
            if ([playersDetails[kTaj_userid_Key] isEqualToString:[[[TAJGameEngine sharedGameEngine] usermodel] userId]])
            {
                if (self.chairsArray && self.chairsArray.count > 0)
                {
                    for (UIView *view in self.chairsArray)
                    {
                        if (view.subviews && view.subviews.count > 0)
                        {
                            // Already added
                            NSArray *subviews = [view subviews];
                            TAJChairView *chair = (TAJChairView *)subviews[0];
                            
                            if ([chair.playerModel.playerID isEqualToString:playersDetails[kTaj_userid_Key]])
                            {
                                
                            }
                            else
                            {
                                [chair playerQuitTableBeforeStartGame:YES];
                            }
                        }
                    }
                }
                
                UIView *chairHolder = [self chairHolderFromUserId:[playersDetails[kTaj_userid_Key] integerValue]];
                if (!chairHolder)
                {
                    [self createChairForPlayerOnReconnect:playersDetails];
                    chairHolder = [self chairHolderFromUserId:[playersDetails[kTaj_userid_Key] integerValue]];
                }
                TAJChairView *chair = [self chairFromView:chairHolder];
                [removeChairArray removeObject:chairHolder];
                [chair removeFromSuperview];
                [playerChairArray addObject:chair];
                DLog(@"sorted players id %d", [playersDetails[kTaj_userid_Key] integerValue]);
                
                for (UIView *chairHolder in removeChairArray)
                {
                    TAJChairView *chair = [self chairFromView:chairHolder];
                    //added for testing
                    chair.playerModel = nil ;
                    [chair playerQuitTableBeforeStartGame:YES];
                    [chair playerQuitTableBeforeStartGame:YES];
                    [playerChairArray addObject:chair];
                    
                }
                int index = 0;
                for (TAJChairView *chair in playerChairArray)
                {
                    UIView *chairHolder = self.chairsArray[index];
                    
                    [chair createNewEmptyChairWithChairID:index + 1];
                    if (chair.playerModel)
                    {
                        chair.playerModel.placeID = [NSString stringWithFormat:@"%d",index + 1];
                    }
                    [chairHolder addSubview:chair];
                    index ++;
                }
            }
            UIView *chairHolder = [self chairHolderFromUserId:[[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue]];
            if (chairHolder)
            {
                self.thisDevicePlayer = chairHolder;
                
                // Set player model for device player
                TAJChairView *chair = [self chairFromView:chairHolder];
                self.thisPlayerModel = chair.playerModel;
                if(self.thisPlayerModel)
                {
                    [self createChatView];
                }
                DLog(@"this player model %@",self.thisPlayerModel);
            }
            [self removeMeldView];
            [self.playTable playerJoined:playersDetails isThisDevicePlayerSpectator:NO];
            [self performSelector:@selector(refreshChairsOnGameDeschedule) withObject:nil afterDelay:2.0f];
            self.canLeaveTable = YES;
        }
    }
    
    [self performSelector:@selector(checkPlayerDropped:) withObject:dictionary afterDelay:1.1f];
    [self performSelector:@selector(resetTableAfterPlayerReconnectedBack:) withObject:dictionary afterDelay:1.0];
    NSString *gameStarted = [self.playTableDictionary valueForKeyPath:kTaj_Table_gamestart];
    if(![gameStarted isEqualToString:@"False"])
    {
        [self showCardBackImagesForChairs];
    }
    if(!self.isPlayerQuit)
    {
        if (self.chatViewController)
        {
            [self.chatViewController updateChatPlayerModel:self.thisPlayerModel];
        }
    }
    
    NSString *welcomeAvatar;
    if ([TAJUtilities isIPhone])
    {
        welcomeAvatar = [NSString stringWithFormat:@"welcome_%@_0%@-568h@2x~iphone.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
        
    }
    else
    {
        welcomeAvatar = [NSString stringWithFormat:@"welcome_%@_0%@~ipad.png",self.thisPlayerModel.playerGender,self.thisPlayerModel.playerCharNumber];
        
    }
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    TAJUserModel *model = engine.usermodel;
    [model updateThisDeviceImageName:welcomeAvatar];
    NSString *welcomeImage = model.thisDeviceImageName;
    self.devicePlayerAvatarImageView.image = [UIImage imageNamed:welcomeImage];
    self.devicePlayerAvatarImageView.layer.cornerRadius = 4.0f;
    
}

// on internet reconnection or on creating new table add player to table and show drop symobol on a chair for the player who left
- (void)fillDroppedPlayerOnReconnect:(NSDictionary *)dictionary
{
    int chairID = [dictionary[kTaj_Place_Key] integerValue];
    chairID = chairID - 1;
    if (dictionary && [dictionary[GAME_DEFFERENT_SCREEN_PLAYERTYPE] isEqualToString:@"table_leave"])
    {
        UIView *view = [self chairHolderFromUserId:[dictionary[kTaj_userid_Key] integerValue]];
        TAJChairView *aChair;
        NSArray *subviews ;
        if (!view)
        {
            view = self.chairsArray[chairID];
            subviews= [view subviews];
            aChair = (TAJChairView *)subviews[0];
            if(aChair.isOccupied && aChair.playerModel)
            {
                
                if (self.chairsArray && self.chairsArray.count > 0)
                {
                    int chairPlace= 0;
                    for (UIView *chairHolder in self.chairsArray)
                    {
                        if (chairHolder.subviews && chairHolder.subviews.count > 0)
                        {
                            // Already added
                            NSArray *subviews = [chairHolder subviews];
                            TAJChairView *chair = (TAJChairView *)subviews[0];
                            if (!chair.playerModel && !chair.isOccupied)
                            {
                                chairID =chairPlace;
                                break;
                            }
                            else
                            {
                                chairPlace++;
                            }
                        }
                    }
                }
            }
            view = self.chairsArray[chairID];
            subviews= [view subviews];
            aChair = (TAJChairView *)subviews[0];
        }
        else
        {
            aChair = [self chairFromView:view];
        }
        
        // Init player model also
        TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:dictionary];
        [aChair updateChairForDroppedPlayerWithData:model];
    }
    
}

//on internet reconnection allocate a chair for a player if chair is not allocated to the player
- (void)createChairForPlayerOnReconnect:(NSDictionary *)dictionary
{
    int chairID = [dictionary[kTaj_Place_Key] integerValue];
    chairID = chairID - 1;
    UIView *view = self.chairsArray[chairID];;
    NSArray *subviews = [view subviews];
    TAJChairView *aChair = (TAJChairView *)subviews[0];
    if(aChair.isOccupied && aChair.playerModel)
    {
        if (self.chairsArray && self.chairsArray.count > 0)
        {
            int chairPlace = 0;
            for (UIView *chairHolder in self.chairsArray)
            {
                if (chairHolder.subviews && chairHolder.subviews.count > 0)
                {
                    // Already added
                    NSArray *subviews = [chairHolder subviews];
                    TAJChairView *chair = (TAJChairView *)subviews[0];
                    
                    if (!chair.playerModel && !chair.isOccupied)
                    {
                        chairID =chairPlace;
                        break;
                    }
                    else
                    {
                        chairPlace++;
                    }
                }
            }
        }
    }
    view = self.chairsArray[chairID];;
    subviews = [view subviews];
    aChair = (TAJChairView *)subviews[0];
    // Init player model also
    TAJPlayerModel *model = [[TAJPlayerModel alloc] initWithDictionary:dictionary];
    [aChair updateChairWithData:model];
    aChair.playerModel.placeID = [NSString stringWithFormat:@"%d",chairID+1] ;
    
}
- (NSString *)avatarImageForPlayerIAmBackView
{
    DLog(@"self.avatarImageForIAmBack=%@",self.avatarImageForIAmBack);
    return self.avatarImageForIAmBack;
}

//on internet reconnection reset the table details i.e.dealer symbol,prize info of table
- (void)resetTableAfterPlayerReconnectedBack:(NSDictionary *)dictionary
{
    if (dictionary)
    {
        [self resetPlayerDealerArray]; // remove dealer image exist
        // place dealer image
        if ([dictionary valueForKeyPath:GAME_TABLE_DEALER_ID])
        {
            [self dealerImageForPlayer:[dictionary valueForKeyPath:kTaj_Table_details]];
        }
        // update game id and prize
        NSDictionary *extraGameInfo = [dictionary valueForKeyPath:GAME_TABLE_PRIZE_INFO];
        [self updateGameIDAndPrize:extraGameInfo];
    }
}

- (void)removeGameTableMessage
{
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView removeExtraMessagePlayerMode];
    }
    [self.playTable removeExtraMessage];
}

- (void)showErrorAlert:(NSString *)message
{
    [self showAlertMessageOnServerError:message];
}

- (void)playerReconnectedBackWithGetTableExtraEvent:(NSDictionary *)dictionary
{
    self.isPlayerDisconnected = NO;
    //    DLog(@"playerReconnectedBackWithGetTableExtraEvent isValidShowPreGameResult %d",self.isValidShowPreGameResult);
    //    // update game result/ score sheet data
    if ([dictionary valueForKeyPath:kTaj_Event_Key]
        && [[dictionary valueForKeyPath:kTaj_Event_Key] isKindOfClass:[NSDictionary class]]
        && [[dictionary valueForKeyPath:kTaj_Game_result_reconnect] isEqualToString:@"GAME_RESULT"] && !self.isValidShowPreGameResult && !self.isGameEnded)
    {
        [self updateGameResultOnReconnection:[dictionary valueForKeyPath:kTaj_Event_Key]];
        self.isValidShowPreGameResult = NO;
    }
    
    // i commented this
    //opened ratheesh
    // update discard history
    if ([dictionary valueForKeyPath:NETWORK_DISCARDED_CARDS]
        && [[dictionary valueForKeyPath:NETWORK_DISCARDED_CARDS] isKindOfClass:[NSArray class]])
    {
        [self addPlayersDiscardedCardAfterPlayerReconnect:[dictionary valueForKeyPath:NETWORK_DISCARDED_CARDS]];
    }
    else if ([dictionary valueForKeyPath:NETWORK_DISCARDED_CARDS]
             && [[dictionary valueForKeyPath:NETWORK_DISCARDED_CARDS] isKindOfClass:[NSDictionary class]])
    {
        NSMutableArray *array = [NSMutableArray array];
        NSMutableDictionary *singleDict = [NSMutableDictionary dictionaryWithDictionary:[dictionary valueForKeyPath:NETWORK_DISCARDED_CARDS]];
        [array addObject:singleDict];
        [self addPlayersDiscardedCardAfterPlayerReconnect:array];
    }
    //opened ratheesh
    if (self.meldViewTable)
    {
        if (self.meldViewTable.sendCardButton.isHidden && !self.meldViewTable.isTimeOut)
        {
            [self.meldViewTable playerReconnectMeldHandlerForDisconnection];
            [self removeMeldView];
        }
        else
        {
            //[self removeMeldView];
        }
    }
    
    if (self.smartCorrectionView)
    {
        if (!self.smartCorrectionView.isTimeOut)
        {
            [self.smartCorrectionView playerReconnectMeldHandlerForDisconnection];
            [self removeSmartCorrectionView];
        }
        else
        {
            //[self removeMeldView];
        }
    }
    
}

- (void)addPlayersDiscardedCardAfterPlayerReconnect:(NSMutableArray *)playerDiscardCards
{
    [self.discardCardHistoryController.discardCardModel removePlayerDiscardedCardsOnReconnect];
    NSPredicate *predicateDiscard = [NSPredicate predicateWithFormat:kTaj_discardHistory_predict];
    NSArray *discardHistory = [playerDiscardCards filteredArrayUsingPredicate:predicateDiscard];
    for (NSUInteger i = 0; i < discardHistory.count; i++)
    {
        [self addPlayersDiscardedCard:discardHistory[i]];
    }
}

- (NSInteger)gameTableCurrentID
{
    return [self.currentTableId integerValue];
}

- (BOOL)playerJoinedSpecator
{
    return self.isSpectator;
}

#pragma mark - Score sheet / Game result delegate -

- (void)createScoreSheetView:(NSDictionary *)dictionary
{
//    [[Crashlytics sharedInstance] crash];
    
    NSLog(@"RESULT GAME END");
    //NSLog(@"RESULT DATA : %@",dictionary);
    self.scoreSheetViewController = [[TAJScoreSheetViewController alloc] initWithNibName:@"TAJScoreSheetViewController" bundle:nil];
    self.scoreSheetViewController.view.frame = self.view.frame;
    
    self.scoreSheetViewController.scoreSheet = [[TAJScoreSheetModel alloc] initWithGameResult:dictionary];
    self.scoreSheetViewController.scoreSheetDelegate = self;
    
    [self.view addSubview:self.scoreSheetViewController.view];
    [self.scoreSheetViewController reloadScoreSheet];
    [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 3");
    
    //added for chunks implementation
    [self setDefaultsChunks];
    [self setDefaultsExtraTimers];
    //end here
    
    if ([self.savedFirstTableID isEqualToString:[dictionary valueForKey:@"TAJ_table_id"]]) {
        //[self saveToCoreDataWithEvent:@"Game End"];
        //[self fetchDataFromCoreData];
    }
    else {
        //[self saveToCoreDataWithSecondTableEvent:@"Game End"];
        //[self fetchSecondTableDataFromCoreData];
    }
    [self updateShowButton:NO];
}

- (void)showScoreSheet
{
    if (self.scoreSheetViewController && ![self.scoreSheetViewController isShowing])
    {
        if(self.reportBugView)
        {
            [self reportBugView];
        }
        self.scoreSheetViewController.view.frame = self.view.frame;
        [self.view addSubview:self.scoreSheetViewController.view];
        
    }
    [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 4");
    
    //added for chunks implementation
    [self setDefaultsChunks];
    [self setDefaultsExtraTimers];
    //end here
}

- (void)didSplitRequested:(NSDictionary *)dictionary withTime:(double)time
{
    [[TAJGameEngine sharedGameEngine]splitRequestWithTableID:self.currentTableId];
    [self resetScoreSheet:dictionary withTime:time];
    if(self.isPlayerModelSlided)
    {
        [self.playerModelView showInstructionPlayerMode:SPLIT_REQUESTED_MSG];
    }
    [self.playTable showInstruction:SPLIT_REQUESTED_MSG];
    self.isSplitRequested = YES;
    self.previousMessage = SPLIT_REQUESTED_MSG;
}


- (void)didScoreSheetClosed:(NSDictionary *)dictionary withTime:(double)timeLeft
{
    [self resetScoreSheet:dictionary withTime:timeLeft];
}

- (void)removeScoreSheetView
{
    if (self.scoreSheetViewController && [self.scoreSheetViewController isShowing])
    {
        [self.scoreSheetViewController.view removeFromSuperview];
        [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
        NSLog(@"UN-HIDE BUTTONS1");
    }
}

- (void)resetScoreSheet:(NSDictionary *)dictionary withTime:(double)timeLeft
{
    [self removeScoreSheetView];
    
    if (timeLeft && dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.currentTableId])
    {
        // refresh chair view once if any player is dropped
        [self refreshChairView];
        
        if (self.rebuyAlertView)
        {
            self.isRebuyin = NO;
#if NEW_INFO_VIEW_IMPLEMENTATION
            [self.rebuyAlertView hide];
#else
            // CANCEL BUTTON Presssed
            [self.rebuyAlertView dismissWithClickedButtonIndex:1 animated:YES];
            self.rebuyAlertView = nil;
#endif
        }
        
        // start running timer for new game
        
        [self restartTimerToNewGame:timeLeft andData:dictionary];
        
        // newly added
        [self hideViewForNewGame];
        // [self.playTable removeImagesFromMyDeckHolder];
        [self.playTable removeMyDeckCardsFromHolder];
    }
}

#pragma mark - Score Board -

- (void)createScoreBoard
{
    if (!self.boardScoreController)
    {
        if (self.isJokerTypeGame)
        {
            self.boardScoreController = [[TAJScoreBoardViewController alloc] initWithNibName:@"TAJScoreBoardViewController" bundle:nil Variant:eStrikesType];
        }
        else
        {
            self.boardScoreController = [[TAJScoreBoardViewController alloc] initWithNibName:@"TAJScoreBoardViewController" bundle:nil Variant:ePoolOrDealType];
        }
    }
    self.boardScoreController.delegate = self;
    [self.view bringSubviewToFront:self.boardScoreController.view];
    //[self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 5");
}

- (void)removeScoreBoard
{
    if (self.boardScoreController && [self.boardScoreController isShowing])
    {
        [self.boardScoreController.view removeFromSuperview];
        [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
        NSLog(@"UN-HIDE BUTTONS2");
    }
    
    //    self.scoreBoardLabel.textColor = [UIColor whiteColor];
}

- (void)changeScoreBoardButtonProperty
{
    [self removeScoreBoard];
}

#pragma mark - Meld View helper methods

- (NSString*)meldCardsStringForArray:(NSMutableArray*)array
{
    NSString *boxStartString = BOX_START_STRING;
    NSString *cardStartString = CARD_START_STRING;
    NSString *cardEndString = CARD_END_STRING;
    NSString *faceString = FACE_STRING;
    NSString *suitString = SUIT_STRING;
    NSString *boxEndString = BOX_END_STRING;
    
    NSString *finalString = @"";
    for (NSArray *cardsInfo in array)
    {
        if ([cardsInfo count] > 0)
        {
            finalString = [finalString stringByAppendingString:boxStartString];
            for (NSDictionary *singleCardInfo in cardsInfo)
            {
                NSString * faceValue = singleCardInfo[FACE_KEY];
                NSString * suitValue = singleCardInfo[SUIT_KEY];
                suitValue = [suitValue lowercaseString];
                NSString *faceValueString = [NSString stringWithFormat:faceString,faceValue];
                NSString *suitValueString = [NSString stringWithFormat:suitString,suitValue];
                finalString = [finalString stringByAppendingString:cardStartString];
                finalString = [finalString stringByAppendingString:faceValueString];
                finalString = [finalString stringByAppendingString:suitValueString];
                finalString = [finalString stringByAppendingString:cardEndString];
            }
            finalString = [finalString stringByAppendingString:boxEndString];
        }
    }
    
    DLog(@"meld cards iteration : %@",finalString);
    return finalString;
}

#pragma mark - Meld View delegate -

- (void)createSmartCorrectionView
{
    NSArray *nibContents;
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = 0;//[TAJUtilities isItIPhone5]? 0 : 1;
    }
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJSmartCorrectionViewNibName  owner:nil options:nil];
    self.smartCorrectionView = nibContents[index];
    [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 6");
    
    [self.view addSubview:self.smartCorrectionView];
    [TAJAppDelegate setScreenSize: self.smartCorrectionView];
    self.smartCorrectionView.meldDelegate = self;
    
    self.smartCorrectionView.userInteractionEnabled = YES;
}

#pragma mark - Meld View delegate -

- (void)createMeldTableView
{
    //commented by ratheesh
//    if ([self.playerShowTime isValid])
//    {
//        [self.playerShowTime invalidate];
//        self.showTimeoutTime = 0;
//        if(self.isPlayerModelSlided)
//        {   // [self.playerModelView removeExtraMessagePlayerMode];
//            [self.playerModelView removeInfoLabelMessagePlayerMode];
//        }
//        //[self.playTable removeInfoLabelMessage];
//    }
    
    [self.playerModelView disablePlayerModeSlideButton];
    NSArray *nibContents;
    //meld view not available
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = 0;//[TAJUtilities isItIPhone5]? 0 : 1;
    }
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJMeldCardViewNibName  owner:nil options:nil];
    self.meldViewTable = nibContents[index];
    [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 7");
    
    [self.view addSubview:self.meldViewTable];
    
    //added for chunks
    [self setDefaultsChunks];
    [self setDefaultsExtraTimers];
    
    //    CGRect frame = self.meldViewTable.frame;
    //    frame.origin.x = (self.view.frame.size.width/2) - (self.meldViewTable.frame.size.width/2);
    //#if PLAYER_MODE_INFO_DISPLAY
    //    frame.origin.y = ([TAJUtilities isIPhone]) ? MELD_VIEW_TOP_OFFSET_IPHONE : MELD_VIEW_TOP_OFFSET_IPAD;// - ([TAJUtilities isIPhone]) ? 40 : 100;
    //
    //#else
    //    frame.origin.y = ([TAJUtilities isIPhone]) ? MELD_VIEW_TOP_OFFSET_IPHONE : MELD_VIEW_TOP_OFFSET_IPAD;
    //
    //#endif
    //
    //    self.meldViewTable.frame = frame;
    [TAJAppDelegate setScreenSize: self.meldViewTable];
    //set meld card view delegate
    self.meldViewTable.meldDelegate = self;
    
    self.meldViewTable.userInteractionEnabled = YES;
    //send cards to meld view
    APP_DELEGATE.jokerNumber = [self.jokerCardId substringToIndex:[self.jokerCardId length] - 1];//by ratheesh
#if CARD_GROUPING_SLOTS
    [self.showButton setTitle: @"DECLARE" forState: UIControlStateNormal];
    [self.meldViewTable setupMyDeckCardsWithArray:self.slotCardInfoArray withJokerNumber:self.jokerCardId];
#else
    [self.meldViewTable setupMyDeckCardsWithArray:self.myCardsDealArray withJokerNumber:self.jokerCardId];
#endif
    
    //hide show button
    //[self updateShowButton:NO]; //ReDIM Changes
    if (!self.isPlayerModelSlided)
    {
        self.playTable.canMeldGroupCards = YES;;
        
        [self.playTable updateMeldGroupButton];
        
    }
    
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView canShowMeldGroupForPlayerModel:YES];
    // Update meld group button for player model
    [self.playerModelView updateMeldGroupModelButton];
#endif
    
    self.playerModelButton.enabled = NO;
    if(self.isPlayerModelSlided)
    {
        self.winnerAIImageView.hidden = YES;
    }
    else
    {
        self.winnerAIImageView.hidden = NO;
    }
}

- (void)removeMeldGroupButtonOnSend
{
    [self.playTable removeMeldGroupAllButton];
    if(self.infoPopUpController)
    {
        [self.infoPopUpController hide];
        self.infoPopUpController = nil;
    }
}

- (void) dismissMeldScreen {
    [self removeMeldView];
}

- (void)didSendMeldCards:(NSMutableArray *)meldCards
{
    //    DLog(@"didsendsendmeldcatds iam back pressed %d, didsendmeldcards %d",self.isIamBackButtonPressed,self.didSendMeldCards);
    if (!self.isIamBackButtonPressed || self.didSendMeldCards)
    {
        if (self.meldViewTable)
        {
            self.meldViewTable.userInteractionEnabled = NO;
            // remove remaining cards from play table
            [self.playTable removeRemainingCardsAfterSendCardFromMeld];
            [self removeMeldView];
            if(self.isPlayerModelSlided)
            {
                [self.playerModelView removeExtraMessagePlayerMode];
            }
            [self.playTable removeExtraMessage];
            [self.sortButton setEnabled:NO];
            [self.playTable removeAllButtons];
        }
        return;
    }
    [self updateShowButton:NO];
    self.myFinalHandCardsArray = [NSMutableArray arrayWithArray:self.myCardsDealArray];
    NSInteger totalMeldCards = [self getNumberOfMeldCards:meldCards];
    //    DLog(@"number of melded cards %d",totalMeldCards);
    if (totalMeldCards > 13 && totalMeldCards < 19)
    {
        for (int index = 0; index <meldCards.count; index++)
        {
            for (NSDictionary * card in meldCards[index])
            {
                if ([card isKindOfClass:[NSDictionary class]])
                {
                    DLog(@"card %@",card);
                    if (![self isCardExistsInFinalSlotArray:card[kTaj_Card_Face_Key] withSuit:card[kTaj_Card_Suit_Key]])
                    {
                        [meldCards[index] removeObject:card];
                    }
                }
            }
            
        }
        
    }
    //    DLog(@"final melded cards %@",meldCards);
    
    if (self.meldViewTable)
    {
        self.meldViewTable.userInteractionEnabled = NO;
        // remove remaining cards from play table
        [self.playTable removeRemainingCardsAfterSendCardFromMeld];
        if (self.discardCardInfoForShow)
        {
            // this api is for sending this player cards
            // if this player clicked on show button with discard card info (13 cards  + 1 discard card)
            [[TAJGameEngine sharedGameEngine] cardsMeldWithTableID:self.currentTableId
                                                             cards:[self meldCardsStringForArray:meldCards]
                                                              suit:[self.discardCardInfoForShow[kTaj_Card_Suit_Key] lowercaseString]
                                                              face:self.discardCardInfoForShow[kTaj_Card_Face_Key] withMsgUUid:self.meldThisMsgUUid];
            
            self.didSendMeldCards = YES;
            NSLog(@"MELD SUCCESS");
            NSLog(@"ONE : %@",self.savedFirstTableID);
            NSLog(@"TWO : %@",self.tableIDString);
            if ([self.savedFirstTableID isEqualToString:self.tableIDString]) {
               // [self saveToCoreDataWithEvent:@"Meld"];
            }
            else {
                //[self saveToCoreDataWithSecondTableEvent:@"Meld"];
            }
            
            [self removeMeldView];
        }
        else if(!self.discardCardInfoForShow)
        {
            // we are setting self.discardCardInfo = nil in (handleCreateMeldForOtherPlayer: )
            // this api is for sending other player meld cards
            // once this player meld success, other player can meld with out discard card info (13 cards remaining in other players)
            [[TAJGameEngine sharedGameEngine] cardsMeldForOtherPlayerWithTableID:self.currentTableId cards:[self meldCardsStringForArray:meldCards] withMsgUUid:self.meldOtherMsgUUid];
            self.didSendMeldCards = YES;
        }
        if(self.isPlayerModelSlided)
        {
            [self.playerModelView removeExtraMessagePlayerMode];
        }
        [self.playTable removeExtraMessage];
        [self.sortButton setEnabled:NO];
        [self.playTable removeAllButtons];
        [self.playerModelView removeAllModelButtons];
        [self slideDownPlayerModel];
        
    }
}

- (void)didSendCheckMeldCards:(NSMutableArray *)meldCards{
    [[TAJGameEngine sharedGameEngine] cardsCheckMeldWithTableID:self.currentTableId
                                                          cards:[self meldCardsStringForArray:meldCards]
                                                           suit:[self.discardCardInfoForShow[kTaj_Card_Suit_Key] lowercaseString]
                                                           face:self.discardCardInfoForShow[kTaj_Card_Face_Key] withMsgUUid:self.meldThisMsgUUid];
}

- (void)didAgreeSmartCards:(NSString *)agree{
    [self removeSmartCorrectionView];
    [[TAJGameEngine sharedGameEngine] smartCorrectionWithTableID:self.currentTableId nickName:self.thisPlayerModel.playerName userID:self.thisPlayerModel.playerID msgUUID:self.smartCorrectionThisMsgUUid agree:agree gameId:self.gameIDString];
}

- (NSInteger)getNumberOfSlotCards:(NSMutableArray *)array
{
    NSInteger numberOfCards = 0;
    for (int cardIndex=0; cardIndex < array.count; cardIndex++)
    {
        if ([array[cardIndex] isKindOfClass:[NSDictionary class]])
        {
            numberOfCards ++;
        }
    }
    return numberOfCards;
}

- (NSInteger)getNumberOfMeldCards:(NSMutableArray *)array
{
    NSInteger numberOfCards = 0;
    for (int arrayIndex = 0; arrayIndex <array.count; arrayIndex++)
    {
        for (NSDictionary * card in array[arrayIndex])
        {
            if ([card isKindOfClass:[NSDictionary class]])
            {
                numberOfCards ++;
            }
        }
    }
    return numberOfCards;
}

- (BOOL)isCardExistsInFinalSlotArray:(NSString *)inCardNo withSuit:(NSString *)inSuit
{
    BOOL isCardExist = NO;
    for (int i = 0; i < self.myFinalHandCardsArray.count; i++)
    {
        NSMutableDictionary *cardInfo = self.myFinalHandCardsArray[i];
        if (cardInfo[kTaj_Card_Face_Key])
        {
            NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
            NSString *cardSuit = [cardInfo[kTaj_Card_Suit_Key] lowercaseString];
            
            if ([cardNum isEqualToString:inCardNo] &&
                [cardSuit isEqualToString:[inSuit lowercaseString]])
            {
                isCardExist = YES;
                [self.myFinalHandCardsArray removeObjectAtIndex:i];
                break;
            }
        }
    }
    return isCardExist;
}

- (void)enableSortButtonForMeld:(BOOL)enable
{
    [self updateSortButton:enable];
}

- (void)didTapMeldCard:(NSUInteger)placeId
{
    if (self.playTable )
    {
        [self.playTable didTapMeldCard:placeId isPlayerModeEnabled:self.isPlayerModelSlided];
    }
#if PLAYER_MODEL_GAME_CARD
    [self.playerModelView didTapModelMeldCard:placeId];
#endif
}

- (void)didQuitMeld
{
    if (self.meldViewTable)
    {
        [self.playTable meldCancelled];
        //at the end remove meld view
        [self removeMeldView];
    }
}

- (void)removeMeldView
{
    if (!self.meldViewTable.isTimeOut)
    {
        //[self updateShowButton:YES];
        [self.showButton setTitle: @"SHOW" forState: UIControlStateNormal];
        [self.meldViewTable.firstMeldArray removeAllObjects];
        [self.meldViewTable.secondMeldArray removeAllObjects];
        [self.meldViewTable.thirdMeldArray removeAllObjects];
        [self.meldViewTable.fourthMeldArray removeAllObjects];
        //[self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
        //NSLog(@"UN-HIDE BUTTONS3");
    }
    
    self.meldViewTable.meldDelegate = nil;
    [self.meldViewTable removeFromSuperview];
    self.meldViewTable = nil;
//    [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
//    NSLog(@"UN-HIDE BUTTONS3");
    self.playerModelButton.enabled = YES;
    [self.playTable removeMeldGroupAllButton];
    
    // meld  AI
    self.winnerAIImageView.hidden = YES;
}

- (void)removeSmartCorrectionView
{
    if (!self.smartCorrectionView.isTimeOut)
    {
        
    }
    self.smartCorrectionView.meldDelegate = nil;
    [self.smartCorrectionView removeFromSuperview];
    self.smartCorrectionView = nil;
    [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
    NSLog(@"UN-HIDE BUTTONS4");
    self.winnerAIImageView.hidden = YES;
}

-(void)cancelMeldAction{
    NSLog(@"CHECK CLOSE MELD");
    [self.meldViewTable setHidden:YES];
    self.winnerAIImageView.hidden = YES;
    [self updateShowButton:YES];
    //[self.playTable removeMeldGroupAllButton];
    [self.meldViewTable.firstMeldArray removeAllObjects];
    [self.meldViewTable.secondMeldArray removeAllObjects];
    [self.meldViewTable.thirdMeldArray removeAllObjects];
    [self.meldViewTable.fourthMeldArray removeAllObjects];
    //[self.playTable removeMeldCardButton];
    [self.meldViewTable unHideMeldSendCardButton];
    [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];//by ratheesh
    self.meldViewTable.sendCardButton.hidden = YES;
}

- (BOOL)playerShowsCard {
    if (self.meldViewTable){
        return YES;
    }
    return NO;
}

- (BOOL)canMeldCards
{
    return NO;
    NSLog(@"self.meldViewTable.sendCardButton.isHidden = %@",self.meldViewTable.sendCardButton.isHidden ? @"true" : @"false");
    if (self.meldViewTable)
    {
        if (self.meldViewTable.sendCardButton.isHidden)
        {
            return NO;
        }
        return YES;
    }
    return NO;
}

- (BOOL)getIsAnyMeldSlotEmpty
{
    if (self.meldViewTable)
    {
        if (self.meldViewTable.firstMeldArray.count !=0 && self.meldViewTable.secondMeldArray.count !=0 && self.meldViewTable.thirdMeldArray.count !=0 && self.meldViewTable.fourthMeldArray.count !=0 )
        {
            return NO;
        }
        else
            return YES;
    }
    return NO;
}

- (void)showMeldErrorPopup
{
    [self showInvalidPopup];
}

- (BOOL)getIsAnyMeldSlotEmptyInPlayerMode
{
    if (self.meldViewTable)
    {
        if (self.meldViewTable.firstMeldArray.count !=0 && self.meldViewTable.secondMeldArray.count !=0 && self.meldViewTable.thirdMeldArray.count !=0 && self.meldViewTable.fourthMeldArray.count !=0 )
        {
            return NO;
        }
        else
            return YES;
    }
    
    return NO;
}

- (void)showMeldErrorPopupInPlayerMode
{
    [self showInvalidPopup];
}

- (void)didMeldCards:(NSMutableArray *)array
{
    if (self.meldViewTable)
    {
        [self.meldViewTable fillMeldView:array];
    }
}

- (void)setCurrentSelectedCards:(NSMutableArray *)array
{
    if (self.meldViewTable)
    {
        [self.meldViewTable setCurrentSelectedCards:array];
    }
}

- (void)setCurrentSelectedCardsFromPlayerMode:(NSMutableArray *)array
{
    if (self.meldViewTable)
    {
        [self.meldViewTable setCurrentSelectedCards:array];
    }
}

- (void)disableUserToEnterMeldDuringAutoplay:(BOOL)boolValue
{
    self.isPlayerCanEnterGamePlayInAutoPlayMode = boolValue;
    //    DLog(@"melf iamback autoplay  %d",self
    //          .isPlayerCanEnterGamePlayInAutoPlayMode);
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(canAllowUserToGamePlayInAutoPlayMode)])
    {
        [self.gameTableDelegate canAllowUserToGamePlayInAutoPlayMode];
    }
}

#pragma mark - Joker Pop Table View -

- (void)createJokerPopTableView
{
    [self disableChairs];
    NSArray *nibContents;
    //joker table view not available
    int index = 0;
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJJokerViewNibName
                                                owner:nil
                                              options:nil];
    self.jokerPopTableView = nibContents[index];
    
    [self performSelector:@selector(setScreensizeofJokerPopTableView) withObject:nil afterDelay:0.3];
    
    //    CGRect frame = self.jokerPopTableView.frame;
    //    frame.origin.x = (self.view.frame.size.width/2) - (self.jokerPopTableView.frame.size.width/2);
    //
    //    frame.origin.y = ([TAJUtilities isIPhone]) ? JOKER_VIEW_TOP_OFFSET_IPHONE : JOKER_VIEW_TOP_OFFSET_IPAD;
    //    self.jokerPopTableView.frame = frame;
    
    //set joker table view delegate
    self.jokerPopTableView.jokerDelegate = self;
    //NSLog(@"GAME TYPE : %@",self.playTableDictionary);
    [self.jokerPopTableView updateJokerView:self.playTableDictionary[kTaj_Table_details] withBet:[self.buyInAmount intValue] gameType:APP_DELEGATE.gameType];
    [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 8");
}

- (void)setScreensizeofJokerPopTableView{
    [TAJAppDelegate setScreenSizeForRes: self.jokerPopTableView];
    [self.view addSubview:self.jokerPopTableView];
}

- (void)didSelectCancelJoker
{
    [self removeJokerTableView];
    [self unSelectChair];
    self.isRebuyin = NO;
    self.leaveButton.enabled = YES;
    //[self leaveTable]; //ratheesh
}

- (void)didSelectJoinJoker:(NSString *)buyInAmount
{
    // Commented by ratheesh
    
    [self removeJokerTableView];
    
    // Commented by ratheesh
    
    self.leaveButton.enabled = YES;
    if (!self.isRebuyin)
    {
        // Sit here tapped
        int placeID = self.jokerChairView.chairId;
        
        if (self.maxPlayers == TWO_PLAYER && placeID == 2)
        {
            placeID = 4;
        }
        
        [[TAJGameEngine sharedGameEngine] joinSeat:placeID tableID:self.currentTableId buyInAmount:buyInAmount middleJoin:self.isStrikesMiddleJoin];
        self.isSeatRequestPending = YES;
        [self disableChairs];
    }
    else if (self.isRebuyin)
    {
        [[TAJGameEngine sharedGameEngine] rebuyinWithTableId:self.currentTableId userID:self.thisPlayerModel.playerID rebuyinAmt:buyInAmount];
        self.isRebuyin = NO;
    }
}

- (void)removeJokerTableView
{
    if (self.jokerPopTableView)
    {
        self.jokerPopTableView.jokerDelegate = nil;
        [self.jokerPopTableView removeFromSuperview];
        self.jokerPopTableView = nil;
        [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
        NSLog(@"UN-HIDE BUTTONS5");

    }
}


- (void)removeRespectiveViewForJokerGame
{
    if (self.isJokerTypeGame)
    {
        [self removeJokerTableView];
        
        if (self.isSpectator)
        {
            //            [self unSelectChair];
        }
    }
    [self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
    NSLog(@"UN-HIDE BUTTONS6");
}

-(void)didJokerViewTextFieldStartEditing
{
    CGRect frame = self.jokerPopTableView.frame;
    frame.origin.x = (self.view.frame.size.width/2) - (self.jokerPopTableView.frame.size.width/2);
    
    frame.origin.y = ([TAJUtilities isIPhone]) ? JOKER_VIEW_TOP_OFFSET_IPHONE - 60.0 : JOKER_VIEW_TOP_OFFSET_IPAD - 30.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.jokerPopTableView.frame=frame;
    }];
}

-(void)didJokerViewTextFieldEndEditing
{
    CGRect frame = self.jokerPopTableView.frame;
    frame.origin.x = (self.view.frame.size.width/2) - (self.jokerPopTableView.frame.size.width/2);
    
    frame.origin.y = ([TAJUtilities isIPhone]) ? JOKER_VIEW_TOP_OFFSET_IPHONE  : JOKER_VIEW_TOP_OFFSET_IPAD ;
    [UIView animateWithDuration:0.25 animations:^{
        self.jokerPopTableView.frame=frame;
    }];
}

- (void)showChatErrorAlert:(NSString *)message
{
    [self showAlertMessageOnServerError:message];
}

#pragma mark - Winner Screen ViewController -

- (void)createWinnerScreen
{
    if (!self.winnerScreenController)
    {
        //        [self.showButton setTitle: @"Show" forState: UIControlStateNormal];
        self.winnerScreenController = [[TAJWinnerScreenViewController alloc]initWithNibName:@"TAJWinnerScreenViewController" bundle:nil];
        
        [TAJAppDelegate setScreenSizeForRes: self.winnerScreenController.view];
        [self.view addSubview:self.winnerScreenController.view];
        self.winnerScreenController.winnerDelegate = self;
    }
    
    [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
    NSLog(@"HIDE BUTTONS 9");
}

- (void)didWinnerScreenClosed
{
    [self removeWinnerScreenView];
}

- (void)removeWinnerScreenView
{
    if (self.winnerScreenController)
    {
        self.winnerScreenController.winnerDelegate = nil;
        [self.winnerScreenController.view removeFromSuperview];
        self.winnerScreenController = nil;
        //NSLog(@"UN-HIDE BUTTONS7");
    }
    //[self.gameTableDelegate resetAlphaValueOfLobbyAndTableButtons];
}

#pragma mark - RESTART SETUP -

- (void)resetControllerForGameResult
{
    [self.playTable restartGameSetupForGameResult];
}

- (void)refreshControllerForNewGameEnd
{
    [self restartControllerSetUp];
    // inform play table
    [self.playTable restartGameSetupForGameEnd];
    
#if PLAYER_MODEL_GAME_CARD
    // Player model reset
    [self.playerModelView resetPlayerModelView];
#endif
    
}

- (void)restartControllerSetUp
{
    [self.cardSlotManager removeAllSlotCards];
    [self removeAllDiscardCardHistoryContent];
    
    [self removeViewFromGamePlay];
    
    self.canShowAutoPlayView = NO;
    self.currentTurnChair = nil;
    self.isMyTurn = NO;
    self.isTossWinner = NO;
    self.isGameStarted = NO;
    self.isThisDeviceTurn = NO;
    if (self.scoreSheetViewController)
    {
        [self.scoreSheetViewController hideSplitButton];
    }
    //Timers
    self.playerTimeoutTime = 0.0;
    if ([self.playerTime isValid])
    {
        [self.playerTime invalidate];
    }
    
    if (self.myCardsDealArray)
    {
        [self.myCardsDealArray removeAllObjects];
    }
    
    if (self.closedDeckArray)
    {
        [self.closedDeckArray removeAllObjects];
    }
    
    [self updateDropButton];
    
    if (self.meldViewTable)
    {
        [self removeMeldView];
    }
}

- (void)hideViewForNewGame
{
    [self removeCardBgPlayerImage];
    [self resetAutoPlayViewArray];
    [self resetPlayerDealerArray];
    [self resetDropMessageForChair];
}

- (void)resetDropMessageForChair
{
    // refresh chair view once if any player is dropped
    for (UIView *view in self.chairsArray)
    {
        TAJChairView *eachChair = [self chairFromView:view];
        [eachChair hideDropMessage];
    }
}

- (void)refreshChairsOnGameDeschedule
{
    [self refreshChairView];
    [self updateChair];
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    
}

- (void)refreshChairView
{
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *playerCardImageViewNew;
        playerCardImageViewNew = self.dropCardImageViewArray[i];
        playerCardImageViewNew.image = nil;
        playerCardImageViewNew.hidden = YES;
    }
    
    // refresh chair view once if any player is dropped
    for (UIView *view in self.chairsArray)
    {
        TAJChairView *eachChair = [self chairFromView:view];
        if ([eachChair.playerModel.playerType isEqualToString:TABLE_LEAVE_TEXT])
        {
            DLog(@"chair cleared ");
            eachChair.playerModel = nil;
            eachChair.isOccupied = NO;
            if (!self.thisPlayerModel && [eachChair.playerModel.playerID isEqualToString:[[[TAJGameEngine sharedGameEngine] usermodel] userId]])
            {
                self.isSpectator = YES;
                self.buyInAmount = 0;
            }
            DLog(@"chair cleared %@ ",eachChair.playerModel.playerID);
        }
        [eachChair resetPlayerModel];
        [eachChair hideDropMessage];
        eachChair.dropBoxImageView.hidden = YES;
        eachChair.dropTextLabel.hidden = YES;
        DLog(@"chair normal  %@ ",eachChair.playerModel.playerID);
    }
}

- (void)updateChair
{
    for (int i = 0; i < self.maxPlayers; i++)
    {
        UIView *view = self.chairsArray[i];
        TAJChairView *aChair = nil;
        if (view.subviews && view.subviews.count > 0)
        {
            // Already added
            NSArray *subviews = [view subviews];
            aChair = (TAJChairView *)subviews[0];
            [aChair updateChairImagesOnGameStart];
        }
    }
}

- (void)hideChairView
{
    // refresh chair view once if any player is dropped
    for (UIView *view in self.chairsArray)
    {
        TAJChairView *eachChair = [self chairFromView:view];
        //[eachChair hideChairs];
        [eachChair hideDropMessage];
        // [eachChair playerQuitTable:YES];
        eachChair.dropBoxImageView.hidden = YES;
        eachChair.dropTextLabel.hidden = YES;
    }
    for(int i=0; i<6; i++)
    {
        UIImageView *imageView1= self.cardBackAnimationImageViewArray[i];
        UIImageView *imageView2= self.playerCardsBgImage[i];
        imageView1.hidden = YES;
        imageView1.image = nil;
        imageView2.hidden = YES;
        imageView2.image = nil;
    }
}

- (void)removeViewFromGamePlay
{
    if (self.viewHolderForChatDiscardLiveFeed.subviews && self.viewHolderForChatDiscardLiveFeed.subviews > 0)
    {
    }
}

- (void)removeCardBgPlayerImage
{
    for (int i = 0; i < self.cardBackAnimationImageViewArray.count; i++)
    {
        UIImageView *imageViewTwo = self.cardBackAnimationImageViewArray[i];
        imageViewTwo.hidden = YES;
    }
    
    for (int x = 0; x < self.playerCardsBgImage.count; x++)
    {
        UIImageView *imageView = self.playerCardsBgImage[x];
        imageView.hidden = YES;
    }
    
}

- (void)resetAutoPlayViewArray
{
    if (self.playersAutoPlayViewArray.count > 0)
    {
        for (UIView *view in self.playersAutoPlayViewArray)
        {
            view.hidden = YES;
        }
    }
}

- (void)resetPlayerDealerArray
{
    if (self.dealerPlayersArray.count > 0)
    {
        for (UIImageView *dealerImageView in self.dealerPlayersArray)
        {
            dealerImageView.hidden = YES;
        }
    }
}

#pragma mark - Error Code Process -

- (void)processErrorEvent:(int)errorCode
{
    NSLog(@"ERROR CODE : %d",errorCode);
    
    switch (errorCode)
    {
        case eOK:
            //200
            break;
            
        case eINTERNAL_ERROR:
            //500
            break;
            
        case eNO_SUCH_PLAYER:
            //401
            break;
            
        case eERR_DELETEING_SCHEDULE:
            //402
            break;
            
        case eNO_SUCH_SCHEDULE:
            //403
            break;
            
        case eNO_SUCH_TABLE:
            //404
            break;
            
        case eNO_SUCH_CHATROOM:
            //405
            break;
            
        case eNOT_ACCEPTABLE:
            //406
            break;
            
        case eNO_SUCH_EDITINGVALUES:
            //407
            break;
            
        case eCANT_DO_THIS_NO_MATCHING_CONDITION:
            //408
            break;
            
        case eALREADY_REQUESTED_EXTRATIME:
            //409
        {
            [self showAlertMessageOnServerError:locate(eALREADY_REQUESTED_EXTRATIME)];
        }
            break;
            
        case eNO_TABLE_WITH_SAME_CONSTRAINS:
            //410
            break;
            
        case eNO_SLOT_FOR_THIS_TABLE:
            //411
            break;
            
        case eDATABASE_ERROR:
            //460
            break;
            
        case eCONNECTION_WAS_NOT_ESTABLISHED_TO_DBSLAYER:
            //463
            break;
            
        case eEMPTY_SET_FROM_DATABASE:
            //464
            break;
            
        case eERROR_IN_GAP:
            //465
            break;
            
        case eERROR_NOT_FUNDED_PLAYER:
        {
            self.isSeatRequestPending = NO;
        }
            break;
            
        case eLOW_BALANCE:
        {
            //501
            // re enable chair once again
            self.isSeatRequestPending = NO;
            [self showAlertMessageOnServerError:ALERT_LOW_BALNCE_TITLE];
            NSLog(@"Server ERROR CHECK");
            [self errorUnSelectChair];
            [self inSufficientChips];//ratheesh
        }
            
            break;
            
        case eSTATE_BLOCKED: {
            NSLog(@"STATE BLOCKED");
            [self showAlertMessageOnServerError:STATE_BLOCKE_MESSAGE];
        }
            break;
        case eTABLE_FULL:
            //502
            break;
            
        case eMORE_THEN_MAXBUYIN:
        {
            //503
            // enable chair for player to take on another sit
            [self showAlertMessageOnServerError:ALERT_MAX_BUYINAMOUNT];
            [self errorUnSelectChair];
        }
            break;
            
        case ePLAYER_ALREADY_INPLAY:
            //504
            break;
            
        case ePLAYER_ALREADY_INVIEW:
            //505
            break;
            
        case eNOT_PROPER_TABLE_TYPE:
            //506
            break;
            
        case eNO_MIDDLE_JOIN:
            //510
            break;
            
        case eDontHaveCashOrLoyalty:
            //507
            break;
            
        case eMIDDLE_JOIN_ALREADY_REQUESTED:
            //508
            break;
            
        case eIMPROPER_CONTIDITION_FOR_REQUEST:
            //509
            break;
            
        case eFALSE_SPLIT_CONDITION:
            //701
            break;
            
        case ePLAYER_CANCEL_SPLIT:
            //702
            break;
            
        case eALREADY_SPLIT_REQUESTED:
            //703
            break;
            
        case eSEAT_ALREADY_TAKEN:
        {
            //801
            // enable chair for player to take on another sit
            // if he takened reserved sit
            
            //[self showAlertMessageOnServerError:locate(eSEAT_ALREADY_TAKEN)];
            [self errorUnSelectChair];
            NSLog(@"sit here taken seat");
            self.isSeatRequestPending = NO;
            [self sitHereAction];
        }
            
            break;
            
        case eERROR_IN_PLAYERADD:
            //802
            break;
            
        case eSEAT_RESERVED_TO_TAKE:
        {
            //803
            // enable chair for player to take on another sit
            // if he takened reserved sit
            [self errorUnSelectChair];
        }
            break;
            
        case eCANT_DO_RELOAD:
            //901
            break;
            
        case ePLAYER_CANNOTJOIN_INTO_THIS_TABLE:
            //469
            break;
            
        case eATTRIB_MISSING:
            //470
            break;
            
        default:
            
            break;
    }
}
- (void) showAlertPopupWithMessage:(NSString *)popupMessage
{
    if (!self.infoPopUpViewController)
    {
        
        self.infoPopUpViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:popupMessage];
        
        [self.infoPopUpViewController show];
        
        self.infoPopUpViewController.view.center = self.view.center;
    }
}

- (void)errorUnSelectChair
{
    [self unSelectChair];
    if (self.jokerChairView)
    {
        self.jokerChairView = nil;
    }
}

- (void)inSufficientChips {
    int type;
    
    if ([APP_DELEGATE.gameType isEqualToString:CASH_GAME_LOBBY]) {
        type = eDEPOSIT_Type;
    }
    else {
        type = eOK_Type;
    }
#if NEW_INFO_VIEW_IMPLEMENTATION
    
    self.noHandAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:INSUUFICIENT_CHIPS_TAG popupButtonType:type message:ALERT_LOW_BALNCE_TITLE];
#else
    self.noHandAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                      message:ALERT_LOW_BALNCE_TITLE
                                                     delegate:self
                                            cancelButtonTitle:OK_STRING
                                            otherButtonTitles: nil];
    self.insufficientChipsAlertView.tag = INSUUFICIENT_CHIPS_TAG;
    
#endif
    [self.noHandAlertView show];
    
}

#pragma mark - Settings Menu -

- (IBAction)settingsMenuButtonAction:(id)sender
{
    if (self.settingMenuListView)
    {
        [self removeSettingMenuListView];
    }
    else if (!self.settingMenuListView)
    {
        if(self.reportBugView)
        {
            [self removeReportBugView];
        }
        [self createSettingMenuListView];
    }
    
}

- (void)resetSettingMenuButton
{
    //    if ([TAJUtilities isIPhone])
    //    {
    //        [self.settingsMenuButton setBackgroundImage:[UIImage imageNamed:SETTING_BUTTON_UNSELECT_IMAGE_IPHONE] forState:UIControlStateNormal];
    //    }
    //    else
    //    {
    //        [self.settingsMenuButton setBackgroundImage:[UIImage imageNamed:SETTING_BUTTON_UNSELECT_IMAGE_IPAD] forState:UIControlStateNormal];
    //    }
}

- (void)createSettingMenuListView
{
    if (!self.settingMenuListView)
    {
        
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TAJMenuListView" owner:nil options:nil];
        self.settingMenuListView = [subviewArray objectAtIndex:0];
        
        CGRect frame = self.settingMenuListView.frame;
    //frame.origin.y=self.settingsMenuButton.frame.origin.y+self.settingsMenuButton.frame.size.height-4.0;
        
        frame.origin.y= 0;
        frame.size.height = [TAJUtilities screenHeight];
        
        if([TAJUtilities isIPhone])
        {
            //+ 45 added by ratheesh
            //            frame.origin.x = self.view.frame.size.width-self.settingMenuListView.frame.size.width + 45.0;
            frame.origin.x = self.view.frame.size.width-self.settingMenuListView.frame.size.width;
        }
        
        else
        {
            frame.origin.x = self.view.frame.size.width-self.settingMenuListView.frame.size.width - 15.0;
        }
        self.settingMenuListView.frame = frame;
        self.settingMenuListView.delegate = self;
        [self.settingMenuListView refreshSoundButton];
        [self.view addSubview:self.settingMenuListView];
    }
}

- (void)removeSettingMenuListView
{
    if (self.settingMenuListView)
    {
        [self resetSettingMenuButton];
        [self.settingMenuListView removeFromSuperview];
        self.settingMenuListView = nil;
        [self removeDiscardHistoryView];
    }
}

- (void)soundButtonPressedUserDefaults:(BOOL)boolValue
{
    [TAJUtilities saveToUserDefaults:[NSNumber numberWithBool:boolValue] forKey:USER_DEFAULT_SOUND];
}

// setting menu delegate
- (void)closeMenuButtonPressed
{
    [self removeSettingMenuListView];
}

- (void)vibrateButtonPressedUserDefaults:(BOOL)boolValue
{
    [TAJUtilities saveToUserDefaults:[NSNumber numberWithBool:boolValue] forKey:USER_DEFAULT_VIBRATE];
}

- (void)vibrateDevice
{
    if ([((NSString*)[TAJUtilities retrieveFromUserDefaultsForKey:USER_DEFAULT_VIBRATE]) boolValue])
    {
        //call vibrate function
        AudioServicesPlaySystemSound(kSystemSoundID_Vibrate);
    }
}

- (void)reportaBugButtonPressed
{
    if(self.settingMenuListView)
    {
        [self.settingMenuListView removeFromSuperview];
        self.settingMenuListView = nil;
    }
    
    if (self.reportBugView)
    {
        [self.reportBugView removeFromSuperview];
        self.reportBugView = nil;
    }
    
    if (!self.reportBugView)
    {
        [self createBugReportView];
    }
    
}

- (void)gameInformationPressed
{
    if(self.settingMenuListView)
    {
        [self.settingMenuListView removeFromSuperview];
        self.settingMenuListView = nil;
    }
    
    if (self.gameInformationView)
    {
        [self.gameInformationView removeFromSuperview];
        self.gameInformationView = nil;
    }
    if (!self.gameInformationView)
    {
        [self creategameInformationView];
    }
    
}

- (void)scoreBoardButtonPressed{
    [self removeRespectiveViewForJokerGame];
    [self.settingMenuListView removeFromSuperview];
    self.isMenuListShowing=NO;
    
    if (self.boardScoreController && ![self.boardScoreController isShowing])
    {
        //[self.boardScoreController quitUserIds:self.quitUsers];
        if ([self.boardScoreController isDataExist])
        {
            [self.boardScoreController reloadScoreBoardPoints];
            [TAJAppDelegate setScreenSize: self.boardScoreController.view fromView: self.view];
            [self.view addSubview:self.boardScoreController.view];
            [self.gameTableDelegate changeAlphaValueOfLobbyAndTableButtons];
            NSLog(@"HIDE BUTTONS 10");
        }
        else
        {
            if (!self.noEntryAlertView)
            {
                
#if NEW_INFO_VIEW_IMPLEMENTATION
                self.noEntryAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_NO_LAST_ENTRIES_TAG popupButtonType:eOK_Type message:SCORE_BOARD_NO_ENTRY];
#else
                self.noEntryAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE message:SCORE_BOARD_NO_ENTRY delegate:self cancelButtonTitle:OK_STRING otherButtonTitles: nil];
                self.noEntryAlertView.tag = ALERT_NO_LAST_ENTRIES_TAG;
#endif
                [self.noEntryAlertView show];
            }
        }
    }
    else
    {
        self.scoreBoardLabel.textColor = [UIColor whiteColor];
    }
}

- (void)discardHistoryButtonPressed {
    [self discardHistoryView];
}

- (void)lastHandButtonPressed
{
    [self removeRespectiveViewForJokerGame];
    
    if (self.scoreSheetViewController && ![self.scoreSheetViewController isShowing])
    {
        if ([self.scoreSheetViewController isDataExist])
        {
            [self showScoreSheet];
            self.scoreSheetViewController.winner_AIImageView.hidden = YES;
            [self.scoreSheetViewController reloadScoreSheet];
        }
    }
    else
    {
        // Show alert here last hand not available
        if (!self.noHandAlertView)
        {
#if NEW_INFO_VIEW_IMPLEMENTATION
            
            self.noHandAlertView= [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_NO_LAST_HAND_TAG popupButtonType:eOK_Type message:LAST_HAND_NO_ENTRY];
#else
            self.noHandAlertView = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                              message:LAST_HAND_NO_ENTRY
                                                             delegate:self
                                                    cancelButtonTitle:OK_STRING
                                                    otherButtonTitles: nil];
            self.noHandAlertView.tag = ALERT_NO_LAST_HAND_TAG;
            
#endif
            [self.noHandAlertView show];
        }
    }
}

#pragma mark - Report a bug View-

-(void)createBugReportView
{
    if (!self.reportBugView)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"TAJReportaBugView" owner:nil options:nil];
        self.reportBugView = [subviewArray objectAtIndex:0];
        CGRect frame = self.reportBugView.frame;
        frame.origin.y = 0.0;
        
        [self.reportBugView initializeTheBugReport:self.playTableDictionary withGameId:self.gameIDString];
        self.reportBugView.frame=frame;
        [TAJAppDelegate setScreenSizeForRes: self.reportBugView];
        self.reportBugView.bugDelecate=self;
        [self.view addSubview:self.reportBugView];
        
        if (self.chatViewController && [self.chatViewController isChatViewShowing])
        {
            CGRect oldframe = self.viewHolderForChatDiscardLiveFeed.frame;
            oldframe.origin.y += self.keyBoardHig;
            [self dropDownTheViewHolderForChat:oldframe];
        }
    }
}

-(void)creategameInformationView
{
    if (!self.gameInformationView)
    {
        NSArray *subviewArray = [[NSBundle mainBundle] loadNibNamed:@"GameInformationView" owner:nil options:nil];
        self.gameInformationView = [subviewArray objectAtIndex:0];
        CGRect frame = self.gameInformationView.frame;
        frame.origin.y = 0.0;
        
        [self.gameInformationView initializeTheGameInformation:self.playTableDictionary withGameId:self.gameIDString];
        self.gameInformationView.frame=frame;
        [TAJAppDelegate setScreenSizeForRes: self.gameInformationView];
        self.gameInformationView.gameInformationDelegate=self;
        [self.view addSubview:self.gameInformationView];
        
    }
}

// Report a bug delegate
- (void)sumbitButtonClickOnReportaBug:(NSString *)tableID withGameID:(NSString *)gameID withUserData:(NSString *)explanation onGameType:(NSString *)gameType haveBugType:(NSString *)bugType
{
    [self removeReportBugView];
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * deviceType,*gameId;
    
    if ([TAJUtilities isIPhone])
    {
        deviceType = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceType = @"Tablet";//@"IPAD";
    }
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    //@"message" :explanation - old
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    gameId = [TAJUtilities checkIfStringIsEmpty:gameID];
    
    NSDictionary *params = @{@"tableid":tableID,
                             @"gametype":gameType,
                             @"bugtype":bugType,
                             @"message" :@"",
                             @"gameid" :gameId,
                             @"device_type" :deviceType,
                             @"client_type" :@"iOS",
                             @"version" :buildNumber
                             };
#if DEBUG
    NSLog(@"REPORT BUG PARAMS: %@", params);
#endif
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"REPORT BUG header: %@", header);
#endif
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,REPORTBUG];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
#if DEBUG
                        NSLog(@"REPORT RESPONSE: %@", jsondata);
#endif
                        //[self.view makeToast:[jsondata valueForKey:@"message"]];
                        self.viewReportBug.hidden = NO;
                    }
                    else {
                        NSLog(@"REPORT REST Error :%@",[jsondata valueForKey:@"message"]);
                        [self.view makeToast:[jsondata valueForKey:@"message"]];
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                [self.view makeToast:@"Error while fetching data."];
            }
        });
    }];
}

- (void)loadGameRulesWebPageFromReportBug
{
    if (self.gameTableDelegate && [self.gameTableDelegate respondsToSelector:@selector(loadGameRulesWebPage)])
    {
        [self.gameTableDelegate loadGameRulesWebPage];
    }
    [self resetSettingMenuButton];
    [self removeReportBugView];
}

- (void)didReportaBugTextFieldStartEditing
{
    CGRect frame = self.reportBugView.frame;
    frame.origin.y -= 60.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.reportBugView.frame=frame;
    }];
    
    if (self.chatViewController && [self.chatViewController isChatViewShowing])
    {
        CGRect oldframe = self.viewHolderForChatDiscardLiveFeed.frame;
        oldframe.origin.y += self.keyBoardHig / 2;
        
        [self dropDownTheViewHolderForChat:oldframe];
    }
}

-(void)didReportaBugTextFieldEndEditing
{
    CGRect frame = self.reportBugView.frame;
    frame.origin.y += 60.0;
    [UIView animateWithDuration:0.25 animations:^{
        self.reportBugView.frame=frame;
    }];
    
    
    if (self.chatViewController && [self.chatViewController isChatViewShowing])
    {
        CGRect oldframe = self.viewHolderForChatDiscardLiveFeed.frame;
        oldframe.origin.y += self.keyBoardHig / 2;
        
        [self dropDownTheViewHolderForChat:oldframe];
    }
    
}

-(void)bugReportCloseButton_p
{
    [self removeReportBugView];
    [self resetSettingMenuButton];
}

-(void)gameInfoCloseButton_p {
    [self removeGameInfoView];
    [self resetSettingMenuButton];
    
}

- (void)removeReportBugView
{
    if (self.reportBugView)
    {
        [self.reportBugView.bugReportTextField resignFirstResponder];
        [self.reportBugView removeFromSuperview];
        self.reportBugView = nil;
    }
}

- (void)removeGameInfoView
{
    if (self.gameInformationView)
    {
        [self.gameInformationView removeFromSuperview];
        self.gameInformationView = nil;
    }
}


#pragma mark - Card Slots Helper -

- (void)removeCardSlotAtIndex:(NSInteger)index
{
    NSMutableDictionary *cardInfo = nil;
    cardInfo = self.slotCardInfoArray[index];
    if ([cardInfo isKindOfClass:[NSDictionary class]])
    {
        [self.slotCardInfoArray removeObjectAtIndex:index];
    }
}

- (void)fillSlotCardArray:(NSArray *)array
{
    //    DLog(@"slot fill gametable %@",array);
    self.slotCardInfoArray = [NSMutableArray arrayWithCapacity:SLOT_ARRAY_CAPACITY];
    
    [self fillNullObjectsForSlotArray:self.slotCardInfoArray];
    
    [self prepareSlotCardArray:array];
    
    [self.cardSlotManager initializeSlotArray:self.slotCardInfoArray];
    //    DLog(@"slot fill slot array in slotmanager gametable %@",[self.cardSlotManager getSlotCardsArray]);
    // Update table cards
    [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
    
#if PLAYER_MODEL_GAME_CARD
    // Update player model cards
    [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
#endif
    
}

- (void)prepareSlotCardArray:(NSArray *)objects
{
    [self insertObjects:objects atIndex:FILL_ARRAY_FROM_INDEX];
}

- (void)insertObjects:(NSArray *)objects atIndex:(NSUInteger)index
{
    [self.slotCardInfoArray insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, [objects count])]];
}

- (void)fillNullObjectsForSlotArray:(NSMutableArray *)array
{
    for (NSUInteger i = 0; i < SLOT_ARRAY_CAPACITY; i++)
    {
        array[i] = [NSNull null];
    }
}

- (void)slotCardArray:(NSMutableArray *)slotArray withTableCardsArray:(NSMutableArray *)tableArray
{
    if (slotArray && tableArray)
    {
        self.slotCardInfoArray = [NSMutableArray arrayWithArray:slotArray];
        
        self.myCardsDealArray = [NSMutableArray arrayWithArray:tableArray];
        NSMutableDictionary *cardInformation = [NSMutableDictionary dictionary];
        [cardInformation setObject:self.currentTableId forKey:TAJ_TABLE_ID];
        [cardInformation setObject:self.myCardsDealArray forKey:SERVER_SLOT_ARRAY_KEY];
        
        [[TAJGameEngine sharedGameEngine] addSlotsInfoForHeartBeat:cardInformation];
    }
}

#pragma mark - PlayerModel View -

- (void)createPlayerModelView
{
    if (!self.playerModelView)
    {
#if PLAYER_MODEL_GAME_CARD
        NSArray *nibContents;
        // table not available
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJPlayerModelViewNibName
                                                    owner:nil
                                                  options:nil];
        self.playerModelView = nibContents[index];
        [self.playerModelViewHolder addSubview:self.playerModelView];
        
        // Set this controller as delegate
        self.playerModelView.modelDelegate = self;
#else
        self.playerModelButton.hidden = YES;
#endif
    }
}

- (void)didSelectArrowButton
{
    if (self.isPlayerModelSlided)
    {
        self.isPlayerModeButtonClicked = NO;
        [self slideDownPlayerModel];
        [self.view insertSubview:self.chair1 belowSubview:self.playerModelViewHolder];
        [self.view insertSubview:self.playerModelViewHolder aboveSubview:self.dealerPlayerOne];
    }
}

/*
 - (IBAction)playerModelButtonAction:(UIButton *)sender
 {
 [self.playerModelView removeAllModelButtons];
 [self.playTable removeAllButtons];
 TAJChairView* chairAtPosition2,*chairAtPosition3;
 if (self.maxPlayers > 2)
 {
 UIView* viewAtChair2 = [self chairHolderAtPlaceId:(NSInteger)2];
 chairAtPosition2 = [self chairFromView:viewAtChair2];
 
 UIView* viewAtChair3 = [self chairHolderAtPlaceId:(NSInteger)3];
 chairAtPosition3 = [self chairFromView:viewAtChair3];
 
 }
 
 if (!self.isPlayerModelSlided)
 {
 if (self.maxPlayers >2)
 {
 CGRect chairRect2 = chairAtPosition2.deviceImageView.frame;
 CGRect chairRect3 = chairAtPosition3.deviceImageView.frame;
 
 CGRect chairRectNew2;
 CGRect chairRectNew3;
 if([TAJUtilities isIPhone])
 {
 chairRectNew2 = CGRectMake(1, chairRect2.origin.y, chairRect2.size.width, chairRect2.size.height);
 chairRectNew3 = CGRectMake(1, chairRect3.origin.y, chairRect3.size.width, chairRect3.size.height);
 }
 else
 {
 chairRectNew2 = CGRectMake(6, chairRect2.origin.y, chairRect2.size.width, chairRect2.size.height);
 chairRectNew3 = CGRectMake(6, chairRect3.origin.y, chairRect3.size.width, chairRect3.size.height);
 }
 [chairAtPosition2.deviceImageView setFrame:chairRectNew2];
 [chairAtPosition3.deviceImageView setFrame:chairRectNew3];
 }
 [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
 [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]];
 self.isPlayerModelSlided = YES;
 self.isPlayerModeButtonClicked = YES;
 [self.playTable enableUserTouchForPlayerModel:NO];
 
 CGSize screenSize = [[UIScreen mainScreen] bounds].size;
 float posY = screenSize.width - (self.playerModelViewHolder.frame.size.height + self.sortButton.frame.size.height + 4);
 
 [self.playerModelViewHolder setFrame:CGRectMake(self.view.frame.origin.x, posY,
 self.playerModelViewHolder.frame.size.width,self.playerModelViewHolder.frame.size.height)];
 
 //        if ([TAJUtilities isIPhone])
 //        {
 //            [self.playerModelViewHolder setFrame:CGRectMake(self.view.frame.origin.x, ([TAJUtilities isItIPhone5] ? 140 : 145),
 //                                                            self.playerModelViewHolder.frame.size.width,self.playerModelViewHolder.frame.size.height)];
 //        }
 //        else
 //        {
 //            [self.playerModelViewHolder setFrame:CGRectMake(self.view.frame.origin.x, 343,
 //                                                            self.playerModelViewHolder.frame.size.width,self.playerModelViewHolder.frame.size.height)];
 //        }
 if ([TAJUtilities isIPhone])
 {
 if ([TAJUtilities isItIPhone5])
 {
 [self.chair2 setFrame:CGRectMake(self.chair2.frame.origin.x, (self.chair3.frame.origin.y+self.chair3.frame.size.height)*1.06,
 self.chair2.frame.size.width,self.chair2.frame.size.height)];
 [self.chair6 setFrame:CGRectMake(self.chair6.frame.origin.x, (self.chair5.frame.origin.y+self.chair5.frame.size.height)*1.06,
 self.chair6.frame.size.width,self.chair6.frame.size.height)];
 [self.dealerPlayerTwo setFrame:CGRectMake(self.dealerPlayerTwo.frame.origin.x,  ([TAJUtilities isItIPhone5] ? 137 : 146),
 self.dealerPlayerTwo.frame.size.width,self.dealerPlayerTwo.frame.size.height)];
 [self.autoPlayPlayerSecondView setFrame:CGRectMake(self.autoPlayPlayerSecondView.frame.origin.x, ([TAJUtilities isItIPhone5] ? 151 : 159),
 self.autoPlayPlayerSecondView.frame.size.width,self.autoPlayPlayerSecondView.frame.size.height)];
 [self.dealerPlayerSix setFrame:CGRectMake(self.dealerPlayerSix.frame.origin.x, ([TAJUtilities isItIPhone5] ? 137 : 146),
 self.dealerPlayerSix.frame.size.width,self.dealerPlayerSix.frame.size.height)];
 [self.autoPlayPlayerSixthView setFrame:CGRectMake(self.autoPlayPlayerSixthView.frame.origin.x, ([TAJUtilities isItIPhone5] ? 151 : 159),
 self.autoPlayPlayerSixthView.frame.size.width,self.autoPlayPlayerSixthView.frame.size.height)];
 }
 else
 {
 [self.chair2 setFrame:CGRectMake(self.chair2.frame.origin.x, (self.chair3.frame.origin.y+self.chair3.frame.size.height)*1.025,
 self.chair2.frame.size.width,self.chair2.frame.size.height)];
 [self.chair6 setFrame:CGRectMake(self.chair6.frame.origin.x, (self.chair5.frame.origin.y+self.chair5.frame.size.height)*1.025,
 self.chair6.frame.size.width,self.chair6.frame.size.height)];
 [self.dealerPlayerTwo setFrame:CGRectMake(self.dealerPlayerTwo.frame.origin.x,  ([TAJUtilities isItIPhone5] ? 137 : 143),
 self.dealerPlayerTwo.frame.size.width,self.dealerPlayerTwo.frame.size.height)];
 [self.autoPlayPlayerSecondView setFrame:CGRectMake(self.autoPlayPlayerSecondView.frame.origin.x, ([TAJUtilities isItIPhone5] ? 151 : 155),
 self.autoPlayPlayerSecondView.frame.size.width,self.autoPlayPlayerSecondView.frame.size.height)];
 [self.dealerPlayerSix setFrame:CGRectMake(self.dealerPlayerSix.frame.origin.x, ([TAJUtilities isItIPhone5] ? 137 : 143),
 self.dealerPlayerSix.frame.size.width,self.dealerPlayerSix.frame.size.height)];
 [self.autoPlayPlayerSixthView setFrame:CGRectMake(self.autoPlayPlayerSixthView.frame.origin.x, ([TAJUtilities isItIPhone5] ? 151 : 155),
 self.autoPlayPlayerSixthView.frame.size.width,self.autoPlayPlayerSixthView.frame.size.height)];
 }
 
 }
 else
 {
 [self.chair2 setFrame:CGRectMake(self.chair2.frame.origin.x, (self.chair3.frame.origin.y+self.chair3.frame.size.height)*1.02,
 self.chair2.frame.size.width,self.chair2.frame.size.height)];
 [self.chair6 setFrame:CGRectMake(self.chair6.frame.origin.x, (self.chair5.frame.origin.y+self.chair5.frame.size.height)*1.02,
 self.chair6.frame.size.width,self.chair6.frame.size.height)];
 [self.dealerPlayerTwo setFrame:CGRectMake(self.dealerPlayerTwo.frame.origin.x,335,
 self.dealerPlayerTwo.frame.size.width,self.dealerPlayerTwo.frame.size.height)];
 [self.autoPlayPlayerSecondView setFrame:CGRectMake(self.autoPlayPlayerSecondView.frame.origin.x,354,
 self.autoPlayPlayerSecondView.frame.size.width,self.autoPlayPlayerSecondView.frame.size.height)];
 
 [self.dealerPlayerSix setFrame:CGRectMake(self.dealerPlayerSix.frame.origin.x,335,
 self.dealerPlayerSix.frame.size.width,self.dealerPlayerSix.frame.size.height)];
 [self.autoPlayPlayerSixthView setFrame:CGRectMake(self.autoPlayPlayerSixthView.frame.origin.x,354,
 self.autoPlayPlayerSixthView.frame.size.width,self.autoPlayPlayerSixthView.frame.size.height)];
 
 
 }
 NSUInteger playerPosition = [self getPlaceIdFromChairUserID:self.currentUserId];
 
 if(self.currentUserId  && playerPosition != 1)
 {
 if(!self.isMyTurn)
 {
 [self setPositionForUserIdPlayerTurnView:self.currentUserId];
 }
 }
 
 //        [UIView commitAnimations];
 
 if(self.currentUserId)
 {
 if(self.isMyTurn || playerPosition == 1)
 {
 [self setPositionForUserIdPlayerTurnView:self.currentUserId];
 }
 }
 
 [self.view insertSubview:self.chair1 aboveSubview:self.playerModelViewHolder];
 [self.view insertSubview:self.dealerPlayerOne aboveSubview:self.chair1];
 [self.view insertSubview:self.playerTurnView aboveSubview:self.playerModelViewHolder];
 if (self.isMyTurn)
 {
 [self.view insertSubview:self.extraTimeButton aboveSubview:self.playerTurnView];
 [self.view insertSubview:self.extraTimeLabel aboveSubview:self.extraTimeButton];
 [self.view insertSubview:self.extraTimeBg belowSubview:self.playerTurnView];
 }
 else
 {
 [self.view insertSubview:self.extraTimeButton aboveSubview:self.playerModelViewHolder];
 [self.view insertSubview:self.extraTimeLabel aboveSubview:self.extraTimeButton];
 [self.view insertSubview:self.extraTimeBg belowSubview:self.self.extraTimeButton];
 }
 
 if (![TAJUtilities isIPhone])
 {
 [self.playerModelButton setBackgroundImage:[UIImage imageNamed:@"playermodel_arrow_down.png"] forState:UIControlStateNormal];
 }
 
 }
 else if (self.isPlayerModelSlided)
 {
 self.isPlayerModeButtonClicked = NO;
 [self.playerModelView updatePlayerModelCards:[self.cardSlotManager getSlotCardsArray] withJokerCardID:self.jokerCardId];
 [self slideDownPlayerModel];
 [self.view insertSubview:self.chair1 belowSubview:self.playerModelViewHolder];
 [self.view insertSubview:self.playerModelViewHolder aboveSubview:self.dealerPlayerOne];
 [self.view insertSubview:self.dealerPlayerOne aboveSubview:self.chair1];
 
 [self.view insertSubview:self.playerTurnView aboveSubview:self.playerModelViewHolder];
 if (self.isMyTurn)
 {
 [self.view insertSubview:self.extraTimeButton aboveSubview:self.playerTurnView];
 [self.view insertSubview:self.extraTimeLabel aboveSubview:self.extraTimeButton];
 [self.view insertSubview:self.extraTimeBg belowSubview:self.playerTurnView];
 }
 else
 {
 [self.view insertSubview:self.extraTimeButton aboveSubview:self.playerModelViewHolder];
 [self.view insertSubview:self.extraTimeLabel aboveSubview:self.extraTimeButton];
 [self.view insertSubview:self.extraTimeBg belowSubview:self.self.extraTimeButton];
 }
 }
 
 }
 */

- (void)slideDownPlayerModel
{
    [self.playerModelView hideInfoLabelAndExtraLabelAndImageViewPlayerMode];
    TAJChairView* chairAtPosition2,*chairAtPosition3;
    if (self.maxPlayers > 2)
    {
        UIView* viewAtChair2 = [self chairHolderAtPlaceId:(NSInteger)2];
        chairAtPosition2 = [self chairFromView:viewAtChair2];
        
        UIView* viewAtChair3 = [self chairHolderAtPlaceId:(NSInteger)3];
        chairAtPosition3 = [self chairFromView:viewAtChair3];
        
        CGRect chairRect2 = chairAtPosition2.deviceImageView.frame;
        CGRect chairRect3 = chairAtPosition3.deviceImageView.frame;
        
        CGRect chairRectNew2;
        CGRect chairRectNew3;
        if([TAJUtilities isIPhone])
        {
            chairRectNew2 = CGRectMake(41, chairRect2.origin.y, chairRect2.size.width, chairRect2.size.height);
            chairRectNew3 = CGRectMake(41, chairRect3.origin.y, chairRect3.size.width, chairRect3.size.height);
            
        }
        else
        {
            chairRectNew2 = CGRectMake(101, chairRect2.origin.y, chairRect2.size.width, chairRect2.size.height);
            chairRectNew3 = CGRectMake(101, chairRect3.origin.y, chairRect3.size.width, chairRect3.size.height);
        }
    }
    
    [self.playerModelView removeAllModelButtons];
    [self.playTable removeAllButtons];
    if (![TAJUtilities isIPhone])
    {
        [self.playerModelButton setBackgroundImage:[UIImage imageNamed:@"playermodel_arrow.png"] forState:UIControlStateNormal];
    }
    self.isPlayerModelSlided = NO;
    [self.playTable enableUserTouchForPlayerModel:YES];
    [self.playerModelView removeAllModelButtons];
    if (!self.isGameEnded && !self.didSendMeldCards)
    {
        [self.playTable updateSlotCardsOnMyDeck:[self.cardSlotManager getSlotCardsArray]]; // this to passing if game end whenever you slide down the old cards to be updated
    }
    
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    float posY = screenSize.width;
    
    [self.playerModelViewHolder setFrame:CGRectMake(self.view.frame.origin.x, posY,self.playerModelViewHolder.frame.size.width,self.playerModelViewHolder.frame.size.height)];
    
    if ([TAJUtilities isIPhone])
    {
        [self.chair2 setFrame:CGRectMake(self.chair2.frame.origin.x, self.chair2.frame.origin.y, self.chair2.frame.size.width, self.chair2.frame.size.height)];
        
        [self.chair6 setFrame:CGRectMake(self.chair6.frame.origin.x, self.chair6.frame.origin.y, self.chair6.frame.size.width, self.chair6.frame.size.height)];
       
        [self.dealerPlayerTwo setFrame:CGRectMake(self.dealerPlayerTwo.frame.origin.x, self.dealerPlayerTwo.frame.origin.y, self.dealerPlayerTwo.frame.size.width, self.dealerPlayerTwo.frame.size.height)];
        
        [self.autoPlayPlayerSecondView setFrame:CGRectMake(self.autoPlayPlayerSecondView.frame.origin.x, self.autoPlayPlayerSecondView.frame.origin.y, self.autoPlayPlayerSecondView.frame.size.width, self.autoPlayPlayerSecondView.frame.size.height)];
        
        [self.dealerPlayerSix setFrame:CGRectMake(self.dealerPlayerSix.frame.origin.x, self.dealerPlayerSix.frame.origin.y, self.dealerPlayerSix.frame.size.width,self.dealerPlayerSix.frame.size.height)];
     
        [self.autoPlayPlayerSixthView setFrame:CGRectMake(self.autoPlayPlayerSixthView.frame.origin.x,self.autoPlayPlayerSixthView.frame.origin.y, self.autoPlayPlayerSixthView.frame.size.width, self.autoPlayPlayerSixthView.frame.size.height)];
    }
    else
    {
        [self.chair2 setFrame:CGRectMake(self.chair2.frame.origin.x, self.chair2.frame.origin.y, self.chair2.frame.size.width, self.chair2.frame.size.height)];
        [self.chair6 setFrame:CGRectMake(self.chair6.frame.origin.x, self.chair6.frame.origin.y, self.chair6.frame.size.width, self.chair6.frame.size.height)];
        [self.dealerPlayerTwo setFrame:CGRectMake(self.dealerPlayerTwo.frame.origin.x, self.dealerPlayerTwo.frame.origin.y, self.dealerPlayerTwo.frame.size.width, self.dealerPlayerTwo.frame.size.height)];
        [self.autoPlayPlayerSecondView setFrame:CGRectMake(self.autoPlayPlayerSecondView.frame.origin.x, self.autoPlayPlayerSecondView.frame.origin.y, self.autoPlayPlayerSecondView.frame.size.width, self.autoPlayPlayerSecondView.frame.size.height)];
        
        [self.dealerPlayerSix setFrame:CGRectMake(self.dealerPlayerSix.frame.origin.x,self.dealerPlayerSix.frame.origin.y, self.dealerPlayerSix.frame.size.width, self.dealerPlayerSix.frame.size.height)];
        [self.autoPlayPlayerSixthView setFrame:CGRectMake(self.autoPlayPlayerSixthView.frame.origin.x, self.autoPlayPlayerSixthView.frame.origin.y, self.autoPlayPlayerSixthView.frame.size.width, self.autoPlayPlayerSixthView.frame.size.height)];
        
    }
    
    NSUInteger playerPosition = [self getPlaceIdFromChairUserID:self.currentUserId];
    
    if(self.currentUserId && playerPosition!=1)
    {
        if(!self.isMyTurn)
        {
            [self setPositionForUserIdPlayerTurnView:self.currentUserId];
        }
    }
    
    if(self.currentUserId || playerPosition == 1)
    {
        if(self.isMyTurn)
        {
            [self setPositionForUserIdPlayerTurnView:self.currentUserId];
        }
    }
}

#pragma mark - PlayerModel View Delegate -

- (BOOL)canModelMeldCard
{
    return [self canMeldCards];
}

- (void)swapModelCardIndex:(NSUInteger)cardAIndex withAnothCardIndex:(NSUInteger)cardBIndex
{
    [self swapCard:cardAIndex withCard:cardBIndex];
}

- (void)resetCardSlotsPlayerMode
{
    [self.cardSlotManager resetCardSlotsInSlotManager];
}

- (void)groupCardsOnModelAtIndex:(NSMutableArray *)indexArray
{
    [self groupCardsAtIndexes:indexArray];
}

- (void)canModelEnableShowButton:(BOOL)boolValue withDiscardCardInfo:(NSDictionary *)discardCardInfo
{
    [self didEnableShowButton:boolValue withDiscardInfo:discardCardInfo];
}

- (void)didDiscardCardOnModelAtIndex:(NSUInteger)discardCardIndex withCard:(TAJPlayerModelCard *)selectedCard
{
#if PLAYER_MODEL_GAME_CARD_ANIMATION
    NSDictionary *cardInfo = self.slotCardInfoArray[discardCardIndex];
    
    NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
    NSString *cardSuit = cardInfo[kTaj_Card_Suit_Key];
    NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, [cardSuit uppercaseString]];
    
    NSArray *nibContents;
    // Place new card
    int index = 0;
    
    if ([TAJUtilities isIPhone])
    {
        index = [TAJUtilities isItIPhone5]? 0 : 1;
    }
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJCardViewNibName
                                                owner:nil
                                              options:nil];
    
    TAJPlayerModelCard *card = nibContents[index];
    
    card.frame=selectedCard.frame;
    [card showCard:cardId isMomentary:YES];
    
    [self.playerModelView.myDeckHolder insertSubview:card belowSubview:selectedCard];
    
    if ([TAJUtilities isIPhone])
    {
        card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.95f, 0.6f);
    }
    else
    {
        card.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.8f, 0.8f);
    }
    
    [UIView animateWithDuration:0.08
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:
     ^{
         CGRect finalrect=[self.playerModelView.myDeckHolder convertRect:self.playTable.openDeckHolder.frame fromView:self.playTable];
         if ([TAJUtilities isIPhone])
         {
             finalrect.origin.y -= 15;
             finalrect.origin.x += 5;
             finalrect.size.width = 40;
             finalrect.size.height = 60;
         }
         card.frame=finalrect;
     }
                     completion:^(BOOL finished)
     {
         [card removeFromSuperview];
     }];
#endif
    [self didDiscardedCardAtIndex:discardCardIndex];
    
}

- (void)didMeldCardsOnModel:(NSMutableArray *)array
{
    [self didMeldCards:array];
}

- (void)removeDiscardedShowCardForMeldSetup:(NSUInteger)showCardIndex
{
    [self removeDiscardedCardForMeldSetup:showCardIndex];
}

- (void)placeDiscardCardFromModelToOpenDeck:(NSString *)cardID
{
    [self.playTable placeCardInOpenDeck:cardID];
}

-(void)showInvalidPopup
{
    if (!self.infoPopUpController)
    {
        self.infoPopUpController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:      ALERT_MELD_INVALID popupButtonType:eOK_Type message:CANNOT_MELD_TEXT];
        
        [self.infoPopUpController show];
    }
}

-(void)showAlertMessageOnServerError:(NSString *)alertMessage
{
    if (!self.errorInfoPopupViewController)
    {
        self.errorInfoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_ERROR popupButtonType:eOK_Type message:alertMessage];
        
        [self.errorInfoPopupViewController show];
    }
}

- (NSString*)convertToDate:(NSString*)dateString {
    return [self convertToFormat:dateString format:@"EEE-dd MMM hh:mm a"];
}

- (NSString*)convertToTime:(NSString*)dateString {
    return [self convertToFormat:dateString format:@"hh:mm"];
}

- (NSString*)convertToTimeWithMeridiem:(NSString*)dateString{
    return [self convertToFormat:dateString format:@"hh:mm a"];
}

- (NSString*)convertToFormat:(NSString*)dateString format:(NSString*)formatString{
    NSDate* date = [NSDate dateWithTimeIntervalSince1970:[dateString longLongValue]];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    NSLocale *locale = [[NSLocale alloc] initWithLocaleIdentifier:@"en_US_POSIX"];
    [formatter setLocale:locale];
    [formatter setDateFormat:formatString];
    NSString *convertedString = [formatter stringFromDate:date];
    return convertedString;
}

- (void)tournamentLevelSchedule:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
}

- (void)tournamentLevelStart:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [[TAJGameEngine sharedGameEngine] requestGetLevelTimer:dictionary[@"TAJ_tournament_id"] msgUUID:dictionary[@"TAJ_msg_uuid"]];
    
}

- (void)tournamentLevelEnd:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
}

- (void)tournamentGetLeveltimer:(NSNotification *) notification{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    levelTimer = [dictionary[@"TAJ_leveltimer"] doubleValue];
    [self stopTimerLevel];
    [self updateTimer];
    [self startTimer];
    runningLevelLbl.text = dictionary[@"TAJ_leveldetails"];
}

- (void)stopTimerLevel{
    [stopWatchTimer invalidate];
    NSLog(@"CHECKING TIMER STOP");
    stopWatchTimer = nil;
}

- (void)startTimer{
    stopWatchTimer = [NSTimer scheduledTimerWithTimeInterval:1.0
                                                      target:self
                                                    selector:@selector(updateTimer)
                                                    userInfo:nil
                                                     repeats:YES];
    [[NSRunLoop currentRunLoop] addTimer:stopWatchTimer
                                 forMode:NSRunLoopCommonModes];
}

- (void)updateTimer{
    NSString *timestamp = [NSString stringWithFormat:@"%f", [[NSDate new] timeIntervalSince1970]];
    double current = [timestamp doubleValue];
    NSTimeInterval difference = [[NSDate dateWithTimeIntervalSince1970:levelTimer] timeIntervalSinceDate:[NSDate dateWithTimeIntervalSince1970:current]];
    runningTimerLbl.text = [self stringFromTimeInterval:difference];
    //NSLog(@"difference: %f", difference);
    if ([runningTimerLbl.text isEqualToString:@"00:00"]) {
        
        [self stopTimerLevel];
    }
}

- (NSString *)stringFromTimeInterval:(NSTimeInterval)interval {
    NSInteger ti = (NSInteger)interval;
    NSInteger seconds = ti % 60;
    NSInteger minutes = (ti / 60) % 60;
    // NSInteger hours = (ti / 3600);
    return [NSString stringWithFormat:@"%02ld:%02ld", (long)minutes, (long)seconds];
}

- (void)deleteAllEntities:(NSString *)nameEntity
{
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:nameEntity];
    [fetchRequest setIncludesPropertyValues:NO]; //only fetch the managedObjectID
    NSError *error;
    NSArray *fetchedObjects = [[self managedObjectContext] executeFetchRequest:fetchRequest error:&error];
    for (NSManagedObject *object in fetchedObjects)
    {
        [[self managedObjectContext] deleteObject:object];
    }
    
    error = nil;
    [[self managedObjectContext] save:&error];
    
    [self fetchDataFromCoreData];
}

-(void)fetchDataFromCoreData {
    self.eventsArray = [NSMutableArray new];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"FirstTableEvents"];
    fetchRequest.resultType = NSDictionaryResultType;
    self.eventsArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"SAVED DATA : %@",self.eventsArray);
    if (self.eventsArray.count > 0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
                       {
                           //[self postEventsToEGCS];
                           [self deleteAllEntities:@"FirstTableEvents"];
                       });
    }
}

-(void)fetchSecondTableDataFromCoreData {
    self.eventsArray = [NSMutableArray new];
    
    NSManagedObjectContext *context = [self managedObjectContext];
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] initWithEntityName:@"SecondTableEvents"];
    fetchRequest.resultType = NSDictionaryResultType;
    self.eventsArray = [[context executeFetchRequest:fetchRequest error:nil] mutableCopy];
    NSLog(@"SAVED SECOND TABLE DATA : %@",self.eventsArray);
    if (self.eventsArray.count > 0) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
                       {
                           //[self postEventsToEGCS];
                           [self deleteAllEntities:@"SecondTableEvents"];
                       });
    }
}

-(void) postEventsToEGCS {
    
    NSDictionary *parameters = @{@"events_list": self.eventsArray};
    NSLog(@"PARAMS : %@",parameters);
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:parameters options:0 error:nil];
    
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    //https://www.tajrummynetwork.com
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:@"http://trn.glserv.info/egcs/track-game-eventlogs/"]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:20.0];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error)
                                      {
                                          if (error) {
                                              NSLog(@"%@", error);
                                          } else {
                                              id jsonResponseString = [[NSString alloc] initWithData:data
                                                                                            encoding:NSASCIIStringEncoding];
                                              
                                              NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
                                              id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                              NSLog(@"EVENTS EGCS RESPONSE : %@",jsonResponse);
                                              
                                          }
                                      }];
    [dataTask resume];
}

-(void)checkInternet {
    // create a Reachability object for www.google.com
    
    self.googleReach = [Reachability reachabilityWithHostName:@"www.google.com"];
    
    self.googleReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this uses NSOperationQueue mainQueue
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
        }];
    };
    
    self.googleReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"GOOGLE Block Says Unreachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        // to update UI components from a block callback
        // you need to dipatch this to the main thread
        // this one uses dispatch_async they do the same thing (as above)
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [self.googleReach startNotifier];
    
    // create a reachability for the local WiFi
    
    self.localWiFiReach = [Reachability reachabilityForLocalWiFi];
    
    // we ONLY want to be reachable on WIFI - cellular is NOT an acceptable connectivity
    self.localWiFiReach.reachableOnWWAN = NO;
    
    self.localWiFiReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"LocalWIFI Block Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    self.localWiFiReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"LocalWIFI Block Says Unreachable(%@)", reachability.currentReachabilityString];
        
        NSLog(@"%@", temp);
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [self.localWiFiReach startNotifier];
    
    // create a Reachability object for the internet
    
    self.internetConnectionReach = [Reachability reachabilityForInternetConnection];
    
    self.internetConnectionReach.reachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@" InternetConnection Says Reachable(%@)", reachability.currentReachabilityString];
        NSLog(@"%@", temp);
        
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    self.internetConnectionReach.unreachableBlock = ^(Reachability * reachability)
    {
        NSString * temp = [NSString stringWithFormat:@"InternetConnection Block Says Unreachable(%@)", reachability.currentReachabilityString];
        
        NSLog(@"%@", temp);
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    };
    
    [self.internetConnectionReach startNotifier];
}

//-(void)reachabilityChanged:(NSNotification *)note
//{
//    Reachability * reach = [note object];
//    if (reach == self.internetConnectionReach)
//    {
//        if([reach isReachable])
//        {
//            NSString * temp = [NSString stringWithFormat:@"InternetConnection Notification Says Reachable(%@)", reach.currentReachabilityString];
//            NSLog(@"%@", temp);
//
//        }
//        else
//        {
//            NSString * temp = [NSString stringWithFormat:@"InternetConnection Notification Says Unreachable(%@)", reach.currentReachabilityString];
//            NSLog(@"%@", temp);
//
//        }
//    }
//}

//- (void)checkInternetConnection:(NSNotification *)notification
//{
//    NSDictionary *dictionary = nil;
//    if (notification)
//    {
//        dictionary = [notification object];
//    }
//
//    NSString *defaultText = dictionary[DEFAULT_TEXT];
//    NSLog(@"CHECKING NET : %@",defaultText);
//
//}

-(void)internetConnectionChanged
{
    NetworkStatus networkStatus = [self.reachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        if ([self.savedFirstTableID isEqualToString:self.tableIDString]) {
            //[self saveToCoreDataWithEvent:@"Internet Disconnect"];
            //[self saveToCoreDataWithEvent:@"Auto Play"];
        }
        else {
            //[self saveToCoreDataWithSecondTableEvent:@"Internet Disconnect"];
            //[self saveToCoreDataWithSecondTableEvent:@"Auto Play"];
        }
    }
    else {
        if ([self.savedFirstTableID isEqualToString:self.tableIDString]) {
           // [self saveToCoreDataWithEvent:@"Internet Connect"];
        }
        else {
            //[self saveToCoreDataWithSecondTableEvent:@"Internet Connect"];
        }
    }
}


- (void)chunksTimerUpdate:(NSNotification *) notification{
    
    //[self.playerTurnView startPlayerTimer:30 turn:self.isMyTurn placeId:playerTurn chunksTimer:10 ischunks:YES];
}


@end
