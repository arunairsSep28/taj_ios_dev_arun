/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJNavigationBar.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/01/14.
 **/

#import "TAJNavigationBar.h"

@interface TAJNavigationBar()

@property (nonatomic,strong)NSNumber *stateBarStyle;
@end

@implementation TAJNavigationBar

@synthesize stateBarColor = _stateBarColor;
@synthesize stateBarStyle = _stateBarStyle;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)drawRect:(CGRect)rect
{
    [[UIImage imageNamed:kBackgroundImage] drawInRect:rect];
}


- (void)setNeedsLayout
{
    [super setNeedsLayout];
    
    self.barStyle = (_stateBarStyle)?[_stateBarStyle integerValue]:kDefaultStateBarSytle;
    UIView *view = [self viewWithTag:99900];
    if (!view) {
        view = [[UIView alloc] initWithFrame:CGRectMake(0, -20, self.bounds.size.width, 20)];
        view.backgroundColor = (_stateBarColor)?_stateBarColor:kDefaultStateBarColor;
        [self addSubview:view];
    }
    
    self.translucent = NO;
    
    self.tintColor = [UIColor clearColor];
}

- (void)setStateBarColor:(UIColor *)stateBarColor
{
    _stateBarColor = stateBarColor;
    UIView *view = [self viewWithTag:99900];
    if (!view&&stateBarColor) {
        [self setNeedsLayout];
    }
}

- (void)setCusBarStyele:(UIBarStyle)cusBarStyele
{
    _stateBarStyle = [NSNumber numberWithInteger:cusBarStyele];
    [self setNeedsLayout];
}

- (void)setDefault
{
    self.stateBarColor = kDefaultStateBarColor;
    self.cusBarStyele = kDefaultStateBarSytle;
}


@end

@implementation TAJNavBarButtonItem

@synthesize itemType = _itemType;
@synthesize button = _button;
@synthesize title = _title;
@synthesize image = _image;
@synthesize font = _font;
@synthesize normalColor = _normalColor;
@synthesize selectedColor = _selectedColor;
@synthesize selector = _selector;
@synthesize target = _target;
@synthesize highlightedWhileSwitch = _highlightedWhileSwitch;

- (void)dealloc
{
    
    self.target = nil;
    self.selector = nil;
}

- (id)initWithType:(TAJNavBarButtonItemType)itemType
{
    self = [super init];
    if (self) {
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        self.button  = button;
        self.itemType = itemType;
        button.titleLabel.font  = [UIFont systemFontOfSize:kItemTextFont];
        [button setTitleColor:kItemTextNormalColot forState:UIControlStateNormal];
        
        [button setTitleColor:kItemTextSelectedColot forState:UIControlStateHighlighted];
        [button setTitleColor:kItemTextSelectedColot forState:UIControlStateSelected];
        button.frame =CGRectMake(0, 0, kItemWidth, kItemHeight);
    }
    
    return self;
}

+ (id)defauleItemWithTarget:(id)target
                     action:(SEL)action
                      title:(NSString *)title
{
    TAJNavBarButtonItem *item = [[TAJNavBarButtonItem alloc]initWithType:TAJNavBarButtonItemTypeDefault];
    item.title = title;
    [item setTarget:target withAction:action];
    return item;
}

+ (id)defauleItemWithTarget:(id)target
                     action:(SEL)action
                      image:(NSString *)image
{
    TAJNavBarButtonItem *item = [[TAJNavBarButtonItem alloc]initWithType:TAJNavBarButtonItemTypeDefault];
    item.image = image;
    [item setTarget:target withAction:action];
    return item;
}

+ (id)backItemWithTarget:(id)target
                  action:(SEL)action
                   title:(NSString *)title
{
    TAJNavBarButtonItem *item = [[TAJNavBarButtonItem alloc]initWithType:TAJNavBarButtonItemTypeBack];
    item.title = title;
    [item setTarget:target withAction:action];
    return item;
}

- (void)setItemType:(TAJNavBarButtonItemType)itemType
{
    _itemType = itemType;
    UIImage *image;
    UIImage *image_s;
    switch (itemType) {
        case TAJNavBarButtonItemTypeBack:
        {
            image = [UIImage imageNamed:kBackItemImage];
            image_s = [UIImage imageNamed:kBackItemSelectedImage];
            
        }
            break;
        case TAJNavBarButtonItemTypeDefault:
        {
            image = [UIImage imageNamed:kItemImage];
            image_s = [UIImage imageNamed:kItemSelectedImage];
        }
            break;
        default:
            break;
    }
    
    [_button setBackgroundImage:image forState:UIControlStateNormal];
    [_button setBackgroundImage:image_s forState:UIControlStateHighlighted];
    [_button setBackgroundImage:image_s forState:UIControlStateSelected];
    
    [self  titleOffsetWithType];
}

- (void)setTitle:(NSString *)title
{
    _title = title;
    [_button setTitle:title forState:UIControlStateNormal];
    [_button setTitle:title forState:UIControlStateHighlighted];
    [_button setTitle:title forState:UIControlStateSelected];
    
    [self  titleOffsetWithType];
}

- (void)setImage:(NSString *)image
{
    _image = image;
    UIImage *image_ = [UIImage imageNamed:image];
    [_button setImage:image_  forState:UIControlStateNormal];
    [_button setImage:image_ forState:UIControlStateHighlighted];
    [_button setImage:image_ forState:UIControlStateSelected];
}

- (void)setFont:(UIFont *)font
{
    _font = font;
    [_button.titleLabel setFont:font];
}

- (void)setNormalColor:(UIColor *)normalColor
{
    _normalColor = normalColor;
    [_button setTitleColor:normalColor forState:UIControlStateNormal];
}

- (void)setSelectedColor:(UIColor *)selectedColor
{
    _selectedColor = selectedColor;
    [_button setTitleColor:selectedColor forState:UIControlStateHighlighted];
    [_button setTitleColor:selectedColor forState:UIControlStateSelected];
}

- (void)setTarget:(id)target withAction:(SEL)action
{
    [_button addTarget:target action:action forControlEvents:UIControlEventTouchUpInside];
}


- (void)titleOffsetWithType
{
    switch (_itemType) {
        case TAJNavBarButtonItemTypeBack:
        {
            [_button setContentEdgeInsets:kBackItemOffset];
        }
            break;
        case TAJNavBarButtonItemTypeDefault:
        {
            [_button setContentEdgeInsets:UIEdgeInsetsMake(0, 0, 0, 0)];
        }
            break;
        default:
            break;
    }
}

- (void)setHighlightedWhileSwitch:(BOOL)highlightedWhileSwitch
{
    UIImage *image;
    if (!highlightedWhileSwitch) {
        
        switch (_itemType) {
            case TAJNavBarButtonItemTypeBack:
            {
                image = [UIImage imageNamed:kBackItemImage];
            }
                break;
            case TAJNavBarButtonItemTypeDefault:
            {
                image = [UIImage imageNamed:kItemImage];
                
            }
                break;
            default:
                break;
        }
    }else
    {
        switch (_itemType) {
            case TAJNavBarButtonItemTypeBack:
            {
                image = [UIImage imageNamed:kBackItemSelectedImage];
            }
                break;
            case TAJNavBarButtonItemTypeDefault:
            {
                image = [UIImage imageNamed:kItemSelectedImage];
                
            }
                break;
            default:
                break;
        }
    }
    [_button setBackgroundImage:image forState:UIControlStateNormal];
    [_button setBackgroundImage:image forState:UIControlStateHighlighted];
    [_button setBackgroundImage:image forState:UIControlStateSelected];
}


@end

@implementation UINavigationItem(CustomBarButtonItem)

- (void)setNewTitle:(NSString *)title
{
    
    UILabel *label = [[UILabel alloc] init];
    label.frame = CGRectMake(0, 0, 180, 20);
    label.backgroundColor = [UIColor clearColor];
    label.tag = 99901;
    label.font = [UIFont systemFontOfSize:kTitleFont];
    label.textColor = kTitleColor;
    label.textAlignment = TAJTextAlignmentCenter;
    label.text = title;
    self.titleView = label;
    
}

- (void)setNewTitleImage:(UIImage *)image
{
    UIImageView *imageView = [[UIImageView alloc] init];
    CGRect bounds = imageView.bounds;
    imageView.tag = 99902;
    bounds.size  =  image.size;
    imageView.bounds = bounds;
    self.titleView = imageView;
}

- (void)setLeftItemWithTarget:(id)target
                       action:(SEL)action
                        title:(NSString *)title
{
    TAJNavBarButtonItem *buttonItem = [TAJNavBarButtonItem defauleItemWithTarget:target
                                                                    action:action
                                                                     title:title];
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem.button];
}


- (void)setLeftItemWithTarget:(id)target
                       action:(SEL)action
                        image:(NSString *)image
{
    TAJNavBarButtonItem *buttonItem = [TAJNavBarButtonItem defauleItemWithTarget:target
                                                                    action:action
                                                                     image:image];
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem.button];
}


- (void)setLeftItemWithButtonItem:(TAJNavBarButtonItem *)item
{
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:item.button];
}


- (void)setRightItemWithTarget:(id)target
                        action:(SEL)action
                         title:(NSString *)title
{
    TAJNavBarButtonItem *buttonItem = [TAJNavBarButtonItem defauleItemWithTarget:target
                                                                    action:action
                                                                     title:title];
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem.button];
}

- (void)setRightItemWithTarget:(id)target
                        action:(SEL)action
                         image:(NSString *)image
{
    TAJNavBarButtonItem *buttonItem = [TAJNavBarButtonItem defauleItemWithTarget:target
                                                                    action:action
                                                                     image:image];
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem.button];
}

- (void)setRightItemWithButtonItem:(TAJNavBarButtonItem *)item
{
    self.rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:item.button];
}

- (void)setBackItemWithTarget:(id)target
                       action:(SEL)action

{
    TAJNavBarButtonItem *buttonItem = [TAJNavBarButtonItem backItemWithTarget:target
                                                                 action:action
                                                                  title:@"Back"];
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem.button];
}

- (void)setBackItemWithTarget:(id)target
                       action:(SEL)action
                        title:(NSString *)title
{
    TAJNavBarButtonItem *buttonItem = [TAJNavBarButtonItem backItemWithTarget:target
                                                                 action:action
                                                                  title:title];
    self.leftBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:buttonItem.button];
}



@end
