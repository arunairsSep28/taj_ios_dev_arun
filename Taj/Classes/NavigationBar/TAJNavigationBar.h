/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJNavigationBar.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/01/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJConstants.h"


@interface TAJNavigationBar : UINavigationBar

@property (nonatomic,strong)UIColor *stateBarColor;
@property (nonatomic,assign)UIBarStyle cusBarStyele;

- (void)setDefault;

@end


typedef enum {
    
    TAJNavBarButtonItemTypeDefault = 0,
    TAJNavBarButtonItemTypeBack = 1
    
}TAJNavBarButtonItemType;


@interface TAJNavBarButtonItem : NSObject
@property (nonatomic,assign)TAJNavBarButtonItemType itemType;
@property (nonatomic,strong)UIButton *button;
@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSString *image;
@property (nonatomic,strong)UIFont *font;
@property (nonatomic,strong)UIColor *normalColor;
@property (nonatomic,strong)UIColor *selectedColor;
@property (nonatomic,weak)id target;
@property (nonatomic,assign)SEL selector;
@property (nonatomic,assign)BOOL highlightedWhileSwitch;

- (id)initWithType:(TAJNavBarButtonItemType)itemType;

+ (id)defauleItemWithTarget:(id)target
                     action:(SEL)action
                      title:(NSString *)title;
+ (id)defauleItemWithTarget:(id)target
                     action:(SEL)action
                      image:(NSString *)image;
+ (id)backItemWithTarget:(id)target
                  action:(SEL)action
                   title:(NSString *)title;

- (void)setTarget:(id)target withAction:(SEL)action;


@end

@interface UINavigationItem (CustomBarButtonItem)

- (void)setNewTitle:(NSString *)title;
- (void)setNewTitleImage:(UIImage *)image;



- (void)setLeftItemWithTarget:(id)target
                       action:(SEL)action
                        title:(NSString *)title;
- (void)setLeftItemWithTarget:(id)target
                       action:(SEL)action
                        image:(UIImage *)image;
- (void)setLeftItemWithButtonItem:(TAJNavBarButtonItem *)item;



- (void)setRightItemWithTarget:(id)target
                        action:(SEL)action
                         title:(NSString *)title;
- (void)setRightItemWithTarget:(id)target
                        action:(SEL)action
                         image:(UIImage *)image;
- (void)setRightItemWithButtonItem:(TAJNavBarButtonItem *)item;



- (void)setBackItemWithTarget:(id)target
                       action:(SEL)action;
- (void)setBackItemWithTarget:(id)target
                       action:(SEL)action
                        title:(NSString *)title;

@end