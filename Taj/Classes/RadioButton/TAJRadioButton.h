/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRadioButton.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/01/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJConstants.h"



@protocol TAJRadioButtonDelegate <NSObject>

-(void)radioButtonSelectedAtIndex:(NSUInteger)index inGroup:(NSString*)groupId;
-(void)radioButtonUnSelectedAtIndex:(NSUInteger)index inGroup:(NSString*)groupId;

@end

@interface TAJRadioButton : UIView
{
    NSString *_groupId;
    NSUInteger _index;
    UIButton *_button;
}

@property(nonatomic,retain)NSString *groupId;
@property(nonatomic,assign)NSUInteger index;

-(id)initWithGroupId:(NSString*)groupId index:(NSUInteger)index;
+(void)addObserverForGroupId:(NSString*)groupId observer:(id)observer;

- (void)setButtonState;
@end
