/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRadioButton.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 21/01/14.
 **/

#import "TAJRadioButton.h"

@interface TAJRadioButton()

-(void)defaultInit;
-(void)otherButtonSelected:(id)sender;
-(void)handleButtonTap:(id)sender;

@end

@implementation TAJRadioButton

@synthesize groupId =_groupId;
@synthesize index   =_index;

static NSMutableArray *rb_instances=nil;
static NSMutableDictionary *rb_observers=nil;

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (void)setButtonState
{
    [self handleButtonTap:self];
}

#pragma mark - Observer

+(void)addObserverForGroupId:(NSString*)groupId observer:(id)observer
{
    if(!rb_observers)
    {
        rb_observers = [[NSMutableDictionary alloc] init];
    }
    
    if ([groupId length] > 0 && observer)
    {
        [rb_observers setObject:observer forKey:groupId];
       
        
    }
}

#pragma mark - Manage Instances

+(void)registerInstance:(TAJRadioButton*)radioButton
{
    if(!rb_instances)
    {
        rb_instances = [[NSMutableArray alloc] init];
    }
    
    [rb_instances addObject:radioButton];
    
}

#pragma mark - Class level handler

+(void)buttonSelected:(TAJRadioButton*)radioButton
{
    
    // Notify observers once selected
    if (rb_observers)
    {
        id observer= [rb_observers objectForKey:radioButton.groupId];
        
        if(observer && [observer respondsToSelector:@selector(radioButtonSelectedAtIndex:inGroup:)])
        {
            [observer radioButtonSelectedAtIndex:radioButton.index inGroup:radioButton.groupId];
        }
    }
}

+(void)buttonUnSelected:(TAJRadioButton*)radioButton
{
    
    // Notify observers once unselected
    if (rb_observers)
    {
        id observer= [rb_observers objectForKey:radioButton.groupId];
        
        if(observer && [observer respondsToSelector:@selector(radioButtonSelectedAtIndex:inGroup:)])
        {
            [observer radioButtonUnSelectedAtIndex:radioButton.index inGroup:radioButton.groupId];
        }
    }
}

#pragma mark - Object Lifecycle

-(id)initWithGroupId:(NSString*)groupId index:(NSUInteger)index
{
    self = [self init];
    if (self) {
        _groupId = groupId;
        _index = index;
    }
    return  self;
}

- (id)init
{
    self = [super init];
    if (self)
    {
        [self defaultInit];
    }
    return self;
}


#pragma mark - Tap handling

-(void)handleButtonTap:(id)sender
{
    if(_button.selected)
    {
        [_button setSelected:NO];
        [TAJRadioButton buttonUnSelected:self];
    }
    else if(!_button.selected)
    {
        [_button setSelected:YES];
        [TAJRadioButton buttonSelected:self];
    }
}

-(void)otherButtonSelected:(id)sender
{
    // Called when other radio button instance got selected
    if(_button.selected)
    {
        [_button setSelected:NO];
    }
}

#pragma mark - RadioButton init

-(void)defaultInit
{
    // Setup container view
    self.frame = CGRectMake(0, 0, kRadioButtonWidth, kRadioButtonHeight);
    
    // Customize UIButton
    _button = [UIButton buttonWithType:UIButtonTypeCustom];
    _button.frame = CGRectMake(0, 0,kRadioButtonWidth, kRadioButtonHeight);
    _button.adjustsImageWhenHighlighted = NO;
    
    [_button setImage:[UIImage imageNamed:kRadioUnSelected] forState:UIControlStateNormal];
    [_button setImage:[UIImage imageNamed:kRadioSelected] forState:UIControlStateSelected];
    
    [_button addTarget:self action:@selector(handleButtonTap:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_button];
    
    [TAJRadioButton registerInstance:self];
}


@end
