/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSwitchLobbyViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJTableViewController.h"

@protocol TAJSwitchLobbyViewControllerDelegate;

@interface TAJSwitchLobbyViewController : TAJBaseViewController<TAJTableViewControllerDelegate>

@property (nonatomic) BOOL isPresented;

@property (nonatomic, strong) NSMutableArray *filterdTableArray;
@property (nonatomic, weak) id<TAJSwitchLobbyViewControllerDelegate> switchDelegate;

- (void)alreadyFilter:(BOOL)boolValue withGameType:(GameTypeStruct)gametype withPlayerType:(PlayerTypeStruct)playerType withBetType:(BetTypeStruct)betType withTableCost:(TableTypeStruct)tableCost withViewTye:(ViewTypeEnum)viewType;

-(void) reloadGameList;

- (BOOL)isSwitchLobbyViewExist;

- (BOOL)isPlayerJoinedAnyTable;
- (BOOL)isPlayerJoinedCompleteTwoTable;
- (void)loadGameTableForDictionary:(NSDictionary *)dictionary;
- (BOOL)isSingleTableJoinedWithDictionary:(NSDictionary *)dictionary;
- (void)loadSingleTable;
//stop connected timer
- (void)stopConnectedTimer;
- (void)destroyAllViewsOnLogout;
- (void)loadCurrentTurnTimeEnableTable;

@end

@protocol TAJSwitchLobbyViewControllerDelegate <NSObject>

- (void)showTableViewLobby;
- (void)isSwitchLobbyShowing;
- (void)displayPlayerTwoTableReached;
- (void)showAlertIndicatorForTableWhenItisMyTurn;
- (void)hideTableAlertImageView;
- (void)disableTablesButton;
- (void)enableTablesButton;
- (void)showSearchJoinErrorAlert;




@end
