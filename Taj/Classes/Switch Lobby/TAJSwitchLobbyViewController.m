/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSwitchLobbyViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Yogisha Poojary on 11/09/14.
 Created by Pradeep BM on 10/02/14.
 **/

#import "TAJSwitchLobbyViewController.h"
#import "TAJTableViewController.h"
#import "TAJGameTableController.h"
#import "TAJGameEngine.h"
#import "TAJLobby.h"
#import "TAJGameTable.h"
#import "TAJIamBackViewController.h"
#import "TAJInfoPopupViewController.h"
#import "TAJWebPageViewController.h"
#import "TAJChatExtentViewController.h"

#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJAppDelegate.h"

#define LOBBY_TABLE_ID_DEFAULT  99999999

@interface TAJSwitchLobbyViewController ()<TAJGameTableControllerDelegate, TAJIamBackViewControllerDelegate, InfoPopupViewDelegate, TAJWebPageViewControllerDelegate,TAJChatExtentViewControllerProtocol>
@property (strong , nonatomic) TAJChatExtentViewController *firstTableChatExtentView;
@property (strong , nonatomic) TAJChatExtentViewController *secondTableChatExtentView;
@property (nonatomic)         BOOL                  isFilteredData;
@property (nonatomic)         BOOL                  twoTableSpectator;
@property (nonatomic)         BOOL                  isFirstButtonBlinking;
@property (nonatomic)         BOOL                  isSecondButtonBlinking;


@property (nonatomic)         GameTypeStruct        gameTypeSearch;
@property (nonatomic)         PlayerTypeStruct      playerTypeSearch;
@property (nonatomic)         BetTypeStruct         betTypeSearch;
@property (nonatomic)         TableTypeStruct       tableCostSearch;
@property (nonatomic)         ViewTypeEnum          viewTypeSearch;
@property (nonatomic) NSInteger counter;
//IBOutlet views
@property (weak, nonatomic) IBOutlet UIView              *lobbyTableView;
@property (weak, nonatomic) IBOutlet UIView              *firstGameTable;
@property (weak, nonatomic) IBOutlet UIView              *secondGameTable;

//IBOutlet buttons
@property (weak, nonatomic) IBOutlet UIButton *lobbyButton;
@property (weak, nonatomic) IBOutlet UIButton *firstTableButton;
@property (weak, nonatomic) IBOutlet UIButton *secondTableButton;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (weak, nonatomic) IBOutlet UILabel *firstTableButtonLabel;
@property (weak, nonatomic) IBOutlet UILabel *secondTableButtonLabel;
@property (weak, nonatomic) IBOutlet UIImageView *firstTableImage;
@property (weak, nonatomic) IBOutlet UIImageView *secondTableImage;
@property (weak, nonatomic) IBOutlet UIView *viewComponents;
@property (weak, nonatomic) IBOutlet UIView *viewTopComponents;
@property (weak, nonatomic) IBOutlet UIView *viewTwoTableAlert;


@property (nonatomic)   BOOL                  isLobbyVisible;
@property (nonatomic)   BOOL                  isPlayerCanEnterGameInAutoPlayMode;

@property (nonatomic)   BOOL                  isFirstGameScheduled;
@property (nonatomic)   BOOL                  isSecondGameScheduled;
@property (nonatomic)  double                 firstGameScheduleTime;
@property (nonatomic)   BOOL                  canShowIamback;

@property (nonatomic)   double                gameScheduleFirstTableTime;
@property (nonatomic, strong)    NSDictionary *firstGameScheduleDictionary;
@property (nonatomic)  double                 secondGameScheduleTime;
@property (nonatomic, strong)    NSDictionary *secondGameScheduleDictionary;
@property (nonatomic)   double                gameScheduleSecondTableTime;
@property (nonatomic) CGRect firstButtonRect;
@property (nonatomic) CGRect secondButtonRect;
@property (nonatomic) CGRect firstButtonTableImageRect;
@property (nonatomic) CGRect secondButtonTableImageRect;
@property (nonatomic) CGRect firstButtonLabelRect;
@property (nonatomic) CGRect secondButtonLabelRect;

@property (nonatomic, strong)    NSString *firstTableChatMessage;
@property (nonatomic, strong)    NSString *secondTableChatMessage;


- (IBAction)lobbyButtonAction:(UIButton *)sender;
- (IBAction)firstTableButtonAction:(UIButton *)sender;
- (IBAction)secondTableButtonAction:(UIButton *)sender;

//create instance of object
@property (strong, nonatomic) TAJTableViewController    *tableViewLoad;
@property (strong, nonatomic) TAJGameTableController    *firstTableGameLoad;
@property (strong, nonatomic) TAJGameTableController    *secondTableGameLoad;
@property (strong, nonatomic) TAJIamBackViewController    *firstTableIAmBackController;
@property (strong, nonatomic) TAJIamBackViewController    *secondTableIAmBackController;

@property (strong, nonatomic) TAJInfoPopupViewController  *infoPopUpController;
@property (strong, nonatomic) TAJInfoPopupViewController  *errorPopUpController;
@property (strong, nonatomic) TAJInfoPopupViewController  *otherLoginAlertView;
@property (strong, nonatomic) TAJInfoPopupViewController  *engineMaintenanceAlertView;


//web page controller
@property (strong, nonatomic) TAJWebPageViewController *webPageViewController;

//table ids
@property (nonatomic) int tableIdFromTable;
@property (nonatomic) int currentTableID;
@property (nonatomic) int alreadyFirstTableID;
@property (nonatomic) int alreadySecondTableID;
@property (nonatomic) int firstTableRequestedNewTable;
@property (nonatomic) int secondTableRequestedNewTable;
@property (weak, nonatomic) IBOutlet UIView *firstTableBlinkView;
@property (weak, nonatomic) IBOutlet UIView *secondTableBlinkView;
@property (nonatomic) NSInteger firstTablePlayers;
@property (nonatomic) int secondTablePlayers;

//Connected Time
@property (weak, nonatomic) IBOutlet UILabel *connectedTimeLabel;
@property (weak, nonatomic) IBOutlet UILabel *connectedLabel;

@property(nonatomic) NSTimer* connectedTimer;

@end

@implementation TAJSwitchLobbyViewController
@synthesize filterdTableArray    = _filterdTableArray;
@synthesize tableIdFromTable     = _tableIdFromTable;
@synthesize alreadyFirstTableID  = _alreadyFirstTableID;
@synthesize alreadySecondTableID = _alreadySecondTableID;
@synthesize tableViewLoad = _tableViewLoad;

@synthesize isPresented;

#pragma mark
#pragma mark UIViewController life cycle

- (id)init
{
    self = [super init];
    if (self)
    {
        self.isFilteredData = NO;
        self.isFirstGameScheduled = NO;
        self.isSecondGameScheduled = NO;
        self.isPlayerCanEnterGameInAutoPlayMode = YES;
        self.twoTableSpectator = YES;
        
        self.firstTableRequestedNewTable = 0;
        self.secondTableRequestedNewTable = 0;
        self.gameScheduleFirstTableTime = 0;
        self.gameScheduleSecondTableTime = 0;
        self.canShowIamback = NO;
        
        [self stopConnectedTimer];
        self.connectedTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateTimerLabel) userInfo:nil repeats:YES];
        
        [self addSwitchNotifications];
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.viewTwoTableAlert.hidden = YES;
    
    self.isPresented = YES;

    self.firstButtonRect = self.firstTableButton.frame;
    self.secondButtonRect = self.secondTableButton.frame;
    self.firstButtonLabelRect = self.firstTableButtonLabel.frame;
    self.firstButtonTableImageRect = self.firstTableImage.frame;
    self.secondButtonLabelRect = self.secondTableButtonLabel.frame;
    self.secondButtonTableImageRect = self.secondTableImage.frame;
    
    self.versionLabel.text = VERSION_NUMBER;
    
    if([TAJUtilities isItIPhone5])
    {
        CGRect connectedLabelRect = self.connectedLabel.frame;
        CGRect connectedTimeLabelRect = self.connectedTimeLabel.frame;
        
        CGRect connectedLabelRectNew = CGRectMake(connectedLabelRect.origin.x + 85, connectedLabelRect.origin.y, connectedLabelRect.size.width, connectedLabelRect.size.height);
        CGRect connectedTimeLabelRectNew = CGRectMake(connectedTimeLabelRect.origin.x + 85, connectedTimeLabelRect.origin.y, connectedTimeLabelRect.size.width, connectedTimeLabelRect.size.height);
        
        [self.connectedLabel setFrame:connectedLabelRectNew];
        
        [self.connectedTimeLabel setFrame:connectedTimeLabelRectNew];
        
    }
    
    
#if NEW_DESIGN_IMPLEMENTATION
    
    [self loadFirstTable];
    
#else
    if ([TAJUtilities isIPhone])
    {
        self.tableViewLoad = [[TAJTableViewController alloc] initWithNibName:@"TAJTableViewController" bundle:nil];
    }
    else
    {
        self.tableViewLoad = [[TAJTableViewController alloc] initWithNibName:@"TAJTableViewController_iPad" bundle:nil];
        
    }
    
    [self addChildViewController:self.tableViewLoad];
    self.tableViewLoad.tableDelegate = self;
    
    [self.lobbyTableView addSubview:self.tableViewLoad.view];
    
    //load table first time
    
    [self loadTableViewAction];
#endif
 

}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    // self.counter = 0;
    
    if (self.firstTableIAmBackController || self.secondTableIAmBackController)
    {
        [self.view endEditing:YES];
    }
    
    [self.tableViewLoad viewWillAppear:YES];
    [self.firstTableGameLoad viewWillAppear:YES];
    [self.secondTableGameLoad viewWillAppear:YES];
    [self addObserversOnViewAppear];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillBeHidden:)
                                                 name:UIKeyboardWillHideNotification object:nil];
    
}

- (void)keyboardWillShow:(NSNotification *)notification
{
    
}

- (void)keyboardWillBeHidden:(NSNotification *)notification
{
    if (notification && self.firstTableChatExtentView )
    {
        [self removeChatExtentViewController];
    }
    if (notification && self.secondTableChatExtentView )
    {
        [self removeChatExtentViewController];
    }
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    [self removeObserversOnViewDisappear];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self removeObserversOnViewDisappear];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    [self removeSwitchNotifications];
    [self removeObserversOnViewDisappear];
    
}

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    if (self.firstTableIAmBackController || self.secondTableIAmBackController)
    {
        [self.view endEditing:YES];
    }
}

- (BOOL)isSwitchLobbyViewExist
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

- (void)alreadyFilter:(BOOL)boolValue withGameType:(GameTypeStruct)gametype withPlayerType:(PlayerTypeStruct)playerType withBetType:(BetTypeStruct)betType withTableCost:(TableTypeStruct)tableCost withViewTye:(ViewTypeEnum)viewType
{
    if (boolValue)
    {
        self.isFilteredData = boolValue;
        _gameTypeSearch = gametype;
        _playerTypeSearch = playerType;
        _betTypeSearch = betType;
        _tableCostSearch = tableCost;
        _viewTypeSearch = viewType;
    }
    
    else if (!boolValue)
    {
        self.isFilteredData = boolValue;
    }
}

- (BOOL)isPlayerJoinedAnyTable
{
    if (self.firstTableGameLoad || self.secondTableGameLoad)
    {
        return YES;
    }
    return NO;
}

- (void)addSwitchNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMoveToGameTable:)
                                                 name:MOVETOGAMESCREEN
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleGameQuit:)
                                                 name:QUIT_TABLE_REPLY object:nil];
    
    // Register search join table
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleSearchJoinTable:)
                                                 name:SEARCH_JOIN_TABLE
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(joinNewTable:)
                                                 name:SEARCH_JOIN_GET_TABLE_DETAILS object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(gameScheduleForNewTable:)
                                                 name:GAME_SCHEDULE object:nil];
    
    // Get table extra information
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getTableExtraInformation:)
                                                 name:GET_TABLE_EXTRA
                                               object:nil];
    
    // Get table details for player already in view / play
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(getTableDetailsInformation:)
                                                 name:GET_TABLE_DETAILS_EVENT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(errorHandle:)
                                                 name:ERROR_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleAutoPlayCount:)
                                                 name:IAM_BACK_VIEW_AUTOPLAY_COUNT
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeTableForID:)
                                                 name:QUIT_UNUSED_TABLE
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeTableOnSearchJoinError:)
                                                 name:SEARCH_JOIN_ERROR_NOTIFICATION
                                               object:nil];
    
}

- (void)removeSwitchNotifications
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MOVETOGAMESCREEN object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QUIT_TABLE_REPLY object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEARCH_JOIN_TABLE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEARCH_JOIN_GET_TABLE_DETAILS object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_SCHEDULE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GET_TABLE_EXTRA object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:IAM_BACK_VIEW_AUTOPLAY_COUNT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:QUIT_UNUSED_TABLE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SEARCH_JOIN_ERROR_NOTIFICATION object:nil];
    
}

- (void)addObserversOnViewAppear
{
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showProgressAlertInLandscape:)
                                                 name:SHOW_ALERT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showOtherLoginAlertInGame:)
                                                 name:SHOW_OTHER_LOGIN_ALERT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(showEngineUnderMaintenancenAlertInGame:)
                                                 name:SHOW_ENGINE_STATUS_TRUE_ALERT
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(removeEngineUnderMaintenancenAlertInGame:)
                                                 name:SHOW_ENGINE_STATUS_FALSE_ALERT
                                               object:nil];
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(requestJoinTable:)
//                                                 name:REQUEST_JOIN_TABLE
//                                               object:nil];
    //ratheesh
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showPlayersStatusOnTable:) name:SHOW_PLAYERS object:nil];
}

- (void)removeObserversOnViewDisappear
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_OTHER_LOGIN_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_ENGINE_STATUS_TRUE_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_ENGINE_STATUS_FALSE_ALERT object:nil];
    
     [[NSNotificationCenter defaultCenter] removeObserver:self name:REQUEST_JOIN_TABLE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_PLAYERS object:nil];
}


#pragma mark - Notification listener -

- (void)requestJoinTable:(NSNotification *)notification
{
    NSLog(@"JOING TABLE DATA : %@",[notification object]);
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [[TAJGameEngine sharedGameEngine] requestJoinTableReply:dictionary[@"TAJ_msg_uuid"]];
    [[TAJGameEngine sharedGameEngine] getTableDetailsAndJoinTableAsPlayTournament:dictionary];
}

- (void)handleAutoPlayCount:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self updateAutoPlayCount:dictionary];
}

//move to game table
- (void)handleMoveToGameTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self moveToGameTable:dictionary];
}

//quit game table
- (void)handleGameQuit:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self quitTableWithID:dictionary];
}

//remove game table when server sends error resopnse for search join game
- (void)removeTableOnSearchJoinError:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    
    if (dictionary && [dictionary[TAJ_MSG_UUID] isEqualToString:engine.lobby.firstTable.msgUUIDForSearchJoinTable])
    {
        [self.firstTableGameLoad quitGame];
        engine.lobby.firstTable = nil;
        engine.lobby.isFirstTableCreated = NO;
        
    }
    
    if (dictionary && [dictionary[TAJ_MSG_UUID] isEqualToString:engine.lobby.secondTable.msgUUIDForSearchJoinTable])
    {
        [self.secondTableGameLoad quitGame];
        engine.lobby.secondTable = nil;
        engine.lobby.isSecondTableCreated = NO;
    }
    [self loadTableViewAction];
    if (self.switchDelegate && [self.switchDelegate respondsToSelector:@selector(showSearchJoinErrorAlert)])
    {
        [self.switchDelegate showSearchJoinErrorAlert];
    }
}

//process search join and update corresponding game table with new table data
- (void)handleSearchJoinTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    DLog(@"search_join_table response --- %@",dictionary);
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    NSString *firstTableID = [NSString stringWithFormat:@"%d",self.alreadyFirstTableID];
    NSString *secondTableID = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
    
    DLog(@"search join notificatro %@,%@,%@,%@",dictionary,dictionary[TAJ_MSG_UUID],engine.lobby.firstTable.msgUUIDForSearchJoinTable,secondTableID);
    if (dictionary && [dictionary[TAJ_MSG_UUID] isEqualToString:engine.lobby.firstTable.msgUUIDForSearchJoinTable])
    {
        if (![dictionary[TAJ_TABLE_ID] isEqualToString:secondTableID])
        {
            DLog(@" first table search join notificatro %@",dictionary);
            
            [engine.lobby updateFirstTableWithSearchJointableDictionary:dictionary];
            [engine getTableDetailsAndDontJoinTable:dictionary];
        }
        else
        {
            [self.firstTableGameLoad quitGame];
            engine.lobby.firstTable = nil;
            engine.lobby.isFirstTableCreated = NO;
            
        }
    }
    DLog(@"search join notificatro %@,%@,%@,%@",dictionary,dictionary[TAJ_MSG_UUID],engine.lobby.secondTable.msgUUIDForSearchJoinTable,firstTableID);
    
    if (dictionary && [dictionary[TAJ_MSG_UUID] isEqualToString:engine.lobby.secondTable.msgUUIDForSearchJoinTable])
    {
        if (![dictionary[TAJ_TABLE_ID] isEqualToString:firstTableID])
        {
            DLog(@" second table search join notificatro %@",dictionary);
            [engine.lobby updateSecondTableWithSearchJointableDictionary:dictionary];
            [engine getTableDetailsAndDontJoinTable:dictionary];
        }
        else
        {
            [self.secondTableGameLoad quitGame];
            engine.lobby.secondTable = nil;
            engine.lobby.isSecondTableCreated = NO;
        }
        
    }
}

//process join new table
- (void)joinNewTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self handleJoinNewTable:dictionary];
}

- (void)gameScheduleForNewTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self handleGameScheduleForNewTable:dictionary];
}

// Get table extra information for this player reconnect notifications
- (void)getTableExtraInformation:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary)
    {
        [self handleGetTableExtraInformationEvent:dictionary];
    }
}

// Get table details for player already in view/ play notifications
- (void)getTableDetailsInformation:(NSNotification *)notification
{
    
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary)
    {
        [self handleGetTableDetailsInformationEvent:dictionary];
    }
}

- (void)errorHandle:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    [self processServerErrorEvent:[dictionary[kTaj_Error_code] integerValue]];
}

#pragma mark - Error Code Process -

//process server error codes
- (void)processServerErrorEvent:(int)errorCode
{
    switch (errorCode)
    {
        case eDATABASE_ERROR:
        {
            [self showAlertOnServerError:ALERT_DATABASE_ERROR];
        }
            break;
        case eENGINE_UNDER_MAINTENANCE:
        {
            //100 engine is under maintenance
            NSLog(@"CHECK eENGINE_UNDER_MAINTENANCE");
            //[self showAlertOnServerError:@"Engine is Under Maintenance"];
        }
            break;
            break;
        default:
            break;
    }
}

- (void)showAlertOnServerError:(NSString *)message
{
    if (!self.errorPopUpController)
    {
        self.errorPopUpController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:message];
        
        [self.errorPopUpController show];
    }
}

//show reconnecting popup when user is disconnected from server
- (void) showProgressAlertInLandscape:(NSNotification *)notification
{
    if (self.webPageViewController)
    {
        [self.webPageViewController.view removeFromSuperview];
    }
    if(self.firstTableGameLoad)
    {
        [self.firstTableGameLoad invalidatePlaayerTimer];
    }
    if(self.secondTableGameLoad)
    {
        [self.secondTableGameLoad invalidatePlaayerTimer];
    }
    self.canShowIamback =NO;
    [[[TAJGameEngine sharedGameEngine] activityAlertForReconnecting] show:false];
    
}

//show dual login alert when server sends other_login event
- (void)showOtherLoginAlertInGame:(NSNotification *)notification
{
    if (!self.otherLoginAlertView)
    {
        self.otherLoginAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eNone_Type message:OTHER_LOGIN_TEXT];
        
        [self.otherLoginAlertView show];
    }
    [self stopConnectedTimer];
    
    [self.firstTableGameLoad stopProgressTimer];
    [self.secondTableGameLoad stopProgressTimer];
    
    [self.firstTableGameLoad stopTimer];
    [self.secondTableGameLoad stopTimer];
    
    [self.tableViewLoad removeAllObserver];
    [self.firstTableGameLoad removeAllNotifications];
    [self.secondTableGameLoad removeAllNotifications];
    self.firstTableGameLoad = nil;
    self.secondTableGameLoad = nil;
    
    [[[TAJGameEngine sharedGameEngine] lobby] destroyFirstTable];
    [[[TAJGameEngine sharedGameEngine] lobby] destroySecondTable];
    
    [[TAJGameEngine sharedGameEngine] lobby].isFirstTableCreated = NO;
    [[TAJGameEngine sharedGameEngine] lobby].isSecondTableCreated = NO;
    
    [[TAJGameEngine sharedGameEngine] destroyLobby];
    
}

//show server under maintenence pop
- (void)showEngineUnderMaintenancenAlertInGame:(NSNotification *)notification
{
    if (!self.engineMaintenanceAlertView)
    {
        self.engineMaintenanceAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:locate(eENGINE_UNDER_MAINTENANCE)];
        
        [self.engineMaintenanceAlertView show];
    }
    
}

- (void)removeEngineUnderMaintenancenAlertInGame:(NSNotification *)notification
{
    if (self.engineMaintenanceAlertView)
    {
        [self.engineMaintenanceAlertView hide];
        self.engineMaintenanceAlertView = nil;
    }
    
}

- (void)playersStatusOnTable:(NSDictionary *)dictionary
{
    NSLog(@"alreadyFirstTableID : %d",_alreadyFirstTableID);
    NSLog(@"alreadySecondTableID : %d",_alreadySecondTableID);
    
    //NSLog(@"FIRST CHECKING STATUS : %@",dictionary);
    
    NSString * imageName;
    
    int tableId = [[dictionary objectForKey:@"table_id"] intValue];
    NSLog(@"tableId %d",tableId);
    
    if (self.alreadyFirstTableID == tableId) {
        NSLog(@"YES FIRST TABLE");
        id players = [dictionary objectForKey:@"count"];
        int maxPlayers = [[dictionary objectForKey:@"maxplayer"] intValue];
        if (maxPlayers == 2) {
            imageName = [NSString stringWithFormat:@"TR_Player2_Status-%@",players];
        }
        else {
            imageName = [NSString stringWithFormat:@"TR_Plr_Status-%@",players];
        }
        self.firstTableImage.image = [UIImage imageNamed:imageName];
//        NSLog(@"F IMAGE : %@",imageName);
        
    }
    if (self.alreadySecondTableID == tableId) {
        NSLog(@"YES SECOND TABLE");
        id players = [dictionary objectForKey:@"count"];
        int maxPlayers = [[dictionary objectForKey:@"maxplayer"] intValue];
        if (maxPlayers == 2) {
            imageName = [NSString stringWithFormat:@"TR_Player2_Status-%@",players];
        }
        else {
            imageName = [NSString stringWithFormat:@"TR_Plr_Status-%@",players];
        }
        self.secondTableImage.image = [UIImage imageNamed:imageName];
        NSLog(@"S IMAGE : %@",imageName);
    }
    
}

- (void)showPlayersStatusOnTable:(NSNotification *)notification
{
    //NSLog(@"alreadyFirstTableID : %d",_alreadyFirstTableID);
    //NSLog(@"alreadySecondTableID : %d",_alreadySecondTableID);
    
    //NSLog(@"showPlayersStatusOnTable : %@",[notification object]);
    
    NSDictionary * dictionary = nil;
    NSString * imageName;
    
    if (notification) {
        dictionary = [notification object];
    }
    if (dictionary) {
        int tableId = [[dictionary objectForKey:@"table_id"] intValue];
        NSLog(@"tableId %d",tableId);
        
        if (self.alreadyFirstTableID == tableId) {
            NSLog(@"YES FIRST TABLE");
            id players = [dictionary objectForKey:@"count"];
            int maxPlayers = [[dictionary objectForKey:@"maxplayer"] intValue];
            if (maxPlayers == 2) {
                 imageName = [NSString stringWithFormat:@"TR_Player2_Status-%@",players];
            }
            else {
            imageName = [NSString stringWithFormat:@"TR_Player_Status-%@",players];
            }
            self.firstTableImage.image = [UIImage imageNamed:imageName];
            //NSLog(@"F IMAGE : %@",imageName);
            
            
        }
        if (self.alreadySecondTableID == tableId) {
            NSLog(@"YES SECOND TABLE");
            id players = [dictionary objectForKey:@"count"];
            int maxPlayers = [[dictionary objectForKey:@"maxplayer"] intValue];
            if (maxPlayers == 2) {
                imageName = [NSString stringWithFormat:@"TR_Player2_Status-%@",players];
            }
            else {
                imageName = [NSString stringWithFormat:@"TR_Player_Status-%@",players];
            }
            self.secondTableImage.image = [UIImage imageNamed:imageName];
            NSLog(@"S IMAGE : %@",imageName);
        }
    }
}

//remove a table using table id on player quit
- (void)removeTableForID:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary)
    {
        [self tableIDForQuit:[dictionary[TAJ_TABLE_ID] intValue]];
    }
    
}

#pragma mark - Notification Observer -

// update autoplay count for a table i.e. number of cards disccarded by server
- (void)updateAutoPlayCount:(NSDictionary *)dictionary
{
    if ([self.firstTableGameLoad.currentTableId isEqualToString:[dictionary valueForKey:TAJ_TABLE_ID]])
    {
        
        if (self.firstTableIAmBackController)
        {
            NSString *count = [dictionary valueForKey:TAJ_COUNT];
            self.firstTableIAmBackController.iamBackModel.currentAutoPlayCount =[count intValue] ;
            [self.firstTableIAmBackController.iamBackModel updateIambackModelArray];
        }
    }
    else if ([self.secondTableGameLoad.currentTableId isEqualToString:[dictionary valueForKey:TAJ_TABLE_ID]])
    {
        if (self.secondTableIAmBackController)
        {
            
            NSString *count = [dictionary valueForKey:TAJ_COUNT];
            self.secondTableIAmBackController.iamBackModel.currentAutoPlayCount =[count intValue] ;
            [self.secondTableIAmBackController.iamBackModel updateIambackModelArray];
        }
    }
}

// create and load game table
- (void)moveToGameTable:(NSDictionary *)dictionary
{
    
    [self.tableViewLoad.activityViewShow stopAnimating];
    self.tableViewLoad.activityViewShow.hidden = YES;
    self.tableViewLoad.isClicked = YES;
    //check player is already in view
    if ( self.firstTableIAmBackController && !self.firstTableGameLoad)
    {
        [self.firstTableIAmBackController.view removeFromSuperview];
        self.firstTableIAmBackController = nil;
        
    }
    if ( self.secondTableIAmBackController && !self.secondTableGameLoad)
    {
        [self.secondTableIAmBackController.view removeFromSuperview];
        self.secondTableIAmBackController = nil;
        
    }
    [self checkPlayerAlreadyInGameTable];
}

- (void)quitTableWithID:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
}

- (void)handleGameScheduleForNewTable:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    //TODO: VERY IMPORTANT CASE HANDLING
    
    if (self.firstTableGameLoad &&
        [self.firstTableGameLoad isRequestedToJoinNewTable] &&
        self.firstTableRequestedNewTable == FIRST_TABLE_REQUESTED_TO_JOIN_NEWTABLE)
    {
        self.isFirstGameScheduled = YES;
        self.gameScheduleFirstTableTime = [[NSDate date] timeIntervalSince1970];
        self.firstGameScheduleDictionary = [NSDictionary dictionaryWithDictionary:dictionary];
        
    }
    
    else if(self.secondTableGameLoad &&
            [self.secondTableGameLoad isRequestedToJoinNewTable] &&
            self.secondTableRequestedNewTable == SECOND_TABLE_REQUESTED_TO_JOIN_NEWTABLE)
    {
        self.isSecondGameScheduled = YES;
        
        self.gameScheduleSecondTableTime = [[NSDate date]timeIntervalSince1970];
        self.secondGameScheduleDictionary = [NSDictionary dictionaryWithDictionary:dictionary];
    }
}

- (void)handleJoinNewTable:(NSDictionary *)dictionary
{
    NSLog(@"handleJoinNewTable :%@",dictionary);
    if (!dictionary)
    {
        return;
    }
    
    [self makePlayerToJoinNewTable:dictionary];
}

// join new table
- (void)makePlayerToJoinNewTable:(NSDictionary *)dictionary
{
    NSLog(@"makePlayerToJoinNewTable %@",dictionary);
    
    //    DLog(@"get_table_details after search_join_table response-- dict %@",dictionary);
    
    if (self.firstTableGameLoad &&
        [self.firstTableGameLoad isRequestedToJoinNewTable] &&
        self.firstTableRequestedNewTable == FIRST_TABLE_REQUESTED_TO_JOIN_NEWTABLE)
    {
        [self removeFirstGameTable];
        
        TAJGameEngine *gameEngine = [TAJGameEngine sharedGameEngine];
        gameEngine.lobby.firstTable.gameTableDetails = (NSMutableDictionary *) dictionary;
        
        [self addFirstGameTable:dictionary];
        self.firstTableRequestedNewTable = 0;
        
        TAJLobby *lobby = [[TAJGameEngine sharedGameEngine] lobby];
        
        lobby.firstTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        
        if (self.isFirstGameScheduled)
        {
            double currentTime = [self.firstGameScheduleDictionary[GAME_TIMER_STAMP] doubleValue];
            double endTime = [self.firstGameScheduleDictionary[GAME_START_TIMER] doubleValue];
            
            double finalTimer = endTime -  currentTime;
            double difference = [[NSDate date] timeIntervalSince1970] - self.gameScheduleFirstTableTime;
            
            self.firstGameScheduleTime = finalTimer - difference;
            self.isFirstGameScheduled = NO;
            
            [self.firstTableGameLoad startWaitingTimerForDuration:self.firstGameScheduleTime andData:self.firstGameScheduleDictionary];
        }
    }
    else if(self.secondTableGameLoad &&
            [self.secondTableGameLoad isRequestedToJoinNewTable] &&
            self.secondTableRequestedNewTable == SECOND_TABLE_REQUESTED_TO_JOIN_NEWTABLE)
    {
        [self removeSecondGameTable];
        
        TAJGameEngine *gameEngine = [TAJGameEngine sharedGameEngine];
        gameEngine.lobby.secondTable.gameTableDetails = (NSMutableDictionary *) dictionary;
        
        [self addSecondGameTable:dictionary];
        self.secondTableRequestedNewTable = 0;
        
        TAJLobby *lobby = [[TAJGameEngine sharedGameEngine] lobby];
        
        lobby.secondTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        
        if (self.isSecondGameScheduled)
        {
            double currentTime = [self.secondGameScheduleDictionary[GAME_TIMER_STAMP] doubleValue];
            double endTime = [self.secondGameScheduleDictionary[GAME_START_TIMER] doubleValue];
            
            double finalTimer = endTime - currentTime;
            double difference = [[NSDate date] timeIntervalSince1970] - self.gameScheduleSecondTableTime;
            self.secondGameScheduleTime = finalTimer - difference;
            self.isSecondGameScheduled = NO;
            
            [self.secondTableGameLoad startWaitingTimerForDuration:self.secondGameScheduleTime andData:self.secondGameScheduleDictionary];
        }
    }
    
#if NEW_DESIGN_IMPLEMENTATION
    if ([self.switchDelegate respondsToSelector:@selector(isSwitchLobbyShowing)])
    {
        [self.switchDelegate isSwitchLobbyShowing];
    }
#endif
}

#pragma mark - Remove game table instance once quit -

//actual function for table quit from server side
- (void)removeGameTableInstane:(NSDictionary *)dictionary
{
    int tableID = [dictionary[TAJ_TABLE_ID] integerValue];
    
    if (tableID == self.alreadyFirstTableID)
    {
        if (self.firstTableGameLoad)
        {
            [self removeFirstGameTable];
        }
    }
    if (tableID == self.alreadySecondTableID)
    {
        if (self.secondTableGameLoad)
        {
            [self removeSecondGameTable];
        }
    }
}

//tableIDForQuit function is temorary for table quit from manualy we are quitting table
-(void)tableIDForQuit:(int)tableID
{
    if (tableID == self.alreadyFirstTableID)
    {
        if (self.firstTableGameLoad)
        {
            [self removeFirstGameTable];
            
        }
    }
    else if (tableID == self.alreadySecondTableID)
    {
        if (self.secondTableGameLoad)
        {
            [self removeSecondGameTable];
        }
    }
    
    if(self.counter == 0)
    {
        [self.switchDelegate disableTablesButton];
    }
}

#pragma mark - IBActions -

//action to load lobby when player is playing games
- (IBAction)lobbyButtonAction:(UIButton *)sender
{
    if (!sender.selected)
    {
        self.lobbyButton.selected = !sender.selected;
    }
    [self loadTableViewAction];
    
    self.viewTwoTableAlert.hidden = YES;
}

//stop blinking of first table button
- (void)stopFirstTableBlinkingAnimation
{
    self.isFirstButtonBlinking = YES;
    [UIView animateWithDuration:0.12
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.firstTableBlinkView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         self.firstTableBlinkView.hidden = YES;
                     }];
    
}

//stop blinking of second table button
- (void)stopSecondTableBlinkingAnimation
{
    self.isSecondButtonBlinking = YES;
    [UIView animateWithDuration:0.12
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut |
     UIViewAnimationOptionBeginFromCurrentState
                     animations:^{
                         self.secondTableBlinkView.alpha = 1.0f;
                     }
                     completion:^(BOOL finished){
                         self.secondTableBlinkView.hidden = YES;
                         // Do nothing
                     }];
    
}

- (IBAction)firstTableButtonAction:(UIButton *)sender
{
    if (!sender.selected)
    {
        self.firstTableButton.selected = !sender.selected;
    }
    if(self.secondTableGameLoad.isThisDeviceTurn)
    {
        self.isSecondButtonBlinking = YES;
        self.secondTableBlinkView.hidden = NO;
        [UIView animateWithDuration:0.35
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut |
         UIViewAnimationOptionRepeat |
         UIViewAnimationOptionAutoreverse |
         UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.secondTableBlinkView.alpha = 0.0f;
                         }
                         completion:^(BOOL finished){
                             // Do nothing
                         }];
        
    }
    [self stopFirstTableBlinkingAnimation];
    
    [self loadFirstTable];
}

- (IBAction)secondTableButtonAction:(UIButton *)sender
{
    if (!sender.selected)
    {
        self.secondTableButton.selected = !sender.selected;
    }
    if(self.firstTableGameLoad.isThisDeviceTurn)
    {
        self.firstTableBlinkView.hidden = NO;
        self.isFirstButtonBlinking = YES;
        [UIView animateWithDuration:0.35
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut |
         UIViewAnimationOptionRepeat |
         UIViewAnimationOptionAutoreverse |
         UIViewAnimationOptionAllowUserInteraction
                         animations:^{
                             self.firstTableBlinkView.alpha = 0.0f;
                         }
                         completion:^(BOOL finished){
                             // Do nothing
                         }];
        
    }
    
    [self stopSecondTableBlinkingAnimation];
    [self loadSecondTable];
}

#pragma mark - Table Control action

- (void)loadTableViewAction
{
    
#if NEW_DESIGN_IMPLEMENTATION
    
    if (self.switchDelegate && [self.switchDelegate respondsToSelector:@selector(showTableViewLobby)])
    {
        [self.switchDelegate showTableViewLobby];
    }
    [self loadLobbyTable];
    
#else
    
    self.firstTableButton.selected = NO;
    self.secondTableButton.selected = NO;
    self.isLobbyVisible = YES;
    [self.lobbyButton setBackgroundColor:[UIColor clearColor]];
    
    self.firstGameTable.hidden = YES;
    self.firstGameTable.userInteractionEnabled = NO;
    self.secondGameTable.hidden = YES;
    self.secondGameTable.userInteractionEnabled = NO;
    self.lobbyTableView.hidden = NO;
    self.lobbyTableView.userInteractionEnabled = YES;
    
    [self.tableViewLoad alreadyFilter:self.isFilteredData
                         withGameType:_gameTypeSearch
                       withPlayerType:_playerTypeSearch
                          withBetType:_betTypeSearch
                        withTableCost:_tableCostSearch
                          withViewTye:_viewTypeSearch];
    
    [self.tableViewLoad reloadTableView];
    
    [self loadLobbyTable];
    
#endif
    
}

- (void)loadLobbyTable
{
    if (!self.firstTableButton.hidden)
    {
        self.firstTableButton.enabled=false;
        
        NSString * tableId = [NSString stringWithFormat:@"%d",self.alreadyFirstTableID];
        NSString *trimmedString=[tableId substringFromIndex:MAX((int)[tableId length]-4, 0)]; //in case string is less than 4 characters long.
        [self.firstTableButtonLabel setText:[NSString stringWithFormat:@"%@",trimmedString]];
        //[self.firstTableButtonLabel setText:[NSString stringWithFormat:@"Table"]];
//        [self.firstTableButtonLabel setText:[NSString stringWithFormat:@"Table %d",self.alreadyFirstTableID]];
        self.firstTableButton.enabled=true;
        
    }
    if (!self.secondTableButton.hidden)
    {
        self.secondTableButton.enabled=false;
        
        NSString * tableId = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
        NSString *trimmedString=[tableId substringFromIndex:MAX((int)[tableId length]-4, 0)]; //in case string is less than 4 characters long.
        
        [self.secondTableButtonLabel setText:[NSString stringWithFormat:@"%@",trimmedString]];
        //self.secondTableButtonLabel.text = @"Table";
        
        self.secondTableButton.enabled=true;
        
    }
    
    self.currentTableID = LOBBY_TABLE_ID_DEFAULT;
    
#if NEW_DESIGN_IMPLEMENTATION
#else
    self.tableViewLoad.isClicked = YES;
    [self.tableViewLoad.listGameTable reloadData];
    
    //set navigation bar hidden
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    
    [self.view bringSubviewToFront:self.lobbyTableView];
    [self bringAllButtonsToFront];
#endif
    
}

#pragma mark - First table Control action

- (void)loadFirstTable
{
    //if(self.counter )
    
    if (self.firstTableGameLoad)
    {
        self.counter = 0;
       // DLog(@"L Self.counter B = %d",self.counter);
        ++self.counter;
       // DLog(@"L Self.counter A = %d",self.counter);
        
        TAJGameEngine *gameEngine = [TAJGameEngine sharedGameEngine];
        gameEngine.lobby.runningTableType = eOpenTableFirst;
        [self.switchDelegate enableTablesButton];
        //NSLog(@"FIRST COUNT : %ld",(long)self.counter);
        //APP_DELEGATE.gameTableIndex = 1;//ratheesh
    }
    self.currentTableID = self.alreadyFirstTableID;
    [self firstGameTableControlAction];
    [self isPlayerJonedAsSpecatorForTwoTable];
}

- (void)firstGameTableControlAction
{
    self.firstTableButton.hidden = NO;
    self.firstTableButtonLabel.hidden = NO;
    self.firstTableImage.hidden = NO;
    
    self.firstTableButton.selected = YES;
    self.lobbyButton.selected = NO;
    self.secondTableButton.selected = NO;
    self.isLobbyVisible = NO;
    [self.firstTableButton setBackgroundColor:[UIColor clearColor]];
    self.firstTableButton.enabled=false;

    NSString * tableId = [NSString stringWithFormat:@"%d",self.alreadyFirstTableID];
    NSString *trimmedString=[tableId substringFromIndex:MAX((int)[tableId length]-4, 0)]; //in case string is less than 4 characters long.
    [self.firstTableButtonLabel setText:[NSString stringWithFormat:@"%@",trimmedString]];
    //self.firstTableButtonLabel.text = @"Table";
    
    self.firstTableButton.enabled=true;
    self.lobbyTableView.hidden = YES;
    self.lobbyTableView.userInteractionEnabled = NO;
    self.secondGameTable.hidden = YES;
    self.secondGameTable.userInteractionEnabled = NO;
    self.firstGameTable.hidden = NO;
    self.firstGameTable.userInteractionEnabled = YES;
    
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    [self.view bringSubviewToFront:self.firstGameTable];
    if(self.secondTableGameLoad)
    {
        [self.view bringSubviewToFront:self.secondGameTable];
    }
    
    if (self.secondTableGameLoad)
    {
        self.secondTableButton.frame = self.secondButtonRect;
        self.secondTableImage.frame = self.secondButtonTableImageRect;
        self.secondTableButtonLabel.frame = self.secondButtonLabelRect;
    }
}

- (void)addFirstGameTable:(NSDictionary *)dictionary
{
    NSLog(@"FIRST G INFO : %@",dictionary);
    //create instance for game table
    
    self.firstTableGameLoad = [self initiateGameTableWithController:self.firstTableGameLoad];
    self.firstTableGameLoad.playTableDictionary = [dictionary mutableCopy];
    
    //set delegate to know the table id for table quit
    self.firstTableGameLoad.gameTableDelegate = self;
    
    NSString *firstTableID = [NSString stringWithFormat:@"%@",[dictionary valueForKey:@"TAJ_table_id"]];
    NSLog(@"PRINT FIRST TABLE ID : %@, %d",firstTableID,self.alreadyFirstTableID);
    [[NSUserDefaults standardUserDefaults] setObject:[NSString stringWithFormat:@"%@",firstTableID] forKey:FIRSTTABLEID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //add to view
    [self.firstGameTable addSubview:self.firstTableGameLoad.view];
    
    [self.firstTableGameLoad initializeGameTableController];
    self.alreadyFirstTableID = [self.firstTableGameLoad gameTableCurrentID];
    
    
    
    //[self saveToCoreDataWithTableId:[NSString stringWithFormat:@"%d",self.alreadyFirstTableID] isFirstTabel:YES];
    
    self.firstTablePlayers = [[dictionary valueForKey:@""] integerValue];
    //load first game table
    [self loadFirstTable];
    
    int count = 0;
    int userId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] intValue];

    //NSLog(@"SWITCH CHECK INFO %@",dictionary);
    //NSLog(@"SWITCH CHECK alreadyFirstTableID %d", self.alreadyFirstTableID);
    NSDictionary * playersDict = [dictionary objectForKey:@"table_details"][@"players"];
    int maxPlayers = [[dictionary objectForKey:@"table_details"][@"TAJ_maxplayer"] intValue];
    
    if ([playersDict count] < 0 || ( playersDict == NULL ) || (playersDict == (NSDictionary*) [NSNull null]))
    {
        count = 1;
    }
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSDictionary class]]) {
        int tajUserId = [[playersDict objectForKey:@"player"][@"TAJ_user_id"] intValue];
        if (userId == tajUserId) {
            count = 1;
        }
        else {
        count = 2;
        }
    }
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSArray class]]){
        NSArray * players = [playersDict objectForKey:@"player"];
        
        int foundUserId = NO;
        
        for (int i = 0; i < players.count; i++)
        {
            int tajUserId = [[players valueForKey:@"TAJ_user_id"][i] intValue] ;
            NSLog(@"userId :%d",userId);
            NSLog(@"loginUserId :%d",tajUserId);
            //NSLog(@"kTaj_scoresheet_userid :%@",userId);
            if (userId == tajUserId) {
                foundUserId = YES;
            }
            
        }
        
        if (foundUserId) {
            count = (int)[players count];
        }
        else {
            count = (int)[players count] + 1;
        }
    }
    
    //NSLog(@"CHECK players : %@",playersDict);
    //    NSArray * player = [[dictionary objectForKey:@"table_details"][@"players"]];
    
    NSDictionary * playersCountDict = @{
                                        @"table_id" : [dictionary objectForKey:@"table_details"][@"TAJ_table_id"],
                                        @"maxplayer" : [NSNumber numberWithInt:maxPlayers],
                                        @"count" : [NSNumber numberWithInt:count]
                                        };
    //NSLog(@"FIRST PLAYE POST DATA : %@",playersCountDict);
    
    [self playersStatusOnTable:playersCountDict];
    
    
}

- (void)removeFirstGameTable
{
    if (self.firstTableGameLoad)
    {
        [self.firstTableGameLoad.chatViewController clearChatHistory];
        [self.firstTableChatExtentView clearChatHistory];
        DLog(@"R-Self.counter B = %d",self.counter);
        --self.counter;
        DLog(@"R-Self.counter A = %d",self.counter);
        [self.firstTableGameLoad.view removeFromSuperview];
        [self.firstTableGameLoad removeFromParentViewController];
        self.firstTableGameLoad = nil;
        //move to table
        [self.firstTableIAmBackController.view removeFromSuperview];
        self.firstTableIAmBackController = nil;
        self.alreadyFirstTableID = (int)nil;
        
        self.firstTableButton.hidden = YES;
        self.firstTableButtonLabel.hidden = YES;
        self.firstTableImage.hidden = YES;
        
        //stop blink
        self.secondTableBlinkView.hidden = YES;
        
        
        if (self.secondTableGameLoad)
        {
            [self loadSecondTable];
        }
        else
        {
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            engine.isListTableRequested = NO;
            [self loadTableViewAction];
        }
        if(self.counter == 0)
        {
            [self.switchDelegate disableTablesButton];
        }
        
    }
    if(!self.firstTableGameLoad && !self.secondTableGameLoad)
    {
        [self.switchDelegate disableTablesButton];
    }
    if (self.secondTableGameLoad)
    {
        self.secondTableButton.frame = self.firstButtonRect;
        self.secondTableImage.frame = self.firstButtonTableImageRect;
        self.secondTableButtonLabel.frame = self.firstButtonLabelRect;
    }
}

#pragma mark - Second table Control action -

- (void)loadSecondTable
{
    if (self.secondTableGameLoad)
    {
        DLog(@"self.counter = %d",self.counter);
        ++self.counter;
        DLog(@"self.counter = %d",self.counter);
        TAJGameEngine *gameEngine = [TAJGameEngine sharedGameEngine];
        gameEngine.lobby.runningTableType = eOpenTableSecond;
        [self.switchDelegate enableTablesButton];
    }
    
    self.currentTableID = self.alreadySecondTableID;
    [self secondGameTableControlAction];
    
    [self isPlayerJonedAsSpecatorForTwoTable];
}

- (void)secondGameTableControlAction
{
    self.secondTableButton.hidden = NO;
    self.secondTableButtonLabel.hidden = NO;
    self.secondTableImage.hidden = NO;
    
    self.secondTableButton.selected = YES;
    self.lobbyButton.selected = NO;
    self.firstTableButton.selected = NO;
    self.isLobbyVisible = NO;
    [self.secondTableButton setBackgroundColor:[UIColor clearColor]];
    self.secondTableButton.enabled=false;
    
    //[self.secondTableButtonLabel setText:[NSString stringWithFormat:@"%d",self.alreadySecondTableID]];
    
    NSString * tableId = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
    NSString *trimmedString=[tableId substringFromIndex:MAX((int)[tableId length]-4, 0)]; //in case string is less than 4 characters long.
    
    [self.secondTableButtonLabel setText:[NSString stringWithFormat:@"%@",trimmedString]];
    
    //self.secondTableButtonLabel.text = @"Table";
    self.secondTableButton.enabled=true;
    
    self.lobbyTableView.hidden = YES;
    self.lobbyTableView.userInteractionEnabled = NO;
    self.firstGameTable.hidden = YES;
    self.firstGameTable.userInteractionEnabled = NO;
    self.secondGameTable.hidden = NO;
    self.secondGameTable.userInteractionEnabled = YES;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self.view bringSubviewToFront:self.secondGameTable];
    
    if (self.firstTableGameLoad)
    {
        self.firstTableButton.frame = self.firstButtonRect;
        self.firstTableImage.frame = self.firstButtonTableImageRect;
        self.firstTableButtonLabel.frame = self.firstButtonLabelRect;
    }
    if (self.firstTableGameLoad && self.secondTableGameLoad)
    {
        self.secondTableButton.frame = self.secondButtonRect;
        self.secondTableImage.frame = self.secondButtonTableImageRect;
        self.secondTableButtonLabel.frame = self.secondButtonLabelRect;
    }
    else if(self.secondTableGameLoad)
    {
        self.secondTableButton.frame = self.firstButtonRect;
        self.secondTableImage.frame = self.firstButtonTableImageRect;
        self.secondTableButtonLabel.frame = self.firstButtonLabelRect;
    }
}

- (void)addSecondGameTable:(NSDictionary *)dictionary
{
    NSLog(@"SECOND G INFO : %@",dictionary);
    
    //create instance for game table
    self.secondTableGameLoad = [self initiateGameTableWithController:self.secondTableGameLoad];
    
    self.secondTableGameLoad.playTableDictionary = [dictionary mutableCopy];
    
    //set delegate to know the table id for table quit
    self.secondTableGameLoad.gameTableDelegate = self;
    //add to view
    [self.secondGameTable addSubview:self.secondTableGameLoad.view];
    
    [self.secondTableGameLoad initializeGameTableController];
    
    self.alreadySecondTableID = [self.secondTableGameLoad gameTableCurrentID];
    
    self.secondTableId = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
    
    [[NSUserDefaults standardUserDefaults] setObject:self.secondTableId forKey:SECONDTABLEID];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    //[self saveToCoreDataWithTableId:[NSString stringWithFormat:@"%d",self.alreadySecondTableID] isFirstTabel:NO];
    
    //load second game table
    [self loadSecondTable];
    
    int count = 0;
    int userId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] intValue];
    //NSLog(@"SWITCH SECOND INFO %@",dictionary);
    NSLog(@"SWITCH SECOND alreadyFirstTableID %d", self.alreadyFirstTableID);
    NSDictionary * playersDict = [dictionary objectForKey:@"table_details"][@"players"];
    int maxPlayers = [[dictionary objectForKey:@"table_details"][@"TAJ_maxplayer"] intValue];
    
    if ([playersDict count] < 0 || ( playersDict == NULL ) || (playersDict == (NSDictionary*) [NSNull null]))
    {
        count = 1;
    }
    
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSDictionary class]]) {
        int tajUserId = [[playersDict objectForKey:@"player"][@"TAJ_user_id"] intValue];
        if (userId == tajUserId) {
            count = 1;
        }
        else {
            count = 2;
        }
        
    }
    
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSArray class]]){
        NSArray * players = [playersDict objectForKey:@"player"];
        int foundUserId = NO;
        
        for (int i = 0; i < players.count; i++)
        {
            int tajUserId = [[players valueForKey:@"TAJ_user_id"][i] intValue] ;
            NSLog(@"userId :%d",userId);
            NSLog(@"loginUserId :%d",tajUserId);

            if (userId == tajUserId) {
                foundUserId = YES;
            }
            
        }
        
        if (foundUserId) {
            count = (int)[players count];
        }
        else {
            count = (int)[players count] + 1;
        }
    }
    
    //NSLog(@"CHECK players : %@",playersDict);
    
    NSDictionary * playersCountDict = @{
                                        @"table_id" : [dictionary objectForKey:@"table_details"][@"TAJ_table_id"],
                                        @"maxplayer" : [NSNumber numberWithInt:maxPlayers],
                                        @"count" : [NSNumber numberWithInt:count]
                                        };
    //NSLog(@"FIRST PLAYE POST DATA : %@",playersCountDict);
    
    [self playersStatusOnTable:playersCountDict];
    
}

- (void)removeSecondGameTable
{
    if (self.secondTableGameLoad)
    {
        [self.secondTableGameLoad.chatViewController clearChatHistory];
        [self.secondTableChatExtentView clearChatHistory];
        --self.counter;
        [self.secondTableGameLoad.view removeFromSuperview];
        [self.secondTableGameLoad removeFromParentViewController];
        self.secondTableGameLoad = nil;
        [self.secondTableIAmBackController.view removeFromSuperview];
        self.secondTableIAmBackController = nil;
        //move to table
        self.alreadySecondTableID = (int)nil;
        
        self.secondTableButton.hidden = YES;
        self.secondTableButtonLabel.hidden = YES;
        self.secondTableImage.hidden = YES;
        
        //stop blink
        self.firstTableBlinkView.hidden = YES;
        
        
        if (self.firstTableGameLoad)
        {
            [self loadFirstTable];
        }
        else
        {
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            engine.isListTableRequested = NO;
            [self loadTableViewAction];
        }
        
        if(self.counter == 0)
        {
            [self.switchDelegate disableTablesButton];
        }
        
        
    }
    if(!self.firstTableGameLoad && !self.secondTableGameLoad)
    {
        [self.switchDelegate disableTablesButton];
    }
}

#pragma mark - Identify user joined table -

- (void)checkPlayerAlreadyInGameTable
{
    
#if NEW_DESIGN_IMPLEMENTATION
    if ([self.switchDelegate respondsToSelector:@selector(isSwitchLobbyShowing)])
    {
        [self.switchDelegate isSwitchLobbyShowing];
    }
#endif
    
    // Init game play table
    if (!self.alreadyFirstTableID && !self.firstTableGameLoad)
    {
        [self addFirstGameTable:[[[[TAJGameEngine sharedGameEngine]lobby ]firstTable ]gameTableDetails]];
       // DLog(@"first table %@",[[[[TAJGameEngine sharedGameEngine]lobby ]firstTable ]gameTableDetails]);
    }
    
    else if(!self.alreadySecondTableID && !self.secondTableGameLoad)
    {
        [self addSecondGameTable:[[[[TAJGameEngine sharedGameEngine]lobby ]secondTable ]gameTableDetails]];
        DLog(@"second table %@",[[[[TAJGameEngine sharedGameEngine]lobby ]secondTable ]gameTableDetails]);
        
    }
    
    else if (self.tableIdFromTable == self.alreadyFirstTableID)
    {
        //if already in first table
        //load first game table
        
        [self loadFirstTable];
    }
    
    else if (self.tableIdFromTable == self.alreadySecondTableID)
    {
        //if already in second table
        //load second game table
        
        [self loadSecondTable];
    }
    
    else
    {
        NSLog(@"CHECKING 2 TABLE LIMIT");
        
       self.viewTwoTableAlert.hidden = NO;
        
//#if NEW_DESIGN_IMPLEMENTATION
//
//        if (self.switchDelegate && [self.switchDelegate respondsToSelector:@selector(displayPlayerTwoTableReached)])
//        {
//            [self.switchDelegate displayPlayerTwoTableReached];
//        }
//
//        [self loadLobbyTable];
//
//        if (self.switchDelegate && [self.switchDelegate respondsToSelector:@selector(showTableViewLobby)])
//        {
//            [self.switchDelegate showTableViewLobby];
//        }
//
//
//#endif
    }
}

#pragma mark - Create game table instance if player not joined -

- (TAJGameTableController *)initiateGameTableWithController:(TAJGameTableController *)gameTableController
{
    gameTableController = [[TAJGameTableController alloc] initWithNibName:@"TAJGameTableController" bundle:nil];
    
    if([TAJUtilities isIPhone]) {
        if (APP_DELEGATE.gameTableIndex == 1) {
            APP_DELEGATE.gameTableIndex = 0;
            APP_DELEGATE.tablesCount = 1;
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            CGRect frame = gameTableController.view.frame;
            //        frame.size.width = screenSize.height;
            //        frame.size.height = screenSize.width;
            frame.size.width = 568;
            frame.size.height = 320;
#if DEBUG
            NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
            NSLog(@"APP DELEGATE 1");
#endif
            gameTableController.view.frame = frame;
            
        }
        else if (APP_DELEGATE.gameTableIndex == 2) {
            APP_DELEGATE.gameTableIndex = 0;
            CGFloat width = 0;
            CGFloat height = 0;
            if ([TAJUtilities isItIPhone6]) {
                width = 375;
                height = 667;
            }
            else if ([TAJUtilities isItIPhone6Plus]) {
                width = 414;
                height = 736;
            }
            else if ([TAJUtilities isItIPhonex]) {
                width = 375 - 20;
                height = 812 - 88;
            }
            else if ([TAJUtilities isItIPhoneXSMax]) {
                width = 414 - 20;
                height = 896 - 88;
            }
            
            CGRect frame = gameTableController.view.frame;
            frame.size.width = height;
            frame.size.height = width;
#if DEBUG
            NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
            NSLog(@"APP DELEGATE 2");
#endif
            gameTableController.view.frame = frame;
            
        }
        else {
            APP_DELEGATE.gameTableIndex = 0;
            APP_DELEGATE.tablesCount = 2;
            
            CGFloat width = 0;
            CGFloat height = 0;
            CGSize screenSize = [[UIScreen mainScreen] bounds].size;
            CGRect frame = gameTableController.view.frame;
            if ([TAJUtilities isItIPhone6]) {
                width = 375;
                height = 667;
#if DEBUG
                NSLog(@"APP DELEGATE 0 iPHONE 6");
                NSLog(@"iPHONE 6");
                NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
#endif
            }
            
            else if ([TAJUtilities isItIPhonex]) {
                height = 896 - 80;
                width = 297;
//                width = screenSize.width;
//                height = 297;
#if DEBUG
                NSLog(@"APP DELEGATE 0 iPHONE X");
                NSLog(@"iPHONE X");
                NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
#endif
            }
            else if ([TAJUtilities isItIPhone6Plus]) {
                width = 414;
                height = 736;
#if DEBUG
                NSLog(@"APP DELEGATE 0 iPHONE Plus");
                NSLog(@"iPHONE Plus");
                NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
#endif
            }
            
            else if ([TAJUtilities isItIPhoneXSMax]) {
                //width = screenSize.width;
//                width = 414;
//                height = 336;
                height = 896;
                width = 336;
#if DEBUG
                NSLog(@"APP DELEGATE 0 iPHONE XSMAX");
                NSLog(@"iPHONE XSMAX");
                NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
#endif
            }
            else {
                width = screenSize.width;
                height = screenSize.height;
                
#if DEBUG
                NSLog(@"ELSE APP DELEGATE 0 NOT iPHONE X");
                NSLog(@"ELSE Not iPHONE X");
                NSLog(@"CHECK game frame WIDTH %f , game frame HEIGHT %f",frame.size.width,frame.size.height);
#endif
            }
            
            frame.size.width = height;
            frame.size.height = width;
            gameTableController.view.frame = frame;
            
        }
    }
    
    [self addChildViewController:gameTableController];
    
    // Add as subview to container
    
    return gameTableController;
    NSLog(@"GAME TESTING");
}

#pragma mark - Table view delegate

- (BOOL)canJoinView
{
    if (self.firstTableGameLoad && self.secondTableGameLoad)
    {
        return NO;
    }
    return YES;
}

- (void)loadGameTable:(NSDictionary *)tableDictionary
{
    NSLog(@"TAB DICT : %@",tableDictionary);
    self.tableIdFromTable = [tableDictionary[TAJ_TABLE_ID] integerValue];
    
    [self checkPlayerAlreadyInGameTable];
}

- (BOOL)ifSingleTableJoined:(NSDictionary *)tableDictionary
{
    self.tableIdFromTable = [tableDictionary[TAJ_TABLE_ID] integerValue];
    if (self.tableIdFromTable == self.alreadyFirstTableID)
    {
        return YES;
    }
    else if (self.tableIdFromTable == self.alreadySecondTableID)
    {
        return YES;
    }
    return NO;
}


- (void)canJoinSingleTable
{
    if (self.tableIdFromTable == self.alreadyFirstTableID)
    {
        [self loadFirstTable];
    }
    else if (self.tableIdFromTable == self.alreadySecondTableID)
    {
        [self loadSecondTable];
    }
    
#if NEW_DESIGN_IMPLEMENTATION
    if ([self.switchDelegate respondsToSelector:@selector(isSwitchLobbyShowing)])
    {
        [self.switchDelegate isSwitchLobbyShowing];
    }
#endif
    
}

- (BOOL)isPlayerJoinedCompleteTwoTable
{
    BOOL isJoined = [self canJoinView];
    return isJoined;
}

- (void)loadGameTableForDictionary:(NSDictionary *)dictionary
{
    [self loadGameTable:dictionary];
}

- (BOOL)isSingleTableJoinedWithDictionary:(NSDictionary *)dictionary
{
    BOOL isSingle = [self ifSingleTableJoined:dictionary];
    return isSingle;
}

- (void)loadSingleTable
{
    [self canJoinSingleTable];
}

#pragma mark - Game play table delegate -

- (void)newTableRequested:(int)tableID
{
    if (self.firstTableGameLoad &&
        [self.firstTableGameLoad isRequestedToJoinNewTable] &&
        self.alreadyFirstTableID == tableID)
    {
        self.firstTableRequestedNewTable = FIRST_TABLE_REQUESTED_TO_JOIN_NEWTABLE;
    }
    else if(self.secondTableGameLoad &&
            [self.secondTableGameLoad isRequestedToJoinNewTable] &&
            self.alreadySecondTableID == tableID)
    {
        self.secondTableRequestedNewTable = SECOND_TABLE_REQUESTED_TO_JOIN_NEWTABLE;
    }
}

- (BOOL)isLobbyScreenShowing:(int)tableID
{
    if (self.currentTableID == tableID)
    {
        return YES;
    }
    else
    {
        return NO;
    }
}

- (void)highLightButtonWhenMyTurn:(int)tableID
{
    if (self.currentTableID == LOBBY_TABLE_ID_DEFAULT)
    {
        // if player is not in game play screen
        if (self.alreadyFirstTableID == tableID)
        {
            [self loadFirstTable];
        }
        else if (self.alreadySecondTableID == tableID)
        {
            [self loadSecondTable];
        }
    }
    else
    {
        // if player in game play screen only
        if (self.alreadyFirstTableID == tableID)
        {
            self.firstTableBlinkView.hidden = NO;
            self.isFirstButtonBlinking = YES;
            [UIView animateWithDuration:0.35
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut |
             UIViewAnimationOptionRepeat |
             UIViewAnimationOptionAutoreverse |
             UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 self.firstTableBlinkView.alpha = 0.0f;
                             }
                             completion:^(BOOL finished){
                                 // Do nothing
                             }];
            
        }
        if (self.alreadySecondTableID == tableID)
        {
            self.isSecondButtonBlinking = YES;
            self.secondTableBlinkView.hidden = NO;
            [UIView animateWithDuration:0.35
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut |
             UIViewAnimationOptionRepeat |
             UIViewAnimationOptionAutoreverse |
             UIViewAnimationOptionAllowUserInteraction
                             animations:^{
                                 self.secondTableBlinkView.alpha = 0.0f;
                             }
                             completion:^(BOOL finished){
                                 // Do nothing
                             }];
            
            
        }
    }
}

- (void)informGameEngineForGameStarted:(int)tableID
{
    TAJGameEngine *gameEngine = [TAJGameEngine sharedGameEngine];
    
    if (self.alreadyFirstTableID == tableID)
    {
        gameEngine.isFirstGameStarted = YES;
    }
    else if (self.alreadySecondTableID == tableID)
    {
        gameEngine.isSecondGameStarted = YES;
    }
}

- (void)informGameEngineForGameEnded:(int)tableID
{
    //NSLog(@"CHECK GAME END");
    
    TAJGameEngine *gameEngine = [TAJGameEngine sharedGameEngine];
    
    [gameEngine removeReconnectingAlert];
    
    if (self.alreadyFirstTableID == tableID)
    {
        gameEngine.isFirstGameStarted = NO;
    }
    else if (self.alreadySecondTableID == tableID)
    {
        gameEngine.isSecondGameStarted = NO;
    }
}

- (void)isPlayerJonedAsSpecatorForTwoTable
{
    if (self.firstTableGameLoad && self.secondTableGameLoad)
    {
        if ([self.firstTableGameLoad playerJoinedSpecator] && [self.secondTableGameLoad playerJoinedSpecator])
        {
            self.twoTableSpectator = YES;
        }
        else
        {
            self.twoTableSpectator = NO;
        }
    }
    else if (self.firstTableGameLoad && !self.secondTableGameLoad)
    {
        self.twoTableSpectator = [self.firstTableGameLoad playerJoinedSpecator];
    }
    else if (self.secondTableGameLoad && !self.firstTableGameLoad)
    {
        self.twoTableSpectator = [self.secondTableGameLoad playerJoinedSpecator];
    }
}

#pragma mark - I AM Back View Controller -

- (void)createFirstTableIAmBackView
{
    NSLog(@"IAM BACK FIRST");
    //[self saveToCoreDataWithEvent:@""];
    if (!self.firstTableIAmBackController)
    {

        self.firstTableIAmBackController = [[TAJIamBackViewController alloc]init];

        self.firstTableIAmBackController.iamBackModel =[[TAJIamBackModel alloc] initWithIamBackModel:nil];
        self.firstTableIAmBackController.iamBackDelegate = self;
        if(!self.secondTableGameLoad )
        {
            self.firstTableIAmBackController.view.frame = self.view.bounds;
            [self.view addSubview:self.firstTableIAmBackController.view];
            [self.view bringSubviewToFront:[[TAJGameEngine sharedGameEngine] activityAlertForReconnecting]];
        }
        self.firstTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
    }
    else
    {
        if(!self.secondTableGameLoad )
        {
            self.firstTableIAmBackController.view.frame = self.view.bounds;
            [self.view addSubview:self.firstTableIAmBackController.view];
            [self.view bringSubviewToFront:[[TAJGameEngine sharedGameEngine] activityAlertForReconnecting]];
            
        }
        self.firstTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
    }
    if(self.firstTableChatExtentView)
    {
        self.firstTableChatMessage = self.firstTableChatExtentView.chatExtentTextField.text;
        [self removeChatExtentViewController];
    }
    if (self.canShowIamback)
    {
        self.canShowIamback = NO;
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [engine iAmBackWithUserID:[[engine usermodel] userId]];
        self.firstTableGameLoad.isIamBackButtonPressed =YES;
        self.secondTableGameLoad.isIamBackButtonPressed = YES;
        [self.firstTableIAmBackController.view removeFromSuperview];
        [self.secondTableIAmBackController.view removeFromSuperview];
    }
    APP_DELEGATE.gameTableIndex = 0;//ratheesh
}

- (void)createSecondTableIAmBackView
{
    NSLog(@"IAM BACK SECOND");
    if (!self.secondTableIAmBackController)
    {
        self.secondTableIAmBackController = [[TAJIamBackViewController alloc]init];
        
        self.secondTableIAmBackController.iamBackModel =[[TAJIamBackModel alloc] initWithIamBackModel:nil];
        self.secondTableIAmBackController.iamBackDelegate = self;
        self.secondTableIAmBackController.view.frame = self.view.bounds;
        [self.view addSubview:self.secondTableIAmBackController.view];
        [self.view bringSubviewToFront:[[TAJGameEngine sharedGameEngine] activityAlertForReconnecting]];
        
        [self.firstTableIAmBackController.view removeFromSuperview];
        self.secondTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
        if (self.secondTableGameLoad)
        {
            [self secondTableButtonAction:self.secondTableButton];
        }
    }
    else
    {
        self.secondTableIAmBackController.view.frame = self.view.bounds;
        [self.view addSubview:self.secondTableIAmBackController.view];
        [self.view bringSubviewToFront:[[TAJGameEngine sharedGameEngine] activityAlertForReconnecting]];
        
        [self.firstTableIAmBackController.view removeFromSuperview];
        self.secondTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
        if (self.secondTableGameLoad)
        {
            [self secondTableButtonAction:self.secondTableButton];
        }
    }
    if(self.secondTableChatExtentView)
    {
        self.secondTableChatMessage = self.secondTableChatExtentView.chatExtentTextField.text;
        [self removeChatExtentViewController];
    }
    if (self.canShowIamback)
    {
        self.canShowIamback = NO;
        self.firstTableGameLoad.isIamBackButtonPressed =YES;
        self.secondTableGameLoad.isIamBackButtonPressed = YES;
        [self.firstTableIAmBackController.view removeFromSuperview];
        [self.secondTableIAmBackController.view removeFromSuperview];
    }
    
}

- (void)updateFirstTableIAmBackModel:(NSDictionary *)dictionary
{
    if (self.firstTableIAmBackController)
    {
        // update i am back model
        [self.firstTableIAmBackController.iamBackModel.thisPlayerNetworkCardsArray removeAllObjects];
        [self.firstTableIAmBackController.iamBackModel updateIamBackModel:dictionary];
        self.firstTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
    }
}

- (void)updateSecondTableIAmBackModel:(NSDictionary *)dictionary
{
    if (self.secondTableIAmBackController)
    {
        // update i am back model
        [self.secondTableIAmBackController.iamBackModel.thisPlayerNetworkCardsArray removeAllObjects];
        [self.secondTableIAmBackController.iamBackModel updateIamBackModel:dictionary];
        self.secondTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
    }
}

- (void)didQuitIAmBack
{
    if (self.isPlayerCanEnterGameInAutoPlayMode)
    {
        if (self.firstTableIAmBackController)
        {
            [self.firstTableIAmBackController.view removeFromSuperview];
            self.firstTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
        }
        if(self.secondTableIAmBackController)
        {
            [self.secondTableIAmBackController.view removeFromSuperview];
            self.secondTableIAmBackController.iamBackButton.userInteractionEnabled = YES;
        }
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [engine iAmBackWithUserID:[[engine usermodel] userId]];
        DLog(@"didquitiamback send autoplaystatus==--==---=------==-----------");
        if (self.firstTableGameLoad)
        {
            APP_DELEGATE.gameTableIndex = 0;//ratheesh
            self.firstTableGameLoad.isIamBackButtonPressed =YES;
            self.firstTableGameLoad.isAutoplayMode = NO;
        }
        if (self.secondTableGameLoad)
        {
            self.secondTableGameLoad.isIamBackButtonPressed =YES;
            self.secondTableGameLoad.isAutoplayMode = NO;
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_IAMBACKBUTTON_ALERT object:nil];
    }
}

- (BOOL)canShowIAmBackView
{
    if (self.firstTableGameLoad && self.secondTableGameLoad)
    {
        if ([self.firstTableGameLoad wantToShowIAmBack] || [self.secondTableGameLoad wantToShowIAmBack])
        {
            return YES;
        }
        else
        {
            return NO;
        }
    }
    else if (self.firstTableGameLoad && !self.secondTableGameLoad)
    {
        return [self.firstTableGameLoad wantToShowIAmBack];
    }
    else if (self.secondTableGameLoad && !self.firstTableGameLoad)
    {
        return [self.secondTableGameLoad wantToShowIAmBack];
    }
    else
    {
        return NO;
    }
}

- (void)canShowIAmBackView:(BOOL)boolValue
{
    self.canShowIamback = boolValue;
}

//checks whether a player can enter game when i am back screen is shown
- (void)canAllowUserToGamePlayInAutoPlayMode
{
    if (self.firstTableGameLoad && self.secondTableGameLoad)
    {
        self.isPlayerCanEnterGameInAutoPlayMode = ([self.firstTableGameLoad canAllowUserToGamePlay] && [self.secondTableGameLoad canAllowUserToGamePlay]);
    }
    else if (self.firstTableGameLoad)
    {
        self.isPlayerCanEnterGameInAutoPlayMode = [self.firstTableGameLoad canAllowUserToGamePlay];
    }
    else if (self.secondTableGameLoad)
    {
        self.isPlayerCanEnterGameInAutoPlayMode = [self.secondTableGameLoad canAllowUserToGamePlay];
    }
    else
    {
        self.isPlayerCanEnterGameInAutoPlayMode = YES;
    }
    
    // show message on i am back view controller
    if (self.firstTableIAmBackController)
    {
        if (self.isPlayerCanEnterGameInAutoPlayMode)
        {
           
             [self.firstTableIAmBackController showIAmBackMessage:I_AM_BUTTON_CAN_CLICK];
           
        }else{
            [self.firstTableIAmBackController showIAmBackMessage:I_AM_BUTTON_CANNOT_CLICK ];
            
        }
        
    }
    if (self.secondTableIAmBackController)
    {
        if (self.isPlayerCanEnterGameInAutoPlayMode)
        {
            [self.secondTableIAmBackController showIAmBackMessage:I_AM_BUTTON_CAN_CLICK];
        }else{
            [self.secondTableIAmBackController showIAmBackMessage:I_AM_BUTTON_CANNOT_CLICK];
        }
    }
}

// Get table extra information for this player reconnect implementation
- (void)handleGetTableExtraInformationEvent:(NSDictionary *)dictionary
{
    if (!dictionary)
    {
        return;
    }
    DLog(@"gett table extra came in switch lobby %@",dictionary);
    
    //If currently is in lobby then move to switch lobby game table controller where turn_update is the event and add I am back dialog
    //If currently is in GameTableController then show game table controller where turn_update is the event, add I am back dialog
    if ([dictionary[TAJ_TABLE_ID] integerValue] == self.alreadyFirstTableID)
    {
        if (self.firstTableGameLoad)
        {
            // decide whether to show i am back or not
            if (self.firstTableIAmBackController)
            {
                DLog(@"auto play count of first table switch lobby %d",self.firstTableIAmBackController.iamBackModel.currentAutoPlayCount);
                
                [self updateFirstTableIAmBackModel:dictionary];
                [self.firstTableIAmBackController updateAvatarImageForView:[self.firstTableGameLoad avatarImageForPlayerIAmBackView]];
                [self.firstTableIAmBackController  showDiscardCardInAutoPlayMode];
                self.firstTableGameLoad.isAutoplayMode = YES;
            }
            // update discard history
            [self.firstTableGameLoad playerReconnectedBackWithGetTableExtraEvent:dictionary];
        }
    }
    else if ([dictionary[TAJ_TABLE_ID] integerValue] == self.alreadySecondTableID)
    {
        if (self.secondTableGameLoad)
        {
            // decide whether to show i am back or not
            if (self.secondTableIAmBackController)
            {
                [self updateSecondTableIAmBackModel:dictionary];
                [self.secondTableIAmBackController updateAvatarImageForView:[self.secondTableGameLoad avatarImageForPlayerIAmBackView]];
                [self.secondTableIAmBackController  showDiscardCardInAutoPlayMode];
                self.secondTableGameLoad.isAutoplayMode = YES;
            }
            // update discard history
            [self.secondTableGameLoad playerReconnectedBackWithGetTableExtraEvent:dictionary];
        }
    }
}

// when ever server card discard we need to display in i am back view
- (void)cardDiscardedWhenAutoPlayMode:(NSDictionary *)dictionary
{
    NSString *tableID = [dictionary valueForKey:TAJ_TABLE_ID];
    if ([tableID isEqualToString:self.firstTableIAmBackController.iamBackModel.currentTableID])
    {
        if (self.firstTableIAmBackController.iamBackModel)
        {
            DLog(@"card discard in switch lobby first table");
            [self.firstTableIAmBackController.iamBackModel addDiscardCardForIamBackModel:dictionary];
        }
        
        if ([self.firstTableIAmBackController isIAMBackShowing])
        {
            DLog(@"card discard in switch lobby first table %@",self.firstTableIAmBackController.iamBackModel.thisPlayerNetworkCardsArray);
            [self.firstTableIAmBackController showDiscardCardInAutoPlayMode];
        }
        
        [[TAJGameEngine sharedGameEngine] getAutoPlayCountWithTableID:tableID];
    }
    if ([tableID isEqualToString:self.secondTableIAmBackController.iamBackModel.currentTableID])
    {
        DLog(@"card discard in switch lobby second table");
        if (self.secondTableIAmBackController.iamBackModel)
        {
            [self.secondTableIAmBackController.iamBackModel addDiscardCardForIamBackModel:dictionary];
        }
        if ([self.secondTableIAmBackController isIAMBackShowing])
        {
            [self.secondTableIAmBackController showDiscardCardInAutoPlayMode];
            DLog(@"card discard in switch lobby secomnd table %@",self.secondTableIAmBackController.iamBackModel.thisPlayerNetworkCardsArray);
        }
        [[TAJGameEngine sharedGameEngine] getAutoPlayCountWithTableID:tableID];
        
    }
    
}

// when ever server card pick we need to display in i am back view
- (void)cardPickedWhenAutoPlayMode:(NSDictionary *)dictionary
{
    NSString *tableID = [dictionary valueForKey:TAJ_TABLE_ID];
    NSString *firstTableID = [NSString stringWithFormat:@"%d",self.alreadyFirstTableID];
    NSString *secondTableID = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
    
    if ([tableID isEqualToString:firstTableID] && self.firstTableIAmBackController) {
        if (self.firstTableIAmBackController.iamBackModel)
        {
            [self.firstTableIAmBackController.iamBackModel addPickedCardForIamBackModel:dictionary];
        }
    }
    else if ([tableID isEqualToString:secondTableID] && self.secondTableIAmBackController)
    {
        if (self.secondTableIAmBackController.iamBackModel)
        {
            [self.secondTableIAmBackController.iamBackModel addPickedCardForIamBackModel:dictionary];
        }
    }
    
}

//Resetting Iam back Model when new game starts in Iam back  mode.
- (void)removeThisPlayerDiscardCardWhenScheduleGame:(NSString *)tableID
{
    if (self.firstTableIAmBackController && [tableID isEqualToString:self.firstTableIAmBackController.iamBackModel.currentTableID])
    {
        [self.firstTableIAmBackController resetIamBackModelForNewGame];
        if ([self.firstTableIAmBackController isIAMBackShowing])
        {
            [self.firstTableIAmBackController showDiscardCardInAutoPlayMode];
        }
    }
    if (self.secondTableIAmBackController  && [tableID isEqualToString:self.secondTableIAmBackController.iamBackModel.currentTableID])
    {
        [self.secondTableIAmBackController resetIamBackModelForNewGame];
        
        if ([self.secondTableIAmBackController isIAMBackShowing])
        {
            [self.secondTableIAmBackController showDiscardCardInAutoPlayMode];
        }
    }
    
    DLog(@"first table iamback model %@,%d,%@,%@",self.firstTableIAmBackController.iamBackModel.thisPlayerNetworkCardsArray,self.firstTableIAmBackController.iamBackModel.currentAutoPlayCount,tableID,self.firstTableIAmBackController.iamBackModel.currentTableID);
    DLog(@"seond table iamback model %@,%d,%@,%@",self.secondTableIAmBackController.iamBackModel.thisPlayerNetworkCardsArray,self.secondTableIAmBackController.iamBackModel.currentAutoPlayCount,tableID,self.secondTableIAmBackController.iamBackModel.currentTableID);
}

// display the no.of turns
- (void)devicePlayerInAutoGamePlay:(NSString *)autoPlayCount WithTotalAutoPlayCount:(NSString *)totalCount
{
    if (self.firstTableIAmBackController)
    {
        [self.firstTableIAmBackController updateAutoPlayCount:autoPlayCount withTotalCount:totalCount];
    }
    if (self.secondTableIAmBackController)
    {
        [self.secondTableIAmBackController updateAutoPlayCount:autoPlayCount withTotalCount:totalCount];
    }
}

// Get table details for player already in view / play implementation such cases like network reconnection,dual login, app forcequit
- (void)handleGetTableDetailsInformationEvent:(NSDictionary *)dictionary
{
    DLog(@"get table details event came %@",dictionary);
    [[TAJGameEngine sharedGameEngine] performSelector:@selector(showIamBackDialogRemovingReconnectingAlert) withObject:nil afterDelay:2.0];
#if NEW_DESIGN_IMPLEMENTATION
    if ([self.switchDelegate respondsToSelector:@selector(isSwitchLobbyShowing)])
    {
        [self.switchDelegate isSwitchLobbyShowing];
    }
#endif
    
    DLog(@"get table detailsfirst current table id %@",self.firstTableGameLoad.currentTableId);
    if (self.firstTableGameLoad && ([self.firstTableGameLoad gameTableCurrentID] == [[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE] integerValue]))
    {
        //update first game table when player goes to autoplay mode
        DLog(@"get table detailsfirst table %@",dictionary);
        if ([self canShowIAmBackView])
        {
            NSString *firstTableID = [NSString stringWithFormat:@"%d",self.alreadyFirstTableID];
            [[TAJGameEngine sharedGameEngine] getAutoPlayCountWithTableID:firstTableID];
            self.firstTableGameLoad.isIamBackButtonPressed = NO;
            [self createFirstTableIAmBackView];
            [self.firstTableGameLoad removeGameTableMessage];
            [self.firstTableGameLoad refreshControllerInGetTableDeatilsEvent:dictionary];
        }
    }
    else if (self.secondTableGameLoad && ([self.secondTableGameLoad gameTableCurrentID] == [[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE] integerValue]))
    {
        //update second game table when player goes to autoplay mode
        if ([self canShowIAmBackView])
        {
            NSString *secondTableID = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
            [[TAJGameEngine sharedGameEngine] getAutoPlayCountWithTableID:secondTableID];
            self.secondTableGameLoad.isIamBackButtonPressed = NO;
            [self createSecondTableIAmBackView];
            [self.secondTableGameLoad removeGameTableMessage];
            [self.secondTableGameLoad refreshControllerInGetTableDeatilsEvent:dictionary];
        }
    }
    // Init game play table
    else if (!self.firstTableGameLoad)
    {
        // this is for player already in view / play when app force quit
        BOOL isThisPlayerExist = NO;
        int loggedInUserID = [[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue];
        
        
        if (dictionary && [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS_FORCE] &&
            [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
        {
            // Only 1 player available
            NSMutableDictionary* onePlayerDict = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
            
            if (onePlayerDict[kTaj_userid_Key] &&
                ([onePlayerDict[kTaj_userid_Key] integerValue] == loggedInUserID))
            {
                isThisPlayerExist = YES;
            }
        }
        else if (dictionary &&
                 [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
        {
            // Get the players array
            NSArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
            
            // More than 1 player data available
            for (int i = 0; i < playersArray.count; i++)
            {
                NSMutableDictionary *playerDictionary = [playersArray objectAtIndex:i];
                
                if (playerDictionary[kTaj_userid_Key] &&
                    ([playerDictionary[kTaj_userid_Key] integerValue] == loggedInUserID))
                {
                    isThisPlayerExist = YES;
                    break;
                }
            }
        }
        
        if (isThisPlayerExist)
        {
            [self addFirstGameTable:dictionary];
            [self.firstTableGameLoad refreshControllerInGetTableDeatilsEvent:dictionary];
            NSString *firstTableID = [NSString stringWithFormat:@"%d",self.alreadyFirstTableID];
            [[TAJGameEngine sharedGameEngine] getAutoPlayCountWithTableID:firstTableID];
            [self createFirstTableIAmBackView];
            [self.firstTableGameLoad playersAfterReconnectForDiscardCardHistory:dictionary];
            self.firstTableGameLoad.isIamBackButtonPressed = NO;
            
            
        }
        else
        {
            [self removeThisPlayerDiscardCardWhenScheduleGame:[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE]];
            [[TAJGameEngine sharedGameEngine] quitTableWithTableID:[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE] ];
            [self informGameEngineForGameEnded:[[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE] integerValue] ];
            
            if (!self.secondTableGameLoad && !self.firstTableGameLoad )
            {
                [self loadTableViewAction];
            }
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            [engine iAmBackWithUserID:[[engine usermodel] userId]];
        }
    }
    else if(!self.secondTableGameLoad)
    {
        // this is for player already in view / play when app force quit
        BOOL isThisPlayerExist = NO;
        int loggedInUserID = [[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue];
        
        if (dictionary &&
            [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
        {
            // Only 1 player available
            NSMutableDictionary* onePlayerDict = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
            
            if (onePlayerDict[kTaj_userid_Key] &&
                ([onePlayerDict[kTaj_userid_Key] integerValue] == loggedInUserID))
            {
                isThisPlayerExist = YES;
            }
            
        }
        else if (dictionary &&
                 [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
        {
            // Get the players array
            NSArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
            
            // More than 1 player data available
            for (int i = 0; i < playersArray.count; i++)
            {
                NSMutableDictionary *playerDictionary = [playersArray objectAtIndex:i];
                
                if (playerDictionary[kTaj_userid_Key] &&
                    ([playerDictionary[kTaj_userid_Key] integerValue] == loggedInUserID))
                {
                    isThisPlayerExist = YES;
                    break;
                }
            }
        }
        
        if (isThisPlayerExist)
        {
            [self addSecondGameTable:dictionary];
            [self.secondTableGameLoad refreshControllerInGetTableDeatilsEvent:dictionary];
            [self createSecondTableIAmBackView];
            NSString *secondTableID = [NSString stringWithFormat:@"%d",self.alreadySecondTableID];
            [[TAJGameEngine sharedGameEngine] getAutoPlayCountWithTableID:secondTableID];
            [self.secondTableGameLoad playersAfterReconnectForDiscardCardHistory:dictionary];
            self.secondTableGameLoad.isIamBackButtonPressed = NO;
        }
        else
        {
            [self removeThisPlayerDiscardCardWhenScheduleGame:[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE]];
            [[TAJGameEngine sharedGameEngine] quitTableWithTableID:[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE] ];
            [self informGameEngineForGameEnded:[[dictionary valueForKeyPath:GAME_TABLE_ID_VALUE] integerValue] ];
            if (!self.secondTableGameLoad && !self.firstTableGameLoad )
            {
                [self loadTableViewAction];
            }
            TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
            [engine iAmBackWithUserID:[[engine usermodel] userId]];
            
            
        }
    }
    if (self.firstTableGameLoad || self.secondTableGameLoad)
    {
#if NEW_DESIGN_IMPLEMENTATION
        if ([self.switchDelegate respondsToSelector:@selector(isSwitchLobbyShowing)])
        {
            [self.switchDelegate isSwitchLobbyShowing];
        }
#endif
    }
    if (self.firstTableGameLoad || self.secondTableGameLoad)
    {
        if(self.firstTableGameLoad && self.secondTableGameLoad)
        {
            NSString *secondTabelID = [NSString stringWithFormat:@"%d",[self.secondTableGameLoad gameTableCurrentID]];
            [[TAJGameEngine sharedGameEngine] getTableExtraForTableID:secondTabelID];
            NSString *firstTabelID = [NSString stringWithFormat:@"%d",[self.firstTableGameLoad gameTableCurrentID]];
            [[TAJGameEngine sharedGameEngine] getTableExtraForTableID:firstTabelID];
        }
        else if (self.firstTableGameLoad)
        {
            NSString *tabelID = [NSString stringWithFormat:@"%d",[self.firstTableGameLoad gameTableCurrentID]];
            [[TAJGameEngine sharedGameEngine] getTableExtraForTableID:tabelID];
        }
        else if (self.secondTableGameLoad)
        {
            NSString *tabelID = [NSString stringWithFormat:@"%d",[self.secondTableGameLoad gameTableCurrentID]];
            [[TAJGameEngine sharedGameEngine] getTableExtraForTableID:tabelID];
        }
    }
    
}

#pragma mark - Game Rules Web View -

- (void) loadGameRulesWebPage
{
    if (!self.webPageViewController)
    {
        if([TAJUtilities isIPhone])
        {
            self.webPageViewController = [[TAJWebPageViewController alloc]  initWithNibName:@"TAJWebPageViewController_landscape" bundle:nil webPageOption:eGameRulesOption];
        }
        else
        {
            self.webPageViewController = [[TAJWebPageViewController alloc]  initWithNibName:@"TAJWebPageViewController" bundle:nil webPageOption:eGameRulesOption];
        }
        self.webPageViewController.webPageDelegate = self;
        [self.view addSubview:self.webPageViewController.view];
    }
    else if (self.webPageViewController)
    {
        self.webPageViewController.view.frame = self.view.bounds;
        [self.view addSubview:self.webPageViewController.view];
        [self.webPageViewController reloadWebPage];
    }
}

// web page delegate method
- (void)removeWebPageViewController
{
    if (self.webPageViewController)
    {
        [self.webPageViewController.view removeFromSuperview];
    }
}

#pragma mark - Other operations -

-(void) reloadGameList
{
    [self.tableViewLoad alreadyFilter:self.isFilteredData
                         withGameType:_gameTypeSearch
                       withPlayerType:_playerTypeSearch
                          withBetType:_betTypeSearch
                        withTableCost:_tableCostSearch
                          withViewTye:_viewTypeSearch];
    [self.tableViewLoad reloadTableView];
}

-(void)changeAlphaValueOfLobbyAndTableButtons
{
    self.viewComponents.hidden = YES;
    self.viewTopComponents.hidden = YES;
    //self.lobbyButton.alpha = 0.35;
    //self.firstTableButton.alpha = 0.35;
    //self.secondTableButton.alpha = 0.35;
}

-(void)resetAlphaValueOfLobbyAndTableButtons
{
    self.viewComponents.hidden = NO;
    self.viewTopComponents.hidden = NO;
    //self.lobbyButton.alpha = 1.0;
    //self.firstTableButton.alpha = 1.0;
    //self.secondTableButton.alpha = 1.0;
    //NSLog(@"UN-HIDE BUTTONS OTHER");
}

#pragma Info_PopUp

- (void)okButtonPressedForInfoPopup:(UIButton *)sender
{
    [self.errorPopUpController hide];
    //self.errorPopUpController = nil;
    
    //[self.engineMaintenanceAlertView hide];
    //self.engineMaintenanceAlertView = nil;
}

- (void)closeButtonPressedForInfoPopup:(UIButton *)sender
{
    [self.errorPopUpController hide];
    self.errorPopUpController = nil;
}

-(void)showAlertIndicatorWhenMyTurn
{
    [self.switchDelegate showAlertIndicatorForTableWhenItisMyTurn];
}

- (void)hideTableAlertImageViewWhenTurnTimeOut
{
    if(self.firstTableGameLoad)
    {
        [self stopFirstTableBlinkingAnimation];
    }
    if(self.secondTableGameLoad)
    {
        [self stopSecondTableBlinkingAnimation];
    }
    
}

- (void)hideAlertImageWhenOtherTurn
{
    if(!self.firstTableGameLoad.isAlertTobeShown && !self.secondTableGameLoad.isAlertTobeShown)
    {
        [self.switchDelegate hideTableAlertImageView];
    }
    
}
-(void)iamBackNetworkCardArrayCount:(NSInteger)arrayCount
{
    DLog(@"SwitchLobby arrayCount=%d",arrayCount);
    NSDictionary *aDictionary = [[NSDictionary alloc] initWithObjectsAndKeys:
                                 [NSNumber numberWithInteger:arrayCount], @"arrayCount",
                                 nil];
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_IAMBACKARRAY_ALERT object:nil userInfo:aDictionary];
}

-(NSString *)getJokerCardNumberForIamBack
{
    if (self.firstTableIAmBackController && self.secondTableIAmBackController)
    {
        NSString *jokerNumber = [self.secondTableGameLoad.playTable getJokerCardNumberForIamBackCell];
        return jokerNumber;
    }
    else if(self.firstTableIAmBackController)
    {
        NSString *jokerNumber = [self.firstTableGameLoad.playTable getJokerCardNumberForIamBackCell];
        return jokerNumber;
    }
    else if(self.secondTableIAmBackController)
    {
        NSString *jokerNumber = [self.firstTableGameLoad.playTable getJokerCardNumberForIamBackCell];
        return jokerNumber;
    }
    else
        return  nil;
}

#pragma mark ChatExtentView

-(void)createChatExtentView:(NSMutableArray *)messageList andPlayerModel:(TAJPlayerModel *)playerModel
{
    if(self.firstTableGameLoad)
    {
        if(self.firstTableChatMessage.length > 0)
        {
            [self createChatExtentViewForFirstTable:messageList andPlayerModel:playerModel];
            self.firstTableChatExtentView.chatExtentTextField.text = self.firstTableChatMessage;
        }
        else
        {
            [self createChatExtentViewForFirstTable:messageList andPlayerModel:playerModel];
            self.firstTableChatExtentView.chatExtentTextField.text = @"";
        }
    }
    if (self.secondTableGameLoad)
    {
        if(self.secondTableChatMessage.length > 0)
        {
            [self createChatExtentViewForSecondTable:messageList andPlayerModel:playerModel];
            self.secondTableChatExtentView.chatExtentTextField.text = self.secondTableChatMessage;
        }
        else
        {
            [self createChatExtentViewForSecondTable:messageList andPlayerModel:playerModel];
            self.secondTableChatExtentView.chatExtentTextField.text = @"";
        }
    }
}

- (void)createChatExtentViewForFirstTable:(NSMutableArray *)messageList andPlayerModel:(TAJPlayerModel *)playerModel
{
    if (!self.firstTableChatExtentView)
    {
        if ([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                self.firstTableChatExtentView = [[TAJChatExtentViewController alloc]initWithNibName:@"TAJChatExtentViewController~iphone5" bundle:[NSBundle mainBundle]];
            }
            else
            {
                self.firstTableChatExtentView = [[TAJChatExtentViewController alloc]initWithNibName:@"TAJChatExtentViewController~iphone" bundle:nil];
            }
        }
        else
        {
            self.firstTableChatExtentView = [[TAJChatExtentViewController alloc]initWithNibName:@"TAJChatExtentViewController~ipad" bundle:nil];
        }
        self.firstTableChatExtentView.chatExtentDelegate = self;
        [self.firstTableChatExtentView currentChatTableID:self.firstTableGameLoad.currentTableId];
        if(!self.secondTableGameLoad )
        {
            CGRect frame = self.firstTableChatExtentView.view.frame;
            frame.origin.x =self.view.frame.origin.x;
            frame.origin.y =self.view.frame.origin.y;
            self.firstTableChatExtentView.view.frame = frame;
            [self.view addSubview:self.firstTableChatExtentView.view];
            
        }
        
    }
    else
    {
        if(!self.secondTableGameLoad )
        {
            CGRect frame = self.firstTableChatExtentView.view.frame;
            frame.origin.x =self.view.frame.origin.x;
            frame.origin.y =self.view.frame.origin.y;
            self.firstTableChatExtentView.view.frame = frame;
            [self.view addSubview:self.firstTableChatExtentView.view];
        }
    }
    if(playerModel)
    {
        [self.firstTableChatExtentView updateChatPlayerModelOfChatExtentView:playerModel];
        [self.firstTableChatExtentView reloadChatHistoryTable:messageList];
    }
    
}

- (void)createChatExtentViewForSecondTable:(NSMutableArray *)messageList andPlayerModel:(TAJPlayerModel *)playerModel
{
    if (!self.secondTableChatExtentView)
    {
        if ([TAJUtilities isIPhone])
        {
            if ([TAJUtilities isItIPhone5])
            {
                self.secondTableChatExtentView =  [[TAJChatExtentViewController alloc]initWithNibName:@"TAJChatExtentViewController~iphone5" bundle:nil];
            }
            else
            {
                self.secondTableChatExtentView = [[TAJChatExtentViewController alloc]initWithNibName:@"TAJChatExtentViewController~iphone" bundle:nil];
            }
        }
        else
        {
            self.secondTableChatExtentView =  [[TAJChatExtentViewController alloc]initWithNibName:@"TAJChatExtentViewController~ipad" bundle:nil];
        }
        
        
        self.secondTableChatExtentView.chatExtentDelegate = self;
        [self.secondTableChatExtentView currentChatTableID:self.secondTableGameLoad.currentTableId];
        CGRect frame = self.secondTableChatExtentView.view.frame;
        frame.origin.x =self.view.frame.origin.x;
        frame.origin.y =self.view.frame.origin.y;
        self.secondTableChatExtentView.view.frame = frame;
        [self.view addSubview:self.secondTableChatExtentView.view];
        [self.firstTableChatExtentView.view removeFromSuperview];
        
    }
    else
    {
        CGRect frame = self.secondTableChatExtentView.view.frame;
        frame.origin.x =self.view.frame.origin.x;
        frame.origin.y =self.view.frame.origin.y;
        self.secondTableChatExtentView.view.frame = frame;
        
        [self.view addSubview:self.secondTableChatExtentView.view];
        [self.firstTableChatExtentView.view removeFromSuperview];
        
    }
    if(playerModel)
    {
        [self.secondTableChatExtentView updateChatPlayerModelOfChatExtentView:playerModel];
        [self.secondTableChatExtentView reloadChatHistoryTable:messageList];
    }
    
}

- (void)removeChatExtentViewController
{
    if(self.firstTableChatExtentView)
    {
        [self removeChatExtentViewFromGamePlay:self.firstTableChatExtentView.chatExtentHistoryDataSource];
    }
    if(self.secondTableChatExtentView)
    {
        [self removeChatExtentViewFromGamePlay:self.secondTableChatExtentView.chatExtentHistoryDataSource];
    }
    
}

- (void)clearTheMessageDataSourceWhenReconnectedBack
{
    if (self.firstTableChatExtentView)
    {
        if(self.firstTableChatExtentView.chatExtentHistoryDataSource.count > 0)
        {
            [self.firstTableChatExtentView clearChatHistory];
        }
    }
    if(self.secondTableChatExtentView)
    {
        if(self.secondTableChatExtentView.chatExtentHistoryDataSource.count > 0)
        {
            [self.secondTableChatExtentView clearChatHistory];
        }
    }
}

- (void)removeChatExtentView:(NSMutableArray *)messageArray
{
    [self removeChatExtentViewFromGamePlay:messageArray];
}

- (void)removeChatExtentViewFromGamePlay:(NSMutableArray *)messageArray
{
    if (self.firstTableChatExtentView)
    {
        [self.firstTableChatExtentView.view removeFromSuperview];
        self.firstTableChatExtentView = nil;
        [self.firstTableChatExtentView.chatExtentTextField resignFirstResponder];
        if(self.firstTableGameLoad)
        {
            if(messageArray.count > 0)
            {
                [self.firstTableGameLoad.chatViewController reloadChatHistoryTableFromChatExtent:messageArray];
            }
            [self.firstTableGameLoad.chatViewController.chatField resignFirstResponder];
        }
    }
    if(self.secondTableChatExtentView)
    {
        [self.secondTableChatExtentView.view removeFromSuperview];
        self.secondTableChatExtentView = nil;
        [self.secondTableChatExtentView.chatExtentTextField resignFirstResponder];
        if(self.secondTableGameLoad)
        {
            if(messageArray.count > 0)
            {
                [self.secondTableGameLoad.chatViewController  reloadChatHistoryTableFromChatExtent:messageArray];
            }
            [self.secondTableGameLoad.chatViewController.chatField resignFirstResponder];
        }
    }
}

#pragma mark scheduler

-(void) updateTimerLabel
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updateTimerLabel for connected time");
#endif
    
    NSInteger sec = 0;
    NSInteger min = 0;
    NSInteger hrs = 0;
    NSInteger totalSecs = [[TAJGameEngine sharedGameEngine] connectedTime];
    sec = totalSecs % 60;
    min = (totalSecs/60) % 60;
    hrs = (totalSecs/3600)%24;
    [self.connectedTimeLabel setText:[NSString stringWithFormat:@"%.2d:%.2d:%.2d",hrs,min,sec]];
}

- (void)stopConnectedTimer
{
    DLog(@"stop connected timer in switch");
    if(self.connectedTimer)
    {
        [self.connectedTimer invalidate];
        self.connectedTimer = nil;
    }
}

- (void)informControllerGameEnded:(NSString *)tableID
{
    NSLog(@"GAME ENDED");
    int tableId = [tableID intValue];
    if(tableId == self.alreadyFirstTableID)
    {
        if(self.isSecondButtonBlinking)
        {
            self.isSecondButtonBlinking = NO;
            [UIView animateWithDuration:0.12
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut |
             UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 self.firstTableBlinkView.alpha = 1.0f;
                             }
                             completion:^(BOOL finished){
                                 self.firstTableBlinkView.hidden = YES;
                             }];
        }
    }
    if(tableId == self.alreadySecondTableID)
    {
        if(self.isSecondButtonBlinking)
        {
            self.isSecondButtonBlinking = NO;
            [UIView animateWithDuration:0.12
                                  delay:0.0
                                options:UIViewAnimationOptionCurveEaseInOut |
             UIViewAnimationOptionBeginFromCurrentState
                             animations:^{
                                 self.secondTableBlinkView.alpha = 1.0f;
                             }
                             completion:^(BOOL finished){
                                 self.secondTableBlinkView.hidden = YES;
                                 // Do nothing
                             }];
        }
    }
}

- (void)destroyAllViewsOnLogout
{
    [self stopConnectedTimer];
    
    [self.firstTableGameLoad stopProgressTimer];
    [self.secondTableGameLoad stopProgressTimer];
    
    [self.firstTableGameLoad stopTimer];
    [self.secondTableGameLoad stopTimer];
    
    [self.tableViewLoad removeAllObserver];
    [self.firstTableGameLoad removeAllNotifications];
    [self.secondTableGameLoad removeAllNotifications];
    self.firstTableGameLoad = nil;
    self.secondTableGameLoad = nil;
    
    [[[TAJGameEngine sharedGameEngine] lobby] destroyFirstTable];
    [[[TAJGameEngine sharedGameEngine] lobby] destroySecondTable];
    
    [[TAJGameEngine sharedGameEngine] lobby].isFirstTableCreated = NO;
    [[TAJGameEngine sharedGameEngine] lobby].isSecondTableCreated = NO;
    
    [[TAJGameEngine sharedGameEngine] destroyLobby];
    
}

- (void)loadCurrentTurnTimeEnableTable
{
    if (self.firstTableGameLoad.isMyTurn && self.secondTableGameLoad.isMyTurn )
    {
        if(self.firstTableGameLoad.playerTimeoutTime < self.secondTableGameLoad.playerTimeoutTime)
        {
            [self loadFirstTable];
        }
        else
        {
            [self loadSecondTable];
        }
    }
    else if (self.firstTableGameLoad.isMyTurn && self.secondTableGameLoad )
    {
        [self loadFirstTable];
    }
    
}

@end
