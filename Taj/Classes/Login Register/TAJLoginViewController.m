/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLoginViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/01/14.
 **/

#import "TAJLoginViewController.h"
#import "TAJProgressAlertView.h"
#import "TAJAppDelegate.h"
#import "TAJGameEngine.h"
#import "TAJClientServerCommunicator.h"
#import "TAJTableViewController.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJLobby.h"
#import "TAJForgotPasswordViewController.h"


@interface TAJLoginViewController ()<UITextFieldDelegate,LocationManagerDelegate,BlockedPopupViewControllerDelegate>

//activity indicator
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *activityIndicator;
@property (nonatomic)   BOOL        isLoginActivityIndicatorStarts;

//login button
@property (weak, nonatomic) IBOutlet UIButton *loginButton;

@property (strong, nonatomic) TAJHomePageViewController *homeViewController;
@property (strong, nonatomic) TAJAppDelegate *appDelegate;
@property (strong, nonatomic) TAJForgotPasswordViewController *forgotPasswordController;
@property (weak, nonatomic) IBOutlet UIButton *createAccount;
@property (weak, nonatomic) IBOutlet UIImageView *errorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *userNameErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorImageView;
@property (weak, nonatomic) IBOutlet UILabel *errorTextLabel;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@property (weak, nonatomic) IBOutlet UILabel *userName;
@property (weak, nonatomic) IBOutlet UILabel *password;
- (IBAction)forgotPassword:(id)sender;
- (IBAction)backButtonPressed:(id)sender;
//action for login button
- (IBAction)loginButtonClick:(UIButton *)sender;
- (IBAction)createAccount:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;

@property (nonatomic, strong) BlockedPopupViewController  *blockedPopup;


@property (strong, nonatomic) NSMutableArray * statesList;
@property (strong, nonatomic) NSString * strState;

typedef void(^addressCompletion)(NSString *);

@property (strong, nonatomic) NSString * strEmail;
@property (strong, nonatomic) NSString * strID;
@property (strong, nonatomic) NSString * strName;
@property (strong, nonatomic) NSString * strType;

@property (nonatomic, assign) BOOL show_lobby;

@property BOOL isPasswordToggle;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPasswordHide;

@property (strong, nonatomic) NSString * registerationDate;

@end

@implementation TAJLoginViewController
@synthesize usernameField = _userNameField;
@synthesize passwordField = _passwordField;
@synthesize activityIndicator = _activityIndicator;
@synthesize loginButton = _loginButton;

#pragma mark - UIViewController life cycle -

- (id)init
{
    self = [super init];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Login Screen"];
    
    self.isLoginActivityIndicatorStarts = NO;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        // if iOS 7
        self.edgesForExtendedLayout = UIRectEdgeNone; //layout adjustements
    }
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.versionLabel.text = VERSION_NUMBER;
    if ([[UIApplication sharedApplication] isIgnoringInteractionEvents])
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    
    
}
//- (void)settingFrameForErrorImageViewAndErrorLabel
//{
//    CGRect iFrame = self.errorImageView.frame;
//    CGRect lFrame = self.errorTextLabel.frame;
//    if(self.errorTextLabel.text.length >= 35)
//    {
//        if([TAJUtilities isIPhone])
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 2) - 20;
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//        else
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 3) - 10;
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//
//    }
//    else if(self.errorTextLabel.text.length <= 35 && self.errorTextLabel.text.length >= 30)
//    {
//        if([TAJUtilities isIPhone])
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 2);
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//        else
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 3);
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//
//    }
//    else if(self.errorTextLabel.text.length <= 30 && self.errorTextLabel.text.length >= 25)
//    {
//        if([TAJUtilities isIPhone])
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 2);
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//        else
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 3.5);
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//
//
//    }
//    else if(self.errorTextLabel.text.length < 25 && self.errorTextLabel.text.length >= 15)
//    {
//        if([TAJUtilities isIPhone])
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 2.5);
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//        else
//        {
//            iFrame.origin.x = self.usernameField.frame.origin.x + (iFrame.size.width * 4);
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//
//
//    }
//    else if(self.errorTextLabel.text.length < 15)
//    {
//        if([TAJUtilities isIPhone])
//        {
//            iFrame.origin.x = self.usernameField.center.x - (iFrame.size.width * 2) ;
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//        else
//        {
//            iFrame.origin.x = self.usernameField.center.x - (iFrame.size.width * 2.2) ;
//            lFrame.origin.x = iFrame.origin.x + iFrame.size.width + 3;
//        }
//
//    }
//
//    self.errorImageView.frame = iFrame;
//    self.errorTextLabel.frame = lFrame;
//}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self sampleGet];
    
    LocationHandler.SharedLocationHandler.delegate = self;
    if ([[LocationHandler SharedLocationHandler] shouldFetchUserLocationInViewController:self] == YES) {
#if DEBUG
        NSLog(@"YES WE CAN GET LOCATION");
#endif
    }
    
    self.loginButton.exclusiveTouch = YES;
    self.createAccount.exclusiveTouch = YES;
    self.errorImageView.hidden = YES;
    self.errorTextLabel.hidden = YES;
    self.userNameErrorImageView.hidden = YES;
    self.passwordErrorImageView.hidden = YES;
    if ([[UIApplication sharedApplication] isIgnoringInteractionEvents])
    {
        [[UIApplication sharedApplication] endIgnoringInteractionEvents];
    }
    self.activityIndicator.hidden = YES;
    //Add Notification
    [self addNotificationListener];
    
#if USERNAME_PASSWORD_STORE_CHECK
    self.usernameField.text = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
    self.passwordField.text = [[NSUserDefaults standardUserDefaults] objectForKey:PASSWORD_KEY];
    
#endif
    
    //[self getBlockedList];
    
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    //Remove notification
    [self removeNotificationListener];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
    
}

- (BOOL) shouldAutorotate
{
    return YES;
}


-(NSUInteger)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}

- (BOOL)validateInput
{
    if ([self.usernameField.text length] == 0) {
        [self.view makeToast:@"Please enter Username"];
        return YES;
    }
    else if ([self.passwordField.text length] == 0) {
        [self.view makeToast:@"Please enter password"];
        return YES;
    }
    else if ([self.passwordField.text length] < PasswordMinLength) {
        [self.view makeToast:@"Password should be minimum 5 characters"];
        return YES;
    }
    return NO;
}

- (void)sampleGet {
    //@"https://www.tajrummy.com/api/v1/get-blocked-states/"
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,BLOCKED_STATES_LIST];
    NSLog(@"STATES LIST : %@",URL);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
                                                    if (error) {
                                                        NSLog(@"%@", error);
                                                    } else {
                                                        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
                                                        //NSLog(@"%@", httpResponse);
                                                        id jsonResponseString = [[NSString alloc] initWithData:data
                                                                                                      encoding:NSASCIIStringEncoding];
                                                        NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
                                                        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
                                                        
                                                        dispatch_async(dispatch_get_main_queue(), ^{
                                                            [self.view hideToastActivity];
                                                            NSLog(@"Sample BlockedList Response :%@",jsonResponse);
                                                            if ([[jsonResponse valueForKey:@"status"] isEqualToString:@"Success"]) {
                                                                
                                                                self.statesList = [[NSMutableArray alloc] init];
                                                                self.statesList = [jsonResponse valueForKey:@"data"][@"blocked_states_list"];
                                                                self.show_lobby = [[jsonResponse valueForKey:@"data"][@"show_lobby"] boolValue];
                                                                NSLog(@"BLOCKED STATES  : %lu",(unsigned long)self.statesList.count);
                                                            }
                                                        });
                                                        
                                                    }
                                                }];
    [dataTask resume];
}

-(void)getBlockedList {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,BLOCKED_STATES_LIST];
    NSLog(@"BlockedList URL :%@",URL);
    [[Service sharedInstance] getRequestWithURL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            NSLog(@"BlockedList Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                
                NSDictionary * dataDic = [responseData valueForKey:@"data"];
                self.statesList = [[NSMutableArray alloc] init];
                self.statesList = [responseData valueForKey:@"data"][@"blocked_states_list"];
                self.show_lobby = [[responseData valueForKey:@"show_lobby"] boolValue];
                // if show_lobby - true direct login
                // if show_lobby - false need to check blockedList and then login
                NSLog(@"BLOCKED STATES  : %lu",(unsigned long)self.statesList.count);
            }
        });
        
        
    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        NSLog(@"STATES ERROR : %@",errorString);
        [self.view makeToast:errorString];
        
    }];
    
    
}

-(void)login
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * deviceType;
    
    if ([TAJUtilities isIPhone])
    {
        deviceType = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceType = @"Tablet";//@"IPAD";
    }
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSDictionary *params = @{@"username": self.usernameField.text,
                             @"password": self.passwordField.text,
                             //@"device_id": @"1",
                             @"device_type" : deviceType,
                             @"client_type" : @"iOS",
                             //@"version" : buildNumber,
                             @"device_id": [UIDevice currentDevice].identifierForVendor,
                             @"device_brand": [TAJUtilities checkIfStringIsEmpty:APP_DELEGATE.brandName]
                             };
#if DEBUG
    NSLog(@"LOGIN PARAMS: %@", params);
#endif
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,LOGIN];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:NO headerValue:nil withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
                    [self.view hideToastActivity];
                    [self.errorTextLabel setText:@"server response"];
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        NSDictionary * jsonDictionay = jsondata;
                        NSDictionary *tempDict = [jsonDictionay dictionaryRemovingNSNullValues];
                        //NSLog(@"%@", tempDict);
#if DEBUG
                        NSLog(@"LOGIN RESPONSE: %@", tempDict);
                        NSLog(@"unique_id : %@",[tempDict valueForKey:@"unique_id"]);
                        NSLog(@"playerid : %@",[tempDict valueForKey:@"playerid"]);
                        NSLog(@"token : %@",[tempDict valueForKey:@"token"]);
#endif
                        NSString *playerId = [tempDict valueForKey:@"playerid"];
                        NSString * email = [tempDict valueForKey:@"email"];
                        NSString * gender = [tempDict valueForKey:@"gender"];
                        NSString * mobile = [tempDict valueForKey:@"mobile"];
                        NSString * dob = [tempDict valueForKey:@"dob"];
                        NSString * firstName = [tempDict valueForKey:@"firstname"];
                        NSString * lastName = [tempDict valueForKey:@"lastname"];
                        NSString * city = [tempDict valueForKey:@"city"];
                        NSString * state = [tempDict valueForKey:@"state"];
                        NSInteger pincode = [[tempDict valueForKey:@"zipcode"] integerValue];
                        NSString * registrationIP = [tempDict valueForKey:@"ip"];
                        NSString * firstDepositDate = [tempDict valueForKey:@"first_deposit_date"];
                        BOOL emailVerificationStatus = [[tempDict valueForKey:@"verified"] boolValue];
                        BOOL manual_mobile_verified = [[tempDict valueForKey:@"manual_mobile_verified"] boolValue];
                        BOOL  mobileverified = [[tempDict valueForKey:@"mobileverified"] boolValue];
                        NSString * KYC_STATUS = [tempDict valueForKey:@"kycverified"];
                        NSString * lastLoginDate = [tempDict valueForKey:@"last_login"];
                        self.registerationDate = [tempDict valueForKey:@"registeredon"];
                        
                        NSString * registrationDeviceType = [tempDict valueForKey:@"register_device_type"];
                        NSString * registrationClientType = [tempDict valueForKey:@"register_client_type"];
                        NSString * lastDepositDate = [tempDict valueForKey:@"last_deposit_at"];
                        NSString * firstWithdrawalDate = [tempDict valueForKey:@"first_withdrawal_at"];
                        NSString * lastWithdrawalDate = [tempDict valueForKey:@"last_withdrawal_at"];
                        NSString * clientType = [tempDict valueForKey:@"client_type"];
                        NSNumber * totalDeposits = [tempDict valueForKey:@"sumdeposit"];
                        // NSString * totalDeposits = [tempDict valueForKey:@"sumdeposit"];
                        NSNumber * totalWithdrawals = [tempDict valueForKey:@"sumwithdrawl"];
                        NSNumber * totalNumberOfWithdrawals = [tempDict valueForKey:@"withdrawls"];
                        NSNumber *deposits = [tempDict valueForKey:@"deposits"];
                        NSNumber *lastLoginDays = [tempDict valueForKey:@"last_login_days"];
                        
                        APP_DELEGATE.dynamicIP = [tempDict valueForKey:@"ip"];
                        
                        NSString * lastLoginDat = [lastLoginDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                        NSLog(@"DATE : %@",lastLoginDat);
                        
                        //NSString * loginDate = [self geTimeFromString:date];
                        NSString * regDate = [_registerationDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                        NSString *firstDeposDate =[firstDepositDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                        NSString * LastDeposDate = [lastDepositDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                        NSString * FirstWithdrawDate = [firstWithdrawalDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                        NSString * LastWithdrawDate = [lastWithdrawalDate stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                  
                        #if DEBUG
                        NSLog(@"DYNAMIC IP : %@",APP_DELEGATE.dynamicIP);
                        NSLog(@"CONVERTED DATE : %@",[self getNSDateFromStringDate:lastLoginDat]);
                        NSLog(@"CONVERTED reg DATE : %@",[self getNSDateFromStringDate:regDate]);
                        NSLog(@"firstDeposDate : %@",[self getNSDateFromStringDate:firstDeposDate]);
                        NSLog(@"LastDeposDate : %@",[self getNSDateFromStringDate:LastDeposDate]);
                        NSLog(@"FirstWithdrawDate : %@",[self getNSDateFromStringDate:FirstWithdrawDate]);
                        NSLog(@"LastWithdrawDate : %@",[self getNSDateFromStringDate:LastWithdrawDate]);
                        #endif

                        WEGUser* weUser = [WebEngage sharedInstance].user;
                        [weUser login:[NSString stringWithFormat:@"%@",playerId]];
                        [weUser setEmail:email];
                        [weUser setGender:[gender lowercaseString]];
                        if ([mobile isEqualToString:@""]) {
                            //
                        }
                        else {
                            [weUser setPhone:[NSString stringWithFormat:@"%@",mobile]];
                            [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"mobile"] forKey:USERMOBILE_KEY];
                        }
                        
                        [weUser setBirthDateString:dob];
                        [weUser setFirstName:firstName];
                        [weUser setLastName:lastName];
                        [weUser setAttribute:@"city" withStringValue:city];
                        [weUser setAttribute:@"State" withStringValue:state];
                        [weUser setAttribute:@"pincode" withValue:[NSNumber numberWithInteger:pincode]];
                        [weUser setAttribute:@"RegisterationIP" withStringValue:registrationIP];
                        //[weUser setAttribute:@"firstDepositDate" withStringValue:firstDeposDate];
                        [weUser setAttribute:@"EmailVerificationStatus" withValue:[NSNumber numberWithBool:emailVerificationStatus]];
                        [weUser setAttribute:@"MobileVerificationStatus" withValue:[NSNumber numberWithBool:mobileverified]];
                        [weUser setAttribute:@"KYC_STATUS" withStringValue:KYC_STATUS];
                        [weUser setAttribute:@"LastLoginDate" withDateValue:[self getNSDateFromStringDate:lastLoginDat]];
                        [weUser setAttribute:@"RegisterationDate" withDateValue:[self getNSDateFromStringDate:regDate]];
                        [weUser setAttribute:@"RegistrationDeviceType" withStringValue:registrationDeviceType];
                        [weUser setAttribute:@"RegistrationClientType" withStringValue:registrationClientType];
                        [weUser setAttribute:@"LastDepositDate" withDateValue:[self getNSDateFromStringDate:LastDeposDate]];
                        [weUser setAttribute:@"FirstWithdrawalDate" withDateValue:[self getNSDateFromStringDate:FirstWithdrawDate]];
                        [weUser setAttribute:@"LastWithdrawalDate" withDateValue:[self getNSDateFromStringDate:LastWithdrawDate]];
                        [weUser setAttribute:@"totalDeposits" withValue:totalDeposits];
                        [weUser setAttribute:@"totalWithdrawals" withValue:totalWithdrawals];
                        [weUser setAttribute:@"totalNumberOfWithdrawals" withValue:totalNumberOfWithdrawals];
                        
                        [weUser setAttribute:@"deposits" withValue:deposits];
                        [weUser setAttribute:@"InactiveDays" withValue:lastLoginDays];
                        
                        id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                        
                        NSDictionary *userData=@{
                                                 @"userId":playerId,
                                                 @"device_type":deviceType,
                                                 @"client_type":clientType
                                                 };
                        [weAnalytics trackEventWithName:@"User Login" andValue:userData];
                        
                        //need to save for future reference - token
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"unique_id"] forKey:UNIQUE_SESSION_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"email"] forKey:USEREMAIL_KEY];
                        
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"username"] forKey:USENAME_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"token"] forKey:TOKEN_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:playerId forKey:USERID];
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        NSString * uniqueID = [jsondata valueForKey:@"unique_id"];
                        
                        [[TAJGameEngine sharedGameEngine] createUserModelWithUserName:self.usernameField.text
                                                                             password:self.passwordField.text sessionId:uniqueID];
                        
                        TAJGameEngine *engine= [TAJGameEngine sharedGameEngine];
                        engine.isOtherLoggedIn =FALSE;
                        [[TAJClientServerCommunicator sharedClientServerCommunicator] createSocketAndConnect];
                        [[TAJGameEngine sharedGameEngine] setCurrentController:self];
                        
                    }
                    else {
                        NSLog(@"REST Error : %@",jsondata);
                        self.activityIndicator.hidden = YES;
                        self.isLoginActivityIndicatorStarts = NO;
                        [self wantToEnableUserInteration:YES];
                        [self.view makeToast:[jsondata valueForKey:@"error"]];
                        
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                //self.activityIndicator.hidden = YES;
                self.isLoginActivityIndicatorStarts = NO;
                [self wantToEnableUserInteration:YES];
                self.errorImageView.hidden = NO;
                self.errorTextLabel.hidden = NO;
                [self.view makeToast:@"Error while fetching data from server"];
            }
        });
    }];
    
}

- (void)socialLoginCheck {
    
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSDictionary *params = @{@"api_email": self.strEmail,
                             @"api_type": self.strType,
                             //@"api_id" : self.strID,
                             //@"api_name" : self.strName
                             };
#if DEBUG
    NSLog(@"LOGIN CHECK PARAMS: %@", params);
#endif
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,SOCIAL_LOGIN_CHECK];
    
    [[Service sharedInstance] getRequestWithHeader:nil URL:URL withParams:params completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            NSLog(@"LOGIN CHECK Response :%@",responseData);             if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSDictionary * dataDic = [responseData valueForKey:@"data"];
                
                if ([[dataDic valueForKey:@"show_form"] isEqualToString:@"register"]) {
                    
                }
                else {
                    
                }
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        [self.view hideToastActivity];
        NSLog(@"LOGIN CHECK ERROR : %@",errorString);
        [self.view makeToast:errorString];
    }];
}

-(NSDate *)getNSDateFromStringDate:(NSString *)dateString
{
    NSString *date = dateString;
    NSDateFormatter *dateFormatterr = [[NSDateFormatter alloc] init];
    [dateFormatterr setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate *dateFromString = [dateFormatterr dateFromString:date];
    //NSLog(@"DOB : %@",dateFromString);
    NSCalendar *calendar =[NSCalendar currentCalendar];
    NSDateComponents *components = [calendar components:(NSDayCalendarUnit | NSMonthCalendarUnit |NSYearCalendarUnit) fromDate:dateFromString];
    int day =[components day]; //day from the date
    int month = [components month];//month from the date
    int year = [components year];// y
    int hour = [components hour];
    int minute = [components minute];
    int seconds = [components second];
    
    NSDateComponents *comps = [[NSDateComponents alloc] init];
    [comps setDay:day];
    [comps setMonth:month];
    [comps setYear:year];
    [comps setHour:hour];
    [comps setMinute:minute];
    [comps setSecond:seconds];
    
    NSDate *date2 = [[NSCalendar currentCalendar] dateFromComponents:comps];
    
    return date2;
}

#pragma mark - Add notification -

- (void)addNotificationListener
{
    //success login
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginUpdate:)
                                                 name:MOVETOHOMEPAGE
                                               object:nil];
    //Wrong user name or password
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(wrongUserEntry:)
                                                 name:ERROR_NOTIFICATION
                                               object:nil];
    
    //Wrong user name or password
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(onErrorSocket:)
                                                 name:ON_SOCKET_ERROR
                                               object:nil];
    
}

#pragma mark - Remove notification -

- (void)removeNotificationListener
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MOVETOHOMEPAGE object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ON_SOCKET_ERROR object:nil];
}

#pragma mark - Login Actions -

- (IBAction)loginButtonClick:(UIButton *)sender
{
    [self onLoginButtonClicked];
}

- (IBAction)createAccount:(id)sender
{
    [(TAJAppDelegate*)TAJ_APP_DELEGATE loadRegistrationScreen];
}

- (void)onLoginButtonClicked
{
    [self hideKeyboard];
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        BOOL error = [self validateInput];
        if (!error) {
            
            self.activityIndicator.hidden = YES;
            self.isLoginActivityIndicatorStarts = YES;
            
            //[self.activityIndicator startAnimating];
            
            [self.usernameField resignFirstResponder];
            [self.passwordField resignFirstResponder];
            
            //start game engine
            if (!self.show_lobby) {
                NSLog(@"LOGIN IF");
                //(!self.show_lobby) - mandatory as per the API
                // no need to restrict
                if ([[LocationHandler SharedLocationHandler] shouldFetchUserLocationInViewController:self] == YES) {
#if DEBUG
                    NSLog(@"YES WE CAN GET LOCATION");
#endif
                    [self findBlickListStates:self.strState];
                }
                else {
                    UIAlertController* alert = [UIAlertController alertControllerWithTitle:@AlertTitle_authorizationLocationDenied
                                                                                   message:@AlertMessage_authorizationLocationDenied
                                                                            preferredStyle:UIAlertControllerStyleAlert];
                    
                    UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:location_cancel style:UIAlertActionStyleDefault
                                                                          handler:^(UIAlertAction * action) {
                                                                              
                                                                              
                                                                          }];
                    
                    UIAlertAction* settingsAction = [UIAlertAction actionWithTitle:location_settings style:UIAlertActionStyleDefault
                                                                           handler:^(UIAlertAction * action) {
                                                                               [self openURLWithString:UIApplicationOpenSettingsURLString];
                                                                               
                                                                           }];
                    [alert addAction:defaultAction];
                    [alert addAction:settingsAction];
                    
                    [self presentViewController:alert animated:YES completion:nil];
                }
                
            }
            else {
                NSLog(@"LOGIN ELSE");
                [self login];
            }
            
        }
    }
    else
    {
        self.errorImageView.hidden = NO;
        self.errorTextLabel.hidden = NO;
        [self.view makeToast:@"Please check your internet connection."];
    }
}

- (void)placeErrorImageAndText:(UILabel *)errorLabel
{
    CGRect newFrame = errorLabel.frame;
    CGRect  xframe = self.usernameField.frame;
    if(errorLabel.text.length>30)
    {
        newFrame.origin.x = xframe.size.width/2 + 48;
        errorLabel.frame = newFrame;
    }
}

#pragma mark - UITextfield Delegates -

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
        [textField resignFirstResponder];
        if (textField == self.passwordField)
        {
            [self onLoginButtonClicked];
        }
        if (textField == self.usernameField)
        {
            [self.passwordField becomeFirstResponder];
        }
        return YES;
}

//- (void)textFieldDidBeginEditing:(UITextField *)textField
//{
//    if (textField == self.usernameField)
//    {
//        self.userName.hidden = YES;
//    }
//    if (textField == self.passwordField)
//    {
//        self.password.hidden = YES;
//    }
//
//    textField.textColor = [UIColor blackColor];
//    self.activityIndicator.hidden = YES;
//    self.isLoginActivityIndicatorStarts = NO;
//    if(textField == self.passwordField)
//    {
//        if(self.usernameField.text.length == 0)
//        {
//            self.userNameErrorImageView.hidden = YES;
//            self.errorImageView.hidden = YES;
//            self.errorTextLabel.hidden = YES;
//            self.userName.hidden = NO;
//        }
//
//        CGRect xframe =  self.usernameField.frame;
//        CGRect yframe =  self.passwordField.frame;
//        CGRect pframe=   self.errorTextLabel.frame;
//        CGRect qframe =  self.errorImageView.frame;
//        CGRect lframe =  self.passwordErrorImageView.frame;
//        CGRect mframe =  self.userNameErrorImageView.frame;
//        CGRect aframe = self.userName.frame;
//        CGRect bframe = self.password.frame;
//        CGRect logoFrame = self.logoImageView.frame;
//        lframe.origin.y -= 20;
//        mframe.origin.y -= 20;
//        xframe.origin.y -= 20;
//        yframe.origin.y -= 20;
//        pframe.origin.y -= 20;
//        qframe.origin.y -= 20;
//        aframe.origin.y -= 20;
//        bframe.origin.y -= 20;
//        logoFrame.origin.y -= 20;
//        [UIView animateWithDuration:0.5f
//                         animations:^{
//                             self.usernameField.frame = xframe;
//                             self.passwordField.frame = yframe;
//                             self.errorImageView.frame = qframe;
//                             self.errorTextLabel.frame = pframe;
//                             self.passwordErrorImageView.frame = lframe;
//                             self.imgViewPasswordHide.frame = lframe;
//                             self.userNameErrorImageView.frame = mframe;
//                             self.userName.frame =aframe;
//                             self.password.frame = bframe;
//                             self.logoImageView.frame = logoFrame;
//                         }
//         ];
//    }
//}

- (BOOL)textFieldShouldBeginEditing:(UITextField *)textField
{
    if (textField == self.usernameField)
    {
        self.userName.hidden = YES;
    }
    if (textField == self.passwordField)
    {
        self.password.hidden = YES;
    }
    return YES;
}

//- (void)textFieldDidEndEditing:(UITextField *)textField
//{
//    self.errorImageView.hidden = YES;
//    self.errorTextLabel.hidden = YES;
//    if([textField isEqual:self.usernameField])
//    {
//        if(self.usernameField.text.length == 0)
//        {
//            self.userNameErrorImageView.hidden = NO;
//            self.errorImageView.hidden = NO;
//            self.errorTextLabel.hidden = NO;
//            self.errorTextLabel.text = @"Please fill out Username field.";
//
//           // [self settingFrameForErrorImageViewAndErrorLabel];
//            self.userName.hidden = NO;
//
//        }
//        else
//        {
//            self.userNameErrorImageView.hidden = YES;
//            self.errorImageView.hidden = YES;
//            self.errorTextLabel.hidden = YES;
//            self.userName.hidden = YES;
//        }
//    }
//    if([textField isEqual:self.passwordField])
//    {
//        CGRect xframe =  self.usernameField.frame;
//        CGRect yframe =  self.passwordField.frame;
//        CGRect pframe=   self.errorTextLabel.frame;
//        CGRect qframe =  self.errorImageView.frame;
//        CGRect lframe =  self.passwordErrorImageView.frame;
//        CGRect mframe =  self.userNameErrorImageView.frame;
//        CGRect aframe = self.userName.frame;
//        CGRect bframe = self.password.frame;
//        CGRect logoFrame = self.logoImageView.frame;
//
//
//        lframe.origin.y +=20;
//        mframe.origin.y +=20;
//        xframe.origin.y +=20;
//        yframe.origin.y +=20;
//        pframe.origin.y +=20;
//        qframe.origin.y +=20;
//        aframe.origin.y += 20;
//        bframe.origin.y += 20;
//        logoFrame.origin.y += 20;
//        [UIView animateWithDuration:0.5f
//                         animations:^{
//                             self.usernameField.frame = xframe;
//                             self.passwordField.frame = yframe;
//                             self.errorImageView.frame = qframe;
//                             self.errorTextLabel.frame = pframe;
//                             self.passwordErrorImageView.frame = lframe;
//                             self.imgViewPasswordHide.frame = lframe;
//                             self.userNameErrorImageView.frame = mframe;
//                             self.userName.frame =aframe;
//                             self.password.frame = bframe;
//                             self.logoImageView.frame = logoFrame;
//
//                         }
//         ];
//
//        if(self.passwordField.text.length == 0)
//        {
//            self.passwordErrorImageView.hidden = NO;
//            self.errorTextLabel.hidden = NO;
//            self.errorImageView.hidden = NO;
//            self.errorTextLabel.text = @"Please fill out Password field.";
//            //[self settingFrameForErrorImageViewAndErrorLabel];
//            self.password.hidden = NO;
//        }
//        else
//        {
//            self.passwordErrorImageView.hidden = YES;
//            self.errorImageView.hidden = YES;
//            self.errorTextLabel.hidden = YES;
//            self.password.hidden = YES;
//            if(self.usernameField.text.length == 0)
//            {
//                self.userNameErrorImageView.hidden = NO;
//                self.errorImageView.hidden = NO;
//                self.errorTextLabel.hidden = NO;
//                self.userName.hidden = YES;
//                self.errorTextLabel.text = @"Please fill out Username field.";
//            }
//
//        }
//
//    }
//
//}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Email
    if (textField == self.passwordField) {
        return !([textField.text length] > PasswordLength && [string length] > range.length);
    }
    
    return YES;
}

#pragma mark - Notification observer -

//Success login
- (void)loginUpdate:(NSNotification *) notification
{
#if USERNAME_PASSWORD_STORE_CHECK
    [[NSUserDefaults standardUserDefaults] setObject:self.usernameField.text forKey:USENAME_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:self.passwordField.text forKey:PASSWORD_KEY];
#endif
    
    if ([[notification name] isEqualToString: MOVETOHOMEPAGE])
    {
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [[NSUserDefaults standardUserDefaults] setObject:engine.lobby.tableDetails forKey: NSUSER_MOVE_DATA];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.appDelegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
        
#if NEW_DESIGN_IMPLEMENTATION
        [self.appDelegate loadPrelobbyScreen];
#else
        [self.appDelegate loadHomePageWindow];
#endif
        
    }
}

//On socket error
- (void)onErrorSocket:(NSNotification *)notification
{
    self.activityIndicator.hidden = YES;
    self.isLoginActivityIndicatorStarts = NO;
    [self wantToEnableUserInteration:YES];
}

//Wrong username and password
- (void)wrongUserEntry:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self handleWrongUserEntryEvent:[dictionary[kTaj_Error_code] integerValue]];
}

//Socket Error
- (void)socketError:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    self.activityIndicator.hidden = YES;
    self.isLoginActivityIndicatorStarts = NO;
    [self wantToEnableUserInteration:YES];
}

//alert popup
- (void) showAlertPopupWithMessage:(NSString *)popupMessage
{
    if (!self.infoPopupViewController)
    {
        self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eOK_Type message:popupMessage];
        [self.infoPopupViewController show];
        
        self.infoPopupViewController.view.center = self.view.center;
    }
}

#pragma mark - ACTIONS

- (IBAction)togglePasswordClicked:(UIButton *)sender {
    
    if(self.isPasswordToggle == true) {
        self.passwordField.secureTextEntry = YES;
        self.imgViewPasswordHide.image = [UIImage imageNamed:@"eye_cross"];
        self.isPasswordToggle = NO;
    } else {
        self.passwordField.secureTextEntry = NO;
        self.imgViewPasswordHide.image = [UIImage imageNamed:@"eye"];
        self.isPasswordToggle = YES;
    }
}
#pragma mark - User Interation -

- (void)wantToEnableUserInteration:(BOOL)boolValue
{
    self.usernameField.userInteractionEnabled = boolValue;
    self.passwordField.userInteractionEnabled = boolValue;
    self.loginButton.userInteractionEnabled = boolValue;
}

#pragma mark - Error Code Process -

- (void)handleWrongUserEntryEvent:(int)errorCode
{
    switch (errorCode)
    {
        case eWRONG_USERNAME:
        {
            //461
            [self.usernameField becomeFirstResponder];
            self.activityIndicator.hidden = YES;
            self.isLoginActivityIndicatorStarts = NO;
            [self wantToEnableUserInteration:YES];
            self.errorImageView.hidden = NO;
            self.errorTextLabel.hidden = NO;
            self.userNameErrorImageView.hidden = NO;
            [self.errorTextLabel setText:@"Wrong Username"];
            //[self settingFrameForErrorImageViewAndErrorLabel];
        }
            break;
            
        case eWRONG_PASSWORD:
        {
            //462
            
            [self.passwordField becomeFirstResponder];
            self.activityIndicator.hidden = YES;
            self.isLoginActivityIndicatorStarts = NO;
            [self wantToEnableUserInteration:YES];
            self.errorImageView.hidden = NO;
            self.errorTextLabel.hidden = NO;
            [self.errorTextLabel setText:@"Wrong Password"];
            self.passwordErrorImageView.hidden = NO;
            //[self settingFrameForErrorImageViewAndErrorLabel];
        }
            break;
            
        default:
            
            self.activityIndicator.hidden = YES;
            self.isLoginActivityIndicatorStarts = NO;
            [self wantToEnableUserInteration:YES];
            self.errorImageView.hidden = NO;
            self.errorTextLabel.hidden = NO;
            [self.errorTextLabel setText:@"Unknown Status"];
            //[self settingFrameForErrorImageViewAndErrorLabel];
            break;
    }
}

- (IBAction)forgotPassword:(id)sender
{
    if (!self.forgotPasswordController)
    {
        self.forgotPasswordController = [[TAJForgotPasswordViewController alloc]init];
    }
    
    if (!self.isLoginActivityIndicatorStarts)
    {
        [self.navigationController pushViewController:self.forgotPasswordController animated:YES];
    }
}

- (IBAction)backButtonPressed:(id)sender
{
    if (!self.isLoginActivityIndicatorStarts)
    {
        [((TAJAppDelegate*)[[UIApplication sharedApplication] delegate]) loadMainWindow];
    }
}

#pragma mark - Info Popup Delegates -

-(void) yesButtonPressedForInfoPopup:(id)object
{
    
}

-(void) noButtonPressedForInfoPopup:(id)object
{
    
}

-(void) okButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

-(void) closeButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

#pragma mark - LOCATION MANAGER DELEGATE

- (void)locationFound:(double)latitiude and:(double)longitude
{
    //NSLog(@"Current - Latitude: %f ,Current - Longitude: %f",latitiude, longitude);
    
    CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:latitiude longitude:longitude];
    
    [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
        if(address) {
            //_address = address;
            //NSLog(@"Address : %@",address);
        }
    }];
    
}

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             self.strState = @"";
             placemark = [placemarks lastObject];
             //             NSLog(@"placemark : %@",placemark);
             //             NSLog(@"subLocality : %@",placemark.subLocality);
             //             NSLog(@"Country : %@",placemark.country);
             //             NSLog(@"Area : %@",placemark.administrativeArea);
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
             self.strState = placemark.administrativeArea;
             
         }
     }];
    
}

#pragma mark - FINDING BLOCKLIST

- (void)findBlickListStates:(NSString *)parameter {
    
    if (APP_DELEGATE.isManualLogin) {
        if ([self.statesList containsObject:parameter.lowercaseString]) {
            NSLog(@"We have %@ in blocked list",parameter);
            [self loadBlockedPopUP];
        }
        else {
            NSLog(@"We don't have %@ in blocked list",parameter);
            [[TAJGameEngine sharedGameEngine] createUserModelWithUserName:self.usernameField.text
                                                                 password:self.passwordField.text sessionId:@"None"];
            
            TAJGameEngine *engine= [TAJGameEngine sharedGameEngine];
            engine.isOtherLoggedIn =FALSE;
            [[TAJClientServerCommunicator sharedClientServerCommunicator] createSocketAndConnect];
            [[TAJGameEngine sharedGameEngine] setCurrentController:self];
            APP_DELEGATE.isManualLogin = NO;
        }
    }
    else {
        if ([self.statesList containsObject:parameter.lowercaseString]) {
            NSLog(@"We have %@ in blocked list",parameter);
            [self loadBlockedPopUP];
        }
        else {
            NSLog(@"We don't have %@ in blocked list",parameter);
            [self login];
        }
    }
    
    //    for (int i = 0 ; i < [self.statesList count ] ; i++) {
    //
    //        if ([[self.statesList objectAtIndex:i] isEqualToString:parameter]) {
    //            NSLog(@"%@ Found in BlockList", parameter);
    //            [self loadBlockedPopUP];
    //
    //        }
    //        else{
    //            NSLog(@"%@ Not Found in BlockList", parameter);
    //            [self login];
    //        }
    //    }
}

#pragma mark - FACEBOOK LOGIN

- (IBAction)fbLoginClicked:(UIButton *)sender
{
    
}

#pragma BLOCKED POPUP

-(void)loadBlockedPopUP
{
    if(!self.blockedPopup)
    {
        self.blockedPopup = [[BlockedPopupViewController alloc] initWithNibName:@"BlockedPopupViewController" bundle:nil];
    }
    self.blockedPopup.BlockedPopupDelegate = self;
    [TAJAppDelegate setScreenSizeForRes: self.blockedPopup.view];
    [self.view addSubview:self.blockedPopup.view];
}

-(void)closeButtonPressedForBlockedPopup
{
    if(self.blockedPopup)
    {
        [self.blockedPopup.view removeFromSuperview];
        self.blockedPopup = nil;
    }
    self.activityIndicator.hidden = YES;
}

@end
