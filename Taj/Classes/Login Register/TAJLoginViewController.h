/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLoginViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 10/01/14.
 **/

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>
#import "TAJUITextField.h"
#import "TAJInfoPopupViewController.h"

@class TAJHomePageViewController;

@interface TAJLoginViewController : UIViewController<InfoPopupViewDelegate>

@property(retain,nonatomic) TAJHomePageViewController *controller;
@property (strong, nonatomic) IBOutlet TAJUITextField *usernameField;
@property (strong, nonatomic) IBOutlet TAJUITextField *passwordField;
@property (strong, nonatomic) TAJInfoPopupViewController *infoPopupViewController;

@end
