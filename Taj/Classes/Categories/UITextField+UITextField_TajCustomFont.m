/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		UITextField+UITextField_TajCustomFont.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 16/04/14.
 **/

#import "UITextField+UITextField_TajCustomFont.h"

@implementation UITextField (UITextField_TajCustomFont)

- (NSString *)fontName
{
    return self.font.fontName;
}

- (void)setFontName:(NSString *)fontName
{
    //DLog(@"%@",fontName);
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end
