/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		NSMutableArray+TAJNSMutableArray.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/05/14.
 **/

#import <Foundation/Foundation.h>

@interface NSMutableArray (TAJNSMutableArray)

- (void)addObjectToStartingIndex:(id)object;
- (void)addObjectToEnd:(id)object;
- (void)addArrayToStartingIndex:(NSArray*)objects;
- (void)addArrayToEnd:(NSArray*)objects;
- (NSArray*)arrayFromAfterObject:(id)object;
- (NSArray*)arrayFromBeforeObject:(id)object;
- (void)insertObjects:(NSArray *)objects atIndex:(NSUInteger)index;
- (void)removeObjectAtIndex:(NSUInteger)index withObject:(id)anObject;
- (void)replaceObjectsAtIndexes:(NSIndexSet *)srcIndexes toIndexes:(NSIndexSet *)dstIndexes;
- (void)replaceObjectsAtIndexes:(NSIndexSet *)indexes toIndex:(NSUInteger)idx;
- (NSMutableArray *)removeObjectsFromIndex:(NSInteger)index inRange:(NSInteger)objectRange;
- (NSMutableArray *)addObjectsFromIndex:(NSInteger)index inRange:(NSInteger)objectRange;

@end
