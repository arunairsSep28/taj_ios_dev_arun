/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		NSMutableArray+TAJNSMutableArray.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/05/14.
 **/

#import "NSMutableArray+TAJNSMutableArray.h"

@implementation NSMutableArray (TAJNSMutableArray)

- (void)addObjectToStartingIndex:(id)object
{
    if (self.count > 0)
    {
        [self removeObjectAtIndex:0];
    }
    [self insertObject:object atIndex:0];
}

- (void)addObjectToEnd:(id)object
{
    [self removeLastObject];
    [self addObject:object];
}

- (void)addArrayToStartingIndex:(NSArray*)objects
{
    [self removeObjectsInArray:objects];
    [self insertObjects:objects atIndex:0];
}

- (void)addArrayToEnd:(NSArray*)objects
{
    [self removeObjectsInArray:objects];
    [self insertObjects:objects atIndex:[self count]];
}

- (NSArray*)arrayFromAfterObject:(id)object
{
    NSInteger idx = [self indexOfObject:object];
    return [self objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(idx+1, [self count] - (idx + 1))]];
}

- (NSArray*)arrayFromBeforeObject:(id)object
{
    NSInteger idx = [self indexOfObject:object];
    return [self objectsAtIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0, idx)]];
}

- (void)insertObjects:(NSArray *)objects atIndex:(NSUInteger)index
{
    [self insertObjects:objects atIndexes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(index, [objects count])]];
}

- (void)replaceObjectsAtIndexes:(NSIndexSet *)srcIndexes toIndexes:(NSIndexSet *)dstIndexes
{
    NSArray *objects = [self objectsAtIndexes:srcIndexes];
    [self removeObjectsAtIndexes:srcIndexes];
    [self insertObjects:objects atIndexes:dstIndexes];
}

- (void)replaceObjectsAtIndexes:(NSIndexSet *)indexes toIndex:(NSUInteger)idx
{
    NSArray *objectsToMove = [self objectsAtIndexes:indexes];
    
    idx -= [indexes countOfIndexesInRange:(NSRange){0, idx}];
    
    [self removeObjectsAtIndexes:indexes];
    [self replaceObjectsInRange:(NSRange){idx,0} withObjectsFromArray:objectsToMove];
}

- (void)removeObjectAtIndex:(NSUInteger)index withObject:(id)anObject
{
    [self removeObjectAtIndex:index];
    [self insertObject:anObject atIndex:index];
}

- (NSMutableArray *)removeObjectsFromIndex:(NSInteger)index inRange:(NSInteger)objectRange
{
    NSMutableArray * result = [self makeArray:index inRange:objectRange];
    [self removeObjectsInRange:NSMakeRange(index, objectRange)];
    return result;
}

- (NSMutableArray *)addObjectsFromIndex:(NSInteger)index inRange:(NSInteger)objectRange
{
    return [self makeArray:index inRange:objectRange];
}

- (NSMutableArray *)makeArray:(NSInteger)index inRange:(NSInteger)objectRange
{
    NSMutableArray * result = [NSMutableArray array];
    int count = 0;
    if (objectRange == 0) objectRange = 1;
    for (NSInteger i = index; count < objectRange; i ++)
    {
        [result addObject:[self objectAtIndex:i]];
        count ++;
    }
    return result;
}

- (NSArray *)arrayByReplacingNullsWithBlanks
{
    NSMutableArray *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    for (int idx = 0; idx < [replaced count]; idx++)
    {
        id object = [replaced objectAtIndex:idx];
        if (object == nul) [replaced replaceObjectAtIndex:idx withObject:blank];
        else if ([object isKindOfClass:[NSDictionary class]]) [replaced replaceObjectAtIndex:idx withObject:[self dictionaryByReplacingNullsWithBlanks:object]];
        else if ([object isKindOfClass:[NSArray class]]) [replaced replaceObjectAtIndex:idx withObject:[object arrayByReplacingNullsWithBlanks]];
    }
    return [replaced copy];
}

- (NSDictionary *)dictionaryByReplacingNullsWithBlanks:(NSDictionary *)dictionary
{
    const NSMutableDictionary *replaced = [self mutableCopy];
    const id nul = [NSNull null];
    const NSString *blank = @"";
    
    for (NSString *key in self) {
        id object = [dictionary objectForKey:key];
        if (object == nul) [replaced setObject:blank forKey:key];
        else if ([object isKindOfClass:[NSDictionary class]]) [replaced setObject:[object dictionaryByReplacingNullsWithBlanks:object] forKey:key];
        else if ([object isKindOfClass:[NSArray class]]) [replaced setObject:[object arrayByReplacingNullsWithBlanks] forKey:key];
    }
    return [NSDictionary dictionaryWithDictionary:[replaced copy]];
}

@end
