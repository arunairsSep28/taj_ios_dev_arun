/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		UILabel+TajCustomFont.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Yogisha Poojary on 02/04/14.
 **/

#import "UILabel+TajCustomFont.h"

@implementation UILabel (TajCustomFont)

- (NSString *)fontName
{
    return self.font.fontName;
}

- (void)setFontName:(NSString *)fontName
{
    //DLog(@"%@",fontName);
    self.font = [UIFont fontWithName:fontName size:self.font.pointSize];
}

@end
