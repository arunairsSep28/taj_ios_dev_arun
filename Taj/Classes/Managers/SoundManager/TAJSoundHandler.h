/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSoundHandler.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Sujit Yadawad on 06/05/14.
 **/

#import <Foundation/Foundation.h>
#import "SoundManager.h"

@interface TAJSoundHandler : NSObject

+ (instancetype)sharedManager;

- (void)playBackgroundMusic;

- (void)playWinnersSound;
- (void)playTossSound;
- (void)playSitSound;
- (void)playDropSound;
- (void)playDrawCard_DiscardSound;
- (void)playClockSound;
- (void)playCardDistributeSound;
- (void)playBellSound;
- (void)playMeldSound;

- (void)DisableSounds;
- (void)EnableSounds;
- (void)StopSounds;

@end
