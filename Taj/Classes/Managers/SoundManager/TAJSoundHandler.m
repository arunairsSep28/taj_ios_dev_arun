/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSoundHandler.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Sujit Yadawad on 06/05/14.
 **/

#import "TAJSoundHandler.h"

#define BELL_SOUND              @"bell.wav"
#define CARD_DISTRIBUTE_SOUND   @"card_distribute.wav"
#define CLOCK_SOUND             @"clock.wav"
#define DRAWCARD_DISCARD_SOUND  @"drawcard_discard.wav"
#define DROP_SOUND              @"drop.wav"
#define SIT_SOUND               @"sit.wav"
#define TOSS_SOUND              @"toss.wav"
#define WINNERS_SOUND           @"winners.wav"
#define MELD_SOUND              @"meld.mp3"

@implementation TAJSoundHandler

+ (instancetype)sharedManager
{
    static TAJSoundHandler *sharedManager = nil;
    if (sharedManager == nil)
    {
        sharedManager = [[self alloc] init];
    }
    return sharedManager;
}

- (instancetype)init
{
    if ((self = [super init]))
    {
        [SoundManager sharedManager].allowsBackgroundMusic = YES;
        [[SoundManager sharedManager] prepareToPlay];
    }
    return self;
}

// If there is anybackground music

- (void)playBackgroundMusic
{
    //[[SoundManager sharedManager] playMusic:BACKGROUND_MUSIC looping:YES];
}

- (void)playWinnersSound
{
    [[SoundManager sharedManager] playSound:WINNERS_SOUND looping:NO];
}

- (void)playTossSound
{
    [[SoundManager sharedManager] playSound:TOSS_SOUND looping:NO];
}

- (void)playSitSound
{
    [[SoundManager sharedManager] playSound:SIT_SOUND looping:NO];
}

- (void)playDropSound
{
    [[SoundManager sharedManager] playSound:DROP_SOUND looping:NO];
}

- (void)playDrawCard_DiscardSound
{
    [[SoundManager sharedManager] playSound:DRAWCARD_DISCARD_SOUND looping:NO];
}

- (void)playClockSound
{
    [[SoundManager sharedManager] playSound:CLOCK_SOUND looping:NO];
}

- (void)playCardDistributeSound
{
    [[SoundManager sharedManager] playSound:CARD_DISTRIBUTE_SOUND looping:NO];
}

- (void)playBellSound
{
    [[SoundManager sharedManager] playSound:BELL_SOUND looping:NO];
}

- (void)playMeldSound
{
    [[SoundManager sharedManager] playSound:MELD_SOUND looping:NO];
}

- (void)DisableSounds
{
    [[SoundManager sharedManager] muteVolume];
}

- (void)StopSounds
{
    [[SoundManager sharedManager] stopAllSounds];
}

- (void)EnableSounds
{
    [[SoundManager sharedManager] UnmuteVolume];
}


@end
