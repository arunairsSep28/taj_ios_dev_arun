//
//  Service.m
//  Taj
//
//
//  Created by svc on 13/12/18.
//  Copyright © 2018 Robosoft Technologies. All rights reserved.
//

#import "Service.h"

@implementation Service

+ (Service *)sharedInstance
{
    static Service *_sharedInstance = nil;
    _sharedInstance = [[Service alloc] init];
    return _sharedInstance;
}

- (void)getRequestWithURL:(NSString *)URL
               withParams:(NSDictionary *)params
          completionBlock:(CompletionBlock)completionBlock
               errorBlock:(ErrorBlock)errorBlock
{
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    
    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",URL,[self httpBodyForParameters:params]]];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:20];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    
    [request setHTTPMethod:@"GET"];
    NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *receivedData, NSURLResponse *responseValue, NSError *error) {
        id jsonResponseString = [[NSString alloc] initWithData:receivedData
                                                      encoding:NSASCIIStringEncoding];
        NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if (error) {
            self.errorBlock(@"error while fetching data");
        }
        else {
            self.completionBlock(jsonResponse);
        }
    }];
    
    [getDataTask resume];
    
}

- (void)getRequestWithHeader:(NSString *)header
                         URL:(NSString *)URL
                  withParams:(NSDictionary *)params
             completionBlock:(CompletionBlock)completionBlock
                  errorBlock:(ErrorBlock)errorBlock
{
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    
    NSURL *url = [NSURL URLWithString:URL];
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:url];
    request.HTTPMethod = @"GET";
    
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request addValue:header forHTTPHeaderField:@"Authorization"];
    
    NSURLSessionDataTask *downloadTask = [session dataTaskWithRequest:request completionHandler:^(NSData *receivedData, NSURLResponse *response, NSError *error) {
        id jsonResponseString = [[NSString alloc] initWithData:receivedData
                                                      encoding:NSASCIIStringEncoding];
        NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if (error) {
            self.errorBlock(@"error while fetching data");
        }
        else {
            self.completionBlock(jsonResponse);
        }
        
    }];
    
    
    [downloadTask resume];
    
}
- (void)getRequestHeaderWithURL:(NSString *)URL
                         header:(NSString *)header
                     withParams:(NSDictionary *)params
                completionBlock:(CompletionBlock)completionBlock
                     errorBlock:(ErrorBlock)errorBlock
{
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
   
   // NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@",URL]];

    NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",URL,[self httpBodyForParameters:params]]];

    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:60];
    [request addValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [request addValue:@"application/json" forHTTPHeaderField:@"Accept"];
     [request addValue:header forHTTPHeaderField:@"Authorization"];
    NSLog(@"GET URL : %@",URL);
    NSLog(@"GET REQUEST : %@",request);
    [request setHTTPMethod:@"GET"];
   // [request setAllHTTPHeaderFields:headers];

    NSURLSessionDataTask *getDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *receivedData, NSURLResponse *responseValue, NSError *error) {
        id jsonResponseString = [[NSString alloc] initWithData:receivedData
                                                      encoding:NSASCIIStringEncoding];
        NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if (error) {
            self.errorBlock(@"error while fetching data");
        }
        else {
            self.completionBlock(jsonResponse);
        }
    }];
    
    [getDataTask resume];
    
}
+(void)postRequest:(NSString *)urlString isHeader:(BOOL)isHeader headerValue:(NSString *)headerValue
       withParams:(NSDictionary *)dict :(void(^)( id loginDict,BOOL Success))completed;
{
    NSLog(@"Post request URL: %@",urlString);
    NSString *str;
    for (int i=0; i<[dict count]; i++)
    {
        //@"%@?%@"
        str=[NSString stringWithFormat:@"%@&%@=%@",str,[[dict allKeys] objectAtIndex:i],[[dict allValues] objectAtIndex:i]];
    }
    if ([str hasPrefix:@"&"])
    {
        str=[str substringFromIndex:1];
    }
    NSString *post=str;
    NSLog(@"Post request String: %@",post);
    NSURL *url=[NSURL URLWithString:urlString];
    NSData *requestData =[post dataUsingEncoding:NSASCIIStringEncoding allowLossyConversion:NO];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:TIME_OUT_INTERVAL];
    
    [request setHTTPMethod:@"POST"];
    [request setURL:url];
    
    if (isHeader) {
        [request setValue:headerValue forHTTPHeaderField:@"Authorization"];
        [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
        [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }
    else {
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    }
    
    [request setValue:[NSString stringWithFormat:@"%lu", (unsigned long)[requestData length]] forHTTPHeaderField:@"Content-Length"];
    [request setHTTPBody:requestData];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:(id<NSURLSessionDataDelegate>)self delegateQueue:nil];
#if DEBUG
    NSLog(@"Post request: %@", request);
#endif
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
#if DEBUG
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        NSLog(@"RESPONSE STATUS CODE: %ld", (long)[httpResponse statusCode]);
        //NSLog(@"RESPONSE  : %@",response);
#endif
        if ( !error )
        {
            id jsonDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
            //NSLog(@"SERVICE RESP : %@",jsonDict);
            if (completed)
            {
                completed(jsonDict,YES);
            }
            else
            {
                id jsonDict=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingMutableLeaves error:&error];
                completed(jsonDict,NO);
                NSLog(@"SERVICE ERROR : %@",jsonDict);
            }
        }
        else
        {
            completed(@"error while fetching data",NO);
        }
    }];
    [postDataTask resume];
    
}

- (void)postRequestWithURL:(NSString *)URL
                withParams:(NSDictionary *)params
           completionBlock:(CompletionBlock)completionBlock
                errorBlock:(ErrorBlock)errorBlock
{
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_HIGH, 0), ^
                   {
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    
    
    NSLog(@"POST PARAMS = %@",params);
    // Changing the Dictinary to String
    NSMutableString *requestString = [NSMutableString string];
    for (NSString *key in [params allKeys]){
        if ([requestString length]>0)
        [requestString appendString:@"&"];
        [requestString appendFormat:@"%@=%@", key, [params objectForKey:key]];
    }
                       
     NSLog(@"Post request String: %@", requestString);
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURL *url = [NSURL URLWithString:URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIME_OUT_INTERVAL];
    [request setHTTPMethod:@"POST"];
    [request setValue:@"application/json" forHTTPHeaderField:@"Accept"];

    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    
    [request setHTTPBody:[requestString dataUsingEncoding:NSUTF8StringEncoding]];
    
#if DEBUG
    NSLog(@"Post request: %@", request);
#endif
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *receivedData, NSURLResponse *responseValue, NSError *error) {
        
        id jsonResponseString = [[NSString alloc] initWithData:receivedData
                                                      encoding:NSASCIIStringEncoding];
        NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if (error) {
            self.errorBlock(@"Error while fetching data from server");
        }
        else {
            self.completionBlock(jsonResponse);
        }
    }];
    
    [postDataTask resume];
    });
}

- (void)postArrayRequestWithURL:(NSString *)URL
                     withParams:(NSDictionary *)paramsArray
                completionBlock:(CompletionBlock)completionBlock
                     errorBlock:(ErrorBlock)errorBlock
{
    self.completionBlock = completionBlock;
    self.errorBlock = errorBlock;
    
    
    NSData *postData = [NSJSONSerialization dataWithJSONObject:paramsArray options:NSJSONWritingPrettyPrinted error:nil];
    NSString *postLength = [NSString stringWithFormat:@"%lu",(unsigned long)[postData length]];
    
    NSURLSessionConfiguration *configuration = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:configuration delegate:self delegateQueue:[NSOperationQueue mainQueue]];
    NSURL *url = [NSURL URLWithString:URL];
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:url
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:TIME_OUT_INTERVAL];
    [request setHTTPMethod:@"POST"];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:postData];
    
#if DEBUG
    NSLog(@"Post array request: %@", request);
#endif
    NSURLSessionDataTask *postDataTask = [session dataTaskWithRequest:request completionHandler:^(NSData *receivedData, NSURLResponse *responseValue, NSError *error) {
        id jsonResponseString = [[NSString alloc] initWithData:receivedData
                                                      encoding:NSASCIIStringEncoding];
        
        NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
        id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        if (error) {
            self.errorBlock(@"Error while fetching data from server");
        }
        else {
            self.completionBlock(jsonResponse);
        }
    }];
    [postDataTask resume];
    
}

- (NSString *)httpBodyForParameters:(NSDictionary *)parameters
{
    NSMutableArray *parameterArray = [NSMutableArray array];
    [parameters enumerateKeysAndObjectsUsingBlock:^(NSString *key, NSString *obj, BOOL *stop) {
        NSString *param = [NSString stringWithFormat:@"%@=%@", [self percentEscapeString:key], [self percentEscapeString:obj]];
        [parameterArray addObject:param];
    }];
    NSString *string = [parameterArray componentsJoinedByString:@"&"];
    return string;
}

- (NSString *)percentEscapeString:(NSString *)string
{
    NSCharacterSet *allowed = [NSCharacterSet characterSetWithCharactersInString:@"abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._~"];
    return [string stringByAddingPercentEncodingWithAllowedCharacters:allowed];
}

#pragma mark - URLSession Delegate

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didBecomeDownloadTask:(NSURLSessionDownloadTask *)downloadTask
{
    
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveResponse:(NSURLResponse *)response
    completionHandler:(void (^)(NSURLSessionResponseDisposition disposition))completionHandler
{
    completionHandler(NSURLSessionResponseAllow);
}

- (void)URLSession:(NSURLSession *)session dataTask:(NSURLSessionDataTask *)dataTask
    didReceiveData:(NSData *)data
{
    NSString * str = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    NSLog(@"Received String %@",str);
}

- (void)URLSession:(NSURLSession *)session task:(NSURLSessionTask *)task
    didCompleteWithError:(NSError *)error
{
    if (error == nil)
    NSLog(@"Download is Succesfull");
    else
    NSLog(@"URLSession ERROR %@",[error userInfo]);
}

- (void)URLSessionDidFinishEventsForBackgroundURLSession:(NSURLSession *)session
{
    NSLog(@"Background URL session %@ finished events.\n", session);
    TAJAppDelegate * delegate =(TAJAppDelegate *)[[UIApplication sharedApplication] delegate];
    if (delegate.completionHandler) {
        void (^handler)(void) = delegate.completionHandler;
        handler();
    }
}

- (void)URLSession:(NSURLSession *)session didReceiveChallenge:(NSURLAuthenticationChallenge *)challenge completionHandler:(void (^)(NSURLSessionAuthChallengeDisposition, NSURLCredential * _Nullable))completionHandler {
}

#pragma mark - Top ViewController

- (UIViewController *)findTopViewController {
    return [self topViewController:[UIApplication sharedApplication].keyWindow.rootViewController];
}

- (UIViewController *)topViewController:(UIViewController *)rootViewController
{
    if ([rootViewController isKindOfClass:[UINavigationController class]]) {
        UINavigationController *navigationController = (UINavigationController *)rootViewController;
        return [self topViewController:[navigationController.viewControllers lastObject]];
    }
    if ([rootViewController isKindOfClass:[UITabBarController class]]) {
        UITabBarController *tabController = (UITabBarController *)rootViewController;
        return [self topViewController:tabController.selectedViewController];
    }
    if (rootViewController.presentedViewController) {
        return [self topViewController:rootViewController];
    }
    return rootViewController;
}

@end
