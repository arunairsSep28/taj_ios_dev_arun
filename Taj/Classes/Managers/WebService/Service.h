//
//  Service.h
//  Taj
//
//
//  Created by svc on 13/12/18.
//  Copyright © 2018 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "TAJAppDelegate.h"

@interface Service : NSObject <NSURLSessionDelegate>

typedef void(^CompletionBlock)(id responseData);
typedef void(^ErrorBlock)(NSString *errorString);

@property(nonatomic, copy) CompletionBlock completionBlock;
@property(nonatomic, copy) ErrorBlock errorBlock;

+ (Service *)sharedInstance;

- (void)getRequestWithURL:(NSString *)URL
               withParams:(NSDictionary *)params
          completionBlock:(CompletionBlock)completionBlock
               errorBlock:(ErrorBlock)errorBlock;
- (void)getRequestHeaderWithURL:(NSString *)URL
                         header:(NSString *)header
               withParams:(NSDictionary *)params
          completionBlock:(CompletionBlock)completionBlock
               errorBlock:(ErrorBlock)errorBlock;

- (void)getRequestWithHeader:(NSString *)header
                         URL:(NSString *)URL
                     withParams:(NSDictionary *)params
                completionBlock:(CompletionBlock)completionBlock
                     errorBlock:(ErrorBlock)errorBlock;

- (void)postRequestWithURL:(NSString *)URL
                withParams:(NSDictionary *)params
           completionBlock:(CompletionBlock)completionBlock
                errorBlock:(ErrorBlock)errorBlock;

- (void)postArrayRequestWithURL:(NSString *)URL
                     withParams:(NSDictionary *)params
                completionBlock:(CompletionBlock)completionBlock
                     errorBlock:(ErrorBlock)errorBlock;

+ (void)postRequest:(NSString *)url
           isHeader:(BOOL)isHeader
        headerValue:(NSString *)headerValue
         withParams:(NSDictionary *)dict
                   :(void(^)( id loginDict,BOOL Success))completed;

@end
