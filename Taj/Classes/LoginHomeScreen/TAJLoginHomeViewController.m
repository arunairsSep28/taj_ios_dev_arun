/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLoginHomeViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 27/03/14.
 **/

#import "TAJLoginHomeViewController.h"
#import "TAJLoginViewController.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJAppDelegate.h"

@interface TAJLoginHomeViewController ()

@property (weak, nonatomic) IBOutlet UIButton *loginButton;
@property (weak, nonatomic) IBOutlet UIButton *playNowButton;
@property (weak, nonatomic) IBOutlet UIButton *responsibleGamingButton;
@property (weak, nonatomic) IBOutlet UILabel *versionNumberLabel;

- (IBAction)loginButtonAction:(UIButton *)sender;
- (IBAction)playNowButtonAction:(UIButton *)sender;
- (IBAction)responsibleGamingButtonTapped:(UIButton *)sender;

@property (strong, nonatomic) TAJLoginViewController *loginController;

@end

@implementation TAJLoginHomeViewController

- (id)init:(LoginScreenOption)option
{
    self = [super init];
    if (self)
    {
        self.loginOption = option;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    [self updateButtons];
    self.loginButton.exclusiveTouch = YES;
    self.playNowButton.exclusiveTouch = YES;
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        // if iOS 7
        self.edgesForExtendedLayout = UIRectEdgeNone; //layout adjustements
    }
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    
    self.versionNumberLabel.text=VERSION_NUMBER;
}

- (void)updateButtons
{
    switch (self.loginOption)
    {
        case eLoginOption:
            self.loginButton.hidden = NO;
            self.playNowButton.hidden = YES;
            break;
            
        case ePlayNowOption:
            self.loginButton.hidden = YES;
            self.playNowButton.hidden = NO;
            break;
            
        case eBothOption:
            self.loginButton.hidden = NO;
            self.playNowButton.hidden = NO;
            break;
            
        default:
            self.loginButton.hidden = NO;
            self.playNowButton.hidden = NO;
            break;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (NSUInteger)supportedInterfaceOrientations // iOS 6 autorotation fix
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (BOOL)shouldAutorotate  // iOS 6 autorotation fix
{
    return YES;
}

- (IBAction)loginButtonAction:(UIButton *)sender
{
    [((TAJAppDelegate*)[[UIApplication sharedApplication] delegate]) loadLoginScreenWindow];
}

- (IBAction)playNowButtonAction:(UIButton *)sender
{
    TAJAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [delegate loadPrelobbyScreen];
}
- (IBAction)registrationButtonAction:(UIButton *)sender
{
    [(TAJAppDelegate*)TAJ_APP_DELEGATE loadRegistrationScreen];
}
- (IBAction)responsibleGamingButtonTapped:(UIButton *)sender
{
    if (self.loginOption==eLoginOption) {
        self.webPageOption=eResponsibleGamingWithLoginOption;
    }
    else
    {
        self.webPageOption=eResponsibleGamingWithPlayNowOption;
    }
    [((TAJAppDelegate*)[[UIApplication sharedApplication] delegate]) loadWebPage:self.webPageOption];
}

@end
