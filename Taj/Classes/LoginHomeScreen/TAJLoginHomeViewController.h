/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLoginHomeViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 27/03/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJConstants.h"

@interface TAJLoginHomeViewController : UIViewController

@property(nonatomic,assign) LoginScreenOption loginOption;
@property(nonatomic,assign) WebPageViewOption webPageOption;

- (id)init:(LoginScreenOption)option;

- (void)updateButtons;

@end
