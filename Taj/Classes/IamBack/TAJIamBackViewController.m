/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJIamBackViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Pradeep BM on 12/05/14.
 **/

#import "TAJIamBackViewController.h"
#import "TAJGameEngine.h"
#import "TAJIAmBackCell.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJLobby.h"
#import "TAJGameTable.h"

#define IAm_Back_Cell_Identifier             @"IAmBackcell"
#define IAmBackCellNibName                   @"TAJIAmBackCell"
#define IAM_BACK_CARD_OFFSET_IPHONE_OFFSET   56
#define IAM_BACK_CARD_OFFSET_IPAD_OFFSET     115
#define IAM_BACK_SCROLL_CARD_REACH_AT        4
#define IAM_BACK_INSTRUCTION_MSG             @"Your game was set to Auto Play and %@/%@ turns have been played and below are the discarded card's"
#define IAM_BACK_USER_INFO                   @"Please click on the \"I Am Back\" button to continue with your game"
@interface TAJIamBackViewController ()<TAJIAmBackCellProtocol>

// Avatar image view
@property (weak, nonatomic) IBOutlet UIImageView *avatarImageView;

// instruction label
@property (weak, nonatomic) IBOutlet UILabel *instructionLabel;

// message label
@property (weak, nonatomic) IBOutlet UILabel *autoPlayMessageLabel;

//Holder View
@property (weak, nonatomic) IBOutlet UIView *cardHolderView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (strong, nonatomic) NSString *jokerCardNumber;
// i am button

- (IBAction)iamBackButtonAction:(UIButton *)sender;

@end

@implementation TAJIamBackViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.iamBackButton.enabled = YES;
    if (self.iamBackModel.thisPlayerNetworkCardsArray.count <= 0)
    {
        self.instructionLabel.text = IAM_BACK_USER_INFO;
        for (TAJIAmBackCell *card in self.cardHolderView.subviews)
        {
            if (card)
            {
                [card removeFromSuperview];
            }
        }
    }
    self.versionLabel.text = VERSION_NUMBER;
    if (self.iamBackModel)
    {
        [self displayDiscardedCards];
    }
}

- (BOOL)isIAMBackShowing
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

- (void)viewWillAppear:(BOOL)animated
{
    self.iamBackButton.enabled = YES;
    
    [super viewWillAppear:animated];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:
    (UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (NSUInteger)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskLandscape;
}

#pragma mark - Update data -

- (void)updateAvatarImageForView:(NSString *)avatarName
{
    DLog(@"avatarName = %@", avatarName);
    self.avatarImageView.image = [UIImage imageNamed:avatarName];
}

- (void)showIAmBackMessage:(NSString *)message
{
    self.autoPlayMessageLabel.text = [NSString stringWithFormat:@"%@",message];
    if([message isEqualToString:I_AM_BUTTON_CAN_CLICK]){
        self.iamBackButton.userInteractionEnabled = YES;
        self.iamBackButton.alpha = 1;
    }else{
        self.iamBackButton.userInteractionEnabled = NO;
        self.iamBackButton.alpha = 0.6;
    }
}

- (void)resetIamBackModelForNewGame
{
    if (self.iamBackModel)
    {
        [self.iamBackModel removeAllNetworkHistory];
        
        [self removeOldCardForNewGame];
    }
}

- (void)updateAutoPlayCount:(NSString *)autoPlayCount withTotalCount:(NSString *)totalCount
{
    if (self.iamBackModel)
    {
        NSString *playerCount = [NSString stringWithFormat:@"%lu",(unsigned long)self.iamBackModel.thisPlayerNetworkCardsArray.count];
        
        self.instructionLabel.text = [NSString stringWithFormat:IAM_BACK_INSTRUCTION_MSG,playerCount,totalCount];
        if (self.iamBackModel.thisPlayerNetworkCardsArray.count <= 0)
        {
            self.instructionLabel.text = IAM_BACK_USER_INFO;
            for (TAJIAmBackCell *card in self.cardHolderView.subviews)
            {
                if (card)
                {
                    [card removeFromSuperview];
                }
            }
        }
    }
}

- (void)showDiscardCardInAutoPlayMode
{
    // if player not yet clicked I'm Back button update discard card
    [self displayDiscardedCards];
}

- (void)displayDiscardedCards
{
    NSInteger flag;
    CGRect xframe = self.view.frame;
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    double halfWidth = screenSize.width/2;
    CGRect yframe = self.cardHolderView.frame;
    [self.iamBackDelegate iamBackNetworkCardArrayCount:[self.iamBackModel.thisPlayerNetworkCardsArray count]];
    [self removeOldCardForNewGame];
    if ([self.iamBackModel.thisPlayerNetworkCardsArray count] > 0)
    {
        if([self.iamBackModel.thisPlayerNetworkCardsArray count] == 1)
        {
            flag = 0;
            if ([TAJUtilities isIPhone])
            {
                //yframe.origin.x = xframe.size.width/2 -  25;
                yframe.origin.x = halfWidth -  25;
            }
            else
            {
                //yframe.origin.x = xframe.size.width/2 -  70;
                yframe.origin.x = halfWidth -  70;
            }
        }
        
        if([self.iamBackModel.thisPlayerNetworkCardsArray count] == 2)
        {
            flag = 1;
            if ([TAJUtilities isIPhone])
            {
                //yframe.origin.x = xframe.size.width/2 - 55;
                yframe.origin.x = halfWidth -  55;
            }
            else
            {
                //yframe.origin.x = xframe.size.width/2 - 125;
                yframe.origin.x = halfWidth -  125;
            }
        }
        
        if([self.iamBackModel.thisPlayerNetworkCardsArray count] == 3)
        {
            flag = 2;
        }
        
        if([self.iamBackModel.thisPlayerNetworkCardsArray count] == 4)
        {
            flag = 3;
        }
        
        if([self.iamBackModel.thisPlayerNetworkCardsArray count] == 5)
        {
            flag = 4;
        }
        
        if([self.iamBackModel.thisPlayerNetworkCardsArray count] >=3)
        {
            if ([TAJUtilities isIPhone])
            {
                //yframe.origin.x = xframe.size.width/2 - flag * 35;
                yframe.origin.x = halfWidth -  (flag * 35);
            }
            else
            {
                //yframe.origin.x = xframe.size.width/2 - flag * 80;
                yframe.origin.x = halfWidth -  (flag * 80);
            }
            
        }
        self.cardHolderView.frame = yframe;
        if(self.iamBackModel.thisPlayerNetworkCardsArray.count == 6)
        {
            [self.iamBackModel.thisPlayerNetworkCardsArray removeLastObject];
        }
        for (int i = self.iamBackModel.thisPlayerNetworkCardsArray.count-1; i >= 0 ; i--)
        //for (int i = 0; i < self.iamBackModel.thisPlayerNetworkCardsArray.count; i ++)
        {
            NSDictionary *cardInfo = self.iamBackModel.thisPlayerNetworkCardsArray[i];
            
            NSArray *nibContents = [NSArray array];
            // Place new card
            int index = 0;
            
            if ([TAJUtilities isIPhone])
            {
                index = 0;//[TAJUtilities isItIPhone5]? 0 : 1;
            }
            nibContents = [[NSBundle mainBundle] loadNibNamed:IAmBackCellNibName
                                                        owner:nil
                                                      options:nil];
            
            TAJIAmBackCell *card = nibContents[index];
            
            card.userInteractionEnabled = NO;
            card.iamBackCellDelegate = self;
            [card placeAutoPlayCards:cardInfo];
            
            [self placeAutoPlayCard:card onHolderView:i];
        }
        
        NSString *playerCount = [NSString stringWithFormat:@"%lu",(unsigned long)self.iamBackModel.thisPlayerNetworkCardsArray.count];
        
        self.instructionLabel.text = [NSString stringWithFormat:IAM_BACK_INSTRUCTION_MSG,playerCount,@"5"];
    }
}

- (void)placeAutoPlayCard:(TAJIAmBackCell *)card onHolderView:(NSUInteger)index
{
    CGRect viewFrame = card.frame;
    
    if ([TAJUtilities isIPhone])
    {
        viewFrame.origin.x = index * IAM_BACK_CARD_OFFSET_IPHONE_OFFSET;
    }
    else
    {
        viewFrame.origin.x = index * IAM_BACK_CARD_OFFSET_IPAD_OFFSET;
    }
    
    viewFrame.origin.y = ([TAJUtilities isIPhone] ? 2 : 12.5);
    
    viewFrame.size.width = ([TAJUtilities isIPhone] ? 55 : 113);
    viewFrame.size.height = ([TAJUtilities isIPhone] ? 70 : 143);
    
    card.frame = viewFrame;
    
    [self.cardHolderView addSubview:card];
}

- (void)removeOldCardForNewGame
{
    for (TAJIAmBackCell *card in self.cardHolderView.subviews)
    {
        if (card)
        {
            [card removeFromSuperview];
            
        }
    }
    NSString *playerCount = [NSString stringWithFormat:@"%lu",(unsigned long)self.iamBackModel.thisPlayerNetworkCardsArray.count];
    self.instructionLabel.text = [NSString stringWithFormat:IAM_BACK_INSTRUCTION_MSG,playerCount,@"5"];
    if (self.iamBackModel.thisPlayerNetworkCardsArray.count <= 0)
    {
        self.instructionLabel.text = IAM_BACK_USER_INFO;
    }
}

#pragma mark - IBActions -

- (IBAction)iamBackButtonAction:(UIButton *)sender
{
    [self quitIAmBackView];
}

- (void)quitIAmBackView
{
    if (self.iamBackDelegate && [self.iamBackDelegate respondsToSelector:@selector(didQuitIAmBack)])
    {
        [self.iamBackDelegate didQuitIAmBack];
        
    }
    [self removeOldCardForNewGame];
    [self.iamBackModel.thisPlayerNetworkCardsArray removeAllObjects];
}

- (NSString *)getJokerCardNumberForIamBackCell
{
    if (self.iamBackDelegate && [self.iamBackDelegate respondsToSelector:@selector(getJokerCardNumberForIamBack)])
    {
        self.jokerCardNumber = [self.iamBackDelegate getJokerCardNumberForIamBack];
    }
    return self.jokerCardNumber;

}

@end
