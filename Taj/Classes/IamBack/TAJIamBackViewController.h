/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJIamBackViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Pradeep BM on 12/05/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJIamBackModel.h"

@protocol TAJIamBackViewControllerDelegate;

@interface TAJIamBackViewController : UIViewController

@property (nonatomic, weak)  id<TAJIamBackViewControllerDelegate>   iamBackDelegate;
@property (nonatomic, strong)   TAJIamBackModel     *iamBackModel;
@property (nonatomic) BOOL  isIamBackButtonPressed;
@property (weak, nonatomic) IBOutlet UIButton *iamBackButton;
- (BOOL)isIAMBackShowing;
- (void)showDiscardCardInAutoPlayMode;
- (void)showIAmBackMessage:(NSString *)message;
- (void)updateAvatarImageForView:(NSString *)avatarName;
- (void)updateAutoPlayCount:(NSString *)autoPlayCount withTotalCount:(NSString *)totalCount;
- (void)resetIamBackModelForNewGame;
- (void)removeOldCardForNewGame;

@end

@protocol TAJIamBackViewControllerDelegate <NSObject>
@optional
- (void)didQuitIAmBack;
- (void)iamBackNetworkCardArrayCount:(NSInteger)arrayCount;
-(NSString *)getJokerCardNumberForIamBack;

@end
