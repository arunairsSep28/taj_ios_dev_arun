/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJIamBackModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Pradeep BM on 16/05/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJIamBackModel : NSObject

@property (nonatomic, strong)   NSMutableDictionary *networkCompleteHistoryDict;
@property (nonatomic, strong)   NSMutableArray  *networkDiscardCardArray;
@property (nonatomic, strong)   NSMutableArray  *thisPlayerNetworkCardsArray;
@property (nonatomic, strong)   NSString  *currentTableID;
@property (nonatomic, assign)   int  currentAutoPlayCount;


- (TAJIamBackModel *)initWithIamBackModel:(NSDictionary *)disconnectedTableDetails;
- (void)updateIamBackModel:(NSDictionary *)dictionary;
- (void)removeThisPlayerCardsOnceNewGameStart;
- (void)addDiscardCardForIamBackModel:(NSDictionary *)dictionary;
- (void)addPickedCardForIamBackModel:(NSDictionary *)dictionary;
- (void)removeAllNetworkHistory;
- (void)updateIambackModelArray;

@end
