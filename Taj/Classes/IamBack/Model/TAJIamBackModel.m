/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJIamBackModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Lloyd Praveen Mascarenhas on 27/07/2014.
 Created by  Pradeep BM on 16/05/14.
 **/

#import "TAJIamBackModel.h"
#import "TAJGameEngine.h"
#import "TAJConstants.h"
#import "TAJLobby.h"
#import "TAJGameTable.h"

@implementation TAJIamBackModel

- (TAJIamBackModel *)initWithIamBackModel:(NSDictionary *)disconnectedTableDetails
{
    self.networkCompleteHistoryDict = [NSMutableDictionary dictionaryWithDictionary:disconnectedTableDetails];
    
    self.networkDiscardCardArray = [NSMutableArray array];
    self.thisPlayerNetworkCardsArray = [NSMutableArray array];
    
    self.currentTableID = [self.networkCompleteHistoryDict valueForKey:TAJ_TABLE_ID];
    if ([self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]
        && [[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS] isKindOfClass:[NSArray class]])
    {
        self.networkDiscardCardArray = [NSMutableArray arrayWithArray:[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]];
    }
    else if ([self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]
             && [[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS] isKindOfClass:[NSDictionary class]])
    {
        
        NSMutableDictionary *singleDict = [NSMutableDictionary dictionaryWithDictionary:[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]];
        
        [self.networkDiscardCardArray addObject:singleDict];
    }
    
    NSPredicate *predicateThisPLayer = [NSPredicate predicateWithFormat:kTaj_networkdiscard_predict,[[[TAJGameEngine sharedGameEngine] usermodel] userId]];
    self.thisPlayerNetworkCardsArray = [NSMutableArray arrayWithArray:[self.networkDiscardCardArray filteredArrayUsingPredicate:predicateThisPLayer]];
    DLog(@"autoplay count in init %d , %d",self.currentAutoPlayCount,self.thisPlayerNetworkCardsArray.count);
    if (self .currentAutoPlayCount  > 1 && self.thisPlayerNetworkCardsArray.count > 0 )
    {
        NSMutableArray *dicardCardsArray = [NSMutableArray array];
        for (int index=1; index < self.currentAutoPlayCount; index++)
        {
            [dicardCardsArray addObject:[self.thisPlayerNetworkCardsArray objectAtIndex:(self.thisPlayerNetworkCardsArray.count-index)]];
        }
        [self.thisPlayerNetworkCardsArray removeAllObjects];
        [self.thisPlayerNetworkCardsArray addObjectsFromArray:dicardCardsArray];
    }
    else
    {
        [self.thisPlayerNetworkCardsArray removeAllObjects];
    }
    return self;
}

- (void) updateIamBackModel:(NSDictionary *)dictionary
{
    self.networkCompleteHistoryDict = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    
    self.networkDiscardCardArray = [NSMutableArray array];
    self.thisPlayerNetworkCardsArray = [NSMutableArray array];
    
    self.currentTableID = [self.networkCompleteHistoryDict valueForKey:TAJ_TABLE_ID];
    if ([self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]
        && [[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS] isKindOfClass:[NSArray class]])
    {
        self.networkDiscardCardArray = [NSMutableArray arrayWithArray:[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]];
    }
    else if ([self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]
             && [[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS] isKindOfClass:[NSDictionary class]])
    {
        
        NSMutableDictionary *singleDict = [NSMutableDictionary dictionaryWithDictionary:[self.networkCompleteHistoryDict valueForKeyPath:NETWORK_DISCARDED_CARDS]];
        
        [self.networkDiscardCardArray addObject:singleDict];
    }
    
    NSPredicate *predicateThisPLayer = [NSPredicate predicateWithFormat:kTaj_networkdiscard_predict,[[[TAJGameEngine sharedGameEngine] usermodel] userId]];
    self.thisPlayerNetworkCardsArray = [NSMutableArray arrayWithArray:[self.networkDiscardCardArray filteredArrayUsingPredicate:predicateThisPLayer]];
    DLog(@"autoplay count in updatre  %d",self.currentAutoPlayCount);
    if (self .currentAutoPlayCount  > 1 && self.thisPlayerNetworkCardsArray.count > 0)
    {
        
        NSMutableArray *dicardCardsArray = [NSMutableArray array];
        for (int index=1; index < self.currentAutoPlayCount; index++)
        {
            [dicardCardsArray addObject:[self.thisPlayerNetworkCardsArray objectAtIndex:(self.thisPlayerNetworkCardsArray.count-index)]];
        }
        [self.thisPlayerNetworkCardsArray removeAllObjects];
        NSArray *array = [[dicardCardsArray reverseObjectEnumerator] allObjects];
        [self.thisPlayerNetworkCardsArray  addObjectsFromArray:array];
        
    }
    else
    {
        [self.thisPlayerNetworkCardsArray removeAllObjects];
        
    }
}

- (void)removeThisPlayerCardsOnceNewGameStart
{
    NSPredicate *predicateThisPLayer = [NSPredicate predicateWithFormat:kTaj_removediscard_predict,[[[TAJGameEngine sharedGameEngine] usermodel] userId]];
    NSMutableArray *tempArray = [NSMutableArray arrayWithArray:[self.networkDiscardCardArray filteredArrayUsingPredicate:predicateThisPLayer]];
    [self.networkDiscardCardArray removeObjectsInArray:tempArray];
    
    predicateThisPLayer = [NSPredicate predicateWithFormat:kTaj_networkdiscard_predict,[[[TAJGameEngine sharedGameEngine] usermodel] userId]];
    self.thisPlayerNetworkCardsArray = [NSMutableArray arrayWithArray:[self.networkDiscardCardArray filteredArrayUsingPredicate:predicateThisPLayer]];
}

- (void)addDiscardCardForIamBackModel:(NSDictionary *)dictionary
{
    [dictionary setValue:@"discard" forKey:kTaj_Network_card_type];
    [self.networkDiscardCardArray addObject:dictionary];
    
    if([[dictionary valueForKey:kTaj_userid_Key] isEqualToString:[[[TAJGameEngine sharedGameEngine] usermodel] userId]] && [dictionary[kTaj_autoPlay_status] isEqualToString:@"True"])
    {
        [self.thisPlayerNetworkCardsArray addObject:dictionary];
        
    }
}

- (void)addPickedCardForIamBackModel:(NSDictionary *)dictionary
{
    [self.networkDiscardCardArray addObject:dictionary];
}

- (void)updateIambackModelArray
{
    if(self.thisPlayerNetworkCardsArray.count > self.currentAutoPlayCount-1)
    {
        if (self .currentAutoPlayCount  > 1 && self.thisPlayerNetworkCardsArray.count > 0 )
        {
            NSMutableArray *dicardCardsArray = [NSMutableArray array];
            for (int index=1; index < self.currentAutoPlayCount; index++)
            {
                [dicardCardsArray addObject:[self.thisPlayerNetworkCardsArray objectAtIndex:(self.thisPlayerNetworkCardsArray.count-index)]];
            }
            [self.thisPlayerNetworkCardsArray removeAllObjects];
            //[self.thisPlayerNetworkCardsArray addObjectsFromArray:dicardCardsArray];
            NSArray *array = [[dicardCardsArray reverseObjectEnumerator] allObjects];
            [self.thisPlayerNetworkCardsArray  addObjectsFromArray:array];
            
        }
        else
        {
            [self.thisPlayerNetworkCardsArray removeAllObjects];
            
        }
    }
}

- (void)removeAllNetworkHistory
{
    [self.networkCompleteHistoryDict removeAllObjects];
    [self.networkDiscardCardArray removeAllObjects];
    [self.thisPlayerNetworkCardsArray removeAllObjects];
    self .currentAutoPlayCount = 0;
}

@end
