/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJIAmBackCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Pradeep BM on 16/05/14.
 **/

#import "TAJIAmBackCell.h"
#import "TAJConstants.h"

@interface TAJIAmBackCell ()

@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *autoPlayImageView;
@property (strong, nonatomic) NSString *jokerCardNumber;
@property (strong, nonatomic) NSString *cardId;

@property (weak, nonatomic) IBOutlet UIImageView *jokerCardImageView;
@end

@implementation TAJIAmBackCell

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (void)placeAutoPlayCards:(NSDictionary *)dictionary
{
    DLog(@"placeAutoPlayCards = %@",dictionary);
    NSString *cardID = [NSString stringWithFormat:@"%@%@",dictionary[kTaj_Card_Face_Key],[dictionary[kTaj_Card_Suit_Key] uppercaseString]];
    self.cardId = cardID;
    UIImage *cardImage = [UIImage imageNamed:cardID];
    self.cardImageView.image = cardImage;
    if ([dictionary[kTaj_autoPlay_status] isEqualToString:@"True"])
    {
        self.autoPlayImageView.hidden = NO;
    }
    //show joker label if joker card is exist in my deck of cards
    self.jokerCardNumber = [self.iamBackCellDelegate getJokerCardNumberForIamBackCell];
    DLog(@"self.jokerCardNumber = %d",[self.jokerCardNumber integerValue]);
    if ([self.jokerCardNumber isEqualToString:@"0"] && [dictionary[kTaj_Card_Face_Key] isEqualToString:@"1"])
    {
        self.jokerCardImageView.hidden = NO;
    }
    else if ([self.jokerCardNumber isEqualToString:dictionary[kTaj_Card_Face_Key]])
    {
        self.jokerCardImageView.hidden = NO;
    }
}

- (NSString *)cardNumber
{
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.cardId];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    return numberString;
}



@end
