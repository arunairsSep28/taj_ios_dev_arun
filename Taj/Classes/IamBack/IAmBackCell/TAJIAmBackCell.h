/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJIAmBackCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Pradeep BM on 16/05/14.
 **/

#import <UIKit/UIKit.h>
@protocol TAJIAmBackCellProtocol;

@interface TAJIAmBackCell : UIView
@property (weak, nonatomic) id<TAJIAmBackCellProtocol> iamBackCellDelegate;
- (void)placeAutoPlayCards:(NSDictionary *)dictionary;

@end

@protocol TAJIAmBackCellProtocol <NSObject>
@optional
- (NSString *)getJokerCardNumberForIamBackCell;
@end
