/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPreLobbyViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Yogisha Poojary on 20/08/14.
 Modified by  Lloyd Praveen Mascarenhas on 11/07/14.
 Modified by  Pradeep BM on 15/05/14.
 Created by  Raghavendra Kamat on 28/04/14.
 **/

#import "TAJPreLobbyViewController.h"
#import "TAJInfoPopupViewController.h"
#import "TAJMainScreenViewController.h"
#import "TAJSwitchLobbyViewController.h"
#import "TAJLobbyViewController.h"
#import "TAJMoreButtonViewController.h"
#import "TAJUtilities.h"
#import "TAJAppDelegate.h"
#import "TAJGameEngine.h"
#import "TAJConstants.h"


@interface TAJPreLobbyViewController ()<TAJSwitchLobbyViewControllerDelegate,TAJLobbyViewControllerDelegate, TAJMainScreenViewControllerDelegate, TAJMoreButtonViewControllerDelegate,TAJSupportButtonViewControllerDelegate,TAJWebPageViewControllerDelegate, InfoPopupViewDelegate >

@property (weak, nonatomic) IBOutlet UIImageView *tableAlertImageView;
@property (nonatomic, strong)   TAJInfoPopupViewController  *twoTableAlertView;
@property (nonatomic, strong)   TAJInfoPopupViewController  *otherLoginAlertView;
@property (nonatomic, strong)   TAJInfoPopupViewController  *engineUnderMaintenanceAlertView;
@property (nonatomic, strong)   TAJInfoPopupViewController  *errorAlertView;
@property (nonatomic, strong)   TAJInfoPopupViewController  *cannotLogoutAlertView;
@property (nonatomic, strong)   TAJInfoPopupViewController  *logoutAlertView;
@property (nonatomic, strong) UIView  *currentView;
@property (weak, nonatomic) IBOutlet UIView *gamePlayHolderView;
@property (nonatomic)           BOOL        isGamePlayScreen;
@property (weak, nonatomic) IBOutlet UIButton *homeButton;
@property (weak, nonatomic) IBOutlet UIButton *lobbyButton;
@property (weak, nonatomic) IBOutlet UIButton *casheirButtoon;
@property (weak, nonatomic) IBOutlet UIButton *tabelsButton;
@property (weak, nonatomic) IBOutlet UIButton *moreButton;
@property (weak, nonatomic) IBOutlet UIButton *supportButton;

@property (weak, nonatomic) IBOutlet UIView *viewHomeTab;
@property (weak, nonatomic) IBOutlet UIView *viewLobbyTab;
@property (weak, nonatomic) IBOutlet UIView *viewCashierTab;
@property (weak, nonatomic) IBOutlet UIView *viewTablesTab;
@property (weak, nonatomic) IBOutlet UIView *viewMoreTab;
@property (weak, nonatomic) IBOutlet UIView *viewSupportTab;

@property (nonatomic)           BOOL        isHomeButtonPressed;
@property (nonatomic)           BOOL        isLobbyButtonPressed;
@property (nonatomic)           BOOL        isCashierButtonPressed;
@property (nonatomic)           BOOL        isSupportButtonPressed;
@property (nonatomic)           BOOL        isTablesButtonPressed;
@property (nonatomic)           BOOL        isMoreButtonPressed;
@property (nonatomic, strong)   TAJRegistrationSuccessPopupViewController  *registrationPopup;
@end

@implementation TAJPreLobbyViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        [self addPrelobbyScreenObserver];
        [self createLobbyTableController];
#if LANDSCAPE_MODE_GAME
        [self createSwitchLobbyScreenController];
#else
        if (self.currentView != self.switchLobbyController.view)
        {
            [self createSwitchLobbyScreenController];
        }
#endif
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    [self preInitialization];
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    [self createMainScreenAndLoad];
    [self createMoreButtonViewController];
    [self createSupportButtonViewController];
    
    [self isGamePlayWindow:NO];
    self.isHomeButtonPressed = YES;
    self.isMoreButtonPressed = YES;
    self.isTablesButtonPressed = YES;
    self.isLobbyButtonPressed = YES;
    self.isCashierButtonPressed = YES;
    self.isSupportButtonPressed = YES;
    
    
    
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    [self removePrelobbyScreenObserver];
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showProgressAlert:) name:SHOW_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEngineUnderMaintenanceAlert:) name:SHOW_ENGINE_STATUS_TRUE_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showEngineUnderMaintenanceAlert:) name:SHOW_ENGINE_STATUS_FALSE_ALERT object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(closePopUp:)
                                                 name:@"closePopUp"
                                               object:nil];
}

-(void) viewDidAppear:(BOOL)animated
{
    for(UIView* v in self.view.subviews)
    {
        if([v isKindOfClass:[UIButton class]])
        {
            UIButton* btn = (UIButton*)v;
            [btn setExclusiveTouch:YES];
        }
    }
    [self.twoTableAlertView hide];
    self.twoTableAlertView = nil;
    [self.errorAlertView hide];
    self.errorAlertView = nil;
    [[TAJGameEngine sharedGameEngine] getPlayerCount];
    [[TAJGameEngine sharedGameEngine] getTablesCount];
}

- (void) viewDidDisappear:(BOOL)animated
{
    [self removeObservers];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    [self removePrelobbyScreenObserver];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationMaskPortrait;
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)addPrelobbyScreenObserver
{
    [self removePrelobbyScreenObserver];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noOfTajTableCount:) name:TABLE_LENGTH object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(noOfTajPlayerCount:) name:PLAYER_COUNT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(showOtherLoginAlert:) name:SHOW_OTHER_LOGIN_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(serverErrorEvent:)
                                                 name:ERROR_NOTIFICATION
                                               object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(requestJoinTable:)
                                                 name:REQUEST_JOIN_TABLE
                                               object:nil];
    
}

- (void)removePrelobbyScreenObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:TABLE_LENGTH object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:PLAYER_COUNT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:ERROR_NOTIFICATION object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_OTHER_LOGIN_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:REQUEST_JOIN_TABLE object:nil];
}

- (void)removeObservers
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_ENGINE_STATUS_TRUE_ALERT object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:SHOW_ENGINE_STATUS_FALSE_ALERT object:nil];
}

- (void)requestJoinTable:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [[TAJGameEngine sharedGameEngine] requestJoinTableReply:dictionary[@"TAJ_msg_uuid"]];
    [[TAJGameEngine sharedGameEngine] getTableDetailsAndJoinTableAsPlayTournament:dictionary];
}

#pragma mark - Deposit method
- (void)closePopUp:(NSNotification *) notification
{
    //    [self performSelector:@selector(navigateToDeposit) withObject:self afterDelay:2.0 ];
    NSLog(@"DEPOSIT CHECK MAIN");
    NSString *uniqueID = [[NSUserDefaults standardUserDefaults] objectForKey:UNIQUE_SESSION_KEY];
    NSString * url = [NSString stringWithFormat:@"%@%@?client_type=ios&device_type=%@&unique_id=%@",BASE_URL,SENDPAYMENTREQUEST,[[TAJUtilities sharedUtilities] getDeviceType],uniqueID];
    
    [self openURLWithString:url];
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}

#pragma mark - initialization -

- (void)preInitialization
{
    self.isGamePlayScreen = NO;
}

// No of tables notification
- (void)noOfTajTableCount:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary)
    {
        if (self.mainViewController)
        {
            [self.mainViewController updateLiveFeedTajTables:dictionary[TOTAL_TABLE_FEED_KEY]];
        }
        if (self.lobbyTableController)
        {
            [self.lobbyTableController showNoOfTableLive:dictionary[TOTAL_TABLE_FEED_KEY]];
        }
    }
}

// No of player count notification
- (void)noOfTajPlayerCount:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    if (dictionary)
    {
        if (self.mainViewController)
        {
            [self.mainViewController updateLiveFeedTajPlayer:dictionary[TOTAL_PLAYER_FEED_KEY]];
        }
        if (self.lobbyTableController)
        {
            [self.lobbyTableController showNoOfPlayerLive:dictionary[TOTAL_PLAYER_FEED_KEY]];
        }
    }
}

//shows reconnecting pop up when disconnected
- (void)showProgressAlert:(NSNotification *)notification
{
    if (self.webPageViewController)
    {
        [self removeWebPageViewController];
    }
    
    [[[TAJGameEngine sharedGameEngine] activityAlertForReconnecting] show:true];
    
}

//show dual login alert when other login event is fired
- (void)showOtherLoginAlert:(NSNotification *)notification
{
    if (!self.otherLoginAlertView)
    {
        self.otherLoginAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:1 popupButtonType:eNone_Type message:OTHER_LOGIN_TEXT];
        
        [self.otherLoginAlertView show];
    };
    
    [self performSelector:@selector(dismissSwitchLobby) withObject:nil afterDelay:2.0];
}

- (void)dismissSwitchLobby
{
    [self.switchLobbyController dismissViewControllerAnimated:NO completion:nil];
    
    if (self.lobbyTableController)
    {
        [self.lobbyTableController.view removeFromSuperview];
    }
    [self.switchLobbyController stopConnectedTimer];
    self.lobbyTableController = nil;
    self.switchLobbyController = nil;
    
}

//show engine under maintenance pop up
- (void)showEngineUnderMaintenanceAlert:(NSNotification *)notification
{
    if (!self.engineUnderMaintenanceAlertView)
    {
        self.engineUnderMaintenanceAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_SERVER_UNDER_MAINTAINANCE_TAG popupButtonType:eOK_Type message:locate(eENGINE_UNDER_MAINTENANCE)];
        
        [self.engineUnderMaintenanceAlertView show];
    }
}

- (void)removeEngineUnderMaintenanceAlert:(NSNotification *)notification
{
    if (self.engineUnderMaintenanceAlertView)
    {
        [self.engineUnderMaintenanceAlertView hide];
        self.engineUnderMaintenanceAlertView = nil;
    }
}

- (void)serverErrorEvent:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    [self processServerErrorEventInPreLobby:[dictionary[kTaj_Error_code] integerValue]];
}

#pragma mark - HELPERS

-(void)setDefaults {
    self.viewHomeTab.backgroundColor = [UIColor clearColor];
    self.viewLobbyTab.backgroundColor = [UIColor clearColor];
    self.viewTablesTab.backgroundColor = [UIColor clearColor];
    self.viewSupportTab.backgroundColor = [UIColor clearColor];
    self.viewMoreTab.backgroundColor = [UIColor clearColor];
}

-(void)setHomeTab {
    [self setDefaults];
    self.viewHomeTab.backgroundColor = [UIColor blackColor];
}

-(void)setLobbyTab {
    [self setDefaults];
    self.viewLobbyTab.backgroundColor = [UIColor blackColor];
}

-(void)setTablesTab {
    [self setDefaults];
    self.viewTablesTab.backgroundColor = [UIColor blackColor];
}

-(void)setSupportTab {
    [self setDefaults];
    self.viewSupportTab.backgroundColor = [UIColor blackColor];
}

-(void)setMoreTab {
    [self setDefaults];
    self.viewMoreTab.backgroundColor = [UIColor blackColor];
}

#pragma mark - IBActions
- (IBAction)homeButtonClicked:(id)sender
{
    [self changeBackgroundImageOfButtons:nil];
    //added
    [self.supportButtonController.view removeFromSuperview];
    
    if ([self.moreButtonController isMoreButtonViewVisible])
    {
        [self changeBackgroundImageOfButtons:nil];
        [self.moreButtonController.view removeFromSuperview];
        [self.supportButtonController.view removeFromSuperview];
    }
    else
    {
        if (self.currentView == self.lobbyTableController.view)
        {
            
            [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
            [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
            
            [self addMainScreenWindow];
            
            [self setHomeTab];//by ratheesh
        }
    }
}

- (IBAction)lobbyButtonClicked:(id)sender
{
    if ([self.moreButtonController isMoreButtonViewVisible])
    {
        [self changeBackgroundImageOfButtons:nil];
        [self.moreButtonController.view removeFromSuperview];
        [self.supportButtonController.view removeFromSuperview];
    }
    else
    {
        [self changeBackgroundImageOfButtons:self.lobbyButton];
        [self addLobbyScreenWindow];
        [[TAJGameEngine sharedGameEngine] requestListTable];
        [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
    }
}

- (IBAction)cashierButtonClicked:(id)sender
{
    [self addCashierPage];
}

- (IBAction)tablesButtonClicked:(id)sender
{
    [self changeBackgroundImageOfButtons:nil];
    
    if ([self.moreButtonController isMoreButtonViewVisible])
    {
        [self changeBackgroundImageOfButtons:nil];
        [self.moreButtonController.view removeFromSuperview];
        [self.supportButtonController.view removeFromSuperview];
    }
    else
    {
        if (self.switchLobbyController  && [self.switchLobbyController isPlayerJoinedAnyTable])
            
        {
            [self enableLobbyButtonPressed];
            
#if LANDSCAPE_MODE_GAME
            
            if (!self.isGamePlayScreen)
            {
                [self isGamePlayWindow:YES];
            }
#else
            [self removeSubviews];
            
            
            [self.gamePlayHolderView addSubview:self.switchLobbyController.view];
            self.currentView = self.switchLobbyController.view;
            [self isGamePlayWindow:YES];
#endif
        }
    }
}

- (IBAction)moreButtonClicked:(id)sender
{
    [self changeBackgroundImageOfButtons:self.moreButton];
    [self addMoreButtonView];
}

- (IBAction)supportButtonClicked:(id)sender
{
    [self changeBackgroundImageOfButtons:self.supportButton];
    [self addSupportButtonView];
}

#pragma mark - Main Screen controller

- (void)createMainScreenAndLoad
{
    if (!self.mainViewController)
    {
        self.mainViewController = [[TAJMainScreenViewController alloc] initWithNibName:@"TAJMainScreenViewController" bundle:Nil];
        
        self.mainViewController.mainScreenDelegate = self;
    }
    [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
    [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
    [self setHomeTab];//by ratheesh
    
    [self removeSubviews];
    if (self.lobbyTableController)
    {
        [self.lobbyTableController.view removeFromSuperview];
    }
    else if(self.moreButtonController)
    {
        [self.moreButtonController.view removeFromSuperview];
        
    }
    
    //added
    else if (self.supportButtonController) {
        [self.supportButtonController.view removeFromSuperview];
    }
    
    [TAJAppDelegate setScreenSize : self.mainViewController.view];//ReDIM Changes
    
    [self.containerView addSubview:self.mainViewController.view];
    self.currentView = self.mainViewController.view;
}

- (void)addMainScreenWindow
{
    if (self.currentView != self.mainViewController.view)
    {
        [self createMainScreenAndLoad];
    }
    else
    {
        [self.containerView addSubview:self.mainViewController.view];
    }
    
    [self isGamePlayWindow:NO];
}

#pragma mark - Table lobby controller -

- (void)createLobbyTableAndLoad
{
    [self createLobbyTableController];
    
    [self removeSubviews];
    
    if (self.mainViewController)
    {
        [self.mainViewController.view removeFromSuperview];
    }
    else if(self.moreButtonController)
    {
        [self.moreButtonController.view removeFromSuperview];
        
    }
    //added
    else if (self.supportButtonController) {
        [self.supportButtonController.view removeFromSuperview];
    }
    [TAJAppDelegate setScreenSize : self.lobbyTableController.view];//ReDIM Changes
    [self.containerView addSubview:self.lobbyTableController.view];
    
    CGRect xframe = self.containerView.frame;
    CGRect yframe = self.lobbyTableController.view.frame;
    yframe.origin.x += xframe.size.height;
    self.lobbyTableController.view.frame = yframe;
    [UIView animateWithDuration:0.8
                          delay:0.0
                        options: UIViewAnimationOptionCurveEaseIn
                     animations:
     ^{
         CGRect finalRect;
         finalRect  = yframe;
         finalRect.origin = CGPointZero;
         //finalRect.origin.y = 0;
         self.lobbyTableController.view.frame = finalRect;
         
     }
                     completion:nil
     ];
    [self.lobbyTableController reloadLobbyScreen];
    self.currentView = self.lobbyTableController.view;
}

- (void)createLobbyTableController
{
    if (!self.lobbyTableController)
    {
        self.lobbyTableController = [[TAJLobbyViewController alloc]init];
        self.lobbyTableController.lobbyDelegate = self;
    }
}

- (void)addCashierPage
{
    [self changeBackgroundImageOfButtons:nil];
    
    if ([self.moreButtonController isMoreButtonViewVisible])
    {
        [self.moreButtonController.view removeFromSuperview];
    }
    
    if (!self.webPageViewController)
    {
        self.webPageViewController = [[TAJWebPageViewController alloc]  initWithNibName:@"TAJWebPageViewController" bundle:nil webPageOption:ePurchaseOption];
        self.webPageViewController.webPageDelegate = self;
        self.webPageViewController.view.frame = self.view.frame;
        [self.view addSubview:self.webPageViewController.view];
    }
    
    else if (self.webPageViewController)
    {
        self.webPageViewController.view.frame = self.view.frame;
        [self.view addSubview:self.webPageViewController.view];
        [self.webPageViewController reloadWebPage];
    }
    [self removeSubviews];
}

- (void)addLobbyScreenWindow
{
    if (self.webPageViewController)
    {
        [self.webPageViewController.view removeFromSuperview];
    }
    if (self.currentView != self.lobbyTableController.view)
    {
        self.lobbyTableController.isTournament = NO;
        [self createLobbyTableAndLoad];
#if LIVE_RELEASE
        [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY],nil]];
#else
        [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY],nil]];
        //[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]
        
#endif
        
        [self enableLobbyButtonPressed];
    }
    else
    {
        [self.containerView addSubview:self.lobbyTableController.view];
        [self enableLobbyButtonPressed];
    }
    
    [self isGamePlayWindow:NO];
}

#pragma mark - More Button View -

- (void)addMoreButtonView
{
    self.isGamePlayScreen = NO;
    //    if ([self.moreButtonController isMoreButtonViewVisible])
    //    {
    //        NSLog(@"MORE UNPRESSED");
    //        [self changeBackgroundImageOfButtons:nil];
    //        if(self.currentView!=self.mainViewController.view)
    //        {
    //            [self enableLobbyButtonPressed];
    //        }
    //        [self.moreButtonController.view removeFromSuperview];
    //    }
    //    else
    //    {
    NSLog(@"MORE PRESSED");
    [self.containerView addSubview:self.moreButtonController.view];
    [self.view bringSubviewToFront:self.containerView];
    //}
}

- (void)addSupportButtonView
{
    self.isGamePlayScreen = NO;
    
    //    if ([self.moreButtonController isMoreButtonViewVisible])
    //    {
    //        [self changeBackgroundImageOfButtons:nil];
    //        if(self.currentView!=self.mainViewController.view)
    //        {
    //            [self enableLobbyButtonPressed];
    //        }
    //        [self.moreButtonController.view removeFromSuperview];
    //    }
    //    else
    //    {
    [self.containerView addSubview:self.supportButtonController.view];
    [self.view bringSubviewToFront:self.containerView];
    //}
}

- (void)createMoreButtonViewController
{
    if (!self.moreButtonController)
    {
        self.moreButtonController = [[TAJMoreButtonViewController alloc]initWithNibName:@"TAJMoreButtonViewController" bundle:nil];
        
        [TAJAppDelegate setScreenSize: self.moreButtonController.view];
        self.moreButtonController.moreButtonDelegate = self;
    }
}

- (void)createSupportButtonViewController
{
    if (!self.supportButtonController)
    {
        self.supportButtonController = [[TAJSupportButtonViewController alloc]initWithNibName:@"TAJSupportButtonViewController" bundle:nil];
        
        [TAJAppDelegate setScreenSize: self.supportButtonController.view];
        self.supportButtonController.supportButtonDelegate = self;
    }
}

-(void)didPressedLogOutPressed
{
    // log out button action
    //[self.moreButtonController.view removeFromSuperview];
    if (![self.switchLobbyController isPlayerJoinedAnyTable])
    {
        if (!self.logoutAlertView)
        {
            self.logoutAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:LOGOUT_TAG popupButtonType:eYes_No_Type message:@"Are you sure you want to logout?"];
            
            [self.logoutAlertView show];
            // [self changeBackgroundImageOfButtons:nil];
        }
        
        
    }
    else
    {
        if (!self.cannotLogoutAlertView)
        {
            self.cannotLogoutAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:CANNOT_LOGOUT_TAG popupButtonType:eOK_Type message:@"You cannot logout at this stage"];
            
            [self.cannotLogoutAlertView show];
            //[self changeBackgroundImageOfButtons:nil];
        }
        
        
    }
    
}

//logout a user from server
- (void)logoutUser
{
    self.isGamePlayScreen = NO;
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        [[TAJGameEngine sharedGameEngine] destroyUserNameAndPassword];
        [[TAJGameEngine sharedGameEngine] logout];
        [self.switchLobbyController destroyAllViewsOnLogout];
        TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
        [delegate loadMainWindow];
    }
}

- (void)removeMoreButtonControllerView
{
    [self.moreButtonController.view removeFromSuperview];
    self.isGamePlayScreen = NO;
    [self changeBackgroundImageOfButtons:nil];
    
}

- (void)removeSupportButtonControllerView
{
    [self.supportButtonController.view removeFromSuperview];
    self.isGamePlayScreen = NO;
    [self changeBackgroundImageOfButtons:nil];
    
}
#pragma mark - Swicth lobby controller -

//create a switch lobby controller where playing game tables are displayed
- (void)createSwitchLobbyScreenController
{
    if (!self.switchLobbyController)
    {
        self.switchLobbyController = [[TAJSwitchLobbyViewController alloc]init];
        self.switchLobbyController.switchDelegate = self;
    }
}

#pragma mark - Utility Functions -

- (void)removeSubviews
{
    if (self.currentView)
    {
        [self.currentView removeFromSuperview];
    }
    
    if (self.errorAlertView)
    {
        [self.errorAlertView hide];
        self.errorAlertView = nil;
    }
    if (self.cannotLogoutAlertView)
    {
        [self.cannotLogoutAlertView hide];
        self.cannotLogoutAlertView = nil;
        
    }
    if (self.logoutAlertView)
    {
        [self.logoutAlertView hide];
        self.logoutAlertView = nil;
    }
}

- (void)isGamePlayWindow:(BOOL)boolValue
{
#if LANDSCAPE_MODE_GAME
    if (boolValue)
    {
        [self showSwitchLobbyScreenView];
    }
    else
    {
        self.gamePlayHolderView.hidden = YES;
        self.gamePlayHolderView.userInteractionEnabled = NO;
        
        self.containerView.hidden = NO;
        self.containerView.userInteractionEnabled = YES;
        
        self.isGamePlayScreen = NO;
    }
#else
    if (boolValue)
    {
        [self showSwitchLobbyScreenView];
    }
    else
    {
        self.gamePlayHolderView.hidden = YES;
        self.gamePlayHolderView.userInteractionEnabled = NO;
        self.containerView.hidden = NO;
        self.containerView.userInteractionEnabled = YES;
        self.isGamePlayScreen = NO;
    }
#endif
}

- (void)showSwitchLobbyScreenView
{
#if LANDSCAPE_MODE_GAME
    if (!self.isGamePlayScreen)
    {
        NSLog(@"entering");
        self.isGamePlayScreen = YES;
        [self.switchLobbyController setModalPresentationStyle:UIModalPresentationFullScreen];
        [self presentViewController:self.switchLobbyController animated:YES completion:nil];
        [self.switchLobbyController loadCurrentTurnTimeEnableTable];
    }
#else
    NSLog(@"Not entering");
    self.containerView.hidden = YES;
    self.containerView.userInteractionEnabled = NO;
    self.gamePlayHolderView.hidden = NO;
    self.gamePlayHolderView.userInteractionEnabled = YES;
    self.isGamePlayScreen = YES;
#endif
}

//change the background images of top bar button on selection
- (void)changeBackgroundImageOfButtons:(UIButton *)selectedbutton
{
    
    if(selectedbutton==self.homeButton)
    {
        [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.casheirButtoon setImage:[UIImage imageNamed:CASHIER_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.supportButton setImage:[UIImage imageNamed:SUPPORT_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:MORE_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        
        [self setHomeTab];//by ratheesh
    }
    
    else if(selectedbutton==self.lobbyButton)
    {
        [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.casheirButtoon setImage:[UIImage imageNamed:CASHIER_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:MORE_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.supportButton setImage:[UIImage imageNamed:SUPPORT_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        
        [self setLobbyTab];//by ratheesh
        
    }
    else if(selectedbutton==self.moreButton)
    {
        [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.casheirButtoon setImage:[UIImage imageNamed:CASHIER_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:MORE_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.supportButton setImage:[UIImage imageNamed:SUPPORT_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        
        [self setMoreTab];//by ratheesh
    }
    else if(selectedbutton==self.supportButton)
    {
        [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.casheirButtoon setImage:[UIImage imageNamed:CASHIER_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.supportButton setImage:[UIImage imageNamed:SUPPORT_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:MORE_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        
        [self setSupportTab];//by ratheesh
    }
    
    else
    {
        if(self.currentView!=self.mainViewController.view)
        {
            [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
            [self setLobbyTab];//by ratheesh
        }
        else
        {
            [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
            [self setHomeTab];//by ratheesh
        }
        
        [self.casheirButtoon setImage:[UIImage imageNamed:CASHIER_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.moreButton setImage:[UIImage imageNamed:MORE_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
        [self.supportButton setImage:[UIImage imageNamed:SUPPORT_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
    }
    //    }
}

- (void) enableLobbyButtonPressed
{
    
    [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
    [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
    
    [self setLobbyTab];//by ratheesh
}

#pragma mark - Switch Lobby Delegate -

//present switch lobby screen if not already shown
- (void)isSwitchLobbyShowing
{
    if (self.isGamePlayScreen)
    {
        [self showSwitchLobbyScreenView];
    }
    else
    {
        if (self.switchLobbyController)
        {
            
#if LANDSCAPE_MODE_GAME
            
            self.isGamePlayScreen = YES;
            [self.switchLobbyController setModalPresentationStyle:UIModalPresentationFullScreen];
            [self presentViewController:self.switchLobbyController animated:NO completion:nil];
#else
            self.containerView.hidden = YES;
            self.containerView.userInteractionEnabled = NO;
            [self.gamePlayHolderView addSubview:self.switchLobbyController.view];
            self.currentView = self.switchLobbyController.view;
            self.gamePlayHolderView.hidden = NO;
            self.gamePlayHolderView.userInteractionEnabled = YES;
            self.isGamePlayScreen = YES;
#endif
        }
    }
}

- (void)showTableViewLobby
{
#if LANDSCAPE_MODE_GAME
    if (self.switchLobbyController)
    {
        [self.switchLobbyController dismissViewControllerAnimated:NO completion:nil];
        
        if (self.currentView != self.lobbyTableController.view)
        {
            [self createLobbyTableAndLoad];
            [self enableLobbyButtonPressed];
        }
        
        if([self.switchLobbyController isPlayerJoinedAnyTable])
        {
        }
        else
        {
        }
        [self.lobbyTableController reloadLobbyScreen];
        
        [self isGamePlayWindow:NO];
    }
#else
    if (self.currentView != self.lobbyTableController.view)
    {
        [self createLobbyTableAndLoad];
        [self enableLobbyButtonPressed];
    }
    [self isGamePlayWindow:NO];
#endif
}

//shows maximum table reached pop up
- (void)displayPlayerTwoTableReached
{
    if (!self.twoTableAlertView)
    {
        self.twoTableAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_MAX_TWO_TABLE_TAG popupButtonType:eOK_Type message:ALERT_MAX_TWO_TABLE_JOIN];
        
        [self.twoTableAlertView show];
    }
}

//show search join error alert when server returns no table when user selects search join table during ending of the the game.
- (void)showSearchJoinErrorAlert
{
    if (!self.twoTableAlertView)
    {
        self.twoTableAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_MAX_TWO_TABLE_TAG popupButtonType:eOK_Type message:ALERT_SERCH_JOIN_ERROR];
        
        [self.twoTableAlertView show];
    }
    
}

#pragma Process_Server_error_events
//processes error events
- (void)processServerErrorEventInPreLobby:(int)errorCode
{
    switch (errorCode)
    {
            
        case eINTERNAL_ERROR:
            //500
            [self showErrorAlert:locate(eINTERNAL_ERROR)];
            break;
            
        case eENGINE_UNDER_MAINTENANCE:
            //100 engine is under maintenance
            [self showEngineUnderMaintenanceAlert:nil];
            break;
            
            
        case eDATABASE_ERROR:
            //460
            [self showErrorAlert:locate(eDATABASE_ERROR)];
            break;
            
        case eCONNECTION_WAS_NOT_ESTABLISHED_TO_DBSLAYER:
            //463
            [self showErrorAlert:locate(eCONNECTION_WAS_NOT_ESTABLISHED_TO_DBSLAYER)];
            break;
            
            
        case ePLAYER_ALREADY_INPLAY:
            //504
            [self showErrorAlert:locate(ePLAYER_ALREADY_INPLAY)];
            break;
            
        case ePLAYER_ALREADY_INVIEW:
            //505
            [self showErrorAlert:locate(ePLAYER_ALREADY_INVIEW)];
            break;
        default:
            
            break;
    }
}

- (void)showErrorAlert:(NSString *)message
{
    if (!self.errorAlertView)
    {
        self.errorAlertView = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:self tag:ALERT_SERVER_ERROR_TAG popupButtonType:eOK_Type message:message];
        
        [self.errorAlertView show];
    }
    
}

#pragma mark - Table Lobby Delegate -
// returns whether a player can join table
- (BOOL)canJoinView
{
    BOOL canJoion = YES;
    if (self.switchLobbyController)
    {
        canJoion = [self.switchLobbyController isPlayerJoinedCompleteTwoTable];
    }
    return canJoion;
}

- (void)loadGameTable:(NSDictionary *)tableDictionary
{
    NSLog(@"LOBBY : %@",tableDictionary);
    if (self.switchLobbyController)
    {
        [self.switchLobbyController loadGameTableForDictionary:tableDictionary];
    }
}

- (BOOL)ifSingleTableJoined:(NSDictionary *)tableDictionary
{
    BOOL canJoionSingleTable = NO;
    if (self.switchLobbyController)
    {
        canJoionSingleTable = [self.switchLobbyController isSingleTableJoinedWithDictionary:tableDictionary];
    }
    return canJoionSingleTable;
}

- (void)canJoinSingleTable
{
    if (self.switchLobbyController)
    {
        [self.switchLobbyController loadSingleTable];
    }
}

- (void)lobbyTableBackButtonClicked
{
    [self changeBackgroundImageOfButtons:nil];
    
    [self.lobbyButton setImage:[UIImage imageNamed:LOBBY_BUTTON_NORMAL_IMAGE_IPAD] forState:UIControlStateNormal];
    [self.homeButton setImage:[UIImage imageNamed:HOME_BUTTON_PRESSED_IMAGE_IPAD] forState:UIControlStateNormal];
    
    [self setHomeTab];//by ratheesh
    
    [self removeSubviews];
    [self addMainScreenWindow];
}

#pragma mark - Main Screen Delegate -

-(void)depositClicked {
    NSLog(@"DEPOSIT DELEGATE");
    DepositViewController * viewController = [[DepositViewController alloc] init];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (void)strikesClickedOrJokerClicked
{
    self.lobbyTableController.isTournament = NO;
    [self createLobbyTableAndLoad];
    [self enableLobbyButtonPressed];
     [self.lobbyTableController gameVariantTypeSelection:STRIKES_LABEL_LOBBY withGameType:self.mainViewController.gameType];
#if LIVE_RELEASE
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:STRIKES_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:GAME_JOKER_LOBBY],[NSString stringWithFormat:GAME_NO_JOKER_LOBBY],nil]];
    [[TAJGameEngine sharedGameEngine] requestListTable];
#else
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:STRIKES_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:GAME_JOKER_LOBBY],[NSString stringWithFormat:GAME_NO_JOKER_LOBBY],nil]];
    [[TAJGameEngine sharedGameEngine] requestListTable];
    
#endif
    [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
   
}

- (void)poolRummyClicked
{
    self.lobbyTableController.isTournament = NO;
    [self createLobbyTableAndLoad];
    [self enableLobbyButtonPressed];
    [self.lobbyTableController gameVariantTypeSelection:POOLS_LABEL_LOBBY withGameType:self.mainViewController.gameType];
#if LIVE_RELEASE
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY],nil]];
    //[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]
#else
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY],nil]];
    //[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]
    
#endif
    [[TAJGameEngine sharedGameEngine] requestListTable];
    [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
    
}

- (void)dealsRummyClicked
{
    self.lobbyTableController.isTournament = NO;
    [self createLobbyTableAndLoad];
    [self enableLobbyButtonPressed];
#if LIVE_RELEASE
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:DEALS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY],nil]];
#else
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:DEALS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY],nil]];
#endif
    
    [[TAJGameEngine sharedGameEngine] requestListTable];
    [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
    [self.lobbyTableController gameVariantTypeSelection:DEALS_LABEL_LOBBY withGameType:self.mainViewController.gameType];
}

- (void)favouritesClicked
{
    /*
     self.lobbyTableController.isTournament = NO;
     [self createLobbyTableAndLoad];
     [self enableLobbyButtonPressed];
     #if LIVE_RELEASE
     [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY],nil]];
     //[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]
     #else
     [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:POOLS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:ONE_NOT_ONE_LABEL_LOBBY],[NSString stringWithFormat:TWO_NOT_ONE_LABEL_LOBBY],nil]];
     //[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY]
     
     #endif
     [[TAJGameEngine sharedGameEngine] requestListTable];
     [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
     */
    [self sampleFav];
}

-(void)sampleFav
{
    NSLog(@"IS FAVOURITE CLICKED");
    self.lobbyTableController.isTournament = NO;
    [self createLobbyTableAndLoad];
    [self enableLobbyButtonPressed];
#if LIVE_RELEASE
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:FAVOURITES_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY], nil]];
#else
    //[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY],nil]
    
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:FAVOURITES_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObject:[NSString stringWithFormat:@"%@", self.mainViewController.gameType]] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_THREE_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY],nil]];
#endif
    [[TAJGameEngine sharedGameEngine] requestListTable];
    [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
}

- (void)tournamentsRummyClicked
{
    self.lobbyTableController.isTournament = YES;
    [self createLobbyTableAndLoad];
    [self enableLobbyButtonPressed];
    [self.lobbyTableController gameVariantTypeSelection:TOURNAMENTS_LABEL_LOBBY withGameType:self.mainViewController.gameType];
#if LIVE_RELEASE
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:TOURNAMENTS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%@", self.mainViewController.gameType],nil] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY],nil]];
#else
    [self.lobbyTableController gameVariantTypeSelection:self.lobbyTableController.variantButton withSelection:[NSMutableArray arrayWithObject:[NSString stringWithFormat:TOURNAMENTS_LABEL_LOBBY]] chipsButton:self.lobbyTableController.chipsButton withChips:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:@"%@", self.mainViewController.gameType],nil] gameButton:self.lobbyTableController.gameTypeButton withGameType:[NSMutableArray arrayWithObjects:[NSString stringWithFormat:BEST_OF_TWO_LABEL_LOBBY],[NSString stringWithFormat:BEST_OF_SIX_LABEL_LOBBY],nil]];
#endif
    [[TAJGameEngine sharedGameEngine] requestListTable];
    [self.lobbyTableController.tableViewController startAnimatingActivityIndicator];
    
}

- (void)buyChipsClicked
{
    [self addCashierPage];
}


#pragma mark - Web Page Delegate -

- (void)removeWebPageViewController
{
    if (self.webPageViewController)
    {
        [self.webPageViewController.view removeFromSuperview];
    }
    if (self.currentView == self.mainViewController.view)
    {
        [self addMainScreenWindow];
    }
    else if (self.currentView == self.lobbyTableController.view)
    {
        [self addLobbyScreenWindow];
    }
}

#pragma mark - info pop up delegate -

- (void)okButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_MAX_TWO_TABLE_TAG:
        {
            [self.twoTableAlertView hide];
            self.twoTableAlertView = nil;
            
        }
            break;
            
        case ALERT_SERVER_UNDER_MAINTAINANCE_TAG: {
            [self.engineUnderMaintenanceAlertView hide];
            self.engineUnderMaintenanceAlertView = nil;
        }
            break;
            
        case ALERT_SERVER_ERROR_TAG:
        {
            [self.errorAlertView hide];
            self.errorAlertView = nil;
            
        }
            break;
        case CANNOT_LOGOUT_TAG:
        {
            [self.cannotLogoutAlertView hide];
            self.cannotLogoutAlertView = nil;
            
        }
            break;
        default:
            break;
    }
}

- (void)yesButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case LOGOUT_TAG:
        {
            [self.logoutAlertView hide];
            self.logoutAlertView = nil;
            [self logoutUser];
        }
            break;
        default:
            break;
    }
}

- (void)noButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case LOGOUT_TAG:
        {
            [self.logoutAlertView hide];
            self.logoutAlertView = nil;
        }
            break;
        default:
            break;
    }
}

- (void)closeButtonPressedForInfoPopup:(id)object
{
    TAJInfoPopupViewController *conroller = object;
    switch (conroller.tag)
    {
        case ALERT_MAX_TWO_TABLE_TAG:
        {
            [self.twoTableAlertView hide];
            self.twoTableAlertView = nil;
            
        }
            break;
        case ALERT_SERVER_ERROR_TAG:
        {
            [self.errorAlertView hide];
            self.errorAlertView = nil;
            
        }
            break;
        case CANNOT_LOGOUT_TAG:
        {
            [self.cannotLogoutAlertView hide];
            self.cannotLogoutAlertView = nil;
            
        }
            break;
        case LOGOUT_TAG:
        {
            [self.logoutAlertView hide];
            self.logoutAlertView = nil;
            
        }
            break;
        default:
            break;
    }
}

- (void)showAlertIndicatorForTableWhenItisMyTurn
{
    self.tableAlertImageView.hidden = NO;
}


- (void)hideTableAlertImageView
{
    self.tableAlertImageView.hidden = YES;
}


- (void)disableTablesButton
{
    [self.tabelsButton setBackgroundImage:[UIImage imageNamed:@"TR_tabbar_table.png"] forState:UIControlStateNormal];
    [self hideTableAlertImageView];
}

- (void)enableTablesButton
{
    [self.tabelsButton setBackgroundImage:[UIImage imageNamed:@"TR_game_table_on"] forState:UIControlStateNormal];
    self.tabelsButton.enabled = YES;
}

#pragma Registration Success
-(void)loadRegistrationSuccessMessageInfoPopUP
{
    if(!self.registrationPopup)
    {
        self.registrationPopup = [[TAJRegistrationSuccessPopupViewController alloc] initWithNibName:@"TAJRegistrationSuccessPopupViewController" bundle:nil];
        
    }
    self.registrationPopup.regSuccessPopupDelegate = self;
    //self.registrationPopup.view.frame = self.view.bounds;
    [TAJAppDelegate setScreenSizeForRes: self.registrationPopup.view];
    [self.view addSubview:self.registrationPopup.view];
}


-(void)closeButtonPressedForRegistrationSuccessInfoPopup
{
    if(self.registrationPopup)
    {
        [self.registrationPopup.view removeFromSuperview];
        self.registrationPopup = nil;
    }
}

-(void)closeButtonPressedForChatPopup {
    if(self.registrationPopup)
    {
        [self.registrationPopup.view removeFromSuperview];
        self.registrationPopup = nil;
        
        NSString *name = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
        NSString *email = [[NSUserDefaults standardUserDefaults] objectForKey:USEREMAIL_KEY];
        NSString *mobile = [[NSUserDefaults standardUserDefaults] objectForKey:USERMOBILE_KEY];
        [ZDCChat updateVisitor:^(ZDCVisitorInfo *user) {
            user.phone = mobile;
            user.name = name;
            user.email = email;
        }];
        
        [ZDCChat startChatIn:self.navigationController withConfig:^(ZDCConfig *config) {
            config.preChatDataRequirements.name = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.email = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.phone = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.department = ZDCPreChatDataNotRequired;
            config.preChatDataRequirements.message = ZDCPreChatDataNotRequired;
            config.emailTranscriptAction = ZDCEmailTranscriptActionNeverSend;
        }];
    }

}

@end
