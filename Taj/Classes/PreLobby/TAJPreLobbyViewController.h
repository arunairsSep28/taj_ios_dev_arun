/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPreLobbyViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by  Yogisha Poojary on 20/08/14.
 Modified by  Lloyd Praveen Mascarenhas on 11/06/14.
 Modified by  Pradeep BM on 15/05/14.
 Created by  Raghavendra Kamat on 28/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJRegistrationSuccessPopupViewController.h"
#import "TAJSupportButtonViewController.h"

@class TAJMainScreenViewController;
@class TAJSwitchLobbyViewController;
@class TAJLobbyViewController;
@class TAJMoreButtonViewController;
@class TAJSupportButtonViewController;
@class TAJWebPageViewController;
@class TAJCustomNavigationViewController;
@class TAJNavigationBar;

@interface TAJPreLobbyViewController : UIViewController<TAJRegistrationSuccessPopupViewControllerDelegate>
{
    
}

@property (nonatomic, strong) TAJMainScreenViewController *mainViewController;
@property (nonatomic, strong) TAJSwitchLobbyViewController  *switchLobbyController;
@property (nonatomic, strong) TAJLobbyViewController  *lobbyTableController;
@property (strong, nonatomic) TAJMoreButtonViewController  *moreButtonController;
@property (strong, nonatomic) TAJSupportButtonViewController  *supportButtonController;
@property (nonatomic, strong) TAJWebPageViewController  *webPageViewController;
@property (weak, nonatomic) IBOutlet UIView *containerView;

@property (strong, nonatomic) UINavigationController *authNavigationController;


- (IBAction)homeButtonClicked:(id)sender;
- (IBAction)lobbyButtonClicked:(id)sender;
- (IBAction)cashierButtonClicked:(id)sender;
- (IBAction)tablesButtonClicked:(id)sender;
- (IBAction)supportButtonClicked:(id)sender;
- (IBAction)moreButtonClicked:(id)sender;
- (void)loadRegistrationSuccessMessageInfoPopUP;

@end
