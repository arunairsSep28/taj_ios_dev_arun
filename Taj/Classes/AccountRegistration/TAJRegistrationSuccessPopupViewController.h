/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRegistrationSuccessPopupViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by  Yogisha Poojary on 20/08/14.
 
 **/

#import <UIKit/UIKit.h>

@protocol TAJRegistrationSuccessPopupViewControllerDelegate <NSObject>
-(void)closeButtonPressedForRegistrationSuccessInfoPopup;
-(void)closeButtonPressedForChatPopup;
@end

@interface TAJRegistrationSuccessPopupViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic, weak) id<TAJRegistrationSuccessPopupViewControllerDelegate> regSuccessPopupDelegate;
@end
