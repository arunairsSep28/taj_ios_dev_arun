/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAccountRegistrationViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 24/04/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJInfoPopupViewController.h"
#import "TAJUITextField.h"
#include "TAJStateSelectionView.h"
#include "TAJStateCell.h"
#include "NSMutableString+EncodingExtensions.h"

@class TAJInfoPopupViewController;
@interface TAJAccountRegistrationViewController : UIViewController<NSURLConnectionDelegate,NSURLConnectionDataDelegate,UITextFieldDelegate>

@property (strong, nonatomic) IBOutlet TAJUITextField *userNameField;
@property (strong, nonatomic) IBOutlet TAJUITextField *passwordField;
@property (strong, nonatomic) IBOutlet TAJUITextField *emailField;

@property (strong, nonatomic) IBOutlet UIButton *createButton;
@property (strong, nonatomic) IBOutlet UIButton *alreadyHaveAccountButton;
@property (strong, nonatomic) IBOutlet UIButton *backButton;

- (IBAction)backButtonPressed:(id)sender;
- (IBAction)registerAccount:(id)sender;
- (IBAction)alreadyHaveAccount:(id)sender;


@end
