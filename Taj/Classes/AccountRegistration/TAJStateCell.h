/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJStateCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 28/05/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJStateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *stateLabel;

@end
