/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJStateSelectionView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 28/05/14.
 **/

#import "TAJStateSelectionView.h"
#import "TAJUtilities.h"

@implementation TAJStateSelectionView

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        // Initialization code
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        self.stateNames = [NSMutableArray arrayWithObjects: @"Arunachal Pradesh", @"Assam",@"Bihar", @"Chhattisgarh", @"Goa", @"Gujarat",@"Haryana", @"Himachal Pradesh",@"Jammu and Kashmir", @"Jharkhand", @"Karnataka",@"Kerala", @"Madhya Pradesh", @"Maharashtra", @"Manipur", @"Meghalaya",@"Mizoram", @"Nagaland",@"Odisha", @"Punjab", @"Rajasthan",@"Rayalaseema",@"Sikkim", @"Tamil Nadu",@"Telangana", @"Tripura", @"Uttarakhand", @"Uttar Pradesh",@"West Bengal",nil];
        self.stateSelectionPickerView.dataSource = self;
        self.stateSelectionPickerView.delegate = self;
    }
    return self;
}

- (void)initializeThePickerViewWithCurrentSelectionofState
{
    [self.stateSelectionPickerView selectRow:[[[NSUserDefaults standardUserDefaults] objectForKey:@"picker"] intValue] inComponent:0 animated:NO];
}

#pragma mark PickerView DataSource

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.stateNames count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [self.stateNames objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([TAJUtilities isIPhone])
    {
        return 40;
    }
    else
    {
        return 70;
    }
}

#pragma mark - PickerView Delegate -

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row  inComponent:(NSInteger)component
{
    NSString *resultString = [[NSString alloc] initWithFormat:@"%@",[self.stateNames objectAtIndex:row]];
    NSInteger selectedRow = [self.stateSelectionPickerView selectedRowInComponent:component];
    NSString *key = [NSString stringWithFormat:@"%@", @"picker"];
    [[NSUserDefaults standardUserDefaults] setInteger:selectedRow forKey:key];
    
    if (self.stateDelegate && [self.stateDelegate respondsToSelector:@selector(updateSelectedState:)])
    {
        [self.stateDelegate updateSelectedState:resultString];
    }
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = (UILabel*) view;
    if (label == nil)
    {
        label = [[UILabel alloc] init];
    }
    
    label.text = [self.stateNames objectAtIndex:row];
    
    // This part just colorizes everything
    
    [label setTextColor:[UIColor whiteColor]];
    [label setBackgroundColor:[UIColor blackColor]];
    CGSize rowSize = [pickerView rowSizeForComponent:component];
    CGRect labelRect = CGRectMake (0, 0, self.stateSelectionPickerView.frame.size.width, rowSize.height);
    UIFont *myFont;
    if ([TAJUtilities isIPhone])
    {
        myFont = [ UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 15.0 ];
    }
    else
    {
        myFont = [ UIFont fontWithName: @"AvantGardeITCbyBT-Demi" size: 20.0 ];
    }
    label.font = myFont;
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
    {
        label.textAlignment = NSTextAlignmentCenter;
    }
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] < 6.0)
    {
        label.textAlignment = UITextAlignmentCenter;
    }
    
    [label setFrame:labelRect];
    
    return label;
}

@end
