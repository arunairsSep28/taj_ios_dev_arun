/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJStateSelectionView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 28/05/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJStateSelectionViewDelegate<NSObject>

- (void)updateSelectedState:(NSString *)state;

@end

@interface TAJStateSelectionView : UIView <UIPickerViewDelegate, UIPickerViewDataSource>
@property (strong, nonatomic) IBOutlet UIPickerView *stateSelectionPickerView;
@property (strong, nonatomic) NSMutableArray *stateNames;
@property (strong,nonatomic) id <TAJStateSelectionViewDelegate> stateDelegate;

- (void)initializeThePickerViewWithCurrentSelectionofState;

@end
