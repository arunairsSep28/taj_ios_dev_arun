/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJRegistrationSuccessPopupViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by  Yogisha Poojary on 20/08/14.
 
 **/

#import "TAJRegistrationSuccessPopupViewController.h"

@interface TAJRegistrationSuccessPopupViewController ()
@property (weak, nonatomic) IBOutlet UIView *viewOptionsContainer;
-(IBAction)closeThePopUp:(UIButton *)sender;

@end

@implementation TAJRegistrationSuccessPopupViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.mainView.layer.cornerRadius = 10;
    self.mainView.layer.masksToBounds = true;
    
   
    //Shawdow to View
    _viewOptionsContainer.layer.shadowColor = [UIColor colorWithWhite:0.0 alpha:0.5].CGColor;
    _viewOptionsContainer.layer.shadowOffset = CGSizeMake(0.0, 0.0);
    _viewOptionsContainer.layer.shadowOpacity = 1.0;
    _viewOptionsContainer.layer.shadowRadius = 6.0;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Notification observer -


- (IBAction)closeThePopUp:(UIButton *)sender
{
    [self.regSuccessPopupDelegate closeButtonPressedForRegistrationSuccessInfoPopup];
}

- (IBAction)depositClicked:(UIButton *)sender
{
//    DepositViewController * viewController = [[DepositViewController alloc] initWithRegistrationIsFromRegistration:YES];
//    [self presentViewController:viewController animated:NO completion:nil];
    NSString *uniqueID = [[NSUserDefaults standardUserDefaults] objectForKey:UNIQUE_SESSION_KEY];
    NSString * url = [NSString stringWithFormat:@"%@%@?client_type=ios&device_type=%@&unique_id=%@",BASE_URL,SENDPAYMENTREQUEST,[[TAJUtilities sharedUtilities] getDeviceType],uniqueID];
    
    [self openURLWithString:url];
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}

- (IBAction)contactUsClicked:(UIButton *)sender
{
    [self.regSuccessPopupDelegate closeButtonPressedForChatPopup];
}

@end
