/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJAccountRegistrationViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *      Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 24/04/14.
 **/

#import "TAJAccountRegistrationViewController.h"
#import "TAJAppDelegate.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJGameEngine.h"
#import "TAJClientServerCommunicator.h"
#import "TAJLobby.h"
#import "TAJWebPageViewController.h"

#define ACCEPTABLE_CHARECTERS @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_."
#define ACCEPTABLE_NUMBERS @"0123456789"

@interface TAJAccountRegistrationViewController ()<TAJStateSelectionViewDelegate,UITextFieldDelegate,UIPickerViewDataSource,UIPickerViewDelegate,InfoPopupViewDelegate,TAJWebPageViewControllerDelegate,BlockedPopupViewControllerDelegate>

@property (strong, nonatomic) TAJInfoPopupViewController *infoPopupViewController;
@property (weak, nonatomic) IBOutlet UIButton *selectStateButton;
@property (weak, nonatomic) IBOutlet UIImageView *popUpImageView;
@property (weak, nonatomic) IBOutlet UIImageView *datePopUp;

//@property (weak, nonatomic) IBOutlet UIView *stateSelectionViewHolder;
@property (weak, nonatomic) IBOutlet UIView *stateContainerView;
//@property(strong, nonatomic) TAJStateSelectionView *stateSelectionView;
@property (weak, nonatomic) IBOutlet UIButton *stateDoneButton;
//yogish
@property (weak, nonatomic) IBOutlet UIPickerView *statePickerView;
//@property (weak, nonatomic) IBOutlet UITableView *stateSelectionTableView;
@property (strong, nonatomic) NSMutableArray *stateNames;
@property ( nonatomic) NSInteger selectedRow;
@property ( nonatomic) int selectedROwForState;
@property (strong, nonatomic) TAJAppDelegate *appDelegate;

@property (nonatomic, strong) TAJStateCell *cell;
@property (nonatomic, strong) IBOutlet UITextField *dateTextField;
@property (weak, nonatomic) IBOutlet UILabel *dateofBirthLabel;
@property (weak, nonatomic) IBOutlet UILabel *stateLabel;
@property (weak, nonatomic) IBOutlet UILabel *genderLabel;
@property (weak, nonatomic) IBOutlet UIButton *maleRadioButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleRadioButton;
@property (strong, nonatomic) NSString *selectedState;
@property (strong, nonatomic) NSString *selectedGender;
@property (strong, nonatomic) NSString *selectedDate;
@property (strong, nonatomic) NSString *selectedMonth;
@property (strong, nonatomic) NSString *selectedYear;
@property (strong, nonatomic) NSString *selectedDOB;
@property (strong, nonatomic) NSArray *monthNames;
@property (strong, nonatomic) NSString *userName;
@property (strong, nonatomic) NSString *password;
@property (strong, nonatomic) NSString *uniqueID;


//@property (weak, nonatomic) IBOutlet UIView *dateOfBirthViewHolder;
@property (weak, nonatomic) IBOutlet UIScrollView *accountRegistrationScrollView;
@property (weak, nonatomic) IBOutlet UIView *stateSubContentView;

//@property (weak, nonatomic) IBOutlet UIView *genderHolderView;
@property BOOL isMaleGenderButtonShow;
@property BOOL isFemaleGenderButtonShow;
@property BOOL isDateButtonShow;
@property BOOL isMonthButtonShow;
@property BOOL isYearButtonShow;
- (IBAction)sateDoneButtonPressed:(id)sender;

@property (weak, nonatomic) IBOutlet UIButton *dateButton;
@property (weak, nonatomic) IBOutlet UIButton *monthButton;
@property (weak, nonatomic) IBOutlet UIButton *yearButton;
@property (weak, nonatomic) IBOutlet UIView *datepickerHolder;
@property (weak, nonatomic) IBOutlet UIDatePicker *datepicker;
@property (weak, nonatomic) IBOutlet UILabel *userNameLable;
@property (weak, nonatomic) IBOutlet UILabel *passwordLabel;
@property (weak, nonatomic) IBOutlet UILabel *emailLabel;
@property (weak, nonatomic) IBOutlet UILabel *mobileLabel;

//ErrorImageViews
@property (weak, nonatomic) IBOutlet UIImageView *usernameErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *passwordErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *emailErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mobileErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *dateofBirthErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *stateErrorImageView;
@property (weak, nonatomic) IBOutlet UIImageView *genderErrorImageView;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;
@property (nonatomic, strong) TAJWebPageViewController  *webPageViewController;

- (IBAction)doneButtonPressed:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *doneButton;

- (IBAction)maleGenderRadioButtonPressed:(id)sender;
- (IBAction)femaleGenderRadioButtonPressed:(id)sender;
- (IBAction)termsAndConditions:(UIButton *)sender;

//- (IBAction)DatePickerSelection:(id)sender;
@property (weak, nonatomic) IBOutlet TAJUITextField *mobileTextField;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *spinner;
@property (strong, nonatomic) UIButton* termsButton;
@property (strong, nonatomic) UIButton* privacyButton;
@property (strong, nonatomic) NSDate *currentDate;
@property CGRect textfieldFrame;
@property (weak, nonatomic) IBOutlet UIButton *leftArrowButton;
@property (weak, nonatomic) IBOutlet UIButton *rightArrowButton;
- (IBAction)leftArrowButtonPressed:(UIButton *)sender;
- (IBAction)rightArrowButtonPressed:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIButton *PreviousButtonForDate;
@property (weak, nonatomic) IBOutlet UIButton *nextButtonForDate;
@property (nonatomic)  int                          scrollPosition;
- (IBAction)previousDate:(UIButton *)sender;
- (IBAction)nextDate:(UIButton *)sender;

@property (strong, nonatomic) NSPredicate *emailPredicate;
@property (strong, nonatomic) NSPredicate  *phonePredicate;

@property (nonatomic, strong) BlockedPopupViewController  *blockedPopup;


@property (strong, nonatomic) NSMutableArray * statesList;
@property (strong, nonatomic) NSString * strState;

typedef void(^addressCompletion)(NSString *);

@property (nonatomic, assign) BOOL show_lobby;

@property BOOL isPasswordToggle;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewPasswordHide;

@end

@implementation TAJAccountRegistrationViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Regsitration screen"];
    
    self.userNameField.delegate = self;
    self.passwordField.delegate = self;
    self.emailField.delegate = self;
    self.mobileTextField.delegate = self;
    [self.navigationController setNavigationBarHidden:YES animated:NO];
    self.isMaleGenderButtonShow = YES;
    self.isFemaleGenderButtonShow = YES;
    self.isDateButtonShow = YES;
    self.isMonthButtonShow = YES;
    self.isYearButtonShow = YES;
    
    self.spinner.hidden = YES;
    self.currentDate = [NSDate date];
    self.textfieldFrame = self.mobileTextField.frame;
    self.versionLabel.text = VERSION_NUMBER;
    self.stateNames = [NSMutableArray arrayWithObjects:@"Andhra Pradesh", @"Arunachal Pradesh", @"Assam",@"Bihar", @"Chhattisgarh", @"Goa", @"Gujarat",@"Haryana", @"Himachal Pradesh",@"Jammu and Kashmir", @"Jharkhand", @"Karnataka",@"Kerala", @"Madhya Pradesh", @"Maharashtra", @"Manipur", @"Meghalaya",@"Mizoram", @"Nagaland",@"Odisha", @"Punjab", @"Rajasthan",@"Sikkim", @"Tamil Nadu",@"Telangana", @"Tripura", @"Uttarakhand", @"Uttar Pradesh",@"West Bengal",nil];
    self.monthNames = [NSArray arrayWithObjects:@"Jan",@"Feb",@"Mar",@"Apr",@"May",@"Jun",@"Jul",@"Aug",@"Sep",@"Oct",@"Nov",@"Dec", nil];
    self.rightArrowButton.enabled = YES;
    self.leftArrowButton.enabled = NO;
    self.PreviousButtonForDate.enabled = YES;
    self.nextButtonForDate.enabled = NO;
    
    self.emailPredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexEmail];
    self.phonePredicate = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", RegexPhoneNumber];
    
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:YES];
    self.backButton.exclusiveTouch = YES;
    self.createButton.exclusiveTouch = YES;
    self.alreadyHaveAccountButton.exclusiveTouch = YES;
    [self addNotificationListener];
    [self getBlockedList];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:YES];
    [self removeNotificationListener];
}

- (void) setTermsText {
    UIColor* textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];;
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:@"Terms of service"];
    [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
    [self.termsButton setAttributedTitle:commentString forState:UIControlStateNormal];
}

- (void) setPrivacyText {
    UIColor* textColor = [UIColor colorWithRed:170.0/255.0 green:170.0/255.0 blue:170.0/255.0 alpha:1];;
    NSMutableAttributedString *commentString = [[NSMutableAttributedString alloc] initWithString:@"Privacy Policy"];
    [commentString setAttributes:@{NSForegroundColorAttributeName:textColor,NSUnderlineStyleAttributeName:[NSNumber numberWithInteger:NSUnderlineStyleSingle]} range:NSMakeRange(0,[commentString length])];
    [self.privacyButton setAttributedTitle:commentString forState:UIControlStateNormal];
}

- (void)listSubviewsOfView:(UIView *)view
{
    // Get the subviews of the view
    NSArray *subviews = [view subviews];
    // Return if there are no subviews
    if ([subviews count] == 0) return; // COUNT CHECK LINE
    for (UIView *subview in subviews)
    {
        // Do what you want to do with the subview
        DLog(@"%@", subview);
        // List the subviews of subview
        [self listSubviewsOfView:subview];
    }
}


#pragma mark - HELPERS

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void)openURLWithString:(NSString *)string
{
    NSURL *URL = [NSURL URLWithString:string];
    UIApplication *application = [UIApplication sharedApplication];
    
    if ([[UIDevice currentDevice].systemVersion floatValue] >= 10.0) {
        
        if ([application respondsToSelector:@selector(openURL:options:completionHandler:)]) {
            if (@available(iOS 10.0, *)) {
                [application openURL:URL options:@{}
                   completionHandler:^(BOOL success) {
#if DEBUG
                       NSLog(@"Open %@: %d",string,success);
#endif
                   }];
            } else {
                // Fallback on earlier versions
            }
        }
        else {
            BOOL success = [application openURL:URL];
#if DEBUG
            NSLog(@"Open %@: %d",string,success);
#endif
        }
    }
    else {
        BOOL canOpen = [application canOpenURL:URL];
        
        if (canOpen) {
            [application openURL:[NSURL URLWithString:string]];
        }
    }
}

- (BOOL)validateInput
{
    
    if ([self.userNameField.text length] == 0) {
        [self.view makeToast:@"Please enter Username"];
        return YES;
    }
    else if ([self.userNameField.text length] < UserNameMinLength) {
        [self.view makeToast:@"Please enter 4 to 15 characters of username"];
        return YES;
    }
    
    else if ([self.passwordField.text length] == 0) {
        [self.view makeToast:@"Please enter password"];
        return YES;
    }
    else if ([self.passwordField.text length] < PasswordMinLength) {
        [self.view makeToast:@"Please enter 5 to 15 characters of password"];
        return YES;
    }
    else if ([self.emailField.text length] == 0) {
        [self.view makeToast:@"Please enter email"];
        return YES;
    }
    else if ((![self.emailPredicate evaluateWithObject:self.emailField.text]) && ([self.emailField.text length] > 0)) {
        [self.view makeToast:@"Please enter vaid email"];
        return YES;
    }
    else if ([self.mobileTextField.text length] > 0) {
         if (![self.phonePredicate evaluateWithObject:self.mobileTextField.text]) {
             [self.view makeToast:@"Mobilenumber inavalid"];
        }
    }
    
    return NO;
}

- (void)getBlockedList {
    //@"https://www.tajrummy.com/api/v1/get-blocked-states/"
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,BLOCKED_STATES_LIST];
    NSLog(@"STATES LIST : %@",URL);
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:URL]
                                                           cachePolicy:NSURLRequestUseProtocolCachePolicy
                                                       timeoutInterval:10.0];
    [request setHTTPMethod:@"GET"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request
                                                completionHandler:^(NSData *data, NSURLResponse *response, NSError *error) {
        if (error) {
            NSLog(@"%@", error);
        } else {
            NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
            //NSLog(@"%@", httpResponse);
            id jsonResponseString = [[NSString alloc] initWithData:data
                                                          encoding:NSASCIIStringEncoding];
            NSData *data = [jsonResponseString dataUsingEncoding:NSUTF8StringEncoding];
            id jsonResponse = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self.view hideToastActivity];
                NSLog(@"BlockedList Response :%@",jsonResponse);
                if ([[jsonResponse valueForKey:@"status"] isEqualToString:@"Success"]) {
                    
                    self.statesList = [[NSMutableArray alloc] init];
                    self.statesList = [jsonResponse valueForKey:@"data"][@"blocked_states_list"];
                    self.show_lobby = [[jsonResponse valueForKey:@"data"][@"show_lobby"] boolValue];
                    NSLog(@"BLOCKED STATES  : %lu",(unsigned long)self.statesList.count);
                }
            });
            
        }
    }];
    [dataTask resume];
}


#pragma mark - IBActions



- (IBAction)backButtonPressed:(id)sender
{
    [(TAJAppDelegate*)TAJ_APP_DELEGATE loadLoginScreenWindow];
}

- (IBAction)registerAccount:(id)sender
{
    [self hideKeyboard];
    [self registerAccountInformation];
}

- (IBAction)alreadyHaveAccount:(id)sender
{
    [(TAJAppDelegate*)TAJ_APP_DELEGATE loadLoginScreenWindow];
}

- (IBAction)togglePasswordClicked:(UIButton *)sender {
    
    if(self.isPasswordToggle == true) {
        self.passwordField.secureTextEntry = YES;
        self.imgViewPasswordHide.image = [UIImage imageNamed:@"eye_cross"];
        self.isPasswordToggle = NO;
    } else {
        self.passwordField.secureTextEntry = NO;
        self.imgViewPasswordHide.image = [UIImage imageNamed:@"eye"];
        self.isPasswordToggle = YES;
    }
}

#pragma mark - Touches Delegates -

- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
    UITouch *touch = [touches anyObject];
    if (!self.datepickerHolder.hidden)
    {
        self.datepickerHolder.hidden = YES;
        [self defaultDateSelect];
        
    }
    if (!self.stateContainerView.hidden)
    {
        self.stateContainerView.hidden = YES;
        if(self.selectedRow == 0)
        {
            [self defaultStateSelect];
        }
        
    }
    if(![touch.view isKindOfClass:[UIView class]])
    {
        self.stateContainerView.hidden = NO;
    }
}



# pragma HelperMethods



- (BOOL)validateEmailWithString:(NSString*)email
{
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:email];
}

- (IBAction)chooseTheStateFromPickerView:(UIButton *)sender
{
    [self.view endEditing:YES];
    self.datepickerHolder.hidden = YES;
    self.stateErrorImageView.hidden = YES;
    self.stateContainerView.hidden = NO;
    if(self.selectedRow >= 0 && self.selectedRow < self.stateNames.count)
    {
        [self initializeThePickerViewWithCurrentSelectedState];
    }
}

- (void)removeStateView
{
    self.stateContainerView.hidden = YES;
}

- (void)updateSelectedState:(NSString *)state
{
    [self.selectStateButton setTitle:state forState:UIControlStateNormal];
    [self.selectStateButton.titleLabel adjustsFontSizeToFitWidth];
    self.selectedState = state;
    if(self.selectedState.length == 0)
    {
        self.stateErrorImageView.hidden = NO;
    }
}

- (void) defaultDateSelect
{
    NSDate *now = [self.datepicker date];
    self.currentDate = now;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *year = [formatter stringFromDate:now];
    [self.yearButton setTitle:year forState:UIControlStateNormal];
    self.selectedYear = year;
    [formatter setDateFormat:@"MM"];
    NSString *month = [formatter stringFromDate:now];
    NSInteger mon = [month integerValue];
    NSInteger mo = mon - 1;
    NSString *currentMonth;
    if (mo < self.monthNames.count && mo >= 0)
    {
        currentMonth  = [self.monthNames objectAtIndex:mo];
        [self.monthButton setTitle:currentMonth forState:UIControlStateNormal];
    }
    self.selectedMonth = month;
    [formatter setDateFormat:@"dd"];
    NSString *day = [formatter stringFromDate:now];
    [self.dateButton setTitle:day forState:UIControlStateNormal];
    self.selectedDate = day;
    self.selectedDOB = [NSString stringWithFormat:@"%@/%@/%@",day,month,year];
    
    if(self.selectedDOB.length == 0)
    {
        self.stateErrorImageView.hidden = NO;
    }
    
}

- (IBAction)doneButtonPressed:(id)sender
{
    if(self.datepickerHolder.hidden == NO)
    {
        self.datepickerHolder.hidden = YES;
        [self defaultDateSelect];
    }
}

- (IBAction)maleGenderRadioButtonPressed:(UIButton *)sender
{
    [self.view endEditing:YES];
    self.datepickerHolder.hidden = YES;
    self.datePopUp.hidden = YES;
    
    self.stateContainerView.hidden = YES;
    if(self.isMaleGenderButtonShow)
    {
        self.usernameErrorImageView.hidden = YES;
        if([TAJUtilities isIPhone])
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active-568h~iphone.png"]forState:UIControlStateNormal];
            [self.femaleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal-568h~iphone.png"]forState:UIControlStateNormal];
        }
        else
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active.png"]forState:UIControlStateNormal];
            [self.femaleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal.png"]forState:UIControlStateNormal];
        }
        self.isMaleGenderButtonShow = NO;
        self.maleRadioButton.enabled = NO;
        self.femaleRadioButton.enabled = YES;
        self.isFemaleGenderButtonShow = YES;
        self.selectedGender = [NSString stringWithFormat:@"Male"];
        self.genderErrorImageView.hidden = YES;
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal-568h~iphone.png"]forState:UIControlStateNormal];
            [self.femaleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active-568h~iphone.png"]forState:UIControlStateNormal];
        }
        else
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal.png"]forState:UIControlStateNormal];
            [self.femaleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active.png"]forState:UIControlStateNormal];
        }
        self.isMaleGenderButtonShow = YES;
    }
    
}
- (IBAction)femaleGenderRadioButtonPressed:(UIButton *)sender
{
    [self.view endEditing:YES];
    self.datepickerHolder.hidden = YES;
    self.stateContainerView.hidden = YES;
    self.popUpImageView.hidden = YES;
    
    if(self.isFemaleGenderButtonShow)
    {
        self.genderErrorImageView.hidden = YES;
        
        if([TAJUtilities isIPhone])
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active-568h~iphone.png"]forState:UIControlStateNormal];
            [self.maleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal-568h~iphone.png"]forState:UIControlStateNormal];
        }
        else
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active.png"]forState:UIControlStateNormal];
            [self.maleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal.png"]forState:UIControlStateNormal];
        }
        self.isFemaleGenderButtonShow = NO;
        self.isMaleGenderButtonShow = YES;
        self.femaleRadioButton.enabled = NO;
        self.maleRadioButton.enabled = YES;
        self.genderErrorImageView.hidden = YES;
        self.selectedGender = [NSString stringWithFormat:@"Female"];
    }
    else
    {
        if([TAJUtilities isIPhone])
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal-568h~iphone.png"]forState:UIControlStateNormal];
            [self.maleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active-568h~iphone.png"]forState:UIControlStateNormal];
        }
        else
        {
            [sender setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_normal.png"]forState:UIControlStateNormal];
            [self.femaleRadioButton setBackgroundImage:[UIImage imageNamed:@"createaccount_radio_active.png"]forState:UIControlStateNormal];
        }
        self.isFemaleGenderButtonShow = YES;
    }
}

- (IBAction)termsAndConditions:(UIButton *)sender {
    WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"Terms & Conditions" url:
                                          [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,TERMS]];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (IBAction) privacyPolicy:(UIButton *)sender {
    WebViewController * viewController = [[WebViewController alloc] initWithTitle:@"Privacy Policy" url:
                                          [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,PRIVACY]];
    [viewController setModalPresentationStyle:UIModalPresentationFullScreen];
    [self presentViewController:viewController animated:NO completion:nil];
}

- (void) loadWebView : (NSString*) urlString {
    if (self.webPageViewController) {
        self.webPageViewController = nil;
    }
    
    if (!self.webPageViewController) {
        self.webPageViewController = [[TAJWebPageViewController alloc]  initWithNibName:@"TAJWebPageViewController" bundle:nil webPageOption:ePurchaseOption];
        self.webPageViewController.urlLinkString = urlString;
        self.webPageViewController.webPageDelegate = self;
        self.webPageViewController.view.frame = self.view.frame;
        [self.navigationController pushViewController:self.webPageViewController animated:YES];
        
    }
}

- (IBAction)previousDate:(UIButton *)sender
{
    NSDate* date = self.currentDate;
    NSDate* yesterday = [date dateByAddingTimeInterval:-(24.0f * 60.0f * 60.0f)];
    [self.datepicker setDate:yesterday  animated:YES];
    [self extractDate:nil];
    self.currentDate = yesterday;
}

- (IBAction)nextDate:(UIButton *)sender
{
    NSDate* date = self.currentDate;
    NSDate* tomorrow = [date dateByAddingTimeInterval:+(24.0f * 60.0f * 60.0f)];
    [self.datepicker setDate:tomorrow  animated:YES];
    [self extractDate:nil];
    self.currentDate = tomorrow;
}

- (IBAction)dateOfBirthSelection:(UIButton *)sender
{
    self.stateContainerView.hidden = YES;
    self.popUpImageView.hidden = YES;
    [self.view endEditing:YES];
    [self validateAge];
    [self chooseDateOfBirthFromDatePicker];
}

-(void)validateAge
{
    NSDateComponents *today = [[NSCalendar currentCalendar] components:NSDayCalendarUnit | NSMonthCalendarUnit | NSYearCalendarUnit fromDate:[NSDate date]];
    NSInteger day = [today day];
    NSInteger month = [today month];
    NSInteger year = [today year];
    int correctYear = year - 18;
    NSDateComponents *correctAge = [[NSDateComponents alloc] init];
    [correctAge setDay:day];
    [correctAge setMonth:month];
    [correctAge setYear:correctYear];
    NSCalendar *calendar = [NSCalendar autoupdatingCurrentCalendar];
    [self.datepicker setDatePickerMode:UIDatePickerModeDate];
    [self.datepicker setMaximumDate:[calendar dateFromComponents:correctAge]];
    [self.datepicker setDate:self.datepicker.maximumDate];
    self.currentDate = self.datepicker.maximumDate;
    return;
}

- (void)chooseDateOfBirthFromDatePicker
{
    if (self.datepickerHolder.hidden)
    {
        self.dateofBirthErrorImageView.hidden = YES;
        self.dateofBirthLabel.textColor =  [UIColor grayColor];
        self.datepickerHolder.hidden = NO;
        self.datePopUp.hidden = NO;
        [self.datepicker addTarget:self action:@selector(extractDate:) forControlEvents:UIControlEventValueChanged];
    }
    else
    {
        self.datepickerHolder.hidden = YES;
        self.datePopUp.hidden = YES;
    }
}

- (void)extractDate:(id) sender
{
    
    NSDate *now = [self.datepicker date];
    NSComparisonResult result = [now compare:self.datepicker.maximumDate];
    
    if(result == NSOrderedSame || result == NSOrderedDescending)
    {
        self.nextButtonForDate.enabled = NO;
    }
    else
    {
        self.nextButtonForDate.enabled = YES;
    }
    self.currentDate = now;
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy"];
    NSString *year = [formatter stringFromDate:now];
    [self.yearButton setTitle:year forState:UIControlStateNormal];
    self.selectedYear = year;
    [formatter setDateFormat:@"MM"];
    NSString *month = [formatter stringFromDate:now];
    NSInteger mon = [month integerValue];
    NSInteger mo = mon - 1;
    NSString *currentMonth;
    if (mo < self.monthNames.count && mo >= 0)
    {
        currentMonth  = [self.monthNames objectAtIndex:mo];
        [self.monthButton setTitle:currentMonth forState:UIControlStateNormal];
    }
    self.selectedMonth = month;
    [formatter setDateFormat:@"dd"];
    NSString *day = [formatter stringFromDate:now];
    [self.dateButton setTitle:day forState:UIControlStateNormal];
    self.selectedDate = day;
    self.selectedDOB = [NSString stringWithFormat:@"%@/%@/%@",day,month,year];
    
    if(self.selectedDOB.length == 0)
    {
        self.stateErrorImageView.hidden = NO;
    }
}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    self.datepickerHolder.hidden = YES;
    self.datePopUp.hidden = YES;
    
    self.stateContainerView.hidden = YES;
    self.popUpImageView.hidden = YES;
    
    if (textField == self.mobileTextField)
    {
        if([TAJUtilities isIPhone])
        {
            self.accountRegistrationScrollView.frame = CGRectMake(self.accountRegistrationScrollView.frame.origin.x, self.accountRegistrationScrollView.frame.origin.y, self.accountRegistrationScrollView.frame.size.width, self.accountRegistrationScrollView.frame.size.height + self.mobileTextField.frame.size.height + 200);
            self.accountRegistrationScrollView.contentSize = CGSizeMake(self.accountRegistrationScrollView.frame.size.width, self.mobileTextField.frame.size.height + 200);
            CGPoint scrollPoint = CGPointMake(self.emailField.frame.origin.x, self.emailField.frame.origin.y);
            [self.accountRegistrationScrollView setContentOffset:scrollPoint animated:YES];
        }
        else
        {
            self.accountRegistrationScrollView.frame = CGRectMake(self.accountRegistrationScrollView.frame.origin.x, self.accountRegistrationScrollView.frame.origin.y, self.accountRegistrationScrollView.frame.size.width, self.accountRegistrationScrollView.frame.size.height + self.mobileTextField.frame.size.height+400);
            self.accountRegistrationScrollView.contentSize = CGSizeMake(self.accountRegistrationScrollView.frame.size.width, self.mobileTextField.frame.size.height+400);
            CGPoint scrollPoint = CGPointMake(self.passwordField.frame.origin.x, self.passwordField.frame.origin.y);
            [self.accountRegistrationScrollView setContentOffset:scrollPoint animated:YES];
        }
        self.mobileLabel.hidden = YES;
    }
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if([textField isEqual:self.userNameField])
    {
        [self.passwordField becomeFirstResponder];
    }
    if([textField isEqual:self.passwordField])
    {
        [self.emailField becomeFirstResponder];
    }
    if([textField isEqual:self.emailField])
    {
        [self.emailField resignFirstResponder];
        //[self chooseDateOfBirthFromDatePicker];
    }
    if([textField isEqual:self.mobileTextField])
    {
        [self.mobileTextField resignFirstResponder];
        [self registerAccountInformation];
    }
    return YES;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Username Validation
    if (textField == self.userNameField) {
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:USERNAME_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        return ([string isEqualToString:filteredString] ? newLength <= 15 : NO);
    }
    // Email
    if (textField == self.emailField) {
        return !([textField.text length] > EmailAddressCharacterLimit && [string length] > range.length);
    }
    // Password
    if (textField == self.passwordField) {
        return !([textField.text length] > PasswordLength && [string length] > range.length);
    }
    // Mobile Number
    else if (textField == self.mobileTextField) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        // Limit phone number input to 8 digits
        return ([string isEqualToString:filteredString] ? newLength <= 10 : NO);
    }
    return YES;
}


- (void)registerAccountInformation
{
    BOOL error = [self validateInput];
    if (!error) {
        if (!self.show_lobby) {
            // no need to restrict
            if ([[LocationHandler SharedLocationHandler] shouldFetchUserLocationInViewController:self] == YES) {
#if DEBUG
                NSLog(@"YES WE CAN GET LOCATION");
#endif
                [self findBlickListStates:self.strState];
            }
            else {
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:@AlertTitle_authorizationLocationDenied
                                                                               message:@AlertMessage_authorizationLocationDenied
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:location_cancel style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          
                                                                          
                                                                      }];
                
                UIAlertAction* settingsAction = [UIAlertAction actionWithTitle:location_settings style:UIAlertActionStyleDefault
                                                                       handler:^(UIAlertAction * action) {
                                                                           [self openURLWithString:UIApplicationOpenSettingsURLString];
                                                                           
                                                                       }];
                [alert addAction:defaultAction];
                [alert addAction:settingsAction];
                
                [self presentViewController:alert animated:YES completion:nil];
            }
            
        }
        else {
            [self registration];
        }
    }
}

- (void)registrationPost {
    
    NSString * deviceID;
    
    if ([TAJUtilities isIPhone])
    {
        deviceID = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceID = @"Tablet";//@"IPAD";
    }
    //NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSDictionary *params = @{@"username": self.userNameField.text,
                             @"password": self.passwordField.text,
                             @"email" : self.emailField.text,
                             //@"device_id": @"1",
                             @"device_type" : deviceID,
                             @"client_type" : @"iOS",
                             @"version" : buildNumber,
                             @"device_id": [UIDevice currentDevice].identifierForVendor,
                             @"device_brand": [TAJUtilities checkIfStringIsEmpty:APP_DELEGATE.brandName]
                             };
#if DEBUG
    NSLog(@"SIGNUP PARAMS: %@", params);
#endif
    
    //NSString * URL = @"http://rc.glserv.info/api/v1/signup/";
    
    [[Service sharedInstance] postRequestWithURL:[NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,SIGNUP] withParams:params completionBlock:^(id jsonResponse) {
        dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
            NSLog(@"RESPONSE: %@", jsonResponse);
#endif
            if ([[jsonResponse valueForKey:@"status"] isEqualToString:@"Success"]) {
                
#if DEBUG
                NSLog(@"SIGNUP RESPONSE: %@", jsonResponse);
                NSLog(@"unique_id : %@",[jsonResponse valueForKey:@"unique_id"]);
                NSLog(@"username : %@",[jsonResponse valueForKey:@"username"]);
                NSLog(@"token : %@",[jsonResponse valueForKey:@"token"]);
#endif
                self.userName = self.userNameField.text;
                self.password = self.passwordField.text;
                
                self.uniqueID = [jsonResponse valueForKey:@"unique_id"];
                //need to save for future reference - token
                [[NSUserDefaults standardUserDefaults] setObject:[jsonResponse valueForKey:@"unique_id"] forKey:UNIQUE_SESSION_KEY];
                
                APP_DELEGATE.dynamicIP = [jsonResponse valueForKey:@"ip"];
                
                [self onLoginButtonClicked];
                
            }
            else {
#if DEBUG
                NSLog(@"Error : %@",jsonResponse);
#endif
                self.usernameErrorImageView.hidden = YES;
                self.passwordErrorImageView.hidden = YES;
                self.emailErrorImageView.hidden = YES;
                
                [self.spinner stopAnimating];
            }
        });
    } errorBlock:^(NSString *errorString) {
#if DEBUG
        NSLog(@"LOGIN ERROR: %@",errorString);
#endif
        dispatch_async(dispatch_get_main_queue(), ^{
        });
    }];
}

#pragma mark - REGISTRATION

-(void)registration
{
    [self.view makeToastActivity:CSToastPositionCenter];
    
    NSString * deviceID;
    
    if ([TAJUtilities isIPhone])
    {
        deviceID = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceID = @"Tablet";//@"IPAD";
    }
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    NSDictionary *infoDict = [[NSBundle mainBundle] infoDictionary];
    NSString *buildNumber = [infoDict objectForKey:@"CFBundleVersion"];
    
    NSDictionary *params = @{@"username": self.userNameField.text,
                             @"password": self.passwordField.text,
                             @"email" : self.emailField.text,
                             @"mobile" : self.mobileTextField.text,
                             //@"device_id": @"1",
                             @"device_type" : deviceID,
                             @"client_type" : @"iOS",
                             @"version" : buildNumber
                             };
#if DEBUG
    NSLog(@"REGISTRATION PARAMS: %@", params);
#endif
    
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,SIGNUP];
    [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:NO headerValue:nil withParams:params :^(id jsondata, BOOL success){
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            if (success)
            {
                dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
                    // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                    if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                        
#if DEBUG
                        NSLog(@"SIGNUP RESPONSE: %@", jsondata);
                        NSLog(@"unique_id : %@",[jsondata valueForKey:@"unique_id"]);
                        NSLog(@"token : %@",[jsondata valueForKey:@"token"]);
#endif
                        self.userName = self.userNameField.text;
                        self.password = self.passwordField.text;
                        
                        NSDictionary * jsonDictionay = jsondata;
                        NSDictionary *tempDict = [jsonDictionay dictionaryRemovingNSNullValues];
#if DEBUG
                        NSLog(@"%@", tempDict);
#endif
                        NSString * playerId = [tempDict valueForKey:@"playerid"];
                        NSString * email = [tempDict valueForKey:@"email"];
                        NSString * gender = [tempDict valueForKey:@"gender"];
                        NSString * mobile =[tempDict valueForKey:@"mobile"];
                        NSString * clientType = [tempDict valueForKey:@"client_type"];
                        
                        APP_DELEGATE.dynamicIP = [tempDict valueForKey:@"ip"];

                        WEGUser* weUser = [WebEngage sharedInstance].user;
                        [weUser login:[NSString stringWithFormat:@"%@",playerId]];
                        [weUser setEmail:email];
                        [weUser setGender:[gender lowercaseString]];
                        if ([mobile isEqualToString:@""]) {
                            //
                        }
                        else {
                            [weUser setPhone:[NSString stringWithFormat:@"%@",mobile]];
                            [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"mobile"] forKey:USERMOBILE_KEY];
                        }
                        
                        id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                        
                        
                        NSDictionary *userData=@{
                                                 @"userId":[NSString stringWithFormat:@"%@",playerId],
                                                 @"device_type":deviceID,
                                                 @"client_type":clientType
                                                 };
                        [weAnalytics trackEventWithName:@"User Registered" andValue:userData];
                        
                        self.uniqueID = [jsondata valueForKey:@"unique_id"];
                        
                        //need to save for future reference - token
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"unique_id"] forKey:UNIQUE_SESSION_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"token"] forKey:TOKEN_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"email"] forKey:USEREMAIL_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:[jsondata valueForKey:@"username"] forKey:USENAME_KEY];
                        [[NSUserDefaults standardUserDefaults] setObject:playerId forKey:USERID];
                        
                        [[NSUserDefaults standardUserDefaults] synchronize];
                        
                        [self onLoginButtonClicked];
                        
                    }
                    else {
#if DEBUG
                        NSLog(@"REGISTRATION Error : %@",jsondata);
#endif
                        [self.view hideToastActivity];
                        [self.view makeToast:[jsondata objectForKey:@"message"]];
                        
                        [self.spinner stopAnimating];
                    }
                });
            }
            else
            {
                [self.view hideToastActivity];
                [self.view makeToast:@"Error while fetching data from server"];
                
                [self.spinner stopAnimating];
            }
        });
    }];
}

- (void)defaultStateSelect
{
    NSInteger selectedRow = [self.statePickerView selectedRowInComponent:0];
    NSString *resultString = [[NSString alloc] initWithFormat:@"%@",[self.stateNames objectAtIndex:selectedRow]];
    NSString *key = [NSString stringWithFormat:@"%@", @"picker"];
    [[NSUserDefaults standardUserDefaults] setInteger:selectedRow forKey:key];
    self.selectedRow = selectedRow;
    [self updateSelectedState:resultString];
    
}

- (IBAction)sateDoneButtonPressed:(id)sender
{
    [self removeStateView];
    if(self.selectedRow == 0)
    {
        [self defaultStateSelect];
    }
}

- (IBAction)leftArrowButtonPressed:(UIButton *)sender
{
    NSInteger currentRow = [self.statePickerView selectedRowInComponent:0];
    currentRow = currentRow - 1;
    if(currentRow <self.stateNames.count && currentRow >=0)
    {
        self.rightArrowButton.enabled = YES;
        [self.statePickerView selectRow:currentRow inComponent:0 animated:NO];
        self.selectedRow = currentRow;
        NSString *resultString = [[NSString alloc] initWithFormat:@"%@",self.stateNames[self.selectedRow] ];
        [self updateSelectedState:resultString];
    }
    else
    {
        self.leftArrowButton.enabled = NO;
        self.rightArrowButton.enabled = YES;
    }
}

- (IBAction)rightArrowButtonPressed:(UIButton *)sender
{
    NSInteger currentRow = [self.statePickerView selectedRowInComponent:0];
    currentRow = currentRow + 1;
    if(currentRow <self.stateNames.count && currentRow >=0)
    {
        self.leftArrowButton.enabled = YES;
        
        [self.statePickerView selectRow:currentRow inComponent:0 animated:NO];
        self.selectedRow = currentRow;
        NSString *resultString = [[NSString alloc] initWithFormat:@"%@",self.stateNames[self.selectedRow] ];
        [self updateSelectedState:resultString];
    }
    else
    {
        self.leftArrowButton.enabled = YES;
        self.rightArrowButton.enabled = NO;
    }
}

- (void)initializeThePickerViewWithCurrentSelectedState
{
    [self.statePickerView selectRow:self.selectedRow  inComponent:0 animated:NO];
}

#pragma mark - PickerView DataSource -

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return [self.stateNames count];
}

- (NSString *)pickerView:(UIPickerView *)pickerView   titleForRow:(NSInteger)row   forComponent:(NSInteger)component
{
    return [self.stateNames objectAtIndex:row];
}

- (CGFloat)pickerView:(UIPickerView *)pickerView rowHeightForComponent:(NSInteger)component
{
    if ([TAJUtilities isIPhone])
    {
        return 30;
    }
    else
    {
        return 50;
    }
}

#pragma mark - PickerView Delegate -

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row  inComponent:(NSInteger)component
{
    NSString *resultString = [[NSString alloc] initWithFormat:@"%@",[self.stateNames objectAtIndex:row]];
    NSInteger selectedRow = [self.statePickerView selectedRowInComponent:component];
    if(selectedRow <= 0)
    {
        self.leftArrowButton.enabled = NO;
        self.rightArrowButton.enabled = YES;
    }
    else if(selectedRow >= self.stateNames.count - 1)
    {
        self.leftArrowButton.enabled = YES;
        self.rightArrowButton.enabled = NO;
    }
    else if(selectedRow > 0 && selectedRow < self.stateNames.count - 1)
    {
        self.leftArrowButton.enabled = YES;
        self.rightArrowButton.enabled = YES;
    }
    NSString *key = [NSString stringWithFormat:@"%@", @"picker"];
    [[NSUserDefaults standardUserDefaults] setInteger:selectedRow forKey:key];
    self.selectedRow = selectedRow;
    [self updateSelectedState:resultString];
    
}

- (UIView *)pickerView:(UIPickerView *)pickerView viewForRow:(NSInteger)row forComponent:(NSInteger)component reusingView:(UIView *)view
{
    UILabel *label = (UILabel*) view;
    if (label == nil)
    {
        label = [[UILabel alloc] init];
    }
    
    label.text = [self.stateNames objectAtIndex:row];
    // This part just colorizes everything
    [label setTextColor:[UIColor blackColor]];
    CGSize rowSize = [pickerView rowSizeForComponent:component];
    CGRect labelRect = CGRectMake (0, 0, self.statePickerView.frame.size.width, rowSize.height);
    UIFont *myFont;
    if ([TAJUtilities isIPhone])
    {
        myFont = [UIFont systemFontOfSize:22];
    }
    else
    {
        myFont = [UIFont systemFontOfSize:25];
    }
    label.font = myFont;    //[cell.textLabel adjustsFo
    if (floor(NSFoundationVersionNumber) < NSFoundationVersionNumber_iOS_6_0)
    {
        //iOS 6.1 or earlier
        label.textAlignment = UITextAlignmentCenter;
    }
    else
    {
        //iOS 7 or later
        label.textAlignment = NSTextAlignmentCenter;
    }
    
    [label setFrame:labelRect];
    return label;
}

#pragma mark - Add notification -

- (void)addNotificationListener
{
    //success login
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(loginUpdate:)
                                                 name:MOVETOHOMEPAGE
                                               object:nil];
}

#pragma mark - Remove notification -

- (void)removeNotificationListener
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:MOVETOHOMEPAGE object:nil];
}

#pragma mark - Login Actions -
#pragma mark - Engine Connection


- (void)onLoginButtonClicked
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        if (!([self.userName length] == 0) &&
            !([self.password length] == 0))
        {
            
            [[TAJGameEngine sharedGameEngine] createUserModelWithUserName: self.userName
                                                                 password: self.password sessionId:self.uniqueID];
            TAJGameEngine *engine= [TAJGameEngine sharedGameEngine];
            engine.isOtherLoggedIn =FALSE;
            [[TAJClientServerCommunicator sharedClientServerCommunicator] createSocketAndConnect];
            [[TAJGameEngine sharedGameEngine] setCurrentController:self];
        }
    }
}

#pragma mark - Notification observer -

//Success login
- (void)loginUpdate:(NSNotification *) notification
{
#if USERNAME_PASSWORD_STORE_CHECK
    [[NSUserDefaults standardUserDefaults] setObject:self.userName forKey:USENAME_KEY];
    [[NSUserDefaults standardUserDefaults] setObject:self.password forKey:PASSWORD_KEY];
#endif
    
    if ([[notification name] isEqualToString: MOVETOHOMEPAGE])
    {
        TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
        [[NSUserDefaults standardUserDefaults] setObject:engine.lobby.tableDetails forKey: NSUSER_MOVE_DATA];
        [[NSUserDefaults standardUserDefaults] synchronize];
        
        self.appDelegate = [[UIApplication sharedApplication] delegate];
        
#if NEW_DESIGN_IMPLEMENTATION
        [self.appDelegate loadPrelobbyScreenAfterRegistrationSuccessful];
#else
        [self.appDelegate loadHomePageWindow];
#endif
        
    }
}


#pragma mark - LOCATION MANAGER DELEGATE

- (void)locationFound:(double)latitiude and:(double)longitude
{
#if DEBUG
   // NSLog(@"Current - Latitude: %f ,Current - Longitude: %f",latitiude, longitude);
#endif
    CLLocation* eventLocation = [[CLLocation alloc] initWithLatitude:latitiude longitude:longitude];
    
    [self getAddressFromLocation:eventLocation complationBlock:^(NSString * address) {
        if(address) {
            //_address = address;
            //NSLog(@"Address : %@",address);
        }
    }];
    
}

-(void)getAddressFromLocation:(CLLocation *)location complationBlock:(addressCompletion)completionBlock
{
    __block CLPlacemark* placemark;
    __block NSString *address = nil;
    
    CLGeocoder* geocoder = [CLGeocoder new];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error)
     {
         if (error == nil && [placemarks count] > 0)
         {
             self.strState = @"";
             placemark = [placemarks lastObject];
#if DEBUG
             //NSLog(@"placemark : %@",placemark);
             //NSLog(@"subLocality : %@",placemark.subLocality);
             //NSLog(@"Country : %@",placemark.country);
             //NSLog(@"Area : %@",placemark.administrativeArea);
#endif
             address = [NSString stringWithFormat:@"%@, %@ %@", placemark.name, placemark.postalCode, placemark.locality];
             completionBlock(address);
             self.strState = placemark.administrativeArea;
             
         }
     }];
    
}

#pragma mark - FINDING BLOCKLIST

- (void)findBlickListStates:(NSString *)parameter {
    
    if ([self.statesList containsObject:parameter.lowercaseString]) {
#if DEBUG
        NSLog(@"We have %@ in blocked list",parameter);
#endif
        [self loadBlockedPopUP];
    }
    else {
#if DEBUG
        NSLog(@"We don't have %@ in blocked list",parameter);
#endif
        [self registration];
    }
    
}

#pragma BLOCKED POPUP

-(void)loadBlockedPopUP
{
    if(!self.blockedPopup)
    {
        self.blockedPopup = [[BlockedPopupViewController alloc] initWithNibName:@"BlockedPopupViewController" bundle:nil];
    }
    self.blockedPopup.BlockedPopupDelegate = self;
    [TAJAppDelegate setScreenSizeForRes: self.blockedPopup.view];
    [self.view addSubview:self.blockedPopup.view];
}

-(void)closeButtonPressedForBlockedPopup
{
    if(self.blockedPopup)
    {
        [self.blockedPopup.view removeFromSuperview];
        self.blockedPopup = nil;
    }
    self.spinner.hidden = YES;
}

@end
