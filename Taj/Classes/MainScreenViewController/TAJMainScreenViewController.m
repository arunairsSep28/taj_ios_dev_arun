/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMainScreenViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Lloyd Praveen Mascarenhas on 08/07/14.
 Modified by Pradeep BM on 07/06/14.
 Created by Raghavendra Kamat on 10/04/14.
 **/

#import "TAJMainScreenViewController.h"
#import "TAJLoginHomeViewController.h"
#import "TAJLoginViewController.h"
#import "TAJSplashViewController.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJInfoPopupViewController.h"
#import "TAJUserModel.h"
#import "TAJGameEngine.h"
#import "TAJAccountRegistrationViewController.h"
#import "TAJAppDelegate.h"
#import "TAJXMLParserDictionary.h"
#import "TAJConfigProvider.h"
#import "TAJClientServerCommunicator.h"
#import <sys/utsname.h>

#import "BannersCollectionViewCell.h"
#import "GamesTypeTableViewCell.h"

#import <Crashlytics/Crashlytics.h>

@interface TAJMainScreenViewController ()<TAJSocketDelegate,UICollectionViewDelegate,UICollectionViewDataSource>

@property (weak, nonatomic) IBOutlet UIButton *createNewAccountButton;
@property (weak, nonatomic) IBOutlet UIButton *playNowButton;

@property (weak, nonatomic) IBOutlet UIButton *logOutButton;
@property (weak, nonatomic) IBOutlet UIImageView *bonusChunkImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *bonusChunkImageView2;
@property (weak, nonatomic) IBOutlet UIImageView *bonusChunkImageView3;
@property (weak, nonatomic) IBOutlet UIImageView *bonusChunkImageView4;
@property (weak, nonatomic) IBOutlet UILabel *firstChunksDivider;
@property (weak, nonatomic) IBOutlet UILabel *secondChunksDivider;
@property (weak, nonatomic) IBOutlet UILabel *thirdChunksDivider;

- (IBAction)buyChipsPressed:(UIButton *)sender;
- (IBAction)playNowButtonClicked:(UIButton *)sender;
@property (weak, nonatomic) IBOutlet UIImageView *thisDeviceImageView;

// live feed data
@property (weak, nonatomic) IBOutlet UILabel *noOfPlayersLabel;
@property (weak, nonatomic) IBOutlet UILabel *noOfTableLabel;

@property (weak, nonatomic) IBOutlet UIView *viewCash;
@property (weak, nonatomic) IBOutlet UIView *viewFree;
@property (weak, nonatomic) IBOutlet UILabel *lblCash;
@property (weak, nonatomic) IBOutlet UILabel *lblFree;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCash;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewFree;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewCashType;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewFreeType;
@property (weak, nonatomic) IBOutlet UIView *viewLogoContainer;
@property (strong, nonatomic) NSString * version;

@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) IBOutlet UIPageControl *pageControl;

@property (strong, nonatomic) NSMutableArray *arrayBaners;

//new
@property (weak, nonatomic) IBOutlet UIView *viewCashType;
@property (weak, nonatomic) IBOutlet UIView *viewFreeType;
@property (weak, nonatomic) IBOutlet UIView *viewFavouritesType;
@property (weak, nonatomic) IBOutlet UIView *viewTounamentsType;

@property (weak, nonatomic) IBOutlet UILabel *lblCashType;
@property (weak, nonatomic) IBOutlet UILabel *lblFreeType;
@property (weak, nonatomic) IBOutlet UILabel *lblFavouritesType;
@property (weak, nonatomic) IBOutlet UILabel *lblTournamentsType;




@property BOOL isFreeGames;
@property BOOL isCahsGames;
@property BOOL isFavouriteGames;

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (strong, nonatomic) NSArray *arrayGamesType;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

@end


@implementation TAJMainScreenViewController

static NSString *const kCellIdentifierProducts = @"cellIdentifierProducts";

static NSString *const kCellIdentifier = @"cellIdentifierProducts";
static const CGFloat kCellHeight = 98;
static const CGFloat kCellHeightiPad = 124;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
        
        CGFloat width, height;
        
        if ([TAJUtilities isIPhone])
        {
            width = SIZE_OF_CAROUSEL_ITEM_IPHONE;
            height = SIZE_OF_CAROUSEL_ITEM_H_IPHONE;
        }
        else
        {
            width = SIZE_OF_CAROUSEL_ITEM_IPAD;
            height = SIZE_OF_CAROUSEL_ITEM_H_IPAD;
        }
        
        NSString *dealsImageName = DEALS_IMAGE;
        NSString *strikesImageName = STRIKES_IMAGE;
        NSString *poolsImageName = POOLS_IMAGE;
        NSString *tournamentsImageName = TOURNAMENTS_IMAGE;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

//    for(int i=0; i<6; i++)
//    {
//        NSLog(@"laugh : ha");
//    }
    
    // Do any additional setup after loading the view from its nib.
    
    self.createNewAccountButton.hidden = YES;
    
    self.navigationController.navigationBar.hidden = YES;
    [self updatePlayersInformation];
    self.version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    self.versionLabel.text = [NSString stringWithFormat:@"Version : %@",self.version];
    self.isScrolling = NO;
    
    // Get an instance of `WEGAnalytics` object
    id weAnalytics = [WebEngage sharedInstance].analytics;
    [weAnalytics navigatingToScreenWithName:@"Pre Lobby"];
    
   // [self.noOfPlayersLabel setTransform:CGAffineTransformMakeRotation(-M_PI / 2)];

    [self setCash];
    
    if ([TAJUtilities isItIPhonex] || [TAJUtilities isItIPhoneXSMax]) {
        CGRect Frame = self.versionLabel.frame;
        Frame.origin.y = self.versionLabel.frame.origin.y - 80;
        self.versionLabel.frame = Frame;
    }
    
    if ([TAJUtilities isItIPhone6]) {
        CGRect Frame = self.viewLogoContainer.frame;
        Frame.origin.y = self.viewLogoContainer.frame.origin.y - 54;
        self.viewLogoContainer.frame = Frame;
    }
    
//    [[NSNotificationCenter defaultCenter] addObserver:self
//                                             selector:@selector(closePopUp:)
//                                                 name:@"closePopUp"
//                                               object:nil];
//
    
    NSLog(@"3rd Gen Width : %f",self.view.frame.size.width);
    NSLog(@"3rd Gen Height : %f",self.view.frame.size.height);
    
    if (self.view.frame.size.height == 688) {
        CGRect Frame = self.versionLabel.frame;
        Frame.origin.y = self.versionLabel.frame.origin.y - 60;
        self.versionLabel.frame = Frame;
        /*
        CGRect FramePlayers = self.noOfPlayersLabel.frame;
        FramePlayers.origin.x = self.noOfPlayersLabel.frame.origin.x - 80;
        self.noOfPlayersLabel.frame = FramePlayers;
        */
    }
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:USERID];
    NSString *userName = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
    
//    [FIRAnalytics setUserID:userID];
//    [FIRAnalytics setValue:userName forKey:@"user_name"];
    [CrashlyticsKit setUserIdentifier:userID];
    [CrashlyticsKit setObjectValue:userName forKey:@"user_name"];
    [CrashlyticsKit setObjectValue:userID forKey:@"user_id"];
    
    [self configureView];
}

- (void)configureView {
    
    self.arrayGamesType = @[
                            @{
                                @"title":@"Points Rummy(Strikes)",
                                @"sub_title" : @"Players online : 5000",
                                @"logo" : @"logo_strikesrummy"
                                },
                            @{
                                @"title":@"Pools Rummy(101,201)",
                                @"sub_title" : @"Players online : 10000",
                                @"logo" : @"logo_Poolsrummy"
                                },
                            @{
                                @"title":@"Deals Rummy(Best of X)",
                                @"sub_title" : @"Players online : 3000",
                                @"logo" : @"logo_dealsrummy"
                                },
                            @{
                                @"title":@"Private Tables(Friends)",
                                @"sub_title" : @"Players online : 2000",
                                @"logo" : @"logo_Tournament"
                                },
                            @{
                                @"title":@"Tournaments",
                                @"sub_title" : @"Players online : 4000",
                                @"logo" : @"logo_Tournament"
                                }
                            ];
    
    // Register TableView cells
    [self.tableView registerNib:[UINib nibWithNibName:NSStringFromClass([GamesTypeTableViewCell class]) bundle:nil] forCellReuseIdentifier:kCellIdentifier];
    [self.tableView reloadData];

    // Register Collection view cells
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BannersCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kCellIdentifierProducts];
    
    self.pageControl.currentPage = 0;
    self.pageControl.numberOfPages = 4;

    [self.collectionView reloadData];
    
    [self setCashGames];
    
    if ([TAJUtilities isIPhone])
    {
        
    }
    else {
        self.tableViewHeightConstraint.constant = kCellHeightiPad * 4;
    }
}
- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    TAJUserModel *model = engine.usermodel;
    DLog(@"thisDeviceImageName = %@",[model thisPlayerImageName]);
    
    UIImage *image = [UIImage imageNamed:[model thisPlayerImageName]];
    self.thisDeviceImageView.image = image;
    
    [self updatePlayersInformation];
    [self requestBonusChunks];
    [self addMainScreenObserver];
    [self addObserver];
}


- (void)viewDidDisappear:(BOOL)animated
{
    [self removeMainScreenObserver];
}

#pragma mark - HELPERS

- (void)relaodFunChips {
    [self.view makeToastActivity:CSToastPositionCenter];
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,RELOAD_FUNCHIPS];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    NSLog(@"header %@", header);
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
            NSLog(@"RELOAD_FUNCHIPS Response :%@",responseData);
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                NSLog(@"RELOAD_FUNCHIPS DATA : %@",[responseData valueForKey:@"data"]);
                
                NSDictionary * jsonDictionay = [responseData valueForKey:@"data"];
                NSDictionary *dataDic = [jsonDictionay dictionaryRemovingNSNullValues];
                NSLog(@"%@", dataDic);
                //[self.view makeToast:[dataDic valueForKey:@"message"]];
                [self.view makeToast:[responseData objectForKey:@"message"]];
            }
            else {
                [self.view hideToastActivity];
                [self.view makeToast:[responseData valueForKey:@"message"]];
            }
        });
        
    } errorBlock:^(NSString *errorString) {
        NSLog(@"RELOAD_FUNCHIPS ERROR : %@",errorString);
        [self.view hideToastActivity];
        [self.view makeToast:errorString];
    }];
}

- (NSString*) deviceName
{
    struct utsname systemInfo;
    
    uname(&systemInfo);
    
    NSString* code = [NSString stringWithCString:systemInfo.machine
                                        encoding:NSUTF8StringEncoding];
    
    static NSDictionary* deviceNamesByCode = nil;
    
    if (!deviceNamesByCode) {
        
        deviceNamesByCode = @{@"i386"      : @"Simulator",
                              @"x86_64"    : @"Simulator",
                              @"iPod1,1"   : @"iPod Touch",        // (Original)
                              @"iPod2,1"   : @"iPod Touch",        // (Second Generation)
                              @"iPod3,1"   : @"iPod Touch",        // (Third Generation)
                              @"iPod4,1"   : @"iPod Touch",        // (Fourth Generation)
                              @"iPod7,1"   : @"iPod Touch",        // (6th Generation)
                              @"iPhone1,1" : @"iPhone",            // (Original)
                              @"iPhone1,2" : @"iPhone",            // (3G)
                              @"iPhone2,1" : @"iPhone",            // (3GS)
                              @"iPad1,1"   : @"iPad",              // (Original)
                              @"iPad2,1"   : @"iPad 2",            //
                              @"iPad3,1"   : @"iPad",              // (3rd Generation)
                              @"iPhone3,1" : @"iPhone 4",          // (GSM)
                              @"iPhone3,3" : @"iPhone 4",          // (CDMA/Verizon/Sprint)
                              @"iPhone4,1" : @"iPhone 4S",         //
                              @"iPhone5,1" : @"iPhone 5",          // (model A1428, AT&T/Canada)
                              @"iPhone5,2" : @"iPhone 5",          // (model A1429, everything else)
                              @"iPad3,4"   : @"iPad",              // (4th Generation)
                              @"iPad2,5"   : @"iPad Mini",         // (Original)
                              @"iPhone5,3" : @"iPhone 5c",         // (model A1456, A1532 | GSM)
                              @"iPhone5,4" : @"iPhone 5c",         // (model A1507, A1516, A1526 (China), A1529 | Global)
                              @"iPhone6,1" : @"iPhone 5s",         // (model A1433, A1533 | GSM)
                              @"iPhone6,2" : @"iPhone 5s",         // (model A1457, A1518, A1528 (China), A1530 | Global)
                              @"iPhone7,1" : @"iPhone 6 Plus",     //
                              @"iPhone7,2" : @"iPhone 6",          //
                              @"iPhone8,1" : @"iPhone 6S",         //
                              @"iPhone8,2" : @"iPhone 6S Plus",    //
                              @"iPhone8,4" : @"iPhone SE",         //
                              @"iPhone9,1" : @"iPhone 7",          //
                              @"iPhone9,3" : @"iPhone 7",          //
                              @"iPhone9,2" : @"iPhone 7 Plus",     //
                              @"iPhone9,4" : @"iPhone 7 Plus",     //
                              @"iPhone10,1": @"iPhone 8",          // CDMA
                              @"iPhone10,4": @"iPhone 8",          // GSM
                              @"iPhone10,2": @"iPhone 8 Plus",     // CDMA
                              @"iPhone10,5": @"iPhone 8 Plus",     // GSM
                              @"iPhone10,3": @"iPhone X",          // CDMA
                              @"iPhone10,6": @"iPhone X",          // GSM
                              @"iPhone11,2": @"iPhone XS",         //
                              @"iPhone11,4": @"iPhone XS Max",     //
                              @"iPhone11,6": @"iPhone XS Max",     // China
                              @"iPhone11,8": @"iPhone XR",         //
                              
                              @"iPad4,1"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Wifi
                              @"iPad4,2"   : @"iPad Air",          // 5th Generation iPad (iPad Air) - Cellular
                              @"iPad4,4"   : @"iPad Mini",         // (2nd Generation iPad Mini - Wifi)
                              @"iPad4,5"   : @"iPad Mini",         // (2nd Generation iPad Mini - Cellular)
                              @"iPad4,7"   : @"iPad Mini",         // (3rd Generation iPad Mini - Wifi (model A1599))
                              @"iPad6,7"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1584)
                              @"iPad6,8"   : @"iPad Pro (12.9\")", // iPad Pro 12.9 inches - (model A1652)
                              @"iPad6,3"   : @"iPad Pro (9.7\")",  // iPad Pro 9.7 inches - (model A1673)
                              @"iPad6,4"   : @"iPad Pro (9.7\")"   // iPad Pro 9.7 inches - (models A1674 and A1675)
                              };
    }
    
    NSString* deviceName = [deviceNamesByCode objectForKey:code];
    
    if (!deviceName) {
        // Not found on database. At least guess main device type from string contents:
        
        if ([code rangeOfString:@"iPod"].location != NSNotFound) {
            deviceName = @"iPod Touch";
        }
        else if([code rangeOfString:@"iPad"].location != NSNotFound) {
            deviceName = @"iPad";
        }
        else if([code rangeOfString:@"iPhone"].location != NSNotFound){
            deviceName = @"iPhone";
        }
        else {
            deviceName = @"Unknown";
        }
    }
    
    return deviceName;
}

- (void)addMainScreenObserver
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateUserInformationOnBalanceUpdate:) name:BALANCE_UPDATE object:nil];
}

- (void)removeMainScreenObserver
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:BALANCE_UPDATE object:nil];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}

- (BOOL) shouldAutorotate
{
    return YES;
}

-(NSUInteger)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
        // return UIInterfaceOrientationMaskPortrait;//| UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) updateUserInformationOnBalanceUpdate:(NSNotification *)notification
{
    [self updatePlayersInformation];
}

- (void) updatePlayersInformation
{
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    TAJUserModel *model = engine.usermodel;
    
    self.playerNameLable.text = [model.nickName lowercaseString];
    self.realChipsValue.text = [NSString stringWithFormat:@"%.02f",[model.realChips floatValue]];
    //self.realChipsValue.text = [NSString stringWithFormat:@"Your Balance : ₹%d",[model.realChips intValue]];
    self.realInPlayValue.text = [NSString stringWithFormat:@"%.02f",[model.realInPlay floatValue]];
    self.funChipsValue.text = [NSString stringWithFormat:@"%.02f",[model.funchips floatValue]];
    //self.funChipsValue.text = [NSString stringWithFormat:@"Fun Chips : %d",[model.funchips intValue]];
    self.funInPlayValue.text = [NSString stringWithFormat:@"%.02f",[model.funInPlay floatValue]];
    
//    [FIRAnalytics setValue:self.funChipsValue.text forKey:@"fun_chips"];
//    [FIRAnalytics setValue:self.funInPlayValue.text forKey:@"fun_inplay"];
//    [FIRAnalytics setValue:self.realInPlayValue.text forKey:@"real_chips"];
//    [FIRAnalytics setValue:self.realInPlayValue.text forKey:@"real_inplay"];
    [CrashlyticsKit setObjectValue:self.funChipsValue.text forKey:@"fun_chips"];
    [CrashlyticsKit setObjectValue:self.funInPlayValue.text forKey:@"fun_inplay"];
    [CrashlyticsKit setObjectValue:self.realChipsValue.text forKey:@"real_chips"];
    [CrashlyticsKit setObjectValue:self.realInPlayValue.text forKey:@"real_inplay"];
}

- (IBAction)createNewAccount:(id)sender
{
    [(TAJAppDelegate*)TAJ_APP_DELEGATE loadRegistrationScreen];
}

- (IBAction)logoutClicked:(id)sender
{
    if ([[TAJUtilities sharedUtilities] isInternetConnected])
    {
        [[TAJGameEngine sharedGameEngine] destroyUserNameAndPassword];
        [[TAJGameEngine sharedGameEngine] logout];
        
        TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
        [delegate loadMainWindow];
    }
}

#pragma mark - GAME TYPES METHODS

- (IBAction)strikesClicked:(UIButton *)sender {
    DLog(@"strikesClicked");
    
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(strikesClickedOrJokerClicked)])
    {
        [self.mainScreenDelegate strikesClickedOrJokerClicked];
    }
}

- (IBAction)tournamentClicked:(UIButton *)sender
{
    DLog(@"tournamentClicked");
    
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(tournamentsRummyClicked)])
    {
        [self.mainScreenDelegate tournamentsRummyClicked];
    }
}

- (IBAction)poolsClicked:(UIButton *)sender
{
    DLog(@"poolsClicked");
    
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(poolRummyClicked)])
    {
        [self.mainScreenDelegate poolRummyClicked];
    }
}

- (IBAction)dealsClicked:(UIButton *)sender
{
    DLog(@"dealsClicked");
    
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(dealsRummyClicked)])
    {
        [self.mainScreenDelegate dealsRummyClicked];
    }
}

- (IBAction)favouritesClicked:(UIButton *)sender {
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(favouritesClicked)])
    {
        [self.mainScreenDelegate favouritesClicked];
    }
}

#pragma mark - TABLES, PLAYERS COUNT

- (void)updateLiveFeedTajTables:(NSString *)noOfTable
{
    self.noOfTableLabel.text = [NSString stringWithFormat:LIVE_TAJ_TABLE_FEED,noOfTable];
}

- (void)updateLiveFeedTajPlayer:(NSString *)noOfPlayer
{
    self.noOfPlayersLabel.text = [NSString stringWithFormat:LIVE_TAJ_PLAYERS_FEED,noOfPlayer];
}

#pragma mark - HELPERS

-(void)setDefault {
    self.lblCash.textColor = [UIColor whiteColor];
    self.lblFree.textColor = [UIColor whiteColor];
    self.viewCash.backgroundColor = [UIColor clearColor];
    self.viewFree.backgroundColor = [UIColor clearColor];
    self.imgViewCash.hidden = YES;
    self.imgViewFree.hidden = YES;
}

-(void)setCash {
    [self setDefault];
    self.lblCash.textColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
    self.viewCash.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.gameType = CASH_GAME_LOBBY;
    self.imgViewCash.hidden = YES;
    APP_DELEGATE.gameType = self.gameType;
    self.imgViewCashType.image = [UIImage imageNamed:@"cashType_selected"];
    self.imgViewFreeType.image = [UIImage imageNamed:@"cashType_unselected"];
}

-(void)setFree {
    [self setDefault];
    self.lblFree.textColor = [UIColor colorWithRed:251.0f/255.0f green:176.0f/255.0f blue:59.0f/255.0f alpha:1.0f];
    self.viewFree.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:0.5f];
    self.gameType = FREE_GAME_LOBBY;
    self.imgViewFree.hidden = YES;
    APP_DELEGATE.gameType = self.gameType;
    self.imgViewCashType.image = [UIImage imageNamed:@"cashType_unselected"];
    self.imgViewFreeType.image = [UIImage imageNamed:@"cashType_selected"];
}

#pragma mark - ACTIONS

- (IBAction)crashClicked:(UIButton *)sender {
    [[Crashlytics sharedInstance] crash];
}

- (IBAction)cashClicked:(UIButton *)sender {
    [self setCash];
}

- (IBAction)freeClicked:(UIButton *)sender {
    [self setFree];
}

- (IBAction)depositClicked:(UIButton *)sender {
    
    //    DepositViewController * viewController = [[DepositViewController alloc] init];
    //    [self presentViewController:viewController animated:NO completion:nil];
    
    
    
    
    NSString *uniqueID = [[NSUserDefaults standardUserDefaults] objectForKey:UNIQUE_SESSION_KEY];
    NSString * url = [NSString stringWithFormat:@"%@%@?client_type=ios&device_type=%@&unique_id=%@",BASE_URL,SENDPAYMENTREQUEST,[[TAJUtilities sharedUtilities] getDeviceType],uniqueID];
    
    [self openURLWithString:url];
}

-(void) yesButtonPressed:(EPopupType)type
{
    DLog(@"YES");
}

-(void) noButtonPressed:(EPopupType)type
{
    DLog(@"NO");
}

-(void) okButtonPressed:(EPopupType)type
{
    DLog(@"OK");
}

-(void) closeButtonPressed:(EPopupType)type
{
    DLog(@"CLOSE");
}

-(void) yesButtonPressedForInfoPopup:(id)object
{
    DLog(@"YES");
}

-(void) noButtonPressedForInfoPopup:(id)object
{
    DLog(@"NO");
}

-(void) okButtonPressedForInfoPopup:(id)object
{
    DLog(@"OK");
}

-(void) closeButtonPressedForInfoPopup:(id)object
{
    DLog(@"CLOSE");
}

#pragma mark - Deposit method
//- (void)closePopUp:(NSNotification *) notification
//{
//    [self performSelector:@selector(navigateToDeposit) withObject:self afterDelay:2.0 ];
//    
//    
//    NSLog(@"DEPOSIT CHECK MAIN");
////    DepositViewController * viewController = [[DepositViewController alloc] init];
////    [self presentViewController:viewController animated:NO completion:nil];
//}
//
//-(void)navigateToDeposit {
//    [self.mainScreenDelegate depositClicked];
//}

- (IBAction)buyChipsPressed:(UIButton *)sender
{
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(buyChipsClicked)])
    {
        [self.mainScreenDelegate buyChipsClicked];
    }
}

- (IBAction)playNowButtonClicked:(UIButton *)sender
{   //goto lobby screen with pools variant selected.
    if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(poolRummyClicked)])
    {
        [self.mainScreenDelegate poolRummyClicked];
    }
}

- (IBAction)funChipsPressed:(UIButton *)sender
{
    [self relaodFunChips];
}

-(void)updatedFunChips {
    //[[TAJGameEngine sharedGameEngine] updatedFunChips];
    [[TAJGameEngine sharedGameEngine] performSelector:@selector(updateFunChips) withObject:nil afterDelay:0.5];
}

- (void)addObserver {
    [self removeAllObserver];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(funChipsUpdate:)
                                                 name:FUNCHIPS_RELOAD
                                               object:nil];
}

- (void)removeAllObserver {
    [[NSNotificationCenter defaultCenter] removeObserver:self name:FUNCHIPS_RELOAD object:nil];
}

- (void)funChipsUpdate:(NSNotification *) notification
{
    //"TAJ_tourney_cost" = "CASH_CASH";
    NSMutableDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    
    NSLog(@"FUNCHIPS DATA : %@",dictionary);
}

- (void)requestAPI:(const char *)api
{
    NSString *input = [[NSString alloc] initWithUTF8String:api];
    if ([input rangeOfString:@"HEART_BEAT"].location == NSNotFound) {
        DLog(@"send data %s", api);
        //NSLog(@"string does not contain bla");
    } else {
        // NSLog(@"string contains bla!");
    }
    //DLog(@"send data %s", api);
    int count = [[TAJUtilities sharedUtilities] stringLength:api];
    NSData *data = [NSData dataWithBytes:api length:count];
    [self sendDataToServer:data];
}

- (void)sendDataToServer:(NSData*)data
{
    [[TAJClientServerCommunicator sharedClientServerCommunicator] sendData:data];
}

- (void)parseRecievedData:(NSDictionary*)dictionary {
    NSLog(@"FUN CHIPS : %@",dictionary);
}

- (void) requestBonusChunks
{
    NSString *urlAsString;
    
#if USING_LIVE_SERVER
    urlAsString = BONUS_CHUNKS_URL_LIVE;
#else
    urlAsString = BONUS_CHUNKS_URL;
    
#endif
    
    TAJUserModel *model = [[TAJGameEngine sharedGameEngine] usermodel];
    urlAsString = [NSString stringWithFormat:urlAsString, [model.userId UTF8String]];
    urlAsString = [urlAsString stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSURL *url = [NSURL URLWithString:urlAsString];
    NSMutableURLRequest *urlRequest = [NSMutableURLRequest requestWithURL:url];
    [urlRequest setTimeoutInterval:60.0f];
    [urlRequest setHTTPMethod:@"GET"];
    
    NSURLConnection * connection = [[NSURLConnection alloc] initWithRequest:urlRequest delegate:self startImmediately:NO];
    [connection scheduleInRunLoop:[NSRunLoop mainRunLoop]
                          forMode:NSDefaultRunLoopMode];
    [connection start];
    
}

#pragma mark NSURLConnection Delegate

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace
{
    return YES;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
#if USING_LIVE_SERVER
    if([challenge.protectionSpace.host isEqualToString:@"www.tajrummy.com"]/*check if this is host you trust: */)
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    NSLog(@"LIVE");
#else
    if([challenge.protectionSpace.host isEqualToString:@"tr.glserv.info"]/*check if this is host you trust: */)
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    NSLog(@"Not LIVE");

#endif
}

- (void)connection:(NSURLConnection *)connection willSendRequestForAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
#if USING_LIVE_SERVER
    if([challenge.protectionSpace.host isEqualToString:@"www.tajrummy.com"]/*check if this is host you trust: */)
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    NSLog(@"LIVE");

#else
    if([challenge.protectionSpace.host isEqualToString:@"tr.glserv.info"]/*check if this is host you trust: */)
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
    NSLog(@"Not LIVE");

#endif
    
}

- (void)connection:(NSURLConnection *)connection didCancelAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge
{
    
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data
{
    DLog(@"DATA :%@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSDictionary *bonusChunkData =  [[TAJXMLParserDictionary sharedInstance] dictionaryWithString:[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]];
    DLog(@"RESPONSE : %@", bonusChunkData);
    //NSLog(@"RESPONSE : %@", bonusChunkData);
    //NSLog(@"DATA : %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    [self updateBonusChunksBar:bonusChunkData];
}

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    NSHTTPURLResponse* httpResponse = (NSHTTPURLResponse*)response;
    int responseStatusCode = [httpResponse statusCode];
    
    switch (responseStatusCode)
    {
            case eRequestSuccessfull:
        {
            
        }
            break;
            
        default:
        {
            
        }
            break;
    }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection
{
    DLog(@"connectionDidFinishLoading");
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error
{
    DLog(@"didFailWithError");
}

- (void)updateBonusChunksBar:(NSDictionary *)bonusChunkData
{
   // NSLog(@"bonusChunkData : %@",bonusChunkData);
    
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    TAJUserModel *model = engine.usermodel;
    NSString *totalGamesPlayed = [bonusChunkData valueForKeyPath:TOTAL_GAMES_PLAYED];
    NSString *todayaGamesPlayed = [bonusChunkData valueForKeyPath:TODAY_GAMES_PLAYED];
    [model updateGamesPlayed:totalGamesPlayed playedGamesOfDay:todayaGamesPlayed];
    
    if (![TAJUtilities isIPhone])
    {
        NSArray *bonusChunkImages = [NSArray arrayWithObjects:self.bonusChunkImageView1,self.bonusChunkImageView2,self.bonusChunkImageView3,self.bonusChunkImageView4,nil];
        NSArray *dividerForChunksArray = [NSArray arrayWithObjects:self.firstChunksDivider,self.secondChunksDivider,self.thirdChunksDivider, nil];
        NSMutableArray *colorForChunksArray = [NSMutableArray array];
        NSMutableArray *currentWagerAmountArray = [NSMutableArray array];
        NSMutableArray *requiredWagerAmountArray = [NSMutableArray array];
        
        if ([[bonusChunkData valueForKeyPath:CURRENT_WAGER_LISTITEM] isKindOfClass:[NSArray class]])
        {
            currentWagerAmountArray = [bonusChunkData valueForKeyPath:CURRENT_WAGER_LISTITEM];
        }
        
        if ([[bonusChunkData valueForKeyPath:STATUS_LIST_ITEM] isKindOfClass:[NSArray class]])
        {
            requiredWagerAmountArray = [bonusChunkData valueForKeyPath:REQUIRED_WAGER_AMOUNT_LIST_ITEM];
        }
        //check whether bonus chunk current and required wager amount arrays have items.
        
        if (currentWagerAmountArray.count >0 && requiredWagerAmountArray >0)
        {
            for (int index=0; index<currentWagerAmountArray.count; index++)
            {
                NSString *currentWagerAmountOfChunk=currentWagerAmountArray[index];
                NSString *requiredWagerAmountOfChunk=requiredWagerAmountArray[index];
                
                if([currentWagerAmountOfChunk integerValue]>0 &&[currentWagerAmountOfChunk integerValue]<[requiredWagerAmountOfChunk integerValue])
                {
                    //change chunk color to orange when current wager amount is partially released.
                    [bonusChunkImages[index] setImage:[UIImage imageNamed:BONUS_CHUNK_ORANGE_IMAGE]];
                    [colorForChunksArray addObject:BONUS_CHUNK_ORANGE];
                    
                }
                else if ([currentWagerAmountOfChunk integerValue]==[requiredWagerAmountOfChunk integerValue])
                {
                    //change chunk color to green when current wager amount is fully released.
                    [bonusChunkImages[index] setImage:[UIImage imageNamed:BONUS_CHUNK_GREEN_IMAGE]];
                    [colorForChunksArray addObject:BONUS_CHUNK_GREEN];
                    
                }
                else
                {
                    //change chunk color to green when current wager amount is not released.
                    [bonusChunkImages[index] setImage:[UIImage imageNamed:BONUS_CHUNK_GRAY_IMAGE]];
                    [colorForChunksArray addObject:BONUS_CHUNK_GRAY];
                    
                }
                
            }
            //hide or show chunks divider based on adjacent chunk colors.
            for (int index=0; index < colorForChunksArray.count-1; index++)
            {
                if([colorForChunksArray[index] isEqualToString:colorForChunksArray[index+1]])
                {
                    [dividerForChunksArray[index] setHidden:false];
                    
                }
                else
                {
                    [dividerForChunksArray[index] setHidden:true];
                    
                }
            }
        }
    }
    
}

#pragma mark - CollectionView Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return  4;//[self.arrayBaners count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    CGFloat width = ([TAJUtilities screenWidth]);
#if DEBUG
   // NSLog(@"CellWidth = %f",width);
#endif
    if ([TAJUtilities isIPhone])
    {
    return CGSizeMake(width, 128);
    }
    else {
        return CGSizeMake(self.collectionView.frame.size.width, self.collectionView.frame.size.height);
    }
}
    

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    // T,L,B,R
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BannersCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifierProducts forIndexPath:indexPath];
    //[cell loadDataForCollectionView:[self.arrayCompanies objectAtIndex:indexPath.row]];
    self.pageControl.currentPage=indexPath.row;

    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
}

#pragma mark - ScrollView Delegate Methods

//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
//    for (UICollectionViewCell *cell in [self.collectionView visibleCells]) {
//        NSIndexPath *indexPath = [self.collectionView indexPathForCell:cell];
//        NSLog(@"%@",indexPath);
//        
//        self.pageControl.currentPage = indexPath.row;
//    }
//}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView
{
    int page = 0;
    
    if ([TAJUtilities isIPhone])
    {
        page = (int)(scrollView.contentOffset.x/(([TAJUtilities screenWidth])));
    }
    else {
        page = (int)(scrollView.contentOffset.x/((self.collectionView.frame.size.width)));
    }
    
    if (self.pageControl.currentPage != page) {
        self.pageControl.currentPage = page;
    }
}

-(void)setDefaultGameType{
    self.isCahsGames = NO;
    self.isFreeGames = NO;
    self.isFavouriteGames = NO;
    
    self.viewCashType.backgroundColor = [UIColor whiteColor];
    self.viewFreeType.backgroundColor = [UIColor whiteColor];
    self.viewFavouritesType.backgroundColor = [UIColor whiteColor];
    
    self.lblCashType.textColor = [UIColor blackColor];
    self.lblFreeType.textColor = [UIColor blackColor];
    self.lblFavouritesType.textColor = [UIColor blackColor];
}

-(void)setCashGames {
    [self setDefaultGameType];
    self.gameType = CASH_GAME_LOBBY;
    APP_DELEGATE.gameType = self.gameType;
    self.lblCashType.textColor = [UIColor whiteColor];
    self.viewCashType.backgroundColor = [UIColor selectedGameTypeColor];
}

-(void)setFreeGames {
    [self setDefaultGameType];
    self.gameType = FREE_GAME_LOBBY;
    APP_DELEGATE.gameType = self.gameType;
    self.lblFreeType.textColor = [UIColor whiteColor];
    self.viewFreeType.backgroundColor = [UIColor selectedGameTypeColor];
}

-(void)setFavouriteGames {
    [self setDefaultGameType];
    self.lblFavouritesType.textColor = [UIColor whiteColor];
    self.viewFavouritesType.backgroundColor = [UIColor selectedGameTypeColor];
}

- (IBAction)cashGamesClicked:(UIButton *)sender {
    [self setCashGames];
}
- (IBAction)freeGamesClicked:(UIButton *)sender {
    [self setFreeGames];
}
- (IBAction)favouritesGamesClikced:(UIButton *)sender {
    [self setFavouriteGames];
}

#pragma mark - TableView Methods

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.arrayGamesType count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([TAJUtilities isIPhone])
    {
        return kCellHeight;
    }
    else {
        return kCellHeightiPad;
    }
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    GamesTypeTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:kCellIdentifier forIndexPath:indexPath];
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    [cell configureView];
    [cell updateCellWithData:self.arrayGamesType[indexPath.row]];
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(strikesClickedOrJokerClicked)])
        {
            [self.mainScreenDelegate strikesClickedOrJokerClicked];
        }
    }
    
    else if (indexPath.row == 1) {
        if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(poolRummyClicked)])
        {
            [self.mainScreenDelegate poolRummyClicked];
        }
    }
    else if (indexPath.row == 2) {
        if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(dealsRummyClicked)])
        {
            [self.mainScreenDelegate dealsRummyClicked];
        }
    }
    else if (indexPath.row == 3) {
        
    }
    else {
        if (self.mainScreenDelegate && [self.mainScreenDelegate respondsToSelector:@selector(tournamentsRummyClicked)])
        {
            [self.mainScreenDelegate tournamentsRummyClicked];
        }
    }
}

@end
