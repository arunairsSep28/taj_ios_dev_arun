/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJMainScreenViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Modified by Lloyd on 08/06/14.
 Modified by Pradeep BM on 07/06/14.
 Created by Raghavendra Kamat on 10/04/14.
 
 **/

#import <UIKit/UIKit.h>
#import "TAJBaseViewController.h"
#import "TAJInfoPopupViewController.h"
#import "Service.h"


@class TAJHomePageViewController;
@class TAJWebViewController;
@class TAJLoginHomeViewController;
@class TAJSettingsViewController;
@class TAJProfileViewController;
@class TAJLoginViewController;
@class TAJSplashViewController;

@class TAJCarouselView;

@protocol TAJMainScreenViewControllerDelegate;

@interface TAJMainScreenViewController : TAJBaseViewController<InfoPopupViewDelegate, NSURLConnectionDelegate>
{
    
}

@property(nonatomic,strong) UITabBarController *tabsController;
@property(nonatomic,strong) TAJHomePageViewController *homePageController;
@property(nonatomic,strong) TAJWebViewController *webViewController;
@property(nonatomic,strong) TAJLoginHomeViewController *loginHomeViewController;
@property(nonatomic,strong) TAJSettingsViewController *settingsViewController;
@property(nonatomic,strong) TAJProfileViewController *profileViewController;
@property(nonatomic,strong) TAJLoginViewController *loginController;
@property(nonatomic,strong) TAJSplashViewController *splashController;
@property (nonatomic,strong) TAJInfoPopupViewController *infoController;

//delegates
@property (nonatomic, weak) id<TAJMainScreenViewControllerDelegate> mainScreenDelegate;

@property (nonatomic,strong) NSArray *arrCarouselItems;
@property (nonatomic) BOOL isScrolling;
@property (strong, nonatomic) IBOutlet UIButton *buyChips;
@property (strong, nonatomic) IBOutlet UIImageView *avatarImage;
@property (strong, nonatomic) IBOutlet UILabel *playerNameLable;
@property (strong, nonatomic) IBOutlet UILabel *realChipsValue;
@property (strong, nonatomic) IBOutlet UILabel *realInPlayValue;
@property (strong, nonatomic) IBOutlet UILabel *funChipsValue;
@property (strong, nonatomic) IBOutlet UILabel *funInPlayValue;
@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@property (strong, nonatomic) UIWindow *window;

- (IBAction)leftButtonClicked:(id)sender;
- (IBAction)rightButtonClicked:(id)sender;
- (IBAction)createNewAccount:(id)sender;
- (IBAction)logoutClicked:(id)sender;

- (void) strikesClicked:(id)sender;
- (void) poolsClicked:(id)sender;
- (void) dealsClicked:(id)sender;

- (void) setUpCarouselView;
- (void) updatePlayersInformation;
- (void) requestBonusChunks;

//Live feed
- (void)updateLiveFeedTajTables:(NSString *)noOfTable;
- (void)updateLiveFeedTajPlayer:(NSString *)noOfPlayer;

@property (strong, nonatomic) NSString * gameType;


@end

@protocol TAJMainScreenViewControllerDelegate <NSObject>

- (void)tournamentsRummyClicked;
- (void)strikesClickedOrJokerClicked;
- (void)poolRummyClicked;
- (void)dealsRummyClicked;
- (void)favouritesClicked;
- (void)buyChipsClicked;
-(void)depositClicked;


@end
