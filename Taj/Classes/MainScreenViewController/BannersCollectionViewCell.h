//
//  BannersCollectionViewCell.h
//  TajRummy
//
//  Created by Grid Logic on 03/06/20.
//  Copyright © 2020 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BannersCollectionViewCell : UICollectionViewCell

@end

NS_ASSUME_NONNULL_END
