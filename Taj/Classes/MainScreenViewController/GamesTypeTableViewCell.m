//
//  GamesTypeTableViewCell.m
//  TajRummy
//
//  Created by Grid Logic on 03/06/20.
//  Copyright © 2020 Robosoft Technologies. All rights reserved.
//

#import "GamesTypeTableViewCell.h"

@interface GamesTypeTableViewCell ()
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblSubTitle;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewLogo;

@end

@implementation GamesTypeTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)configureView {
    
    self.viewBorder.layer.borderColor = [[UIColor lightGrayColor] CGColor];
}

- (void)updateCellWithData:(NSDictionary *)gamesData
{
    self.lblTitle.text = [gamesData valueForKey:@"title"];
    self.lblSubTitle.text = [gamesData valueForKey:@"sub_title"];
    self.imgViewLogo.image = [UIImage imageNamed:[gamesData valueForKey:@"logo"]];
}

@end
