/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/02/14.
 **/

#import <Foundation/Foundation.h>
#import "TAJPlayerScoreBoardModel.h"

@interface TAJScoreSheetModel : NSObject

@property (nonatomic , strong) NSMutableDictionary *scoreSheetDictionary;

@property (nonatomic , strong) NSString            *bet;
@property (nonatomic , strong) NSString            *conversion;
@property (nonatomic , strong) NSString            *jokerFace;
@property (nonatomic , strong) NSString            *playerCount;
@property (nonatomic , strong) NSString            *jokerSuit;
@property (nonatomic , strong) NSString            *tableID;
@property (nonatomic , strong) NSString            *tableType;
@property (nonatomic , strong) NSString            *gameID;

@property (nonatomic , strong) NSMutableArray      *allPlayerListArray;

- (TAJScoreSheetModel *)initWithGameResult:(NSDictionary *)gameResult;

@end
