/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 06/02/14.
 **/

#import "TAJScoreSheetModel.h"
#import "TAJConstants.h"

@implementation TAJScoreSheetModel

@synthesize scoreSheetDictionary = _scoreSheetDictionary;
@synthesize bet                  = _bet;
@synthesize conversion           = _conversion;
@synthesize jokerFace            = _jokerFace;
@synthesize jokerSuit            = _jokerSuit;
@synthesize playerCount          = _playerCount;
@synthesize tableID              = _tableID;
@synthesize gameID = _gameID;
@synthesize tableType            = _tableType;
@synthesize allPlayerListArray   = _allPlayerListArray;

- (TAJScoreSheetModel *)initWithGameResult:(NSDictionary *)gameResult
{
    self.scoreSheetDictionary = [NSMutableDictionary dictionaryWithDictionary:gameResult];
    
    self.bet = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_bet]];
    self.conversion = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_conversion]];
    self.jokerFace = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_cardface]];
    self.jokerSuit = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_cardsuit]];
    self.tableID = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_tableid]];
    self.gameID = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_gameid]];
    self.playerCount = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_playercount]];
    self.tableType = [NSString stringWithFormat:@"%@" ,self.scoreSheetDictionary[kTaj_scoresheet_tabletype]];
    
    if ([self.scoreSheetDictionary[kTaj_scoresheet_player] isKindOfClass:[NSArray class]])
    {
        self.allPlayerListArray = [NSMutableArray arrayWithArray:self.scoreSheetDictionary[kTaj_scoresheet_player]];
    }
    else if ([self.scoreSheetDictionary[kTaj_scoresheet_player] isKindOfClass:[NSDictionary class]])
    {
        NSMutableDictionary *singlePlayerDict = [NSMutableDictionary dictionaryWithDictionary:self.scoreSheetDictionary[kTaj_scoresheet_player]];
        self.allPlayerListArray = [NSMutableArray arrayWithObjects:singlePlayerDict, nil];
    }
    
    return self;
}

- (void)updateGameResultWithData:(NSDictionary *)resultData
{
    self.allPlayerListArray = [NSMutableArray arrayWithArray:self.scoreSheetDictionary[kTaj_scoresheet_player]];
    
}

@end
