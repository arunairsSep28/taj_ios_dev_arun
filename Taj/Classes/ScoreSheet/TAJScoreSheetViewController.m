/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 05/02/14.
 **/

#import "TAJScoreSheetViewController.h"
#import "TAJScoreSheetCell.h"
#import "TAJResultSection.h"
#import "TAJRummyCard.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJCardView.h"
#import "TAJGameJoker.h"

#define RESULT_CARD_OFFSET_X_IPAD             28.0f
#define RESULT_CARD_OFFSET_X_IPHONE_5         13.0f
#define RESULT_CARD_OFFSET_X_IPHONE_4         8.0f

@interface TAJScoreSheetViewController ()<UITableViewDataSource, UITableViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel      *scoreSheetTimer;
@property (weak, nonatomic) IBOutlet UITableView  *tableScoreSheet;
@property (weak, nonatomic) IBOutlet UIButton     *closeButton;
@property (weak, nonatomic) IBOutlet UIImageView  *jokerImageView;
@property (weak, nonatomic) IBOutlet UIView *jokerImageHolderView;
@property (weak, nonatomic) IBOutlet UILabel *lblGameID;
@property (weak, nonatomic) IBOutlet UILabel *lblTableID;

//@property (weak, nonatomic) IBOutlet UIView *splitContainerView;

- (IBAction)closeButtonClicks:(UIButton *)sender;
- (IBAction)splitButtonAction:(UIButton *)sender;

//for timer
@property (nonatomic, strong) NSTimer                   *gameRestartTimer;
@property (nonatomic) double                            restartTime;
@property (nonatomic, strong) NSMutableDictionary       *restartGameDictionay;
@property (nonatomic)       BOOL                        isDealRummyGame;
@property (nonatomic)       BOOL                        isJokerGame;

// pre game result timer
@property (nonatomic, strong)   NSTimer             *preGameNSTimer;
@property (nonatomic) double                        preGameTime;

@property(strong, nonatomic) NSMutableArray * playersStatus;
@property(assign, nonatomic) int timeOutCont;
@property(assign, nonatomic) int dropOutCont;
@property(assign, nonatomic) int wrongCont;
@property(assign, nonatomic) int winnerCont;
@property(assign, nonatomic) int lostCount;


@property (weak, nonatomic) IBOutlet UILabel *versionLabel;

@end

@implementation TAJScoreSheetViewController

#pragma mark
#pragma mark UIViewController Life Cycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self)
    {
        self.restartTime = 0.0f;
        
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.isDealRummyGame = NO;
    self.isJokerGame = NO;
    
    if ([self respondsToSelector:@selector(setEdgesForExtendedLayout:)])
    {
        // if iOS 7
        self.edgesForExtendedLayout = UIRectEdgeNone; //layout adjustements
    }
	//[_tableScoreSheet setSeparatorColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"game_result_devider01.png"]]];
    self.splitContainerView.userInteractionEnabled = YES;
    [self.view bringSubviewToFront:self.self.splitContainerView];
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    self.versionLabel.text = [NSString stringWithFormat:@"Version : %@",version];
    
    self.scoreSheetContentView.layer.borderColor = [UIColor whiteColor].CGColor;
    self.scoreSheetContentView.layer.borderWidth = 2.0f;
    self.scoreSheetContentView.layer.cornerRadius = 8.0f;
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    self.splitContainerView.userInteractionEnabled = YES;
    
    self.isDealRummyGame = [self conditionForDealRummyScoreSheetTimer];
    self.isJokerGame = [self isJokerGameTable];
    
    //GameID & TableID
    self.lblTableID.text = self.scoreSheet.tableID;
    self.lblGameID.text = self.scoreSheet.gameID;
    
    // check any holder subviewview exist
    TAJGameJoker *jokerView = nil;
    if (self.jokerImageHolderView.subviews && self.jokerImageHolderView.subviews.count > 0)
    {
        // Already added
        NSArray *subviews = [self.jokerImageHolderView subviews];
        jokerView = (TAJGameJoker *)subviews[0];
    }
    else
    {
        NSArray *nibContents;
        
        int index = 0;
        
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJGameJokerNibName //change the nib name
                                                    owner:nil
                                                  options:nil];
        jokerView = nibContents[index];
        [self.jokerImageHolderView addSubview:jokerView];
    }
    
    if([TAJUtilities isIPhone])
    {
        if([self.scoreSheet.jokerFace isEqual:@"None"] && [self.scoreSheet.jokerSuit isEqual:@"None"])
        {
            // self.jokerImageView.image = [UIImage imageNamed:@"jocker_no0JO@2x~ipad.png"];
            //self.jokerImageView.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.7f, 0.7f);
            [jokerView showJokerCardForCardID:nil];
        }
        else
        {
            
            NSString *jokerImageName = [NSString stringWithFormat:@"%@%@",self.scoreSheet.jokerFace,[self.scoreSheet.jokerSuit uppercaseString]];
            [jokerView showJokerCardForCardID:jokerImageName];
            
        }
        
    }
    else
    {
        if([self.scoreSheet.jokerFace isEqual:@"None"] && [self.scoreSheet.jokerSuit isEqual:@"None"])
        {
            //self.jokerImageView.image = [UIImage imageNamed:@"jocker_no0JO@2x~ipad.png"];
            //self.jokerImageView.transform=CGAffineTransformScale(CGAffineTransformIdentity, 0.9f, 0.9f);
            
            [jokerView showJokerCardForCardID:nil];
            
            
        }
        else
        {
            NSString *jokerImageName = [NSString stringWithFormat:@"%@%@",self.scoreSheet.jokerFace,[self.scoreSheet.jokerSuit uppercaseString]];
            [jokerView showJokerCardForCardID:jokerImageName];
            
            
        }
        
        
    }
    // Restart timer to schedule new game
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(scheduleGameAfterResult:)
                                                 name:GAME_SCHEDULE
                                               object:nil];
    // Restart timer to schedule new game
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(resetGameResultTimer:)
                                                 name:RESET_TIMER_NOTIFICATION
                                               object:nil];
    self.playersStatus = [NSMutableArray new];
    
    if (self.scoreSheet.allPlayerListArray.count > 0) {
        
        for (int i = 0 ; i < [self.scoreSheet.allPlayerListArray count ] ; i++) {
            NSString * playerStatus = [self.scoreSheet.allPlayerListArray[i] objectForKey:@"TAJ_result"];
            [self.playersStatus addObject:playerStatus];
            
        }
        
        self.timeOutCont = 0;
        self.dropOutCont = 0;
        self.wrongCont = 0;
        self.winnerCont = 0;
        self.lostCount = 0;
        
        for(NSString *string in self.playersStatus){
            self.timeOutCont += ([string isEqualToString:@"timeout"]?1:0); //certain object is @"drop"
            self.dropOutCont += ([string isEqualToString:@"drop"]?1:0);
            self.winnerCont += ([string isEqualToString:@"winner"]?1:0);
            self.wrongCont += ([string isEqualToString:@"eliminate"]?1:0);
            self.lostCount += ([string isEqualToString:@"meld_timeout"] ||  [string isEqualToString:@"meld"] || [string isEqualToString:@"table_leave"]?1:0);
        }
//        NSLog(@"Checking  number of timeOutCont %d", self.timeOutCont);
//        NSLog(@"Checking  number of dropOutCont %d", self.dropOutCont);
//        NSLog(@"Checking  number of wrongCont %d", self.wrongCont);
//        NSLog(@"Checking  number of winnerCont %d", self.winnerCont);
//        NSLog(@"Checking  number of lostCount %d", self.lostCount);
        
    }
    
    [self.tableScoreSheet reloadData];
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:GAME_SCHEDULE object:nil];
}

-(void)viewDidUnload
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:RESET_TIMER_NOTIFICATION object:nil];
    
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

- (BOOL)isDataExist
{
    if ([self.scoreSheet.allPlayerListArray count] > 0)
    {
        return YES;
    }
    return NO;
}

- (void)reloadScoreSheet
{
    [self.tableScoreSheet reloadData];
    
}

- (void)removeAllScoreSheetData
{
    [self.scoreSheet.allPlayerListArray removeAllObjects];
}

- (BOOL)isShowing
{
    if (self.view.superview)
    {
        return YES;
    }
    return NO;
}

#pragma mark - Notification observer -

- (void)resetGameResultTimer:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary)
    {
        if ([self.gameRestartTimer isValid])
        {
            [self.gameRestartTimer invalidate];
            self.gameRestartTimer = nil;
        }
        self.scoreSheetTimer.text = @"";
        self.restartTime = 0.0f;
    }
}

- (void)scheduleGameAfterResult:(NSNotification *)notification
{
    NSDictionary *dictionary = nil;
    if (notification)
    {
        dictionary = [notification object];
    }
    if (dictionary && [dictionary[TAJ_TABLE_ID] isEqualToString:self.scoreSheet.tableID])
    {
        double currentTime = [dictionary[GAME_TIMER_STAMP] doubleValue];
        double endTime = [dictionary[GAME_START_TIMER] doubleValue];
        double duration = endTime - currentTime;
        [self timerToLeaveScoreSheet:duration andData:dictionary];
    }
}

- (void)resetGameResultOnDisconnection
{
    if ([self.gameRestartTimer isValid])
    {
        [self.gameRestartTimer invalidate];
        self.gameRestartTimer = nil;
    }
    self.scoreSheetTimer.text = @"";
    self.restartTime = 0.0f;
}

- (void)timerToLeaveScoreSheet:(double)duration andData:(NSDictionary *)dictionary
{
    self.restartTime = duration;
    self.gameRestartTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                           selector:@selector(scheduleRestartGame:)
                                                           userInfo:dictionary
                                                            repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
    //Author : RK Note : If slows down the app add the timer to runloop
    //[[NSRunLoop mainRunLoop] addTimer:self.gameRestartTimer forMode:NSRunLoopCommonModes];
#endif
}

- (void)scheduleRestartGame:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for scheduleRestartGame ");
#endif
    
    --self.restartTime;
    if (self.restartTime == 11)
    {
        // if its not a best 2, 6 game you neeed to dismiss score sheet when it was 11
        [self removeScoreSheet];
    }
    else if (self.restartTime == 20 && self.isDealRummyGame)
    {
        // for best of 2 and best of 6 game you need to dismiss score sheet when it was 20
        [self removeScoreSheet];
    }
    
    else if (self.restartTime <= 0)
    {
        [self.gameRestartTimer invalidate];
        self.scoreSheetTimer.text = @"";
    }
    else
    {
        self.restartGameDictionay = [timer userInfo];
        // Show time for restart game
        self.scoreSheetTimer.text = [NSString stringWithFormat:kTaj_scoresheet_restart_msg, (int)self.restartTime];
        
    }
}

- (BOOL)conditionForDealRummyScoreSheetTimer
{
    if (([self.scoreSheet.tableType isEqualToString:kBEST_OF_2] == NSOrderedSame) ||
        ([self.scoreSheet.tableType isEqualToString:kBEST_OF_6] == NSOrderedSame ) )
    {
        return YES;
    }
    return NO;
}

- (BOOL)isJokerGameTable
{
    if ([self.scoreSheet.tableType isEqualToString:kPR_JOKER]
        ||[self.scoreSheet.tableType isEqualToString:kPR_NOJOKER])
    {
        return YES;
    }
    return NO;
}

- (void)runPreGameResultTimer:(NSDictionary *)dictionary
{
    if (dictionary)
    {
        if ([self.preGameNSTimer isValid])
        {
            [self.preGameNSTimer invalidate];
        }
        
        self.preGameTime = [dictionary[kTaj_playershow_timeout] floatValue];
        
        self.preGameNSTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self
                                                             selector:@selector(updatePreGameResultTimer:)
                                                             userInfo:dictionary repeats:YES];
#if DONT_USE_RUNLOOPCOMMONMODE
#else
        [[NSRunLoop mainRunLoop] addTimer:self.preGameNSTimer forMode:NSRunLoopCommonModes];
#endif
        
    }
}

- (void)updatePreGameResultTimer:(NSTimer *)timer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for updatePreGameResultTimer ");
#endif
    
    --self.preGameTime;
    if (self.preGameTime <= 0)
    {
        [self.preGameNSTimer invalidate];
        self.scoreSheetTimer.text = @"";
    }
    else
    {
        // Show time for other player meld timer
        self.scoreSheetTimer.text = [NSString stringWithFormat:kTaj_scoresheet_pregame_msg, (int)self.preGameTime];
    }
}

#pragma mark - UITableView Delegate Datasource -

- (NSInteger)tableView:(UITableView *)tableView
 numberOfRowsInSection:(NSInteger)section
{
    return [self.scoreSheet.allPlayerListArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([TAJUtilities isIPhone])
    {
        if([TAJUtilities isItIPhone5])
        {
            return 34;
        }
        return 33;
    }
    return 68;
}

- (UITableViewCell *)tableView:(UITableView *)tableView
         cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *identifier = @"ScoreSheetCellID";
    
    TAJScoreSheetCell *cell = (TAJScoreSheetCell*) [tableView dequeueReusableCellWithIdentifier:identifier];
    
    for (int i = 0; i < [cell.contentView.subviews count]; i++)
    {
        [[cell.contentView.subviews objectAtIndex:i] removeFromSuperview];
    }
    
    if (!cell)
    {
        NSArray *array = Nil;
        array = [[NSBundle mainBundle] loadNibNamed:TAJScoreSheetCellNibName owner:self options:nil];
        cell = array[0];
    }
    
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    
    [cell loadCellData:[self.scoreSheet.allPlayerListArray objectAtIndex:indexPath.row] withJokerNumber:self.scoreSheet.jokerFace isJokerGame:self.isJokerGame withConvertion:self.scoreSheet.conversion];
    cell.backgroundColor=[UIColor clearColor];
    
    if (self.scoreSheet.allPlayerListArray.count == 2) {
        //NSLog(@"PLAYERS COUNT : %lu",(unsigned long)self.scoreSheet.allPlayerListArray.count);
        
        //NSLog(@"STATUS DATA : %@",self.playersStatus);
        if ([self.playersStatus containsObject:@"drop"] || [self.playersStatus containsObject:@"timeout"]) {
            cell.resultScrollView.hidden = YES;
        }

        else {
            
            if ([self.playersStatus containsObject:@"eliminate"]) {

                if ([[self.scoreSheet.allPlayerListArray[indexPath.row] objectForKey:@"TAJ_result"] isEqualToString:@"winner"]) {
                    cell.resultScrollView.hidden = YES;
                }
                else {
                    cell.resultScrollView.hidden = NO;
                }
            }
        }
    
    }
    else {

        if ([[self.scoreSheet.allPlayerListArray[indexPath.row] objectForKey:@"TAJ_result"] isEqualToString:@"drop"] || [[self.scoreSheet.allPlayerListArray[indexPath.row] objectForKey:@"TAJ_result"] isEqualToString:@"timeout"] ) {
            cell.resultScrollView.hidden = YES;
        }
        int total = 0;
        total = self.timeOutCont + self.dropOutCont;
        //NSLog(@"CHECKING TOTAL %d",total);
        //NSLog(@"CHECKING TOTAL %lu",(unsigned long)self.scoreSheet.allPlayerListArray.count);
        
        if (([[self.scoreSheet.allPlayerListArray[indexPath.row] objectForKey:@"TAJ_result"] isEqualToString:@"winner"])) {
            cell.contentView.backgroundColor = [UIColor blackColor];
            if (total == self.scoreSheet.allPlayerListArray.count - 1) {
                cell.resultScrollView.hidden = YES;
            }
            else if (self.lostCount == 0 && self.wrongCont > 0) {
                cell.resultScrollView.hidden = YES;
            }
            else {
                cell.resultScrollView.hidden = NO;
            }
        }
        NSLog(@"CHECK WINNER : %@",[self.scoreSheet.allPlayerListArray[indexPath.row] objectForKey:@"TAJ_result"]);
        
        if (([[self.scoreSheet.allPlayerListArray[indexPath.row] objectForKey:@"TAJ_result"] isEqualToString:@"winner"])) {
            cell.viewBg.backgroundColor = [UIColor blackColor];
        }
        else {
            cell.viewBg.backgroundColor = [UIColor whiteColor];
        }
    }
    
    return cell;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    if ([TAJUtilities isIPhone])
    {
        if ([TAJUtilities isItIPhone5])
        {
            return 27.0f;
        }
        else
        {
            return 26.0f;
        }
    }
    
    return 48.0f;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSArray *nibContents = [NSArray array];
    // Header view
    int index = 0;
    
    nibContents = [[NSBundle mainBundle] loadNibNamed:TAJResultSectionNibName
                                                owner:nil
                                              options:nil];
    
    TAJResultSection *headerSection;
    headerSection = nibContents[index];
    
    if (self.isJokerGame)
    {
        [headerSection jokerHeaderView];
    }
    
    return headerSection;
}

#pragma mark - Close button action -

- (IBAction)closeButtonClicks:(UIButton *)sender
{
    [self removeScoreSheet];
}


- (void)removeScoreSheet
{
    if ([self.scoreSheetDelegate respondsToSelector:@selector(didScoreSheetClosed:withTime:)])
    {
        [self.scoreSheetDelegate didScoreSheetClosed:self.restartGameDictionay withTime:self.restartTime];
    }
}

#pragma mark - Split button action

- (IBAction)checkingAction:(UIButton *)sender {
    NSLog(@"Checking SPLIT CLICKED");
    self.splitContainerView.hidden = YES;
    self.viewGameDetails.hidden = NO;
    
    [self didSplitRequested];
}

- (IBAction)splitButtonAction:(UIButton *)sender
{
    NSLog(@"SPLIT CLICKED");
    self.splitContainerView.hidden = YES;
    self.viewGameDetails.hidden = NO;
    
    [self didSplitRequested];
}

- (void)didSplitRequested
{
    NSLog(@"SPLIT REQUESTED");
    if ([self.scoreSheetDelegate respondsToSelector:@selector(didSplitRequested:withTime:)])
    {
        [self.scoreSheetDelegate didSplitRequested:self.restartGameDictionay withTime:self.restartTime];
    }
}

- (void)showSplitButton
{
    self.splitContainerView.hidden = NO;
    self.viewGameDetails.hidden = YES;
    self.splitContainerView.userInteractionEnabled = YES;
   // [self.view addSubview:self.splitContainerView];
}

- (void)hideSplitButton
{
    self.splitContainerView.hidden = YES;
    self.viewGameDetails.hidden = NO;
}


@end
