/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 07/02/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJScoreSheetCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel            *playerName;
@property (strong, nonatomic) IBOutlet UILabel            *playerStatus;
@property (strong, nonatomic) IBOutlet UILabel            *playerCount;
@property (strong, nonatomic) IBOutlet UILabel            *playerTotalCount;
@property (strong, nonatomic) IBOutlet UIView             *gameOverCardView;

@property (weak, nonatomic) IBOutlet UIScrollView *resultScrollView;
@property (weak, nonatomic) IBOutlet UIView *viewBg;


- (TAJScoreSheetCell *)init;
- (void)loadCellData:(NSDictionary *)dictionary withJokerNumber:(NSString *)jokerNumber isJokerGame:(BOOL)jokerGame withConvertion:(NSString *)converstion;

@end
