/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetCell.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 07/02/14.
 **/

#import "TAJScoreSheetCell.h"
#import "TAJPlayerScoreBoardModel.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJCardView.h"
#import "TAJScoreSheetCard.h"

#define RESULT_CARD_OFFSET_X_IPAD             18.0f
#define RESULT_CARD_OFFSET_X_IPHONE_5         10.0f
#define RESULT_CARD_OFFSET_X_IPHONE_4         10.0f

@interface TAJScoreSheetCell ()<TAJCardViewProtocol,UIScrollViewDelegate, TAJScoreSheetCardProtocol>

//activity indicator
@property (weak, nonatomic) IBOutlet UILabel *waitingTextLabel;
@property (weak, nonatomic) IBOutlet UIActivityIndicatorView *totalCountActivity;
//@property (weak, nonatomic) IBOutlet UIScrollView *resultScrollView;
@property (weak, nonatomic) IBOutlet UIImageView *aiImageView;
@property (weak, nonatomic) IBOutlet UIImageView *winnerImageView;
@property (nonatomic, strong) NSString                  *jokerNumber;
//player score board for representing points in score board
@property (strong, nonatomic) TAJPlayerScoreBoardModel *playerPointModel;
@property (nonatomic) CGRect rectForGameOverHolderView;
@property CGRect aiImageViewFrame;
@property CGRect statusLabelFrame;

@end

@implementation TAJScoreSheetCell

@synthesize playerPointModel   = _playerPointModel;
@synthesize playerName         = _playerName;
@synthesize playerStatus       = _playerStatus;
@synthesize playerCount        = _playerCount;
@synthesize playerTotalCount   = _playerTotalCount;
@synthesize gameOverCardView   = _gameOverCardView;
@synthesize totalCountActivity = _totalCountActivity;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self)
    {
        
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];
}

-(TAJScoreSheetCell *)init
{
    self.rectForGameOverHolderView = self.gameOverCardView.frame;
    self.aiImageViewFrame = self.aiImageView.frame;
    self.statusLabelFrame = self.playerStatus.frame;
    
    return self;
}

#pragma mark - Load cell data -

- (void)loadCellData:(NSDictionary *)dictionary withJokerNumber:(NSString *)jokerNumber isJokerGame:(BOOL)jokerGame withConvertion:(NSString *)converstion
{
    NSLog(@"ScoreSheetDictionary = %@",dictionary);
    DLog(@"ScoreSheetDictionary = %@",dictionary);
    
    NSString * result = dictionary[kTaj_scoresheet_result];
    
    if ([result isEqualToString:@"winner"]) {
        self.viewBg.backgroundColor = [UIColor blackColor];
    }
    else {
        self.viewBg.backgroundColor = [UIColor whiteColor];
    }
    //update player name
    self.playerName.text = [dictionary[kTaj_scoresheet_nickname] lowercaseString];
    // [self.playerName adjustsFontSizeToFitWidth];
    self.jokerNumber = jokerNumber;
    if (![dictionary[kTaj_scoresheet_result] isEqualToString:@"None"])
    {
        // if data is exist update
        // update rest of the remaining cells
        [self updateOtherCell:dictionary isJokerGame:jokerGame withConversion:converstion];
    }
    else
    {
        //show activity indicator in cell
        self.waitingTextLabel.hidden = NO;
        [self.totalCountActivity startAnimating];
    }
    CGRect frame=self.resultScrollView.frame;
    //NSLog(@"result card 1  %f,result card 2 %f",self.gameOverCardView.frame.size.width,self.resultScrollView.contentSize.width);
    //lingam
    frame.origin.x=self.gameOverCardView.frame.origin.x + (self.gameOverCardView.frame.size.width / 2)-(self.resultScrollView.contentSize.width / 2);//self.gameOverCardView.frame.origin.x;
    frame.origin.y=self.gameOverCardView.frame.origin.y;
    self.resultScrollView.frame=frame;
    [self addSubview:self.resultScrollView];
}

- (void)updateOtherCell:(NSDictionary *)dictionary isJokerGame:(BOOL)jokerGame withConversion:(NSString *)conversion
{
    //update player score result
    self.waitingTextLabel.hidden = YES;
    [self.totalCountActivity stopAnimating];
    self.winnerImageView.hidden = YES;
    self.playerStatus.hidden = NO;
    self.aiImageView.highlighted = NO;
    if ([dictionary[kTaj_scoresheet_result] isEqualToString:@"meld_timeout"]
        || [dictionary[kTaj_scoresheet_result] isEqualToString:@"table_leave"]
        || [dictionary[kTaj_scoresheet_result] isEqualToString:@"meld"])
    {
        self.playerStatus.text = kTaj_scoresheet_result_modified;
        if([dictionary[@"TAJ_aienable"] isEqualToString:@"True"])
        {
            self.aiImageView.hidden = NO;
        }
        else
        {
            self.aiImageView.hidden = YES;
        }
    }
    else if ([dictionary[kTaj_scoresheet_result] isEqualToString:@"eliminate"]) {
        self.playerStatus.text = @"Wrong show";
        
    }
    else if ([dictionary[kTaj_scoresheet_result] isEqualToString:@"drop"] || [dictionary[kTaj_scoresheet_result] isEqualToString:@"timeout"])
    {
        self.playerStatus.text = @"Dropped";
        if([dictionary[@"TAJ_aienable"] isEqualToString:@"True"])
        {
            self.aiImageView.hidden = NO;
            
        }
        else
        {
            self.aiImageView.hidden = YES;
        }
    }
    else
    {
        self.winnerImageView.hidden = NO;
        self.playerStatus.hidden = YES;
        //winner to Winner
        NSString *firstCapChar = [[dictionary[kTaj_scoresheet_result] substringToIndex:1] capitalizedString];
        NSString *cappedString = [dictionary[kTaj_scoresheet_result] stringByReplacingCharactersInRange:NSMakeRange(0,1) withString:firstCapChar];
        self.playerStatus.text = cappedString;
        if([dictionary[@"TAJ_aienable"] isEqualToString:@"True"])
        {
            self.aiImageView.hidden = NO;
        }
        else
        {
            self.aiImageView.hidden = YES;
        }
        if([dictionary[@"TAJ_sc_use"] isEqualToString:@"True"])
        {
            self.aiImageView.hidden = NO;
            self.aiImageView.highlighted = YES;
        }
    }
    
    //get player cards
    NSMutableDictionary *playerMeldCard = [NSMutableDictionary dictionaryWithDictionary:dictionary[kTaj_scoresheet_eachplayer]];
    [self checkIsCardGroupedAndFill:playerMeldCard];
    
    //update player count data
    self.playerCount.text = dictionary[kTaj_scoresheet_points];
    
    //update player totalcount data
    // you need to display the score for PR-JOKER and PR_NOJOKER
    [self updateFinalTotalScore:dictionary isJokerGame:jokerGame withConverstion:conversion];
}

- (void)updateFinalTotalScore:(NSDictionary *)dictionary isJokerGame:(BOOL)boolValue withConverstion:(NSString *)converstion
{
    if (!boolValue)
    {
        self.playerTotalCount.text = dictionary[kTaj_scoresheet_totalscore];
        self.playerTotalCount.text = [NSString stringWithFormat:@" %g",[self.playerTotalCount.text floatValue]];
    }
    else
    {
        NSString *conversion = converstion;
        conversion = [conversion substringFromIndex:([conversion length]-1)];
        int conversionValue = 1;
        conversionValue = [conversion intValue];
        
        NSString *score = Nil;
        score = dictionary[kTaj_scoresheet_score];
        float scoreValue = [score floatValue];
        
        float finalValue = scoreValue * conversionValue;
        //int finalIntValue = finalValue;
        
        if ([dictionary[kTaj_scoresheet_result] isEqualToString:@"winner"])
        {
            NSString *positivePointsValue = [NSString stringWithFormat:@"+%g",finalValue];
            self.playerTotalCount.text = positivePointsValue;
        }
        else
        {
            NSString *minusPointsValue = [NSString stringWithFormat:@"-%@",dictionary[kTaj_scoresheet_score]];
            self.playerTotalCount.text = minusPointsValue;
        }
    }
    
    // set color for winner only
    if ([dictionary[kTaj_scoresheet_result] isEqualToString:@"winner"])
    {
//        self.playerStatus.textColor = [UIColor colorWithRed:251.0/255.0 green:176.0/255.0 blue:59/255.0 alpha:1];
//        self.playerName.textColor = [UIColor colorWithRed:251.0/255.0 green:176.0/255.0 blue:59/255.0 alpha:1];
        self.playerStatus.textColor = [UIColor whiteColor];
        self.playerName.textColor = [UIColor whiteColor];
        self.playerCount.textColor = [UIColor whiteColor];
        self.playerTotalCount.textColor = [UIColor whiteColor];
    }
}

#pragma mark - Fill cards in cell -

- (void)checkIsCardGroupedAndFill:(NSMutableDictionary *)playerMeldCard
{
    if (playerMeldCard[kTaj_scoresheet_box] &&
        [playerMeldCard[kTaj_scoresheet_box] isKindOfClass:[NSDictionary class]])
    {
        //ungrouped cards
        NSMutableArray *ungroupedPlayerCards = [NSMutableArray arrayWithArray:[playerMeldCard
                                                                               valueForKeyPath:kTaj_scoresheet_card]];
        
        [self fillCardsWithCardArray:ungroupedPlayerCards withCardOffset:0 andIsGrouped:NO];
        self.resultScrollView.delegate = self;
        
        float width;
        if ([TAJUtilities isIPhone]){
            width = (RESULT_CARD_OFFSET_X_IPHONE_4 * (ungroupedPlayerCards.count - 6));
        }else{
            width = (RESULT_CARD_OFFSET_X_IPAD * (ungroupedPlayerCards.count + 2));
        }
        
        self.resultScrollView.contentSize= CGSizeMake(width, self.resultScrollView.frame.size.height);
        self.resultScrollView.userInteractionEnabled=NO;
        self.resultScrollView.scrollEnabled=NO;
        
    }
    
    else if (playerMeldCard[kTaj_scoresheet_box] &&
             [playerMeldCard[kTaj_scoresheet_box] isKindOfClass:[NSArray class]])
    {
        //grouped cards
        NSMutableArray *boxArray = [NSMutableArray arrayWithArray:playerMeldCard[kTaj_scoresheet_box]];
        float cardXoffset = 0;
        for (int i = 0; i<boxArray.count; i++)
        {
            if ([boxArray[i] valueForKey:@"card"] &&  [[boxArray[i] valueForKey:@"card"] isKindOfClass:[NSArray class]])
            {
                NSMutableArray *cardArray = [boxArray[i] valueForKey:@"card"];
                [self fillCardsWithCardArray:cardArray withCardOffset:cardXoffset andIsGrouped:YES];
                //[self fillCardsWithCardArray:cardArray withCardOffset:cardXoffset];
                cardXoffset += cardArray.count + 0.2;
                
            }
            else if ([boxArray[i] valueForKey:@"card"] && [[boxArray[i] valueForKey:@"card"] isKindOfClass:[NSDictionary class]])
            {
                // if any player, single card is grouped he will send format in NSDictionary.
                NSMutableDictionary *cardDict = [boxArray[i] valueForKey:@"card"];
                NSMutableArray *cardArray = [NSMutableArray arrayWithObjects:cardDict, nil];
                [self fillCardsWithCardArray:cardArray withCardOffset:cardXoffset andIsGrouped:YES];
                cardXoffset += cardArray.count + 0.2;
            }
            
            cardXoffset += 1.0;
        }
        self.resultScrollView.delegate = self;
        
        float width;
        if ([TAJUtilities isIPhone]){
            width = (RESULT_CARD_OFFSET_X_IPHONE_4 * 7) + ((boxArray.count - 1) * RESULT_CARD_OFFSET_X_IPHONE_4);
        }else{
            width = (RESULT_CARD_OFFSET_X_IPAD * 15) + ((boxArray.count - 1) * RESULT_CARD_OFFSET_X_IPAD);
            //  width = (RESULT_CARD_OFFSET_X_IPAD * boxArray.count) + RESULT_CARD_OFFSET_X_IPAD;
        }
        
        //self.resultScrollView.contentSize= CGSizeMake(self.resultScrollView.frame.size.width*boxArray.count/2, self.resultScrollView.frame.size.height);
        
        self.resultScrollView.contentSize= CGSizeMake(width, self.resultScrollView.frame.size.height);
        
        
        self.resultScrollView.userInteractionEnabled=NO;
        self.resultScrollView.scrollEnabled=NO;
        
    }
    
}

- (void)fillCardsWithCardArray:(NSMutableArray *)array withCardOffset:(float)xOffset andIsGrouped:(BOOL) isGrouped
{
    for (int i = 0; i < array.count; i++)
    {
        NSDictionary *cardInfo = array[i];
        NSString *cardNum = cardInfo[kTaj_Card_Face_Key];
        NSString *cardType = [cardInfo[kTaj_Card_Suit_Key] uppercaseString];
        NSString *cardId = [NSString stringWithFormat:@"%@%@", cardNum, cardType];
        
        NSArray *nibContents = [NSArray array];
        // Place new card
        int index = 0;
        
        if ([TAJUtilities isIPhone])
        {
            index = [TAJUtilities isItIPhone5]? 0 : 1;
        }
        nibContents = [[NSBundle mainBundle] loadNibNamed:TAJScoreSheetCardViewNibName  owner:nil  options:nil];
        
        TAJScoreSheetCard *card = nibContents[index];
        card.scoreCardDelegate = self;
        
        //yogish: set frame
        
        [card showScoreSheetCard:cardId];
        
        if(isGrouped == YES)
        {
            [self placeCardInView:card onMyDeckAtIndex:(i + xOffset)];
        }
        if(isGrouped == NO)
        {
            [self placeCardInView:card onMyDeckAtIndex:i];
        }
    }
}

#pragma mark - keep card in view -

- (void)placeCardInViewWithView:(TAJScoreSheetCard *)card onMyDeckAtIndex:(NSUInteger)index
{
    CGRect holderFrame = self.resultScrollView.frame;
    CGRect cardFrame = card.frame;
    
    if ([TAJUtilities isIPhone])
    {
        cardFrame.origin.x = (index + 1) * ([TAJUtilities isItIPhone5] ? RESULT_CARD_OFFSET_X_IPHONE_5 : RESULT_CARD_OFFSET_X_IPHONE_4);
        
        if([TAJUtilities isItIPhone5])
        {
            cardFrame.size.width = 19;
            cardFrame.size.height = 28;
        }
        else
        {
            cardFrame.size.width = 19;
            cardFrame.size.height = 28;
        }
    }
    else
    {
        cardFrame.origin.x = (index + 1) * RESULT_CARD_OFFSET_X_IPAD;
        cardFrame.size.width = 36;
        cardFrame.size.height = 60;
        
    }
    cardFrame.origin.y = holderFrame.origin.y;
    card.frame = cardFrame;
    card.userInteractionEnabled = NO;
    
    [self.resultScrollView addSubview:card];
}

- (void)placeCardInView:(TAJScoreSheetCard *)card onMyDeckAtIndex:(NSUInteger)index
{
    CGRect holderFrame = self.resultScrollView.frame;
    CGRect cardFrame = card.frame;
    
    if ([TAJUtilities isIPhone])
    {
        cardFrame.origin.x = (index + 1) * ([TAJUtilities isItIPhone5] ? RESULT_CARD_OFFSET_X_IPHONE_5  : RESULT_CARD_OFFSET_X_IPHONE_4);
        
        if([TAJUtilities isItIPhone5])
        {
            cardFrame.size.width = 19;
            cardFrame.size.height = 28;
        }
        else
        {
            cardFrame.size.width = 19;
            cardFrame.size.height = 28;
        }
    }
    else
    {
        if(index == 0)
        {
            cardFrame.origin.x = holderFrame.origin.x+20;
        }
        else
        {
            cardFrame.origin.x = (index + 1) * RESULT_CARD_OFFSET_X_IPAD;
        }
        cardFrame.size.width = 36;
        cardFrame.size.height = 60;
        
    }
    cardFrame.origin.y = holderFrame.origin.y + 1;
    
    card.frame = cardFrame;
    card.userInteractionEnabled = NO;
    
    [self.resultScrollView addSubview:card];
}

#pragma mark -Card View Delegate-

- (NSString *)getJokerCardNumber
{
    if (self.jokerNumber)
    {
        return self.jokerNumber;
    }
    return nil;
}

- (NSString *)getScoreSheetJoker
{
    if (self.jokerNumber)
    {
        return self.jokerNumber;
    }
    return nil;
}

@end
