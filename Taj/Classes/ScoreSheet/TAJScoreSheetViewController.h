/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 05/02/14.
 **/

#import <UIKit/UIKit.h>
#import "TAJScoreSheetModel.h"

@protocol TAJScoreSheetViewControllerDelegate;

@interface TAJScoreSheetViewController : UIViewController

@property (weak, nonatomic) id<TAJScoreSheetViewControllerDelegate> scoreSheetDelegate;

@property (weak, nonatomic) IBOutlet UIView *scoreSheetContentView;
@property (weak, nonatomic) IBOutlet UIView *splitContainerView;
@property (weak, nonatomic) IBOutlet UIView *viewGameDetails;

@property (strong, nonatomic) TAJScoreSheetModel *scoreSheet;
@property (weak, nonatomic) IBOutlet UIImageView *winner_AIImageView;

- (void)reloadScoreSheet;
- (void)removeAllScoreSheetData;
- (BOOL)isShowing;
- (BOOL)isDataExist;
- (void)showSplitButton;
- (void)hideSplitButton;
- (void)runPreGameResultTimer:(NSDictionary *)dictionary;
- (void)resetGameResultOnDisconnection;

@end

@protocol TAJScoreSheetViewControllerDelegate <NSObject>

- (void)didScoreSheetClosed:(NSDictionary *)dictionary withTime:(double)timeLeft;
- (void)didSplitRequested:(NSDictionary *)dictionary withTime:(double)time;

@end



