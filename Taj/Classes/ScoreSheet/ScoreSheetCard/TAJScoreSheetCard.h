/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetCard.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 07/06/14.
 **/

#import <UIKit/UIKit.h>

@protocol TAJScoreSheetCardProtocol;

@interface TAJScoreSheetCard : UIView

@property (nonatomic, weak) id <TAJScoreSheetCardProtocol> scoreCardDelegate;

- (void)showScoreSheetCard:(NSString *)cardID;

@end

@protocol TAJScoreSheetCardProtocol <NSObject>

- (NSString *)getScoreSheetJoker;

@end