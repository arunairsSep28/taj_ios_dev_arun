/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJScoreSheetCard.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 07/06/14.
 **/

#import "TAJScoreSheetCard.h"

@interface TAJScoreSheetCard ()

@property (weak, nonatomic) IBOutlet UIImageView *cardImageView;
@property (weak, nonatomic) IBOutlet UIImageView *jokerImageView;

@property (nonatomic, strong) NSString  *jokerCardNumber;
@property (nonatomic, strong) NSString  *cardId;
@end

@implementation TAJScoreSheetCard

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (void)showScoreSheetCard:(NSString *)cardID
{
    self.cardId = [cardID uppercaseString];
    
    // Set card image according to the input string
    UIImage *cardImage = [UIImage imageNamed:cardID];
    self.cardImageView.image = cardImage;
    
    //show joker label if joker card is exist in my deck of cards
    self.jokerCardNumber = [self getScoreSheetJoker];
    
    if ([self.jokerCardNumber isEqualToString:@"0"] && [self.cardNumber isEqualToString:@"1"])
    {
        self.jokerImageView.hidden = NO;
    }
    else if ([self.jokerCardNumber isEqualToString:self.cardNumber])
    {
        self.jokerImageView.hidden = NO;
    }
}

- (NSString *)cardNumber
{
    // Intermediate
    NSString *numberString;
    
    NSScanner *scanner = [NSScanner scannerWithString:self.cardId];
    NSCharacterSet *numbers = [NSCharacterSet characterSetWithCharactersInString:@"0123456789"];
    
    // Collect numbers.
    [scanner scanCharactersFromSet:numbers intoString:&numberString];
    
    // Result.
    return numberString;
}

- (NSString *)getScoreSheetJoker
{
    NSString *jokerCardNumber = nil;
    if ([self.scoreCardDelegate respondsToSelector:@selector(getScoreSheetJoker)])
    {
        jokerCardNumber = [self.scoreCardDelegate getScoreSheetJoker];
    }
    return jokerCardNumber;
}

@end
