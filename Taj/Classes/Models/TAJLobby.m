/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLobby.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 27/01/14.
 **/

#import "TAJLobby.h"
#import "TAJGameTable.h"

@implementation TAJLobby

@synthesize tableDetails = _tableDetails;
@synthesize firstTable = _firstTable;
@synthesize secondTable = _secondTable;
@synthesize isFirstTableCreated = _isFirstTableCreated;
@synthesize isSecondTableCreated = _isSecondTableCreated;
@synthesize runningTableType = _runningTableType;
@synthesize allTableEvents = _allTableEvents;
@synthesize startGameEvents = _startGameEvents;
@synthesize sendStackEvents = _sendStackEvents;
@synthesize sendDealEvents = _sendDealEvents;
@synthesize gameEndEvents = _gameEndEvents;
@synthesize turnUpdateEvents = _turnUpdateEvents;
@synthesize playerQuitEvents = _playerQuitEvents;
@synthesize playerJoinEvents = _playerJoinEvents;
@synthesize turnTimeoutEvents = _turnTimeoutEvents;
@synthesize gameResultEvents = _gameResultEvents;
@synthesize gameScheduleEvents = _gameScheduleEvents;
@synthesize gameLevelUpdateEvents = _gameLevelUpdateEvents;
@synthesize gameSittingSeqEvents = _gameSittingSeqEvents;
@synthesize showEvents = _showEvents;
@synthesize playerEliminateEvents = _playerEliminateEvents;
@synthesize playerDropEvents = _playerDropEvents;
@synthesize cardDiscardEvents = _cardDiscardEvents;
@synthesize cardPickEvents = _cardPickEvents;
@synthesize stackResuffleEvents = _stackResuffleEvents;
@synthesize chatMSGEvents = _chatMSGEvents;
@synthesize heartBeatEvents = _heartBeatEvents;
@synthesize reportBugEvents = _reportBugEvents;
@synthesize showDealEvents = _showDealEvents;
@synthesize tableStartEvents = _tableStartEvents;
@synthesize tableClosedEvents = _tableClosedEvents;
@synthesize bestOfWinnerEvents = _bestOfWinnerEvents;
@synthesize poolWinnerEvents = _poolWinnerEvents;
@synthesize tableTossEvents = _tableTossEvents;
@synthesize splitStatusEvents = _splitStatusEvents;
@synthesize splitResultEvents = _splitResultEvents;
@synthesize rejoinEvents = _rejoinEvents;
@synthesize eventNewGameEvent = _eventNewGameEvent;
@synthesize turnExtraTimeEvents = _turnExtraTimeEvents;
@synthesize gameDescheduleEvents = _gameDescheduleEvents;
@synthesize splitFalseEvents = _splitFalseEvents;
@synthesize meldFailEvents = _meldFailEvents;
@synthesize turnExtratimeReconnectEvents = _turnExtratimeReconnectEvents;
@synthesize meldExtratimeReconnectEvents = _meldExtratimeReconnectEvents;
@synthesize showExtratimeReconnectEvents = _showExtratimeReconnectEvents;
@synthesize meldRequestEvents = _meldRequestEvents;
@synthesize meldReplyEvents = _meldReplyEvents;
@synthesize rejoinRequests = _rejoinRequests;
@synthesize splitReplies = _splitReplies;
@synthesize searchJoinTableReplies = _searchJoinTableReplies;
@synthesize sendSlotEvents = _sendSlotEvents;
@synthesize splitRequests = _splitRequests;
@synthesize rebuyinEvents = _rebuyinEvents;
@synthesize rebuyinReplies = _rebuyinReplies;
@synthesize autoplayStatusReplies = _autoplayStatusReplies;
@synthesize autoplayStatusEvents = _autoplayStatusEvents;
@synthesize playerCount = _playerCount;
@synthesize tableLength = _tableLength;
@synthesize standUpReplies = _standUpReplies;

@synthesize getTableExtraReply = _getTableExtraReply;

- (id)init
{
    self = [super init];
    if (self) {
        
        _tableDetails = Nil;
        _firstTable = Nil;
        _secondTable = Nil;
        _isFirstTableCreated = false;
        _isSecondTableCreated = false;
        _runningTableType = eOpenTableNone;
        _poller = Nil;
        _allTableEvents = [[NSMutableArray alloc] init];
        _startGameEvents = [[NSMutableArray alloc] init];
        _sendStackEvents = [[NSMutableArray alloc] init];
        _sendDealEvents = [[NSMutableArray alloc] init];
        _gameEndEvents = [[NSMutableArray alloc] init];
        _turnUpdateEvents = [[NSMutableArray alloc] init];
        _playerQuitEvents = [[NSMutableArray alloc] init];
        _turnTimeoutEvents = [[NSMutableArray alloc] init];
        _gameResultEvents = [[NSMutableArray alloc] init];
        _gameScheduleEvents = [[NSMutableArray alloc] init];
        _gameLevelUpdateEvents = [[NSMutableArray alloc] init];
        _playerJoinEvents = [[NSMutableArray alloc] init];
        _gameSittingSeqEvents = [[NSMutableArray alloc] init];
        _showEvents = [[NSMutableArray alloc] init];
        _playerEliminateEvents = [[NSMutableArray alloc] init];
        _playerDropEvents = [[NSMutableArray alloc] init];
        _cardDiscardEvents = [[NSMutableArray alloc] init];
        _cardPickEvents = [[NSMutableArray alloc] init];
        _stackResuffleEvents = [[NSMutableArray alloc] init];
        _eventNewGameEvent = [[NSMutableArray alloc] init];
        _chatMSGEvents = [[NSMutableArray alloc] init];
        _heartBeatEvents = [[NSMutableArray alloc] init];
        _reportBugEvents = [[NSMutableArray alloc] init];
        _showDealEvents = [[NSMutableArray alloc] init];
        _tableStartEvents = [[NSMutableArray alloc] init];
        _tableClosedEvents = [[NSMutableArray alloc] init];
        _bestOfWinnerEvents = [[NSMutableArray alloc] init];
        _poolWinnerEvents = [[NSMutableArray alloc] init];
        _tableTossEvents = [[NSMutableArray alloc] init];
        _splitStatusEvents = [[NSMutableArray alloc] init];
        _splitResultEvents = [[NSMutableArray alloc] init];
        _rejoinEvents = [[NSMutableArray alloc] init];
        _turnExtraTimeEvents = [[NSMutableArray alloc] init];
        _gameDescheduleEvents = [[NSMutableArray alloc] init];
        _splitFalseEvents = [[NSMutableArray alloc] init];
        _meldSuccessEvents = [[NSMutableArray alloc] init];
        _preGameResultEvents = [[NSMutableArray alloc] init];
        _meldFailEvents = [[NSMutableArray alloc] init];
        _turnExtratimeReconnectEvents = [[NSMutableArray alloc] init];
        _meldExtratimeReconnectEvents = [[NSMutableArray alloc] init];
        _showExtratimeReconnectEvents = [[NSMutableArray alloc] init];
        _meldRequestEvents = [[NSMutableArray alloc] init];
        _meldReplyEvents = [[NSMutableArray alloc] init];
        _rejoinRequests = [[NSMutableArray alloc] init];
        _splitReplies = [[NSMutableArray alloc] init];
        _searchJoinTableReplies = [[NSMutableArray alloc] init];
        _sendSlotEvents = [[NSMutableArray alloc] init];
        _splitRequests = [[NSMutableArray alloc] init];
        _rebuyinEvents = [[NSMutableArray alloc] init];
        _otherLoginEvents = [[NSMutableArray alloc] init];
        _rebuyinReplies = [[NSMutableArray alloc] init];
        
        _autoplayStatusReplies = [[NSMutableArray alloc] init];
        _autoplayStatusEvents = [[NSMutableArray alloc] init];
        
        _playerCount = [[NSMutableArray alloc] init];
        _tableLength = [[NSMutableArray alloc] init];
        _standUpReplies = [[NSMutableArray alloc] init];
        
        _getTableExtraReply = [[NSMutableArray alloc] init];
        
        return self;
    }
    return nil;
}

- (void)assignTheTableDetails:(NSDictionary *)dictionary
{
    self.tableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
}

- (void)createFirstTable:(NSDictionary*)dictionary
{
    self.isFirstTableCreated = TRUE;
    TAJGameTable *table = [[TAJGameTable alloc] init];
    self.firstTable = table;
    self.firstTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    self.firstTable.tableIdentifier = [dictionary objectForKey:TAJ_TABLE_ID];
    DLog(@"first table created in lobby *******************************************************%@",self.firstTable.tableIdentifier);
    DLog(@"first table created *****************************************************************************%hhd",self.isFirstTableCreated);
    
}

- (void)createSecondTable:(NSDictionary*)dictionary
{
    self.isSecondTableCreated = TRUE;
    TAJGameTable *table = [[TAJGameTable alloc] init];
    self.secondTable = table;
    self.secondTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
    self.secondTable.tableIdentifier = [dictionary objectForKey:TAJ_TABLE_ID];
    DLog(@"second table created   in lobby ***************************************************** %@", self.secondTable.tableIdentifier);
    DLog(@"first table created  ******************************************************************************%hhd",self.isSecondTableCreated);
    
}

- (void)updateFirstTableGameTableDetailsWithSearchJointableDictionary:(NSDictionary*)dictionary
{
    self.firstTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
}

- (void)updateSecondTableGameTableDetailsWithSearchJointableDictionary:(NSDictionary*)dictionary
{
    self.secondTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
}

- (void)updateFirstTableWithSearchJointableDictionary:(NSDictionary*)dictionary
{
    self.firstTable.gameTableDetails = Nil;
    self.firstTable.gameTableViewDetails = Nil;
    self.firstTable.gameTablePlayDetails = Nil;
    self.firstTable.tableIdentifier = [dictionary objectForKey:TAJ_TABLE_ID];
    DLog(@"lobby first table id %@ ", self.firstTable.tableIdentifier );
    self.firstTable.mSeatNumber = [[dictionary objectForKey:TAJ_SEAT] intValue];
    self.firstTable.searchJoinTableMode = TRUE;
}

- (void)updateSecondTableWithSearchJointableDictionary:(NSDictionary*)dictionary
{
    self.secondTable.gameTableDetails = Nil;
    self.secondTable.gameTableViewDetails = Nil;
    self.secondTable.gameTablePlayDetails = Nil;
    self.secondTable.tableIdentifier = [dictionary objectForKey:TAJ_TABLE_ID];
    DLog(@"lobby second table id %@ ", self.secondTable.tableIdentifier );
    self.secondTable.mSeatNumber = [[dictionary objectForKey:TAJ_SEAT] intValue];
    self.secondTable.searchJoinTableMode = TRUE;
}

- (void)destroyFirstTable
{
    self.firstTable = Nil;
    self.isFirstTableCreated = FALSE;
    DLog(@"first table destroyed *****************************************************************************%hhd",self.isFirstTableCreated);
    
}

- (void)destroySecondTable
{
    self.secondTable = Nil;
    self.isSecondTableCreated = FALSE;
    DLog(@"second table destroyed  ******************************************************************************%hhd",self.isSecondTableCreated);
    
}

- (void)destroyTable:(NSString*)tableID
{
    if (self.firstTable)
    {
        if (NSOrderedSame == [tableID compare:self.firstTable.tableIdentifier])
        {
            DLog(@"destroy first table %hhd",self.isFirstTableCreated);
            [self destroyFirstTable];
        }
    }
    
    if(self.secondTable)
    {
        if (NSOrderedSame == [tableID compare:self.secondTable.tableIdentifier])
        {
            DLog(@"detroy second table  %hhd",self.isSecondTableCreated);
            [self destroySecondTable];
        }
    }
}

- (void)startPoller
{
#if NEW_LOBBY_EVENT_PROCESSING
    self.poller = [NSTimer scheduledTimerWithTimeInterval:0.05
                                                   target:self
                                                 selector:@selector(processTableEvents)
                                                 userInfo:nil
                                                  repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.poller forMode:NSRunLoopCommonModes];
#else
    self.poller = [NSTimer scheduledTimerWithTimeInterval:0.1
                                                   target:self
                                                 selector:@selector(processEvents)
                                                 userInfo:nil
                                                  repeats:YES];
    [[NSRunLoop mainRunLoop] addTimer:self.poller forMode:NSRunLoopCommonModes];
#endif
}

- (void)stopPoller
{
    [self.poller invalidate];
    self.poller = Nil;
}

- (NSString*)getCurrentRunningTableID
{
    NSString *tableID = Nil;
    switch (self.runningTableType)
    {
        case eOpenTableFirst:
            if (self.firstTable)
            {
                tableID = self.firstTable.tableIdentifier;
            }
            break;
            
        case eOpenTableSecond:
            if (self.secondTable)
            {
                tableID = self.secondTable.tableIdentifier;
            }
            break;
            
        case eOpenTableNone:
            tableID = Nil;
            break;
            
        default:
            break;
    }
    return tableID;
}

#pragma mark events processing

- (void)processEventsWithArray:(NSMutableArray*)dataArray andNotification:(NSString*)notificationName;
{
    int len = 0;
    len = [dataArray count];
    if (len > 0) {
        NSDictionary *dictionary = NULL;
        dictionary = [dataArray objectAtIndex:0];
        [dataArray removeObjectAtIndex:0];
        [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:dictionary];
    }
}

- (void)processEvents
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for processEvents ");
#endif
    
    [self processEventsWithArray:self.startGameEvents andNotification:START_GAME];
    [self processEventsWithArray:self.sendStackEvents andNotification:SEND_STACK];
    [self processEventsWithArray:self.gameSittingSeqEvents andNotification:GAME_SITTING_SEQUENCE];
    [self processEventsWithArray:self.sendDealEvents  andNotification:SEND_DEAL];
    [self processEventsWithArray:self.sendSlotEvents andNotification:SEND_SLOTS];
    [self processEventsWithArray:self.gameEndEvents andNotification:GAME_END];
    [self processEventsWithArray:self.turnUpdateEvents andNotification:TURN_UPDATE];
    [self processEventsWithArray:self.gameResultEvents andNotification:GAME_RESULT];
    [self processEventsWithArray:self.turnTimeoutEvents andNotification:TURN_TIMEOUT];
    [self processEventsWithArray:self.gameScheduleEvents andNotification:GAME_SCHEDULE];
    [self processEventsWithArray:self.gameLevelUpdateEvents andNotification:GAME_LEVEL_UPDATE];
    [self processEventsWithArray:self.playerQuitEvents andNotification:PLAYER_QUIT];
    [self processEventsWithArray:self.playerJoinEvents andNotification:PLAYER_JOIN];
    [self processEventsWithArray:self.showEvents andNotification:SHOW_EVENTS];
    [self processEventsWithArray:self.playerEliminateEvents andNotification:PLAYER_ELIMINATE];
    [self processEventsWithArray:self.playerDropEvents andNotification:PLAYER_DROP];
    [self processEventsWithArray:self.stackResuffleEvents andNotification:STACK_RESUFFLE];
    [self processEventsWithArray:self.eventNewGameEvent andNotification:NEW_GAME];
    [self processEventsWithArray:self.chatMSGEvents andNotification:CHAT_MSG];
    [self processEventsWithArray:self.heartBeatEvents andNotification:HEART_BEAT];
    [self processEventsWithArray:self.reportBugEvents andNotification:REPORT_BUG];
    [self processEventsWithArray:self.showDealEvents andNotification:SHOW_DEAL];
    [self processEventsWithArray:self.tableStartEvents andNotification:TABLE_START];
    [self processEventsWithArray:self.tableClosedEvents andNotification:TABLE_CLOSED];
    [self processEventsWithArray:self.bestOfWinnerEvents andNotification:BEST_OF_WINNER];
    [self processEventsWithArray:self.poolWinnerEvents andNotification:POOL_WINNER];
    [self processEventsWithArray:self.tableTossEvents andNotification:TABLE_TOSS];
    [self processEventsWithArray:self.splitStatusEvents andNotification:SPLIT_STATUS];
    [self processEventsWithArray:self.splitResultEvents andNotification:SPLIT_RESULT];
    [self processEventsWithArray:self.rejoinEvents andNotification:REJOIN_EVENT];
    [self processEventsWithArray:self.turnExtraTimeEvents andNotification:TURN_EXTRA_TIME];
    [self processEventsWithArray:self.gameDescheduleEvents andNotification:GAME_DESCHEDULE];
    [self processEventsWithArray:self.splitFalseEvents andNotification:SPLIT_FALSE];
    [self processEventsWithArray:self.meldSuccessEvents andNotification:MELD_SUCCESS_CURRENT_PLAYER];
    [self processEventsWithArray:self.preGameResultEvents andNotification:PRE_GAME_RESULT];
    [self processEventsWithArray:self.meldFailEvents andNotification:MELD_FAIL];
    [self processEventsWithArray:self.turnExtratimeReconnectEvents andNotification:TURN_EXTRATIME_RECONNECT];
    [self processEventsWithArray:self.meldExtratimeReconnectEvents andNotification:MELD_EXTRATIME_RECONNECT];
    [self processEventsWithArray:self.showExtratimeReconnectEvents andNotification:SHOW_EXTRATIME_RECONNECT];
    [self processEventsWithArray:self.meldReplyEvents andNotification:MELD_REPLY_NOTIFICATION];
    [self processEventsWithArray:self.checkMeldReplyEvents andNotification:CHECKMELD_REPLY_NOTIFICATION];

    [self processEventsWithArray:self.meldRequestEvents andNotification:MELD_REQUEST_NOTIFICATION_FOR_OTHER_PLAYER];
    [self processEventsWithArray:self.getTableExtraReply andNotification:GET_TABLE_EXTRA];
    [self processEventsWithArray:self.rejoinRequests andNotification:REJOIN_REQUEST];
    [self processEventsWithArray:self.splitReplies andNotification:SPLIT_REPLY];
    [self processEventsWithArray:self.searchJoinTableReplies andNotification:SEARCH_JOIN_TABLE];
    [self processEventsWithArray:self.splitRequests andNotification:SPLIT_REQUEST];
    [self processEventsWithArray:self.rebuyinEvents andNotification:REBUYIN_EVENT];
    [self processEventsWithArray:self.otherLoginEvents andNotification:OTHER_LOGIN];
    [self processEventsWithArray:self.rebuyinReplies andNotification:REBUYIN_REPLY];
    [self processEventsWithArray:self.autoplayStatusReplies andNotification:AUTO_PLAY_STATUS_REPLY];
    [self processEventsWithArray:self.autoplayStatusEvents andNotification:AUTO_PLAY_STATUS_EVENT];
    [self processEventsWithArray:self.playerCount andNotification:PLAYER_COUNT];
    [self processEventsWithArray:self.tableLength andNotification:TABLE_LENGTH];
    [self processEventsWithArray:self.standUpReplies andNotification:STAND_UP];
    [self processEventsWithArray:self.cardPickEvents andNotification:CARD_PICK];
    [self processEventsWithArray:self.cardDiscardEvents andNotification:CARD_DISCARD];
}

- (void)processTableEvents
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for processTableEvents ");
#endif
    
    int count = 0;
    count = [self.allTableEvents count];
    if (count > 0)
    {
        NSDictionary *dictionary = NULL;
        NSString *tagName = NULL;
        dictionary = [self.allTableEvents objectAtIndex:0];
        [self.allTableEvents removeObjectAtIndex:0];
        tagName = [dictionary valueForKeyPath:TAJ_COMMAND_NAME];
        
        if (NSOrderedSame == [tagName compare:REPLY_COMMAND])
        {
            [self processTheReplyTag:dictionary];
        }
        
        if (NSOrderedSame == [tagName compare:EVENT_COMMAND])
        {
            [self processTheEventTag:dictionary];
        }
        if (NSOrderedSame == [tagName compare:REQUEST_COMMAND])
        {
            [self processTheRequestTag:dictionary];
        }
    }
}

- (void)processTheRequestTag:(NSDictionary*)dictionary
{
    NSString *commandName = NULL;
    commandName = [dictionary valueForKeyPath:TAJ_REQUEST_COMMAND_TYPE];
    
    if (NSOrderedSame == [commandName compare:MELD_REQUEST])
    {
        [self processNotificationForEvents:dictionary andNotification:MELD_REQUEST_NOTIFICATION_FOR_OTHER_PLAYER];
    }
    
    if (NSOrderedSame == [commandName compare:REJOIN_TAG])
    {
        [self processNotificationForEvents:dictionary andNotification:REJOIN_REQUEST];
    }
    
    if (NSOrderedSame == [commandName compare:SPLIT_TAG])
    {
        [self processNotificationForEvents:dictionary andNotification:SPLIT_REQUEST];
    }
    if (NSOrderedSame == [commandName compare:SMART_CORRECTION])
    {
        [self processNotificationForEvents:dictionary andNotification:SMART_CORRECTION];
    }
}

- (void)processTheReplyTag:(NSDictionary *)dictionary
{
    NSString *data = NULL;
    data = [dictionary valueForKeyPath:TAJ_DATA];
    
    if (data && (NSOrderedSame == [data compare:TURN_EXTRA_TIME]))
    {
        [self processNotificationForEvents:dictionary andNotification:TURN_EXTRA_TIME];
    }
    if (data && (NSOrderedSame == [data compare:MELD_REPLY]))
    {
        [self processNotificationForEvents:dictionary andNotification:MELD_REPLY_NOTIFICATION];
    }
    if (data && (NSOrderedSame == [data compare:CHECKMELD_REPLY]))
    {
        [self processNotificationForEvents:dictionary andNotification:CHECKMELD_REPLY_NOTIFICATION];
    }
    if (data && (NSOrderedSame == [data compare:AUTO_PLAY_STATUS]))
    {
        [self processNotificationForEvents:dictionary andNotification:AUTO_PLAY_STATUS_REPLY];
    }
    if (data && (NSOrderedSame == [data compare:GET_TABLE_EXTRA]))
    {
        [self processNotificationForEvents:dictionary andNotification:GET_TABLE_EXTRA];
    }
    if (data && (NSOrderedSame == [data compare:SPLIT_TAG]))
    {
        [self processNotificationForEvents:dictionary andNotification:SPLIT_REPLY];
    }
    if (data && (NSOrderedSame == [data compare:SEARCH_JOIN_TABLE]))
    {
        //APP_DELEGATE.gameTableIndex = 2;
        [self processNotificationForEvents:dictionary andNotification:SEARCH_JOIN_TABLE];
    }
    if (data && (NSOrderedSame == [data compare:REBUYIN]))
    {
        [self processNotificationForEvents:dictionary andNotification:REBUYIN_REPLY];
    }
    if (data && (NSOrderedSame == [data compare:PLAYER_COUNT]))
    {
        [self processNotificationForEvents:dictionary andNotification:PLAYER_COUNT];
    }
    if (data && (NSOrderedSame == [data compare:TABLE_LENGTH]))
    {
        [self processNotificationForEvents:dictionary andNotification:TABLE_LENGTH];
    }
    if (data && (NSOrderedSame == [data compare:STAND_UP]))
    {
        [self processNotificationForEvents:dictionary andNotification:STAND_UP];
    }
}

- (void)processTheEventTag:(NSDictionary *)dictionary
{
    NSString *eventName = NULL;
    eventName = [dictionary valueForKeyPath:TAJ_EVENT_NAME];
    
    if (eventName)
    {
        if (NSOrderedSame == [eventName compare:START_GAME])
        {
            [self processNotificationForEvents:dictionary andNotification:START_GAME];
           // NSLog(@"START_GAME : %@",dictionary);
        }
        
        if (NSOrderedSame == [eventName compare:GAME_SCHEDULE])
        {
            [self processNotificationForEvents:dictionary andNotification:GAME_SCHEDULE];
        }
        if (NSOrderedSame == [eventName compare:SEND_DEAL])
        {
            [self processNotificationForEvents:dictionary andNotification:SEND_DEAL];
            //NSLog(@"SEND_DEAL : %@",dictionary);

            //DLog(@"************ processTheEventTag : SEND_DEAL");
            //DLog(@"send deal In Lobby:");
        }
        
        if (NSOrderedSame == [eventName compare:SEND_SLOTS])
        {
            [self processNotificationForEvents:dictionary andNotification:SEND_SLOTS];
           // DLog(@"************ processSendSlotsEvent : SEND_SLOTS");
        }
        if (NSOrderedSame == [eventName compare:BALANCE_UPDATE])
        {
            //BALANCE UPDATE
            //DLog(@"************ processTheEventTag : BALANCE_UPDATE");
            [self processNotificationForEvents:dictionary andNotification:BALANCE_UPDATE];
        }
        
        if (NSOrderedSame == [eventName compare:SHOW_EVENTS])
        {
            [self processNotificationForEvents:dictionary andNotification:SHOW_EVENTS];
            //DLog(@"************ processTheEventTag : SHOW");
        }
        
        if (NSOrderedSame == [eventName compare:TABLE_CLOSED])
        {
            [self processNotificationForEvents:dictionary andNotification:TABLE_CLOSED];
           // DLog(@"************ processTheEventTag : TABLE_CLOSED");
        }
        
        if (NSOrderedSame == [eventName compare:GAME_SITTING_SEQUENCE])
        {
            [self processNotificationForEvents:dictionary andNotification:GAME_SITTING_SEQUENCE];
           // DLog(@"************ processTheEventTag : SITTING_SEQ");
            NSLog(@"************ processTheEventTag : SITTING_SEQ");
            NSLog(@"SITTING_SEQ DICT : %@",dictionary);
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_ELIMINATE])
        {
            [self processNotificationForEvents:dictionary andNotification:PLAYER_ELIMINATE];
           // DLog(@"************ processTheEventTag : PLAYER_ELIMINATE");
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_DROP])
        {
            [self processNotificationForEvents:dictionary andNotification:PLAYER_DROP];
            //DLog(@"************ processTheEventTag : PLAYER_DROP");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_UPDATE])
        {
            [self processNotificationForEvents:dictionary andNotification:TURN_UPDATE];
           // DLog(@"************ processTheEventTag : TURN_UPDATE");
        }
        
        
        if (NSOrderedSame == [eventName compare:GAME_END])
        {
            [self processNotificationForEvents:dictionary andNotification:GAME_END];
           // DLog(@"************ processTheEventTag : GAME_END");
        }
        
        if (NSOrderedSame == [eventName compare:STACK_RESUFFLE])
        {
            [self processNotificationForEvents:dictionary andNotification:STACK_RESUFFLE];
           // DLog(@"************ processTheEventTag : STACK_RESUFFLE");
        }
        
        if (NSOrderedSame == [eventName compare:SEND_STACK])
        {
            [self processNotificationForEvents:dictionary andNotification:SEND_STACK];
            //DLog(@"************ processTheEventTag : SEND_STACK");
            NSLog(@"SEND_STACK");
            NSLog(@"STACK DICT : %@",dictionary);
            
        }
        
        if (NSOrderedSame == [eventName compare:NEW_GAME])
        {
            [self processNotificationForEvents:dictionary andNotification:NEW_GAME];
            //DLog(@"************ processTheEventTag : NEW_GAME");
        }
        
        if (NSOrderedSame == [eventName compare:CHAT_MSG])
        {
            [self processNotificationForEvents:dictionary andNotification:CHAT_MSG];
           // DLog(@"************ processTheEventTag : CHAT_MSG");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_TIMEOUT])
        {
            [self processNotificationForEvents:dictionary andNotification:TURN_TIMEOUT];
            //DLog(@"************ processTheEventTag : TURN_TIMEOUT");
        }
        if (NSOrderedSame == [eventName compare:CARD_PICK])
        {
            [self processNotificationForEvents:dictionary andNotification:CARD_PICK];
            //DLog(@"************ processTheEventTag : CARD_PICK");
        }
        
        if (NSOrderedSame == [eventName compare:CARD_DISCARD])
        {
            [self processNotificationForEvents:dictionary andNotification:CARD_DISCARD];
            //DLog(@"************ processTheEventTag : CARD_DISCARD");
        }
        if (NSOrderedSame == [eventName compare:PLAYER_JOIN])
        {
            [self processNotificationForEvents:dictionary andNotification:PLAYER_JOIN];
            //DLog(@"************ processTheEventTag : PLAYER_JOIN in lobby %@",dictionary);
        }
        
        if (NSOrderedSame == [eventName compare:HEART_BEAT])
        {
            [self processNotificationForEvents:dictionary andNotification:HEART_BEAT];
            //DLog(@"************ processTheEventTag : HEART_BEAT");
        }
        
        if (NSOrderedSame == [eventName compare:REPORT_BUG])
        {
            [self processNotificationForEvents:dictionary andNotification:REPORT_BUG];
            //DLog(@"************ processTheEventTag : REPORT_BUG");
        }
        
        if (NSOrderedSame == [eventName compare:SHOW_DEAL])
        {
            [self processNotificationForEvents:dictionary andNotification:SHOW_DEAL];
            //DLog(@"************ processTheEventTag : SHOW_DEAL");
        }
        
        if (NSOrderedSame == [eventName compare:TABLE_START])
        {
            [self processNotificationForEvents:dictionary andNotification:TABLE_START];
            //DLog(@"************ processTheEventTag : TABLE_START");
        }
        
        if (NSOrderedSame == [eventName compare:TABLE_TOSS])
        {
            [self processNotificationForEvents:dictionary andNotification:TABLE_TOSS];
            //DLog(@"************ processTheEventTag : TABLE_TOSS");
        }
        
        if (NSOrderedSame == [eventName compare:PRE_GAME_RESULT])
        {
            
            [self processNotificationForEvents:dictionary andNotification:PRE_GAME_RESULT];
            //DLog(@"************ processTheEventTag : PRE_GAME_RESULT");
        }
        
        if (NSOrderedSame == [eventName compare:GAME_RESULT])
        {
            [self processNotificationForEvents:dictionary andNotification:GAME_RESULT];
            //DLog(@"************ processTheEventTag : GAME_RESULT");
        }
        
        if (NSOrderedSame == [eventName compare:SPLIT_FALSE])
        {
            [self processNotificationForEvents:dictionary andNotification:SPLIT_FALSE];
            //DLog(@"************ processTheEventTag : SPLIT_FALSE");
        }
        
        if (NSOrderedSame == [eventName compare:SPLIT_STATUS])
        {
            [self processNotificationForEvents:dictionary andNotification:SPLIT_STATUS];
           // DLog(@"************ processTheEventTag : SPLIT_STATUS");
        }
        
        if (NSOrderedSame == [eventName compare:SPLIT_RESULT])
        {
            [self processNotificationForEvents:dictionary andNotification:SPLIT_RESULT];
            //DLog(@"************ processTheEventTag : SPLIT_RESULT");
        }
        
        if (NSOrderedSame == [eventName compare:REJOIN_TAG])
        {
            [self processNotificationForEvents:dictionary andNotification:REJOIN_EVENT];
           // DLog(@"************ processTheEventTag : REJOIN");
        }
        
        if (NSOrderedSame == [eventName compare:GET_TABLE_DETAILS])
        {
            [self processNotificationForEvents:dictionary andNotification:GET_TABLE_DETAILS];
           // DLog(@"************ processTheEventTag : get_table_details");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_EXTRA_TIME_ANOTHER])
        {
            [self processNotificationForEvents:dictionary andNotification:TURN_EXTRA_TIME];
            //DLog(@"************ processTheEventTag : TURN_EXTRATIME");
        }
        
        if (NSOrderedSame == [eventName compare:GAME_DESCHEDULE])
        {
            [self processNotificationForEvents:dictionary andNotification:GAME_DESCHEDULE];
           // DLog(@"************ processTheEventTag : GAME_DESCHEDULE");
        }
        
        if (NSOrderedSame == [eventName compare:MELD_SUCCESS_CURRENT_PLAYER])
        {
            [self processNotificationForEvents:dictionary andNotification:MELD_SUCCESS_CURRENT_PLAYER];
            //DLog(@"************ processTheEventTag : meld_sucess");
        }
        
        if (NSOrderedSame == [eventName compare:BEST_OF_WINNER])
        {
            [self processNotificationForEvents:dictionary andNotification:BEST_OF_WINNER];
            //DLog(@"************ processTheEventTag : BEST_OF_WINNER");
        }
        
        if (NSOrderedSame == [eventName compare:POOL_WINNER])
        {
            [self processNotificationForEvents:dictionary andNotification:POOL_WINNER];
           // DLog(@"************ processTheEventTag : POOL_WINNER");
        }
        
        if (NSOrderedSame == [eventName compare:MELD_FAIL])
        {
            [self processNotificationForEvents:dictionary andNotification:MELD_FAIL];
           // DLog(@"************ processTheEventTag : MELD_FAIL");
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_QUIT])
        {
            [self processNotificationForEvents:dictionary andNotification:PLAYER_QUIT];
            //DLog(@"************ processTheEventTag : PLAYER_QUIT");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_EXTRATIME_RECONNECT])
        {
            [self processNotificationForEvents:dictionary andNotification:TURN_EXTRATIME_RECONNECT];
           // DLog(@"************ processTheEventTag : TURN_EXTRATIME_RECONNECT");
        }
        
        if (NSOrderedSame == [eventName compare:MELD_EXTRATIME_RECONNECT])
        {
            [self processNotificationForEvents:dictionary andNotification:MELD_EXTRATIME_RECONNECT];
           // DLog(@"************ processTheEventTag : MELD_EXTRATIME_RECONNECT");
        }
        
        if (NSOrderedSame == [eventName compare:SHOW_EXTRATIME_RECONNECT])
        {
            [self processNotificationForEvents:dictionary andNotification:SHOW_EXTRATIME_RECONNECT];
            //DLog(@"************ processTheEventTag : SHOW_EXTRATIME_RECONNECT");
        }
        
        if (NSOrderedSame == [eventName compare:REBUYIN])
        {
            [self processNotificationForEvents:dictionary andNotification:REBUYIN_EVENT];
           // DLog(@"************ processTheEventTag : REBUYIN");
        }
        
        if (NSOrderedSame == [eventName compare:OTHER_LOGIN])
        {
            [self processNotificationForEvents:dictionary andNotification:OTHER_LOGIN];
           // DLog(@"************ processTheEventTag : OTHER_LOGIN");
        }
        
        if (NSOrderedSame == [eventName compare:AUTO_PLAY_STATUS])
        {
            [self processNotificationForEvents:dictionary andNotification:AUTO_PLAY_STATUS_EVENT];
           // DLog(@"************ processTheEventTag : AUTO_PLAY_STATUS");
        }
    }
}

- (void)processNotificationForEvents:(NSDictionary *)dictionary andNotification:(NSString*)notificationName
{
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationName object:dictionary];
//    DLog(@"process notification in lobby %@",dictionary);
}

#pragma mark event updates

- (void)processStartGameEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.startGameEvents addObject:dictionary];
#endif
}

- (void)processSendStackEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.sendStackEvents addObject:dictionary];
#endif
}

- (void)processSendDealEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.sendDealEvents addObject:dictionary];
#endif
}

- (void)processGameEndEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.gameEndEvents addObject:dictionary];
#endif
}

- (void)processTurnUpdateEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
//    [[NSNotificationCenter defaultCenter] postNotificationName:TURN_UPDATE object:dictionary];

#else
    [self.turnUpdateEvents addObject:dictionary];
#endif
}

- (void)processPlayerQuitEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.playerQuitEvents addObject:dictionary];
#endif
}

- (void)processTurnTimeoutEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.turnTimeoutEvents addObject:dictionary];
#endif
}

- (void)processGameResultEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.gameResultEvents addObject:dictionary];
#endif
}

- (void)processGameScheduleEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.gameScheduleEvents addObject:dictionary];
#endif
}

- (void)processLevelUpdateEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.gameLevelUpdateEvents addObject:dictionary];
#endif
}

- (void)processPlayerJoinUpdateEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.playerJoinEvents addObject:dictionary];
#endif
}

- (void)processSittingSeqUpdateEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.gameSittingSeqEvents addObject:dictionary];
#endif
}

- (void)processShowEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.showEvents addObject:dictionary];
#endif
}

- (void)processPlayerEliminateEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.playerEliminateEvents addObject:dictionary];
#endif
}

- (void)processPlayerDropEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.playerDropEvents addObject:dictionary];
#endif
}

- (void)processCardDiscardEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.cardDiscardEvents addObject:dictionary];
#endif
}

- (void)processCardPickEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.cardPickEvents addObject:dictionary];
#endif
}

- (void)processStackResuffleEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.stackResuffleEvents addObject:dictionary];
#endif
}

- (void)processNewGameEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.eventNewGameEvent addObject:dictionary];
#endif
}

- (void)processChatMSGEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.chatMSGEvents addObject:dictionary];
#endif
}

- (void)processHeartBeatEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.heartBeatEvents addObject:dictionary];
#endif
}

- (void)processReportBugEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.reportBugEvents addObject:dictionary];
#endif
}

- (void)processShowDealEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.showDealEvents addObject:dictionary];
#endif
}

- (void)processTableStartEventUpdate:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.tableStartEvents addObject:dictionary];
#endif
}

- (void)processTableClosedEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.tableClosedEvents addObject:dictionary];
#endif
}

- (void)processBestOfWinnerEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.bestOfWinnerEvents addObject:dictionary];
#endif
}

- (void)processPoolWinnerEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.poolWinnerEvents addObject:dictionary];
#endif
}

- (void)processTableTossEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.tableTossEvents addObject:dictionary];
#endif
}

- (void)processSplitStatusEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.splitStatusEvents addObject:dictionary];
#endif
}

- (void)processSplitResultEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.splitResultEvents addObject:dictionary];
#endif
}

- (void) processSplitReply:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.splitReplies addObject:dictionary];
#endif
}

- (void)processRejoinEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.rejoinEvents addObject:dictionary];
#endif
}

- (void)processTurnExtraTime:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.turnExtraTimeEvents addObject:dictionary];
#endif
}

- (void)processGameDeschedule:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.gameDescheduleEvents addObject:dictionary];
#endif
}

- (void)processSplitFalseEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.splitFalseEvents addObject:dictionary];
#endif
}

- (void)processMeldSuccess:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.meldSuccessEvents addObject:dictionary];
#endif
}

- (void)processPreGameResult:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.preGameResultEvents addObject:dictionary];
#endif
}

- (void)processMeldFailEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.meldFailEvents addObject:dictionary];
#endif
}

- (void)processTurnExtraTimeReconnectEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.turnExtraTimeEvents addObject:dictionary];
#endif
}

- (void)processMeldExtraTimeReconnectEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.meldExtratimeReconnectEvents addObject:dictionary];
#endif
}

- (void)processShowExtraTimeReconnectEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.showExtratimeReconnectEvents addObject:dictionary];
#endif
}

- (void)processRebuyinEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.rebuyinEvents addObject:dictionary];
#endif
}

- (void)processOtherLoginEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.otherLoginEvents addObject:dictionary];
#endif
}

- (void)processMeldReplyEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.meldReplyEvents addObject:dictionary];
#endif
}

- (void)processCheckMeldReplyEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.meldReplyEvents addObject:dictionary];
#endif
}

- (void)processMeldRequestEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.meldRequestEvents addObject:dictionary];
#endif
}

- (void)processRejoinRequest:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.rejoinRequests addObject:dictionary];
#endif
}

- (void)processSearchJoinTableReply:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.searchJoinTableReplies addObject:dictionary];
#endif
}

- (void)processSendSlotsEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.sendSlotEvents addObject:dictionary];
#endif
}

- (void)processSmartCorrectionEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.sendSlotEvents addObject:dictionary];
#endif
}

- (void)processSplitRequest:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.splitRequests addObject:dictionary];
#endif
}

- (void)processRebuyinReply:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.rebuyinReplies addObject:dictionary];
#endif
}

- (void)processAutoPlayStatusReply:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.autoplayStatusReplies addObject:dictionary];
#endif
}

- (void)processAutoPlayStatusEvent:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.autoplayStatusEvents addObject:dictionary];
#endif
}

- (void)processPlayerCount:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.playerCount addObject:dictionary];
#endif
}

- (void)processTableLength:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.tableLength addObject:dictionary];
#endif
}

- (void)processStandUpReply:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.standUpReplies addObject:dictionary];
#endif
}

- (void)processGetTableExtraReply:(NSDictionary*)dictionary
{
#if NEW_LOBBY_EVENT_PROCESSING
    [self.allTableEvents addObject:dictionary];
#else
    [self.getTableExtraReply addObject:dictionary];
#endif
}


//{
//    NSMutableArray * betsList = [NSMutableArray new];
//    NSArray * sortedTablesList = [NSArray new];
//    
//    NSDictionary * betDict = [NSDictionary new];
//    
//    for (NSDictionary *dict in listArray) {
//        [betsList addObject:[dict objectForKey:@"TAJ_bet"]];
//    }
//    
//    NSLog(@"BET Array : %@",betsList);
//    
//    NSSet *betsSet = [NSSet setWithArray:betsList];
//    //NSLog(@"TEl : %@",poolsSet);
//    NSArray *uniqueBetsArray = [betsSet allObjects];
//    //NSLog(@"setArray : %@",poolsSetArray);
//    
//    sortedTablesList = [uniqueBetsArray sortedArrayUsingDescriptors:
//                        @[[NSSortDescriptor sortDescriptorWithKey:@"intValue"
//                                                        ascending:YES]]];
//    
//    NSLog(@"sorted_Array : %@",sortedTablesList);
//    
//    return sortedTablesList;
//}

@end
