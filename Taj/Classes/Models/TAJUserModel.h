/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJUserModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 10/01/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJUserModel : NSObject

@property(nonatomic,strong)NSString *displayName;
@property(nonatomic,strong)NSString *password;
@property(nonatomic,strong)NSString *sessionId;
@property(nonatomic,strong)NSString *bonusChips;
@property(nonatomic,strong)NSString *bonusInPlay;
@property(nonatomic,strong)NSString *chatBlock;
@property(nonatomic,strong)NSString *code;
@property(nonatomic,strong)NSString *funchips;
@property(nonatomic,strong)NSString *funInPlay;
@property(nonatomic,strong)NSString *gender;
@property(nonatomic,strong)NSString *loyaltyChips;
@property(nonatomic,strong)NSString *loyaltyInPlay;
@property(nonatomic,strong)NSString *nickName;
@property(nonatomic,strong)NSString *playerLevel;
@property(nonatomic,strong)NSString *poolLowBet;
@property(nonatomic,strong)NSString *poolMediumBet;
@property(nonatomic,strong)NSString *prLowBet;
@property(nonatomic,strong)NSString *prMediumbet;
@property(nonatomic,strong)NSString *realChips;
@property(nonatomic,strong)NSString *realInPlay;
@property(nonatomic,strong)NSString *socialChips;
@property(nonatomic,strong)NSString *socialInPlay;
@property(nonatomic,strong)NSString *userId;
@property(nonatomic,strong)NSString *thisDeviceImageName;
@property(nonatomic,strong)NSString *totalGamesPlayed;
@property(nonatomic,strong)NSString *todayGamesPlayed;
@property(nonatomic,strong)NSArray  *tableIDsArray;

- (id)initWithName:(NSString*)name password:(NSString*)password sessionId:(NSString *)sessionId;
- (void)reset;
- (void)setData:(NSDictionary*)dictionary;
- (void)setDataForEventUpdate:(NSDictionary*)dictionary;
- (void)updateThisDeviceImageName:(NSString *)thisDeviceImageName;
- (NSString *)thisPlayerImageName;
- (void)updateGamesPlayed:(NSString *)totalGames playedGamesOfDay:(NSString *)todaysGames;
- (void)setTableIDs:(NSDictionary *)dictionary;

@end
