/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerDetail.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/01/14.
 **/

#import "TAJUserModel.h"

typedef enum
{
	PlayerMiddleBottom=0,
	PlayerRightBottom,
	PlayerRightTop,
	PlayerMiddleTop,
    PlayerLeftTop,
	PlayerLeftBottom
}
PlayerPosition;

typedef enum
{
    PlayerJoined=10,
    PlayerPlaying,
	PlayerShowed,
    PlayerDroped,
    PlayerQuit
}
PlayerStatus;

@interface TAJPlayerDetail : TAJUserModel

@property(nonatomic) PlayerPosition position;
@property(nonatomic) PlayerStatus status;
@property(nonatomic) int tableID;
@property (nonatomic, strong) UIImage *     image;

@end
