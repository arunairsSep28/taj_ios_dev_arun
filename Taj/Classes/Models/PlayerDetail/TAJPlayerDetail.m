/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPlayerDetail.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 29/01/14.
 **/

#import "TAJPlayerDetail.h"

@implementation TAJPlayerDetail

@synthesize tableID=_tableID;
@synthesize position=_position;
@synthesize status=_status;
@synthesize image=_image;

@end
