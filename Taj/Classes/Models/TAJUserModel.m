/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJUserModel.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 10/01/14.
 **/

#import "TAJUserModel.h"

@implementation TAJUserModel

- (id)initWithName:(NSString*)name password:(NSString*)password sessionId:(NSString *)sessionId
{
    self = [super init];
    if (self) {
        self.displayName = name;
        self.password = password;
        self.sessionId = sessionId;
        
    }
    return self;
}

- (void)reset
{
    //TODO: Reset the member variables
}

- (void)setData:(NSDictionary*)dictionary
{
    //TODO: set the member variables data
    self.bonusChips = [dictionary valueForKeyPath:@"TAJ_bonuschips"];
    self.bonusInPlay = [dictionary valueForKeyPath:@"TAJ_bonusinplay"];
    self.chatBlock = [dictionary valueForKeyPath:@"TAJ_chatblock"];
    self.code = [dictionary valueForKeyPath:@"TAJ_code"];
    self.funchips = [dictionary valueForKeyPath:@"TAJ_fun_chips"];
    self.funInPlay = [dictionary valueForKeyPath:@"TAJ_fun_inplay"];
    self.gender = [dictionary valueForKeyPath:@"TAJ_gender"];
    self.loyaltyChips = [dictionary valueForKeyPath:@"TAJ_loyaltychips"];
    self.loyaltyInPlay = [dictionary valueForKeyPath:@"TAJ_loyaltyinplay"];
    self.nickName = [dictionary valueForKeyPath:@"TAJ_nick_name"];
    self.playerLevel = [dictionary valueForKeyPath:@"TAJ_playerlevel"];
    self.poolLowBet = [dictionary valueForKeyPath:@"TAJ_poollowbet"];
    self.poolMediumBet = [dictionary valueForKeyPath:@"TAJ_poolmediumbet"];
    self.prLowBet = [dictionary valueForKeyPath:@"TAJ_prlowbet"];
    self.prMediumbet = [dictionary valueForKeyPath:@"TAJ_prmediumbet"];
    self.realChips = [dictionary valueForKeyPath:@"TAJ_real_chips"];
    self.realInPlay = [dictionary valueForKeyPath:@"TAJ_real_inplay"];
    self.socialChips = [dictionary valueForKeyPath:@"TAJ_socialchips"];
    self.socialInPlay = [dictionary valueForKeyPath:@"TAJ_socialinplay"];
    self.userId = [dictionary valueForKeyPath:@"TAJ_user_id"] ;
    
}

- (void)setDataForEventUpdate:(NSDictionary*)dictionary
{
    self.bonusChips = [dictionary valueForKeyPath:@"TAJ_bonuschips"];
    self.bonusInPlay = [dictionary valueForKeyPath:@"TAJ_bonusinplay"];
    self.funchips = [dictionary valueForKeyPath:@"TAJ_fun_chips"];
    self.funInPlay = [dictionary valueForKeyPath:@"TAJ_fun_inplay"];
    self.loyaltyChips = [dictionary valueForKeyPath:@"TAJ_loyaltychips"];
    self.loyaltyInPlay = [dictionary valueForKeyPath:@"TAJ_loyaltyinplay"];
    self.realChips = [dictionary valueForKeyPath:@"TAJ_real_chips"];
    self.realInPlay = [dictionary valueForKeyPath:@"TAJ_real_inplay"];
    self.socialChips = [dictionary valueForKeyPath:@"TAJ_socialchips"];
    self.socialInPlay = [dictionary valueForKeyPath:@"TAJ_socialinplay"];
    self.userId = [dictionary valueForKeyPath:@"TAJ_user_id"];
}

- (void)updateThisDeviceImageName:(NSString *)thisDeviceImageName
{
    self.thisDeviceImageName = thisDeviceImageName;
}

- (NSString *)thisPlayerImageName
{
    DLog(@"self.thisDeviceImageName = %@",self.thisDeviceImageName);
    
    return self.thisDeviceImageName;
}

- (void)updateGamesPlayed:(NSString *)totalGames playedGamesOfDay:(NSString *)todaysGames
{
    self.totalGamesPlayed = totalGames;
    self.todayGamesPlayed = todaysGames;
}
- (void)setTableIDs:(NSDictionary *)dictionary
{
    NSString *tableIDs = Nil;
    tableIDs = [dictionary objectForKey:@"TAJ_table_id"];
    self.tableIDsArray = [tableIDs componentsSeparatedByString:@","];
    DLog(@"############################## table ids %@",self.tableIDsArray);
}

@end
