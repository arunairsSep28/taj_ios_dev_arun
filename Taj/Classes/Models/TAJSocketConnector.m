/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSocketConnector.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 31/12/13.
 **/

#import "TAJSocketConnector.h"

@implementation TAJSocketConnector

#pragma mark Initialisation

- (id)initWithDelegate:(id)inDelegate onHost:(NSString *) inHost onPort:(UInt32)inPort
{
	if ((self = [super init]))
    {
		delegate			= inDelegate;
		writeBufferArray	= [[NSMutableArray alloc] init];
		iStreamConnected	= TRUE;
		oStreamConnected	= FALSE;
		host = [NSString stringWithString:inHost] ;
		port = inPort;
		connectionFlag = FALSE;
        isConnectionClosed = FALSE;
        timeCheck = FALSE;
	}
	return self;
}

#pragma mark Communication API

- (BOOL)connect
{
	DLog(@"********** connecting");
    if (!isConnectionClosed)
    {
        [self closeCurrentConnection];
    }
	
	
	CFReadStreamRef readStream = NULL;
	CFWriteStreamRef writeStream = NULL;
	CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, (__bridge CFStringRef)(host), port, &readStream, &writeStream);
	
	if (readStream && writeStream)
    {
		CFReadStreamSetProperty(readStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
		CFWriteStreamSetProperty(writeStream, kCFStreamPropertyShouldCloseNativeSocket, kCFBooleanTrue);
		
		iStream = (NSInputStream *)CFBridgingRelease(readStream);
		[iStream setDelegate:self];
#if DONT_USE_RUNLOOPCOMMONMODE
        //        [iStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [iStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
#else
		[iStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
#endif
		[iStream open];
		
		oStream = (NSOutputStream *)CFBridgingRelease(writeStream);
		[oStream setDelegate:self];
#if DONT_USE_RUNLOOPCOMMONMODE
        //        [oStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
        [oStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
        
#else
		[oStream scheduleInRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
#endif
		[oStream open];
		
		if (connectionCheker)
        {
			[connectionCheker invalidate];
			connectionCheker=nil;
		}
	}
    else
    {
		[delegate onSocketError];
		return FALSE;
	}
	
	return TRUE;
}

- (void)closeCurrentConnection
{
	[iStream close];
	[oStream close];
	[iStream setDelegate:nil];
	[oStream setDelegate:nil];
	[iStream removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
	[oStream removeFromRunLoop:[NSRunLoop mainRunLoop] forMode:NSRunLoopCommonModes];
}

- (void)sendData:(NSData *) inData
{
   
	[writeBufferArray addObject:inData];
	if([oStream hasSpaceAvailable])
    {
		[self writeCurrentBuffer];
	}
}

#pragma mark Handlers/Notificators

- (void)connectionEstablished
{
	DLog(@"connectionEstablished");
	[delegate onSocketConnected:self];
}

- (void)checkOpenConnection
{
	if (iStreamConnected && oStreamConnected)
    {
		[self connectionEstablished];
	}
}

- (void)writeCurrentBuffer
{
	//send data
	int count=[writeBufferArray count];
	if (count == 0)
    {
		return;
	}
	NSData *data = (NSData *)[writeBufferArray objectAtIndex:0];
	[writeBufferArray removeObjectAtIndex:0];
	int len = 0;
	len = [oStream write:(const uint8_t *)[data bytes] maxLength:[data length]];
	if (len > 0)
	{
        //        if ([data bytes])
        //        {
        //            DLog(@"writeCurrentBuffer %d %s",len,(unsigned char *)[data bytes]);
        //        }
	}
}

- (void)readAllAvailableData
{
    //RK : Commented the old data for reference
	connectionFlag=TRUE;
	//read data
	uint8_t buffer[65536];
    //	NSMutableData *receivedData = [[NSMutableData alloc] init];
    if (!receivedData)
    {
        receivedData = [[NSMutableData alloc] init];
    }
    int len = 0;
    int reducedLenght = 0;
	//int offset=0;
    int maxLength;
#if NEW_SOCKET_ALGORITHM
    maxLength = 65536;
#else
    maxLength = 1;
#endif
	while ([iStream hasBytesAvailable])
	{
        memset(buffer,'\0',maxLength);
		len = [iStream read:buffer maxLength:maxLength];
        reducedLenght = len;
        //		DLog(@"**********length of string read %d",len);
#if NEW_SOCKET_ALGORITHM
		if (len > 0)
		{
            dataHolder = "";
            dataArray = strtok((char*)buffer, "\0");
            int dataArrayLen;
            //            char* dataArray = (&(char*)buffer + 1);
            if (buffer [0] == '\0')
            {
                reducedLenght --;
            }
            else
            {
                dataArrayLen = strlen(dataArray);
                
            }
            
            while(dataArray!=NULL)
            {
                if (!receivedData)
                {
                    receivedData = [[NSMutableData alloc] init];
                }
                if(dataArrayLen < len)
                {
                    if(dataArray[dataArrayLen] == '\0')
                    {
                        [receivedData appendBytes:dataArray length:dataArrayLen];
                        timeCheck = false;
                        [self notifyData:receivedData];
                        receivedData = NULL;
                        dataArray = NULL;
                    }
                    else
                    {
                        [receivedData appendBytes:dataArray length:dataArrayLen];
                        break;
                        
                    }
                    reducedLenght = reducedLenght - (dataArrayLen+1);
                    if(reducedLenght>0)
                    {
                        dataArray = strtok((char*)&buffer[len - reducedLenght], "\0");
                        dataArrayLen = strlen(dataArray);
                    }
                    else
                    {
                        timeCheck = false;
                        break;
                    }
                }
                else
                {
                    [receivedData appendBytes:dataArray length:len];
                    break;
                }
            }
		}
#else
        if (len > 0)
		{
            if(!timeCheck)
            {
                double CurrentTime = CACurrentMediaTime();
                timeCheck = true;
            }
            [receivedData appendBytes:buffer length:len];
			if (buffer[len-1]==0)
            {
                [self notifyData:receivedData];
                [receivedData release];
                receivedData = nil;
                break;
				//[self notifyData:receivedData];//Commented this to avoid unnessessary data recieved callbacks
				//[receivedData setData:NULL];
				//continue;
			}
        }
#endif
        else {
			DLog(@"breaking from reading");
			//handle error.
            [delegate onSocketError];
			break;
		}
	}
}

- (void)checkConnection
{
	if (!connectionFlag)
    {
		if (connectionCheker)
        {
			[connectionCheker invalidate];
			connectionCheker=nil;
		}
		[delegate onSocketError];
	}
    else
    {
		connectionFlag=FALSE;
	}
}

- (void)readDataFromStream
{
    uint8_t buffer[4096];
    NSInteger len = 0;
    len = [iStream read:buffer maxLength:4096];
    if (!receivedData)
    {
        receivedData = [[NSMutableData alloc] init];
    }
    if (len > 0)
    {
        NSData* streamData = [NSData dataWithBytesNoCopy:buffer length:len freeWhenDone:NO];
        NSString* streamString = [[NSString alloc] initWithData:streamData encoding:NSUTF8StringEncoding];
        
        NSRange searchRange = NSMakeRange(0, len);
        while(searchRange.location != NSNotFound)
        {
            searchRange = [streamString rangeOfString:@"\0" options:NSCaseInsensitiveSearch];
            if(searchRange.location != NSNotFound)
            {
                NSString* subString = [streamString substringWithRange:NSMakeRange(0, searchRange.location)];
                NSData* data = [subString dataUsingEncoding:NSUTF8StringEncoding];
                [receivedData appendData:data];
                if ([receivedData length] > 0)
                {
                    timeCheck = NO;
                    [self notifyData:receivedData];
                    [receivedData setLength:0];
                }
                
                streamString = [streamString substringFromIndex:(searchRange.location + searchRange.length)];
            }
            else if(streamString.length > 0)
            {
                NSData* data = [streamString dataUsingEncoding:NSUTF8StringEncoding];
                [receivedData appendData:data];
            }
        }
    }
    else
    {
        DLog(@"No buffer!");
    }
}

- (void)notifyData:(NSMutableData *)inReceivedData
{
//    DLog(@"length of data %d",[inReceivedData length]);
    [delegate onSocket:self data:inReceivedData];
}

- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent
{
	NSString *io;
	if (theStream == iStream) io = @"i>>";
	else io = @"o<<";
	
	NSString *event;
	switch (streamEvent)
	{
		case NSStreamEventNone:
        {
			event = @"NSStreamEventNone";
        }
			break;
		case NSStreamEventOpenCompleted:
        {
			event = @"NSStreamEventOpenCompleted";
            
			if(theStream==iStream)iStreamConnected=TRUE;
			if(theStream==oStream)oStreamConnected=TRUE;
			[self checkOpenConnection];
        }
			break;
		case NSStreamEventHasBytesAvailable:
        {
			event = @"NSStreamEventHasBytesAvailable";
            //DLog(@"readall  available");
            if (theStream == iStream)
            {
                [self readDataFromStream];
            }
        }
			break;
		case NSStreamEventHasSpaceAvailable:
        {
			event = @"NSStreamEventHasSpaceAvailable";
			if (theStream == oStream)
			{
				[self writeCurrentBuffer];
			}
        }
			break;
		case NSStreamEventErrorOccurred:
        {
			event = @"NSStreamEventErrorOccurred";
            DLog(@"Error description %@",[[theStream streamError] localizedDescription]);
			if (connectionCheker)
            {
				[connectionCheker invalidate];
				connectionCheker=nil;
			}
			[delegate onSocketError];
        }
			break;
		case NSStreamEventEndEncountered:
        {
			event = @"NSStreamEventEndEncountered";
            [theStream close];
            [theStream removeFromRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
            theStream = nil;
        }
			break;
		default:
			event = @"** Unknown";
	}
}

#pragma mark -

- (void)closeConnectionChecker
{
	if (connectionCheker)
    {
		[connectionCheker invalidate];
		connectionCheker=nil;
	}
}

- (void)dealloc
{
	DLog(@"EFSocket dealloc");
	[self closeConnectionChecker];
	[self closeCurrentConnection];
	delegate = nil;
}

- (void)setDelegate:(void*)value
{
    delegate = (__bridge id<TAJSocketDelegate>)(value);
}

- (void)disconnect
{
    isConnectionClosed = TRUE;
    [self closeCurrentConnection];
}

- (void)reconnect
{
    [self connect];
    isConnectionClosed = FALSE;
}

//check whether network data is available or not
- (BOOL)isNetworkDataAvailable
{
    if ([iStream hasBytesAvailable])
    {
        return TRUE;
    }
    return FALSE;
}

@end
