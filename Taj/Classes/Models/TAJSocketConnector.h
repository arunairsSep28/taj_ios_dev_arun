/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJSocketConnector.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 31/12/13.
 **/


#import <Foundation/Foundation.h>
#import <CFNetwork/CFSocketStream.h>

@protocol TAJSocketDelegate

- (void)onSocketError;
- (void)onSocketConnected:(id)inSocket;
- (void)onSocket:(id)inSocket data:(id)inReceivedData;

@end

@interface TAJSocketConnector : NSObject<NSStreamDelegate>
{
	NSInputStream *iStream;
	NSOutputStream *oStream;
	NSMutableArray *writeBufferArray;
	id<TAJSocketDelegate> delegate;
	BOOL iStreamConnected;
	BOOL oStreamConnected;
	int port;
    NSString *host;
	BOOL connectionFlag;
	NSTimer *connectionCheker;
    NSMutableData *receivedData;
    BOOL isConnectionClosed;
    char* dataArray;
    char* dataHolder;
    BOOL timeCheck;
}

- (id)initWithDelegate:(id)inDelegate onHost:(NSString *)inHost onPort:(UInt32)inPort;
- (BOOL)connect;
- (void)sendData:(NSData *)inData;
- (void)writeCurrentBuffer;
- (void)notifyData:(NSMutableData *)inReceivedData;
- (void)closeCurrentConnection;
- (void)closeConnectionChecker;
- (void)setDelegate:(void*)inValue;
- (void)disconnect;
- (void)reconnect;
//network data available or not
- (BOOL)isNetworkDataAvailable;
@end
