/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJClientServerCommunicator.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 07/01/14.
 **/

#import <Foundation/Foundation.h>
#import "TAJSocketConnector.h"
@interface TAJClientServerCommunicator : NSObject<TAJSocketDelegate>

@property(nonatomic,strong) TAJSocketConnector *socketConnector;
@property (nonatomic) NSMutableArray *onSocketDatas;

+ (id)sharedClientServerCommunicator;
+ (void)destroySharedClientServerCommunicator;
- (id)init;
- (void)createSocketAndConnect;
- (void)closeConnection;
- (void)sendData:(NSData *)inData;
- (void)reconnect;
- (void)disconnect;
- (void)processDataOnBackgroundThread:(NSData*)inReceivedData;
- (void)destroySocket;

@end
