/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		  TAJGameEngine.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by Raghavendra Kamat on 07/01/14.
 
 **/
#import "TAJGameEngine.h"
#import "TAJConfigProvider.h"
#import "TAJClientServerCommunicator.h"
#import "TAJUtilities.h"

#import "TAJLoginViewController.h"
#import "TAJTableViewController.h"
#import "TAJGameTableController.h"
#import "TAJUserModel.h"
#import "TAJLobby.h"
#import "TAJGameTable.h"
#import "TAJConstants.h"
#import "TAJProgressAlertView.h"
#import "TAJAppDelegate.h"
#import "TAJInfoPopupViewController.h"

@interface TAJGameEngine()<InfoPopupViewDelegate>
{
    TAJAppDelegate *appDelegate;
}
@property (strong, nonatomic)TAJInfoPopupViewController *infoPopupViewController;

@property (nonatomic) NSTimer* connectedTimer;
@end


@implementation TAJGameEngine
static TAJGameEngine *sharedEngine = nil;

+ (id)sharedGameEngine;
{
    if (sharedEngine == nil)
    {
        sharedEngine = [[TAJGameEngine alloc] init];
    }
    return sharedEngine;
}

- (id)init
{
    self = [super init];
    if (self) {
        _usermodel = Nil;
        _msgUUID = Nil;
        _heartBeat = Nil;
        _currentController = Nil;
        _lobby = Nil;
        _socketReconnectAlert = Nil;
        self.socketReconnectAlert = [[UIAlertView alloc] initWithTitle:RECONNECT message:SOCKET_RECONNECT_MESSAGE delegate:self cancelButtonTitle:RECONNECT otherButtonTitles:nil];
        _iAmBackAlert = Nil;
        self.iAmBackAlert = [[UIAlertView alloc] initWithTitle:I_AM_BACK_HEADER message:I_AM_BACK_MESSAGE delegate:self cancelButtonTitle:I_AM_BACK otherButtonTitles:nil];
        _isReconnectRequest = FALSE;
        _activityIndicator = NULL;
        _sentTime = NULL;
        _currentTime = NULL;
        _isHeartBeatRecieved = TRUE;
        _isSendingHeartBeatForFirstTime = TRUE;
        _activityAlertForReconnecting = Nil;
        _isReconnectActivityAlertActive = FALSE;
        _joinTableAfterGettingDetails = TRUE;
        _joinTableTypeJokerNoJoker = FALSE;
        _buyInAmount = Nil;
        _isLoggedIn = FALSE;
        _isOtherLoggedIn = FALSE;
        _alertForOtherLogin = Nil;
        _webViewController = Nil;
        _showAutoplayStatusPopup = FALSE;
        _arrayForSlotsInfo = [NSMutableArray array];
        _timeOut = 0.0f;
        _isFirstGameStarted = NO;
        _isSecondGameStarted = NO;
        _quitTableValues=[[NSMutableDictionary alloc]init];
        self.connectedTimer = nil;
    }
    return self;
}

- (void)findTopMostView
{
    appDelegate = (TAJAppDelegate *)[[UIApplication sharedApplication] delegate];
    self.infoPopupViewController.delegate = self;
    [appDelegate.window.rootViewController.view addSubview:self.infoPopupViewController.view];
    [appDelegate.window.rootViewController.view bringSubviewToFront:self.infoPopupViewController.view];
    
}

- (void)showAlertPopupWithMessage:(NSString *)popupMessage
{
    if (!self.infoPopupViewController)
    {
        self.infoPopupViewController = [[TAJInfoPopupViewController alloc] initWithNibName:@"TAJInfoPopupViewController" bundle:Nil delegate:nil tag:1 popupButtonType:eOK_Type message:popupMessage];
        
        [self findTopMostView];
    }
}

- (void)okButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

- (void)closeButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

- (void)noButtonPressedForInfoPopup:(id)object
{
    [self.infoPopupViewController hide];
    self.infoPopupViewController = Nil;
}

#pragma mark AlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    NSString *title = [alertView buttonTitleAtIndex:buttonIndex];
    //Author : RK
    if(NSOrderedSame == [title compare:RECONNECT])
    {
        UIView *parentView = [alertView superview];
        if ([[TAJUtilities sharedUtilities] isInternetConnected])
        {
            DLog(@"RECONNECT was selected.");
            
            self.activityIndicator = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
            [self onReconnect];
            NSLog(@"SOCKET CONNECTED");
            [self saveDataIntoCoreDataWithEventName:@"Engine Connect"];
            [alertView dismissWithClickedButtonIndex:buttonIndex animated:TRUE];
            [parentView addSubview:self.activityIndicator];
            CGRect windowSize = [[UIScreen mainScreen] bounds];
            self.activityIndicator.center = CGPointMake(windowSize.size.width * 0.75f, windowSize.size.height * 0.30f);
            [self.activityIndicator startAnimating];
        }
        else
        {
            [self saveDataIntoCoreDataWithEventName:@"Engine Disconnect"];
            
            NSLog(@"SOCKET DISCONNECTED");
            self.socketReconnectAlert = [[UIAlertView alloc] initWithTitle:RECONNECT message:SOCKET_RECONNECT_MESSAGE delegate:self cancelButtonTitle:RECONNECT otherButtonTitles:nil];
            [self showSocketReconnectDialog];
        }
    }
    
    if (NSOrderedSame == [title compare:I_AM_BACK])
    {
        
        [[NSNotificationCenter defaultCenter] postNotificationName:PLAYER_RECONNECTED object:Nil];
        
        if ([[TAJUtilities sharedUtilities] isInternetConnected])
        {
            if (self.usermodel && self.usermodel.userId)
            {
                DLog(@"in gameengine sending autoplaystatus");
            }
        }
    }
}

#pragma mark Parsing data and handling tags

- (void)parseRecievedData:(NSDictionary*)dictionary
{
    NSString *tagName = NULL;
    
    tagName = [dictionary valueForKeyPath:TAJ_COMMAND_NAME];
    
    //lingam
    if (NSOrderedSame == [tagName compare:EVENT_COMMAND])
    {
        //[self processTheEventTag:dictionary];
       NSString *data = [dictionary valueForKeyPath:TAJ_EVENT_NAME];
        if ([data isEqualToString:HEART_BEAT] || [data isEqualToString:PLAYER_QUIT])
        {
           // [self processHeartBeatEvent:dictionary];
            // DLog(@"************ processTheEventTag : HEART_BEAT");
           // NSLog(@"HEART BEAT : %@",dictionary);
        }else{
            // DLog(@"Received data : %@",dictionary);
        }
    }
    else{
       DLog(@"Received data : %@",dictionary);
    }
   
    
    if (NSOrderedSame == [tagName compare:AUTH_REQ_COMMAND])
    {
        [self processTheAuthReqTag:dictionary];
    }
    
    if (NSOrderedSame == [tagName compare:REPLY_COMMAND])
    {
        [self processTheReplyTag:dictionary];
    }
    
    if (NSOrderedSame == [tagName compare:EVENT_COMMAND])
    {
        [self processTheEventTag:dictionary];
    }
    
    if (NSOrderedSame == [tagName compare:REQUEST_COMMAND])
    {
        [self processTheRequestTag:dictionary];
      
    }
}

- (void)processTheReplyTag:(NSDictionary*)dictionary
{
   // NSLog(@"PROCESS DATA : %@",dictionary);
    
    NSString *data = NULL, *code = NULL;
    data = [dictionary valueForKeyPath:TAJ_DATA];
//    DLog(@" %@ ===s=======s==s=s=s=s==s=s=s=s=s=s==s=s==s=s=  ,  %@",data ,dictionary );
    code = [dictionary valueForKeyPath:TAJ_CODE];
    if (NSOrderedSame == [code compare:CODE_200])
    {
        //    DLog(@"********************************table list %d",[data compare:@"list_table"]);
        
        if (data && (NSOrderedSame == [data compare:QUIT_TABLE]))
        {
            [[NSNotificationCenter defaultCenter] postNotificationName:QUIT_TABLE_REPLY object:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:JOIN_TABLE])) {
    
            [self processJoinTableReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:GET_TABLE_DETAILS]))
        {
            [self processGetTableDetails:dictionary];
//            if(!self.isTournament){
//                [self processGetTableDetails:dictionary];
//            }else{
//                [self processGetTableDetailsEvent:dictionary];
//            }
//            DLog(@"get_table_details %@",data);
        }
        else if (data && (NSOrderedSame == [data compare:GET_TABLE_EXTRA]))
        {
            [self processGetTableExtraReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:LIST_TABLE]))
        {
//            DLog(@"list_table %@",data);
          //  NSLog(@"LIST_TABLE : %@",dictionary);
            [self processListTable:dictionary];
        }
        
        //added to update fav list
        else if (data && (NSOrderedSame == [data compare:FAVOURITE_REPLY]))
        {
            NSLog(@"REFRESH GAME LIST");
            //[self requestListTable];
        }
        //end here
        
        else if (data && (NSOrderedSame == [data compare:CHIPRELOAD]))
        {
            //DLog(@"list_table %@",data);
            //NSLog(@"CHIPRELOAD : %@",dictionary);
            [self processFunChipsData:dictionary];
        }
        
        else if (data && (NSOrderedSame == [data compare:LIST_TOURNAMENTS]))
        {
            //NSLog(@"TOURNEY LIST : %@",dictionary);
            [self processTournamentListTable:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:GET_TOURNAMENT_DETAIL]))
        {
            [self processTournamentDetails:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:GET_TOURNAMENT_PRIZE_LIST]))
        {
            [self processTournamentPrizeList:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:TOURNAMENT_GET_LEVEL_TIMER]))
        {
             [self processTournament:dictionary notification:TOURNAMENT_GET_LEVEL_TIMER];
        }
        else if (data && (NSOrderedSame == [data compare:GET_TOURNAMENT_WAIT_LIST]))
        {
            [self processTournament:dictionary notification:GET_TOURNAMENT_WAIT_LIST];
        }
        else if (data && (NSOrderedSame == [data compare:GET_TOURNAMENT_REGISTERED_PLAYER]))
        {
            [self processTournament:dictionary notification:GET_TOURNAMENT_REGISTERED_PLAYER];
        }
        else if (data && (NSOrderedSame == [data compare:GET_TOURNAMENT_LEADER_BOARD]))
        {
            [self processTournament:dictionary notification:GET_TOURNAMENT_LEADER_BOARD];
        }
        else if (data && (NSOrderedSame == [data compare:TOURNAMENT_TABLES]))
        {
            [self processTournament:dictionary notification:TOURNAMENT_TABLES];
        }
        
        else if (data && (NSOrderedSame == [data compare:TOURNAMENT_REGISTER]))
        {
            [self processTournament:dictionary notification:TOURNAMENT_REGISTER];
        }
        
        else if (data && (NSOrderedSame == [data compare:TOURNAMENT_DEREGISTER]))
        {
            [self processTournament:dictionary notification:TOURNAMENT_DEREGISTER];
        }
        
        else if (data && (NSOrderedSame == [data compare:GET_TOURNAMENT_PRIZE_DISTRIBUTION]))
        {
            [self processTournament:dictionary notification:GET_TOURNAMENT_PRIZE_DISTRIBUTION];
        }
        else if (data && (NSOrderedSame == [data compare:LEVEL_TOP_PLAYER]))
        {
            [self processTournament:dictionary notification:LEVEL_TOP_PLAYER];
        }
        else if (data && (NSOrderedSame == [data compare:LEAVE_TOURNAMENT]))
        {
            [self processTournament:dictionary notification:LEAVE_TOURNAMENT];
        }
        
        else if (data && (NSOrderedSame == [data compare:TURN_EXTRA_TIME]))
        {
            [self processTurnExtraTime:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:CHECKMELD_REPLY]))
        {
            [self processCheckMeldReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:MELD_REPLY]))
        {
            [self processMeldReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:AUTO_PLAY_STATUS]))
        {
            [self processAutoPlayStatusReply:dictionary];
        }
        
        else if (data && (NSOrderedSame == [data compare:SPLIT_TAG]))
        {
            [self processSplitReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:SEARCH_JOIN_TABLE]))
        {
            [self processSearchJoinTableReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:REBUYIN]))
        {
            [self processRebuyinReply:dictionary];
        }
        else if (data && (NSOrderedSame == [data compare:TOURNEY_REBUYIN_REPLY]))
        {
            [self processTournament:dictionary notification:TOURNEY_REBUYIN_REPLY];
        }
        
        else if (data && (NSOrderedSame == [data compare:PLAYER_COUNT]))
        {
            //<reply code="200" data="player_count" msg_uuid="6683fa8e-c7bc-11e3-a352-002522ee32f3" totalplayers="1" type="+OK" timestamp="1397911125.0"/>
            [self processPlayerCount:dictionary];
        }
        
        else if (data && (NSOrderedSame == [data compare:TABLE_LENGTH]))
        {
            //<reply code="200" data="player_count" msg_uuid="6683fa8e-c7bc-11e3-a352-002522ee32f3" totalplayers="1" type="+OK" timestamp="1397911125.0"/>
            [self processTableLength:dictionary];
        }
        
        else if (data && (NSOrderedSame == [data compare:STAND_UP]))
        {
            [self processStandUpReply:dictionary];
        }
        
        else if(data && (NSOrderedSame == [data compare:AUTOPLAY_COUNT]))
        {
            [self processAutoPlayCountReply:dictionary];
        }
        
        else
        {
            [self processAuthenticationMessage:dictionary];
        }
    }
    else if (NSOrderedSame == [code compare:CODE_100])
    {
        [self showAlertPopupWithMessage:@"Engine is Under Maintanance"];
    }
    else if (NSOrderedSame == [code compare:CODE_500])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:LIST_TABLE_UPDATE_AFTER_INVALID object:Nil];
    }
    //by ratheesh
    else if (NSOrderedSame == [code compare:CODE_474])
    {
        APP_DELEGATE.isManualLogin = YES;
        APP_DELEGATE.gameTableIndex = 1;
        [self manualLogin];
        
    }
    else if (NSOrderedSame == [code compare:CODE_483])
    {
        NSLog(@"PROCESS ERROR CODE : %@",code);
        [[NSNotificationCenter defaultCenter] postNotificationName:ERROR_NOTIFICATION object:dictionary];
    }
    
    // end ratheesh
    
    else
    {
//        DLog(@"Code  not 200 %@",dictionary);
        [[NSNotificationCenter defaultCenter] postNotificationName:ERROR_NOTIFICATION object:dictionary];
        /*{
         "TAJ_code" = 7004;
         "TAJ_data" = "list_tournaments";
         "TAJ_msg_uuid" = "8A6A2F80-E6A8-4C30-8BFC-09DC7B3378F8";
         "TAJ_name" = reply;
         "TAJ_timestamp" = "1519191579.0";
         "TAJ_type" = "-ERR";
         }*/
    }
    if ([code isEqualToString:CODE_SEARCH_JOIN_ERROR])
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_JOIN_ERROR_NOTIFICATION object:dictionary];
    }
}

- (void)manualLogin {
    //ratheesh
    APP_DELEGATE.gameTableIndex = 1;
    //end
    
    NSLog(@"Manual Login");
    TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
    [delegate loadLoginScreenWindow];
}

- (void)processTheAuthReqTag:(NSDictionary*)dictionary
{
    NSLog(@"AuthReq : %@",dictionary);
    
    NSString *msg_uuid = NULL , *deviceID ;
    
    msg_uuid = [dictionary valueForKeyPath:TAJ_MSG_UUID];
    //remove previous lobby data
    [self.lobby.allTableEvents removeAllObjects];
    self.loginTime = CACurrentMediaTime();
    DLog(@"logintime %f",self.loginTime);
    
    self.msgUUID = [msg_uuid mutableCopy];
    char api[API_CHARACTERS_LIMIT];
    if ([TAJUtilities isIPhone])
    {
        deviceID = @"Mobile";//@"IPHONE";
    }
    else
    {
        deviceID = @"Tablet";//@"IPAD";
    }
    //"<authrep user_id=\"redimuser4\" player_in=\"new_lobby\" password=\"123456\" session_id=\"\" system=\"TajRummy\" msg_uuid=\"CC003665-0CCA-4916-BA98-98AE343F352B\" DEVICE_ID=\"Mobile\" device_type=\"Mobile\", client_type=\"IOS\" os=\"IOS\" siteid=\"4\"/>"
    NSString * userName = @"None";
    NSString * password = @"None";
    NSString * sessionId = @"None";
    
    if (APP_DELEGATE.isManualLogin) {
        sprintf(api,[kLoginAPI UTF8String], [self.usermodel.displayName UTF8String], [self.usermodel.password UTF8String],[sessionId UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[deviceID UTF8String],[deviceID UTF8String]);
        NSLog(@"IF KLOGIN : %@",kLoginAPI);
    }
    
    else {
        sprintf(api,[kLoginAPI UTF8String], [self.usermodel.displayName UTF8String], [self.usermodel.password UTF8String],[sessionId UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[deviceID UTF8String],[deviceID UTF8String]);
        //sprintf(api, [kLoginAPI UTF8String],[self.usermodel.sessionId UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[deviceID UTF8String],[deviceID UTF8String]);
        NSLog(@"ELSE KLOGIN : %@",kLoginAPI);
    }
    //NSLog(@"KLOGIN : %@",kLoginAPI);
    //sprintf(api,[kLoginAPI UTF8String], [self.usermodel.displayName UTF8String], [self.usermodel.password UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[deviceID UTF8String]);
    [self requestAPI:api];
}

- (void)processTheEventTag:(NSDictionary*)dictionary
{

    NSString *eventName = NULL;
    eventName = [dictionary valueForKeyPath:TAJ_EVENT_NAME];
    
    if ([eventName isEqualToString:@"HEART_BEAT"]) {
        //
    }
    else {
       // NSLog(@"EventTag : %@",dictionary);
    }
    
    if (eventName)
    {
        if (NSOrderedSame == [eventName compare:FUNCHIPS_RELOAD])
        {
            [self processFunChipsData:dictionary];
        }
        
        if (NSOrderedSame == [eventName compare:START_GAME])
        {
            [self processStartGameEvent:dictionary];
            NSLog(@"START_GAME");
           // NSLog(@"START_GAME DICT : %@",dictionary);
        }
        
        if (NSOrderedSame == [eventName compare:DEVICE_CHANGE])
        {
            [self processDeviceChangeEvent:dictionary];
        }
        
        if (NSOrderedSame == [eventName compare:ENGINE_STATUS])
        {
            [self processEngineStatusEvent:dictionary];
        }
        
        if (NSOrderedSame == [eventName compare:GAME_SCHEDULE])
        {
            [self processGameScheduleEvent:dictionary];
        }
        
        if (NSOrderedSame == [eventName compare:BALANCE_UPDATE])
        {
            //BALANCE UPDATE
            //DLog(@"************ processTheEventTag : BALANCE_UPDATE");
            [self processBalanceUpdateEvent:dictionary];
        }
        
        if (NSOrderedSame == [eventName compare:SHOW_EVENTS])
        {
            [self processShowEvent:dictionary];
//            DLog(@"************ processTheEventTag : SHOW");
        }
        
        if (NSOrderedSame == [eventName compare:TABLE_CLOSED])
        {
            [self processTableClosedEvent:dictionary];
//            DLog(@"************ processTheEventTag : TABLE_CLOSED");
        }
        
        if (NSOrderedSame == [eventName compare:GAME_SITTING_SEQUENCE])
        {
            [self processSittingSeqEvent:dictionary];
//            DLog(@"************ processTheEventTag : SITTING_SEQ");
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_ELIMINATE])
        {
            [self processPlayerEliminateEvent:dictionary];
//            DLog(@"************ processTheEventTag : PLAYER_ELIMINATE");
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_DROP])
        {
            [self processPlayerDropEvent:dictionary];
//            DLog(@"************ processTheEventTag : PLAYER_DROP");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_UPDATE])
        {
            [self processTurnUpdateEvent:dictionary];
//            DLog(@"************ processTheEventTag : TURN_UPDATE");
        }
        
        if (NSOrderedSame == [eventName compare:CARD_DISCARD])
        {
            [self processCardDiscardEvent:dictionary];
//            DLog(@"************ processTheEventTag : CARD_DISCARD");
        }
        
        if (NSOrderedSame == [eventName compare:CARD_PICK])
        {
            [self processCardPickEvent:dictionary];
//            DLog(@"************ processTheEventTag : CARD_PICK");
        }
        
        if (NSOrderedSame == [eventName compare:SEND_DEAL])
        {
            [self processSendDealEvent:dictionary];
//            DLog(@"************ processTheEventTag : SEND_DEAL");
            NSLog(@"************ processTheEventTag : SEND_DEAL");
            NSLog(@"DEAL CARDS : %@",dictionary);
        }
        
        if (NSOrderedSame == [eventName compare:GAME_END])
        {
            [self processGameEndEvent:dictionary];
//            DLog(@"************ processTheEventTag : GAME_END");
        }
        
        if (NSOrderedSame == [eventName compare:STACK_RESUFFLE])
        {
            [self processStackResuffleEvent:dictionary];
//            DLog(@"************ processTheEventTag : STACK_RESUFFLE");
        }
        
        if (NSOrderedSame == [eventName compare:SEND_STACK])
        {
            [self processSendStackEvent:dictionary];
//            DLog(@"************ processTheEventTag : SEND_STACK");
        }
        
        if (NSOrderedSame == [eventName compare:NEW_GAME])
        {
            [self processNewGameEvent:dictionary];
//            DLog(@"************ processTheEventTag : NEW_GAME");
        }
        
        if (NSOrderedSame == [eventName compare:CHAT_MSG])
        {
            [self processChatMSGEvent:dictionary];
//            DLog(@"************ processTheEventTag : CHAT_MSG");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_TIMEOUT])
        {
            [self processTurnTimeOutEvent:dictionary];
            NSLog(@"TURN OUT : %@",dictionary);
//            DLog(@"************ processTheEventTag : TURN_TIMEOUT");
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_JOIN])
        {
            [self processPlayerJoinEvent:dictionary];
//            DLog(@"************ processTheEventTag : PLAYER_JOIN");
        }
        
        if (NSOrderedSame == [eventName compare:HEART_BEAT])
        {
            [self processHeartBeatEvent:dictionary];
           // DLog(@"************ processTheEventTag : HEART_BEAT");
        }
        
        if (NSOrderedSame == [eventName compare:REPORT_BUG])
        {
            [self processReportBugEvent:dictionary];
           // DLog(@"************ processTheEventTag : REPORT_BUG");
        }
        
        if (NSOrderedSame == [eventName compare:SHOW_DEAL])
        {
            [self processShowDealEvent:dictionary];
            //DLog(@"************ processTheEventTag : SHOW_DEAL");
        }
        
        if (NSOrderedSame == [eventName compare:TABLE_START])
        {
            [self processTableStartEvent:dictionary];
           // DLog(@"************ processTheEventTag : TABLE_START");
        }
        
        if (NSOrderedSame == [eventName compare:TABLE_TOSS])
        {
            [self processTableTossEvent:dictionary];
            //DLog(@"************ processTheEventTag : TABLE_TOSS");
            NSLog(@"************ processTheEventTag : TABLE_TOSS");
            NSLog(@"TABLE_TOSS DICT : %@",dictionary);
            
        }
        
        if (NSOrderedSame == [eventName compare:PRE_GAME_RESULT]) {
            
            [self processPreGameResult:dictionary];
           // DLog(@"************ processTheEventTag : PRE_GAME_RESULT");
        }
        
        if (NSOrderedSame == [eventName compare:GAME_RESULT]) {
            [self processGameResult:dictionary];
           // DLog(@"************ processTheEventTag : GAME_RESULT");
        }
        
        if (NSOrderedSame == [eventName compare:SPLIT_FALSE]) {
            [self processSplitFalseEvent:dictionary];
            //DLog(@"************ processTheEventTag : SPLIT_FALSE");
        }
        
        if (NSOrderedSame == [eventName compare:SPLIT_STATUS])
        {
            [self processSplitStatusEvent:dictionary];
           // DLog(@"************ processTheEventTag : SPLIT_STATUS");
        }
        
        if (NSOrderedSame == [eventName compare:SPLIT_RESULT])
        {
            [self processSplitResultEvent:dictionary];
           // DLog(@"************ processTheEventTag : SPLIT_RESULT");
        }
        
        if (NSOrderedSame == [eventName compare:REJOIN_TAG])
        {
            [self processRejoinEvent:dictionary];
            //DLog(@"************ processTheEventTag : REJOIN");
        }
        
        if (NSOrderedSame == [eventName compare:GET_TABLE_DETAILS])
        {
            [self processGetTableDetailsEvent:dictionary];
           // DLog(@"************ processTheEventTag : get_table_details");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_EXTRA_TIME_ANOTHER])
        {
            [self processTurnExtraTime:dictionary];
           // DLog(@"************ processTheEventTag : TURN_EXTRATIME");
        }
        
        if (NSOrderedSame == [eventName compare:GAME_DESCHEDULE])
        {
            [self processGameDeschedule:dictionary];
           // DLog(@"************ processTheEventTag : GAME_DESCHEDULE");
        }
        
        if (NSOrderedSame == [eventName compare:MELD_SUCCESS_CURRENT_PLAYER])
        {
            [self processMeldSuccess:dictionary];
           // DLog(@"************ processTheEventTag : meld_sucess");
        }
        
        if (NSOrderedSame == [eventName compare:BEST_OF_WINNER])
        {
            [self processBestOfWinnerEvent:dictionary];
            //DLog(@"************ processTheEventTag : BEST_OF_WINNER");
        }
        
        if (NSOrderedSame == [eventName compare:POOL_WINNER])
        {
            [self processPoolWinnerEvent:dictionary];
           // DLog(@"************ processTheEventTag : POOL_WINNER");
        }
        
        if (NSOrderedSame == [eventName compare:MELD_FAIL]) {
            [self processMeldFailEvent:dictionary];
           // DLog(@"************ processTheEventTag : MELD_FAIL");
        }
        
        if (NSOrderedSame == [eventName compare:PLAYER_QUIT])
        {
            [self processPlayerQuitEvent:dictionary];
           // DLog(@"************ processTheEventTag : PLAYER_QUIT");
        }
        
        if (NSOrderedSame == [eventName compare:TURN_EXTRATIME_RECONNECT]) {
            [self processTurnExtraTimeReconnectEvent:dictionary];
           // DLog(@"************ processTheEventTag : TURN_EXTRATIME_RECONNECT");
        }
        
        if (NSOrderedSame == [eventName compare:MELD_EXTRATIME_RECONNECT]) {
            [self processMeldExtraTimeReconnectEvent:dictionary];
           // DLog(@"************ processTheEventTag : MELD_EXTRATIME_RECONNECT");
        }
        
        if (NSOrderedSame == [eventName compare:SHOW_EXTRATIME_RECONNECT]) {
            [self processShowExtraTimeReconnectEvent:dictionary];
           // DLog(@"************ processTheEventTag : SHOW_EXTRATIME_RECONNECT");
        }
        
        if (NSOrderedSame == [eventName compare:REBUYIN]) {
            [self processRebuyinEvent:dictionary];
            //DLog(@"************ processTheEventTag : REBUYIN");
        }
        
        if (NSOrderedSame == [eventName compare:OTHER_LOGIN])
        {
            [self processOtherLoginEvent:dictionary];
            //DLog(@"************ processTheEventTag : OTHER_LOGIN");
        }
        
        if (NSOrderedSame == [eventName compare:AUTO_PLAY_STATUS])
        {
            [self processAutoPlayStatusEvent:dictionary];
            //DLog(@"************ processTheEventTag : AUTO_PLAY_STATUS");
        }
        
        if (NSOrderedSame == [eventName compare:SEND_SLOTS])
        {
            [self processSendSlotsEvent:dictionary];
           // DLog(@"************ processSendSlotsEvent : SEND_SLOTS");
        }
        if (NSOrderedSame == [eventName compare:TOURNAMENT_LEVEL_START])
        {
            [self processTournament:dictionary notification:TOURNAMENT_LEVEL_START];
        }
        if (NSOrderedSame == [eventName compare:TOURNAMENT_LEVEL_SCHEDULE])
        {
            [self processTournament:dictionary notification:TOURNAMENT_LEVEL_SCHEDULE];
        }
        if (NSOrderedSame == [eventName compare:TOURNAMENT_LEVEL_END])
        {
            [self processTournament:dictionary notification:TOURNAMENT_LEVEL_END];
        }
        if (NSOrderedSame == [eventName compare:TOURNAMENT_RESULT])
        {
            [self processTournament:dictionary notification:TOURNAMENT_RESULT];
        }
        if (NSOrderedSame == [eventName compare:SHOW_TOURNAMENT])
        {
            [self processTournament:dictionary notification:SHOW_TOURNAMENT];
        }
        if (NSOrderedSame == [eventName compare:END_TOURNAMENT])
        {
            [self processTournament:dictionary notification:END_TOURNAMENT];
        }
        if (NSOrderedSame == [eventName compare:TOURNAMENT_TO_START])
        {
            [self processTournament:dictionary notification:TOURNAMENT_TO_START];
        }
        if (NSOrderedSame == [eventName compare:START_TOURNAMENT])
        {
            [self processTournament:dictionary notification:START_TOURNAMENT];
        }
        if (NSOrderedSame == [eventName compare:START_REGISTRATION])
        {
            [self processTournament:dictionary notification:START_REGISTRATION];
        }
        if (NSOrderedSame == [eventName compare:STOP_REGISTRATION])
        {
            [self processTournament:dictionary notification:STOP_REGISTRATION];
        }
        if (NSOrderedSame == [eventName compare:STOP_CANCEL_REGISTRATION])
        {
            [self processTournament:dictionary notification:STOP_CANCEL_REGISTRATION];
        }
        if (NSOrderedSame == [eventName compare:DISQUALIFIED])
        {
            [self processTournament:dictionary notification:DISQUALIFIED];
        }
        if (NSOrderedSame == [eventName compare:PLAYER_REGISTERED])
        {
            [self processTournament:dictionary notification:PLAYER_REGISTERED];
        }
        if (NSOrderedSame == [eventName compare:PLAYER_DEREGISTERED])
        {
            [self processTournament:dictionary notification:PLAYER_DEREGISTERED];
        }
        if (NSOrderedSame == [eventName compare:BREAKUPTIME])
        {
            [self processTournament:dictionary notification:BREAKUPTIME];
        }
        if (NSOrderedSame == [eventName compare:TOURNEY_END_WAIT])
        {
            [self processTournament:dictionary notification:TOURNEY_END_WAIT];
        }
        if (NSOrderedSame == [eventName compare:TOURNEY_BALANCE])
        {
            [self processTournament:dictionary notification:TOURNEY_BALANCE];
        }
        if (NSOrderedSame == [eventName compare:PLAYERS_RANK])
        {
            [self processTournament:dictionary notification:PLAYERS_RANK];
        }
        if (NSOrderedSame == [eventName compare:TOURNEY_REBUYIN_EVENT]) {
            [self processTournamentRebuyinEvent:dictionary notification:TOURNEY_REBUYIN_EVENT];
        }
        if (NSOrderedSame == [eventName compare:TOURNEY_ELEMINATE]){
           [self processTournament:dictionary notification:TOURNEY_ELEMINATE];
        }
    }
}

- (void)processTheRequestTag:(NSDictionary*)dictionary
{
   // NSLog(@"CHECK REQUEST TAG : %@",dictionary);
    
    NSString *commandName = NULL;
    commandName = [dictionary valueForKeyPath:TAJ_REQUEST_COMMAND_TYPE];
    
    if (NSOrderedSame == [commandName compare:CHIPRELOAD])
    {
        [self processFunChipsData:dictionary];
    }
    
    if (NSOrderedSame == [commandName compare:MELD_REQUEST])
    {
        [self processMeldRequest:dictionary];
    }
    
    if (NSOrderedSame == [commandName compare:REJOIN_TAG])
    {
        [self processRejoinRequest:dictionary];
    }
    
    if (NSOrderedSame == [commandName compare:SPLIT_TAG])
    {
        [self processSplitRequest:dictionary];
    }
    if (NSOrderedSame == [commandName compare:SMART_CORRECTION])
    {
        [self processSmartCorrectionEvent:dictionary];
    }
    if (NSOrderedSame == [commandName compare:REQUEST_JOIN_TABLE])
    {
        [self processTournament:dictionary notification:REQUEST_JOIN_TABLE];
        self.tournamentID = dictionary[@"TAJ_tournament_id"];
    }
}

- (void)createUserModelWithUserName:(NSString*)userName password:(NSString*)password sessionId:(NSString *)sessionId
{
    NSLog(@"UserModel SESSION ID : %@",sessionId);
    //Should have to make reconnect request false in case tries to login again
    _isReconnectRequest = FALSE;
    TAJUserModel *model = [[TAJUserModel alloc] initWithName:userName password:password sessionId:sessionId];
    self.usermodel = model;
}

- (void)sendDataToServer:(NSData*)data
{
    [[TAJClientServerCommunicator sharedClientServerCommunicator] sendData:data];
}

- (void)scheduleHeartBeat
{
#if ENABLE_HEART_BEAT
    if (!_heartBeat)
    {
        self.isSendingHeartBeatForFirstTime = TRUE;
        self.isHeartBeatRecieved = TRUE;
        self.currentTime = Nil;
        self.sentTime = Nil;
#if ENABLE_HEART_BEAT_IMPLEMENTATION_TYPE_TWO
        self.heartBeat =[NSTimer scheduledTimerWithTimeInterval:1.0f target:self selector:@selector(heartBeatMessage) userInfo:nil repeats:YES];
        [[NSRunLoop mainRunLoop] addTimer:self.heartBeat forMode:NSRunLoopCommonModes];
#else
        self.heartBeat =[NSTimer scheduledTimerWithTimeInterval:0.1f target:self selector:@selector(heartBeatMessage) userInfo:nil repeats:YES];
#endif
        
#if DONT_USE_RUNLOOPCOMMONMODE
#else
        //[[NSRunLoop mainRunLoop] addTimer:self.heartBeat forMode:NSRunLoopCommonModes];
#endif
    }
    
#else
    return;
#endif
}

- (void)heartBeatMessage
{
#if ENABLE_TIMER_DBUGGING
//    DLog(@"update timer for heartBeat ");
#endif
    
#if ENABLE_HEART_BEAT_IMPLEMENTATION_TYPE_TWO
    if (_timeOut > 10.0f)
    {
        DLog(@"Manually disconnecting...");
        _timeOut = 0.0f;
        [[TAJClientServerCommunicator sharedClientServerCommunicator] destroySocket];
        [self onError];//Calling on error for timeout
    }
    else
    {
        BOOL isReadindDataFromNetwork = [[[TAJClientServerCommunicator sharedClientServerCommunicator] socketConnector] isNetworkDataAvailable];
        if (!isReadindDataFromNetwork)
        {
//            DLog(@"update timer for heartBeat timeout %f ",_timeOut);
            _timeOut += 1.0f;
        }
        else
        {
            DLog(@"hearbeat else");
        }
        
        if (self.arrayForSlotsInfo && [self.arrayForSlotsInfo count] > 0)
        {
            NSString *info = Nil;
            info = [self getInfoStringForDictionary:[self.arrayForSlotsInfo objectAtIndex:0]];
            [self.arrayForSlotsInfo removeObjectAtIndex:0];
            char api[API_CHARACTERS_LIMIT];
            sprintf(api, [kHeartBeatWithSlotInfo UTF8String],[self.usermodel.nickName UTF8String], [info UTF8String]);
            [self requestAPI:api];
            _timeOut = 0;
        }
        else
        {
            char api[API_CHARACTERS_LIMIT];
            sprintf(api, [kHeartBeat UTF8String],[self.usermodel.nickName UTF8String]);
            [self requestAPI:api];
        }
    }
#else
    
    NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];
    
    NSTimeInterval timeDifference = 0.0f;
    if (self.currentTime && self.sentTime)
    {
        timeDifference = [self.currentTime timeIntervalSinceDate:self.sentTime];
    }
    
    if ((self.isHeartBeatRecieved && timeDifference > 2.7f) || self.isSendingHeartBeatForFirstTime)
    {
        if (self.isSendingHeartBeatForFirstTime)
        {
            self.isSendingHeartBeatForFirstTime = FALSE;
        }
        
        if (self.usermodel && self.usermodel.nickName)
        {
            //Resetting values
            self.isHeartBeatRecieved = FALSE;
            self.currentTime = [NSDate date];
            self.sentTime = self.currentTime;
            DLog(@"HEARTBEAT SENT");
            char api[API_CHARACTERS_LIMIT];
            sprintf(api, [kHeartBeat UTF8String],[self.usermodel.nickName UTF8String]);
            [self requestAPI:api];
        }
    }
    else
    {
        self.currentTime = [NSDate date];
    }
    
    if (self.currentTime && self.sentTime)
    {
        NSTimeInterval timeDifferenceInternal = [self.currentTime timeIntervalSinceDate:self.sentTime];
        DLog(@"timeDifference : %lf",timeDifferenceInternal);
        if (timeDifferenceInternal > 20.0f)
        {
            DLog(@"Destroying socket");
            [[TAJClientServerCommunicator sharedClientServerCommunicator] destroySocket];
            [self onError];//Calling on error for timeout
        }
    }
#endif
    
}

- (void)destroySocketOnIncommingCall
{
    [[TAJClientServerCommunicator sharedClientServerCommunicator] destroySocket];
    [self onError];
}

- (void)onError
{
    DLog(@"onerror called in game engine");
    NSLog(@"onerror called in game engine");
    if(self.connectedTimer)
    {
        [self.connectedTimer invalidate];
        self.connectedTimer  = nil;
    }
#if ENABLE_RECONNECT_ALERT
    
    //Checking whether in login screen or other screen
    if (self.isLoggedIn && !self.isOtherLoggedIn)
    {
        if (self.isReconnectActivityAlertActive)
        {
            [self reconnectToSocket];
        }
        else
        {
            [self showErrorAlert];
            [self reconnectToSocket];
        }
    }
    else
    {
        self.isOtherLoggedIn =FALSE;
    }
    
    [[NSNotificationCenter defaultCenter] postNotificationName:ON_SOCKET_ERROR object:Nil];
    
#endif
    self.sentTime = NULL;//Previous time should be assigned NULL
    self.currentTime = NULL;
    [self.heartBeat invalidate];
    self.heartBeat = nil;
}

- (void)destroySocketOnError
{
    [[TAJClientServerCommunicator sharedClientServerCommunicator] destroySocket];
    [self reconnectToSocket];
    self.sentTime = NULL;//Previous time should be assigned NULL
    self.currentTime = NULL;
    [self.heartBeat invalidate];
    self.heartBeat = nil;
    
}

#pragma mark handle error alert

- (void)showErrorAlert
{
#if ENABLE_RECONNECT_ALERT
    [[NSNotificationCenter defaultCenter] postNotificationName:PLAYER_DISCONNECTED object:Nil];
    NSLog(@"DISCONNECTED");
    self.activityAlertForReconnecting = [[TAJUtilities sharedUtilities] createProgressInstance];
    
    //If I am back alert has to be shown
    if (self.isFirstGameStarted || self.isSecondGameStarted)
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_ALERT object:nil];
        [self.activityAlertForReconnecting showMessage:OOPS_WARNING_TEXT_1];
    }
    else //If I am back alert not to be shown
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_ALERT object:nil];
        [self.activityAlertForReconnecting showMessage:OOPS_WARNING_TEXT_2];
    }
    
    //    [self.activityAlertForReconnecting show];
    
    self.isReconnectActivityAlertActive = TRUE;
#endif
}

- (void)reconnectToSocket
{
    
#if ENABLE_RECONNECT_ALERT
    [self onReconnect];
#endif
}

- (void)onConnected
{
#if ENABLE_RECONNECT_ALERT
    self.isListTableRequested = NO;
    //If I am back alert has to be shown
    if (self.isFirstGameStarted || self.isSecondGameStarted)
    {
        
    }
    else //If I am back alert not to be shown
    {
        if (self.isReconnectActivityAlertActive)
        {
            [self performSelector:@selector(showIamBackDialogRemovingReconnectingAlert) withObject:nil afterDelay:1.5 ];
        }
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:ON_SOCKET_CONNECTION object:Nil];
    self.connectedTime = 0;
    if(self.connectedTimer)
    {
        [self.connectedTimer invalidate];
        self.connectedTimer = nil;
    }
    self.connectedTimer = [NSTimer scheduledTimerWithTimeInterval:1.0 target:self selector:@selector(updateConnectionTimer) userInfo:nil repeats:YES];
#endif
}

- (void)updateConnectionTimer
{
#if ENABLE_TIMER_DBUGGING
    DLog(@"update timer for connectionTimer");
#endif
    
    self.connectedTime++;
}

- (void)stopHeartBeat
{
    [self.heartBeat invalidate];
    self.heartBeat = nil;
}

- (void)logout
{
    //by rateesh
    APP_DELEGATE.gameTableIndex = 1;
    //end
    
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kLogout UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
    [self destroyLobbyUserAndDisconnectSocket];
    
    //Here it is moved to login screen
    self.isLoggedIn = FALSE;
}

- (void)requestAPI:(const char *)api
{
   // NSLog(@"API : %s",api);
    
    NSString *input = [[NSString alloc] initWithUTF8String:api];
    if ([input rangeOfString:@"HEART_BEAT"].location == NSNotFound) {
         DLog(@"send data %s", api);
        //NSLog(@"string does not contain bla");
    } else {
       // NSLog(@"string contains bla!");
    }
    //DLog(@"send data %s", api);
    int count = [[TAJUtilities sharedUtilities] stringLength:api];
    NSData *data = [NSData dataWithBytes:api length:count];
    [self sendDataToServer:data];
}

- (void)createLobby:(NSDictionary*)dictionary
{
    if (!self.lobby)
    {
        TAJLobby *lobby = [[TAJLobby alloc] init];
        [lobby assignTheTableDetails:dictionary];
        [lobby startPoller];
        self.lobby = lobby;
    }
}

- (void)destroyLobby
{
    self.lobby.tableDetails = Nil;
    [self.lobby stopPoller];
    self.lobby = Nil;
}

- (void)destroyUserModel
{
    self.usermodel = Nil;
}

//Author: RK
- (void)destroyLobbyUserAndDisconnectSocket
{
    [self destroyUserModel];
    [self destroyLobby];
    [[TAJClientServerCommunicator sharedClientServerCommunicator] closeConnection];
}

#pragma mark Joining tables
- (void)joinFirstTable
{
    if (self.lobby.firstTable)
    {
        self.lobby.runningTableType = eOpenTableFirst;
    }
}

- (void)joinSecondTable
{
    if (self.lobby.secondTable)
    {
        self.lobby.runningTableType = eOpenTableSecond;
    }
}

#pragma mark Table Joining methods

- (void)cardDiscardWithFace:(NSString*)face withUserName:(NSString*)userName withSuit:(NSString*)suit tableID:(NSString*)tableID userID:(NSString*)userID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kCardDiscard UTF8String], [face UTF8String], [userName UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [suit UTF8String], [tableID UTF8String], [userID UTF8String]);
    [self requestAPI:api];
}

- (void)cardAutoDiscardWithFace:(NSString*)face withUserName:(NSString*)userName withSuit:(NSString*)suit tableID:(NSString*)tableID userID:(NSString*)userID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kCardAutoDiscard UTF8String], [face UTF8String], [userName UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [suit UTF8String], [tableID UTF8String], [userID UTF8String]);
    [self requestAPI:api];
}
- (void)cardPickWithFace:(NSString*)face withStack:(NSString*)stack withSuit:(NSString*)suit tableID:(NSString*)tableID userID:(NSString*)userID nickName:(NSString*)nickName
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kCardPick UTF8String], [nickName UTF8String], [face UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [stack UTF8String], [suit UTF8String], [tableID UTF8String], [userID UTF8String]);
    [self requestAPI:api];
}

- (void)cardsMeldWithTableID:(NSString*)tableID cards:(NSString*)cards suit:(NSString*)suit face:(NSString*)face withMsgUUid:(NSString *)msgUuid
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kCardsMeld UTF8String], [suit UTF8String], [face UTF8String], [tableID UTF8String], [msgUuid UTF8String], [cards UTF8String]);
    [self requestAPI:api];
}

- (void)cardsMeldForOtherPlayerWithTableID:(NSString*)tableID cards:(NSString*)cards withMsgUUid:(NSString *)msgUuid
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kCardsMeldForOtherPlayer UTF8String], [tableID UTF8String], [msgUuid UTF8String], [cards UTF8String]);
    [self requestAPI:api];
}

- (void)smartCorrectionWithTableID:(NSString*)tableID nickName:(NSString*)nickname userID:(NSString*)userID msgUUID:(NSString*)msgUUID agree:(NSString*)agree gameId:(NSString*)gameId
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kSmartCorrection UTF8String], [nickname UTF8String],[userID UTF8String],[agree UTF8String], [tableID UTF8String],[gameId UTF8String],[msgUUID UTF8String]);
    [self requestAPI:api];
}

- (void)cardsCheckMeldWithTableID:(NSString*)tableID cards:(NSString*)cards suit:(NSString*)suit face:(NSString*)face withMsgUUid:(NSString *)msgUuid
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kCardsCheckMeld UTF8String], [suit UTF8String], [face UTF8String], [tableID UTF8String], [msgUuid UTF8String], [cards UTF8String]);
    [self requestAPI:api];
}

- (void)showEventWithSuit:(NSString*)suit face:(NSString*)face tableID:(NSString*)tableID userID:(NSString*)userID nickName:(NSString*)nickname
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kShow UTF8String], [suit UTF8String], [face UTF8String], [tableID UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [userID UTF8String], [nickname UTF8String]);
    [self requestAPI:api];
}

- (void)turnExtraTimeEventWithNickName:(NSString*)nickname tableID:(NSString*)tableID userID:(NSString*)userID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTurnExtraTime UTF8String],  [tableID UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [userID UTF8String],[nickname UTF8String]);
    [self requestAPI:api];
}

- (void)playerDropEventWithNickName:(NSString*)nickname tableID:(NSString*)tableID userID:(NSString*)userID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kDropEvent UTF8String],  [nickname UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [tableID UTF8String],[userID UTF8String]);
    [self requestAPI:api];
}

- (void)chatMessageWithUserID:(NSString*)userID tableID:(NSString*)tableID nickName:(NSString*)nickname text:(NSString*)text
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kChatMessage UTF8String],  [userID UTF8String], [tableID UTF8String], [nickname UTF8String],[text UTF8String]);
    [self requestAPI:api];
}

- (void)searchJoinTableWithUserID:(NSString*)userID nickName:(NSString*)nickname bet:(NSString*)bet maxplayers:(NSString*)maxplayers tableType:(NSString*)tableType tableCost:(NSString*)tableCost tableID:(NSString*)tableID conversion:(NSString*)conversion streamName:(NSString*)streamName streamID:(NSString*)streamID gameSettingsID:(NSString*)gameSettingsID msgUUID:(NSString*)msgUUID
{
    
    APP_DELEGATE.gameTableIndex = 2;
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kSearchJoinTable UTF8String], [msgUUID UTF8String], [userID UTF8String], [nickname UTF8String], [bet UTF8String], [maxplayers UTF8String], [tableType UTF8String], [tableCost UTF8String], [tableID UTF8String], [conversion UTF8String], [streamName UTF8String], [streamID UTF8String], [gameSettingsID UTF8String]);
    [self requestAPI:api];
   
}

- (void)splitRequestWithTableID:(NSString*)tableID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kSplitRequest UTF8String], [tableID UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)addToFavouriteWithTableID:(NSString*)tableID nickName:(NSString*)nickname userID:(NSString*)userID tableCost:(NSString *)tableCost
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kAddToFavourite UTF8String],[tableID UTF8String],[userID UTF8String],[nickname UTF8String],[tableCost UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    NSLog(@"ADD FAV ENGINE CALL : %s",api);
    [self requestAPI:api];
}

- (void)removeFromFavouriteWithTableID:(NSString*)tableID nickName:(NSString*)nickname userID:(NSString*)userID tableCost:(NSString *)tableCost
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kRemoveFromFavourite UTF8String],[tableID UTF8String],[userID UTF8String],[nickname UTF8String],[tableCost UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    NSLog(@"REMOVE FAV ENGINE CALL : %s",api);
    [self requestAPI:api];
}

- (void)splitAcceptReplyWithMsgUUID:(NSString*)msgUUID userID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kSplitAcceptReply UTF8String], [tableID UTF8String], [userID UTF8String], [nickname UTF8String], [msgUUID UTF8String]);
    [self requestAPI:api];
}

- (void)splitRejectReplyWithMsgUUID:(NSString*)msgUUID userID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kSplitRejectReply UTF8String], [tableID UTF8String], [userID UTF8String], [nickname UTF8String], [msgUUID UTF8String]);
    [self requestAPI:api];
}

- (void)rejoinReplyYesWithUserID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID msgUUID:(NSString*)msgUUID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kRejoinReplyYes UTF8String], [msgUUID UTF8String], [userID UTF8String], [nickname UTF8String], [tableID UTF8String]);
    [self requestAPI:api];
}

- (void)rejoinReplyNoWithUserID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID msgUUID:(NSString*)msgUUID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kRejoinReplyNo UTF8String], [msgUUID UTF8String], [userID UTF8String], [nickname UTF8String], [tableID UTF8String]);
    [self requestAPI:api];
}

- (void)getTableExtraForTableID:(NSString*)tableID
{
    [self performSelector:@selector(getAutoPlayCountWithTableID:) withObject:tableID afterDelay:3.0f];
    [self performSelector:@selector(getTableExtraWithDelay:) withObject:tableID afterDelay:3.0f];
}

- (void)getTableExtraWithDelay:(NSString *)tableID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTableExtra UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [tableID UTF8String]);
    [self requestAPI:api];
    
}

- (void)iAmBackWithUserID:(NSString*)userID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kIAmBack UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [userID UTF8String]);
    [self requestAPI:api];
}

- (void)rebuyinWithTableId:(NSString*) tableID userID:(NSString*)userId rebuyinAmt:(NSString*) rebuyinAmount
{
    int updatedVal=[self.buyInAmount intValue]+[rebuyinAmount intValue];
    self.buyInAmount=[NSString stringWithFormat:@"%d",updatedVal];
    
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kRebuyin UTF8String], [tableID UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [userId UTF8String], [rebuyinAmount UTF8String]);
    [self requestAPI:api];
}

- (void)tourneyRebuyinWithTourneyId:(NSString*)tourneyID level:(NSString*)level rebuyinAmt:(NSString*) rebuyinAmount
{
//    int updatedVal=[self.buyInAmount intValue]+[rebuyinAmount intValue];
//    self.buyInAmount=[NSString stringWithFormat:@"%d",updatedVal];
    
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTourneyRebuyin UTF8String], [rebuyinAmount UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [level UTF8String], [tourneyID UTF8String]);
    [self requestAPI:api];
}

- (void)reportABugForTableID:(NSString*)tableID gameId:(NSString*)gameID bugExaplanation:(NSString*)explanation gameType:(NSString*)gameType bugType:(NSString*)bugType
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kReportABug UTF8String], [tableID UTF8String], [gameID UTF8String], [explanation UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [gameType UTF8String], [bugType UTF8String]);
    [self requestAPI:api];
}

- (void)standUpWithTableID:(NSString*)tableID tableType:(NSString*)tableType tableCost:(NSString*)tableCost buyinAmount:(NSString*)buyinAmount seat:(NSString*) seat
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kStandUpAPI UTF8String], [tableID UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String], [tableType UTF8String], [tableCost UTF8String], [buyinAmount UTF8String], [seat UTF8String]);
    [self requestAPI:api];
}

- (void)getPlayerCount
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kPlayerCount UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)getTablesCount
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTableLength UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)getAutoPlayCountWithTableID:(NSString *)tableID
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kAutoPlayCount UTF8String], [[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[tableID UTF8String]);
    [self requestAPI:api];
//    DLog(@"autoplay count requested %s",api);
    
}

- (void)requestListTable
{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kListTable UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);//[msg_uuid UTF8String]);
    [self requestAPI:api];
    self.isListTableRequested = YES;
}

- (void)requestTournamentListTable {
    
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTournamentListTable UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);//[msg_uuid UTF8String]);
    [self requestAPI:api];//[msg_uuid UTF8String]
}

- (void)updateFunChips {
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kFunChipsReload UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);//
    [self requestAPI:api];
    NSLog(@"REQUEST FOR UPDATE FUN CHIPS : %s",api);
}

- (void)requestTournamentDetails:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTournamentDetails UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestGetPrizeList:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetPrizeList UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestGetLevelTimer:(NSString *)tournamentID msgUUID:(NSString*)msgUUID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetLevelTimer UTF8String],[tournamentID UTF8String],[msgUUID UTF8String]);
    [self requestAPI:api];
}

- (void)requestGetTournamentTables:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTournamentTables UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestRegisterTournament:(NSString *)tournamentID level:(NSString *)level amt:(NSString *)amt{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTournamentRegister UTF8String],[tournamentID UTF8String],[level UTF8String],[amt UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestDeregisterTournament:(NSString *)tournamentID level:(NSString *)level{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kTournamentDeregister UTF8String],[tournamentID UTF8String],[level UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestGetTournamentWaitList:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTournamentWaitList UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}
- (void)requestGetLeaderBoard:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTournamentLeaderBoard UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}
- (void)requestGetRegisteredPlayer:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTournamentRegisteredPlayer UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestGetPrizeDistribution:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetPrizeDistribution UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestLeaveTourney:(NSString *)tournamentID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kLeaveTournament UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String]);
    [self requestAPI:api];
}

- (void)requestGetTopPlayers:(NSString *)tournamentID level:(NSString*)levelID{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kLevelTopPlayer UTF8String],[tournamentID UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[levelID UTF8String]);
    [self requestAPI:api];
}

- (void)requestJoinTableReply:(NSString *)uuid{
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kRequestJoinTableReply UTF8String],[uuid UTF8String]);
    [self requestAPI:api];
}

- (void)processJoinTableReply:(NSDictionary*)dictionary
{
    NSString *tableID = NULL;
    tableID = [dictionary objectForKey:TAJ_TABLE_ID];
    
    if (self.joinAsView)
    {
        if (self.lobby.isFirstTableCreated)
        {
            if (NSOrderedSame == [self.lobby.firstTable.tableIdentifier compare:tableID])
            {
                self.lobby.firstTable.gameTableViewDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
            }
        }
        
        if (self.lobby.isSecondTableCreated)
        {
            if (NSOrderedSame == [self.lobby.secondTable.tableIdentifier compare:tableID])
            {
                self.lobby.secondTable.gameTableViewDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
            }
        }
        [[NSNotificationCenter defaultCenter] postNotificationName:MOVETOGAMESCREEN object:nil];
    }
    else
    {
        //APP_DELEGATE.gameTableIndex = 2;
        if (NSOrderedSame == [self.lobby.firstTable.tableIdentifier compare:tableID])
        {
            if (self.lobby.firstTable.searchJoinTableMode)
            {
                self.lobby.firstTable.searchJoinTableMode = FALSE;
                [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_JOIN_TABLE_PLAY_START_NOTIFICATION object:dictionary];
            }
            self.lobby.firstTable.gameTablePlayDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        }
        
        if (NSOrderedSame == [self.lobby.secondTable.tableIdentifier compare:tableID])
        {
            if (self.lobby.secondTable.searchJoinTableMode)
            {
                self.lobby.secondTable.searchJoinTableMode = FALSE;
                [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_JOIN_TABLE_PLAY_START_NOTIFICATION object:dictionary];
            }
            self.lobby.secondTable.gameTablePlayDetails = [NSMutableDictionary dictionaryWithDictionary:dictionary];
        }
    }
}

- (void)processGetTableDetails:(NSDictionary*)dictionary
{
    int count = 0;
    int userId = [[[NSUserDefaults standardUserDefaults] objectForKey:USERID] intValue];
    //NSLog(@"CHECK INFO %@",dictionary);
    
    NSDictionary * playersDict = [dictionary objectForKey:@"table_details"][@"players"];
    int maxPlayers = [[dictionary objectForKey:@"table_details"][@"TAJ_maxplayer"] intValue];
    
    if ([playersDict count] < 0 || ( playersDict == NULL ) || (playersDict == (NSDictionary*) [NSNull null]))
    {
        count = 1;
    }
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSDictionary class]]) {
        int tajUserId = [[playersDict objectForKey:@"player"][@"TAJ_user_id"] intValue];
        if (userId == tajUserId) {
            count = 1;
        }
        else {
            count = 2;
        }
    }
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSArray class]]){
        NSArray * players = [playersDict objectForKey:@"player"];
        int foundUserId = NO;
        
        for (int i = 0; i < players.count; i++)
        {
            int tajUserId = [[players valueForKey:@"TAJ_user_id"][i] intValue] ;
            NSLog(@"userId :%d",userId);
            NSLog(@"loginUserId :%d",tajUserId);
            //NSLog(@"kTaj_scoresheet_userid :%@",userId);
            if (userId == tajUserId) {
                foundUserId = YES;
            }
            
        }
        
        if (foundUserId) {
            count = (int)[players count];
        }
        else {
        count = (int)[players count] + 1;
        }
    }
    
   // NSLog(@"CHECK players : %@",playersDict);
    //    NSArray * player = [[dictionary objectForKey:@"table_details"][@"players"]];
    
    NSDictionary * playersCountDict = @{
                                        @"table_id" : [dictionary objectForKey:@"TAJ_table_id"],
                                        @"maxplayer" : [NSNumber numberWithInt:maxPlayers],
                                        @"count" : [NSNumber numberWithInt:count]
                                        };
    //NSLog(@"playersCountDict : %@",playersCountDict);
    
    
    char api[API_CHARACTERS_LIMIT];
    if (self.joinTableAfterGettingDetails)
    {
        if (self.joinAsView)
        {
            NSMutableArray *arrayOfTables = Nil;
            arrayOfTables = [[self.lobby tableDetails] valueForKeyPath:TABLE_GAMELIST_DATA];
            BOOL isFirstFound = NO;
            BOOL isSecondFound = NO;
            
            for (NSDictionary* tempDict in arrayOfTables)
            {
//                DLog(@"%@ %@ %@",[tempDict valueForKey:TAJ_TABLE_ID],self.lobby.firstTable.tableIdentifier,self.lobby.firstTable.tableIdentifier);
                if( [[tempDict valueForKey:TAJ_TABLE_ID] isEqualToString:self.lobby.firstTable.tableIdentifier])
                {
                    isFirstFound=YES;
                }
                
                if( [[tempDict valueForKey:TAJ_TABLE_ID] isEqualToString:self.lobby.secondTable.tableIdentifier])
                {
                    isSecondFound=YES;
                }
                
            }
            
            if ([self.lobby.firstTable.tableIdentifier isEqualToString:self.lobby.secondTable.tableIdentifier] )
            {
                [self.lobby destroySecondTable];
            }
            
            if (!self.lobby.isFirstTableCreated)
            {
                
                [self.lobby createFirstTable:dictionary];
//                DLog(@"first table created joinasview");
                self.lobby.runningTableType = eOpenTableFirst;
                sprintf(api, [kJoinTableAsView UTF8String],[[dictionary valueForKeyPath:TAJ_TABLE_ID] UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_TYPE_KEY] UTF8String], [[dictionary valueForKeyPath:TAJ_JOIN_TABLE_COST_KEY] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_MIN_BUYIN_KEY] UTF8String]);
                
            }
            else if (!self.lobby.isSecondTableCreated)
            {
                DLog(@"second table created joinasview");
                
                [self.lobby createSecondTable:dictionary];
                self.lobby.runningTableType = eOpenTableSecond;
                sprintf(api, [kJoinTableAsView UTF8String],[[dictionary valueForKeyPath:TAJ_TABLE_ID] UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_TYPE_KEY] UTF8String], [[dictionary valueForKeyPath:TAJ_JOIN_TABLE_COST_KEY] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_MIN_BUYIN_KEY] UTF8String]);
                
            }
            else
            {
                DLog(@"Both Table filled");
            }
        }
        else
        {
            NSString *tableID = NULL;
            tableID = [dictionary objectForKey:@"TAJ_table_id"];
            int seatNumber = 0;
           // DLog(@"process get table %@",dictionary);
            if (self.lobby.firstTable && (NSOrderedSame == [self.lobby.firstTable.tableIdentifier compare:tableID]))
            {
                //DLog(@"process get table %@",dictionary);
                seatNumber = self.lobby.firstTable.mSeatNumber;
                [self.lobby updateFirstTableGameTableDetailsWithSearchJointableDictionary:dictionary];
            }
            
            if (self.lobby.secondTable && (NSOrderedSame == [self.lobby.secondTable.tableIdentifier compare:tableID]))
            {
               // DLog(@"process get table %@",dictionary);
                
                seatNumber = self.lobby.secondTable.mSeatNumber;
                [self.lobby updateSecondTableGameTableDetailsWithSearchJointableDictionary:dictionary];
            }
            
            if (self.joinTableTypeJokerNoJoker)
            {
                //check for middle join
                if (self.isMiddleJoin)
                {
                    sprintf(api, [kJoinTableAsMiddle UTF8String],[[dictionary valueForKeyPath:TAJ_TABLE_ID] UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_TYPE_KEY] UTF8String], [[dictionary valueForKeyPath:TAJ_JOIN_TABLE_COST_KEY] UTF8String],[self.buyInAmount UTF8String], seatNumber);
                }
                else
                {
                    sprintf(api, [kJoinTableAsPlay UTF8String],[[dictionary valueForKeyPath:@"TAJ_table_id"] UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_TYPE_KEY] UTF8String], [[dictionary valueForKeyPath:TAJ_JOIN_TABLE_COST_KEY] UTF8String],[self.buyInAmount UTF8String], seatNumber);
                }
            }
            else
            {
                sprintf(api, [kJoinTableAsPlay UTF8String],[[dictionary valueForKeyPath:TAJ_TABLE_ID] UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_TYPE_KEY] UTF8String], [[dictionary valueForKeyPath:TAJ_JOIN_TABLE_COST_KEY] UTF8String],[[dictionary valueForKeyPath:TAJ_JOIN_TABLE_MIN_BUYIN_KEY] UTF8String], seatNumber);
            }
        }
        if(!self.isTournament){
             [self requestAPI:api];
        }else{
            [self processJoinTableReply:dictionary];
        }
       
        
    }
    else
    {
        self.joinTableAfterGettingDetails = TRUE;
        NSString *tableID = NULL;
        tableID = [dictionary objectForKey:TAJ_TABLE_ID];
        [[NSNotificationCenter defaultCenter] postNotificationName:SEARCH_JOIN_GET_TABLE_DETAILS object:dictionary];
    }
    
}

- (void)processTournamentListTable:(NSDictionary*)dictionary {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TOURNAMENT_LIST_TABLE_UPDATE object:dictionary];
    
}

- (void)processFunChipsData:(NSDictionary*)dictionary {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:FUNCHIPS_RELOAD object:dictionary];
    
}

- (void)processTournamentDetails:(NSDictionary*)dictionary {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TOURNAMENT_DETAIL_UPDATE object:dictionary];
}

- (void)processTournamentPrizeList:(NSDictionary*)dictionary {
    
    [[NSNotificationCenter defaultCenter] postNotificationName:TOURNAMENT_PRIZE_LIST object:dictionary];
}

- (void)processTournament:(NSDictionary*)dictionary notification:(NSString*)notificationString{
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationString object:dictionary];
}

- (void)processTournamentRebuyinEvent:(NSDictionary*)dictionary notification:(NSString*)notificationString{
    [[NSNotificationCenter defaultCenter] postNotificationName:notificationString object:dictionary];
}


- (void)processListTable:(NSDictionary*)dictionary
{
    if (_isReconnectRequest)
    {
        [self.activityIndicator removeFromSuperview];
        _isReconnectRequest = FALSE;
        //Handle the list_table updation in lobby and every where
        [self.lobby assignTheTableDetails:dictionary];
        
    }
    else
    {
        
#if GAME_ENGINE_FORCE_QUIT
        
        if (!self.lobby)
        {
            
            [self createLobby:dictionary];
            //To know whether is login screen logging in or in other screen
            self.isLoggedIn = TRUE;
            
        }
        else
        {
            // creating a the lobby if application is force quit, server has sending the player already in play / view
            [self.lobby assignTheTableDetails:dictionary];
            
            //To know whether is login screen logging in or in other screen
            self.isLoggedIn = TRUE;
        }
        
#else
        
        if (!self.lobby)
        {
            
            [self createLobby:dictionary];
            
            //RK Dismiss the logging in activity
            [[TAJUtilities sharedUtilities] dismissLoginActivity];
            
            //To know whether is login screen logging in or in other screen
            self.isLoggedIn = TRUE;
            
            [[NSNotificationCenter defaultCenter] postNotificationName:MOVETOHOMEPAGE object:Nil];
        }
#endif
        
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:LIST_TABLE_UPDATE_AFTER_RECONNECT object:Nil];
    
}

- (void)processAuthenticationMessage:(NSDictionary*)dictionary
{
    NSString *msg_uuid = Nil, *tableIDs = Nil;
    msg_uuid = [dictionary valueForKeyPath:TAJ_MSG_UUID];
    [_usermodel setData:dictionary];
    //After authentication we should call schedule heart beat
    [[TAJGameEngine sharedGameEngine] scheduleHeartBeat];
    //logged in
    self.isLoggedIn = TRUE;
    //Handling of the quit table for many table ids
    tableIDs = [dictionary objectForKey:TAJ_TABLE_ID];
    [self.usermodel setTableIDs:dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:MOVETOLOBBY object:Nil];
    
    if (tableIDs)
    {
        NSArray *array = Nil;
        array = [tableIDs componentsSeparatedByString:@","];
#if GAME_ENGINE_FORCE_QUIT
        if (array && array.count > 0)
        {
            // creating a the lobby if application is force quit, server has sending the player already in play / view
            [self createLobby:nil];
            for (int index=0; index < array.count; index++)
            {
                [self getAutoPlayCountWithTableID:array[index]];
            }
        }
        
#endif
        if (array && ([array count] > 2))
        {
            unsigned int count = [array count];
            unsigned int index = 0;
            for (index = 2; index < count; index++)
            {
                NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:[array objectAtIndex:index], GAME_TABLE_ID_VALUE ,nil];
                [self quitTable:dic];
            }
        }
    }
    else
    {
        [self requestListTable];
    }
    [[NSNotificationCenter defaultCenter] postNotificationName:MOVETOHOMEPAGE object:Nil];
    [self getPlayerCount];
    [self getTablesCount];
    
}

- (void)processQuitTableReply:(NSDictionary*)dictionary
{
}

- (void)getTableDetailsAndJoinTableAsView:(NSDictionary*)dictionary
{
    BOOL processTableJoinAsView = false;
    self.joinTableAfterGettingDetails = TRUE;
    self.joinAsView = true;
    self.isTournament = false;
    NSString *tableId = [dictionary valueForKey:TAJ_TABLE_ID];
    
    if (!processTableJoinAsView)
    {
        if (self.lobby.firstTable)
        {
            if (NSOrderedSame == [self.lobby.firstTable.tableIdentifier compare:tableId])
            {
                processTableJoinAsView = TRUE;
            }
        }
    }
    
    if (!processTableJoinAsView)
    {
        if (self.lobby.secondTable)
        {
            if (NSOrderedSame == [self.lobby.secondTable.tableIdentifier compare:tableId])
            {
                processTableJoinAsView = TRUE;
            }
        }
    }
        
    if (!processTableJoinAsView)
    {
        processTableJoinAsView = TRUE;
        char api[API_CHARACTERS_LIMIT];
        sprintf(api, [kGetTableDetails UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[tableId UTF8String]);
        [self requestAPI:api];
    }
}

- (void)getTableDetailsAndJoinTableAsPlay:(NSDictionary*)dictionary
{
    self.joinTableAfterGettingDetails = TRUE;
    self.joinAsView = FALSE;
    self.isTournament = false;
    NSString *tableId = [dictionary valueForKey:TAJ_TABLE_ID];
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTableDetails UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[tableId UTF8String]);
    [self requestAPI:api];
}

- (void)getTableDetailsAndJoinTableAsPlayTournament:(NSDictionary*)dictionary
{
    self.joinTableAfterGettingDetails = true;
    self.joinAsView = true;
    self.isTournament = true;
    NSString *tableId = [dictionary valueForKey:TAJ_TABLE_ID];
    NSString *uuid = [dictionary valueForKey:TAJ_MSG_UUID];
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTableDetails UTF8String],[uuid UTF8String],[tableId UTF8String]);
    [self requestAPI:api];
}

- (void)getTableDetailsAndDontJoinTable:(NSDictionary*)dictionary
{
    self.joinTableAfterGettingDetails = FALSE;
    NSString *tableId = [dictionary valueForKey:TAJ_TABLE_ID];
    char api[API_CHARACTERS_LIMIT];
    sprintf(api, [kGetTableDetails UTF8String],[[[TAJUtilities sharedUtilities] getTheUUID] UTF8String],[tableId UTF8String]);
    [self requestAPI:api];
}

- (void)quitTable:(NSDictionary*)dictionary
{
    NSString *tableID = NULL;
    tableID = [dictionary valueForKeyPath:GAME_TABLE_ID_VALUE];
    if (tableID)
    {
        char api[API_CHARACTERS_LIMIT];
        [self.lobby destroyTable:tableID];
        
        NSString* uuidString=[NSString stringWithFormat:@"%@",[[TAJUtilities sharedUtilities] getTheUUID] ];
        TAJGameEngine *engine =[TAJGameEngine sharedGameEngine];
        [engine.quitTableValues setObject:tableID  forKey:uuidString];
        
        sprintf(api, [kQuitTable UTF8String],[tableID UTF8String],[uuidString UTF8String]);
        [self requestAPI:api];
    }
}

- (void)joinSeat:(int)seatNumber tableID:(NSString*)tableID buyInAmount:(NSString*)buyInAmount middleJoin:(BOOL)value
{
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    self.isMiddleJoin = value;
    if (buyInAmount)
    {
        self.buyInAmount = buyInAmount;
        _joinTableTypeJokerNoJoker = TRUE;
    }
    else
    {
        _joinTableTypeJokerNoJoker = FALSE;
    }
    
    if (self.lobby.firstTable)
    {
        if (NSOrderedSame == [tableID compare:self.lobby.firstTable.tableIdentifier])
        {
            self.lobby.firstTable.mSeatNumber = seatNumber;
            [engine getTableDetailsAndJoinTableAsPlay:engine.lobby.firstTable.gameTableDetails];
        }
    }
    
    if (self.lobby.secondTable)
    {
        if (NSOrderedSame == [tableID compare:self.lobby.secondTable.tableIdentifier])
        {
            self.lobby.secondTable.mSeatNumber = seatNumber;
            [engine getTableDetailsAndJoinTableAsPlay:engine.lobby.secondTable.gameTableDetails];
            
        }
    }
}

- (void)quitCurrentRunningTable
{
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    
    switch (engine.lobby.runningTableType)
    {
        case eOpenTableNone:
            
            break;
            
        case eOpenTableFirst:
            [engine quitTable:engine.lobby.firstTable.gameTableDetails];
            break;
            
        case eOpenTableSecond:
            [engine quitTable:engine.lobby.secondTable.gameTableDetails];
            break;
            
        default:
            break;
    }
}

- (void)quitTableWithTableID:(NSString *)tableID
{
    TAJGameEngine *engine = [TAJGameEngine sharedGameEngine];
    NSMutableDictionary *dic = [NSMutableDictionary dictionaryWithObjectsAndKeys:tableID, GAME_TABLE_ID_VALUE ,nil];
    [engine quitTable:dic];
}

- (void)addSlotsInfoForHeartBeat:(NSDictionary*)dictionary
{
    [self.arrayForSlotsInfo addObject:dictionary];
    [self heartBeatMessage];
}

#pragma mark Events Processing Methods

- (void)processStartGameEvent:(NSDictionary*)dictionary
{
    [self.lobby processStartGameEventUpdate:dictionary];
}

- (void)processDeviceChangeEvent:(NSDictionary*)dictionary
{
    DLog(@"******___processDeviceChangeEvent______******");
    [[NSNotificationCenter defaultCenter] postNotificationName:DEVICE_CHANGE object:dictionary];
}

- (void)processEngineStatusEvent:(NSDictionary*)dictionary
{
    DLog(@"******___processsenginestatusevent______******");
    NSString *status = dictionary[@"status"];
    if (NSOrderedSame == [status compare:@"True"])
    {
        //engine is under maintenance
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_ENGINE_STATUS_TRUE_ALERT object:dictionary];
        
    }
    else
    {
        [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_ENGINE_STATUS_FALSE_ALERT object:dictionary];
    }
    
}
- (void)processGameScheduleEvent:(NSDictionary*)dictionary
{
    [self.lobby processGameScheduleEventUpdate:dictionary];
}

- (void)processGameLevelUpdateEvent:(NSDictionary*)dictionary
{
    [self.lobby processLevelUpdateEventUpdate:dictionary];
}

- (void)processBalanceUpdateEvent:(NSDictionary*)dictionary
{
    //RK : Handling BALANCE UPDATE event
    [_usermodel setDataForEventUpdate:dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:BALANCE_UPDATE object:dictionary];
}

- (void)processShowEvent:(NSDictionary*)dictionary
{
    [self.lobby processShowEventUpdate:dictionary];
}

- (void)processPlayerEliminateEvent:(NSDictionary*)dictionary
{
    [self.lobby processPlayerEliminateEventUpdate:dictionary];
}

- (void)processPlayerDropEvent:(NSDictionary*)dictionary
{
    [self.lobby processPlayerDropEventUpdate:dictionary];
}

- (void)processTurnUpdateEvent:(NSDictionary*)dictionary
{
    [self.lobby processTurnUpdateEventUpdate:dictionary];
}

- (void)processCardDiscardEvent:(NSDictionary*)dictionary
{
    [self.lobby processCardDiscardEventUpdate:dictionary];
}

- (void)processCardPickEvent:(NSDictionary*)dictionary
{
    [self.lobby processCardPickEventUpdate:dictionary];
}

- (void)processSendDealEvent:(NSDictionary*)dictionary
{
    [self.lobby processSendDealEventUpdate:dictionary];
}

- (void)processGameEndEvent:(NSDictionary*)dictionary
{
    [self.lobby processGameEndEventUpdate:dictionary];
}

- (void)processStackResuffleEvent:(NSDictionary*)dictionary
{
    [self.lobby processStackResuffleEventUpdate:dictionary];
}

- (void)processSendStackEvent:(NSDictionary*)dictionary
{
    [self.lobby processSendStackEventUpdate:dictionary];
}

- (void)processNewGameEvent:(NSDictionary*)dictionary
{
    [self.lobby processNewGameEventUpdate:dictionary];
}

- (void)processChatMSGEvent:(NSDictionary*)dictionary
{
    [self.lobby processChatMSGEventUpdate:dictionary];
}

- (void)processPlayerQuitEvent:(NSDictionary*)dictionary
{
    [self.lobby processPlayerQuitEventUpdate:dictionary];
}

- (void)processTurnTimeOutEvent:(NSDictionary*)dictionary
{
    [self.lobby processTurnTimeoutEventUpdate:dictionary];
}

- (void)processPlayerJoinEvent:(NSDictionary*)dictionary
{
    [self.lobby processPlayerJoinUpdateEventUpdate:dictionary];
}

- (void)processHeartBeatEvent:(NSDictionary*)dictionary
{
    
#if ENABLE_HEART_BEAT_IMPLEMENTATION_TYPE_TWO
    _timeOut = 0.0f;
#else
    self.isHeartBeatRecieved = TRUE;
#endif
}

- (void)processSittingSeqEvent:(NSDictionary*)dictionary
{
    [self.lobby processSittingSeqUpdateEventUpdate:dictionary];
}

- (void)processReportBugEvent:(NSDictionary*)dictionary
{
    [self.lobby processReportBugEventUpdate:dictionary];
}

- (void)processShowDealEvent:(NSDictionary*)dictionary
{
    [self.lobby processShowDealEventUpdate:dictionary];
}

- (void)processTableStartEvent:(NSDictionary*)dictionary
{
    [self.lobby processTableStartEventUpdate:dictionary];
}

- (void)processTableClosedEvent:(NSDictionary*)dictionary
{
    [self.lobby processTableClosedEvent:dictionary];
}

- (void)processBestOfWinnerEvent:(NSDictionary*)dictionary
{
    [self.lobby processBestOfWinnerEvent:dictionary];
}

- (void)processPoolWinnerEvent:(NSDictionary*)dictionary
{
    [self.lobby processPoolWinnerEvent:dictionary];
}

- (void)processTableTossEvent:(NSDictionary*)dictionary
{
    [self.lobby processTableTossEvent:dictionary];
}

- (void)processSplitStatusEvent:(NSDictionary*)dictionary
{
    [self.lobby processSplitStatusEvent:dictionary];
}

- (void)processSplitResultEvent:(NSDictionary*)dictionary
{
    [self.lobby processSplitResultEvent:dictionary];
}

- (void)processSplitReply:(NSDictionary*)dictionary
{
    [self.lobby processSplitReply:dictionary];
}

- (void)processRejoinEvent:(NSDictionary*)dictionary
{
    [self.lobby processRejoinEvent:dictionary];
}

- (void)processGetTableDetailsEvent:(NSDictionary*)dictionary
{
    //NSLog(@"GET INFO : %@",dictionary);
    
    //start by ratheesh
    int count = 0;
    
    NSDictionary * playersDict = [dictionary objectForKey:@"table_details"][@"players"];
    int maxPlayers = [[dictionary objectForKey:@"table_details"][@"TAJ_maxplayer"] intValue];
    
    if ([playersDict count] < 0 || ( playersDict == NULL ) || (playersDict == (NSDictionary*) [NSNull null]))
    {
        count = 1;
    }
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSDictionary class]]) {
        count = 2;
    }
    else if ([playersDict count] > 0 && [[playersDict objectForKey:@"player"] isKindOfClass:[NSArray class]]){
        NSArray * players = [playersDict objectForKey:@"player"];
        count = (int)[players count];
    }
    
   // NSLog(@"CHECK players : %@",playersDict);
    //    NSArray * player = [[dictionary objectForKey:@"table_details"][@"players"]];
    
    NSDictionary * playersCountDict = @{
                                        @"table_id" : [dictionary objectForKey:@"table_details"][@"TAJ_table_id"],
                                        @"maxplayer" : [NSNumber numberWithInt:maxPlayers],
                                        @"count" : [NSNumber numberWithInt:count]
                                        };
    //NSLog(@"playersCountDict : %@",playersCountDict);
    
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_PLAYERS object:playersCountDict];
    //end by ratheesh
    
    NSString *tableID = [dictionary valueForKeyPath:GAME_TABLE_ID_VALUE];
    NSMutableDictionary *tableDetailsDictionary = [dictionary valueForKeyPath:TAJ_TABLE_DETAILS];
    if (self.lobby)
    {
        if ([self isPlayerExists:dictionary])
        {
            
            if (self.lobby.firstTable && (NSOrderedSame == [self.lobby.firstTable.tableIdentifier compare:tableID]))
            {
                self.lobby.firstTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:tableDetailsDictionary];
                DLog(@"first table assigned %@", dictionary);
            }
            
            else if (self.lobby.secondTable && (NSOrderedSame == [self.lobby.secondTable.tableIdentifier compare:tableID]))
            {
                self.lobby.secondTable.gameTableDetails = [NSMutableDictionary dictionaryWithDictionary:tableDetailsDictionary];
                DLog(@"second table assigned");
            }
            
            else if (!self.lobby.firstTable )
            {
                //DLog(@"first table created %@" ,dictionary);
                [self.lobby createFirstTable:tableDetailsDictionary];
            }
            else if (!self.lobby.secondTable )
            {
                //DLog(@"second table created");
                
                [self.lobby createSecondTable:tableDetailsDictionary];
            }
        }
        
        [[NSNotificationCenter defaultCenter] postNotificationName:GET_TABLE_DETAILS_EVENT object:dictionary];
    }
    
}

- (BOOL)isPlayerExists:(NSDictionary *)dictionary
{
    BOOL isThisPlayerExist = NO;
    int loggedInUserID = [[[[TAJGameEngine sharedGameEngine] usermodel] userId] integerValue];
    
    if (dictionary &&
        [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSDictionary class]])
    {
        // Only 1 player available
        NSMutableDictionary* onePlayerDict = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        
        if (onePlayerDict[kTaj_userid_Key] &&
            ([onePlayerDict[kTaj_userid_Key] integerValue] == loggedInUserID))
        {
            isThisPlayerExist = YES;
        }
        
    }
    else if (dictionary &&
             [[dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS] isKindOfClass:[NSArray class]])
    {
        // Get the players array
        NSArray *playersArray = [dictionary valueForKeyPath:GAME_GET_EXISTING_PLAYERS];
        
        // More than 1 player data available
        for (int i = 0; i < playersArray.count; i++)
        {
            NSMutableDictionary *playerDictionary = [playersArray objectAtIndex:i];
            
            if (playerDictionary[kTaj_userid_Key] &&
                ([playerDictionary[kTaj_userid_Key] integerValue] == loggedInUserID))
            {
                isThisPlayerExist = YES;
                break;
            }
        }
    }
    return isThisPlayerExist;
    
}

- (void)processTurnExtraTime:(NSDictionary*)dictionary
{
    [self.lobby processTurnExtraTime:dictionary];
}

- (void)processGameDeschedule:(NSDictionary*)dictionary
{
    [self.lobby processGameDeschedule:dictionary];
}

- (void)processSplitFalseEvent:(NSDictionary*)dictionary
{
    [self.lobby processSplitFalseEvent:dictionary];
}

- (void)processMeldSuccess:(NSDictionary*)dictionary
{
    [self.lobby processMeldSuccess:dictionary];
}

- (void)processPreGameResult:(NSDictionary*)dictionary
{
    [self.lobby processPreGameResult:dictionary];
}

- (void)processGameResult:(NSDictionary*)dictionary
{
    [self.lobby processGameResultEventUpdate:dictionary];
}

- (void)processMeldFailEvent:(NSDictionary*)dictionary
{
    [self.lobby processMeldFailEvent:dictionary];
}

- (void)processTurnExtraTimeReconnectEvent:(NSDictionary*)dictionary
{
    [self.lobby processTurnExtraTimeReconnectEvent:dictionary];
}

- (void)processMeldExtraTimeReconnectEvent:(NSDictionary*)dictionary
{
    [self.lobby processMeldExtraTimeReconnectEvent:dictionary];
}

- (void)processShowExtraTimeReconnectEvent:(NSDictionary*)dictionary
{
    [self.lobby processShowExtraTimeReconnectEvent:dictionary];
}

- (void)processRebuyinEvent:(NSDictionary*)dictionary
{
    [self.lobby processRebuyinEvent:dictionary];
}

- (void)processOtherLoginEvent:(NSDictionary*)dictionary
{
    [self.lobby stopPoller];
    self.isOtherLoggedIn = TRUE;
    self.isLoggedIn = FALSE;
    self.isFirstGameStarted = NO;
    self.isSecondGameStarted = NO;
    [[NSNotificationCenter defaultCenter] postNotificationName:SHOW_OTHER_LOGIN_ALERT object:nil];
    [self performSelector:@selector(loadLoginScreenAfterOtherLogin) withObject:Nil afterDelay:2.0f];
}

- (void)processMeldReply:(NSDictionary*)dictionary
{
//    DLog(@"**************************handle meld event");
    [self.lobby processMeldReplyEvent:dictionary];
}

- (void)processCheckMeldReply:(NSDictionary*)dictionary
{
    //    DLog(@"**************************handle meld event");
    [self.lobby processCheckMeldReplyEvent:dictionary];
}


- (void)processMeldRequest:(NSDictionary*)dictionary
{
    [self.lobby processMeldRequestEvent:dictionary];
}

- (void)processAutoPlayStatusReply:(NSDictionary*)dictionary
{
    [self.lobby processAutoPlayStatusReply:dictionary];
}

- (void)processAutoPlayStatusEvent:(NSDictionary*)dictionary
{
    [self.lobby processAutoPlayStatusEvent:dictionary];
}

- (void)processGetTableExtraReply:(NSDictionary*)dictionary
{
    [self.lobby processGetTableExtraReply:dictionary];
}

- (void)processPlayerCount:(NSDictionary*)dictionary
{
    [self.lobby processPlayerCount:dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:PLAYER_COUNT object:dictionary];
}

- (void)processTableLength:(NSDictionary*)dictionary
{
    [self.lobby processTableLength:dictionary];
    [[NSNotificationCenter defaultCenter] postNotificationName:TABLE_LENGTH object:dictionary];
}

- (void)processStandUpReply:(NSDictionary*)dictionary
{
    [self.lobby processStandUpReply:dictionary];
}

- (void)processAutoPlayCountReply:(NSDictionary*)dictionary
{
    [[NSNotificationCenter defaultCenter] postNotificationName:IAM_BACK_VIEW_AUTOPLAY_COUNT object:dictionary];
}

- (void)showIamBackDialogRemovingReconnectingAlert
{
    if (self.isFirstGameStarted || self.isSecondGameStarted)
    {
        
        if (self.isReconnectActivityAlertActive)
        {
            [self.activityAlertForReconnecting hide];
            self.activityAlertForReconnecting = nil;
            self.isReconnectActivityAlertActive = FALSE;
        }
    }
    else
    {
        [self removeReconnectingAlert];
    }
}

- (void)removeReconnectingAlert
{
    if (self.isReconnectActivityAlertActive)
    {
        [self.activityAlertForReconnecting hide];
        self.activityAlertForReconnecting = nil;
        self.isReconnectActivityAlertActive = FALSE;
    }
}

- (void)processRejoinRequest:(NSDictionary*)dictionary
{
    [self.lobby processRejoinRequest:dictionary];
}

- (void)processSearchJoinTableReply:(NSDictionary*)dictionary
{
    [self.lobby processSearchJoinTableReply:dictionary];
}

- (void)processRebuyinReply:(NSDictionary*)dictionary
{
    [self.lobby processRebuyinReply:dictionary];
}

- (void)processSendSlotsEvent:(NSDictionary*)dictionary
{
    [self.lobby processSendSlotsEvent:dictionary];
}

- (void)processSmartCorrectionEvent:(NSDictionary*)dictionary
{
    [self.lobby processSmartCorrectionEvent:dictionary];
}

- (void)processSplitRequest:(NSDictionary*)dictionary
{
    [self.lobby processSplitRequest:dictionary];
}

- (void)loadLoginScreenAfterOtherLogin
{
    [self.alertForOtherLogin hide];
    TAJAppDelegate *delegate = [[UIApplication sharedApplication] delegate];
    [self stopHeartBeat];
    [self destroyLobbyUserAndDisconnectSocket];
    [self destroyUserNameAndPassword];
    [delegate loadLoginScreenWindow]; //load login  screen
}

- (void)destroyUserNameAndPassword
{
#if USERNAME_PASSWORD_STORE_CHECK
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:USENAME_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:PASSWORD_KEY];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:UNIQUE_SESSION_KEY];
    [[NSUserDefaults standardUserDefaults] synchronize];
#endif
}

#pragma heart beat helper methods
- (NSString*)getInfoStringForDictionary:(NSDictionary*)dictionary
{
    NSString *finalString = Nil;
    
    NSString *tableID = [dictionary objectForKey:TAJ_TABLE_ID];
    NSString *tableStartString = TABLE_START_STRING;
    finalString = [NSString stringWithFormat:tableStartString,tableID];
    
    NSArray *arrayOfInfo = [dictionary objectForKey:CARDS_KEY];
    
    for (NSDictionary *object in arrayOfInfo)
    {
        finalString = [finalString stringByAppendingString:CARD_START_STRING];
        
        NSString *faceValue = [object objectForKey:FACE_KEY];
        NSString *slotValue = [object objectForKey:SLOT_KEY];
        NSString *suitValue = [object objectForKey:SUIT_KEY];
        
        NSString *faceString = FACE_STRING;
        NSString *suitString = SUIT_STRING;
        NSString *slotString = SLOT_STRING;
        
        faceString = [NSString stringWithFormat:faceString,faceValue];
        suitString = [NSString stringWithFormat:suitString,suitValue];
        slotString = [NSString stringWithFormat:slotString,slotValue];
        
        finalString = [finalString stringByAppendingString:faceString];
        finalString = [finalString stringByAppendingString:suitString];
        finalString = [finalString stringByAppendingString:slotString];
        
        finalString = [finalString stringByAppendingString:CARD_END_STRING];
    }
    
    finalString = [finalString stringByAppendingString:TABLE_END_STRING];
//    DLog(@"final string : %@",finalString);
    return finalString;
}

#pragma  - table helper methods -

- (NSString*)getCurrentRunningTableID
{
    NSString *tableID = NULL;
    if (self.lobby.runningTableType == eOpenTableFirst)
    {
        tableID = self.lobby.firstTable.tableIdentifier;
    }
    else if(self.lobby.runningTableType == eOpenTableSecond)
    {
        tableID = self.lobby.secondTable.tableIdentifier;
    }
    return tableID;
}

- (NSString*)getTournamentID
{
    return self.tournamentID;
}

#pragma mark - Internet reconnect alertview -

- (void)showSocketReconnectDialog
{
    [self.socketReconnectAlert show];
}

- (void)onReconnect
{
    DLog(@"****************handle Reconnect********");
    [[TAJClientServerCommunicator sharedClientServerCommunicator] createSocketAndConnect];
    _isReconnectRequest = TRUE;
}

- (void)disconnect
{
    [[TAJClientServerCommunicator sharedClientServerCommunicator] disconnect];
}

#pragma mark - Core Data SAVE

-(NSManagedObjectContext *)managedObjectContext{
    NSManagedObjectContext *context = nil;
    
    TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
    
    
    context = [[delegate persistentContainer] viewContext];
    
    return context;
}

- (void)saveDataIntoCoreDataWithEventName:(NSString *)eventName {
    
    NSString *userID = [[NSUserDefaults standardUserDefaults] objectForKey:USERID];
    
    NSDate *date = [NSDate date];
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd hh:mm:ss"];
    NSString *timeString = [formatter stringFromDate:date];
    NSLog(@"CURRENT TIME : %@",timeString);
    
    NSManagedObjectContext *context = [self managedObjectContext];
    
    NSManagedObject *events = [NSEntityDescription insertNewObjectForEntityForName:@"Events" inManagedObjectContext:context];
    
    [events setValue:eventName forKey:@"event"];
    [events setValue:userID forKey:@"playerid"];
    [events setValue:@"345678" forKey:@"tableid"];
    [events setValue:@"09876" forKey:@"gameid"];
    [events setValue:@"2" forKey:@"subgameid"];
    [events setValue:timeString forKey:@"timestamp"];
    [events setValue:@"1" forKey:@"log_level"];
    [events setValue:@"free" forKey:@"chipstype"];
    [events setValue:@"100" forKey:@"bet"];
    [events setValue:@"Type" forKey:@"gametype"];
    
    NSError *error = nil;
    // Save the object to persistent store
    if (![context save:&error]) {
        NSLog(@"Can't Save! %@ %@", error, [error localizedDescription]);
    }
    else{
        NSLog(@"Success");
        
        // [self fetchDataFromCoreData];
    }
}


@end
