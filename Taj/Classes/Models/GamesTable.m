//
//  GamesTable.m
//  TajRummy
//
//  Created by Grid Logic on 18/08/20.
//  Copyright © 2020 Robosoft Technologies. All rights reserved.
//

#import "GamesTable.h"

@implementation GamesTable

static GamesTable *instance;

@synthesize TAJ_bet,
TAJ_conversion,
TAJ_current_players,
TAJ_favorite,
TAJ_game_schedule,
TAJ_game_start,
TAJ_id,
TAJ_joined_players,
TAJ_maximumbuyin,
TAJ_maxplayer,
TAJ_minimumbuyin,
TAJ_minplayer,
TAJ_schedule_name,
TAJ_site_id,
TAJ_status,
TAJ_stream_id,
TAJ_stream_name,
TAJ_table_cost,
TAJ_table_id,
TAJ_table_type,
TAJ_total_player;

+ (GamesTable *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    GamesTable *copy = [[GamesTable allocWithZone: zone] init];
    
    [copy setTAJ_bet:TAJ_bet];
    [copy setTAJ_conversion:TAJ_conversion];
    [copy setTAJ_current_players:TAJ_current_players];
    [copy setTAJ_favorite:TAJ_favorite];
    [copy setTAJ_game_schedule:TAJ_game_schedule];
    [copy setTAJ_game_start:TAJ_game_start];
    [copy setTAJ_id:TAJ_id];
    [copy setTAJ_joined_players:TAJ_joined_players];
    [copy setTAJ_maximumbuyin:TAJ_maximumbuyin];
    [copy setTAJ_maxplayer:TAJ_maxplayer];
    [copy setTAJ_minimumbuyin:TAJ_minimumbuyin];
    [copy setTAJ_minplayer:TAJ_minplayer];
    [copy setTAJ_schedule_name:TAJ_schedule_name];
    [copy setTAJ_site_id:TAJ_site_id];
    [copy setTAJ_status:TAJ_status];
    [copy setTAJ_stream_id:TAJ_stream_id];
    [copy setTAJ_stream_name:TAJ_stream_name];
    [copy setTAJ_table_cost:TAJ_table_cost];
    [copy setTAJ_table_id:TAJ_table_id];
    [copy setTAJ_table_type:TAJ_table_type];
    [copy setTAJ_total_player:TAJ_total_player];
    
    
    return copy;
}

- (GamesTable *)assignCategoriesData:(NSDictionary *)getTables
{
    TAJ_bet = [[getTables objectForKey:@"TAJ_bet"] integerValue];
    TAJ_conversion = [getTables objectForKey:@"TAJ_conversion"];
    TAJ_game_schedule = [getTables objectForKey:@"TAJ_game_schedule"];
    TAJ_game_start = [getTables objectForKey:@"TAJ_game_start"];
TAJ_id = [getTables objectForKey:@"TAJ_id"];
    TAJ_joined_players =  [getTables objectForKey:@"TAJ_joined_players"];
    TAJ_maximumbuyin = [[getTables objectForKey:@"TAJ_maximumbuyin"] integerValue];
    TAJ_maxplayer = [[getTables objectForKey:@"TAJ_maxplayer"] integerValue];
    TAJ_minimumbuyin = [[getTables objectForKey:@"TAJ_minimumbuyin"] integerValue];
    TAJ_minplayer = [[getTables objectForKey:@"TAJ_minplayer"] integerValue];
    TAJ_schedule_name =  [getTables objectForKey:@"TAJ_schedule_name"];
    TAJ_site_id =  [getTables objectForKey:@"TAJ_site_id"];
    TAJ_status =  [getTables objectForKey:@"TAJ_status"];
    
    return self;
}

@end
