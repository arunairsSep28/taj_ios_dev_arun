/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJClientServerCommunicator.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 07/01/14.
 **/

#import "TAJClientServerCommunicator.h"
#import "TAJGameEngine.h"
#import "TAJXMLParserDictionary.h"
#import "TAJUtilities.h"
#include "TAJConstants.h"

static TAJClientServerCommunicator *sharedController = nil;

@implementation TAJClientServerCommunicator
@synthesize socketConnector = _socketConnector;

+ (id)sharedClientServerCommunicator
{
    if (sharedController == nil)
    {
        sharedController = [[TAJClientServerCommunicator alloc] init];
    }
    return sharedController;
}

+ (void)destroySharedClientServerCommunicator
{
    
}

- (id)init
{
    self = [super init];
    if (self) {
        _socketConnector = Nil;
        _onSocketDatas = [[NSMutableArray alloc] init];
    }
    return self;
}

- (void)createSocketAndConnect
{
    //54.88.243.113 - New test
    //180.179.43.251 - live
    //4271 - Old Port
    //4276 - New Port
    self.socketConnector = Nil;
#if USING_LIVE_SERVER
//    self.socketConnector = [[TAJSocketConnector alloc] initWithDelegate:self onHost:@"54.88.243.113" onPort:4271];
self.socketConnector = [[TAJSocketConnector alloc] initWithDelegate:self onHost:APP_DELEGATE.dynamicIP onPort:4271];
#else
    self.socketConnector = [[TAJSocketConnector alloc] initWithDelegate:self onHost:APP_DELEGATE.dynamicIP onPort:4271];
#endif
    NSLog(@"SOCKET CONNECTION : %@",self.socketConnector);
    NSLog(@"SOCKET DYNAMIC IP : %@",APP_DELEGATE.dynamicIP);

    [self.socketConnector connect];
}

- (void)closeConnection
{
    [[TAJGameEngine sharedGameEngine] stopHeartBeat];
    _socketConnector = nil;
}

- (void)onSocketError
{
    DLog(@"onSocketError");
    [[TAJGameEngine sharedGameEngine] onError];
}

- (void)onSocketConnected:(id)inSocket
{
    DLog(@"onSocketConnected");
    [[TAJGameEngine sharedGameEngine] onConnected];
}

- (void)onSocket:(id)inSocket data:(id)inReceivedData
{
    if (inReceivedData != NULL)
    {
#if ENABLE_DEBUG_FILE_WRITE
        [[TAJUtilities sharedUtilities] writeToTextFile:inReceivedData requestType:false];
#endif
        NSDictionary *dictionary =  NULL;
        dictionary = [NSDictionary dictionaryWithXMLData:inReceivedData];
        //DLog(@"final dictionary : %@",[dictionary description]);
        // DLog(@"Received data : %@",[dictionary description]);
        if (dictionary)
        {
            [[TAJGameEngine sharedGameEngine] parseRecievedData:dictionary];
        }
    }
}

- (void)sendData:(NSData *)inData
{
    //  DLog(@"send Data : %@",[[NSString alloc] initWithData:inData encoding:NSUTF8StringEncoding]);
    [_socketConnector sendData:inData];
#if ENABLE_DEBUG_FILE_WRITE
    [[TAJUtilities sharedUtilities] writeToTextFile:inData requestType:true];
#endif
}

- (void)reconnect
{
    [_socketConnector reconnect];
}

- (void)disconnect
{
    [_socketConnector disconnect];
}

- (void)processDataOnBackgroundThread:(NSData*)inReceivedData
{
#if ENABLE_DEBUG_FILE_WRITE
    [[TAJUtilities sharedUtilities] writeToTextFile:inReceivedData requestType:false];
#endif
    
    int len = 0;
    len = [self.onSocketDatas count];
    if (len > 0)
    {
        NSData *data = NULL;
        data = [self.onSocketDatas objectAtIndex:0];
        [self.onSocketDatas  removeObjectAtIndex:0];
        
        NSString *dataString = NULL;
        dataString = [[NSString alloc] initWithData:inReceivedData encoding:NSUTF8StringEncoding];
        //        DLog(@"onSocket data %@",dataString);
        NSDictionary *dictionary =  NULL;
        dictionary = [NSDictionary dictionaryWithXMLData:inReceivedData];
        // DLog(@"Received data : %@",[dictionary description]);
        if (dictionary)
        {
            [[TAJGameEngine sharedGameEngine] parseRecievedData:dictionary];
        }
    }
}

- (void)destroySocket
{
    self.socketConnector = nil;
}


@end
