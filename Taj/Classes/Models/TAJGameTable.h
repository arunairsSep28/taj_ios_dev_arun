/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJGameTable.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 27/01/14.
 **/

#import <Foundation/Foundation.h>

@interface TAJGameTable : NSObject

@property (nonatomic, strong) NSString *tableIdentifier;//The table identifier
@property (nonatomic, strong) NSMutableDictionary *gameTableDetails;//Game table details at initial
@property (nonatomic, strong) NSMutableDictionary *gameTableViewDetails;//Game table details after joining as view
@property (nonatomic, strong) NSMutableDictionary *gameTablePlayDetails;//Game table details after joining as play
@property (nonatomic)         int mSeatNumber;//Seat number to sit on
@property (nonatomic, strong) NSString *msgUUIDForSearchJoinTable;// UUID for search join table
@property (nonatomic)         BOOL searchJoinTableMode;// To know whether game play will be starting in search join table mode

- (id)init;

@end
