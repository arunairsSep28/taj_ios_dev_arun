/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJGameTable.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 27/01/14.
 **/

#import "TAJGameTable.h"

#define  DEFAULT_SEAT_NUMBER 1
@implementation TAJGameTable

@synthesize tableIdentifier = _tableIdentifier;
@synthesize gameTableDetails = _gameTableDetails;
@synthesize gameTablePlayDetails = _gameTablePlayDetails;
@synthesize gameTableViewDetails = _gameTableViewDetails;
@synthesize mSeatNumber = _mSeatNumber;
@synthesize msgUUIDForSearchJoinTable = _msgUUIDForSearchJoinTable;
@synthesize searchJoinTableMode = _searchJoinTableMode;

- (id)init
{
    self = [super init];
    if (self) {
        _tableIdentifier = Nil;
        _gameTableDetails = Nil;
        _gameTablePlayDetails = Nil;
        _gameTableViewDetails = Nil;
        _mSeatNumber = DEFAULT_SEAT_NUMBER;
        _msgUUIDForSearchJoinTable = Nil;
        _searchJoinTableMode = FALSE;
        return self;
    }
    return nil;

}

@end
