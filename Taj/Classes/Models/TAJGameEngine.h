/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		  TAJGameEngine.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by Raghavendra Kamat on 07/01/14.
 
 **/

#import <Foundation/Foundation.h>
#import "TAJUserModel.h"

@class TAJLobby;
@class TAJProgressAlertView;
@class TAJWebViewController;

@interface TAJGameEngine : NSObject<UIAlertViewDelegate>
{
    
}
@property (nonatomic, strong) TAJLobby *lobby;
@property (nonatomic, strong) TAJUserModel *usermodel;
@property (nonatomic, strong) NSString *msgUUID;
@property (nonatomic, strong) NSString *tournamentID;
@property (nonatomic, strong) NSTimer *heartBeat;
@property (nonatomic, weak)   UIViewController *currentController;
@property (nonatomic, weak)   UIViewController *previousController;
@property (nonatomic)         BOOL joinAsView;//Join the game as view
@property (nonatomic, strong) UINavigationController *navigationController;
@property (nonatomic, strong) UIAlertView *socketReconnectAlert;
@property (nonatomic, strong) UIAlertView *iAmBackAlert;
@property (nonatomic)         BOOL isReconnectRequest;
@property (nonatomic)         UIActivityIndicatorView *activityIndicator;
@property (nonatomic)         TAJProgressAlertView *activityAlertForReconnecting;
@property (nonatomic)         TAJProgressAlertView *alertForOtherLogin;
@property (nonatomic, strong) NSDate* sentTime;
@property (nonatomic, strong) NSDate* currentTime;
@property (nonatomic)         BOOL isHeartBeatRecieved;
@property (nonatomic)         BOOL isSendingHeartBeatForFirstTime;
@property (nonatomic)         BOOL isReconnectActivityAlertActive;
@property (nonatomic)         BOOL joinTableAfterGettingDetails;
@property (nonatomic)         BOOL joinTableTypeJokerNoJoker;
@property (nonatomic, strong) NSString *buyInAmount;
@property (nonatomic)         BOOL isLoggedIn;
@property (nonatomic)         BOOL isOtherLoggedIn;
@property (nonatomic)         BOOL isTournament;

@property (nonatomic, strong) TAJWebViewController *webViewController;
@property (nonatomic)         BOOL showAutoplayStatusPopup;
@property (nonatomic, strong) NSMutableArray *arrayForSlotsInfo;
@property (nonatomic, assign) float timeOut;
@property (nonatomic, strong) NSMutableDictionary *quitTableValues;

@property (nonatomic)         BOOL    isFirstGameStarted;
@property (nonatomic)         BOOL    isSecondGameStarted;
@property (nonatomic)         BOOL    isMiddleJoin;
@property (nonatomic)         BOOL    isListTableRequested;
@property (nonatomic)         BOOL    isTournamentListTableRequested;

//onConnected Timer
@property(nonatomic) NSInteger connectedTime;
@property(nonatomic) double loginTime;

+ (id)sharedGameEngine;
- (id)init;

#pragma mark AlertView Delegate
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex;

#pragma mark Parsing data and handling tags
- (void)parseRecievedData:(NSDictionary*)dictionary;
- (void)createUserModelWithUserName:(NSString*)userName password:(NSString*)password sessionId:(NSString *)sessionId;
- (void)sendDataToServer:(NSData*)data;
- (void)processTheReplyTag:(NSDictionary*)dictionary;
- (void)processTheAuthReqTag:(NSDictionary*)dictionary;
- (void)processTheEventTag:(NSDictionary*)dictionary;
- (void)processTheRequestTag:(NSDictionary*)dictionary;


#pragma mark handle error alert
- (void)showErrorAlert;
- (void)reconnectToSocket;
- (void)onConnected;
- (void)scheduleHeartBeat;
- (void)heartBeatMessage;
- (void)onError;
- (void)stopHeartBeat;
- (void)destroySocketOnIncommingCall;
- (void)destroySocketOnError;
- (void)logout;
- (void)requestAPI:(const char *)api;

- (void)createLobby:(NSDictionary*)dictionary;
- (void)destroyLobby;
- (void)destroyUserModel;
- (void)destroyLobbyUserAndDisconnectSocket;

#pragma mark Joining tables
- (void)joinFirstTable;
- (void)joinSecondTable;

#pragma client requests
- (void)cardDiscardWithFace:(NSString*)face withUserName:(NSString*)name withSuit:(NSString*)suit tableID:(NSString*)tableID userID:(NSString*)userID;
- (void)cardAutoDiscardWithFace:(NSString*)face withUserName:(NSString*)name withSuit:(NSString*)suit tableID:(NSString*)tableID userID:(NSString*)userID;
- (void)cardPickWithFace:(NSString*)face withStack:(NSString*)stack withSuit:(NSString*)suit tableID:(NSString*)tableID userID:(NSString*)userID  nickName:(NSString*)nickName;
- (void)cardsMeldWithTableID:(NSString*)tableID cards:(NSString*)cards suit:(NSString*)suit face:(NSString*)face withMsgUUid:(NSString *)msgUuid;
- (void)cardsMeldForOtherPlayerWithTableID:(NSString*)tableID cards:(NSString*)cards withMsgUUid:(NSString *)msgUuid;
- (void)cardsCheckMeldWithTableID:(NSString*)tableID cards:(NSString*)cards suit:(NSString*)suit face:(NSString*)face withMsgUUid:(NSString *)msgUuid;
- (void)showEventWithSuit:(NSString*)suit face:(NSString*)face tableID:(NSString*)tableID userID:(NSString*)userID nickName:(NSString*)nickname;
- (void)turnExtraTimeEventWithNickName:(NSString*)nickname tableID:(NSString*)tableID userID:(NSString*)userID;
- (void)playerDropEventWithNickName:(NSString*)nickname tableID:(NSString*)tableID userID:(NSString*)userID;
- (void)chatMessageWithUserID:(NSString*)userID tableID:(NSString*)tableID nickName:(NSString*)nickname text:(NSString*)text;
- (void)searchJoinTableWithUserID:(NSString*)userID nickName:(NSString*)nickname bet:(NSString*)bet maxplayers:(NSString*)maxplayers tableType:(NSString*)tableType tableCost:(NSString*) tableCost tableID:(NSString*)tableID conversion:(NSString*)conversion streamName:(NSString*)streamName streamID:(NSString*)streamID gameSettingsID:(NSString*)gameSettingsID msgUUID:(NSString*)msgUUID;
- (void)splitRequestWithTableID:(NSString*)tableID;
- (void)addToFavouriteWithTableID:(NSString*)tableID nickName:(NSString*)nickname userID:(NSString*)userID tableCost:(NSString *)tableCost;
- (void)removeFromFavouriteWithTableID:(NSString*)tableID nickName:(NSString*)nickname userID:(NSString*)userID tableCost:(NSString *)tableCost;

- (void)splitAcceptReplyWithMsgUUID:(NSString*)msgUUID userID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID;
- (void)splitRejectReplyWithMsgUUID:(NSString*)msgUUID userID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID;
- (void)rejoinReplyYesWithUserID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID msgUUID:(NSString*)msgUUID;
- (void)rejoinReplyNoWithUserID:(NSString*)userID nickName:(NSString*)nickname tableID:(NSString*)tableID msgUUID:(NSString*)msgUUID;
- (void)getTableExtraForTableID:(NSString*)tableID;
- (void)iAmBackWithUserID:(NSString*)userID;
- (void)rebuyinWithTableId:(NSString*) tableID userID:(NSString*)userId rebuyinAmt:(NSString*) rebuyinAmount;
- (void)reportABugForTableID:(NSString*)tableID gameId:(NSString*)gameID bugExaplanation:(NSString*)explanation gameType:(NSString*)gameType bugType:(NSString*)bugType;
- (void)standUpWithTableID:(NSString*)tableID tableType:(NSString*)tableType tableCost:(NSString*)tableCost buyinAmount:(NSString*)buyinAmount seat:(NSString*) seat;
- (void)getAutoPlayCountWithTableID:(NSString *)tableID;
- (void)getPlayerCount;
- (void)getTablesCount;
- (void)requestListTable;
- (void)smartCorrectionWithTableID:(NSString*)tableID nickName:(NSString*)nickname userID:(NSString*)userID msgUUID:(NSString*)msgUUID agree:(NSString*)agree gameId:(NSString*)gameId;
- (void)requestTournamentDetails:(NSString *)tournamentID;
- (void)requestGetPrizeList:(NSString *)tournamentID;
- (void)requestGetLevelTimer:(NSString *)tournamentID msgUUID:(NSString*)msgUUID;
- (void)requestGetTournamentTables:(NSString *)tournamentID;
- (void)requestRegisterTournament:(NSString *)tournamentID level:(NSString *)level amt:(NSString *)amt;
- (void)requestDeregisterTournament:(NSString *)tournamentID level:(NSString *)level;
- (void)requestGetTournamentWaitList:(NSString *)tournamentID;
- (void)requestGetLeaderBoard:(NSString *)tournamentID;
- (void)requestGetRegisteredPlayer:(NSString *)tournamentID;
- (void)requestGetPrizeDistribution:(NSString *)tournamentID;
- (void)requestLeaveTourney:(NSString *)tournamentID;
- (void)requestGetTopPlayers:(NSString *)tournamentID level:(NSString*)levelID;
- (void)getTableDetailsAndJoinTableAsPlayTournament:(NSDictionary*)dictionary;
- (void)requestJoinTableReply:(NSString *)uuid;
- (void)tourneyRebuyinWithTourneyId:(NSString*)tourneyID level:(NSString*)level rebuyinAmt:(NSString*) rebuyinAmount;

#pragma mark Table Joining methods
- (void)processJoinTableReply:(NSDictionary*)dictionary;
- (void)processGetTableDetails:(NSDictionary*)dictionary;
- (void)processListTable:(NSDictionary*)dictionary;
- (void)processAuthenticationMessage:(NSDictionary*)dictionary;
- (void)processQuitTableReply:(NSDictionary*)dictionary;
- (void)getTableDetailsAndJoinTableAsView:(NSDictionary*)dictionary;
- (void)getTableDetailsAndJoinTableAsPlay:(NSDictionary*)dictionary;
- (void)getTableDetailsAndDontJoinTable:(NSDictionary*)dictionary;
- (void)quitTable:(NSDictionary*)dictionary;
- (void)joinSeat:(int)seatNumber tableID:(NSString*)tableID buyInAmount:(NSString*)buyInAmount middleJoin:(BOOL)value;
- (void)quitCurrentRunningTable;
- (void)quitTableWithTableID:(NSString *)tableID;
//Adding the heart beat slots information to be sent to engine
- (void)addSlotsInfoForHeartBeat:(NSDictionary*)dictionary;

#pragma mark Events Processing Methods
- (void)processBalanceUpdateEvent:(NSDictionary*)dictionary;
- (void)processGetTableDetailsEvent:(NSDictionary*)dictionary;
- (void)processPlayerJoinEvent:(NSDictionary*)dictionary;
- (void)processStartGameEvent:(NSDictionary*)dictionary;
- (void)processSendStackEvent:(NSDictionary*)dictionary;
- (void)processSendDealEvent:(NSDictionary*)dictionary;
- (void)processGameEndEvent:(NSDictionary*)dictionary;
- (void)processTurnUpdateEvent:(NSDictionary*)dictionary;
- (void)processPlayerQuitEvent:(NSDictionary*)dictionary;
- (void)processTurnTimeOutEvent:(NSDictionary*)dictionary;
- (void)processGameResult:(NSDictionary*)dictionary;
- (void)processGameScheduleEvent:(NSDictionary*)dictionary;
- (void)processGameLevelUpdateEvent:(NSDictionary*)dictionary;
- (void)processSittingSeqEvent:(NSDictionary*)dictionary;
- (void)processShowEvent:(NSDictionary*)dictionary;
- (void)processPlayerEliminateEvent:(NSDictionary*)dictionary;
- (void)processPlayerDropEvent:(NSDictionary*)dictionary;
- (void)processCardDiscardEvent:(NSDictionary*)dictionary;
- (void)processCardPickEvent:(NSDictionary*)dictionary;
- (void)processStackResuffleEvent:(NSDictionary*)dictionary;
- (void)processNewGameEvent:(NSDictionary*)dictionary;
- (void)processChatMSGEvent:(NSDictionary*)dictionary;
- (void)processHeartBeatEvent:(NSDictionary*)dictionary;
- (void)processReportBugEvent:(NSDictionary*)dictionary;
- (void)processShowDealEvent:(NSDictionary*)dictionary;
- (void)processTableStartEvent:(NSDictionary*)dictionary;
- (void)processTableClosedEvent:(NSDictionary*)dictionary;
- (void)processBestOfWinnerEvent:(NSDictionary*)dictionary;
- (void)processPoolWinnerEvent:(NSDictionary*)dictionary;
- (void)processTableTossEvent:(NSDictionary*)dictionary;
- (void)processSplitStatusEvent:(NSDictionary*)dictionary;
- (void)processSplitResultEvent:(NSDictionary*)dictionary;
- (void)processSplitReply:(NSDictionary*)dictionary;
- (void)processRejoinEvent:(NSDictionary*)dictionary;
- (void)processTurnExtraTime:(NSDictionary*)dictionary;
- (void)processGameDeschedule:(NSDictionary*)dictionary;
- (void)processSplitFalseEvent:(NSDictionary*)dictionary;
- (void)processMeldSuccess:(NSDictionary*)dictionary;
- (void)processPreGameResult:(NSDictionary*)dictionary;
- (void)processMeldFailEvent:(NSDictionary*)dictionary;
- (void)processTurnExtraTimeReconnectEvent:(NSDictionary*)dictionary;
- (void)processMeldExtraTimeReconnectEvent:(NSDictionary*)dictionary;
- (void)processShowExtraTimeReconnectEvent:(NSDictionary*)dictionary;
- (void)processRebuyinEvent:(NSDictionary*)dictionary;
- (void)processOtherLoginEvent:(NSDictionary*)dictionary;
- (void)processMeldReply:(NSDictionary*)dictionary;
- (void)processMeldRequest:(NSDictionary*)dictionary;
- (void)processAutoPlayStatusReply:(NSDictionary*)dictionary;
- (void)processAutoPlayStatusEvent:(NSDictionary*)dictionary;
- (void)processGetTableExtraReply:(NSDictionary*)dictionary;
- (void)processRejoinRequest:(NSDictionary*)dictionary;
- (void)processSearchJoinTableReply:(NSDictionary*)dictionary;
- (void)processSendSlotsEvent:(NSDictionary*)dictionary;
- (void)processSplitRequest:(NSDictionary*)dictionary;
- (void)processPlayerCount:(NSDictionary*)dictionary;
- (void)processTableLength:(NSDictionary*)dictionary;
- (void)processStandUpReply:(NSDictionary*)dictionary;


- (void)showIamBackDialogRemovingReconnectingAlert;
- (void)removeReconnectingAlert;

- (void)loadLoginScreenAfterOtherLogin;
- (void)destroyUserNameAndPassword;

#pragma heart beat helper methods
- (NSString*)getInfoStringForDictionary:(NSDictionary*)dictionary;

#pragma table helper methods
- (NSString*)getCurrentRunningTableID;
- (NSString*)getTournamentID;

#pragma mark Internet reconnect alertview

- (void)showSocketReconnectDialog;
- (void)onReconnect;
- (void)disconnect;

@end
