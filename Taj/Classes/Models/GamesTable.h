//
//  GamesTable.h
//  TajRummy
//
//  Created by Grid Logic on 18/08/20.
//  Copyright © 2020 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface GamesTable : NSObject

@property (assign, nonatomic) NSInteger TAJ_bet;
@property (strong, nonatomic) NSString *TAJ_conversion;
@property (assign, nonatomic) NSInteger TAJ_current_players;
@property (assign, nonatomic) BOOL TAJ_favorite;
@property (assign, nonatomic) BOOL TAJ_game_schedule;
@property (assign, nonatomic) BOOL TAJ_game_start;
@property (strong, nonatomic) NSString *TAJ_id;
@property (strong, nonatomic) NSString *TAJ_joined_players;
@property (assign, nonatomic) NSInteger TAJ_maximumbuyin;
@property (assign, nonatomic) NSInteger TAJ_maxplayer;
@property (assign, nonatomic) NSInteger TAJ_minimumbuyin;
@property (assign, nonatomic) NSInteger TAJ_minplayer;
@property (strong, nonatomic) NSString *TAJ_schedule_name;
@property (strong, nonatomic) NSString *TAJ_site_id;
@property (strong, nonatomic) NSString *TAJ_status;
@property (assign, nonatomic) NSInteger TAJ_stream_id;
@property (strong, nonatomic) NSString *TAJ_stream_name;
@property (strong, nonatomic) NSString *TAJ_table_cost;
@property (assign, nonatomic) NSInteger TAJ_table_id;
@property (strong, nonatomic) NSString *TAJ_table_type;
@property (assign, nonatomic) NSInteger TAJ_total_player;


@end

NS_ASSUME_NONNULL_END
