/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJLobby.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 27/01/14.
 **/

#import <Foundation/Foundation.h>
#import "TAJConstants.h"

@class TAJGameTable;
@interface TAJLobby : NSObject

@property (nonatomic, strong) NSMutableDictionary *tableDetails;//Details of  the table to be updated for every event changes
@property (nonatomic, strong) NSMutableArray *freeTableableDetails;
@property (nonatomic, strong) NSMutableArray *cashTableableDetails;
@property (nonatomic, strong) NSMutableArray *vipCashTableDetails;
@property (nonatomic, strong) NSMutableArray *happyCashTableDetails;
@property (nonatomic, strong) NSMutableArray *blazeTableDetails;

@property (nonatomic, strong) TAJGameTable *firstTable;
@property (nonatomic, strong) TAJGameTable *secondTable;

@property (nonatomic) BOOL isFirstTableCreated;
@property (nonatomic) BOOL isSecondTableCreated;

@property (nonatomic) EOpenTableType runningTableType;

@property (nonatomic) NSTimer *poller;

@property (nonatomic,strong) NSMutableArray *allTableEvents;
@property (nonatomic) NSMutableArray *startGameEvents;
@property (nonatomic) NSMutableArray *sendStackEvents;
@property (nonatomic) NSMutableArray *sendDealEvents;
@property (nonatomic) NSMutableArray *gameEndEvents;
@property (nonatomic) NSMutableArray *turnUpdateEvents;
@property (nonatomic) NSMutableArray *playerQuitEvents;
@property (nonatomic) NSMutableArray *turnTimeoutEvents;
@property (nonatomic) NSMutableArray *gameResultEvents;
@property (nonatomic) NSMutableArray *gameScheduleEvents;
@property (nonatomic) NSMutableArray *gameLevelUpdateEvents;
@property (nonatomic) NSMutableArray *playerJoinEvents;
@property (nonatomic) NSMutableArray *gameSittingSeqEvents;
@property (nonatomic) NSMutableArray *showEvents;
@property (nonatomic) NSMutableArray *playerEliminateEvents;
@property (nonatomic) NSMutableArray *playerDropEvents;
@property (nonatomic) NSMutableArray *cardDiscardEvents;
@property (nonatomic) NSMutableArray *cardPickEvents;
@property (nonatomic) NSMutableArray *stackResuffleEvents;
@property (nonatomic) NSMutableArray *eventNewGameEvent;
@property (nonatomic) NSMutableArray *chatMSGEvents;
@property (nonatomic) NSMutableArray *heartBeatEvents;
@property (nonatomic) NSMutableArray *reportBugEvents;
@property (nonatomic) NSMutableArray *showDealEvents;
@property (nonatomic) NSMutableArray *tableStartEvents;
@property (nonatomic) NSMutableArray *tableClosedEvents;
@property (nonatomic) NSMutableArray *bestOfWinnerEvents;
@property (nonatomic) NSMutableArray *poolWinnerEvents;
@property (nonatomic) NSMutableArray *tableTossEvents;
@property (nonatomic) NSMutableArray *splitStatusEvents;
@property (nonatomic) NSMutableArray *splitResultEvents;
@property (nonatomic) NSMutableArray *rejoinEvents;
@property (nonatomic) NSMutableArray *turnExtraTimeEvents;
@property (nonatomic) NSMutableArray *gameDescheduleEvents;
@property (nonatomic) NSMutableArray *splitFalseEvents;
@property (nonatomic) NSMutableArray *meldSuccessEvents;
@property (nonatomic) NSMutableArray *preGameResultEvents;
@property (nonatomic) NSMutableArray *meldFailEvents;
@property (nonatomic) NSMutableArray *turnExtratimeReconnectEvents;
@property (nonatomic) NSMutableArray *meldExtratimeReconnectEvents;
@property (nonatomic) NSMutableArray *showExtratimeReconnectEvents;
@property (nonatomic) NSMutableArray *meldRequestEvents;
@property (nonatomic) NSMutableArray *meldReplyEvents;
@property (nonatomic) NSMutableArray *checkMeldReplyEvents;
@property (nonatomic) NSMutableArray *rejoinRequests;
@property (nonatomic) NSMutableArray *splitReplies;
@property (nonatomic) NSMutableArray *searchJoinTableReplies;
@property (nonatomic) NSMutableArray *sendSlotEvents;
@property (nonatomic) NSMutableArray *splitRequests;
@property (nonatomic) NSMutableArray *rebuyinEvents;
@property (nonatomic) NSMutableArray *otherLoginEvents;
@property (nonatomic) NSMutableArray *rebuyinReplies;
@property (nonatomic) NSMutableArray *playerCount;
@property (nonatomic) NSMutableArray *tableLength;
@property (nonatomic) NSMutableArray *standUpReplies;

@property (nonatomic) NSMutableArray *autoplayStatusReplies;
@property (nonatomic) NSMutableArray *autoplayStatusEvents;
@property (nonatomic) NSMutableArray *getTableExtraReply;

- (id)init;
- (void)assignTheTableDetails:(NSDictionary *)dictionary;
- (void)addNewTable:(NSDictionary *)dictionary;
- (void)removeTable:(NSDictionary *)dictionary;
- (void)createFirstTable:(NSDictionary*)dictionary;
- (void)createSecondTable:(NSDictionary*)dictionary;
- (void)updateFirstTableGameTableDetailsWithSearchJointableDictionary:(NSDictionary*)dictionary;
- (void)updateSecondTableGameTableDetailsWithSearchJointableDictionary:(NSDictionary*)dictionary;
- (void)updateFirstTableWithSearchJointableDictionary:(NSDictionary*)dictionary;
- (void)updateSecondTableWithSearchJointableDictionary:(NSDictionary*)dictionary;
- (void)destroyFirstTable;
- (void)destroySecondTable;
- (void)destroyTable:(NSString*)tableID;
- (void)startPoller;
- (void)stopPoller;
- (NSString*)getCurrentRunningTableID;
- (void)processEventsWithArray:(NSMutableArray*)dataArray andNotification:(NSString*)notificationName;
- (void)processEvents;

#pragma mark event updates
- (void)processStartGameEventUpdate:(NSDictionary*)dictionary;
- (void)processSendStackEventUpdate:(NSDictionary*)dictionary;
- (void)processSendDealEventUpdate:(NSDictionary*)dictionary;
- (void)processGameEndEventUpdate:(NSDictionary*)dictionary;
- (void)processTurnUpdateEventUpdate:(NSDictionary*)dictionary;
- (void)processPlayerQuitEventUpdate:(NSDictionary*)dictionary;
- (void)processTurnTimeoutEventUpdate:(NSDictionary*)dictionary;
- (void)processGameResultEventUpdate:(NSDictionary*)dictionary;
- (void)processGameScheduleEventUpdate:(NSDictionary*)dictionary;
- (void)processLevelUpdateEventUpdate:(NSDictionary*)dictionary;
- (void)processPlayerJoinUpdateEventUpdate:(NSDictionary*)dictionary;
- (void)processSittingSeqUpdateEventUpdate:(NSDictionary*)dictionary;
- (void)processShowEventUpdate:(NSDictionary*)dictionary;
- (void)processPlayerEliminateEventUpdate:(NSDictionary*)dictionary;
- (void)processPlayerDropEventUpdate:(NSDictionary*)dictionary;
- (void)processCardDiscardEventUpdate:(NSDictionary*)dictionary;
- (void)processCardPickEventUpdate:(NSDictionary*)dictionary;
- (void)processStackResuffleEventUpdate:(NSDictionary*)dictionary;
- (void)processNewGameEventUpdate:(NSDictionary*)dictionary;
- (void)processChatMSGEventUpdate:(NSDictionary*)dictionary;
- (void)processHeartBeatEventUpdate:(NSDictionary*)dictionary;
- (void)processReportBugEventUpdate:(NSDictionary*)dictionary;
- (void)processShowDealEventUpdate:(NSDictionary*)dictionary;
- (void)processTableStartEventUpdate:(NSDictionary*)dictionary;
- (void)processTableClosedEvent:(NSDictionary*)dictionary;
- (void)processBestOfWinnerEvent:(NSDictionary*)dictionary;
- (void)processPoolWinnerEvent:(NSDictionary*)dictionary;
- (void)processTableTossEvent:(NSDictionary*)dictionary;
- (void)processSplitStatusEvent:(NSDictionary*)dictionary;
- (void)processSplitResultEvent:(NSDictionary*)dictionary;
- (void)processSplitReply:(NSDictionary*)dictionary;
- (void)processRejoinEvent:(NSDictionary*)dictionary;
- (void)processTurnExtraTime:(NSDictionary*)dictionary;
- (void)processGameDeschedule:(NSDictionary*)dictionary;
- (void)processSplitFalseEvent:(NSDictionary*)dictionary;
- (void)processMeldSuccess:(NSDictionary*)dictionary;
- (void)processPreGameResult:(NSDictionary*)dictionary;
- (void)processMeldFailEvent:(NSDictionary*)dictionary;
- (void)processTurnExtraTimeReconnectEvent:(NSDictionary*)dictionary;
- (void)processMeldExtraTimeReconnectEvent:(NSDictionary*)dictionary;
- (void)processShowExtraTimeReconnectEvent:(NSDictionary*)dictionary;
- (void)processRebuyinEvent:(NSDictionary*)dictionary;
- (void)processOtherLoginEvent:(NSDictionary*)dictionary;
- (void)processMeldReplyEvent:(NSDictionary*)dictionary;
- (void)processCheckMeldReplyEvent:(NSDictionary*)dictionary;
- (void)processMeldRequestEvent:(NSDictionary*)dictionary;
- (void)processRejoinRequest:(NSDictionary*)dictionary;
- (void)processSearchJoinTableReply:(NSDictionary*)dictionary;
- (void)processSendSlotsEvent:(NSDictionary*)dictionary;
- (void)processSplitRequest:(NSDictionary*)dictionary;
- (void)processRebuyinReply:(NSDictionary*)dictionary;
- (void)processAutoPlayStatusReply:(NSDictionary*)dictionary;
- (void)processAutoPlayStatusEvent:(NSDictionary*)dictionary;
- (void)processPlayerCount:(NSDictionary*)dictionary;
- (void)processTableLength:(NSDictionary*)dictionary;
- (void)processStandUpReply:(NSDictionary*)dictionary;
- (void)processGetTableExtraReply:(NSDictionary*)dictionary;
- (void)processSmartCorrectionEvent:(NSDictionary*)dictionary;

@end
