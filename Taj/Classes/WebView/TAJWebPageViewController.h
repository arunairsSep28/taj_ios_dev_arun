/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWebPageViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by Lloyd Praveen Mascarenhas on 03/06/14.
 
 **/

#import <UIKit/UIKit.h>
#import "TAJBaseViewController.h"
#import "TAJConstants.h"

@protocol TAJWebPageViewControllerDelegate;

@interface TAJWebPageViewController : TAJBaseViewController<UIWebViewDelegate>
{
    
}

@property(nonatomic,weak) id<TAJWebPageViewControllerDelegate> webPageDelegate;
@property(nonatomic,assign) WebPageViewOption webPageViewOption;
@property (strong, nonatomic) IBOutlet UIWebView *webPageView;
@property (strong, nonatomic) IBOutlet UIActivityIndicatorView *loadinActivity;
@property (weak, nonatomic) IBOutlet UIButton *backButton;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil webPageOption:(WebPageViewOption)option;
- (IBAction)backButtonTapped:(UIButton *)sender;

- (void) loadWebPage;
- (void)reloadWebPage;

@property NSString* urlLinkString;

@end

@protocol TAJWebPageViewControllerDelegate <NSObject>
@optional
- (void)removeWebPageViewController;

@end

