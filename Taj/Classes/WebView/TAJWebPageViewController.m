/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJWebPageViewController.m
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 
 Created by Lloyd Praveen Mascarenhas on 03/06/14.
 
 **/

#import "TAJWebPageViewController.h"
#import "TAJConstants.h"
#import "TAJUtilities.h"
#import "TAJAppDelegate.h"

@interface TAJWebPageViewController ()<UIWebViewDelegate>

@property (assign, nonatomic)  BOOL allowLoad;


@end

@implementation TAJWebPageViewController

@synthesize urlLinkString;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil webPageOption:(WebPageViewOption)option
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.webPageViewOption=option;
    }
    
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.loadinActivity.hidden = YES;
    
    // Do any additional setup after loading the view from its nib.
    [self loadWebPage];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)backButtonTapped:(UIButton *)sender
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void) loadWebPage
{
    self.webPageView.delegate = self;
    [self.loadinActivity startAnimating];
    NSString *urlString = self.urlLinkString;
    //urlString = PRIVACY_RULES_URL;
    NSURL *url = [NSURL URLWithString:urlString];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webPageView loadRequest:requestObj];
    [self.view addSubview:self.webPageView];
    
//    NSURLRequest *request = [NSURLRequest requestWithURL:url];
//    NSOperationQueue *queue = [[NSOperationQueue alloc] init];
//    [NSURLConnection sendAsynchronousRequest:request queue:queue completionHandler:^(NSURLResponse *response, NSData *data, NSError *error)
//
//     {
//         if ([data length] > 0 && error == nil)
//         {
//             [self.webPageView loadRequest:request];
//         }
//         else if (error != nil)
//         {
//             [self.loadinActivity stopAnimating];
//         }
//     }];
}

- (void)reloadWebPage
{
    [self.webPageView reload];
    self.loadinActivity.hidden=false;
    [self.loadinActivity startAnimating];
}


//- (void)webViewDidFinishLoad:(UIWebView *)webView
//{
//    [self.loadinActivity stopAnimating];
//}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [self.loadinActivity stopAnimating];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

- (BOOL) shouldAutorotate
{
    return YES;
}

- (BOOL)webView:(UIWebView*)webView shouldStartLoadWithRequest:(NSURLRequest*)request navigationType:(UIWebViewNavigationType)navigationType {
    return self.allowLoad;
}

- (void)webViewDidFinishLoad:(UIWebView*)webView {
    self.allowLoad = NO;
     [self.loadinActivity stopAnimating];
}

-(NSUInteger)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationMaskPortrait;
        
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
    
}

@end
