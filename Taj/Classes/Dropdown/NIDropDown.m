//
//  NIDropDown.m
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import "NIDropDown.h"
#import "TAJUtilities.h"
#import "QuartzCore/QuartzCore.h"

#define LOBBY_FONT_NAME    @"AvantGardeITCbyBT-Demi"
#define LOBBY_TOP_PADDING  1.46f
#define LOBBY_TOP_IPAD_PADDING  1.22f


@interface NIDropDown ()
@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *initialSelectedList;

@property(nonatomic, retain) NSArray *imageList;
@end

@implementation NIDropDown
@synthesize table;
@synthesize btnSender;
@synthesize list;
@synthesize initialSelectedList;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;


- (id)showDropDown:(UIButton *)b:(CGFloat *)height:(NSArray *)arr:(NSArray *)imgArr:(NSString *)direction selectedIndices:(NSMutableArray *)selectedArray withTitle:(NSString*)titleLabel
{
    btnSender = b;
    animationDirection = direction;
    self.sectionName=titleLabel ;
    *height=[arr count]* 40.0f+22.0f;
    
    self.padding=0.0f;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        CGRect btn = b.frame;
        self.list = [NSArray arrayWithArray:arr];
        self.initialSelectedList=[[NSArray alloc ]initWithArray:selectedArray copyItems:YES];
        self.selectedIndex=selectedArray;
        self.imageList = [NSArray arrayWithArray:imgArr];
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width*1.3f, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        } else if ([direction isEqualToString:@"down"]) {
            
            if ([TAJUtilities isIPhone])
            {
                self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_PADDING, btn.size.width*1.3f, 0);
                
            }
            else
            {
                self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_IPAD_PADDING, btn.size.width*1.3f, 0);
                
            }
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, btn.size.width*1.3f, 0)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 0;
        table.backgroundColor = [UIColor blackColor];
        //table.backgroundColor = [UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1];
        //        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        table.separatorStyle = UITableViewCellSeparatorStyleNone;
        //        table.separatorColor = [UIColor whiteColor];
        table.separatorColor = [UIColor clearColor];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.3];
        if ([direction isEqualToString:@"up"])
        {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y-*height, btn.size.width*1.3f, *height);
        } else if([direction isEqualToString:@"down"]) {
            
            if ([TAJUtilities isIPhone])
            {
                self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_PADDING, btn.size.width*1.3f, *height);
                
            }
            else
            {
                self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_IPAD_PADDING, btn.size.width*1.3f, *height);
                
            }
        }
        table.frame = CGRectMake(0, 0, btn.size.width*1.35f, *height + 8);
        [UIView commitAnimations];
        [b.superview addSubview:self];
        
        [self addSubview:table];
        //        NSLog(@"NIDROPDOWN POSITION %f",(self.frame.origin.x+btn.size.width*1.3f));
        
        if ((self.frame.origin.x+btn.size.width*1.3f)>320.0f && [TAJUtilities isIPhone])
        {
            self.padding=(self.frame.origin.x+btn.size.width*1.3f)-320.0f;
            //NSLog(@"PADDING : %f",self.padding);
            if ([direction isEqualToString:@"up"])
            {
                self.frame = CGRectMake(btn.origin.x-self.padding, btn.origin.y-*height, btn.size.width*1.3f, *height);
            } else if([direction isEqualToString:@"down"]) {
                //self.frame = CGRectMake(btn.origin.x-self.padding, btn.origin.y+btn.size.height*LOBBY_TOP_PADDING, btn.size.width*1.3f, *height);
                self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_PADDING, btn.size.width*1.3f, *height);
            }
        }
        
        
    }
    return self;
}

-(void)hideDropDown:(UIButton *)b duration:(float)animDuration refreshView:(BOOL) isRefresh
{
    CGRect btn = b.frame;
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animDuration];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(btn.origin.x, btn.origin.y, btn.size.width*1.3f, 0);
    } else if ([animationDirection isEqualToString:@"down"]) {
        
        if ([TAJUtilities isIPhone])
        {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_PADDING, btn.size.width*1.3f, 0);
            
        }
        else
        {
            self.frame = CGRectMake(btn.origin.x, btn.origin.y+btn.size.height*LOBBY_TOP_IPAD_PADDING, btn.size.width*1.3f, 0);
            
        }
    }
    table.frame = CGRectMake(0, 0, btn.size.width*1.35f, 0);
    [UIView commitAnimations];
    
    if (isRefresh)
    {
        [self myDelegate];
        
    }
}


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 40;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        
        if ([TAJUtilities isIPhone])
        {
            cell.textLabel.font = [cell.textLabel.font fontWithSize:12];
        }
        [cell.textLabel setFont:[UIFont fontWithName:LOBBY_FONT_NAME size:cell.textLabel.font.pointSize]];
        
        cell.contentView.backgroundColor=[UIColor blackColor];
        //cell.contentView.backgroundColor=[UIColor colorWithRed:66.0/255.0 green:66.0/255.0 blue:66.0/255.0 alpha:1];
        cell.textLabel.textAlignment = UITextAlignmentLeft;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
    }
    
    if ([self.imageList count] == [self.list count])
    {
        cell.textLabel.text =[list objectAtIndex:indexPath.row];
        cell.imageView.image = [imageList objectAtIndex:indexPath.row];
    }
    else if ([self.imageList count] > [self.list count])
    {
        cell.textLabel.text =[list objectAtIndex:indexPath.row];
        if (indexPath.row < [imageList count])
        {
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        }
    } else if ([self.imageList count] < [self.list count])
    {
        cell.textLabel.text =[list objectAtIndex:indexPath.row];
        if (indexPath.row < [imageList count])
        {
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        }
    }
    
    cell.textLabel.textColor = [UIColor colorWithRed: 162.0/255.0 green: 162.0/255.0 blue:162.0/255.0 alpha:1.0f];;//[UIColor whiteColor];
    
    UIImageView* imageview = [[UIImageView alloc] init];
    imageview.image = [UIImage imageNamed:@"tick-568h@2x~iphone.png"];
    
    if ([TAJUtilities isIPhone])
    {
        imageview.frame = CGRectMake(self.table.bounds.size.width*0.7f, 12, 20, 20);
    }
    else
    {
#if DEBUG
        //NSLog(@"%f",self.btnSender.bounds.size.width);
#endif
        imageview.frame = CGRectMake(self.table.bounds.size.width*0.7f, 12, 20, 20);
    }
    [imageview setTag:1234];
    [imageview setHidden:YES];
    
    if ([self.selectedIndex containsObject:[NSString stringWithFormat:@"%s",[cell.textLabel.text UTF8String] ]])
    {
        [cell.textLabel setTextColor:[UIColor colorWithRed:(251.0f/255.f) green:(176.0f/255.f) blue:(59.0f/255.f) alpha:100.0f]];
        
        [imageview setHidden:NO];
    }
    
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor grayColor];
    cell.selectedBackgroundView = v;
    
    [cell addSubview:imageview];
    
    UIView *imgView = [[UIView alloc] init];
    imgView.frame = CGRectMake(0, cell.frame.size.height - 4, cell.frame.size.width, 1);
    imgView.backgroundColor = [UIColor whiteColor];
    [cell.contentView addSubview:imgView];
    
    return cell;
}

-(void) selectSelection:(NSString *)title
{
    NSString * tempString=[NSString stringWithFormat:@"%@",title];
    [btnSender setTitle:tempString forState:UIControlStateNormal];
}

-(void) deselectSelection
{
    [btnSender setTitle:@"Select" forState:UIControlStateNormal];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString * tempString;
    //    [self hideDropDown:btnSender duration:0.5f];
    
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    
    //ReDIM Changes
    NSString* selectedText = [NSString stringWithFormat:@"%s",[c.textLabel.text UTF8String]];
    if ([self.selectedIndex containsObject:selectedText])
    {
        [self.selectedIndex removeObject:[NSString stringWithFormat:@"%s",[c.textLabel.text UTF8String]]];
        [c.textLabel setTextColor: [UIColor colorWithRed: 251.0/255.0 green: 176.0/255.0 blue:59.0/255.0 alpha:1.0f]];
        [[c viewWithTag:1234] setHidden:YES];
    }
    else
    {
        if ([c.textLabel.text compare:POOLS_LABEL_LOBBY]==0)
        {
            self.mVariantAdded=1;
        }
        else if([c.textLabel.text compare:DEALS_LABEL_LOBBY]==0)
        {
            self.mVariantAdded=2;
        }
        else if([c.textLabel.text compare:STRIKES_LABEL_LOBBY]==0)
        {
            self.mVariantAdded=3;
        }
//        else if([c.textLabel.text compare:FAVOURITES_LABEL_LOBBY]==0)
//        {
//            self.mVariantAdded=4;
//        }

        if ([c.textLabel.text compare:TOURNAMENTS_LABEL_LOBBY]==0) {//ReDIM Changes
            [self.selectedIndex removeAllObjects];
        } else {
            [self.selectedIndex removeObject: TOURNAMENTS_LABEL_LOBBY];
        }
        
        [self.selectedIndex addObject:[NSString stringWithFormat:@"%s",[c.textLabel.text UTF8String]]];
        [[c viewWithTag:1234] setHidden:NO];
        [c.textLabel setTextColor:[UIColor colorWithRed:(251.0f/255.f) green:(176.0f/255.f) blue:(59.0f/255.0f) alpha:1.0f]];
    }
    
    for (UIView *subview in btnSender.subviews)
    {
        if ([subview isKindOfClass:[UIImageView class]])
        {
            [subview removeFromSuperview];
        }
    }
    imgView.image = c.imageView.image;
    imgView = [[UIImageView alloc] initWithImage:c.imageView.image];
    imgView.frame = CGRectMake(5, 5, 25, 25);
    [btnSender addSubview:imgView];
    
    if ([self.selectedIndex count]==1)
    {
        tempString=[NSString stringWithFormat:@"%@",((NSString*)[self.selectedIndex objectAtIndex:0])] ;
#if DEBUG
        NSLog(@"SET TITLE : %@",tempString);
#endif
    }
    else if([self.selectedIndex count]==0)
    {
        tempString=[NSString stringWithFormat:@"Select"];
    }
    else if([self.selectedIndex count]==[self.list count])
    {
        tempString=[NSString stringWithFormat:@"Any"];
    }
    else if([self.selectedIndex count]>1)
    {
        tempString=[NSString stringWithFormat:@"Multiple"];
    }
    
    [btnSender setTitle:tempString forState:UIControlStateNormal];
    
    [self myDelegate];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *headerView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width, 40)];
    
    [headerView setBackgroundColor:[UIColor blackColor]];
    
    UILabel *customLabel = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, tableView.bounds.size.width - 10, 28)];
    customLabel.text = self.sectionName;
    
    if ([TAJUtilities isIPhone])
    {
        customLabel.font = [UIFont boldSystemFontOfSize:10];
    }
    else
    {
        customLabel.font = [UIFont boldSystemFontOfSize:18];
    }
    
    [customLabel setTextColor:[UIColor colorWithRed:(255.0f/255.f) green:(255.0f/255.f) blue:(255.0f/255.f) alpha:1.0f]];
    customLabel.backgroundColor = [UIColor clearColor];
    customLabel.textAlignment=NSTextAlignmentCenter;
    UIImageView* backgroundImageview = [[UIImageView alloc] init];
    backgroundImageview.image = [UIImage imageNamed:@"top_menu_patch-568h@2x~iphone.png"];
    backgroundImageview.frame = CGRectMake(headerView.frame.origin.x, headerView.frame.origin.y, headerView.frame.size.width, 30);
    [headerView addSubview:backgroundImageview];
    
    UIImageView* imageview = [[UIImageView alloc] init];
    imageview.image = [UIImage imageNamed:@"orangearrow-568h@2x~iphone.png"];
    imageview.frame = CGRectMake(self.padding+headerView.frame.origin.x+headerView.frame.size.width*0.1f, (headerView.frame.origin.y-5), 20, 20);
    
    if (![TAJUtilities isIPhone])
    {
        imageview.frame = CGRectMake(self.padding+headerView.frame.origin.x+headerView.frame.size.width*0.1f, (headerView.frame.origin.y-5), 26, 26);
    }
    
    [headerView addSubview:imageview];
    
    [headerView addSubview:customLabel];
    
    return headerView;
}

- (void) myDelegate
{
    BOOL pool=false;
    BOOL deal=false;
    BOOL point=false;
    //BOOL favourites = false;

    if ([self.initialSelectedList count]>0 )
    {
        if (![self.initialSelectedList containsObject:POOLS_LABEL_LOBBY] && [self.selectedIndex containsObject:POOLS_LABEL_LOBBY])
        {
            pool=true;
        }
        if (![self.initialSelectedList containsObject:DEALS_LABEL_LOBBY] && [self.selectedIndex containsObject:DEALS_LABEL_LOBBY])
        {
            deal=true;
        }
        if (![self.initialSelectedList containsObject:STRIKES_LABEL_LOBBY] && [self.selectedIndex containsObject:STRIKES_LABEL_LOBBY])
        {
            point=true;
        }
//        if (![self.initialSelectedList containsObject:FAVOURITES_LABEL_LOBBY] && [self.selectedIndex containsObject:FAVOURITES_LABEL_LOBBY])
//        {
//            favourites=true;
//        }
    }
    //[self.delegate niDropDownDelegateMethod:self ifPools:pool ifDeals:deal ifPoints:point ifFavourites:favourites];
    [self.delegate niDropDownDelegateMethod:self ifPools:pool ifDeals:deal ifPoints:point];

}

@end
