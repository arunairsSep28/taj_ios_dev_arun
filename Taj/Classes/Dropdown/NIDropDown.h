//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod: (NIDropDown *) sender ifPools:(BOOL)ifpool ifDeals:(BOOL)ifdeals ifPoints:(BOOL)ifpoints;
//- (void) niDropDownDelegateMethod: (NIDropDown *) sender ifPools:(BOOL)ifpool ifDeals:(BOOL)ifdeals ifPoints:(BOOL)ifpoints ifFavourites:(BOOL)ifFavourites;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
@property (nonatomic, retain) NSMutableArray* selectedIndex;
@property (nonatomic, retain) NSString* sectionName;
@property (nonatomic, assign) float padding;
@property (nonatomic, assign) int mVariantAdded;


-(void)hideDropDown:(UIButton *)b duration:(float)animDuration refreshView:(BOOL) isRefresh;

-(void) deselectSelection;
-(void) selectSelection:(NSString *)title;
- (id)showDropDown:(UIButton *)b:(CGFloat *)height:(NSArray *)arr:(NSArray *)imgArr:(NSString *)direction selectedIndices:(NSMutableArray *)selectedArray withTitle:(NSString*)titleLabel;

@end
