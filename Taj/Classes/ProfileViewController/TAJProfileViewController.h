/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJProfileViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 24/02/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJProfileViewController : UIViewController


@property (nonatomic,assign) BOOL isInEditState;
@property (weak, nonatomic) IBOutlet UITextField *playerNameField;
@property (weak, nonatomic) IBOutlet UITextField *genderField;
@property (weak, nonatomic) IBOutlet UITextField *locationField;
@property (weak, nonatomic) IBOutlet UITextField *cashChipsField;
@property (weak, nonatomic) IBOutlet UITextField *funChipsField;
@property (weak, nonatomic) IBOutlet UITextField *emailIDField;


- (IBAction)editAction:(id)sender;
- (IBAction)buyChipsAction:(id)sender;
- (IBAction)changePasswordAction:(id)sender;
- (IBAction)faceBookLoginAction:(id)sender;
- (IBAction)twitterLoginAction:(id)sender;

@end
