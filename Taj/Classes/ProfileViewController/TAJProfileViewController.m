/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJProfileViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 24/02/14.
 **/

#import "TAJProfileViewController.h"
#import "TAJConstants.h"
#import "TAJGameEngine.h"
#import "TAJUserModel.h"
#import "TAJWebViewController.h"

@interface TAJProfileViewController ()

- (void) updateProfileInformation;
- (void) changeTextFieldEditableProperty:(BOOL) value;

@end

@implementation TAJProfileViewController
@synthesize isInEditState = _isInEditState;
@synthesize playerNameField = _playerNameField;
@synthesize genderField = _genderField;
@synthesize locationField = _locationField;
@synthesize cashChipsField = _cashChipsField;
@synthesize funChipsField = _funChipsField;
@synthesize emailIDField = _emailIDField;

- (id)init
{
    self = [super init];
    if (self) {
        // Custom initialization
        _isInEditState = FALSE;
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    [self updateProfileInformation];
    
    [self changeTextFieldEditableProperty:FALSE];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)editAction:(id)sender
{
    UIButton *button = (UIButton*)sender;
    if (!_isInEditState)
    {
        _isInEditState = TRUE;
        [button setTitle:@"SAVE" forState:UIControlStateNormal];
        [self changeTextFieldEditableProperty:TRUE];
    }
    else
    {
        _isInEditState = FALSE;
        [button setTitle:@"EDIT" forState:UIControlStateNormal];
        [self changeTextFieldEditableProperty:FALSE];
    }
}

- (IBAction)buyChipsAction:(id)sender
{
    TAJWebViewController *controller = [TAJ_GAME_ENGINE getWebViewController];
    [self.navigationController pushViewController:controller animated:YES];
}

- (IBAction)changePasswordAction:(id)sender {
}

- (IBAction)faceBookLoginAction:(id)sender {
}

- (IBAction)twitterLoginAction:(id)sender {
}

- (void) updateProfileInformation
{
    TAJGameEngine *engine = TAJ_GAME_ENGINE;
    TAJUserModel *model = engine.usermodel;
    
    [self.playerNameField setText:model.nickName];
    [self.genderField setText:model.gender];
    [self.cashChipsField setText:model.realChips];
    [self.funChipsField setText:model.funchips];
}

- (void) changeTextFieldEditableProperty:(BOOL) value
{
    [self.playerNameField setEnabled:value];
    [self.genderField setEnabled:value];
    [self.locationField setEnabled:value];
    [self.cashChipsField setEnabled:value];
    [self.funChipsField setEnabled:value];
    [self.emailIDField setEnabled:value];
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
}

@end
