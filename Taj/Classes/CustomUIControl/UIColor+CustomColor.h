//
//  UIColor+CustomColor.h
//  Funoon
//
//  Created by Fazululla Kalluru on 03/04/18.
//  Copyright © 2018 safacs. All rights reserved.
//

#import <UIKit/UIKit.h>

#define RGB2UIColor(r, g, b) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:1.0]
#define RGB2UIColorCustomAlpha(r, g, b) [UIColor colorWithRed:((r) / 255.0) green:((g) / 255.0) blue:((b) / 255.0) alpha:0.5]

@interface UIColor (CustomColor)

+ (UIColor *)themeColor;
+ (UIColor *)statusBarColor;
+ (UIColor *)greenishColor;
+ (UIColor *)inputBorderColor;
+ (UIColor *)buttonBorderColor;
+(UIColor *)selectedGameTypeColor;
+(UIColor *)lobbySelectedGameTypeColor;

@end

