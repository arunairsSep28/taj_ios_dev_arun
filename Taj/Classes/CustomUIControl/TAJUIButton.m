/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJUIButton.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 01/05/14.
 **/

#import "TAJUIButton.h"

@implementation TAJUIButton

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        // Initialization code
    }
    return self;
}

//- (void)setHighlighted:(BOOL)highlighted {
//    [super setHighlighted:highlighted];
//    
//    if (highlighted)
//    {
//        self.backgroundColor = [UIColor orangeColor];
//        
//    }
//    
//    else
//    {
//        self.backgroundColor = [UIColor clearColor];
//    }
//    
//}

-(void)setBackgroundColor:(UIColor *)backgroundColor
{
    self.backgroundColor = [UIColor orangeColor];
}

@end
