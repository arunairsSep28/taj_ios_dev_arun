//
//  UIColor+CustomColor.m
//  Funoon
//
//  Created by Fazululla Kalluru on 03/04/18.
//  Copyright © 2018 safacs. All rights reserved.
//

#import "UIColor+CustomColor.h"

@implementation UIColor (CustomColor)

+ (UIColor *)statusBarColor
{
    return RGB2UIColor(165, 87, 109);
}

+ (UIColor *)themeColor
{
    return RGB2UIColor(242, 131, 160);
}

+ (UIColor *)greenishColor
{
    return RGB2UIColor(72, 169, 45);
}

+ (UIColor *)inputBorderColor
{
    return RGB2UIColor(128, 128, 128);
}

+ (UIColor *)buttonBorderColor
{
    return RGB2UIColor(202, 202, 202);
}

+(UIColor *)selectedGameTypeColor
{
    return RGB2UIColor(134, 0, 0);
}

+(UIColor *)lobbySelectedGameTypeColor
{
    return RGB2UIColor(165, 0, 0);
}
@end
