/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJUITextField.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Yogisha Poojary on 28/04/14.
 **/

#import "TAJUITextField.h"
#import "TAJUtilities.h"

@implementation TAJUITextField


- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        if([TAJUtilities isIPhone])
        {
            self.edgeInsets = UIEdgeInsetsMake(0, 27, 0, 56);

        }
        else
        {
            self.edgeInsets = UIEdgeInsetsMake(0, 27, 0, 54);
 
        }
    }
    return self;
}

-(id)initWithCoder:(NSCoder *)aDecoder{
    self = [super initWithCoder:aDecoder];
    if(self)
    {
        if([TAJUtilities isIPhone])
        {
            self.edgeInsets = UIEdgeInsetsMake(0, 27, 0, 56);
            
        }
        else
        {
            self.edgeInsets = UIEdgeInsetsMake(0, 27, 0, 54);
            
        }

    }
    return self;
}

- (CGRect)textRectForBounds:(CGRect)bounds {
    return [super textRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

- (CGRect)editingRectForBounds:(CGRect)bounds {
    return [super editingRectForBounds:UIEdgeInsetsInsetRect(bounds, self.edgeInsets)];
}

//- (id)initWithFrame:(CGRect)frame
//{
//    self = [super initWithFrame:frame];
//    if (self)
//    {
//        // Initialization code
//    }
//    return self;
//}
//
//
//
//- (void)awakeFromNib
//{
//    self.clearButtonMode = UITextFieldViewModeWhileEditing;
//}
//

- (CGRect)clearButtonRectForBounds:(CGRect)bounds
{
    CGRect originalRect = [super clearButtonRectForBounds:bounds];
    if([TAJUtilities isIPhone])
    {
        return CGRectOffset(originalRect, -40, 0); //shift the button 10 points to the left
 
    }
    else
    {
        return CGRectOffset(originalRect, -60, 0); //shift the button 10 points to the left

    }
}

//// placeholder position
//- (CGRect)textRectForBounds:(CGRect)bounds
//{
//    return CGRectInset( bounds , 22 , 0 );
//}
//
//// text position
//- (CGRect)editingRectForBounds:(CGRect)bounds
//{
//    return CGRectInset( bounds , 22 , 0 );
//}
//
//-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
//{
//    [self resignFirstResponder];
//    
//}


@end
