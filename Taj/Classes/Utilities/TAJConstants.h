/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJConstants.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 24/01/14.
 **/

#ifndef Taj_TAJConstants_h
#define Taj_TAJConstants_h

#define SYSTEM_VERSION_GRATERTHAN_OR_EQUALTO(v)  ([[[UIDevice currentDevice] systemVersion] compare:v options:NSNumericSearch] != NSOrderedAscending)

#define kTAJLocaleBundle [[NSBundle mainBundle] pathForResource:@"Taj" ofType:@"bundle"]
#define TAJLocalizedString(stringKey) \
[[NSBundle bundleWithPath:kTAJLocaleBundle] localizedStringForKey:stringKey value:stringKey table:@"TajRummyLocale"]

#define ENABLE_DEBUG_FILE_WRITE 0

#define VERSION_NUMBER @""

#define ENABLE_HEART_BEAT 1
#define ENABLE_HEART_BEAT_IMPLEMENTATION_TYPE_TWO 1
#define USERNAME_PASSWORD_STORE_CHECK 1
#define ENABLE_RECONNECT_ALERT 1
#define INCLUDE_SPLASH  1
#define NEW_DESIGN_IMPLEMENTATION   1
#define NEW_INFO_VIEW_IMPLEMENTATION 1
#define CARD_GROUPING_SLOTS         1
#define LANDSCAPE_MODE_GAME         1
#define GAME_ENGINE_FORCE_QUIT      1
#define PLAYER_MODEL_GAME_CARD      1
#define GAME_ENGINE_MIDDLE_JOIN     1
#define NEW_LOBBY_EVENT_PROCESSING  1
#define PLAYER_MODEL_GAME_CARD_ANIMATION  1
#define PLAYER_MODE_INFO_DISPLAY          1
#define LIVE_RELEASE 0


#define FORGOT_PASSWORD_SUCCESS_MESSAGE         @"An email with a link to set your new password have been sent to you"
#define FORGOT_PASSWORD_NOT_REGISTERED_MESSAGE  @"The Email address entered is not registered with us"
#define INVALID_EMAIL_MESSAGE                   @"Please enter valid Email address."

#define PURCHASE_URL @"https://www.tajrummy.com/sendpaymentrequest/"

#define RESPONSIBLE_GAMING_URL @"https://www.tajrummy.com/rummy-responsible-gaming/"
#define GAME_RULES_URL @"https://www.tajrummy.com/learn-how-to-play-rummy-games/"
#define PRIVACY_RULES_URL @"https://www.tajrummy.com/indian-rummy-privacy-policy/"
#define TERMS_CONDITION_URL @"https://www.tajrummy.com/rummy-terms-and-conditions/"

#define FORGOT_PASSWORD_URL_LIVE @"mobilepasswordreset/?email=%@"
#define REGISTRATION_URL_LIVE    @"http://www.tajrummy.com/mobilesignup/?Username=%@&Password=%@&Email=%@&client_type=ios&device_type=%@&version=%@"
#define BONUS_CHUNKS_URL_LIVE    @"https://www.tajrummy.com/flashbonuschunksinfo/%s";
#define FORGOT_PASSWORD_URL      @"mobilepasswordreset/?email=%@"

#define REGISTRATION_URL    @"http://www.tajrummy.com/mobilesignup/?Username=%@&Password=%@&Email=%@&client_type=ios&device_type=%@&version=%@"
#define BONUS_CHUNKS_URL         @"https://www.tajrummy.com/flashbonuschunksinfo/%s";

#define TAJ_SHARED_UTILITIES            [TAJUtilities sharedUtilities]
#define TAJ_GAME_ENGINE                 [TAJGameEngine sharedGameEngine]
#define TAJ_CLIENT_SERVER_COMMUNICATOR  [TAJClientServerCommunicator sharedClientServerCommunicator]
#define NSNOTIFICATION_CENTRE_DEFAULT   [NSNotificationCenter defaultCenter]
#define TAJ_APP_DELEGATE                [[UIApplication sharedApplication] delegate]

//ratheesh
#define APP_DELEGATE ((TAJAppDelegate*)[[UIApplication sharedApplication] delegate])
//ratheesh

#define TAP_HERE_TO_RETRY        @"Tap Retry to try again"
#define LOGGING_IN               @"Logging in..."
#define LOADING_LOGIN_HOME       @"Loading..."

#pragma mark LEAVE TABLE CONSTANTS

#define LEAVE_TABLE_TITLE               @"Leave Table"
#define LEAVE_TABLE_MESSAGE             @"Are you sure you want to leave the table?"
#define CANNOT_LEAVE_TABLE_MESSAGE      @"You cannot leave the table at this stage."
#define LEAVE_TABLE_SCHEDULE            @"You cannot leave table during game schedule"
#define LEAVE_TABLE_TOSS                @"You cannot leave the table during the toss"
#define LEAVE_TABLE_14CARD              @"You cannot leave after picking card"
#define LEAVE_TABLE_MELD                @"You cannot leave table during meld"
#define LEAVE_TABLE_CARD_DISTRIBUTION   @"You cannot leave the table during card distribution"
#define ELEMINATE_TOURNEY_MESSAGE      @"Sorry, you are eleminated from this tournament."

#define NO_STRING               @"NO"
#define NO_STRING_FOR_POPUP     @"No"
#define YES_STRING              @"YES"
#define YES_STRING_FOR_POPUP    @"Yes"
#define CARDS_PICKED_COUNT      14
#define OK_STRING               @"OK"
#define OK_STRING_FOR_POPUP     @"Ok"
#define CANCEL_STRING           @"Cancel"
#define DEPOSIT_STRING               @"Deposit"

#define LIST_TABLE              @"list_gamesettings"
#define CHIPRELOAD              @"chipreload"
#define LIST_TOURNAMENTS        @"list_tournaments"
#define GET_TOURNAMENT_DETAIL   @"get_tournament_details"
#define GET_TOURNAMENT_PRIZE_LIST   @"get_prize_list"
#define TOURNAMENT_LEVEL_START @"level_start"
#define TOURNAMENT_LEVEL_SCHEDULE @"level_schedule"
#define TOURNAMENT_LEVEL_END @"level_end"
#define TOURNAMENT_GET_LEVEL_TIMER @"get_level_timer"
#define TOURNAMENT_TABLES @"get_tournament_tables"
#define TOURNAMENT_RESULT @"tournament_result"
#define TOURNAMENT_REGISTER @"register_tournament"
#define TOURNAMENT_DEREGISTER @"deregister_tournament"
#define GET_TOURNAMENT_WAIT_LIST   @"tournament_wait_list"
#define GET_TOURNAMENT_REGISTERED_PLAYER   @"get_registered_player"
#define GET_TOURNAMENT_LEADER_BOARD   @"leader_board"
#define GET_TOURNAMENT_PRIZE_DISTRIBUTION   @"prize_distribution"

#define SHOW_TOURNAMENT   @"show_tournament"
#define END_TOURNAMENT   @"end_tournament"
#define TOURNAMENT_TO_START   @"tournament_to_start"
#define START_TOURNAMENT   @"start_tournament"
#define LEAVE_TOURNAMENT   @"leave_tournament"
#define START_REGISTRATION   @"start_registration"
#define STOP_REGISTRATION   @"stop_registration"
#define STOP_CANCEL_REGISTRATION   @"stop_cancel_registration"
#define DISQUALIFIED   @"disqualified"
#define PLAYER_REGISTERED   @"player_registered"
#define PLAYER_DEREGISTERED   @"player_deregistered"
#define BREAKUPTIME   @"breakuptime"
#define TOURNEY_END_WAIT   @"tourney_end_wait"
#define LEVEL_TOP_PLAYER   @"level_top_player"
#define TOURNEY_BALANCE   @"TOURNEY_BALANCE"
#define PLAYERS_RANK   @"players_rank"
#define REQUEST_JOIN_TABLE   @"request_join_table"
#define TOURNEY_REBUYIN_REPLY   @"tournament_rebuyin"
#define TOURNEY_REBUYIN_EVENT   @"tournament_rebuyin"
#define TOURNEY_ELEMINATE   @"tournament_eleminate"

#define GET_TABLE_DETAILS       @"get_table_details"
#define JOIN_TABLE              @"join_table"
#define QUIT_TABLE              @"quit_table"

#define TABLE_TYPE_101_POOL    @"101_POOL"
#define TABLE_TYPE_201_POOL    @"201_POOL"
#define TABLE_TYPE_BEST_OF_3   @"BEST_OF_3"
#define TABLE_TYPE_BEST_OF_2   @"BEST_OF_2"
#define TABLE_TYPE_BEST_OF_6   @"BEST_OF_6"
#define TABLE_TYPE_PR_JOKER    @"PR_JOKER"
#define TABLE_TYPE_PR_NO_JOKER @"PR_NOJOKER"

#define TABLE_TYPE_101_POOL_DISPLAY_STRING    @"101 Pool"
#define TABLE_TYPE_201_POOL_DISPLAY_STRING    @"201 Pool"
#define TABLE_TYPE_BEST_OF_3_DISPLAY_STRING   @"Best Of 3"
#define TABLE_TYPE_BEST_OF_2_DISPLAY_STRING   @"Best Of 2"
#define TABLE_TYPE_BEST_OF_6_DISPLAY_STRING   @"Best Of 6"
#define TABLE_TYPE_PR_JOKER_DISPLAY_STRING    @"PR - Joker"
#define TABLE_TYPE_PR_NO_JOKER_DISPLAY_STRING @"PR - No Joker"

#define TAJ_COMMAND_NAME @"TAJ_name"

#define REPLY_COMMAND     @"reply"
#define AUTH_REQ_COMMAND  @"authreq"
#define REQUEST_COMMAND   @"request"
#define EVENT_COMMAND     @"event"

#define TAJ_DATA @"TAJ_data"
#define TAJ_CODE @"TAJ_code"
#define CODE_200 @"200"
#define CODE_500 @"500"
#define CODE_100 @"100"
#define CODE_SEARCH_JOIN_ERROR @"410"
#define CODE_474 @"474"
#define CODE_483 @"483"

#define TAJ_EVENT_NAME           @"TAJ_event_name"
#define TAJ_MSG_UUID             @"TAJ_msg_uuid"
#define TAJ_REQUEST_COMMAND_TYPE @"TAJ_command"

#define BALANCE_UPDATE           @"BALANCE_UPDATE"
#define GET_TABLE_DETAILS_EVENT  @"get_table_details_event"
#define PLAYER_JOIN              @"PLAYER_JOIN"
#define START_GAME               @"START_GAME"
#define DEVICE_CHANGE            @"device_change"
#define ENGINE_STATUS            @"enginestatus"
#define SEND_STACK               @"SEND_STACK"
#define SEND_DEAL                @"SEND_DEAL"
#define GAME_END                 @"GAME_END"
#define TURN_UPDATE              @"TURN_UPDATE"
#define PLAYER_QUIT              @"PLAYER_QUIT"
#define TURN_TIMEOUT             @"TURN_TIMEOUT"
#define GAME_RESULT              @"GAME_RESULT"
#define GAME_SCHEDULE            @"GAME_SCHEDULE"
#define GAME_LEVEL_UPDATE        @"level_update"
#define GAME_SITTING_SEQUENCE    @"SITTING_SEQ"
#define SHOW_EVENTS              @"SHOW"
#define SMART_CORRECTION         @"wrong_meld_correction"
#define PLAYER_ELIMINATE         @"PLAYER_ELIMINATE"
#define PLAYER_DROP              @"PLAYER_DROP"
#define CARD_DISCARD             @"CARD_DISCARD"
#define CARD_PICK                @"CARD_PICK"
#define STACK_RESUFFLE           @"STACK_RESUFFLE"
#define NEW_GAME                 @"NEW_GAME"
#define CHAT_MSG                 @"CHAT_MSG"
#define HEART_BEAT               @"HEART_BEAT"
#define REPORT_BUG               @"REPORT_BUG"
#define SHOW_DEAL                @"SHOW_DEAL"
#define TABLE_START              @"TABLE_START"
#define TABLE_CLOSED             @"TABLE_CLOSED"
#define BEST_OF_WINNER           @"BEST_OF_WINNER"
#define POOL_WINNER              @"POOL_WINNER"
#define TABLE_TOSS               @"TABLE_TOSS"
#define SPLIT_STATUS             @"SPLIT_STATUS"
#define SPLIT_RESULT             @"SPLIT_RESULT"
#define SPLIT_TAG                @"split"
#define SPLIT_REPLY              @"split_reply"
#define SPLIT_REQUEST            @"split_request"
#define REJOIN_TAG               @"rejoin"
#define REJOIN_EVENT             @"rejoin_event"
#define REJOIN_REQUEST           @"rejoin_request"
#define TURN_EXTRA_TIME          @"extratime"
#define TURN_EXTRA_TIME_ANOTHER  @"TURN_EXTRATIME"
#define GAME_DESCHEDULE          @"GAME_DESCHEDULE"
#define SPLIT_FALSE              @"SPLIT_FALSE"
#define PRE_GAME_RESULT          @"PRE_GAME_RESULT"
#define QUIT_TABLE_REPLY         @"quit_table_reply"
#define SEND_SLOTS               @"SEND_SLOTS"
#define SEARCH_JOIN_TABLE        @"search_join_table"
#define REBUYIN                  @"rebuyin"
#define REBUYIN_REPLY            @"rebuyin_reply"
#define REBUYIN_EVENT            @"rebuyin_event"
#define OTHER_LOGIN              @"OTHER_LOGIN"
#define PLAYER_JOIN_EVENT        @"PLAYER_JOIN"
#define PLAYER_COUNT             @"player_count"
#define TABLE_LENGTH             @"table_length"
#define STAND_UP                 @"standup"
#define AUTOPLAY_COUNT           @"autoplaycount"
#define MELD_SUCCESS_CURRENT_PLAYER     @"meld_sucess"
#define SEARCH_JOIN_GET_TABLE_DETAILS   @"search_join_get_table_details"
#define STATE_BLOCKED_REPLY         @"state_blocked_reply"

#define LIST_TABLE_UPDATE_AFTER_INVALID     @"list_table_update_after_invalid"
#define LIST_TABLE_UPDATE_AFTER_RECONNECT   @"list_table_update_after_reconnect"
#define TOURNAMENT_LIST_TABLE_UPDATE @"tournament_list_table_update"
#define TOURNAMENT_DETAIL_UPDATE @"tournament_detail_update"
#define TOURNAMENT_PRIZE_LIST @"tournament_prize_list"

#define FUNCHIPS_RELOAD @"update_fun_chips"

#define SEARCH_JOIN_TABLE_PLAY_START_NOTIFICATION @"seach_join_table_play_start"
#define REMOVE_STORE_CREDENTIAL_LOGIN_ACTIVITY    @"loggin_in"

#define PLEASE_CHECK_YOUR_INTERNET_CONNECTION       @"Please check your internet connection"
#define NO_INTERNET                                 @"No Internet"
#define MELD_FAIL                                   @"meld_fail"
#define TURN_EXTRATIME_RECONNECT                    @"TURN_EXTRATIME_RECONNECT"
#define MELD_EXTRATIME_RECONNECT                    @"MELD_EXTRATIME_RECONNECT"
#define SHOW_EXTRATIME_RECONNECT                    @"SHOW_EXTRATIME_RECONNECT"
#define CHECKMELD_REPLY                             @"check_meld"
#define CHECKMELD_REPLY_NOTIFICATION                @"check_meld_reply"
#define MELD_REPLY                                  @"meld"
#define MELD_REPLY_NOTIFICATION                     @"meld_reply"
#define MELD_REQUEST                                @"meld"
#define MELD_REQUEST_NOTIFICATION_FOR_OTHER_PLAYER  @"meld_request"

#define AUTO_PLAY_STATUS       @"autoplaystatus"
#define AUTO_PLAY_STATUS_REPLY @"autoplaystatus_reply"
#define AUTO_PLAY_STATUS_EVENT @"autoplaystatus_event"
#define GET_TABLE_EXTRA        @"get_table_extra"
#define FAVOURITE_REPLY        @"favorite_game"

#define IAM_BACK_VIEW_NOTIFICATION    @"Iam_Back_notify"
#define IAM_BACK_VIEW_AUTOPLAY_COUNT  @"Iam_Back_Autoplay_Count"


#pragma mark Error notification
#define ERROR_NOTIFICATION @"Error_Notification"
#define SEARCH_JOIN_ERROR_NOTIFICATION @"SearcjJoinError"


#pragma mark Socket Error
#define ON_SOCKET_ERROR             @"OnSocketError"
#define ON_SOCKET_CONNECTION        @"OnSocketConnection"
#define RECONNECT                   @"Reconnect"
#define SOCKET_RECONNECT_MESSAGE    @"Socket Disconnected Please Reconnect"
#define PLAYER_ELIMINATED           @"Player Disconnected to the server"
#define CANCEL                      @"Cancel"
#define PLAYER_DISCONNECTED         @"Player_Disconnected"
#define PLAYER_RECONNECTED          @"Player_Reconnected"
#define CONNECTION_ESTABLISHED      @"connectionestablished"
#define CONNECTION_LOST             @"connectionlost"


#pragma mark I am back alert
#define I_AM_BACK_HEADER    @"I'm Back"
#define I_AM_BACK_MESSAGE   @"Please click on the \"I Am Back\" button to continue with your game"
#define I_AM_BACK           @"I Am Back"

#define OOPS_WARNING_TEXT_1 @"Oops. You have lost your Internet connection. Your game has been set to Auto Play. Please wait while we attempt to reconnect"
#define OOPS_WARNING_TEXT_2 @"Oops! You have been disconnected. Trying to reconnect"

#define OTHER_LOGIN_TEXT @"Oops! \n You have been logged out as you have logged in some other system/browser"
#define CANNOT_MELD_TEXT @"All boxes are full, please click on \n the appropriate box to meld the cards."

#pragma mark face and suit keys
#define BOX_START_STRING    @"<box> "
#define CARD_START_STRING   @"<card "
#define CARD_END_STRING     @"/> "
#define FACE_STRING         @"face=\"%@\" "
#define SUIT_STRING         @"suit=\"%@\" "
#define BOX_END_STRING      @" </box> "
#define FACE_KEY            @"TAJ_face"
#define SUIT_KEY            @"TAJ_suit"
#define SLOT_KEY            @"TAJ_slot"
#define SLOT_STRING         @"slot=\"%@\""
#define TABLE_START_STRING  @"<table table_id=\"%@\"> "
#define TABLE_END_STRING    @" </table>"
#define CARDS_KEY           @"TAJ_deckcard"

#define MOVETOHOMEPAGE                      @"MoveToHomePageController"
#define MOVETOLOBBY                         @"GameQuitForPlayerWhenNotExistsInTable"
#define MOVETOGAMESCREEN                    @"MoveToGameScreenController"
#define SHOW_ALERT                          @"showProgessAlertViewController"
#define SHOW_OTHER_LOGIN_ALERT              @"showOtherLoginAlertViewControllerInGamePlay"
#define DEVICE_CHANED                       @"DeviceChangedByUser"
#define SHOW_ENGINE_STATUS_TRUE_ALERT       @"EngineIsUnderMaintenance"
#define SHOW_ENGINE_STATUS_FALSE_ALERT      @"EngineIsNotUnderMaintenance"
#define SHOW_MINIMUM_PLAYERS_MESSAGE        @"GameDeschedule"
#define SHOW_IAMBACKBUTTON_ALERT            @"Iam_Back"
#define SHOW_IAMBACKARRAY_ALERT             @"Iam_Back_Array_Count"
#define CHATMENU_TABLE_CELL_SELECTED_ALERT  @"chatMenuTableCellSelected"
#define SHOW_IAMBACKARRAY_FOR_DISCARD_ALERT @"Iam_Back_Array_Discard_Count"
#define QUIT_UNUSED_TABLE                   @"quitUnusedTableForRefresh"
#define DEFAULT_TEXT                        @"defaultText"
#define SHOW_MESSAGE_LIST                   @"showMessageList_Alert"
#define SHOW_CHAT_MESSAGE_FOR_EXTENT_ALERT  @"ShowChatMessage_ForExtent"
#define SHOW_PLAYERS                          @"showPlayers"//ratheesh

#pragma mark MAIN_SCREEN_VIEW

#define LIVE_TAJ_TABLE_FEED       @"%@ Tables"
#define LIVE_TAJ_PLAYERS_FEED     @"%@ Players"
#define TOTAL_TABLE_FEED_KEY      @"TAJ_totaltables"
#define TOTAL_PLAYER_FEED_KEY     @"TAJ_totalplayers"

#define SIZE_OF_CAROUSEL_ITEM_IPAD     232.0f
#define SIZE_OF_CAROUSEL_ITEM_H_IPAD   290.0f
//#define SIZE_OF_CAROUSEL_ITEM_IPHONE   90.0f
//#define SIZE_OF_CAROUSEL_ITEM_IPHONE   96.0f
//#define SIZE_OF_CAROUSEL_ITEM_H_IPHONE 116.0f
#define SIZE_OF_CAROUSEL_ITEM_IPHONE   116.0f
#define SIZE_OF_CAROUSEL_ITEM_H_IPHONE 136.0f
#define STRIKES_TITLE                  @"STRIKES \n RUMMY"
#define POOLS_TITLE                    @"POOLS \n RUMMY"
#define DEALS_TITLE                    @"DEALS \n RUMMY"
#define TOURNAMENTS_TITLE              @"TOURNEYS \n RUMMY"
//#define STRIKES_IMAGE_IPAD             @"strikes_rummy.png"
//#define POOLS_IMAGE_IPAD               @"pools_rummy.png"
//#define DEALS_IMAGE_IPAD               @"deals_rummy.png"
//#define STRIKES_IMAGE_IPHONE           @"strikes_rummy.png"
//#define POOLS_IMAGE_IPHONE             @"pools_rummy.png"
//#define DEALS_IMAGE_IPHONE             @"deals_rummy.png"
#define STRIKES_IMAGE           @"TR_strikes_red"
#define POOLS_IMAGE             @"TR_polls_white"
#define DEALS_IMAGE             @"TR_deals_red"
#define TOURNAMENTS_IMAGE       @"TR_torney_red"
#define POINTS_TITLE            @"POINTS RUMMY"

#pragma Preference Keys
#define USENAME_KEY         @"username"
#define PASSWORD_KEY        @"password"
#define UNIQUE_SESSION_KEY        @"session_id"
#define TOKEN_KEY        @"token"
#define USEREMAIL_KEY       @"email"
#define USERMOBILE_KEY       @"mobile"
#define USERID       @"userid"
#define SURL       @"surl"
#define FIRSTTABLEID       @"first_table_id"
#define SECONDTABLEID      @"second_table_id"

#define PENDING_KEY         @"PENDING"
#define RELEASE_KEY         @"RELEASED"
#define EXPIRERD_KEY        @"EXPIRED"
#define TABLE_ONE_BOOL_KEY  @"TAJ_UITABLE_ID"
#define TABLE_TWO_BOOL_KEY  @"TABLETWOBOOL"
//bonus chunk images
#define BONUS_CHUNK_GRAY_IMAGE   @"bonus_chunk_gray_straight.png"
#define BONUS_CHUNK_GREEN_IMAGE  @"bonus_chunk_green_straight.png"
#define BONUS_CHUNK_ORANGE_IMAGE @"bonus_chunk_yellow_straight.png"
//bonus chunk colors
#define BONUS_CHUNK_GRAY    @"gray"
#define BONUS_CHUNK_GREEN   @"green"
#define BONUS_CHUNK_ORANGE  @"orange"

//main screen constants
#define TOTAL_GAMES_PLAYED              @"totalgamesplayed"
#define TODAY_GAMES_PLAYED              @"todaygamesplayed"
#define CURRENT_WAGER_LISTITEM          @"currentwageramount.listitem"
#define STATUS_LIST_ITEM                @"status.listitem"
#define REQUIRED_WAGER_AMOUNT_LIST_ITEM @"requiredwageramount.listitem"

#pragma mark LOBBY_SCREEN images
#define HOME_BUTTON_PRESSED_IMAGE       @"tabbar_home_on"
#define HOME_BUTTON_NORMAL_IMAGE        @"tabbar_home"
#define LOBBY_BUTTON_PRESSED_IMAGE      @"tabbar_lobby_on"
#define LOBBY_BUTTON_NORMAL_IMAGE       @"tabbar_lobby"
#define CASHIER_BUTTON_PRESSED_IMAGE    @"tabbar_cashier_on"
#define CASHIER_BUTTON_NORMAL_IMAGE     @"tabbar_cashier"
#define TABELS_BUTTON_PRESSED_IMAGE     @"TR_tabbar_table_on"
#define TABELS_BUTTON_NORMAL_IMAGE      @"tables_alert_green_button.png"
#define MORE_BUTTON_PRESSED_IMAGE       @"tabbar_more_on"
#define MORE_BUTTON_NORMAL_IMAGE        @"tabbar_more"

#define HOME_BUTTON_PRESSED_IMAGE_IPAD      @"tabbar_home_on"
#define HOME_BUTTON_NORMAL_IMAGE_IPAD       @"tabbar_home"
#define LOBBY_BUTTON_PRESSED_IMAGE_IPAD     @"tabbar_lobby_on"
#define LOBBY_BUTTON_NORMAL_IMAGE_IPAD      @"tabbar_lobby"
#define CASHIER_BUTTON_PRESSED_IMAGE_IPAD   @"tabbar_cashier_on"
#define CASHIER_BUTTON_NORMAL_IMAGE_IPAD    @"tabbar_cashier"
#define TABELS_BUTTON_PRESSED_IMAGE_IPAD    @"TR_tabbar_table_on"
#define TABELS_BUTTON_NORMAL_IMAGE_IPAD     @"tables_alert_green_button.png"
#define MORE_BUTTON_PRESSED_IMAGE_IPAD      @"tabbar_more_on"
#define MORE_BUTTON_NORMAL_IMAGE_IPAD       @"tabbar_more"
#define SUPPORT_BUTTON_PRESSED_IMAGE_IPAD   @"tabbar_support_on"
#define SUPPORT_BUTTON_NORMAL_IMAGE_IPAD    @"tabbar_support"

#pragma mark GAME_SCREEN
#define SCOREBOARD_BUTTON_PRESSED_IMAGE          @"scoreboard_yellow-568h.png"
#define SCOREBOARD_BUTTON_PRESSED_IMAGE_IPHONE_5 @"iphn5_buttonbg03_yellow@2x~iphone.png"

#define SCOREBOARD_BUTTON_NORMAL_IMAGE           @"buttonbg03-568h.png"
#define SCOREBOARD_BUTTON_NORMAL_IMAGE_IPHONE_5  @"iphn5_buttonbg03.png"

#define SCOREBOARD_BUTTON_PRESSED_IMAGE_IPAD     @"scoreboard_yellowbg.png"
#define SCOREBOARD_BUTTON_NORMAL_IMAGE_IPAD     @"scoreboard_blackbutton.png"

//player handcards
//IPHONE
//FIRST AND FOURTH PLAYER
//#define FOURTH_PLAYER_ADDING_1_IPHONE   @"top_adding0001-568h@2x~iphone.png"

#define FOURTH_PLAYER_ADDING_1_IPHONE   @"top_adding0001-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_2_IPHONE   @"top_adding0002-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_3_IPHONE   @"top_adding0003-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_4_IPHONE   @"top_adding0004-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_5_IPHONE   @"top_adding0005-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_6_IPHONE   @"top_adding0006-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_7_IPHONE   @"top_adding0007-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_8_IPHONE   @"top_adding0008-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_9_IPHONE   @"top_adding0009-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_10_IPHONE  @"top_adding0010-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_11_IPHONE  @"top_adding0011-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_12_IPHONE  @"top_adding0012-568h@2x~iphone.png"
#define FOURTH_PLAYER_ADDING_13_IPHONE  @"top_adding0013-568h@2x~iphone.png"

//THIS DEVICE PLAYER
#define FOURTH_PLAYER_DELETING_1_IPHONE @"device_card_closing0001-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_2_IPHONE @"device_card_closing0002-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_3_IPHONE @"device_card_closing0003-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_4_IPHONE @"device_card_closing0004-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_5_IPHONE @"device_card_closing0005-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_6_IPHONE @"device_card_closing0006-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_7_IPHONE @"device_card_closing0007-568h@2x~iphone.png"

//FOR SPECTATOR
#define FOURTH_PLAYER_DELETING_SPEC_1_IPHONE @"top_deleting0001-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_SPEC_2_IPHONE @"top_deleting0002-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_SPEC_3_IPHONE @"top_deleting0003-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_SPEC_4_IPHONE @"top_deleting0004-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_SPEC_5_IPHONE @"top_deleting0005-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_SPEC_6_IPHONE @"top_deleting0006-568h@2x~iphone.png"
#define FOURTH_PLAYER_DELETING_SPEC_7_IPHONE @"top_deleting0007-568h@2x~iphone.png"

//SECONDPLAYER
#define SECOND_PLAYER_ADDING_1_IPHONE   @"btm_right_adding0001-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_2_IPHONE   @"btm_right_adding0002-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_3_IPHONE   @"btm_right_adding0003-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_4_IPHONE   @"btm_right_adding0004-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_5_IPHONE   @"btm_right_adding0005-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_6_IPHONE   @"btm_right_adding0006-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_7_IPHONE   @"btm_right_adding0007-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_8_IPHONE   @"btm_right_adding0008-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_9_IPHONE   @"btm_right_adding0009-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_10_IPHONE  @"btm_right_adding0010-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_11_IPHONE  @"btm_right_adding0011-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_12_IPHONE  @"btm_right_adding0012-568h@2x~iphone.png"
#define SECOND_PLAYER_ADDING_13_IPHONE  @"btm_right_adding0013-568h@2x~iphone.png"

#define SECOND_PLAYER_DELETING_1_IPHONE @"btm_right_deleting0001-568h@2x~iphone.png"
#define SECOND_PLAYER_DELETING_2_IPHONE @"btm_right_deleting0002-568h@2x~iphone.png"
#define SECOND_PLAYER_DELETING_3_IPHONE @"btm_right_deleting0003-568h@2x~iphone.png"
#define SECOND_PLAYER_DELETING_4_IPHONE @"btm_right_deleting0004-568h@2x~iphone.png"
#define SECOND_PLAYER_DELETING_5_IPHONE @"btm_right_deleting0005-568h@2x~iphone.png"
#define SECOND_PLAYER_DELETING_6_IPHONE @"btm_right_deleting0006-568h@2x~iphone.png"
#define SECOND_PLAYER_DELETING_7_IPHONE @"btm_right_deleting0007-568h@2x~iphone.png"
//THIRD PLAYER
#define THIRD_PLAYER_ADDING_1_IPHONE  @"top_right_adding0001-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_2_IPHONE  @"top_right_adding0002-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_3_IPHONE  @"top_right_adding0003-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_4_IPHONE  @"top_right_adding0004-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_5_IPHONE  @"top_right_adding0005-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_6_IPHONE  @"top_right_adding0006-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_7_IPHONE  @"top_right_adding0007-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_8_IPHONE  @"top_right_adding0008-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_9_IPHONE  @"top_right_adding0009-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_10_IPHONE @"top_right_adding0010-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_11_IPHONE @"top_right_adding0011-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_12_IPHONE @"top_right_adding0012-568h@2x~iphone.png"
#define THIRD_PLAYER_ADDING_13_IPHONE @"top_right_adding0013-568h@2x~iphone.png"

#define THIRD_PLAYER_DELETING_1_IPHONE @"top_right_deleting0001-568h@2x~iphone.png"
#define THIRD_PLAYER_DELETING_2_IPHONE @"top_right_deleting0002-568h@2x~iphone.png"
#define THIRD_PLAYER_DELETING_3_IPHONE @"top_right_deleting0003-568h@2x~iphone.png"
#define THIRD_PLAYER_DELETING_4_IPHONE @"top_right_deleting0004-568h@2x~iphone.png"
#define THIRD_PLAYER_DELETING_5_IPHONE @"top_right_deleting0005-568h@2x~iphone.png"
#define THIRD_PLAYER_DELETING_6_IPHONE @"top_right_deleting0006-568h@2x~iphone.png"
#define THIRD_PLAYER_DELETING_7_IPHONE @"top_right_deleting0007-568h@2x~iphone.png"

//FIFTH PLAYER
#define FIFTH_PLAYER_ADDING_1_IPHONE  @"top_left_adding0001-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_2_IPHONE  @"top_left_adding0002-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_3_IPHONE  @"top_left_adding0003-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_4_IPHONE  @"top_left_adding0004-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_5_IPHONE  @"top_left_adding0005-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_6_IPHONE  @"top_left_adding0006-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_7_IPHONE  @"top_left_adding0007-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_8_IPHONE  @"top_left_adding0008-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_9_IPHONE  @"top_left_adding0009-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_10_IPHONE @"top_left_adding0010-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_11_IPHONE @"top_left_adding0011-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_12_IPHONE @"top_left_adding0012-568h@2x~iphone.png"
#define FIFTH_PLAYER_ADDING_13_IPHONE @"top_left_adding0013-568h@2x~iphone.png"

#define FIFTH_PLAYER_DELETING_1_IPHONE @"top_left_deleting0001-568h@2x~iphone.png"
#define FIFTH_PLAYER_DELETING_2_IPHONE @"top_left_deleting0002-568h@2x~iphone.png"
#define FIFTH_PLAYER_DELETING_3_IPHONE @"top_left_deleting0003-568h@2x~iphone.png"
#define FIFTH_PLAYER_DELETING_4_IPHONE @"top_left_deleting0004-568h@2x~iphone.png"
#define FIFTH_PLAYER_DELETING_5_IPHONE @"top_left_deleting0005-568h@2x~iphone.png"
#define FIFTH_PLAYER_DELETING_6_IPHONE @"top_left_deleting0006-568h@2x~iphone.png"
#define FIFTH_PLAYER_DELETING_7_IPHONE @"top_left_deleting0007-568h@2x~iphone.png"

//SIXTH PLAYER
#define SIXTH_PLAYER_ADDING_1_IPHONE  @"btm_left_adding0001-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_2_IPHONE  @"btm_left_adding0002-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_3_IPHONE  @"btm_left_adding0003-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_4_IPHONE  @"btm_left_adding0004-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_5_IPHONE  @"btm_left_adding0005-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_6_IPHONE  @"btm_left_adding0006-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_7_IPHONE  @"btm_left_adding0007-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_8_IPHONE  @"btm_left_adding0008-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_9_IPHONE  @"btm_left_adding0009-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_10_IPHONE @"btm_left_adding0010-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_11_IPHONE @"btm_left_adding0011-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_12_IPHONE @"btm_left_adding0012-568h@2x~iphone.png"
#define SIXTH_PLAYER_ADDING_13_IPHONE @"btm_left_adding0013-568h@2x~iphone.png"

#define SIXTH_PLAYER_DELETING_1_IPHONE @"btm_left_deleting0001-568h@2x~iphone.png"
#define SIXTH_PLAYER_DELETING_2_IPHONE @"btm_left_deleting0002-568h@2x~iphone.png"
#define SIXTH_PLAYER_DELETING_3_IPHONE @"btm_left_deleting0003-568h@2x~iphone.png"
#define SIXTH_PLAYER_DELETING_4_IPHONE @"btm_left_deleting0004-568h@2x~iphone.png"
#define SIXTH_PLAYER_DELETING_5_IPHONE @"btm_left_deleting0005-568h@2x~iphone.png"
#define SIXTH_PLAYER_DELETING_6_IPHONE @"btm_left_deleting0006-568h@2x~iphone.png"
#define SIXTH_PLAYER_DELETING_7_IPHONE @"btm_left_deleting0007-568h@2x~iphone.png"


//IPAD
//FIRST AND FOURTH PLAYER
#define FOURTH_PLAYER_ADDING_1_IPAD  @"top_adding0001.png"
#define FOURTH_PLAYER_ADDING_2_IPAD  @"top_adding0002.png"
#define FOURTH_PLAYER_ADDING_3_IPAD  @"top_adding0003.png"
#define FOURTH_PLAYER_ADDING_4_IPAD  @"top_adding0004.png"
#define FOURTH_PLAYER_ADDING_5_IPAD  @"top_adding0005.png"
#define FOURTH_PLAYER_ADDING_6_IPAD  @"top_adding0006.png"
#define FOURTH_PLAYER_ADDING_7_IPAD  @"top_adding0007.png"
#define FOURTH_PLAYER_ADDING_8_IPAD  @"top_adding0008.png"
#define FOURTH_PLAYER_ADDING_9_IPAD  @"top_adding0009.png"
#define FOURTH_PLAYER_ADDING_10_IPAD @"top_adding0010.png"
#define FOURTH_PLAYER_ADDING_11_IPAD @"top_adding0011.png"
#define FOURTH_PLAYER_ADDING_12_IPAD @"top_adding0012.png"
#define FOURTH_PLAYER_ADDING_13_IPAD @"top_adding0013.png"

//THIS DEVICE PLAYER
#define FOURTH_PLAYER_DELETING_1_IPAD @"device_card_closing0001.png"
#define FOURTH_PLAYER_DELETING_2_IPAD @"device_card_closing0002.png"
#define FOURTH_PLAYER_DELETING_3_IPAD @"device_card_closing0003.png"
#define FOURTH_PLAYER_DELETING_4_IPAD @"device_card_closing0004.png"
#define FOURTH_PLAYER_DELETING_5_IPAD @"device_card_closing0005.png"
#define FOURTH_PLAYER_DELETING_6_IPAD @"device_card_closing0006.png"
#define FOURTH_PLAYER_DELETING_7_IPAD @"device_card_closing0007.png"

//FOR SPECTATOR
#define FOURTH_PLAYER_DELETING_SPEC_1_IPAD @"top_deleting0001.png"
#define FOURTH_PLAYER_DELETING_SPEC_2_IPAD @"top_deleting0002.png"
#define FOURTH_PLAYER_DELETING_SPEC_3_IPAD @"top_deleting0003.png"
#define FOURTH_PLAYER_DELETING_SPEC_4_IPAD @"top_deleting0004.png"
#define FOURTH_PLAYER_DELETING_SPEC_5_IPAD @"top_deleting0005.png"
#define FOURTH_PLAYER_DELETING_SPEC_6_IPAD @"top_deleting0006.png"
#define FOURTH_PLAYER_DELETING_SPEC_7_IPAD @"top_deleting0007.png"

//SECONDPLAYER
#define SECOND_PLAYER_ADDING_1_IPAD @"btm_right_adding0001.png"
#define SECOND_PLAYER_ADDING_2_IPAD @"btm_right_adding0002.png"
#define SECOND_PLAYER_ADDING_3_IPAD @"btm_right_adding0003.png"
#define SECOND_PLAYER_ADDING_4_IPAD @"btm_right_adding0004.png"
#define SECOND_PLAYER_ADDING_5_IPAD @"btm_right_adding0005.png"
#define SECOND_PLAYER_ADDING_6_IPAD @"btm_right_adding0006.png"
#define SECOND_PLAYER_ADDING_7_IPAD @"btm_right_adding0007.png"
#define SECOND_PLAYER_ADDING_8_IPAD @"btm_right_adding0008.png"
#define SECOND_PLAYER_ADDING_9_IPAD @"btm_right_adding0009.png"
#define SECOND_PLAYER_ADDING_10_IPAD @"btm_right_adding0010.png"
#define SECOND_PLAYER_ADDING_11_IPAD @"btm_right_adding0011.png"
#define SECOND_PLAYER_ADDING_12_IPAD @"btm_right_adding0012.png"
#define SECOND_PLAYER_ADDING_13_IPAD @"btm_right_adding0013.png"

#define SECOND_PLAYER_DELETING_1_IPAD @"btm_right_deleting0001.png"
#define SECOND_PLAYER_DELETING_2_IPAD @"btm_right_deleting0002.png"
#define SECOND_PLAYER_DELETING_3_IPAD @"btm_right_deleting0003.png"
#define SECOND_PLAYER_DELETING_4_IPAD @"btm_right_deleting0004.png"
#define SECOND_PLAYER_DELETING_5_IPAD @"btm_right_deleting0005.png"
#define SECOND_PLAYER_DELETING_6_IPAD @"btm_right_deleting0006.png"
#define SECOND_PLAYER_DELETING_7_IPAD @"btm_right_deleting0007.png"
//THIRD PLAYER
#define THIRD_PLAYER_ADDING_1_IPAD @"top_right_adding0001.png"
#define THIRD_PLAYER_ADDING_2_IPAD @"top_right_adding0002.png"
#define THIRD_PLAYER_ADDING_3_IPAD @"top_right_adding0003.png"
#define THIRD_PLAYER_ADDING_4_IPAD @"top_right_adding0004.png"
#define THIRD_PLAYER_ADDING_5_IPAD @"top_right_adding0005.png"
#define THIRD_PLAYER_ADDING_6_IPAD @"top_right_adding0006.png"
#define THIRD_PLAYER_ADDING_7_IPAD @"top_right_adding0007.png"
#define THIRD_PLAYER_ADDING_8_IPAD @"top_right_adding0008.png"
#define THIRD_PLAYER_ADDING_9_IPAD @"top_right_adding0009.png"
#define THIRD_PLAYER_ADDING_10_IPAD @"top_right_adding0010.png"
#define THIRD_PLAYER_ADDING_11_IPAD @"top_right_adding0011.png"
#define THIRD_PLAYER_ADDING_12_IPAD @"top_right_adding0012.png"
#define THIRD_PLAYER_ADDING_13_IPAD @"top_right_adding0013.png"

#define THIRD_PLAYER_DELETING_1_IPAD @"top_right_deleting0001.png"
#define THIRD_PLAYER_DELETING_2_IPAD @"top_right_deleting0002.png"
#define THIRD_PLAYER_DELETING_3_IPAD @"top_right_deleting0003.png"
#define THIRD_PLAYER_DELETING_4_IPAD @"top_right_deleting0004.png"
#define THIRD_PLAYER_DELETING_5_IPAD @"top_right_deleting0005.png"
#define THIRD_PLAYER_DELETING_6_IPAD @"top_right_deleting0006.png"
#define THIRD_PLAYER_DELETING_7_IPAD @"top_right_deleting0007.png"

//FIFTH PLAYER
#define FIFTH_PLAYER_ADDING_1_IPAD @"top_left_adding0001.png"
#define FIFTH_PLAYER_ADDING_2_IPAD @"top_left_adding0002.png"
#define FIFTH_PLAYER_ADDING_3_IPAD @"top_left_adding0003.png"
#define FIFTH_PLAYER_ADDING_4_IPAD @"top_left_adding0004.png"
#define FIFTH_PLAYER_ADDING_5_IPAD @"top_left_adding0005.png"
#define FIFTH_PLAYER_ADDING_6_IPAD @"top_left_adding0006.png"
#define FIFTH_PLAYER_ADDING_7_IPAD @"top_left_adding0007.png"
#define FIFTH_PLAYER_ADDING_8_IPAD @"top_left_adding0008.png"
#define FIFTH_PLAYER_ADDING_9_IPAD @"top_left_adding0009.png"
#define FIFTH_PLAYER_ADDING_10_IPAD @"top_left_adding0010.png"
#define FIFTH_PLAYER_ADDING_11_IPAD @"top_left_adding0011.png"
#define FIFTH_PLAYER_ADDING_12_IPAD @"top_left_adding0012.png"
#define FIFTH_PLAYER_ADDING_13_IPAD @"top_left_adding0013.png"

#define FIFTH_PLAYER_DELETING_1_IPAD @"top_left_deleting0001.png"
#define FIFTH_PLAYER_DELETING_2_IPAD @"top_left_deleting0002.png"
#define FIFTH_PLAYER_DELETING_3_IPAD @"top_left_deleting0003.png"
#define FIFTH_PLAYER_DELETING_4_IPAD @"top_left_deleting0004.png"
#define FIFTH_PLAYER_DELETING_5_IPAD @"top_left_deleting0005.png"
#define FIFTH_PLAYER_DELETING_6_IPAD @"top_left_deleting0006.png"
#define FIFTH_PLAYER_DELETING_7_IPAD @"top_left_deleting0007.png"

//SIXTH PLAYER
#define SIXTH_PLAYER_ADDING_1_IPAD @"btm_left_adding0001.png"
#define SIXTH_PLAYER_ADDING_2_IPAD @"btm_left_adding0002.png"
#define SIXTH_PLAYER_ADDING_3_IPAD @"btm_left_adding0003.png"
#define SIXTH_PLAYER_ADDING_4_IPAD @"btm_left_adding0004.png"
#define SIXTH_PLAYER_ADDING_5_IPAD @"btm_left_adding0005.png"
#define SIXTH_PLAYER_ADDING_6_IPAD @"btm_left_adding0006.png"
#define SIXTH_PLAYER_ADDING_7_IPAD @"btm_left_adding0007.png"
#define SIXTH_PLAYER_ADDING_8_IPAD @"btm_left_adding0008.png"
#define SIXTH_PLAYER_ADDING_9_IPAD @"btm_left_adding0009.png"
#define SIXTH_PLAYER_ADDING_10_IPAD @"btm_left_adding0010.png"
#define SIXTH_PLAYER_ADDING_11_IPAD @"btm_left_adding0011.png"
#define SIXTH_PLAYER_ADDING_12_IPAD @"btm_left_adding0012.png"
#define SIXTH_PLAYER_ADDING_13_IPAD @"btm_left_adding0013.png"

#define SIXTH_PLAYER_DELETING_1_IPAD @"btm_left_deleting0001.png"
#define SIXTH_PLAYER_DELETING_2_IPAD @"btm_left_deleting0002.png"
#define SIXTH_PLAYER_DELETING_3_IPAD @"btm_left_deleting0003.png"
#define SIXTH_PLAYER_DELETING_4_IPAD @"btm_left_deleting0004.png"
#define SIXTH_PLAYER_DELETING_5_IPAD @"btm_left_deleting0005.png"
#define SIXTH_PLAYER_DELETING_6_IPAD @"btm_left_deleting0006.png"
#define SIXTH_PLAYER_DELETING_7_IPAD @"btm_left_deleting0007.png"
#pragma mark ERROR_NOTIFICATIONS
#define OK @"200"
#define INTERNAL_ERROR @"500"
#define NO_SUCH_PLAYER ="401"
#define ERR_DELETEING_SCHEDULE @"402"
#define NO_SUCH_SCHEDULE @"403"
#define NO_SUCH_TABLE @"404"
#define NO_SUCH_CHATROOM @"405"
#define NOT_ACCEPTABLE @"406"
#define NO_SUCH_EDITINGVALUES @"407"
#define CANT_DO_THIS_NO_MATCHING_CONDITION @"408"
#define ALREADY_REQUESTED_EXTRATIME ="409"
#define NO_TABLE_WITH_SAME_CONSTRAINS @"410"
#define NO_SLOT_FOR_THIS_TABLE @"411"
#define DATABASE_ERROR @"460"
#define WRONG_USERNAME @"461"
#define WRONG_PASSWORD @"462"
#define CONNECTION_WAS_NOT_ESTABLISHED_TO_DBSLAYER @"463"
#define EMPTY_SET_FROM_DATABASE @"464"
#define ERROR_IN_GAP @"465"
#define STATE_BLOCK @"483"
#define LOW_BALANCE @"501"
#define TABLE_FULL @"502"
#define MORE_THEN_MAXBUYIN @"503"
#define PLAYER_ALREADY_INPLAY @"504"
#define PLAYER_ALREADY_INVIEW @"505"
#define NOT_PROPER_TABLE_TYPE @"506"
#define NO_MIDDLE_JOIN @"510"
#define NO_SUFF_AMOUNT @"507"
#define MIDDLE_JOIN_ALREADY_REQUESTED @"508"
#define IMPROPER_CONTIDITION_FOR_REQUEST @"509"
#define FALSE_SPLIT_CONDITION @"701"
#define PLAYER_CANCEL_SPLIT @"702"
#define ALREADY_SPLIT_REQUESTED @"703"
#define SEAT_ALREADY_TAKEN @"801"
#define ERROR_IN_PLAYERADD @"802"
#define SEAT_RESERVED_TO_TAKE @"803"
#define CANT_DO_RELOAD @"901"
#define PLAYER_CANNOTJOIN_INTO_THIS_TABLE @"469"
#define ATTRIB_MISSING = @"470"
#define ERR_ADDING_TOURNEY = @"300"
#define ERR_EDITING_TOURNEY = @"301"
#define ERR_DELETE_TOURNEY = @"302"
#define NO_TOURNAMENT_SCHEDULED = @"303"


#pragma  mark ENUM types

typedef enum EOpenTableType {
    eOpenTableNone,
    eOpenTableFirst,
    eOpenTableSecond
}EOpenTableType;

typedef enum EPopupButtonType {
    eYes_No_Type,
    eOK_Cancel_Type,
    eOK_Type,
    eDEPOSIT_Type,
    eNone_Type
}EPopupButtonType;

typedef enum EPopupType {
    eRejoin,
    eDrop,
    eShow,
    eSearchJoin,
    eRebuyin,
    eSplitRequest,
    eLeaveTable,
    eSplitReject,
    eLastHand
}EPopupType;

typedef enum EErrorTypes {
    eOK = 200,
    eINTERNAL_ERROR = 500,
    eENGINE_UNDER_MAINTENANCE = 100,
    eNO_SUCH_PLAYER =401,
    eERR_DELETEING_SCHEDULE = 402,
    eNO_SUCH_SCHEDULE = 403,
    eNO_SUCH_TABLE = 404,
    eNO_SUCH_CHATROOM = 405,
    eNOT_ACCEPTABLE = 406,
    eNO_SUCH_EDITINGVALUES = 407,
    eCANT_DO_THIS_NO_MATCHING_CONDITION = 408,
    eALREADY_REQUESTED_EXTRATIME =409,
    eNO_TABLE_WITH_SAME_CONSTRAINS = 410,
    eNO_SLOT_FOR_THIS_TABLE = 411,
    eDATABASE_ERROR = 460,
    eWRONG_USERNAME = 461,
    eWRONG_PASSWORD = 462,
    eCONNECTION_WAS_NOT_ESTABLISHED_TO_DBSLAYER = 463,
    eEMPTY_SET_FROM_DATABASE = 464,
    eERROR_IN_GAP = 465,
    eERROR_NOT_FUNDED_PLAYER = 481,
    eLOW_BALANCE = 501,
    eSTATE_BLOCKED = 483,
    eTABLE_FULL = 502,
    eMORE_THEN_MAXBUYIN = 503,
    ePLAYER_ALREADY_INPLAY = 504,
    ePLAYER_ALREADY_INVIEW = 505,
    eNOT_PROPER_TABLE_TYPE = 506,
    eNO_MIDDLE_JOIN = 510,
    eMIDDLE_JOIN_ALREADY_REQUESTED = 508,
    eIMPROPER_CONTIDITION_FOR_REQUEST = 509,
    eFALSE_SPLIT_CONDITION = 701,
    ePLAYER_CANCEL_SPLIT = 702,
    eALREADY_SPLIT_REQUESTED = 703,
    eSEAT_ALREADY_TAKEN = 801,
    eERROR_IN_PLAYERADD = 802,
    eSEAT_RESERVED_TO_TAKE = 803,
    eCANT_DO_RELOAD = 901,
    ePLAYER_CANNOTJOIN_INTO_THIS_TABLE = 469,
    eATTRIB_MISSING = 470,
    eAlreadyRegistered = 7005,
    eAlreadyWaiting = 7006,
    eNotEligible = 7007,
    eNotRegistered = 7008,
    eRegistrationFull = 7010,
    eRegistrationNotOpen = 7011,
    eRegistrationClosed = 7012,
    eYouCantCancel = 7013,
    eCurrentlyNotAvailable = 7028,
    eDontHaveCashOrLoyalty = 507,
    eNeedDeposit1 = 7030,
    eNeedDeposit2 = 7031,
    eNeedDeposit3 = 7040,
    eNeedDeposit4 = 7041,
    eSufficientLoyaltyPoints = 7034,
    eVerifyEmail = 7042,
    eVerifyMobile = 7043,
    eFirstTimeFund = 7044,
    eOnlyFreePlayer = 7045,
    eVerifyMobileAndEmail = 7046,
    eYouNeedMaximumDeposit = 481,
    eVIPRegiNotThere = 7024,
    eVIPRegiFull = 7025,
    eVIPCodeInvalid = 7026,
    eNoTournament = 7004
}EErrorTypes;

static inline NSString* locateEN(int textEnum) {
    
    switch (textEnum) {
        case eOK:
            return @"OK";
            break;
        case eENGINE_UNDER_MAINTENANCE:
            return @"Engine is Under Maintenance";
            break;
        case eINTERNAL_ERROR:
            return @"Internal Error";
            break;
            
        case eNO_SUCH_PLAYER:
            return @"No such Error";
            break;
            
        case eERR_DELETEING_SCHEDULE:
            return @"Error deleting schedule";
            break;
            
        case eNO_SUCH_SCHEDULE:
            return @"No such schedule";
            break;
            
        case eNO_SUCH_TABLE:
            return @"No such table";
            break;
            
        case eNO_SUCH_CHATROOM:
            return @"No such chat room";
            break;
            
        case eNOT_ACCEPTABLE:
            return @"Not acceptable";
            break;
            
        case eNO_SUCH_EDITINGVALUES:
            return @"No such editing values";
            break;
            
        case eCANT_DO_THIS_NO_MATCHING_CONDITION:
            return @"Cant do this no matching condition";
            break;
            
        case eALREADY_REQUESTED_EXTRATIME:
            return @"Already requested extra time";
            break;
            
        case eNO_TABLE_WITH_SAME_CONSTRAINS:
            return @"No table with same constrains";
            break;
            
        case eNO_SLOT_FOR_THIS_TABLE:
            return @"NO slot for this table";
            break;
            
        case eDATABASE_ERROR:
            return @"There is a problem with the server";
            break;
            
        case eWRONG_USERNAME:
            return @"No Alert";
            break;
            
        case eWRONG_PASSWORD:
            return @"No Alert";
            break;
            
        case eCONNECTION_WAS_NOT_ESTABLISHED_TO_DBSLAYER:
            return @"Connection was not established to DB'slayer";
            break;
            
        case eEMPTY_SET_FROM_DATABASE:
            return @"Empty set from database";
            break;
            
        case eERROR_IN_GAP:
            return @"Error in gap";
            break;
            
        case eLOW_BALANCE:
            return @"No Alert";
            break;
            
        case eTABLE_FULL:
            return @"table full";
            break;
            
        case eMORE_THEN_MAXBUYIN:
            return @"Please enter maximum amount only";
            break;
            
        case ePLAYER_ALREADY_INPLAY:
            return @"Player already in play";
            break;
            
        case ePLAYER_ALREADY_INVIEW:
            return @"Player already in view";
            break;
            
        case eNOT_PROPER_TABLE_TYPE:
            return @"Not proper table type";
            break;
            
        case eNO_MIDDLE_JOIN:
            return @"eNO_MIDDLE_JOIN";
            break;
            
        case eMIDDLE_JOIN_ALREADY_REQUESTED:
            return @"eMIDDLE_JOIN_ALREADY_REQUESTED";
            break;
            
        case eIMPROPER_CONTIDITION_FOR_REQUEST:
            return @"eIMPROPER_CONTIDITION_FOR_REQUEST";
            break;
            
        case eFALSE_SPLIT_CONDITION:
            return @"eFALSE_SPLIT_CONDITION";
            break;
            
        case ePLAYER_CANCEL_SPLIT:
            return @"Player cancelled split";
            break;
            
        case eALREADY_SPLIT_REQUESTED:
            return @"Split Already requested";
            break;
            
        case eSEAT_ALREADY_TAKEN:
            return @"Seat already taken";
            break;
            
        case eERROR_IN_PLAYERADD:
            return @"eERROR_IN_PLAYERADD";
            break;
            
        case eSEAT_RESERVED_TO_TAKE:
            return @"Seat already taken";
            break;
            
        case eCANT_DO_RELOAD:
            return @"eCANT_DO_RELOAD";
            break;
            
        case ePLAYER_CANNOTJOIN_INTO_THIS_TABLE:
            return @"ePLAYER_CANNOTJOIN_INTO_THIS_TABLE";
            break;
            
        case eATTRIB_MISSING:
            return @"eATTRIB_MISSING";
            break;
        case eAlreadyRegistered:
            return @"You are already registered for this tourney";
            break;
        case eAlreadyWaiting:
            return @"You are already in waiting list for this tourney";
            break;
        case eNotEligible:
            return @"You are not eligible for this tourney";
            break;
        case eNotRegistered:
            return @"You are not registered for this tourney";
            break;
        case eRegistrationFull:
            return @"Registrations full for this tourney";
            break;
        case eRegistrationNotOpen:
            return @"Registrations are not Opened";
            break;
        case eRegistrationClosed:
            return @"Registrations closed";
            break;
        case eYouCantCancel:
            return @"You cannot cancel your registration request since the tourney is going to start";
            break;
        case eCurrentlyNotAvailable:
            return @"Our services are currently not available on Taj Rummy in your state";
            break;
        case eDontHaveCashOrLoyalty:
            return @"Not having sufficient cash chips or loyalty points to register for this tournament";
            break;
        case eNeedDeposit1:
        case eNeedDeposit2:
        case eNeedDeposit3:
        case eNeedDeposit4:
            return @"You need to be a depositing player to register for this tournament";
            break;
        case eSufficientLoyaltyPoints:
            return @"You do not have sufficient loyalty points to register for this tournament";
            break;
        case eVerifyEmail:
            return @"Verify your email to register for this tournament";
            break;
        case eVerifyMobile:
            return @"Verify your mobile number to register for this tournament";
            break;
        case eVerifyMobileAndEmail:
            return @"Verify your mobile number and email to register for this tournament";
            break;
        case eFirstTimeFund:
            return @"Only first time funded players can register for first";
            break;
        case eOnlyFreePlayer:
            return @"Only Free Players can join this tournament";
            break;
        case eYouNeedMaximumDeposit:
            return @"You need to have minimum deposit";
            break;
        case eVIPRegiNotThere:
            return @"VIP Registrations not there for this tourney";
            break;
        case eVIPRegiFull:
            return @"VIP Registrations are full for this tourney";
            break;
        case eVIPCodeInvalid:
            return @"The VIP code you have entered is invalid. Please check and try again";
            break;
        case eNoTournament:
            return @"There is no tournament created";
            break;
        case eSTATE_BLOCKED:
            return @"Our services are currently not availabe on Taj Rummy in your state. As the state regulation prohibits a player from using the services offered by us.";
            break;
            
        default:
            return @"No Alert";
            break;
    }
}

static inline NSString* locate(int textEnum) {
    
    //ccLanguageType ret = CCApplication::sharedApplication()->getCurrentLanguage();
    switch (1) {
        case 1:
            return locateEN(textEnum);
            break;
        case 2:
            return 0;//locateAR(textEnum);
            break;
        default:
            return locateEN(textEnum);
            break;
    }
    return Nil;
}

#define TRUE_VALUE @"True"
#define FALSE_VALUE @"False"
#define FALSE_VALUE_UPPER @"False"

// table Key constants
#define TAJ_BET                         @"TAJ_bet"
#define TAJ_CURRENT_PLAYER              @"TAJ_current_players"
#define TAJ_GAME_SCHEDULE               @"TAJ_game_schedule"
#define TAJ_GAME_START                  @"TAJ_game_start"
#define TAJ_MAX_BUYIN                   @"TAJ_maximumbuyin"
#define TAJ_MIN_BUYIN                   @"TAJ_minimumbuyin"

#define TAJ_MAX_PLAYER                  @"TAJ_maxplayer"
#define TAJ_MIN_BUYIN                   @"TAJ_minimumbuyin"
#define TAJ_MIN_PLAYER                  @"TAJ_minplayer"
#define TAJ_SCHEDULE_NAME               @"TAJ_schedule_name"
#define TAJ_TABLE_COST                  @"TAJ_table_cost"
#define TAJ_TABLE_ID                    @"TAJ_table_id"
#define TAJ_FAVOURITE                   @"TAJ_favorite"
#define TAJ_TABLE_DETAILS               @"table_details"
#define TAJ_Player_Type                 @"TAJ_type"
#define TAJ_TABLE_TYPE                  @"TAJ_table_type"
#define TAJ_TOTAL_PLAYER                @"TAJ_total_player"
#define TAJ_MSG_UUID                    @"TAJ_msg_uuid"
#define TAJ_SEAT                        @"TAJ_seat"
#define TAJ_HEART_BEAT_TABLES_TABLE_ID  @"table.TAJ_table_id"
#define TAJ_HEART_BEAT_TABLE            @"table"
#define TAJ_HEART_BEAT_CARDS            @"card"
#define TAJ_COUNT                       @"TAJ_count"
#define TAJ_GAME_COUNT                  @"TAJ_gamecount"
#define TAJ_BUYIN_PLAYERS               @"players.player"
#define TAJ_REASON_KEY                  @"TAJ_reason"
#define TAJ_JOIN_TABLE_TYPE_KEY         @"table_details.TAJ_table_type"
#define TAJ_JOIN_TABLE_COST_KEY         @"table_details.TAJ_table_cost"
#define TAJ_JOIN_TABLE_MIN_BUYIN_KEY    @"table_details.TAJ_minimumbuyin"
#define TAJ_JOIN_MUUID                  @"PREVIOUS_JOIN_UUID"
#define TAJ_QUIT_MUUID                  @"PREVIOUS_QUIT_UUID"

//autoplay constants
#define TAJ_AUTOPLAY_CARDS_COUNT @"NumberOfAutoPlayCardOnNetDiconnect"
#define TAJ_FIRST_TABLE_AUTOPLAY_DATA @"AutoplayDataForFirstTable"
#define TAJ_SECOND_TABLE_AUTOPLAY_DATA @"AutoplayDataForSecondTable"

//ChairView
//ipad
#define TAJ_AVATAR_MALE_O1_ipad  @"avatar_MALE_01.png"
#define TAJ_AVATAR_MALE_O2_ipad  @"avatar_MALE_02.png"
#define TAJ_AVATAR_MALE_O3_ipad  @"avatar_MALE_03.png"
#define TAJ_AVATAR_MALE_O4_ipad  @"avatar_MALE_04.png"
#define TAJ_AVATAR_MALE_O5_ipad  @"avatar_MALE_05.png"
#define TAJ_AVATAR_MALE_O6_ipad  @"avatar_MALE_06.png"

#define TAJ_AVATAR_FEMALE_O1_ipad @"avatar_FEMALE_01.png"
#define TAJ_AVATAR_FEMALE_O2_ipad @"avatar_FEMALE_02.png"
#define TAJ_AVATAR_FEMALE_O3_ipad @"avatar_FEMALE_03.png"
#define TAJ_AVATAR_FEMALE_O4_ipad @"avatar_FEMALE_04.png"
#define TAJ_AVATAR_FEMALE_O5_ipad @"avatar_FEMALE_05.png"
#define TAJ_AVATAR_FEMALE_O6_ipad @"avatar_FEMALE_06.png"


//iphone
#define TAJ_AVATAR_MALE_O1_iphone  @"avatar_MALE_01-568h~iphone.png"
#define TAJ_AVATAR_MALE_O2_iphone  @"avatar_MALE_02-568h~iphone.png"
#define TAJ_AVATAR_MALE_O3_iphone  @"avatar_MALE_03-568h~iphone.png"
#define TAJ_AVATAR_MALE_O4_iphone  @"avatar_MALE_04-568h~iphone.png"
#define TAJ_AVATAR_MALE_O5_iphone  @"avatar_MALE_05-568h~iphone.png"
#define TAJ_AVATAR_MALE_O6_iphone  @"avatar_MALE_06-568h~iphone.png"

#define TAJ_AVATAR_FEMALE_O1_iphone @"avatar_FEMALE_01-568h~iphone.png"
#define TAJ_AVATAR_FEMALE_O2_iphone @"avatar_FEMALE_02-568h~iphone.png"
#define TAJ_AVATAR_FEMALE_O3_iphone @"avatar_FEMALE_03-568h~iphone.png"
#define TAJ_AVATAR_FEMALE_O4_iphone @"avatar_FEMALE_04-568h~iphone.png"
#define TAJ_AVATAR_FEMALE_O5_iphone @"avatar_FEMALE_05-568h~iphone.png"
#define TAJ_AVATAR_FEMALE_O6_iphone @"avatar_FEMALE_06-568h~iphone.png"

//ipad
#define TAJ_AVATAR_PLAYERRATE01_ipad @"star_symbol1.png"
#define TAJ_AVATAR_PLAYERRATE02_ipad @"star_symbol2.png"
#define TAJ_AVATAR_PLAYERRATE03_ipad @"star_symbol3.png"
#define TAJ_AVATAR_PLAYERRATE04_ipad @"star_symbol4.png"
#define TAJ_AVATAR_PLAYERRATE05_ipad @"star_symbol5.png"
#define TAJ_AVATAR_PLAYERRATE06_ipad @"star_symbol6.png"
//iphone
#define TAJ_AVATAR_PLAYERRATE01_iphone @"star_symbol1-568h@2x.png"
#define TAJ_AVATAR_PLAYERRATE02_iphone @"star_symbol2-568h@2x.png"
#define TAJ_AVATAR_PLAYERRATE03_iphone @"star_symbol3-568h@2x.png"
#define TAJ_AVATAR_PLAYERRATE04_iphone @"star_symbol4-568h@2x.png"
#define TAJ_AVATAR_PLAYERRATE05_iphone @"star_symbol5-568h@2x.png"
#define TAJ_AVATAR_PLAYERRATE06_iphone @"star_symbol6-568h@2x.png"
#define TAJ_ONE 1

//score sheet
#define kTaj_scoresheet_points          @"TAJ_points"
#define kTaj_scoresheet_result          @"TAJ_result"
#define kTaj_scoresheet_score           @"TAJ_score"
#define kTaj_scoresheet_totalscore      @"TAJ_total_score"
#define kTaj_scoresheet_eachplayer      @"meld"
#define kTaj_scoresheet_conversion      @"TAJ_conversion"
#define kTaj_scoresheet_playercount     @"TAJ_player_count"
#define kTaj_scoresheet_tabletype       @"TAJ_table_type"
#define kTaj_scoresheet_player          @"player"
#define kTaj_scoresheet_cardsuit        @"TAJ_suit"
#define kTaj_scoresheet_cardface        @"TAJ_face"
#define kTaj_scoresheet_nickname        @"TAJ_nick_name"
#define kTaj_scoresheet_bet             @"TAJ_bet"
#define kTaj_scoresheet_tableid         @"TAJ_table_id"
#define kTaj_gameid @"TAJ_game_id"
#define kTaj_scoresheet_userid          @"TAJ_user_id"
#define kTaj_scoresheet_box             @"box"
#define kTaj_scoresheet_card            @"box.card"
#define kTaj_scoresheet_restart_msg     @"Please wait while your game starts in %d seconds"
#define kTaj_scoresheet_pregame_msg     @"Please wait other player melding in %d seconds"
#define kTaj_scoresheet_result_modified @"Lost"
#define kTaj_scoresheet_conversion      @"TAJ_conversion"
#define kTaj_scoresheet_gameid          @"TAJ_game_id"
#define kTaj_nick_name                  @"nick_name"

#define kTaj_cmeld                      @"cmeld"
#define kTaj_wmeld                      @"wmeld"

// Navigation bar constants
#define kTitleFont 18
#define kTitleColor [UIColor whiteColor]
#define kBackgroundImage @"nav_bg_image.png"
#define kBackItemImage @"back_bar_button.png"
#define kItemImage @"bar_button_item.png"
#define kBackItemSelectedImage @"back_bar_button_s.png"
#define kItemSelectedImage @"bar_button_item_s.png"
#define kBackItemOffset UIEdgeInsetsMake(0, 5, 0, 0)
#define kItemLeftMargin 10
#define kItemWidth 52
#define kItemHeight kSysNavbarHeight
#define kItemTextFont 12
#define kItemTextNormalColot [UIColor whiteColor]
#define kItemTextSelectedColot [UIColor colorWithWhite:0.7 alpha:1]
#define kSysNavbarHeight 44
#define kDefaultStateBarColor [UIColor blackColor]
#define kDefaultStateBarSytle UIBarStyleBlackOpaque


//radio button model constants

#define kRadioSelected @"RadioButton-Selected"
#define kRadioUnSelected @"RadioButton-Unselected"
#define kRadioButtonWidth 28
#define kRadioButtonHeight 28

//login view controller

#define NSUSER_USER_ID @"User_id"
#define NSUSER_EMAIL_ID @"Email"
#define NSUSER_PASSWORD_ID @"Password"
#define NSUSER_AUTO_LOGIN @"AutoLogin"

#define kOFFSET_FOR_KEYBOARD 80

#define RETRIVE_IMAGE_NORMAL    @"retrieve.png"
#define RETRIVE_IMAGE_HIGHLIGHT @"retrieve_touched.png"
#define LOGIN_IMAGE_NORMAL      @"login.png"
#define LOGIN_IMAGE_HIGHLIGHT   @"login_touched.png"
#define SIGNUP_IMAGE_NORMAL     @"signup.png"
#define SIGNUP_IMAGE_HIGHLIGHT  @"signup_touched.png"
#define CANCEL_IMAGE_NORMAL     @"cancel.png"
#define CANCEL_IMAGE_HIGHLIGHT  @"cancel_touched.png"
#define SUBMIT_IMAGE_NORMAL     @"submit.png"
#define SUBMIT_IMAGE_HIGHLIGHT  @"submit_touched.png"
#define CHECKBOX_IMAGE_NORMAL   @"checkbox.png"
#define CHECKBOX_IMAGE_HIGHLIGHT @"checkbox_touched.png"


#define NAV_SEARCH_TITLE            @"Search View"
#define NAV_BUYCHIPS_TITLE          @"Buy Chips"
#define NAV_TABLE_TITLE             @"Game Lists"
#define NAV_GAMESCREEN_TITLE        @"Game"
#define NAV_LOGINSCREEN_TITLE       @"Login"
#define NAV_HOME_TITLE              @"Home"
// Search View Controller

#define RADIO_BUTTON_WIDTH          40
#define RADIO_BUTTON_HEIGHT         20

#define DEFAULT_POLL_SEARCH         @"101_POOL"
#define DEFAULT_PLAYER_SEARCH       2
#define DEFAULT_BET_SEARCH          5

#define TABLE_GAMELIST_DATA         @"tables.table"
#define SERVER_FIRST_DICTIONARY     @"tables"
#define SERVER_SECOND_DICTIONARY    @"table"
#define SERVER_FILTER_DATA          @"(TAJ_table_type == %@) && (TAJ_maxplayer.integerValue == %d) && (TAJ_bet.integerValue <= %d)"
#define FILTER_FOR_WATCH_TYPE       @"(TAJ_game_schedule == %@)"
#define FILTER_FOR_GAME_COST        @"(TAJ_table_cost == %@) "
#define FILTER_FOR_GAME_COST_IN_LOBBY        @"(TAJ_table_cost == %@)"
#define FILTER_FOR_FREE_GAME_COST_IN_LOBBY        @"(TAJ_schedule_name like '*Fun*')"
#define FILTER_FOR_CASH_GAME_COST_IN_LOBBY        @"(TAJ_schedule_name like [c]'*cash*')"
#define FILTER_FOR_VIP_GAME_COST_IN_LOBBY        @"(TAJ_schedule_name like [c]'*VIP*')"
#define FILTER_FOR_BLAZE_GAME_COST_IN_LOBBY        @"(TAJ_schedule_name like [c]'*blaze*')"
#define FILTER_FOR_HAPPY_GAME_COST_IN_LOBBY        @"(TAJ_schedule_name like [c]'*HAPPY*')"

#define FILTER_FOR_GAME_COST_BOTH        @"(TAJ_table_cost == %@ || TAJ_table_cost == %@)"

#define OR_FILTERS                  @" || "
#define AND_FILTERS                 @" && "

#define FILTER_FOR_101_POOL_LOW_BET            @"((TAJ_table_type == \"101_POOL\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_101_POOL_MEDIUM_BET         @"((TAJ_table_type == \"101_POOL\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_101_POOL_HIGH_BET           @"((TAJ_table_type == \"101_POOL\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_201_POOL_LOW_BET            @"((TAJ_table_type == \"201_POOL\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_201_POOL_MEDIUM_BET         @"((TAJ_table_type == \"201_POOL\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_201_POOL_HIGH_BET           @"((TAJ_table_type == \"201_POOL\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_BEST_OF_3_LOW_BET           @"((TAJ_table_type == \"BEST_OF_3\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_BEST_OF_3_MEDIUM_BET        @"((TAJ_table_type == \"BEST_OF_3\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_BEST_OF_3_HIGH_BET          @"((TAJ_table_type == \"BEST_OF_3\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_BEST_OF_6_LOW_BET           @"((TAJ_table_type == \"BEST_OF_6\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_BEST_OF_6_MEDIUM_BET        @"((TAJ_table_type == \"BEST_OF_6\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_BEST_OF_6_HIGH_BET          @"((TAJ_table_type == \"BEST_OF_6\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_BEST_OF_2_LOW_BET           @"((TAJ_table_type == \"BEST_OF_2\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_BEST_OF_2_MEDIUM_BET        @"((TAJ_table_type == \"BEST_OF_2\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_BEST_OF_2_HIGH_BET          @"((TAJ_table_type == \"BEST_OF_2\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_PR_JOKER_LOW_BET            @"((TAJ_table_type == \"PR_JOKER\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_PR_JOKER_MEDIUM_BET         @"((TAJ_table_type == \"PR_JOKER\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_PR_JOKER_HIGH_BET           @"((TAJ_table_type == \"PR_JOKER\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_PR_NOJOKER_LOW_BET          @"((TAJ_table_type == \"PR_NOJOKER\") && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_PR_NOJOKER_MEDIUM_BET       @"((TAJ_table_type == \"PR_NOJOKER\") && (TAJ_bet.integerValue > %d) && (TAJ_bet.integerValue <= %d))"
#define FILTER_FOR_PR_NOJOKER_HIGH_BET         @"((TAJ_table_type == \"PR_NOJOKER\") && (TAJ_bet.integerValue > %d))"

#define FILTER_FOR_6_PLAYERS                   @"(TAJ_maxplayer == \"6\")"
#define FILTER_FOR_2_PLAYERS                   @"(TAJ_maxplayer == \"2\")"


#define NSUSER_MOVE_DATA            @"Filtered"
#define NSUSER_BET_TITLE            @"BetTile"
#define NSUSER_PLAYER_TITLE         @"PlayeTile"
#define NSUSER_GAME_TITLE           @"GameTile"

#define NSUSER_BET_CONST            @"Betconst"
#define NSUSER_PLAYER_CONST         @"Playeconst"
#define NSUSER_GAME_CONST           @"Gameconst"


#define ONE_NOT_ONE                 101
#define TWO_NOT_ONE                 201
#define BEST_OF_THREE               3

#define ONE_NOT_ONE_LABEL           @"101"
#define TWO_NOT_ONE_LABEL           @"201"
#define BEST_OF_THREE_LABEL         @"BO3"

#define FREE_GAME_LOBBY                   @"Free"
#define CASH_GAME_LOBBY                   @"Cash"
#define VIP_GAME_LOBBY                   @"VIP"
#define BLAZE_GAME_LOBBY                   @"Blaze"
#define HAPPY_GAME_LOBBY                   @"Happy"


#define ONE_NOT_ONE_LABEL_LOBBY           @"101"
#define TWO_NOT_ONE_LABEL_LOBBY           @"201"
#define BEST_OF_THREE_LABEL_LOBBY         @"Best of 3"
#define BEST_OF_TWO_LABEL_LOBBY           @"Best of 2"
#define BEST_OF_SIX_LABEL_LOBBY           @"Best of 6"
#define GAME_JOKER_LOBBY                  @"Joker"
#define GAME_NO_JOKER_LOBBY               @"No Joker"

#define BEST_OF_TWO                 2
#define BEST_OF_SIX                 6



#define BEST_OF_TWO_LABEL           @"BO2"
#define BEST_OF_SIX_LABEL           @"BO6"

#define GAME_JOKER                  1
#define GAME_NO_JOKER               0

#define GAME_JOKER_LABEL            @"Joker"
#define GAME_NO_JOKER_LABEL         @"No Joker"

#define GAME_TYPE                   @"GameType"
#define NO_OF_PLAYER                @"Player"
#define BET_TYPE                    @"Bet"

#define GAME_TYPE_LOBBY                   @"Game Type"


#define TWO_PLAYER                  2
#define SIX_PLAYER                  6
#define TWO_PLAYER_LABEL            @"2"
#define SIX_PLAYER_LABEL            @"6"

#define LOW_BET                     5
#define MEDIUM_BET                  10
#define HIGH_BET                    15

#define LOW_BET_LABEL               @"Low"
#define MEDIUM_BET_LABEL            @"Medium"
#define HIGH_BET_LABEL              @"High"

#define LOW_BET_LABEL_LOBBY               @"Low"
#define MEDIUM_BET_LABEL_LOBBY            @"Medium"
#define HIGH_BET_LABEL_LOBBY              @"High"

#define FILTER_101                  @"101_POOL"
#define FILTER_201                  @"201_POOL"
#define FILTER_BEST_3               @"BEST_OF_3"
#define FILTER_BEST_2               @"BEST_OF_2"
#define FILTER_BEST_6               @"BEST_OF_6"
#define FILTER_JOKER                @"PR_JOKER"
#define FILTER_NO_JOKER             @"PR_NOJOKER"

#define FILTER_2_PLAYER             2
#define FILTER_6_PLAYER             6

#define FILTER_LOW_BET              55
#define FILTER_MEDIUM_BET           100
#define FILTER_HIGH_BET             30000


#define FREE_GAME                   @"FREE"
#define CASH_GAME                   @"CASH"



#define POOLS_LABEL                   @"POOLS"
#define DEALS_LABEL                   @"DEALS"
#define STRIKES_LABEL                 @"POINTS"
#define FAVOUTITES_LABEL                 @"FAVOURITES"


#define POOLS_LABEL_LOBBY                   @"Pools"
#define DEALS_LABEL_LOBBY                   @"Deals"
#define STRIKES_LABEL_LOBBY                 @"Points"
#define TOURNAMENTS_LABEL_LOBBY             @"Tourneys"
#define FAVOURITES_LABEL_LOBBY              @"Favourites"


//score sheet

#define SCORE_SHEET_HEADER_IPHONE @" Name          Result           Show Cards            Count           Total Count"
#define SCORE_SHEET_HEADER_IPAD	@"       Name                       Result                      Show Cards                          Count                                       Total Count"
#define SCORE_SHEET_TIMER @"Your game will starts in %d seconds"

//tableview list
#define TABLE_HEADER_LABEL_IPHONE	@"  BET      MaxPlayer     Table No       Players      Status    Join"
#define TABLE_HEADER_LABEL_IPAD	@"    BET                         MaxPlayer                  Table No                Players                    Status              Join"

#define TABLE_DESCR_LABEL_IPHONE	@"    Game Type: %@            Players: %@                   Bet: %@"
#define TABLE_DESCR_LABEL_IPAD	@"        Game Type: %@                                                     Players: %@                                                   Bet: %@"

#define SCREEN_JOINED @"ScreenPla"
#define WINDOW_JOINED @"WindowPla"

#define TABLE_BET_ALL      @"All"
#define TABLE_PLAYER_ALL   @"2/6"
#define TABLE_GAMETYPE_ALL @"All"

//game play screen
#define USER_NAME_LABEL @"%@"
#define GAME_TABLE_TYPE_LABEL @"%@"
#define GAME_TABLE_ID_LABEL @"%@"
#define GAME_TABLE_BET_LABEL @"%@"
#define GAME_TYPE_LABEL @"%@"
#define GAME_ID_LABEL @"#%@"
#define GAME_PRIZE_LABEL @"%.2f"

#define GAME_MESSAGE_ORIGIN_X_IPAD 190
#define GAME_MESSAGE_ORIGIN_Y_IPAD 90
#define GAME_MESSAGE_SIZE_WIDTH_IPAD 280

#define GAME_ID_VALUE                         @"TAJ_game_id"
#define GAME_PRIZE_MONEY_VALUE                @"TAJ_prizemoney"
#define GAME_TABLE_TYPE_VALUE                 @"table_details.TAJ_table_type"
#define GAME_TABLE_ID_VALUE                   @"table_details.TAJ_table_id"
#define GAME_TABLE_BET_VALUE                  @"table_details.TAJ_bet"
#define GAME_GET_MAX_PLAYER                   @"table_details.TAJ_maxplayer"
#define GAME_GET_MIN_PLAYER                   @"table_details.TAJ_minplayer"
#define GAME_GET_EXISTING_PLAYERS             @"table_details.players.player"
#define GAME_GET_EXISTING_PLAYERS_FORCE       @"table_details.players"
#define GAME_DROPPED_PLAYERS_LIST             @"table_details.drop_list.player"
#define GAME_MIDDLE_PLAYERS_LIST              @"table_details.middle.player"
#define GAME_TABLE_PRIZE_INFO                 @"table_details.gamedetails"
#define GAME_TABLE_IS_SPECATOR                @"table_details.TAJ_game_start"
#define GAME_TABLE_DEALER_ID                  @"table_details.TAJ_dealer_id"
#define GAME_TABLE_COST_TYPE                  @"table_details.TAJ_table_cost"
#define CASH_GAMES                            @"CASH_CASH"
#define FREE_GAMES                            @"FUNCHIPS_FUNCHIPS"
#define CASH_TEXT                             @"(CASH)"
#define FREE_TEXT                             @"(FREE)"
#define PLAYER_SIT_MESSAGE                    @"Please Take Your Sit"
#define PLAYER_WAIT_FOR_ANOTHER_PLAYER_TO_SIT @"Please Wait For one more players to sit"

#define GAME_CURRENT_SCREEN_PLAYERJOIN        @"TAJ_seat"
#define GAME_DEFFERENT_SCREEN_PLAYERJOIN      @"TAJ_place"
#define GAME_DEFFERENT_SCREEN_PLAYERTYPE      @"TAJ_type"
#define GAME_PLACE_PLAYER_NAME                @"TAJ_nick_name"

#define GAME_PLAYER_JOIN_AS_PLAY_KEY          @"TAJ_table_join_as"
#define GAME_PLAYER_JOIN_AS_PLAY_VALUE        @"play"
#define GAME_PLAYER_JOIN_AS_MIDDLE_VALUE      @"middle"
#define GAME_SEARCH_JOIN_TABLE_JOIN_TYPE      @"TAJ_type"
#define GAME_SEARCH_JOIN_TABLE_JOIN_PLAY      @"Play"

#define GAME_SCHEDULE_TEXT                    @"Please wait your game will be start in %d seconds"
#define GAME_START_TIMER                      @"TAJ_starttime"
#define GAME_TIMER_STAMP                      @"TAJ_timestamp"
#define GAME_WAIT_FOR_TOSS                    @"Please wait for toss"

//gameplay player status values
#define DROP_TEXT                   @"drop"
#define ELIMINATE_TEXT              @"eliminate"
#define TIMEOUT_TEXT                @"timeout"
#define TABLE_LEAVE_TEXT            @"table_leave"

//progress alert view
#define ALERT_BLACK_COLOR 0x000000
#define ALERT_WHITE_COLOR 0xffffff
#define ALERT_GREEN_COLOR 0x339900

#define ALERT_VIEW_WIDTH 290

#define ALERT_VIEW_BG ALERT_WHITE_COLOR
#define ALERT_VIEW_BG_ALPHA 0


#define ALERT_TopAndBottomHeight 40*2

#define ALERT_HeadViewHeight 40
#define ALERT_HeadViewColor 0x5388b2
#define ALERT_HeadTextFontSize 18
#define ALERT_HeadTextFontColor ALERT_WHITE_COLOR
#define ALERT_HeadTextMargin 10

#define ALERT_BtnViewHeight 40
#define ALERT_BtnHeight 30
#define ALERT_BtnWidth 120
#define ALERT_BtnFontSize 14
#define ALERT_BtnViewBgColor 0x999999
#define ALERT_BtnBgColor 0xdadada
#define ALERT_BtnPressColor 0xbababa
#define ALERT_BtnMargin 10

#define ALERT_TitleFontSize 18
#define ALERT_TitleLabelHeight 30

#define ALERT_LabelFontSize 15
#define ALERT_OneLabelHeight 26
#define ALERT_LabelPadding 10

#define ALERT_TableMaxHeight 44*7

#define ALERT_CellBgColor ALERT_WHITE_COLOR
#define ALERT_CellFontColor ALERT_BLACK_COLOR
#define ALERT_CellTouchColor ALERT_GREEN_COLOR

#define kTransitionDuration 0.2

#define ALERT_CenterBorderWidth 2


#define AppFontWithSize(a) [UIFont fontWithName:@"Helvetica" size:(a)]
#define AppBoldFontWithSize(a) [UIFont fontWithName:@"Helvetica-Bold" size:(a)]

#define UIColorFromRGB(rgbValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:1.0]

#define UIColorFromRGBAndAlpha(rgbValue,alphaValue) [UIColor colorWithRed:((float)((rgbValue & 0xFF0000) >> 16))/255.0 green:((float)((rgbValue & 0xFF00) >> 8))/255.0 blue:((float)(rgbValue & 0xFF))/255.0 alpha:alphaValue]

#define degreesToRadinas(x) (M_PI * (x)/180.0)

// switch lobby
#define FIRST_TABLE_REQUESTED_TO_JOIN_NEWTABLE   1
#define SECOND_TABLE_REQUESTED_TO_JOIN_NEWTABLE  2

//predicate format for sort cards
#define kTaj_predicate_suit @"TAJ_suit == %@"
#define kTaj_suit_clube     @"c"
#define kTaj_suit_heart     @"h"
#define kTaj_suit_joker     @"jo"
#define kTaj_suit_diamond   @"d"
#define kTaj_suit_spade     @"s"

//Nibname
#define TAJChairViewNibName           @"TajChairView"
#define TAJPlayTableViewNibName       @"TAJPlayTableView"
#define TAJPlayerModelViewNibName     @"TAJPlayerModelView"
#define TAJAutoPlayViewNibName        @"TAJAutoPlayView"
#define TAJCardViewNibName            @"TAJCardView"
#define TAJScoreSheetCardViewNibName  @"TAJScoreSheetCard"
#define TAJDiscardCardViewNibName     @"TAJDiscardCardView"
#define TAJMeldCardViewNibName        @"TAJMeldCardView"
#define TAJSmartCorrectionViewNibName @"TAJSmartCorrectionView"
#define TAJTableSectionNibName        @"TAJTableSectionView"
#define TAJTableSectionFooterNibName  @"TAJTableFooterSectionView"//rath

#define TAJProgressAlertViewNibName   @"TAJProgressAlertView"
#define TAJJokerViewNibName           @"TAJJokerView"
#define TAJWinnerViewNibName          @"TAJWinnerScreen"
#define TAJGameJokerNibName           @"TAJGameJoker"
#define TAJListSectionViewNibName     @"TAJListSectionView"
#define TAJResultSectionNibName       @"TAJResultSection"
#define TAJScoreSheetCellNibName      @"TAJScoreSheetCell"
#define TAJScoreBoardCellNibName      @"TAJScoreBoardCell"
#define TAJTableViewCellNibName       @"TAJTableViewCell"
#define TAJLiveFeedCellNibName        @"TAJLiveFeedCell"
#define TAJPlayerTurnViewNibName      @"TAJPlayerTurnView"
#define TAJScoreBoardCellNibName      @"TAJScoreBoardCell"
#define TAJPlayerModelCardViewNibName  @"TAJPlayerModelCard"

#define TAJScoreBoardCellStrikesNibName     @"TAJScoreBoardCellForStrikes"
#define TAJTableSectionViewStrikesNibName   @"TAJTableSectionViewForStrikes"

//card offsets
#define CARD_OFFSET_X_IPAD                  35.0f
#define ANIMATE_CARD_OFFSET_X_IPAD          33.5f
#define DISCARDCARD_OFFSET_X_IPAD           35.0f
#define DISCARDCARD_OFFSET_X_IPHONE_4       15.0f
#define DISCARDCARD_OFFSET_X_IPHONE_5       26.0f
#define CARD_OFFSET_X_IPHONE_5              20.0f
#define ANIMATE_CARD_OFFSET_X_IPHONE_5      5.0f

#define CARD_ANIMATION_OFFSET_X_IPHONE_5    25.0f
#define CARD_OFFSET_X_IPHONE_4              16.0f
#define CARD_MOVE_UP_OFFSET_Y_IPAD          12.0f
#define CARD_MOVE_UP_OFFSET_Y_IPHONE        6.0f

#define DISCARD_CARD_OFFSET_X_IPAD                  45.0f
#define DISCARD_CARD_OFFSET_X_IPHONE_5              28.0f
#define DISCARD_CARD_OFFSET_X_IPHONE_4              25.0f

// Dictionary Keys
#define kTaj_Place_Key              @"TAJ_place"
#define kTaj_Event_name             @"TAJ_event_name"
#define kTaj_Error_code             @"TAJ_code"
#define kTaj_Seat_key               @"TAJ_seat"
#define kTaj_buyin_amt_other        @"TAJ_buyinammount"
#define kTaj_buyin_amt_this         @"TAJ_buyinamount"
#define kTaj_player_rating          @"TAJ_playerlevel"
#define kTaj_player_gender          @"TAJ_gender"
#define kTaj_player_char_no         @"TAJ_char_no"
#define kTaj_minplayers_Key         @"TAJ_minplayer"
#define kTaj_userid_Key             @"TAJ_user_id"
#define kTaj_winner_key             @"TAJ_winner_id"
#define kTaj_winner_name            @"TAJ_winner_nickname"
#define kTaj_split_nickname         @"TAJ_nick_name"
#define kTaj_split_amount           @"TAJ_amount"
#define kTaj_split_winners          @"splitter.player"
#define kTaj_winner_prizemoney      @"TAJ_prizemoney"
#define kPlayers_From_Toss_Key      @"players.player"
#define kTaj_Card_Face_Key          @"TAJ_face"
#define kTaj_isJoker_card           @"TAJ_jocker"
#define kTaj_Card_Suit_Key          @"TAJ_suit"
#define kTaj_Toss_Winner_Key        @"TAJ_toss_winner"
#define kPlayers_Key                @"player"
#define kFaceDown_Cards_Key         @"face_down_stack.card"
#define kFaceUp_Cards_Key           @"face_up_stack.card"
#define kCards_Key                  @"card"
#define kTaj_nickname               @"TAJ_nick_name"
#define kTaj_autoPlay_status        @"TAJ_autoplay"
#define kTaj_autoplaystatus         @"TAJ_status"
#define kTaj_autoPlay_cardscount    @"TAJ_autoplaycount"
#define kTaj_autoPlay_totalCount    @"TAJ_totalcount"
#define kTaj_Dealer_name            @"TAJ_dealer_nick_name"
#define kTaj_Dealer_id              @"TAJ_dealer_id"
#define kTaj_Text                   @"TAJ_text"
#define kTaj_meld_success_user      @"TAJ_sucess_nickname"
#define kTaj_player_timeout         @"TAJ_time_out"
#define kTaj_playershow_timeout     @"TAJ_timeout"
#define kTaj_rejoin_timeout         @"TAJ_timeout"
#define kTaj_rejoin_score           @"TAJ_score"
#define kTaj_rejoin_status          @"TAJ_status"
#define kTaj_split_timeout          @"TAJ_timeout"
#define kTaj_split_status           @"TAJ_split"
#define kTaj_split_requester        @"TAJ_requester"
#define kTaj_split_true             @"True"
#define kTaj_split_false            @"False"
#define kTaj_rebuyin_other_amt      @"TAJ_totaltableamt"
#define kTaj_rebuyin_this_amt       @"TAJ_table_ammount"
#define kFace_Up_Stack_Key          @"face_up"
#define kFace_Down_Stack_Key        @"face_down"
#define kTaj_Stack_Key              @"TAJ_stack"
#define kTaj_Table_close_msg        @"TAJ_message"
#define kTaj_Max_Players            @"maxplayer"
#define kTaj_Card_Place_Id          @"card_place"
#define kTaj_Table_Type_Path        @"table_details.TAJ_table_type"
#define kTaj_Table_min_buyin        @"table_details.TAJ_minimumbuyin"
#define kTaj_Table_max_player       @"table_details.TAJ_maxplayer"
#define kTaj_Table_extraTime        @"table_details.TAJ_extratime"
#define kTaj_Table_middleJoin       @"table_details.TAJ_middlejoin"
#define kTaj_Table_gamestart       @"table_details.TAJ_game_start"
#define kTaj_Table_gamecount       @"table_details.TAJ_gamecount"
#define kTaj_Player_DeviceId       @"TAJ_DEVICE_ID"

#define kTaj_Table_details          @"table_details"
#define kTaj_StarCardInfo           @"TAJ_StarcardInfo"
#define kTaj_Star_card              @"TAJ_Star_card"
#define kTaj_Deschedule_reason      @"TAJ_reason"
#define kONE_NOT_ONE                @"101_POOL"
#define kTWO_NOT_ONE                @"201_POOL"
#define kBEST_OF_3                  @"BEST_OF_3"
#define kBEST_OF_2                  @"BEST_OF_2"
#define kBEST_OF_6                  @"BEST_OF_6"
#define kPR_NOJOKER                 @"PR_NOJOKER"
#define kPR_JOKER                   @"PR_JOKER"

// Other Keys
#define kTimer_Duration_key     @"Duration"


//error alert title
#define ALERT_EMPTY_TITLE                     @""
#define ALERT_ERROR_TITLE                     @"Error"
#define ALERT_DATABASE_ERROR                  @"There is a problem with the server"
#define ALERT_LOW_BALNCE_TITLE                @"You do not have enough chips to play this table"
#define ALERT_MAX_TWO_TABLE_JOIN              @"You have reached maximum number (2) of tabs. Please close a tab to open a new one"
#define ALERT_SERCH_JOIN_ERROR                @"This table is currently not available,\nPlease select another table."
#define ALERT_MAX_BUYINAMOUNT                 @"Please enter maximum amount only."
#define STATE_BLOCKE_MESSAGE                 @"Our services are currently not availabe on Taj Rummy in your state. As the state regulation prohibits a player from using the services offered by us."
#define ALERT_MIN_BUYINAMOUNT                 @"Please enter minimum amount."
#define ALERT_NOT_FUNDED_PLAYER                @"You are not funded player"

//level checking alerts
#define ALERT_CAN_NOT_REJOIN_PLAY_TAG             5
#define ALERT_CAN_REJOIN_PLAY_TAG                 8
#define ALERT_SPLIT_REQUEST_TIMER_TAG             10
#define ALERT_SPLIT_REJECT_TAG                    28
#define ALERT_CAN_NOT_LEAVE_TAG                   3
#define ALERT_CAN_LEAVE_TAG                       2
#define ALERT_SHOW_BUTTON_TAG                     12
#define ALERT_DROP_BUTTON_TAG                     15
#define ALERT_NO_LAST_HAND_TAG                    18
#define ALERT_NO_LAST_ENTRIES_TAG                 20
#define ALERT_REBUYIN_JOKER_TAG                   22
#define ALERT_LEAVE_JOKER_TABLE_TAG               24
#define ALERT_RESHUFFLE_TAG                       26
#define ALERT_MAX_TWO_TABLE_TAG                   35
#define ALERT_MELD_INVALID                        44
#define ALERT_SERVER_ERROR_TAG                    55
#define ALERT_ERROR                               66
#define ALERT_JOKER_CREATE_TAG                    27
#define CANNOT_LOGOUT_TAG                         77
#define LOGOUT_TAG                                88
#define ALERT_SERVER_UNDER_MAINTAINANCE_TAG       100
#define ALERT_REGISTER_TAG                        101
#define ALERT_DEREGISTER_TAG                      102
#define REGISTER_SUCCESS_TAG                      103
#define DEREGISTER_SUCCESS_TAG                    104
#define STOP_REGISTRATION_TAG                     105
#define STOP_CANCEL_REGISTRATION_TAG              106
#define TOURNAMENT_TO_START_TAG                   107
#define TOURNAMENT_ELEMINATE_TAG                  108
#define INSUUFICIENT_CHIPS_TAG 109
#define ALERT_STATE_BLOCKED_ERROR_TAG             483 //ratheesh

#define ALERT_SPLIT_REJECT_TITLE              @"Player %@ has rejected to split."
#define ALERT_SPLIT_TIMER_TITLE               @"Player %@ has requested for a split. Do you agree to split?(%d)"

#define ALERT_LEVEL_CHECKING_FAIL_TITLE       @"You have reached max no. of points, Would you like to play another game?"
#define ALERT_LEVEL_CHECKING_SUCCESS_TITLE    @"Congratulations,you won the game, Do you want to join 1 more game?"
#define ALERT_LEVEL_CHECKING_REJOIN_TITLE     @"You won the game, Do you want to join 1 more game?"
#define ALERT_JOKER_ELIMINATE_TITLE           @"Sorry you are eliminated from the game."
#define ALERT_REBUYIN_REQUEST_TITLE           @"Your balance is low, do you want to rebuy in?"
#define ALERT_JOKER_SIT_INSUFFICIENT           @"You do not have enough chips to play this table."
#define ALERT_STACK_RESHUFFLE_TITLE           @"Cards have been shuffled"
#define ALERT_LEVEL_SORRY_TITLE               @"Sorry"
#define ALERT_LEVEL_CONGRATULATION_TITLE      @"Congratulations"

//notifications

#define RESET_TIMER_NOTIFICATION              @"Reset_Timer"

//other level game constants
#define GAME_NEW_TABLE_BET                @"table_details.TAJ_bet"
#define GAME_NEW_TABLE_MAXPLAYER          @"table_details.TAJ_maxplayer"
#define GAME_NEW_TABLE_TABLETYPE          @"table_details.TAJ_table_type"
#define GAME_NEW_TABLE_CASH_TYPE          @"table_details.TAJ_table_cost"
#define GAME_NEW_TABLE_CONVERSION         @"table_details.TAJ_conversion"
#define GAME_NEW_TABLE_STREAM_NAME        @"table_details.TAJ_streamname"
#define GAME_NEW_TABLE_STREAM_ID          @"table_details.TAJ_streamid"
#define GAME_NEW_TABLE_GAME_SETTING_ID    @"table_details.TAJ_gamesettingid"

// Table info messages
#define PLAY_TABLE_EMPTY_MSG            @""
#define PLAYER_LEFT_MESSGAE             @"Player %@ left the table"
#define STACK_RESHUFFLE_MESSAGE         @"Cards reshuffling"
#define PLAYER_DROP_MESSAGE             @"%@ Dropped"
#define PLEASE_WAIT                     @"Please wait"
#define PLEASE_TAKE_SEAT_MESSAGE        @"Please take a seat to play the game"
#define PLESE_WAIT_FOR_START_GAME       @"Please wait while your game starts in %d seconds"
#define REJOIN_TIMER_MESSAGE            @"Would you like to Rejoin the game? Time left(%d)"
#define TURN_AND_TIME_LEFT              @"Turn : %s Time Left : %d"
#define TOSS_WINNER_MESSAGE             @"Player %@ has won the toss and will start the game"
#define EXTRATIME_THISPLAYER_INFO       @"You have extended your time"
#define EXTRATIME_OTHERPLAYER_INFO      @"%@ choosen extra time"
#define REARRANGING_SEATS               @"Rearranging seats"
#define PLEASE_WAIT_FOR_TOSS            @"Please wait for the toss"
#define STARTING_GAME                   @"Starting game, Please wait..."
#define PLAYER_TURN_MESSAGE             @"Time Left: %d"
#define PLAYER_SHOW_MESSAGE             @"%@ has placed show, please wait while we evaluate the cards  in %d seconds"
#define SHOW_MESSAGE_OTHER              @"%@ has placed show, please wait while we evaluate the cards"
#define OTHER_PLAYER_WRONG_MELD         @"Player %@ has placed an invalid show."
#define THIS_PLAYER_WRONG_MELD          @"You have placed an invalid show"
#define MELD_OTHER_PLAYER               @"Player %@ has placed a valid show, please meld your cards"
#define THIS_PLAYER_MELD_SUCCESS        @"Congratulations you have a valid show; Please wait while we evaluate your opponent(s) cards"
#define GAME_END_MESSAGE                @"Game Ended"
#define GAME_PLAYER_TURN_TIMEOUT        @" %@ Turn Timeout"
#define PLAYER_TIMEOUT                  @"Timeout"
#define WAIT_FOR_OTHER_PLAYERS          @"Please wait for (%d) more players to join"
#define JOKER_TABLE_SIT_MESSAGE         @"Please take a seat and wait for next game to start."
#define JOKER_TABLE_SIT_CONFIRM         @"Please wait for next game to start."
#define MELD_TIMEOUT                    @"Meld timeout"
#define MELD_PLAYER_TIMER               @"Please send your cards :"
#define INVALIDSHOW                     @"Invalid Show -"
#define PURESEQUENCEMISSING             @"Pure Sequence is missing"
#define SOMESETMISSING                  @"Some Sets are missing"


#define INFO_PICKED_CARD                @"You are already picked a card"
#define CANT_PICK_JOKERCARD             @"You cannot pick a joker card"
#define INFO_NOT_MYTURN                 @"Please wait for your turn"
#define SCORE_BOARD_NO_ENTRY            @"There are no entries"
#define LAST_HAND_NO_ENTRY              @"There is no last hand"
#define SHOW_BUTTON_TITLE               @"Are you sure you want to place a show?"
#define DROP_BUTTON_TITLE               @"Are you sure you want to drop your game?"
#define TABLE_CLOSE_MESSAGE             @"This table has been closed, please join another table."
#define PLAYER_LESS_THEN_MINIMUM        @"Player Less than Minimum Player limit."
#define SPLIT_REQUESTED_MSG             @"Split Requested."
#define MELD_TIMER_MESSAGE              @"Please send cards in %d"
#define AUTO_PLAY_PLAYER_LABEL          @"Auto Play %@/%@"
//#define AUTO_PLAY_DEFAULT_TEXT          @"Auto Play 0/5"
#define AUTO_PLAY_DEFAULT_TEXT          @"0/5"

#define PLAYER_RECONNECTED_MESSAGE_TABLE      @"Player %@ has joined back."
#define PLAYER_DISCONNECTED_MESSAGE_TABLE     @"Player %@ has lost the internet connection and is in Auto Play."
#define END_TOURNEY @"This tournament is completed"

// filter view
// MeldView Scrolling
#define MELDVIEWSCROLLING_IPHONE   40

#define MELDVIEWSCROLLING_IPAD     54

typedef struct gameTypeStructure
{
    BOOL isOneNotOne;
    BOOL isTwoNotOne;
    BOOL isBestOfThree;
    BOOL isBestOfTwo;
    BOOL isBestOfSix;
    BOOL isJoker;
    BOOL isNoJoker;
    BOOL isFavourite;
}GameTypeStruct;

typedef struct tableTypeStructure
{
    BOOL isFree;
    BOOL isCash;
    BOOL isVIP;
    BOOL isBlaze;
    BOOL isHappy;
}TableTypeStruct;

typedef struct playerStructure
{
    BOOL isTwoPlayer;
    BOOL isSixPlayer;
    
}PlayerTypeStruct;

typedef struct betStructure
{
    BOOL isLowBet;
    BOOL isMediumBet;
    BOOL isHighBet;
    
}BetTypeStruct;

typedef enum tableCostTypeEnum
{
    eCashGame,
    eFreeGame,
    eVIP,
    eBlaze,
    eHappy,
    eBoth
    
}TableCostEnum;

typedef enum tableViewTypeEnum
{
    ePlayGame,
    eWatchGame
    
}ViewTypeEnum;

typedef enum variantsTypeEnum
{
    ePoolOrDealType,
    eStrikesType
}VariantsTypeEnum;

typedef enum statusCode
{
    eInvalidEmail = 707,
    eUnregistered = 708,
    eRequestSuccessfull = 200,
    eOnlyAlphabets = 701,
    eUsernameCharacters = 702,
    ePasswordMinimum = 703,
    eValidEmail = 704,
    eSelectGender = 705,
    eValidDOB = 706,
    eSelectState = 707,
    eBelow18 = 708,
    eMobileAlreadyReg = 709,
    eCorrectMobileNumber = 710,
    eReadTOU = 711,
    eEmailAlreadyReg = 712,
    eRegFailed = 713,
    eSelectValidRef = 714,
    eUserNotAvailable = 715
}StatusCode;

typedef enum loginScreenOption
{
    eLoginOption,
    ePlayNowOption,
    eBothOption
}LoginScreenOption;

typedef enum webPageViewOption
{
    eResponsibleGamingWithLoginOption,
    eResponsibleGamingWithPlayNowOption,
    eGameRulesOption,
    ePurchaseOption
}WebPageViewOption;


// Live feed message
#define LIVE_FEED_PLAYER_JOIN           @"Player %@ has joined the table."
#define LIVE_FEED_NORMAL_GAME_START     @"Round"
#define LIVE_FEED_JOKER_GAME_START      @"New game"
#define LIVE_FEED_PLAYER_WON_TOSS       @"Player %@ has won the toss"
#define LIVE_FEED_PLAYER_DEALER         @"Player %@ will be the dealer"
#define LIVE_FEED_PLAYER_DISCARD_MSG    @"Player %@ has discarded a %@ %@ card"
#define LIVE_FEED_PICK_FROM_OPEN_DECK   @"Player %@ has picked  from the open deck"
//#define LIVE_FEED_PLAYER_PUT_SHOW       @"Player %@ has requested for a show"
#define LIVE_FEED_PICK_FROM_CLOSED_DECK @"Player %@ has picked a card from the closed deck"
#define LIVE_FEED_PLAYER_DROP_MSG       @"Player %@ drops"
#define LIVE_FEED_PLAYER_EXTRA_TIME     @"Player %@ has chosen to extend the time"
#define LIVE_FEED_PLAYER_LEFT_TABLE     @"Player %@ has left the table"
#define LIVE_FEED_PLAYER_REQUEST_SHOW   @"Player %@ has request for a show."
#define LIVE_FEED_PLAYER_INVALID_SHOW   @"Player %@ has an invalid show."
#define LIVE_FEED_PLAYER_VALID_SHOW     @"Player %@ has a valid show."
#define LIVE_FEED_PLAYER_ELIMINATED     @"Player %@ is eliminated from the game"
#define LIVE_FEED_PLAYER_MIDDLE_JOIN    @"Player %@ has middle joined the game."
#define LIVE_FEED_PLAYER_REJOIN_MSG     @"Player %@ has rejoined the game at %@"
#define LIVE_FEED_PLAYER_SPLIT_REQUEST  @"Player %@ requested to split the prize money"
#define LIVE_FEED_PLAYER_SPLIT_REJECT   @"Player %@ rejected to split."
#define LIVE_FEED_PLAYER_DISCONNECTED   @"Player %@ has lost the internet connection and is in Auto Play."
#define LIVE_FEED_PLAYER_CONNECTED      @"Player %@ has joined back."

// controller nib name
#define IAM_NIB_CONTROLLER_IPHONE_5     @"TAJIamBackViewController_iphone5"
#define IAM_NIB_CONTROLLER_IPHONE_4     @"TAJIamBackViewController_iphone4"
#define SCORE_SHEET_NIB_CONTROLLER_IPHONE_5   @"TAJScoreSheetViewController_iPhone"
#define SCORE_SHEET_NIB_CONTROLLER_IPHONE_4   @"TAJScoreSheetViewController_iphone4"
#define SCORE_SHEET_NIB_CONTROLLER_IPAD     @"TAJScoreSheetViewController_iPad"

// iam back
#define NETWORK_DISCARDED_CARDS        @"pickdiscards.pickdiscard"
#define kTaj_Network_card_type         @"TAJ_type"
#define kTaj_Network_pick_type         @"TAJ_deck"
#define kTaj_networkdiscard_predict    @"(TAJ_user_id == %@) && (TAJ_autoplay == 'True') && (TAJ_type == 'discard')"
#define kTaj_removediscard_predict     @"(TAJ_user_id == %@)"
#define kTaj_discardHistory_predict    @"(TAJ_type == 'discard')"
#define kTaj_Event_Key                 @"event"
#define kTaj_Game_result_reconnect     @"event.TAJ_event_name"
//#define I_AM_BUTTON_CANNOT_CLICK       @"You cannot rejoin at last 4 seconds please wait for other player turn update"
#define I_AM_BUTTON_CAN_CLICK          @"You can tap \"I Am Back\" Now"
#define I_AM_BUTTON_CANNOT_CLICK          @"You cannot rejoin the game in the last 4 seconds. Please wait until the current turn updates to the next player and then you can tap on  \"I Am Back\""

#define MAX_AUTOPLAY_CARDS             5
// card slots
#define SLOT_ARRAY_CAPACITY             25
#define MAX_SLOT_PLAY_TABLE             25
#define FILL_ARRAY_FROM_INDEX           3
#define SERVER_SLOT_ARRAY_KEY           @"TAJ_deckcard"
#define MAX_NULL_OBJECT_IN_GAME         2
#define REMOVE_EMPTY_SPACE_FROM_INDEX_IPHONE   18
#define REMOVE_EMPTY_SPACE_FROM_INDEX_IPAD   18
#define TAJ_SLOT_INDEX                  @"TAJ_slot"

#define USER_DEFAULT_VIBRATE            @"vibrate"
#define USER_DEFAULT_SOUND            @"sound"


//WebEngage - KEY - LIVE - aa1325b9

//#define ZDCHAT_LIVE_KEY @"2Vj6FS3wY2GXOGUlzVrEEw5whxfszYDj"
#define ZDCHAT_LIVE_KEY @"" // dev

//*****RATHEESH*****//
#ifdef DEBUG
#define API_BASE_URL @"api/v1/"
#define BASE_URL @"https://www.tajrummy.com/"
//#define BASE_URL @"https://tr.glserv.info/"
//#define BASE_URL @"http://dev-tr.glserv.info/"
#else
#define API_BASE_URL @"api/v1/"
//#define BASE_URL @"https://tr.glserv.info/"
//#define BASE_URL @"http://dev-tr.glserv.info/"
#define BASE_URL @"https://www.tajrummy.com/"
#endif

#define TIME_OUT_INTERVAL 20.0f
#define LOGIN @"login/"
#define SIGNUP @"signup/"
#define FORGOTPASSWORD @"forgot-password/"
#define ACCOUNT_OVERVIEW @"account-overview/"
#define CHANGE_PASSWORD @"change-password/"
#define KYC_DETAILS @"kyc/"
#define PREFERENCES @"edit-preference/"
#define STATES_LIST @"states-list/"
#define GET_PROFILE @"profile-overview/"
#define EDIT_PROFILE @"edit-profile/"
#define REFER_FRIEND @"refer-a-friend/"
#define CHANGE_EMAIL @"change-email/"
#define CHANGE_MOBILE @"change-mobile/"
#define MOBILE_VERIFY @"mobile-verify/"
#define GET_OTP @"mobile-otp-verify/"
#define FLOWBACK @"flow-back/"
#define PROMOTIONS @"current-promotions/"
#define WITHDRAWCASH @"withdraw/"
#define ADD_ACCOUNT @"add-account/"
#define DELETE_ACCOUNT @"del-account/"
#define SUPPORT @"support/"
#define INVITE @"refer-a-friend/"
#define BLOCKED_STATES_LIST @"get-blocked-states/"
#define RESEND @"resend-email-verification/"
#define SOCIAL_LOGIN_CHECK @"social-login-check/"
#define RELOAD_FUNCHIPS @"reload-funchips/"
#define ACCOUNT_MANAGER @"account-manger-details/"
#define SENDPAYMENTREQUEST @"sendpaymentrequest"
#define REPORTBUG @"bug-report/"
#define DEPOSIT_CHECK @"player-deposit-check/"
#define BONUS_CHECK @"bonuscode-check/"
#define PAYMENT_REQUEST @"sendpaymentrequest/"
#define ORDER_DETAILS @"get-order-status/"
#define DEPOSIT_HISTORY @"rummy-deposit-history/"
#define WITHDRAW_HISTORY @"rummy-withdrawal-history/"
#define BONUS_HISTORY @"rummy-bonus-history/"
#define RECONCILE_REPORTS @"rummy-reconsile/"
#define GAME_LOGS @"rummy-gamelogs-history/"
#define RFR_LOGS @"referlogs/"
#define WITHDRAW_OTP_VERIFY @"withdraw-otp-verify/"
#define APP_DOWNLOAD_ENTRY @"app-download-entry/"


//statis pages
#define ABOUTUS @"about-us/"
#define LEGALITY @"rummy-legality/"
#define PRIVACY @"privacy-policy/"
#define HOWTOPLAY @"how-to-play-content/"
#define TERMS @"rummy-terms/"
#define OVERVIEW_INFO @"withdrawable-balance-content/"

#define PHONE_NO_VALIDATION_CHARACTER_SET @"0123456789"
static const NSInteger EmailAddressCharacterLimit = 50;
static NSString *const RegexEmail = @"\\b[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\\.[a-zA-Z]{2,4}\\b";
#define USERNAME_VALIDATION_CHARACTER_SET @" ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789_@."
#define PANCARD_VALIDATION_CHARACTER_SET @"ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"
static const NSInteger PasswordMinLength = 5;
static const NSInteger UserNameMinLength = 4;
static const NSInteger PasswordLength = 15;
static NSString *const RegexPhoneNumber = @"[0-9]{10}";

#define AlertTitle_authorizationLocationDenied  "Location Authorisation Denied"
#define AlertTitle_authorizationLocationRestricted "Location Restricted"
#define AlertTitle_authorizationLocationDisabled   "Location Service disabled"

#define AlertMessage_authorizationLocationDisabled  @"Needs access to your current location.Please turn on location service in your device settings."
#define AlertMessage_authorizationLocationRestricted @"is restricted from using location services.Please go to Settings and enable the Location for this app to use this feature to restric the user don't play in judiciary areas."
#define AlertMessage_authorizationLocationDenied "level location permission settings has been denied. Please go to Settings and enable the Location for this app to use this feature to restric the user don't play in judiciary areas."

#define location_settings @"Settings"
#define location_cancel @"Cancel"

//*****RATHEESH****//

#endif
