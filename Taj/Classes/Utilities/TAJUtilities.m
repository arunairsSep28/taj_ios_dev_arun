/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJUtilities.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 15/01/14.
 **/

#import "TAJXMLParserDictionary.h"
#import "TAJUtilities.h"
#import "Reachability.h"
#import "TAJGameEngine.h"


#define iPhone5 ([UIScreen instancesRespondToSelector:@selector(currentMode)] ? \
                CGSizeEqualToSize(CGSizeMake(640,1136), [[UIScreen mainScreen] currentMode].size) : NO)

static TAJUtilities *sSharedUtilities = nil;
static NSString *const kUserDefaultsServiceName = @"com.tajrummy";

@implementation TAJUtilities

@synthesize activity = _activity;

+(id) sharedUtilities
{
    if (sSharedUtilities == nil)
    {
        sSharedUtilities = [[TAJUtilities alloc] init];
    }
    return sSharedUtilities;
}

-(id) init
{
    self = [super init];
    if (self)
    {
        self.activity = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhite];
        self.reachability = Nil;
        self.reachability = [Reachability reachabilityForInternetConnection];
        [self.reachability startNotifier];
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(connectionChanges) name:kReachabilityChangedNotification object:Nil];
    }
    return self;
}

-(int) stringLength:(const char*)string
{
    int count = 1;
    char *apiPointer = NULL;
    apiPointer = (char *)string;
    while (*apiPointer != '\0')
    {
        count++;
        apiPointer = apiPointer+1;
    }
    return count;
}

-(id) getTheUUID
{
    CFUUIDRef udid = CFUUIDCreate(NULL);
    NSString *udidString = (NSString *) CFBridgingRelease(CFUUIDCreateString(NULL, udid));
    return udidString;
}

-(NSString *) getDeviceType {
    if ([TAJUtilities isIPhone])
    {
        return @"Mobile";//@"IPHONE";
    }
    else
    {
        return @"Tablet";//@"IPAD";
    }
}

-(void) writeToTextFile :(NSData*)data requestType:(BOOL)isRequest;
{
    NSDictionary *dictionary = [NSDictionary dictionaryWithXMLData:data];
    //get the documents directory:
    NSArray *paths = NSSearchPathForDirectoriesInDomains
    (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    //make a file name to write the data to using the documents directory:
    NSString *fileName = [NSString stringWithFormat:@"%@/data.txt",
                          documentsDirectory];
    
    //create content - four lines of text
    NSString *content = NULL;
    NSString *newData = NULL;
    NSError* error = nil;
    NSStringEncoding encoding = 0;
    content = [NSString stringWithContentsOfFile:fileName
                                    usedEncoding:&encoding
                                           error:&error];
    
    NSDateFormatter *formatter = Nil;
    NSString        *dateString = Nil;
    
    formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm:ss"];
    
    dateString = [formatter stringFromDate:[NSDate date]];

    if (!content)
    {
        content = [dictionary description];
        //content = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
    }
    else
    {
        newData = [dictionary description];
        //newData = [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding];
        if (isRequest)
        {
            content = [content stringByAppendingString:@"\n************* Request *****\n"];
            content = [content stringByAppendingString:dateString];
        }
        else
        {
            content = [content stringByAppendingString:@"\n******** Reply **********\n"];
            content = [content stringByAppendingString:dateString];
        }
        content = [content stringByAppendingString:newData];
    }
    
    //save content to the documents directory
    [content writeToFile:fileName
              atomically:NO
                encoding:NSStringEncodingConversionAllowLossy
                   error:nil];
    
}

-(void)showAlert:(NSString*)message
{
    UIAlertView *view = [[UIAlertView alloc] initWithTitle:ALERT_EMPTY_TITLE
                                                   message:message
                                                  delegate:self
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil];
    [view show];
}

#pragma mark - User Defaults-

+ (void)saveToUserDefaults:(NSObject *)object forKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	
    if (userDefaults)
    {
		[userDefaults setObject:object forKey:[NSString stringWithFormat:@"%@.%@", kUserDefaultsServiceName, key]];
		[userDefaults synchronize];
	}
}

+ (NSObject *)retrieveFromUserDefaultsForKey:(NSString *)key
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
	NSObject *object = nil;
	
    if (userDefaults)
		object = [userDefaults objectForKey:[NSString stringWithFormat:@"%@.%@", kUserDefaultsServiceName, key]];
    
	return object;
}

+ (void)deleteSavedUserDefaultsForKey:(NSString *)key
{
    [self saveToUserDefaults:nil forKey:[NSString stringWithFormat:@"%@.%@", kUserDefaultsServiceName, key]];
}

+ (BOOL)isIPhone//ReDIM Changes
{
//#ifdef UI_USER_INTERFACE_IDIOM
    return (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone);
//#else
//    return NO;
//#endif
    
}

+ (BOOL)isIOSGreaterThan6
{
    NSArray *iosVersion = [[UIDevice currentDevice].systemVersion componentsSeparatedByString:@"."];
    
    if ([[iosVersion objectAtIndex:0] intValue] >= 6)
    {
        // iOS-6 code[current] or greater
        return YES;
    }
    return NO;
}

+ (BOOL)isItIPhone5
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (MAX(screenSize.height, screenSize.width) == 568);
}
+ (BOOL)isItIPhone6
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (MAX(screenSize.height, screenSize.width) == 667);
}
+ (BOOL)isItIPhonex
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (MAX(screenSize.height, screenSize.width) == 812);
}
+ (BOOL)isItIPhoneXSMax
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (MAX(screenSize.height, screenSize.width) == 896);
}

+ (BOOL)isItIPhone6Plus
{
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (MAX(screenSize.height, screenSize.width) == 736);
}

+(BOOL)isItiPad3Gen {
    CGSize screenSize = [[UIScreen mainScreen] bounds].size;
    return (MAX(screenSize.height, screenSize.width) == 704);
}

+ (CGFloat)screenWidth
{
    return ([UIScreen mainScreen].bounds.size.width);
}
+ (CGFloat)screenHeight
{
    return ([UIScreen mainScreen].bounds.size.height);
}

-(void) showLoginActivity
{
    [self.activity startAnimating];
}

-(void) dismissLoginActivity
{
    [self.activity stopAnimating];
    [self.activity removeFromSuperview];
    [[NSNotificationCenter defaultCenter] postNotificationName:REMOVE_STORE_CREDENTIAL_LOGIN_ACTIVITY object:nil];
}

-(BOOL) isInternetConnected
{
    NetworkStatus networkStatus = [self.reachability currentReachabilityStatus];
    return networkStatus != NotReachable;
}

-(void) showNOInternetAlert
{
//    UIAlertView *alert = [[UIAlertView alloc]initWithTitle:[@"Util" stringByAppendingString:NO_INTERNET] message:PLEASE_CHECK_YOUR_INTERNET_CONNECTION delegate:self cancelButtonTitle:OK_STRING otherButtonTitles:nil];
//    [alert show];
}

-(void) connectionChanges
{
    NetworkStatus networkStatus = [self.reachability currentReachabilityStatus];
    
    if (networkStatus == NotReachable)
    {
        //TODO : Handle the not reachable status of the connection
        
    }
}

+ (NSString *)checkIfStringIsEmpty:(NSString *)str
{
    if ([str isKindOfClass:[NSNull class]]) {
        return @"";
    }
    if (str.length == 0 || [str isKindOfClass:[NSNull class]] || [str isEqualToString:@""] || [str isEqualToString:@"(null)"] || str == nil || [str isEqualToString:@"<null>"]) {
        return @"";
    }
    return str;
}

-(NSString *)getProperNameForTableType:(NSString*)type
{
    NSString *properType = Nil;
    if ([type compare:TABLE_TYPE_101_POOL] == NSOrderedSame)
    {
        properType = TABLE_TYPE_101_POOL_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_201_POOL] == NSOrderedSame)
    {
        properType = TABLE_TYPE_201_POOL_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_BEST_OF_2] == NSOrderedSame)
    {
        properType = TABLE_TYPE_BEST_OF_2_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_BEST_OF_3] == NSOrderedSame)
    {
        properType = TABLE_TYPE_BEST_OF_3_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_BEST_OF_6] == NSOrderedSame)
    {
        properType = TABLE_TYPE_BEST_OF_6_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_PR_JOKER] == NSOrderedSame)
    {
        properType = TABLE_TYPE_PR_JOKER_DISPLAY_STRING;
    }
    if ([type compare:TABLE_TYPE_PR_NO_JOKER] == NSOrderedSame)
    {
        properType = TABLE_TYPE_PR_NO_JOKER_DISPLAY_STRING;
    }
    return properType;
}

- (TAJProgressAlertView *)createProgressInstance
{
    int index = 0;
    
    NSArray  *nibContents = [[NSBundle mainBundle] loadNibNamed:TAJProgressAlertViewNibName
                                                owner:nil
                                              options:nil];
    
    TAJProgressAlertView *alert = nibContents[index];
    
    return alert;
}

+(NSMutableArray*)getFilteredArrayForDictionary:(NSDictionary*)dictionary withGameType:(GameTypeStruct) gameType withPlayers:(PlayerTypeStruct) playerType withBetType:(BetTypeStruct) betType withTableCost:(TableTypeStruct) tableCost withViewType:(ViewTypeEnum) viewType
{
    TAJGameEngine *engine = Nil;
    engine = [TAJGameEngine sharedGameEngine];
    
    NSMutableArray *arrayOfTables = Nil;
    arrayOfTables = [dictionary valueForKeyPath:TABLE_GAMELIST_DATA];
    NSPredicate *predicateForGameCost = Nil;
    NSPredicate *predicateForViewType = Nil;
    //tableCost.isCash = NO; //ReDIM Changes Commented to enable cash selection
    //Filter the array based on watch or play type
    if (arrayOfTables)
    {
        NSString *viewTypeValue = Nil;
        if (viewType == eWatchGame)
        {
            viewTypeValue = viewType? @"True":@"False";
            predicateForViewType = [NSPredicate predicateWithFormat:FILTER_FOR_WATCH_TYPE,viewTypeValue];
            arrayOfTables = [NSMutableArray arrayWithArray:[arrayOfTables filteredArrayUsingPredicate:predicateForViewType]];
        }
    }
    
    //Filter the array based on free or cash type
    if (arrayOfTables)
    {
#if LIVE_RELEASE
        predicateForGameCost = [NSPredicate predicateWithFormat:@"TAJ_table_cost == %@",@"FUNCHIPS_FUNCHIPS"];
        arrayOfTables = [NSMutableArray arrayWithArray:[arrayOfTables filteredArrayUsingPredicate:predicateForGameCost]];
#else
        predicateForGameCost = [NSPredicate predicateWithFormat:FILTER_FOR_GAME_COST_BOTH,@"FUNCHIPS_FUNCHIPS",@"CASH_CASH"];
        arrayOfTables = [NSMutableArray arrayWithArray:[arrayOfTables filteredArrayUsingPredicate:predicateForGameCost]];

#endif
        NSString *predicateFormat = @"";

        
        
 #if LIVE_RELEASE
        bool applyOrFilter = false;
        bool isOred = false;
        if (tableCost.isFree)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_GAME_COST_IN_LOBBY,@"\"FUNCHIPS_FUNCHIPS\""]];
            
            applyOrFilter = true;
        }


#else  
        bool applyOrFilter = false;
        bool isOred = false;
        if (tableCost.isFree)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_GAME_COST_IN_LOBBY,@"\"FUNCHIPS_FUNCHIPS\""]];
            
            applyOrFilter = true;
        }

        if (tableCost.isCash)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_GAME_COST_IN_LOBBY,@"\"CASH_CASH\""]];
            
            applyOrFilter = true;
        }
        
        if (tableCost.isVIP)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_VIP_GAME_COST_IN_LOBBY]];
            
            applyOrFilter = true;
        }
        
        if (tableCost.isBlaze)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BLAZE_GAME_COST_IN_LOBBY]];
            
            applyOrFilter = true;
        }
        
        if (tableCost.isHappy)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_HAPPY_GAME_COST_IN_LOBBY]];
            
            applyOrFilter = true;
        }
 #endif
        if (isOred)
        {
            predicateFormat = [@"(" stringByAppendingString:predicateFormat];
            predicateFormat = [predicateFormat stringByAppendingString:@")"];
        }

        applyOrFilter = false;
        

            predicateForGameCost = [NSPredicate predicateWithFormat:predicateFormat];

        arrayOfTables = [NSMutableArray arrayWithArray:[arrayOfTables filteredArrayUsingPredicate:predicateForGameCost]];
    }
    
    if (arrayOfTables)
    {
        NSString *predicateFormat = @"";
        bool applyOrFilter = false;
        bool isOred = false;
        
        if (gameType.isOneNotOne && betType.isLowBet)
        {
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_101_POOL_LOW_BET,[engine.usermodel.poolLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isOneNotOne && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_101_POOL_MEDIUM_BET,[engine.usermodel.poolLowBet integerValue], [engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isOneNotOne && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_101_POOL_HIGH_BET,[engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isTwoNotOne && betType.isLowBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_201_POOL_LOW_BET,[engine.usermodel.poolLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isTwoNotOne && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_201_POOL_MEDIUM_BET,[engine.usermodel.poolLowBet integerValue], [engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isTwoNotOne && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_201_POOL_HIGH_BET,[engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfThree && betType.isLowBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_3_LOW_BET,[engine.usermodel.poolLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfThree && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_3_MEDIUM_BET,[engine.usermodel.poolLowBet integerValue], [engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfThree && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_3_HIGH_BET,[engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfTwo && betType.isLowBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_2_LOW_BET,[engine.usermodel.poolLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfTwo && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_2_MEDIUM_BET,[engine.usermodel.poolLowBet integerValue], [engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfTwo && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_2_HIGH_BET,[engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfSix && betType.isLowBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_6_LOW_BET,[engine.usermodel.poolLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfSix && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_6_MEDIUM_BET,[engine.usermodel.poolLowBet integerValue], [engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isBestOfSix && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_BEST_OF_6_HIGH_BET,[engine.usermodel.poolMediumBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isJoker && betType.isLowBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_PR_JOKER_LOW_BET,[engine.usermodel.prLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isJoker && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_PR_JOKER_MEDIUM_BET,[engine.usermodel.prLowBet integerValue], [engine.usermodel.prMediumbet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isJoker && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_PR_JOKER_HIGH_BET,[engine.usermodel.prMediumbet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isNoJoker && betType.isLowBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_PR_NOJOKER_LOW_BET,[engine.usermodel.prLowBet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isNoJoker && betType.isMediumBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_PR_NOJOKER_MEDIUM_BET,[engine.usermodel.prLowBet integerValue], [engine.usermodel.prMediumbet integerValue]]];
            applyOrFilter = true;
        }
        
        if (gameType.isNoJoker && betType.isHighBet)
        {
            if (applyOrFilter)
            {
                isOred = true;
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:[NSString stringWithFormat:FILTER_FOR_PR_NOJOKER_HIGH_BET,[engine.usermodel.prMediumbet integerValue]]];
            applyOrFilter = true;
        }
        
        if (isOred)
        {
            predicateFormat = [@"(" stringByAppendingString:predicateFormat];
            predicateFormat = [predicateFormat stringByAppendingString:@")"];
        }
   if(predicateFormat.length > 0)
   {
       predicateFormat = [predicateFormat stringByAppendingString:AND_FILTERS];
       applyOrFilter = false;
   }
      
        
        if (playerType.isTwoPlayer)
        {
            NSString *twoPlayerPredicate = Nil;
            twoPlayerPredicate = [@"("  stringByAppendingString:FILTER_FOR_2_PLAYERS];
            predicateFormat = [predicateFormat stringByAppendingString:twoPlayerPredicate];
            applyOrFilter = true;
        }
        
        if (playerType.isSixPlayer)
        {
            NSString *sixPlayerPredicate = Nil;
            sixPlayerPredicate = [@"" stringByAppendingString:FILTER_FOR_6_PLAYERS];
            
            if (applyOrFilter)
            {
                
                predicateFormat = [predicateFormat stringByAppendingString:OR_FILTERS];
            }
            else
            {
                sixPlayerPredicate = [@"(" stringByAppendingString:FILTER_FOR_6_PLAYERS];
            }
            predicateFormat = [predicateFormat stringByAppendingString:sixPlayerPredicate];
        }
    if(predicateFormat.length > 0)
    {
         predicateFormat = [predicateFormat stringByAppendingString:@")"];
        NSPredicate *predicate = [NSPredicate predicateWithFormat:predicateFormat];
        
        arrayOfTables = [NSMutableArray arrayWithArray:[arrayOfTables filteredArrayUsingPredicate:predicate]];

    }
        
    
        
    }
    
    
    return arrayOfTables;
}

- (NSMutableArray*)sortTablesArraybyCurrentPlayerAndGameStart:(NSMutableArray*)goalArray
{
    NSArray *sortedArray = [goalArray sortedArrayUsingComparator:^NSComparisonResult(NSDictionary *firstObject, NSDictionary *secondObject)
                            {
                                NSString *currentPlayersFirstObject = [firstObject objectForKey:TAJ_CURRENT_PLAYER];
                                NSString *currentPlayersSecondObject = [secondObject objectForKey:TAJ_CURRENT_PLAYER];
                                NSString *firstObjectGameSchedule = [firstObject objectForKey:TAJ_GAME_SCHEDULE];
                                NSString *secondObjectGameSchedule = [secondObject objectForKey:TAJ_GAME_SCHEDULE];
                                
                                NSString *firstObjectGameStart = [firstObject objectForKey:TAJ_GAME_START];
                                NSString *secondObjectGameStart = [secondObject objectForKey:TAJ_GAME_START];
                                
                                
                                
                                if ([firstObjectGameStart compare:FALSE_VALUE] == NSOrderedSame && [secondObjectGameStart compare:FALSE_VALUE] == NSOrderedSame )
                                {
                                    if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                    {
                                        ;
                                    }
                                    else if([currentPlayersFirstObject intValue] == 0)
                                    {
                                        return NSOrderedDescending;
                                    }
                                    else if([currentPlayersSecondObject intValue] == 0)
                                    {

                                        return NSOrderedAscending;
                                    }
                                    
                                }
                                else if ([firstObjectGameStart compare:FALSE_VALUE] == NSOrderedSame)
                                {
                                    if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                    {
                                        ;
                                    }
                                    else if([currentPlayersFirstObject intValue] == 0)
                                    {

                                        return NSOrderedDescending;
                                    }
                                    else
                                    {

                                        return NSOrderedAscending;
                                    }
                                }
                                else if ([secondObjectGameStart compare:FALSE_VALUE] == NSOrderedSame)
                                {
                                    if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                    {
                                        ;
                                    }
                                    else if([currentPlayersSecondObject intValue] == 0)
                                    {

                                        return NSOrderedAscending;
                                    }
                                    else
                                    {

                                        return NSOrderedDescending;
                                    }
                                }
                                
                               
                                
                                if ([firstObjectGameSchedule compare:TRUE_VALUE] == NSOrderedSame)
                                {
                                    if ([secondObjectGameSchedule compare:TRUE_VALUE] == NSOrderedSame)
                                    {
                                        if ([firstObjectGameStart compare:FALSE_VALUE] == NSOrderedSame && [secondObjectGameStart compare:FALSE_VALUE] == NSOrderedSame )
                                        {
                                            if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                            {

                                                return NSOrderedSame;
                                            }
                                            else if([currentPlayersFirstObject intValue] == 0)
                                            {

                                                return NSOrderedDescending;
                                            }
                                            else
                                            {

                                                return NSOrderedAscending;
                                            }
                                        }
                                        else if ([firstObjectGameStart compare:FALSE_VALUE] == NSOrderedSame)
                                        {
                                            if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                            {

                                                return NSOrderedDescending;
                                            }
                                            else if([currentPlayersFirstObject intValue] == 0)
                                            {

                                                return NSOrderedDescending;
                                            }
                                            else
                                            {

                                                return NSOrderedAscending;
                                            }
                                        }
                                        else if ([secondObjectGameStart compare:FALSE_VALUE] == NSOrderedSame)
                                        {
                                            if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                            {

                                                return NSOrderedAscending;
                                            }
                                            else if([currentPlayersFirstObject intValue] == 0)
                                            {

                                                return NSOrderedDescending;
                                            }
                                            else
                                            {

                                                return NSOrderedDescending;
                                            }
                                        }
                                        else
                                        {

                                            return NSOrderedSame;
                                        }
                                    }
                                    else
                                    {
                                        if ([currentPlayersSecondObject intValue] == 0)
                                        {

                                            return NSOrderedAscending;
                                        }
                                        else
                                        {

                                            return NSOrderedDescending;
                                        }
                                    }
                                }
                                else
                                {
                                    if ([secondObjectGameSchedule compare:TRUE_VALUE] == NSOrderedSame)
                                    {
                                        if ([currentPlayersFirstObject intValue] == 0)
                                        {

                                            return NSOrderedDescending;
                                        }
                                        else
                                        {

                                            return NSOrderedAscending;
                                        }
                                    }
                                    else
                                    {
                                        //If both is not running then it might be sitting or open
                                        
                                        if ([currentPlayersFirstObject intValue] == 0)
                                        {
                                            if ([currentPlayersSecondObject intValue] == 0)
                                            {

                                                return NSOrderedSame;
                                            }
                                            else
                                            {

                                                return NSOrderedDescending;
                                            }
                                        }
                                        else
                                        {
                                            if ([currentPlayersSecondObject intValue] == 0)
                                            {

                                                return NSOrderedAscending;
                                            }
                                            else
                                            {

                                                if ([firstObjectGameStart compare:FALSE_VALUE] == NSOrderedSame && [secondObjectGameStart compare:FALSE_VALUE] == NSOrderedSame )
                                                {
                                                    if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                                    {
                                                        ;
                                                    }
                                                    else if([currentPlayersFirstObject intValue] == 0)
                                                    {
                                                        return NSOrderedDescending;
                                                    }
                                                    else if([currentPlayersSecondObject intValue] == 0)
                                                    {
                                                        
                                                        return NSOrderedAscending;
                                                    }
                                                    
                                                }
                                                else if ([firstObjectGameStart compare:FALSE_VALUE] == NSOrderedSame)
                                                {
                                                    if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                                    {
                                                        ;
                                                    }
                                                    else if([currentPlayersFirstObject intValue] == 0)
                                                    {
                                                        
                                                        return NSOrderedDescending;
                                                    }
                                                    else
                                                    {
                                                        
                                                        return NSOrderedAscending;
                                                    }
                                                }
                                                else if ([secondObjectGameStart compare:FALSE_VALUE] == NSOrderedSame)
                                                {
                                                    if ([currentPlayersSecondObject intValue] == 0 && [currentPlayersFirstObject intValue] == 0)
                                                    {
                                                        ;
                                                    }
                                                    else if([currentPlayersSecondObject intValue] == 0)
                                                    {
                                                        
                                                        return NSOrderedAscending;
                                                    }
                                                    else
                                                    {
                                                        
                                                        return NSOrderedDescending;
                                                    }
                                                }
                                                else
                                                {
                                                }

                                                return NSOrderedSame;
                                            }
                                        }
                                    }
                                }

                                return NSOrderedSame;
                            }];
    
    NSMutableArray *mutableArray = [NSMutableArray arrayWithArray:sortedArray];
    return mutableArray;
}

-(NSString *)getTextForPopupType:(EPopupType)popupType
{
    NSString *message = Nil;
    switch (popupType)
    {
        case eDrop:
            message = DROP_BUTTON_TITLE;
            break;
            
        case eLastHand:
            message = LAST_HAND_NO_ENTRY;
            break;
            
        case eLeaveTable:
            message = LEAVE_TABLE_MESSAGE;
            break;
            
        case eRebuyin:
            //message = ;
            break;
            
        case eRejoin:
            //message = REJOIN_TAG;
            break;
            
        case eSearchJoin:
            //message = SEARCH_JOIN_TABLE;
            break;
            
        case eShow:
            message = SHOW_BUTTON_TITLE;
            break;
            
        case eSplitReject:
            //message = SPLIT_RESULT;
            break;
            
        case eSplitRequest:
            message = SPLIT_REQUESTED_MSG;
            break;
            
        default:
            message = @"No Such Popup Type";
            break;
    }
    return message;
}
@end
