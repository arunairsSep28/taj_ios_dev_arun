/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJConfigProvider.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 09/01/14.
 **/

#ifndef Taj_TAJConfigProvider_h
#define Taj_TAJConfigProvider_h

#define API_CHARACTERS_LIMIT 2048
//                          <authrep user_id="None" player_in="new_lobby" password="None" session_id="" system="TajRummy" msg_uuid="F103A1A6-2C28-F50E-E9E4-D88B0699E914" DEVICE_ID="Tablet" flash="4" version_api="v2" device_type="Tablet", client_type="IOS" os="IOS"/>
//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\"  flash=\"4\" version_api=\"v2\" device_type=\"%s\", client_type=\"IOS\" os=\"IOS\" siteid=\"4\"/>";
//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"None\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\" device_type=\"%s\" client_type=\"IOS\" os=\"IOS\" siteid=\"4\"/>";

//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"None\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\" device_type=\"%s\" client_type=\"IOS\" os=\"IOS\" flash=\"6\" siteid=\"6\"/>";

    //NSString * const kLoginAPI = @"<authrep user_id=\"None\" player_in=\"new_lobby\" password=\"None\" session_id=\"%s\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\" device_type=\"%s\" client_type=\"IOS\" os=\"IOS\" flash=\"1\" siteid=\"1\"/>"; - old tr

//present using
NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"%s\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\" device_type=\"%s\" client_type=\"IOS\" os=\"IOS\" flash=\"1\" siteid=\"1\" version_api=\"v2\"/>";

//flash type - for testing purpose added
//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"%s\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"Desktop\" flash=\"1\" version_api=\"v2\" device_type=\"Desktop\" client_type=\"Flash\" os=\"OS\"/>";

//test Andriod
//NSString * const kLoginAPI = @"<authrep user_id=\"wildbunny\" player_in=\"new_lobby\" password=\"12345\" session_id=\"\" system=\"TajRummy\" msg_uuid=\"F103A1A6-2C28-F50E-E9E4-D88B0699E914\" DEVICE_ID=\"FLASH\" flash=\"1\" version_api=\"v2\"/>";

//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"%s\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\" device_type=\"%s\" client_type=\"IOS\" os=\"IOS\" flash=\"1\" version_api=\"v2\" siteid=\"1\"/>";

//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" password=\"%s\" session_id=\"None\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\"/>";

//NSString * const kLoginAPI = @"<authrep user_id=\"%s\" player_in=\"new_lobby\" password=\"%s\" session_id=\"None\" system=\"TajRummy\" msg_uuid=\"%s\" DEVICE_ID=\"%s\" version_api=\"v1\"/>";

NSString * const kQuitTableReply = @"<reply code=\"200\" data=\"join_table\" msg_uuid=\"FB28837A-AD48-AE1B-AA53-F974A3309CA9\" timestamp=\"1352167428.0\" type=\"+OK\" />";
NSString * const kJoinTableAsView = @"<request command=\"join_table\" table_id=\"%s\" msg_uuid=\"%s\" table_join_as=\"view\" table_type=\"%s\" table_cost=\"%s\" buyinamount=\"%s\"  seat=\"0\" char_no=\"1\"/>";
NSString * const kJoinTableAsPlay = @"<request command=\"join_table\" table_id=\"%s\" msg_uuid=\"%s\" table_join_as=\"play\" table_type=\"%s\" table_cost=\"%s\" buyinamount=\"%s\"  seat=\"%d\" char_no=\"1\"/>";
NSString * const kJoinTableAsMiddle = @"<request command=\"join_table\" table_id=\"%s\" msg_uuid=\"%s\" table_join_as=\"middle\" table_type=\"%s\" table_cost=\"%s\" buyinamount=\"%s\"  seat=\"%d\" char_no=\"1\"/>";

NSString * const kChipReload = @"<reply code=\"200\" data=\"chipreload\" funchips=\"5000\" msg_uuid=\"9652BC67-DF40-1591-25E1-00F06FC7C041\" timestamp=\"1352167354.0\" type=\"+OK\"  />";
NSString * const kFunChipsReload = @"<request command=\"chipreload\" msg_uuid=\"%s\"/>";

NSString * const kTableDetails = @"<request command=\"get_table_details\" msg_uuid=\"C3F47C85-F20F-D884-41F1-656B536060F4\" table_id=\"211\"/>";
NSString * const kListTable = @"<request command=\"list_gamesettings\" msg_uuid=\"%s\" />";
//NSString * const kListTable = @"<request command=\"list_gaxxmesettings\" msg_uuid=\"%s\" />";
NSString * const kGetTableDetails = @"<request command=\"get_table_details\" msg_uuid=\"%s\" table_id=\"%s\" />";
NSString * const kHeartBeat = @"<event event_name=\"HEART_BEAT\" code=\"+OK\" nick_name=\"%s\" />";
NSString * const kHeartBeatWithSlotInfo = @"<event event_name=\"HEART_BEAT\" nick_name=\"%s\"> %s </event>";
NSString * const kLogout = @"<request command=\"logout\" msg_uuid=\"%s\" />";
NSString * const kQuitTable = @"<request command=\"quit_table\" table_id=\"%s\" msg_uuid=\"%s\" />";
NSString * const kCardPick = @"<event event_name=\"CARD_PICK\" nick_name=\"%s\" face=\"%s\" msg_uuid=\"%s\" stack=\"%s\" suit=\"%s\" table_id=\"%s\" user_id=\"%s\" />";
NSString * const kCardDiscard = @"<event event_name=\"CARD_DISCARD\" face=\"%s\" nick_name=\"%s\" msg_uuid=\"%s\" suit=\"%s\" table_id=\"%s\"  user_id=\"%s\" card_sending=\"player\" />";
NSString * const kCardAutoDiscard = @"<event event_name=\"CARD_DISCARD\" face=\"%s\" nick_name=\"%s\" msg_uuid=\"%s\" suit=\"%s\" table_id=\"%s\"  user_id=\"%s\" card_sending=\"flash\" />";

NSString * const kCardsMeld = @"<request command=\"meld\" suit=\"%s\" face=\"%s\" table_id=\"%s\" msg_uuid=\"%s\"> %s </request>" ;
NSString * const kCardsCheckMeld = @"<request command=\"check_meld\" suit=\"%s\" face=\"%s\" table_id=\"%s\" msg_uuid=\"%s\"> %s </request>" ;
NSString * const kShow = @"<event event_name=\"SHOW\" suit=\"%s\" face=\"%s\" table_id=\"%s\" msg_uuid=\"%s\" user_id=\"%s\" nick_name=\"%s\" />";
NSString * const kTurnExtraTime = @"<request command=\"extratime\" table_id=\"%s\" msg_uuid=\"%s\" user_id=\"%s\" nick_name=\"%s\" />";
NSString * const kRejoinReplyYes = @"<reply type=\"+OK\" msg_uuid=\"%s\" user_id=\"%s\" nick_name=\"%s\" table_id=\"%s\" />";
NSString * const kSmartCorrection = @"<reply command=\"wrong_meld_correction\" nick_name=\"%s\" user_id=\"%s\" agree=\"%s\" type=\"+OK\" text=\"200\" table_id=\"%s\" game_id=\"%s\" msg_uuid=\"%s\" />";
NSString * const kRejoinReplyNo = @"<reply type=\"-ERR\" code=\"909\" msg_uuid=\"%s\" user_id=\"%s\" nick_name=\"%s\" table_id=\"%s\" />";
NSString * const kChatMessage = @"<event event_name = \"CHAT_MSG\" user_id = \"%s\" table_id = \"%s\" nick_name = \"%s\" text = \"%s\" />";
NSString * const kSplitRequest = @"<request command=\"split\"  table_id=\"%s\" msg_uuid=\"%s\"/>";

NSString * const kAddToFavourite = @"<request command=\"favorite_game\" set=\"1\" table_id=\"%s\" user_id=\"%s\" nick_name=\"%s\" table_cost=\"%s\" msg_uuid=\"%s\"/>";

NSString * const kRemoveFromFavourite = @"<request command=\"favorite_game\" set=\"0\" table_id=\"%s\" user_id=\"%s\" nick_name=\"%s\" table_cost=\"%s\" msg_uuid=\"%s\"/>";

NSString * const kSplitAcceptReply = @"<reply code=\"200\" table_id=\"%s\" user_id=\"%s\" nick_name=\"%s\" data=\"split\" msg_uuid=\"%s\" type=\"+OK\"/>";
NSString * const kSplitRejectReply = @"<reply code=\"701\" table_id=\"%s\" user_id=\"%s\" nick_name=\"%s\" data=\"split\" msg_uuid=\"%s\" type=\"-ERR\"/>";
NSString * const kDropEvent = @"<event event_name=\"PLAYER_DROP\" nick_name=\"%s\" msg_uuid=\"%s\" table_id=\"%s\" user_id=\"%s\" />";
NSString * const kSearchJoinTable = @"<request command=\"search_join_table\" msg_uuid=\"%s\" user_id=\"%s\" nick_name=\"%s\" bet=\"%s\" maxplayers=\"%s\" table_type=\"%s\" table_cost=\"%s\" table_id=\"%s\" conversion=\"%s\" stream_name=\"%s\" stream_id=\"%s\" gamesettings_id=\"%s\" />";
NSString * const kCardsMeldForOtherPlayer = @"<reply type=\"+OK\" text=\"200\" table_id=\"%s\" msg_uuid=\"%s\"> %s </reply>";
NSString * const kGetTableExtra = @"<request command=\"get_table_extra\" msg_uuid=\"%s\" table_id=\"%s\"/>";
NSString * const kIAmBack = @"<request command=\"autoplaystatus\" msg_uuid=\"%s\" user_id=\"%s\"/>";
NSString * const kRebuyin = @"<request command=\"rebuyin\" table_id=\"%s\" msg_uuid=\"%s\" user_id=\"%s\" rebuyinamt=\"%s\" />";
NSString * const kTourneyRebuyin = @"<request command=\"tournament_rebuyin\" amount=\"%s\" msg_uuid=\"%s\" level=\"%s\" tournament_id=\"%s\" />";
NSString * const kPlayerCount = @"<request command=\"player_count\" msg_uuid=\"%s\" />";
NSString * const kTableLength = @"<request command=\"table_length\" msg_uuid=\"%s\" />";
NSString * const kAutoPlayCount = @"<request command=\"autoplaycount\" msg_uuid=\"%s\" table_id=\"%s\"/>";
NSString * const kStandUpAPI = @"<request command=\"standup\" table_id=\"%s\" msg_uuid=\"%s\" table_join_as=\"view\" table_type=\"%s\" table_cost=\"%s\" buyinamount=\"%s\" seat=\"%s\" char_no=\"0\" />";
NSString * const kReportABug = @"<event event_name=\"REPORT_BUG\" table_id=\"%s\" game_id=\"%s\" bugexplanation = \"%s\" msg_uuid=\"%s\" game_type=\"%s\" bug_type=\"%s\"/>";

NSString * const kTournamentListTable = @"<request command=\"list_tournaments\" msg_uuid=\"%s\" />";
NSString * const kGetTournamentDetails = @"<request command=\"get_tournament_details\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kGetPrizeList = @"<request command=\"get_prize_list\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kGetLevelTimer = @"<request command=\"get_level_timer\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kTournamentTables = @"<request command=\"get_tournament_tables\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kTournamentRegister= @"<request command=\"register_tournament\" tournament_id=\"%s\" level=\"%s\" player_amount=\"%s\" msg_uuid=\"%s\" vipcode=\"None\"/>";
NSString * const kTournamentDeregister= @"<request command=\"deregister_tournament\" tournament_id=\"%s\" level=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kGetTournamentWaitList = @"<request command=\"tournament_wait_list\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kGetTournamentLeaderBoard = @"<request command=\"leader_board\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kGetTournamentRegisteredPlayer = @"<request command=\"get_registered_player\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kGetPrizeDistribution = @"<request command=\"prize_distribution\" tournament_id=\"%s\" msg_uuid=\"%s\"/>";
NSString * const kLeaveTournament = @"<request command=\"leave_tournament\" tournament_id=\"%s\" msg_uuid=\"%s\" timestamp=\"1352167354.0\"/>";
NSString * const kLevelTopPlayer = @"<request command=\"level_top_player\" tournament_id=\"%s\" msg_uuid=\"%s\" level=\"%s\"/>";
NSString * const kRequestJoinTableReply = @"<reply type=\"+OK\" code=\"200\" msg_uuid=\"%s\" />";
#endif


NSUInteger DeviceSystemMajorVersion();


NSUInteger DeviceSystemMajorVersion() {
    static NSUInteger _deviceSystemMajorVersion = -1;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        _deviceSystemMajorVersion = [[[[[UIDevice currentDevice] systemVersion] componentsSeparatedByString:@"."] objectAtIndex:0] intValue];
	});
    
	return _deviceSystemMajorVersion;
}
