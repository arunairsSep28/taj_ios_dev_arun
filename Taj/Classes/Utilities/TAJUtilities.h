/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJUtilities.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Raghavendra Kamat on 15/01/14.
 **/

#import <Foundation/Foundation.h>
#import "TAJConstants.h"
#import "TAJProgressAlertView.h"
#import "Reachability.h"

@interface TAJUtilities : NSObject{
    
    
}

@property(retain,nonatomic) UIActivityIndicatorView *activity;
@property(nonatomic, strong) Reachability *reachability;
//@property(nonatomic, strong) NSMutableArray *chat;


+(id) sharedUtilities;
-(id) init;
-(int) stringLength:(const char*)string;
-(id) getTheUUID;
-(void) writeToTextFile : (NSData*)data requestType:(BOOL)isRequest;
-(void)showAlert:(NSString*)message;
+ (BOOL)isIOSGreaterThan6;
+ (BOOL)isIPhone;
+ (BOOL)isItIPhone5;
+ (BOOL)isItIPhone6;
+ (BOOL)isItIPhonex;
+ (BOOL)isItIPhoneXSMax;
+ (BOOL)isItIPhone6Plus;
+(BOOL)isItiPad3Gen;
-(NSString *)getDeviceType;

-(void) showLoginActivity;
-(void) dismissLoginActivity;
-(BOOL) isInternetConnected;
-(void) showNOInternetAlert;
-(NSString *)getProperNameForTableType:(NSString*)type;
-(NSString *)getTextForPopupType:(EPopupType)popupType;
-(void) connectionChanges;


- (TAJProgressAlertView *)createProgressInstance;

+(NSMutableArray*)getFilteredArrayForDictionary:(NSDictionary*)dictionary withGameType:(GameTypeStruct) gameType withPlayers:(PlayerTypeStruct) playerType withBetType:(BetTypeStruct) betType withTableCost:(TableTypeStruct) tableCost withViewType:(ViewTypeEnum) viewType;
-(NSMutableArray*)sortTablesArraybyCurrentPlayerAndGameStart:(NSMutableArray*)goalArray;

//user defaults
+ (void)saveToUserDefaults:(NSObject *)object forKey:(NSString *)key;
+ (NSObject *)retrieveFromUserDefaultsForKey:(NSString *)key;
+ (void)deleteSavedUserDefaultsForKey:(NSString *)key;

+ (NSString *)checkIfStringIsEmpty:(NSString *)str;

+ (CGFloat)screenWidth;
+ (CGFloat)screenHeight;


@end
