/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJProgressAlertView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 30/01/14.
 **/

#import <UIKit/UIKit.h>

@interface TAJProgressAlertView : UIView

- (void)showMessage:(NSString *)message;
- (void)show:(BOOL)isPortraitMode;
- (void)hide;

@end