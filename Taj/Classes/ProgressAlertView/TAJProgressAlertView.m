/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJProgressAlertView.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	    Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by Pradeep BM on 30/01/14.
 **/

#import "TAJProgressAlertView.h"
#import "TAJUtilities.h"
#import "TAJConstants.h"
#import "TAJAppDelegate.h"

#define MELD_VIEW_TOP_OFFSET_IPAD        25.0f
#define MELD_VIEW_TOP_OFFSET_IPHONE      15.0f

@interface TAJProgressAlertView ()

@property (weak, nonatomic) IBOutlet UILabel *messageLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@end

@implementation TAJProgressAlertView

- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
        
    }
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        
    }
    return self;
}

- (void)showMessage:(NSString *)message
{
    self.messageLabel.text = message;
    self.imageView.image = [UIImage imageNamed:@"loading"];
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"transform"];
    animation.delegate = self;
    animation.toValue = [NSValue valueWithCATransform3D:CATransform3DMakeRotation(M_PI , 0, 0, 1.0)];
    animation.duration = 0.3;
    animation.cumulative = YES;
    animation.repeatCount = INT_MAX;
    
    [TAJAppDelegate setScreenSizeForRes: self];
    
    self.mainView.layer.cornerRadius = 10;
    self.mainView.layer.masksToBounds = true;
    
    [self.imageView.layer addAnimation:animation forKey:@"animation"];
}

- (void)show:(BOOL)isPortraitMode
{
    [[[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0] addSubview:self];
//    UIView *view = [[[[UIApplication sharedApplication] keyWindow] subviews] objectAtIndex:0];
//    if ([TAJUtilities isIPhone])
//    {
//        if (isPortraitMode) {
//            self.center = view.center;
//        }
//        else
//        {
//         self.center = CGPointMake(view.frame.size.height/2, view.frame.size.width/2);
//        }
//    }
//    else
//    {
//        self.center = CGPointMake(view.frame.size.height/2, view.frame.size.width/2);
//    }
}

- (void)hide
{
    if (self)
    {
        [self removeFromSuperview];
    }
}

@end
