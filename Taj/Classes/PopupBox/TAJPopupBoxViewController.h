/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPopupBoxViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 21/04/14.
 **/

#import <UIKit/UIKit.h>

#import "TAJConstants.h"

@protocol PopupBoxDelegate <NSObject>

//PopupType will be passed
-(void) yesButtonPressed:(EPopupType)type;
-(void) noButtonPressed:(EPopupType)type;
-(void) okButtonPressed:(EPopupType)type;
-(void) closeButtonPressed:(EPopupType)type;

@end


//Note: After initializing set the delegate 

@interface TAJPopupBoxViewController : UIViewController
{
    
}

@property (nonatomic, weak) id<PopupBoxDelegate> delegate;
@property (nonatomic) EPopupButtonType buttonType;
@property (nonatomic) EPopupType popupType;
@property (strong, nonatomic) IBOutlet UILabel *message;
@property (strong, nonatomic) IBOutlet UIButton *yesButton;
@property (strong, nonatomic) IBOutlet UIButton *okButton;
@property (strong, nonatomic) IBOutlet UIButton *noButton;


- (void) updatePopupBasedOnTypes;

- (IBAction)yesPressed:(id)sender;
- (IBAction)okPressed:(id)sender;
- (IBAction)noPressed:(id)sender;
- (IBAction)closePressed:(id)sender;

@end
