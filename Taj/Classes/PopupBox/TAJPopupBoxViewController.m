/** * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * *
 * @File:
 *		TAJPopupBoxViewController.h
 * @Project:
 *		Taj
 * @Abstract:
 *
 * @Copyright:
 *      Copyright © 2012-2014, Grid Logic Software Pvt. Ltd.
 *	Written under contract by Robosoft Technologies Pvt. Ltd.
 * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */

/** Revision history (Most recent first)
 Created by  Raghavendra Kamat on 21/04/14.
 **/

#import "TAJPopupBoxViewController.h"
#import "TAJUtilities.h"

@interface TAJPopupBoxViewController ()

@end

@implementation TAJPopupBoxViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
    
}


- (void) updatePopupBasedOnTypes
{
    switch (self.buttonType)
    {
        case eYes_No_Type:
            
            [self.yesButton setTitle:YES_STRING forState:UIControlStateNormal];
            [self.noButton setTitle:NO_STRING forState:UIControlStateNormal];
            self.okButton.hidden = YES;
            break;
        
        case eOK_Cancel_Type:
            [self.yesButton setTitle:OK_STRING forState:UIControlStateNormal];
            [self.noButton setTitle:CANCEL_STRING forState:UIControlStateNormal];
            self.okButton.hidden = YES;

            break;
            
        case eOK_Type:
            [self.okButton setTitle:OK_STRING forState:UIControlStateNormal];
            self.yesButton.hidden = YES;
            self.noButton.hidden = YES;
            break;
        
        default:
            break;
    }
    
    self.message.text = [[TAJUtilities sharedUtilities] getTextForPopupType:self.popupType];
}

- (IBAction)yesPressed:(id)sender
{
    if (self.delegate)
    {
        [self.delegate yesButtonPressed:self.popupType];
    }
    [self.view removeFromSuperview];
}

- (IBAction)okPressed:(id)sender
{
    if (self.delegate)
    {
        [self.delegate okButtonPressed:self.popupType];
    }
    [self.view removeFromSuperview];
}

- (IBAction)noPressed:(id)sender
{
    if (self.delegate)
    {
        [self.delegate noButtonPressed:self.popupType];
    }
    [self.view removeFromSuperview];
}

- (IBAction)closePressed:(id)sender
{
    if (self.delegate)
    {
        [self.delegate closeButtonPressed:self.popupType];
    }
    [self.view removeFromSuperview];
}


@end
