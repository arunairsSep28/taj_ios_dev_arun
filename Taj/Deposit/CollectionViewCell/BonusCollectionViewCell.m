//
//  BonusCollectionViewCell.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "BonusCollectionViewCell.h"

@interface BonusCollectionViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *imgBg;
@property (weak, nonatomic) IBOutlet UILabel *lblPrice;
@property (weak, nonatomic) IBOutlet UILabel *lblPercent;
@property (weak, nonatomic) IBOutlet UILabel *lblCoupon;
@property (weak, nonatomic) IBOutlet UIView *viewContainer;

@end

@implementation BonusCollectionViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)updateCellWithBonusData:(Bonus *)bonusData {
    NSString * symbol = @"%";
    self.lblPrice.text = [NSString stringWithFormat:@"Rs. %@ - Rs. %@",bonusData.min_amount,bonusData.max_amount];
    self.lblPercent.text = [NSString stringWithFormat:@"%@%@ BONUS",bonusData.percentage,symbol];
    self.lblCoupon.text = bonusData.bonus_code;
    self.imgBg.image = [UIImage imageNamed:@"deposit-inactive"];
    if (bonusData.isSelected) {
        self.imgBg.image = [UIImage imageNamed:@"deposit-active"];
    }
}

@end
