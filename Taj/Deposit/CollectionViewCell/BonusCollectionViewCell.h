//
//  BonusCollectionViewCell.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Bonus.h"

NS_ASSUME_NONNULL_BEGIN

@interface BonusCollectionViewCell : UICollectionViewCell

- (void)updateCellWithBonusData:(Bonus *)bonusData;

@end

NS_ASSUME_NONNULL_END
