//
//  DepositViewController.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DepositViewController : UIViewController

-(instancetype)initWithRegistrationIsFromRegistration:(BOOL)isFromRegistration;

@end

NS_ASSUME_NONNULL_END
