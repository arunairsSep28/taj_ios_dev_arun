//
//  Bonus.h
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Bonus : NSObject

@property(strong, nonatomic) NSString *bonus_code;
@property(strong, nonatomic) NSString *max_amount;
@property(strong, nonatomic) NSString *min_amount;
@property(strong, nonatomic) NSString *percentage;
@property(strong, nonatomic) NSString *valid_from;
@property(strong, nonatomic) NSString *valid_to;

@property(assign, nonatomic) BOOL isSelected;

+ (Bonus *)sharedInstance;
- (Bonus *)assignData:(NSDictionary*)getData;
@end
