//
//  Bonus.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "Bonus.h"

@implementation Bonus
static Bonus *instance;

@synthesize max_amount, bonus_code,min_amount,percentage,valid_from,valid_to, isSelected;

+ (Bonus *)sharedInstance{
    
    @synchronized(self) {
        instance = [[super alloc] init];
    }
    return instance;
}

- (id)copyWithZone: (NSZone *) zone
{
    Bonus *copy = [[Bonus allocWithZone: zone] init];
    
    [copy setBonus_code:bonus_code];
    [copy setMax_amount:max_amount];
    [copy setMin_amount:min_amount];
    [copy setPercentage:percentage];
    [copy setValid_to:valid_to];
    [copy setValid_from:valid_from];
    [copy setIsSelected:isSelected];
    
    return copy;
}

- (Bonus *)assignData:(NSDictionary*)getData;
{
    bonus_code = [getData objectForKey:@"bonus_code"];
    max_amount = [getData objectForKey:@"max_amount"];
    min_amount = [getData objectForKey:@"min_amount"];
    percentage = [getData objectForKey:@"percentage"];
    valid_from = [getData objectForKey:@"valid_from"];
    valid_to = [getData objectForKey:@"valid_to"];
    
    isSelected = FALSE;
    return self;
}

- (BOOL)isContainsKey:(NSDictionary *)player key:(NSString *)key
{
    BOOL retVal = 0;
    NSArray *allKeys = [player allKeys];
    retVal = [allKeys containsObject:key];
    return retVal;
}

@end
