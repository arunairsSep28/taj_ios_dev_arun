//
//  DepositViewController.m
//  TajRummy
//
//  Created by svc on 03/06/19.
//  Copyright © 2019 Robosoft Technologies. All rights reserved.
//

#import "DepositViewController.h"
#import <PayUCustomBrowser/PayUCustomBrowser.h>

@interface DepositViewController ()<PUCBWebVCDelegate,PayUCBWebViewResponseDelegate,UITextFieldDelegate>

@property (weak, nonatomic) IBOutlet UIView *viewTopBar;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIButton *buttonBack;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UIView *viewAmountContainer;
@property (weak, nonatomic) IBOutlet UIView *view250;
@property (weak, nonatomic) IBOutlet UIView *view500;
@property (weak, nonatomic) IBOutlet UIView *view1000;
@property (weak, nonatomic) IBOutlet UIView *view5000;
@property (weak, nonatomic) IBOutlet UIView *view10000;
@property (weak, nonatomic) IBOutlet UIView *view20000;
@property (weak, nonatomic) IBOutlet UIView *viewOther;

@property (weak, nonatomic) IBOutlet UILabel *lbl250;
@property (weak, nonatomic) IBOutlet UILabel *lbl500;
@property (weak, nonatomic) IBOutlet UILabel *lbl1000;
@property (weak, nonatomic) IBOutlet UILabel *lbl5000;
@property (weak, nonatomic) IBOutlet UILabel *lbl10000;
@property (weak, nonatomic) IBOutlet UILabel *lbl20000;
@property (weak, nonatomic) IBOutlet UILabel *lblOther;

@property(strong, nonatomic) UIColor * selectedColor;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldAmount;

@property (weak, nonatomic) IBOutlet UIView *viewCoupon;
@property (weak, nonatomic) IBOutlet UITextField *txtFieldCoupon;

@property (strong, nonatomic) NSMutableArray *arrayBonusList;
@property (weak, nonatomic) IBOutlet UICollectionView *collectionView;
@property (strong, nonatomic) Bonus *selectedBonus;

typedef void (^urlRequestCompletionBlock)(NSURLResponse *response, NSData *data, NSError *connectionError);

@property (assign, nonatomic) NSInteger lowerlimit, upperlimit ;

@property(nonatomic,strong)NSMutableURLRequest *req;
@property(nonatomic,strong) NSString *payUKey, *saltKey;
@property(nonatomic,strong) NSString *hashKey;
@property(nonatomic,strong) NSString *transactionID;
@property(nonatomic, strong) NSString *amount;
@property(nonatomic, strong) NSString *firstname, *email, *udf1, *udf2, *udf3, *udf4, *udf5, *surl, *furl, *phone, *productinfo;

@property(nonatomic,strong)NSMutableData *dataResponse;

//Transaction Details
@property (weak, nonatomic) IBOutlet UIView *viewTransactionDetails;
@property (weak, nonatomic) IBOutlet UILabel *lblSuccessMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderID;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblRealChips;
@property (weak, nonatomic) IBOutlet UILabel *lblOrderDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;
@property (weak, nonatomic) IBOutlet UIView *viewDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnStatus;

@property (assign ,nonatomic) BOOL isFromRegistration;
@property(nonatomic, strong) NSString *deviceID;

@end

@implementation DepositViewController

static NSString *const kCellIdentifier = @"cellIdentifier";

-(instancetype)initWithRegistrationIsFromRegistration:(BOOL)isFromRegistration
{
    self = [super init];
    if (self) {
        _isFromRegistration = isFromRegistration;
    }
    
    return self;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureView];
    //sampletest
    
    // Register collection view cells
    [self.collectionView registerNib:[UINib nibWithNibName:NSStringFromClass([BonusCollectionViewCell class]) bundle:nil] forCellWithReuseIdentifier:kCellIdentifier];
    
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hideKeyboard)];
    tapGesture.cancelsTouchesInView = NO;
    [self.scrollView addGestureRecognizer:tapGesture];
    
    [self set250];
    [self getDepositDetails];
    
    // notifications for surl/furl responses
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataReceived:) name:@"passData" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(dataReceived:) name:@"paymentResponse" object:nil];
}

-(void)dataReceived:(NSNotification *)noti
{
    //NSLog(@"dataReceived internally from surl/furl:%@", noti.object);
    [self.navigationController popToRootViewControllerAnimated:YES];
    _dataResponse=noti.object;
    if(_dataResponse)
    {
        UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"paymentResponse" message:_dataResponse delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil ];
        [alert show];
    }
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)toInterfaceOrientation
{
    if ([TAJUtilities isIPhone])
    {
        return UIInterfaceOrientationIsPortrait(toInterfaceOrientation);
    }
    else
    {
        return UIInterfaceOrientationIsLandscape(toInterfaceOrientation);
    }
}


- (BOOL) shouldAutorotate
{
    return YES;
}

- (UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    if ([TAJUtilities isIPhone])
    {
        return (UIInterfaceOrientationMaskPortrait | UIInterfaceOrientationMaskPortraitUpsideDown);
    }
    else
    {
        return UIInterfaceOrientationMaskLandscape;
    }
}
-(void)configureView
{
    self.scrollView.hidden = YES;
    self.viewTransactionDetails.hidden = YES;
    
    self.viewAmountContainer.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.view250.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.view500.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.view1000.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.view5000.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.view10000.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.view20000.layer.borderColor = [UIColor lightGrayColor].CGColor;
    self.viewOther.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    self.selectedColor = [UIColor colorWithRed:139.0f/255.0f green:0.0f/255.0f blue:0.0f/255.0f alpha:1.0f];
    
    self.viewCoupon.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
    
    if ([TAJUtilities isIPhone])
    {
        self.deviceID = @"Mobile";//@"IPHONE";
    }
    else
    {
        self.deviceID = @"Tablet";//@"IPAD";
    }
    
}

#pragma mark - HELPERS

- (BOOL)validateInput
{
    NSUInteger checkDeposit = [self.txtFieldAmount.text integerValue];
    
    if (self.txtFieldAmount.text.length == 0) {
        [self.view makeToast:@"Please enter amount"];
        return YES;
    }
    if (checkDeposit < self.lowerlimit) {
        [self.view makeToast:@"Please enter minimum amount"];
        return YES;
    }
    else if (checkDeposit > self.upperlimit) {
        [self.view makeToast:[NSString stringWithFormat:@"Deposit amount should not exceed %ld",(long)self.upperlimit]];
        return YES;
    }
    
    return NO;
}

-(void)setDefaults {
    self.view250.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view500.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view1000.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view5000.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view10000.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.view20000.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.viewOther.backgroundColor = [UIColor groupTableViewBackgroundColor];
    self.lbl250.textColor = [UIColor blackColor];
    self.lbl500.textColor = [UIColor blackColor];
    self.lbl1000.textColor = [UIColor blackColor];
    self.lbl5000.textColor = [UIColor blackColor];
    self.lbl10000.textColor = [UIColor blackColor];
    self.lbl20000.textColor = [UIColor blackColor];
    self.lblOther.textColor = [UIColor blackColor];
}

-(void)set250 {
    [self setDefaults];
    self.view250.backgroundColor = self.selectedColor;
    self.lbl250.textColor = [UIColor whiteColor];
    self.txtFieldAmount.text = @"250";
}

-(void)set500 {
    [self setDefaults];
    self.view500.backgroundColor = self.selectedColor;
    self.lbl500.textColor = [UIColor whiteColor];
    self.txtFieldAmount.text = @"500";
}

-(void)set1000 {
    [self setDefaults];
    self.view1000.backgroundColor = self.selectedColor;
    self.lbl1000.textColor = [UIColor whiteColor];
    self.txtFieldAmount.text = @"1000";
}

-(void)set5000 {
    [self setDefaults];
    self.view5000.backgroundColor = self.selectedColor;
    self.lbl5000.textColor = [UIColor whiteColor];
    self.txtFieldAmount.text = @"5000";
}

-(void)set10000 {
    [self setDefaults];
    self.view10000.backgroundColor = self.selectedColor;
    self.lbl10000.textColor = [UIColor whiteColor];
    self.txtFieldAmount.text = @"10000";
}

-(void)set20000 {
    [self setDefaults];
    self.view20000.backgroundColor = self.selectedColor;
    self.lbl20000.textColor = [UIColor whiteColor];
    self.txtFieldAmount.text = @"20000";
}

-(void)setOther {
    [self setDefaults];
    self.viewOther.backgroundColor = self.selectedColor;
    self.lblOther.textColor = [UIColor whiteColor];
    NSUInteger checkDeposit = [self.txtFieldAmount.text integerValue];
    
    if (checkDeposit == 250 || checkDeposit == 500 || checkDeposit == 1000 || checkDeposit == 5000 || checkDeposit ==10000 || checkDeposit == 20000) {
        self.txtFieldAmount.text = @"";
    }
    else {
        
    }
    
}

- (void)hideKeyboard
{
    [self.view endEditing:YES];
}

- (void)getOrderDetails {
    
    NSDictionary *params = @{@"orderid": self.transactionID};
    NSString * URL = [NSString stringWithFormat:@"%@%@%@?orderid=%@",BASE_URL,API_BASE_URL,ORDER_DETAILS,self.transactionID];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
    
#if DEBUG
    NSLog(@"ORDERDETAILS PARAMS: %@", params);
    NSLog(@"ORDERDETAILS URL: %@", URL);
    NSLog(@"ORDERDETAILS header: %@", header);
#endif
    
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
#if DEBUG
            NSLog(@"ORDERDETAILS Response :%@",responseData);
#endif
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                self.scrollView.hidden = YES;
                self.viewTransactionDetails.hidden = NO;
                self.lblOrderID.text = [NSString stringWithFormat:@": %@",[responseData valueForKey:@"orderid"]];
                self.lblOrderStatus.text = [NSString stringWithFormat:@": %@",[responseData valueForKey:@"orderstatus"]];
                self.lblOrderAmount.text = [NSString stringWithFormat:@": %@",[responseData valueForKey:@"order_amount"]];
                self.lblRealChips.text = [NSString stringWithFormat:@": %@",[responseData valueForKey:@"user_balance"]];
                NSString * date = [[NSString stringWithFormat:@": %@",[responseData valueForKey:@"order_time"]] stringByReplacingOccurrencesOfString:@"T" withString:@" "];
                
                self.lblOrderDate.text = [NSString stringWithFormat:@"%@",date];
                
                id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                
                NSDictionary *userData=@{
                                         @"userId":userId,
                                         @"device_type":self.deviceID,
                                         @"client_type":@"iOS",
                                         @"amount" : self.amount,
                                         @"bonuscode" : self.txtFieldCoupon.text,
                                         @"modeofdeposit" :@"creditcard"
                                         };
                [weAnalytics trackEventWithName:@"PaymentSuccess" andValue:userData];
            }
            else {
                self.viewTransactionDetails.hidden = NO;
                self.scrollView.hidden = YES;
                self.lblSuccessMessage.text = @"Payment failed";
                [self.btnStatus setTitle:@"DEPOSIT AGAIN" forState:UIControlStateNormal];
                self.viewDetails.hidden = YES;
                self.imgStatus.image = [UIImage imageNamed:@"failure"];
            }
        });
        
    } errorBlock:^(NSString *errorString) {
#if DEBUG
        NSLog(@"ORDERDETAILS ERROR : %@",errorString);
#endif
    }];
}

-(void)getPaymentDetails {
    
    [self getOrderDetails];
}

-(void)getDepositDetails {
    [self.view makeToastActivity:CSToastPositionCenter];
    NSString * URL = [NSString stringWithFormat:@"%@%@%@",BASE_URL,API_BASE_URL,DEPOSIT_CHECK];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"header %@", header);
    NSLog(@"DEPOSIT_CHECK URL %@", URL);
#endif
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
#if DEBUG
            NSLog(@"DEPOSIT_CHECK Response :%@",responseData);
#endif
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                
                self.scrollView.hidden = NO;
#if DEBUG
                NSLog(@"BONUS LIST DATA : %@",[responseData valueForKey:@"data"][@"bonus_list"]);
#endif
                self.arrayBonusList = [[NSMutableArray alloc] init];
                
                for (NSMutableDictionary *ordersDic in [responseData valueForKey:@"data"][@"bonus_list"]){
                    
                    Bonus *bonus = [Bonus sharedInstance];
                    [bonus assignData:ordersDic];
                    [self.arrayBonusList addObject:bonus];
                }
                
                [self.collectionView reloadData];
                //NSLog(@"COUNT : %lu",(unsigned long)self.arrayBonusList.count);
                
                self.lowerlimit = [[responseData valueForKey:@"data"][@"lowerlimit"] integerValue];
                self.upperlimit = [[responseData valueForKey:@"data"][@"upperlimit"] integerValue];
                NSLog(@"lowerlimit : %ld",(long)_lowerlimit);
                NSLog(@"upperlimit : %ld",(long)_upperlimit);
            }
            else {
                [self.view hideToastActivity];
                //[self.view makeToast:[responseData valueForKey:@"error"]];
                UIAlertController* alert = [UIAlertController alertControllerWithTitle:nil
                                                                               message:[responseData valueForKey:@"message"]
                                                                        preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction* defaultAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault
                                                                      handler:^(UIAlertAction * action) {
                                                                          [self dismissViewControllerAnimated:NO completion:nil];
                                                                      }];
                
                [alert addAction:defaultAction];
                [self presentViewController:alert animated:YES completion:nil];
            }
        });
        
    } errorBlock:^(NSString *errorString) {
#if DEBUG
        NSLog(@"PROFILE ERROR : %@",errorString);
#endif
        [self.view hideToastActivity];
        [self.view makeToast:errorString];
    }];
}



-(void)bonusCheck {
    [self.view makeToastActivity:CSToastPositionCenter];
    NSString * URL = [NSString stringWithFormat:@"%@%@%@?client_type=iOS&bonus_code=%@&selectamount=%@&device_type=Mobile",BASE_URL,API_BASE_URL,BONUS_CHECK,self.txtFieldCoupon.text,self.txtFieldAmount.text];
    NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
    NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
    NSLog(@"header %@", header);
    NSLog(@"BONUS_CHECK URL %@", URL);
#endif
    [[Service sharedInstance] getRequestWithHeader:header URL:URL withParams:nil completionBlock:^(id responseData) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.view hideToastActivity];
#if DEBUG
            NSLog(@"BONUS_CHECK Response :%@",responseData);
#endif
            if ([[responseData valueForKey:@"status"] isEqualToString:@"Success"]) {
                
                [self.view makeToast:[responseData valueForKey:@"message"]];
                
            }
            else {
                [self.view hideToastActivity];
                [self.view makeToast:[responseData valueForKey:@"message"]];
                
            }
        });
        
    } errorBlock:^(NSString *errorString) {
#if DEBUG
        NSLog(@"BONUS_CHECK ERROR : %@",errorString);
#endif
        [self.view hideToastActivity];
        [self.view makeToast:errorString];
    }];
}

-(void)paymentRequest
{
    BOOL error = [self validateInput];
    if (!error) {
        [self.view makeToastActivity:CSToastPositionCenter];
        NSString * deviceType;
        
        if ([TAJUtilities isIPhone])
        {
            deviceType = @"Mobile";//@"IPHONE";
        }
        else
        {
            deviceType = @"Tablet";//@"IPAD";
        }
        NSDictionary *params = @{
                                 //@"device_type" : deviceType,
                                 //@"client_type" : @"iOS",
                                 @"paymenttype": @"creditcard",
                                 @"modeofdeposit": @"creditcard",
                                 @"amount": self.txtFieldAmount.text,
                                 @"bonuscode" : self.txtFieldCoupon.text,
                                 @"cardtype" : @"visa",
                                 @"mode" : @"cc"
                                 };
#if DEBUG
        NSLog(@"PAYMENT REQUEST PARAMS: %@", params);
#endif
        
        NSString * URL = [NSString stringWithFormat:@"%@%@?device_type=%@&client_type=iOS",BASE_URL,PAYMENT_REQUEST,deviceType];
        NSString * token = [[NSUserDefaults standardUserDefaults]valueForKey:TOKEN_KEY];
        NSString * header = [NSString stringWithFormat:@"Token %@",token];
#if DEBUG
        NSLog(@"URL : %@", URL);
        NSLog(@"HEADER: %@", header);
#endif
        [Service postRequest:[NSString stringWithFormat:@"%@",URL] isHeader:YES headerValue:header withParams:params :^(id jsondata, BOOL success){
            dispatch_async(dispatch_get_main_queue(), ^{
                if (success)
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        [self.view hideToastActivity];
#if DEBUG
                        // NSLog(@"RESPONSE: %@", jsonResponse);
#endif
                        if ([[jsondata valueForKey:@"status"] isEqualToString:@"Success"]) {
                            NSDictionary * jsonDictionay = jsondata;
                            NSDictionary *tempDict = [jsonDictionay dictionaryRemovingNSNullValues];
                            NSLog(@"%@", tempDict);
                            
#if DEBUG
                            NSLog(@"PAYMENT REQUEST RESPONSE: %@", tempDict);
                            NSLog(@"hash : %@",[tempDict valueForKey:@"data"][@"hash"]);
                            NSLog(@"key : %@",[tempDict valueForKey:@"data"][@"key"]);
                            NSLog(@"txnid : %@",[tempDict valueForKey:@"data"][@"txnid"]);
                            NSLog(@"salt : %@",[tempDict valueForKey:@"data"][@"salt"]);
                            
#endif
                            self.payUKey = [tempDict valueForKey:@"data"][@"key"];
                            self.saltKey = [tempDict valueForKey:@"data"][@"salt"];
                            self.hashKey = [tempDict valueForKey:@"data"][@"hash"];
                            self.transactionID = [tempDict valueForKey:@"data"][@"txnid"];
                            self.firstname = [tempDict valueForKey:@"data"][@"firstname"];
                            self.email = [tempDict valueForKey:@"data"][@"email"];
                            self.amount = [tempDict valueForKey:@"data"][@"amount"];
                            self.phone = [tempDict valueForKey:@"data"][@"phone"];
                            self.furl = [tempDict valueForKey:@"data"][@"furl"];
                            self.surl = [tempDict valueForKey:@"data"][@"surl"];
                            
                            self.udf1 = [tempDict valueForKey:@"data"][@"udf1"];
                            self.udf2 = [tempDict valueForKey:@"data"][@"udf2"];
                            self.udf3 = [tempDict valueForKey:@"data"][@"udf3"];
                            self.udf4 = [tempDict valueForKey:@"data"][@"udf4"];
                            self.udf5 = [tempDict valueForKey:@"data"][@"udf5"];
                            
                            self.productinfo = [tempDict valueForKey:@"data"][@"productinfo"];
#if DEBUG
                            NSLog(@"key : %@",_payUKey);
                            NSLog(@"Salt key : %@",_saltKey);
                            
                            NSLog(@"hashKey : %@",_hashKey);
                            NSLog(@"transactionID : %@",_transactionID);
                            NSLog(@"firstname : %@",_firstname);
                            NSLog(@"amount : %@",_amount);
                            NSLog(@"email : %@",_email);
                            
                            NSLog(@"FURL : %@",_furl);
                            NSLog(@"SURL : %@",_surl);
                            NSLog(@"UDF1 : %@",_udf1);
                            NSLog(@"UDF2 : %@",_udf2);
                            NSLog(@"UDF3 : %@",_udf3);
                            NSLog(@"UDF4 : %@",_udf4);
                            NSLog(@"UDF5 : %@",_udf5);
#endif
                            [[NSUserDefaults standardUserDefaults] setObject:self.surl forKey:SURL];
                            
                            [[NSUserDefaults standardUserDefaults] synchronize];
                            
                            id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
                            NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
                            
                            NSDictionary *userData=@{
                                                     @"userId":userId,
                                                     @"device_type":self.deviceID,
                                                     @"client_type":@"iOS",
                                                     @"amount" : self.amount,
                                                     @"bonuscode" : self.txtFieldCoupon.text,
                                                     @"modeofdeposit" :@"creditcard"
                                                     };
                            [weAnalytics trackEventWithName:@"PaymentInitiated" andValue:userData];
                            
                            [self payUCB];
                        }
                        else {
#if DEBUG
                            NSLog(@"PAYMENT REST Error : %@",jsondata);
#endif
                            [self.view makeToast:[jsondata valueForKey:@"message"]];
                        }
                    });
                }
                else
                {
                    [self.view hideToastActivity];
                    [self.view makeToast:@"Error while fetchng data."];
                }
            });
        }];
    }
}

#pragma mark - ACTIONS

- (IBAction)backClicked:(UIButton *)sender {
    if (self.isFromRegistration) {
        TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
        
#if NEW_DESIGN_IMPLEMENTATION
        [delegate loadPrelobbyScreen];
#else
        [delegate loadHomePageWindow];
#endif
    }
    else {
        [self dismissViewControllerAnimated:NO completion:nil];
        //[self.navigationController popViewControllerAnimated:YES];
    }
}

- (IBAction)view250Clicked:(UIButton *)sender {
    [self set250];
}

- (IBAction)view500Clicked:(UIButton *)sender {
    [self set500];
}

- (IBAction)view1000Clicked:(UIButton *)sender {
    [self set1000];
}

- (IBAction)view5000Clicked:(UIButton *)sender {
    [self set5000];
}

- (IBAction)view10000Clicked:(UIButton *)sender {
    [self set10000];
}

- (IBAction)view20000Clicked:(UIButton *)sender {
    [self set20000];
}

- (IBAction)viewOtherClicked:(UIButton *)sender {
    [self setOther];
}

- (IBAction)applyClicked:(UIButton *)sender {
    [self hideKeyboard];
    if (self.txtFieldCoupon.text.length != 0) {
        //coupon service call
        [self bonusCheck];
    }
    else {
        [self.view makeToast:@"Please enter coupon code"];
    }
}

- (IBAction)paymentClicked:(UIButton *)sender {
    [self paymentRequest];
}

- (IBAction)paymentStatusClicked:(UIButton *)sender {
    if ([self.btnStatus.titleLabel.text isEqualToString:@"DEPOSIT AGAIN"]) {
        self.viewTransactionDetails.hidden = YES;
        self.scrollView.hidden = NO;
    }
    else {
        TAJAppDelegate *delegate = (TAJAppDelegate*)[[UIApplication sharedApplication] delegate];
        
#if NEW_DESIGN_IMPLEMENTATION
        [delegate loadPrelobbyScreen];
#else
        [delegate loadHomePageWindow];
#endif
    }
}

#pragma mark - CollectionView Methods

- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.arrayBonusList count];
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if ([TAJUtilities isIPhone]) {
        CGFloat width = ([TAJUtilities screenWidth] - 44) / 2.0f;
        return CGSizeMake(width, 144);
    }
    else {
        CGFloat width = ([TAJUtilities screenWidth] - 44) / 3.0f;
        return CGSizeMake(width, 240);
    }
    
}

- (UIEdgeInsets)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout insetForSectionAtIndex:(NSInteger)section
{
    return UIEdgeInsetsMake(0, 0, 0, 0);
    // T,L,B,R
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    BonusCollectionViewCell *cell = [self.collectionView dequeueReusableCellWithReuseIdentifier:kCellIdentifier forIndexPath:indexPath];
    [cell updateCellWithBonusData:self.arrayBonusList[indexPath.item]];
    return cell;
}

- (void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    Bonus * bonus;
    bonus = self.arrayBonusList[indexPath.row];
    
    //    if (bonus.isSelected) {
    //        [self makeEveryTypeBonusUnselect];
    //        [self.collectionView reloadData];
    //        self.selectedBonus = nil;
    //        return;
    //    }
    
    [self makeEveryTypeBonusUnselect];
    bonus.isSelected = TRUE;
    [self.collectionView reloadData];
    self.selectedBonus = bonus;
    
    self.txtFieldCoupon.text = self.selectedBonus.bonus_code;
    
    [self hideKeyboard];
    if (self.txtFieldCoupon.text.length != 0) {
        //coupon service call
        [self bonusCheck];
    }
    else {
        [self.view makeToast:@"Please enter coupon code"];
    }
}

-(void)makeEveryTypeBonusUnselect {
    for (Bonus *bonusList in self.arrayBonusList) {
        bonusList.isSelected = FALSE;
    }
    [self.collectionView reloadData];
}

#pragma mark - PAYU METHODS

-(void)payUCB{
    PUCBConfiguration *cbConfig = [PUCBConfiguration getSingletonInstance];
    cbConfig.enableWKWebView = false;
    
    //Production URL
    NSURL *restURL=[NSURL URLWithString:@"https://secure.payu.in/_payment"];
    //test URL
    //NSURL *restURL=[NSURL URLWithString:@"https://test.payu.in/_payment"];
    NSString *postData=[NSString stringWithFormat:@"key=%@&txnid=%@&amount=%@&productinfo=%@&firstname=%@&email=%@&udf1=%@&udf2=%@&udf3=%@&udf4=%@&udf5=%@&surl=%@&furl=%@&phone=%@&hash=%@&device_type=2",self.payUKey,self.transactionID,self.amount,self.productinfo,self.firstname,self.email,self.udf1,self.udf2,self.udf3,self.udf4,self.udf5,self.surl,self.furl,self.phone,self.hashKey];
#if DEBUG
    NSLog(@"PAY U POST : %@",postData);
#endif
    //@"pw8atV"
    NSError *err = nil;
    PUCBWebVC *webVC = [[PUCBWebVC alloc] initWithPostParam:postData
                                                        url:restURL
                                                merchantKey:self.payUKey
                                                      error:&err];
    webVC.cbWebVCDelegate = self;
    
    if (!err) {
        //[self.navigationController pushViewController:webVC animated:true];
        [self presentViewController:webVC animated:NO completion:nil];
    }
    
}

- (void)PayUSuccessResponse:(id) payUResponse SURLResponse:(id) surlResponse
{
    //NSLog(@"PayUSuccessResponse : %@, RESPONSE : %@",surlResponse,payUResponse);
}

- (void)PayUFailureResponse:(id) payUResponse FURLResponse:(id) furlResponse
{
    //NSLog(@"PayUFailureResponse : %@RESPONSE : %@",furlResponse,payUResponse);
}

- (void)PayUSuccessResponse:(id)response
{
    self.scrollView.hidden = YES;
    
    NSData *data = [response dataUsingEncoding:NSUTF8StringEncoding];
    id json = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
#if DEBUG
    NSLog(@"PAYU SUCCESS : %@",json);
#endif
    NSString * status = [NSString stringWithFormat:@"%@",[json valueForKey:@"status"]];
    if ([status isEqualToString:@"success"]) {
        self.lblSuccessMessage.text = @"Payment success!";
        [self.btnStatus setTitle:@"PLAY NOW" forState:UIControlStateNormal];
        self.viewDetails.hidden = NO;
        self.imgStatus.image = [UIImage imageNamed:@"Success"];
        //[self performSelector:@selector(getPaymentDetails) withObject:self afterDelay:5.0 ];
        
        self.scrollView.hidden = YES;
        self.viewTransactionDetails.hidden = NO;
        self.lblOrderID.text = [NSString stringWithFormat:@": %@",[json valueForKey:@"txnid"]];
        self.lblOrderStatus.text = [NSString stringWithFormat:@": %@",[json valueForKey:@"status"]];
        self.lblOrderAmount.text = [NSString stringWithFormat:@": %@",[json valueForKey:@"amount"]];
        //self.lblRealChips.text = [NSString stringWithFormat:@": %@",[json valueForKey:@"user_balance"]];
        NSString * date = [NSString stringWithFormat:@": %@",[json valueForKey:@"addedon"]];
        
        self.lblOrderDate.text = [NSString stringWithFormat:@"%@",date];
        
        id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
        NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
        
        NSDictionary *userData=@{
                                 @"userId":userId,
                                 @"device_type":self.deviceID,
                                 @"client_type":@"iOS",
                                 @"amount" : self.amount,
                                 @"bonuscode" : self.txtFieldCoupon.text,
                                 @"modeofdeposit" :@"creditcard"
                                 };
        [weAnalytics trackEventWithName:@"PaymentSuccess" andValue:userData];
        
    }
    else {
        self.lblSuccessMessage.text = @"Payment failed";
        [self.btnStatus setTitle:@"DEPOSIT AGAIN" forState:UIControlStateNormal];
        self.viewDetails.hidden = YES;
        self.imgStatus.image = [UIImage imageNamed:@"failure"];
    }
}

- (void)PayUFailureResponse:(id)response
{
#if DEBUG
    NSLog(@"PAYU FAIL : %@",response);
#endif
    self.viewTransactionDetails.hidden = NO;
    self.scrollView.hidden = YES;
    self.lblSuccessMessage.text = @"Payment failed";
    [self.btnStatus setTitle:@"DEPOSIT AGAIN" forState:UIControlStateNormal];
    self.viewDetails.hidden = YES;
    self.imgStatus.image = [UIImage imageNamed:@"failure"];
    
    id<WEGAnalytics> weAnalytics = [WebEngage sharedInstance].analytics;
    NSString *userId = [[NSUserDefaults standardUserDefaults] objectForKey:USENAME_KEY];
    
    NSDictionary *userData=@{
                             @"userId":userId,
                             @"device_type":self.deviceID,
                             @"client_type":@"iOS",
                             @"amount" : self.amount,
                             @"bonuscode" : self.txtFieldCoupon.text,
                             @"modeofdeposit" :@"creditcard"
                             };
    [weAnalytics trackEventWithName:@"Payment Failed" andValue:userData];
}

// General callbacks in case of ongoing transaction getting issues
- (void)PayUConnectionError:(NSDictionary *)notification
{
#if DEBUG
    NSLog(@"PAYU ConnectionError : %@",notification);
#endif
}

- (void)PayUTransactionCancel
{
    //NSLog(@"PAYU TransactionCancel");
}

- (void)shouldDismissVCOnBackPress
{
    //NSLog(@"PAYU Dismiss");
    [self dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - TEXTFIELD DELEGATES

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    NSUInteger checkDeposit = [self.txtFieldAmount.text integerValue];
    
    if (checkDeposit == 250) {
        [self set250];
    }
    else if (checkDeposit == 500) {
        [self set500];
    }
    else if (checkDeposit == 1000) {
        [self set1000];
    }
    else if (checkDeposit == 5000) {
        [self set5000];
    }
    else if (checkDeposit == 10000) {
        [self set10000];
    }
    else if (checkDeposit == 20000) {
        [self set20000];
    }
    else {
        [self setOther];
    }
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    // Mobile Number
    if (textField == self.txtFieldAmount) {
        // To block from entering zero as first digit of phone number
        if (range.location == 0 && [string isEqualToString:@"0"]) {
            return NO;
        }
        NSCharacterSet *characterSet = [[NSCharacterSet characterSetWithCharactersInString:PHONE_NO_VALIDATION_CHARACTER_SET] invertedSet];
        NSString *filteredString = [[string componentsSeparatedByCharactersInSet:characterSet] componentsJoinedByString:@""];
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        // Limit phone number input to 8 digits
        return ([string isEqualToString:filteredString] ? newLength <= 5 : NO);
    }
    return YES;
}

@end
